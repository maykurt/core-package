/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const LovColumnType = {
    Label: 'label',
    Text: 'text',
    Date: 'date',
    Select: 'select',
    Numeric: 'numeric',
    Boolean: 'boolean',
    Lov: 'lov',
};
export { LovColumnType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWNvbHVtbi10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvbG92LWNvbHVtbi10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsT0FBUSxPQUFPO0lBQ2YsTUFBTyxNQUFNO0lBQ2IsTUFBTyxNQUFNO0lBQ2IsUUFBUyxRQUFRO0lBQ2pCLFNBQVUsU0FBUztJQUNuQixTQUFVLFNBQVM7SUFDbkIsS0FBTSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gTG92Q29sdW1uVHlwZSB7XHJcbiAgTGFiZWwgPSAnbGFiZWwnLFxyXG4gIFRleHQgPSAndGV4dCcsXHJcbiAgRGF0ZSA9ICdkYXRlJyxcclxuICBTZWxlY3QgPSAnc2VsZWN0JyxcclxuICBOdW1lcmljID0gJ251bWVyaWMnLFxyXG4gIEJvb2xlYW4gPSAnYm9vbGVhbicsXHJcbiAgTG92ID0gJ2xvdidcclxufVxyXG4iXX0=