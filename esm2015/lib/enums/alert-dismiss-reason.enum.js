/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const AlertDismissReason = {
    Cancel: 'cancel',
    Backdrop: 'backdrop',
    Close: 'close',
    Esc: 'esc',
    Timer: 'timer',
};
export { AlertDismissReason };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtZGlzbWlzcy1yZWFzb24uZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lbnVtcy9hbGVydC1kaXNtaXNzLXJlYXNvbi5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUNJLFFBQVMsUUFBUTtJQUNqQixVQUFXLFVBQVU7SUFDckIsT0FBUSxPQUFPO0lBQ2YsS0FBTSxLQUFLO0lBQ1gsT0FBUSxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gQWxlcnREaXNtaXNzUmVhc29uIHtcclxuICAgIENhbmNlbCA9ICdjYW5jZWwnLFxyXG4gICAgQmFja2Ryb3AgPSAnYmFja2Ryb3AnLFxyXG4gICAgQ2xvc2UgPSAnY2xvc2UnLFxyXG4gICAgRXNjID0gJ2VzYycsXHJcbiAgICBUaW1lciA9ICd0aW1lcidcclxufVxyXG5cclxuIl19