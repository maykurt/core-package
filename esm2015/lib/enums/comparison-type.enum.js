/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const Comparison = {
    In: 0,
    LessThanOrEqualTo: 1,
    LessThan: 2,
    IsNullOrWhiteSpace: 3,
    IsNull: 4,
    IsNotNullNorWhiteSpace: 5,
    IsNotNull: 6,
    IsNotEmpty: 7,
    IsEmpty: 8,
    StartsWith: 9,
    GreaterThanOrEqualTo: 10,
    GreaterThan: 11,
    EqualTo: 12,
    EndsWith: 13,
    DoesNotContain: 14,
    Contains: 15,
    Between: 16,
    NotEqualTo: 17,
};
export { Comparison };
Comparison[Comparison.In] = 'In';
Comparison[Comparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
Comparison[Comparison.LessThan] = 'LessThan';
Comparison[Comparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
Comparison[Comparison.IsNull] = 'IsNull';
Comparison[Comparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
Comparison[Comparison.IsNotNull] = 'IsNotNull';
Comparison[Comparison.IsNotEmpty] = 'IsNotEmpty';
Comparison[Comparison.IsEmpty] = 'IsEmpty';
Comparison[Comparison.StartsWith] = 'StartsWith';
Comparison[Comparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
Comparison[Comparison.GreaterThan] = 'GreaterThan';
Comparison[Comparison.EqualTo] = 'EqualTo';
Comparison[Comparison.EndsWith] = 'EndsWith';
Comparison[Comparison.DoesNotContain] = 'DoesNotContain';
Comparison[Comparison.Contains] = 'Contains';
Comparison[Comparison.Between] = 'Between';
Comparison[Comparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const DefaultComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
export { DefaultComparison };
DefaultComparison[DefaultComparison.EqualTo] = 'EqualTo';
DefaultComparison[DefaultComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const TextComparison = {
    Contains: 15,
    DoesNotContain: 14,
    EndsWith: 13,
    EqualTo: 12,
    IsEmpty: 8,
    IsNotEmpty: 7,
    IsNotNull: 6,
    IsNotNullNorWhiteSpace: 5,
    IsNull: 4,
    IsNullOrWhiteSpace: 3,
    NotEqualTo: 17,
    StartsWith: 9,
};
export { TextComparison };
TextComparison[TextComparison.Contains] = 'Contains';
TextComparison[TextComparison.DoesNotContain] = 'DoesNotContain';
TextComparison[TextComparison.EndsWith] = 'EndsWith';
TextComparison[TextComparison.EqualTo] = 'EqualTo';
TextComparison[TextComparison.IsEmpty] = 'IsEmpty';
TextComparison[TextComparison.IsNotEmpty] = 'IsNotEmpty';
TextComparison[TextComparison.IsNotNull] = 'IsNotNull';
TextComparison[TextComparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
TextComparison[TextComparison.IsNull] = 'IsNull';
TextComparison[TextComparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
TextComparison[TextComparison.NotEqualTo] = 'NotEqualTo';
TextComparison[TextComparison.StartsWith] = 'StartsWith';
/** @enum {number} */
const NumberComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
export { NumberComparison };
NumberComparison[NumberComparison.Between] = 'Between';
NumberComparison[NumberComparison.EqualTo] = 'EqualTo';
NumberComparison[NumberComparison.GreaterThan] = 'GreaterThan';
NumberComparison[NumberComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
NumberComparison[NumberComparison.LessThan] = 'LessThan';
NumberComparison[NumberComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
NumberComparison[NumberComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const BooleanComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
export { BooleanComparison };
BooleanComparison[BooleanComparison.EqualTo] = 'EqualTo';
BooleanComparison[BooleanComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const DateComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
export { DateComparison };
DateComparison[DateComparison.Between] = 'Between';
DateComparison[DateComparison.EqualTo] = 'EqualTo';
DateComparison[DateComparison.GreaterThan] = 'GreaterThan';
DateComparison[DateComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
DateComparison[DateComparison.LessThan] = 'LessThan';
DateComparison[DateComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
DateComparison[DateComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const ComparisonType = {
    Default: 1,
    Text: 2,
    Number: 3,
    Date: 4,
    Boolean: 5,
};
export { ComparisonType };
ComparisonType[ComparisonType.Default] = 'Default';
ComparisonType[ComparisonType.Text] = 'Text';
ComparisonType[ComparisonType.Number] = 'Number';
ComparisonType[ComparisonType.Date] = 'Date';
ComparisonType[ComparisonType.Boolean] = 'Boolean';
export var ComparisonList;
(function (ComparisonList) {
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function values(comparisonType) {
        return Object.keys(this.getEnumAccordingToType(comparisonType)).filter((type) => isNaN((/** @type {?} */ (type))) && type !== 'values').sort();
    }
    ComparisonList.values = values;
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function getEnumAccordingToType(comparisonType) {
        /** @type {?} */
        let enums;
        if (!comparisonType) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Default) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Text) {
            enums = TextComparison;
        }
        else if (comparisonType === ComparisonType.Number) {
            enums = NumberComparison;
        }
        else if (comparisonType === ComparisonType.Boolean) {
            enums = BooleanComparison;
        }
        else if (comparisonType === ComparisonType.Date) {
            enums = DateComparison;
        }
        return enums;
    }
    ComparisonList.getEnumAccordingToType = getEnumAccordingToType;
    /**
     * @param {?} comparisonType
     * @param {?=} key
     * @param {?=} label
     * @return {?}
     */
    function createKeyLabelArray(comparisonType, key = 'Id', label = 'Definition') {
        /** @type {?} */
        let enumKeys = this.values(comparisonType);
        /** @type {?} */
        let newKeyLabelArray = [];
        enumKeys.forEach((enumKey) => {
            /** @type {?} */
            let newValue = {};
            newValue[key] = this.getEnumAccordingToType(comparisonType)[enumKey];
            newValue[label] = enumKey;
            newKeyLabelArray.push(newValue);
        });
        return newKeyLabelArray;
    }
    ComparisonList.createKeyLabelArray = createKeyLabelArray;
})(ComparisonList || (ComparisonList = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGFyaXNvbi10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBRUUsS0FBTTtJQUNOLG9CQUFxQjtJQUNyQixXQUFZO0lBQ1oscUJBQXNCO0lBQ3RCLFNBQVU7SUFDVix5QkFBMEI7SUFDMUIsWUFBYTtJQUNiLGFBQWM7SUFDZCxVQUFXO0lBQ1gsYUFBYztJQUNkLHdCQUF5QjtJQUN6QixlQUFnQjtJQUNoQixXQUFZO0lBQ1osWUFBYTtJQUNiLGtCQUFtQjtJQUNuQixZQUFhO0lBQ2IsV0FBWTtJQUNaLGNBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBTWYsV0FBWTtJQUNaLGNBQWU7Ozs7Ozs7SUFJZixZQUFhO0lBQ2Isa0JBQW1CO0lBQ25CLFlBQWE7SUFDYixXQUFZO0lBQ1osVUFBVztJQUNYLGFBQWM7SUFDZCxZQUFhO0lBQ2IseUJBQTBCO0lBQzFCLFNBQVU7SUFDVixxQkFBc0I7SUFDdEIsY0FBZTtJQUNmLGFBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBSWQsV0FBWTtJQUNaLFdBQVk7SUFDWixlQUFnQjtJQUNoQix3QkFBeUI7SUFDekIsV0FBWTtJQUNaLG9CQUFxQjtJQUNyQixjQUFlOzs7Ozs7Ozs7Ozs7SUFJZixXQUFZO0lBQ1osY0FBZTs7Ozs7OztJQUlmLFdBQVk7SUFDWixXQUFZO0lBQ1osZUFBZ0I7SUFDaEIsd0JBQXlCO0lBQ3pCLFdBQVk7SUFDWixvQkFBcUI7SUFDckIsY0FBZTs7Ozs7Ozs7Ozs7O0lBSWYsVUFBVztJQUNYLE9BQVE7SUFDUixTQUFVO0lBQ1YsT0FBUTtJQUNSLFVBQVc7Ozs7Ozs7O0FBR2IsTUFBTSxLQUFXLGNBQWMsQ0FxQzlCO0FBckNELFdBQWlCLGNBQWM7Ozs7O0lBRTdCLFNBQWdCLE1BQU0sQ0FBQyxjQUErQjtRQUNwRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUNwRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLG1CQUFLLElBQUksRUFBQSxDQUFDLElBQUksSUFBSSxLQUFLLFFBQVEsQ0FDaEQsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNYLENBQUM7SUFKZSxxQkFBTSxTQUlyQixDQUFBOzs7OztJQUVELFNBQWdCLHNCQUFzQixDQUFDLGNBQStCOztZQUNoRSxLQUFVO1FBQ2QsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNuQixLQUFLLEdBQUcsaUJBQWlCLENBQUM7U0FDM0I7YUFBTSxJQUFJLGNBQWMsS0FBSyxjQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3BELEtBQUssR0FBRyxpQkFBaUIsQ0FBQztTQUMzQjthQUFNLElBQUksY0FBYyxLQUFLLGNBQWMsQ0FBQyxJQUFJLEVBQUU7WUFDakQsS0FBSyxHQUFHLGNBQWMsQ0FBQztTQUN4QjthQUFNLElBQUksY0FBYyxLQUFLLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDbkQsS0FBSyxHQUFHLGdCQUFnQixDQUFDO1NBQzFCO2FBQU0sSUFBSSxjQUFjLEtBQUssY0FBYyxDQUFDLE9BQU8sRUFBRTtZQUNwRCxLQUFLLEdBQUcsaUJBQWlCLENBQUM7U0FDM0I7YUFBTSxJQUFJLGNBQWMsS0FBSyxjQUFjLENBQUMsSUFBSSxFQUFFO1lBQ2pELEtBQUssR0FBRyxjQUFjLENBQUM7U0FDeEI7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFoQmUscUNBQXNCLHlCQWdCckMsQ0FBQTs7Ozs7OztJQUVELFNBQWdCLG1CQUFtQixDQUFDLGNBQThCLEVBQUUsTUFBYyxJQUFJLEVBQUUsUUFBZ0IsWUFBWTs7WUFDOUcsUUFBUSxHQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDOztZQUNoRCxnQkFBZ0IsR0FBUSxFQUFFO1FBQzlCLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFlLEVBQUUsRUFBRTs7Z0JBQy9CLFFBQVEsR0FBUSxFQUFFO1lBQ3RCLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDckUsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLE9BQU8sQ0FBQztZQUMxQixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7SUFWZSxrQ0FBbUIsc0JBVWxDLENBQUE7QUFDSCxDQUFDLEVBckNnQixjQUFjLEtBQWQsY0FBYyxRQXFDOUIiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpdCBpcyBoZXJlIGZvciBub3QgbWFraW5nIHdvcnNlIG9uIGNvZGUgZm9yIHZpZXdzIHRoYXQgcGVvcGxlIHdvcmsgb25cclxuZXhwb3J0IGVudW0gQ29tcGFyaXNvbiB7XHJcbiAgSW4gPSAwLFxyXG4gIExlc3NUaGFuT3JFcXVhbFRvID0gMSxcclxuICBMZXNzVGhhbiA9IDIsXHJcbiAgSXNOdWxsT3JXaGl0ZVNwYWNlID0gMyxcclxuICBJc051bGwgPSA0LFxyXG4gIElzTm90TnVsbE5vcldoaXRlU3BhY2UgPSA1LFxyXG4gIElzTm90TnVsbCA9IDYsXHJcbiAgSXNOb3RFbXB0eSA9IDcsXHJcbiAgSXNFbXB0eSA9IDgsXHJcbiAgU3RhcnRzV2l0aCA9IDksXHJcbiAgR3JlYXRlclRoYW5PckVxdWFsVG8gPSAxMCxcclxuICBHcmVhdGVyVGhhbiA9IDExLFxyXG4gIEVxdWFsVG8gPSAxMixcclxuICBFbmRzV2l0aCA9IDEzLFxyXG4gIERvZXNOb3RDb250YWluID0gMTQsXHJcbiAgQ29udGFpbnMgPSAxNSxcclxuICBCZXR3ZWVuID0gMTYsXHJcbiAgTm90RXF1YWxUbyA9IDE3XHJcbn1cclxuXHJcblxyXG5cclxuZXhwb3J0IGVudW0gRGVmYXVsdENvbXBhcmlzb24ge1xyXG4gIEVxdWFsVG8gPSAxMixcclxuICBOb3RFcXVhbFRvID0gMTdcclxufVxyXG5cclxuZXhwb3J0IGVudW0gVGV4dENvbXBhcmlzb24ge1xyXG4gIENvbnRhaW5zID0gMTUsXHJcbiAgRG9lc05vdENvbnRhaW4gPSAxNCxcclxuICBFbmRzV2l0aCA9IDEzLFxyXG4gIEVxdWFsVG8gPSAxMixcclxuICBJc0VtcHR5ID0gOCxcclxuICBJc05vdEVtcHR5ID0gNyxcclxuICBJc05vdE51bGwgPSA2LFxyXG4gIElzTm90TnVsbE5vcldoaXRlU3BhY2UgPSA1LFxyXG4gIElzTnVsbCA9IDQsXHJcbiAgSXNOdWxsT3JXaGl0ZVNwYWNlID0gMyxcclxuICBOb3RFcXVhbFRvID0gMTcsXHJcbiAgU3RhcnRzV2l0aCA9IDlcclxufVxyXG5cclxuZXhwb3J0IGVudW0gTnVtYmVyQ29tcGFyaXNvbiB7XHJcbiAgQmV0d2VlbiA9IDE2LFxyXG4gIEVxdWFsVG8gPSAxMixcclxuICBHcmVhdGVyVGhhbiA9IDExLFxyXG4gIEdyZWF0ZXJUaGFuT3JFcXVhbFRvID0gMTAsXHJcbiAgTGVzc1RoYW4gPSAyLFxyXG4gIExlc3NUaGFuT3JFcXVhbFRvID0gMSxcclxuICBOb3RFcXVhbFRvID0gMTdcclxufVxyXG5cclxuZXhwb3J0IGVudW0gQm9vbGVhbkNvbXBhcmlzb24ge1xyXG4gIEVxdWFsVG8gPSAxMixcclxuICBOb3RFcXVhbFRvID0gMTdcclxufVxyXG5cclxuZXhwb3J0IGVudW0gRGF0ZUNvbXBhcmlzb24ge1xyXG4gIEJldHdlZW4gPSAxNixcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgR3JlYXRlclRoYW4gPSAxMSxcclxuICBHcmVhdGVyVGhhbk9yRXF1YWxUbyA9IDEwLFxyXG4gIExlc3NUaGFuID0gMixcclxuICBMZXNzVGhhbk9yRXF1YWxUbyA9IDEsXHJcbiAgTm90RXF1YWxUbyA9IDE3XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIENvbXBhcmlzb25UeXBlIHtcclxuICBEZWZhdWx0ID0gMSxcclxuICBUZXh0ID0gMixcclxuICBOdW1iZXIgPSAzLFxyXG4gIERhdGUgPSA0LFxyXG4gIEJvb2xlYW4gPSA1XHJcbn1cclxuXHJcbmV4cG9ydCBuYW1lc3BhY2UgQ29tcGFyaXNvbkxpc3Qge1xyXG5cclxuICBleHBvcnQgZnVuY3Rpb24gdmFsdWVzKGNvbXBhcmlzb25UeXBlPzogQ29tcGFyaXNvblR5cGUpIHtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmdldEVudW1BY2NvcmRpbmdUb1R5cGUoY29tcGFyaXNvblR5cGUpKS5maWx0ZXIoXHJcbiAgICAgICh0eXBlKSA9PiBpc05hTig8YW55PnR5cGUpICYmIHR5cGUgIT09ICd2YWx1ZXMnXHJcbiAgICApLnNvcnQoKTtcclxuICB9XHJcblxyXG4gIGV4cG9ydCBmdW5jdGlvbiBnZXRFbnVtQWNjb3JkaW5nVG9UeXBlKGNvbXBhcmlzb25UeXBlPzogQ29tcGFyaXNvblR5cGUpIHtcclxuICAgIGxldCBlbnVtczogYW55O1xyXG4gICAgaWYgKCFjb21wYXJpc29uVHlwZSkge1xyXG4gICAgICBlbnVtcyA9IERlZmF1bHRDb21wYXJpc29uO1xyXG4gICAgfSBlbHNlIGlmIChjb21wYXJpc29uVHlwZSA9PT0gQ29tcGFyaXNvblR5cGUuRGVmYXVsdCkge1xyXG4gICAgICBlbnVtcyA9IERlZmF1bHRDb21wYXJpc29uO1xyXG4gICAgfSBlbHNlIGlmIChjb21wYXJpc29uVHlwZSA9PT0gQ29tcGFyaXNvblR5cGUuVGV4dCkge1xyXG4gICAgICBlbnVtcyA9IFRleHRDb21wYXJpc29uO1xyXG4gICAgfSBlbHNlIGlmIChjb21wYXJpc29uVHlwZSA9PT0gQ29tcGFyaXNvblR5cGUuTnVtYmVyKSB7XHJcbiAgICAgIGVudW1zID0gTnVtYmVyQ29tcGFyaXNvbjtcclxuICAgIH0gZWxzZSBpZiAoY29tcGFyaXNvblR5cGUgPT09IENvbXBhcmlzb25UeXBlLkJvb2xlYW4pIHtcclxuICAgICAgZW51bXMgPSBCb29sZWFuQ29tcGFyaXNvbjtcclxuICAgIH0gZWxzZSBpZiAoY29tcGFyaXNvblR5cGUgPT09IENvbXBhcmlzb25UeXBlLkRhdGUpIHtcclxuICAgICAgZW51bXMgPSBEYXRlQ29tcGFyaXNvbjtcclxuICAgIH1cclxuICAgIHJldHVybiBlbnVtcztcclxuICB9XHJcblxyXG4gIGV4cG9ydCBmdW5jdGlvbiBjcmVhdGVLZXlMYWJlbEFycmF5KGNvbXBhcmlzb25UeXBlOiBDb21wYXJpc29uVHlwZSwga2V5OiBzdHJpbmcgPSAnSWQnLCBsYWJlbDogc3RyaW5nID0gJ0RlZmluaXRpb24nKSB7XHJcbiAgICBsZXQgZW51bUtleXM6IHN0cmluZ1tdID0gdGhpcy52YWx1ZXMoY29tcGFyaXNvblR5cGUpO1xyXG4gICAgbGV0IG5ld0tleUxhYmVsQXJyYXk6IGFueSA9IFtdO1xyXG4gICAgZW51bUtleXMuZm9yRWFjaCgoZW51bUtleTogc3RyaW5nKSA9PiB7XHJcbiAgICAgIGxldCBuZXdWYWx1ZTogYW55ID0ge307XHJcbiAgICAgIG5ld1ZhbHVlW2tleV0gPSB0aGlzLmdldEVudW1BY2NvcmRpbmdUb1R5cGUoY29tcGFyaXNvblR5cGUpW2VudW1LZXldO1xyXG4gICAgICBuZXdWYWx1ZVtsYWJlbF0gPSBlbnVtS2V5O1xyXG4gICAgICBuZXdLZXlMYWJlbEFycmF5LnB1c2gobmV3VmFsdWUpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gbmV3S2V5TGFiZWxBcnJheTtcclxuICB9XHJcbn1cclxuIl19