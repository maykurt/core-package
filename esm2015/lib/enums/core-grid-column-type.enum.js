/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const CoreGridColumnType = {
    Label: 'label',
    Text: 'text',
    Date: 'date',
    Select: 'select',
    Numeric: 'numeric',
    Boolean: 'boolean',
    Lov: 'lov',
    Mask: 'mask',
    Currency: 'currency',
    NumericLabel: 'numeric-label',
    TextSelect: 'text-select',
};
export { CoreGridColumnType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLWNvbHVtbi10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvY29yZS1ncmlkLWNvbHVtbi10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsT0FBUSxPQUFPO0lBQ2YsTUFBTyxNQUFNO0lBQ2IsTUFBTyxNQUFNO0lBQ2IsUUFBUyxRQUFRO0lBQ2pCLFNBQVUsU0FBUztJQUNuQixTQUFVLFNBQVM7SUFDbkIsS0FBTSxLQUFLO0lBQ1gsTUFBTyxNQUFNO0lBQ2IsVUFBVyxVQUFVO0lBQ3JCLGNBQWUsZUFBZTtJQUM5QixZQUFhLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBDb3JlR3JpZENvbHVtblR5cGUge1xyXG4gIExhYmVsID0gJ2xhYmVsJyxcclxuICBUZXh0ID0gJ3RleHQnLFxyXG4gIERhdGUgPSAnZGF0ZScsXHJcbiAgU2VsZWN0ID0gJ3NlbGVjdCcsXHJcbiAgTnVtZXJpYyA9ICdudW1lcmljJyxcclxuICBCb29sZWFuID0gJ2Jvb2xlYW4nLFxyXG4gIExvdiA9ICdsb3YnLFxyXG4gIE1hc2sgPSAnbWFzaycsXHJcbiAgQ3VycmVuY3kgPSAnY3VycmVuY3knLFxyXG4gIE51bWVyaWNMYWJlbCA9ICdudW1lcmljLWxhYmVsJyxcclxuICBUZXh0U2VsZWN0ID0gJ3RleHQtc2VsZWN0J1xyXG59XHJcbiJdfQ==