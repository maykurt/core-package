/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const Headers = {
    CoreId: 'X-CoreId',
    Behaviour: 'X-Behaviour',
    ReferenceDate: 'ReferenceDate',
    PagingHeader: 'X-Paging-Header',
    PagingResponseHeader: 'X-Paging-Response-Header',
    ReferenceDateHeader: 'X-ReferenceDate-Header',
    DisableLoading: 'disableGeneralLoading',
    DisableNavigation: 'disableNavigation',
    FileOperation: 'fileOperation',
    ContentType: 'Content-Type',
    Authentication: 'Authentication',
    BreadcrumbCustomLabel: 'BreadcrumbCustomLabel',
};
export { Headers };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVycy5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2VudW1zL2hlYWRlcnMuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxRQUFTLFVBQVU7SUFDbkIsV0FBWSxhQUFhO0lBQ3pCLGVBQWdCLGVBQWU7SUFDL0IsY0FBZSxpQkFBaUI7SUFDaEMsc0JBQXVCLDBCQUEwQjtJQUNqRCxxQkFBc0Isd0JBQXdCO0lBQzlDLGdCQUFpQix1QkFBdUI7SUFDeEMsbUJBQW9CLG1CQUFtQjtJQUN2QyxlQUFnQixlQUFlO0lBQy9CLGFBQWMsY0FBYztJQUM1QixnQkFBaUIsZ0JBQWdCO0lBQ2pDLHVCQUF3Qix1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBIZWFkZXJzIHtcclxuICBDb3JlSWQgPSAnWC1Db3JlSWQnLFxyXG4gIEJlaGF2aW91ciA9ICdYLUJlaGF2aW91cicsXHJcbiAgUmVmZXJlbmNlRGF0ZSA9ICdSZWZlcmVuY2VEYXRlJyxcclxuICBQYWdpbmdIZWFkZXIgPSAnWC1QYWdpbmctSGVhZGVyJyxcclxuICBQYWdpbmdSZXNwb25zZUhlYWRlciA9ICdYLVBhZ2luZy1SZXNwb25zZS1IZWFkZXInLFxyXG4gIFJlZmVyZW5jZURhdGVIZWFkZXIgPSAnWC1SZWZlcmVuY2VEYXRlLUhlYWRlcicsXHJcbiAgRGlzYWJsZUxvYWRpbmcgPSAnZGlzYWJsZUdlbmVyYWxMb2FkaW5nJyxcclxuICBEaXNhYmxlTmF2aWdhdGlvbiA9ICdkaXNhYmxlTmF2aWdhdGlvbicsXHJcbiAgRmlsZU9wZXJhdGlvbiA9ICdmaWxlT3BlcmF0aW9uJyxcclxuICBDb250ZW50VHlwZSA9ICdDb250ZW50LVR5cGUnLFxyXG4gIEF1dGhlbnRpY2F0aW9uID0gJ0F1dGhlbnRpY2F0aW9uJyxcclxuICBCcmVhZGNydW1iQ3VzdG9tTGFiZWwgPSAnQnJlYWRjcnVtYkN1c3RvbUxhYmVsJ1xyXG59XHJcbiJdfQ==