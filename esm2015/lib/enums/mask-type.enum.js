/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const MaskType = {
    Phone: 'phone',
    Mail: 'mail',
    PostCode: 'postCode',
};
export { MaskType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvbWFzay10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsT0FBUSxPQUFPO0lBQ2YsTUFBTyxNQUFNO0lBQ2IsVUFBVyxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gTWFza1R5cGUge1xyXG4gIFBob25lID0gJ3Bob25lJyxcclxuICBNYWlsID0gJ21haWwnLFxyXG4gIFBvc3RDb2RlID0gJ3Bvc3RDb2RlJ1xyXG59XHJcbiJdfQ==