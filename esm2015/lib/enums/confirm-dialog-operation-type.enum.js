/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ConfirmDialogOperationType = {
    Add: 'add',
    Update: 'update',
    Delete: 'delete',
    Undo: 'undo',
    Cancel: 'cancel',
    Refresh: 'refresh',
    Process: 'process',
};
export { ConfirmDialogOperationType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2ctb3BlcmF0aW9uLXR5cGUuZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lbnVtcy9jb25maXJtLWRpYWxvZy1vcGVyYXRpb24tdHlwZS5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUNFLEtBQU0sS0FBSztJQUNYLFFBQVMsUUFBUTtJQUNqQixRQUFTLFFBQVE7SUFDakIsTUFBTyxNQUFNO0lBQ2IsUUFBUyxRQUFRO0lBQ2pCLFNBQVUsU0FBUztJQUNuQixTQUFVLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZSB7XHJcbiAgQWRkID0gJ2FkZCcsXHJcbiAgVXBkYXRlID0gJ3VwZGF0ZScsXHJcbiAgRGVsZXRlID0gJ2RlbGV0ZScsXHJcbiAgVW5kbyA9ICd1bmRvJyxcclxuICBDYW5jZWwgPSAnY2FuY2VsJyxcclxuICBSZWZyZXNoID0gJ3JlZnJlc2gnLFxyXG4gIFByb2Nlc3MgPSAncHJvY2VzcydcclxufVxyXG4iXX0=