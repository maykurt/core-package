/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { SystemReferenceDateComponent } from '../components/system-date-picker/system-reference-date.component';
// import { SystemReferenceDateService } from './shared/services/system-reference-date.service';
export class SystemReferanceDateLibModule {
}
SystemReferanceDateLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    SystemReferenceDateComponent
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    CalendarModule,
                    BsDatepickerModule.forRoot()
                ],
                exports: [
                    SystemReferenceDateComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXJlZmVyYW5jZS1kYXRlLWxpYi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvbW9kdWxlcy9zeXN0ZW0tcmVmZXJhbmNlLWRhdGUtbGliLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sa0VBQWtFLENBQUM7O0FBa0JoSCxNQUFNLE9BQU8sNEJBQTRCOzs7WUFkeEMsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWiw0QkFBNEI7aUJBQzdCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLFdBQVc7b0JBQ1gsY0FBYztvQkFDZCxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7aUJBQzdCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCw0QkFBNEI7aUJBQzdCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBCc0RhdGVwaWNrZXJNb2R1bGUgfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuaW1wb3J0IHsgQ2FsZW5kYXJNb2R1bGUgfSBmcm9tICdwcmltZW5nL2NhbGVuZGFyJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFN5c3RlbVJlZmVyZW5jZURhdGVDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL3N5c3RlbS1kYXRlLXBpY2tlci9zeXN0ZW0tcmVmZXJlbmNlLWRhdGUuY29tcG9uZW50JztcclxuLy8gaW1wb3J0IHsgU3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UgfSBmcm9tICcuL3NoYXJlZC9zZXJ2aWNlcy9zeXN0ZW0tcmVmZXJlbmNlLWRhdGUuc2VydmljZSc7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIFN5c3RlbVJlZmVyZW5jZURhdGVDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgQ2FsZW5kYXJNb2R1bGUsXHJcbiAgICBCc0RhdGVwaWNrZXJNb2R1bGUuZm9yUm9vdCgpXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBTeXN0ZW1SZWZlcmVuY2VEYXRlQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3lzdGVtUmVmZXJhbmNlRGF0ZUxpYk1vZHVsZSB7XHJcbn1cclxuIl19