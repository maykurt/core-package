/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ColumnSortedEvent() { }
if (false) {
    /** @type {?} */
    ColumnSortedEvent.prototype.SortColumn;
    /** @type {?} */
    ColumnSortedEvent.prototype.SortDirection;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLXNvcnRlZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvY29sdW1uLXNvcnRlZC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUEsdUNBR0M7OztJQUZDLHVDQUFtQjs7SUFDbkIsMENBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU29ydFR5cGUgfSBmcm9tICcuLi9lbnVtcy9zb3J0LXR5cGUuZW51bSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIENvbHVtblNvcnRlZEV2ZW50IHtcclxuICBTb3J0Q29sdW1uOiBzdHJpbmc7XHJcbiAgU29ydERpcmVjdGlvbjogU29ydFR5cGU7XHJcbn1cclxuIl19