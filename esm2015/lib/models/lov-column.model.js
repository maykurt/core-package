/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
export class LovColumn {
    /**
     * visible parameter default is true
     * @param {?} name
     * @param {?} headerName
     * @param {?} type
     * @param {?=} visible
     * @param {?=} isKeyField
     */
    constructor(name, headerName, type, visible, isKeyField) {
        this.Name = name;
        this.HeaderName = headerName;
        this.Type = type;
        this.Visible = _.isNil(visible) ? true : visible;
        this.IsKeyField = _.isNil(isKeyField) ? false : isKeyField;
    }
}
if (false) {
    /** @type {?} */
    LovColumn.prototype.Name;
    /** @type {?} */
    LovColumn.prototype.HeaderName;
    /** @type {?} */
    LovColumn.prototype.Type;
    /** @type {?} */
    LovColumn.prototype.Visible;
    /** @type {?} */
    LovColumn.prototype.IsKeyField;
    /** @type {?} */
    LovColumn.prototype.SearchText;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWNvbHVtbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbG92LWNvbHVtbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFHNUIsTUFBTSxPQUFPLFNBQVM7Ozs7Ozs7OztJQUdwQixZQUFZLElBQVksRUFBRSxVQUFrQixFQUFFLElBQW1CLEVBQUUsT0FBaUIsRUFBRSxVQUFvQjtRQUN4RyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7SUFDN0QsQ0FBQztDQVFGOzs7SUFOQyx5QkFBb0I7O0lBQ3BCLCtCQUEwQjs7SUFDMUIseUJBQTJCOztJQUMzQiw0QkFBd0I7O0lBQ3hCLCtCQUEyQjs7SUFDM0IsK0JBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTG92Q29sdW1uVHlwZSB9IGZyb20gJy4uL2VudW1zL2xvdi1jb2x1bW4tdHlwZS5lbnVtJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBMb3ZDb2x1bW4ge1xyXG5cclxuICAvKiogdmlzaWJsZSBwYXJhbWV0ZXIgZGVmYXVsdCBpcyB0cnVlICovXHJcbiAgY29uc3RydWN0b3IobmFtZTogc3RyaW5nLCBoZWFkZXJOYW1lOiBzdHJpbmcsIHR5cGU6IExvdkNvbHVtblR5cGUsIHZpc2libGU/OiBib29sZWFuLCBpc0tleUZpZWxkPzogYm9vbGVhbikge1xyXG4gICAgdGhpcy5OYW1lID0gbmFtZTtcclxuICAgIHRoaXMuSGVhZGVyTmFtZSA9IGhlYWRlck5hbWU7XHJcbiAgICB0aGlzLlR5cGUgPSB0eXBlO1xyXG4gICAgdGhpcy5WaXNpYmxlID0gXy5pc05pbCh2aXNpYmxlKSA/IHRydWUgOiB2aXNpYmxlO1xyXG4gICAgdGhpcy5Jc0tleUZpZWxkID0gXy5pc05pbChpc0tleUZpZWxkKSA/IGZhbHNlIDogaXNLZXlGaWVsZDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBOYW1lOiBzdHJpbmc7XHJcbiAgcHVibGljIEhlYWRlck5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgVHlwZTogTG92Q29sdW1uVHlwZTtcclxuICBwdWJsaWMgVmlzaWJsZTogYm9vbGVhbjtcclxuICBwdWJsaWMgSXNLZXlGaWVsZDogYm9vbGVhbjtcclxuICBwdWJsaWMgU2VhcmNoVGV4dDogc3RyaW5nO1xyXG59XHJcbiJdfQ==