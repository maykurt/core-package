/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IAppConfig() { }
if (false) {
    /** @type {?} */
    IAppConfig.prototype.env;
    /** @type {?|undefined} */
    IAppConfig.prototype.appInsights;
    /** @type {?|undefined} */
    IAppConfig.prototype.logging;
    /** @type {?|undefined} */
    IAppConfig.prototype.aad;
    /** @type {?|undefined} */
    IAppConfig.prototype.apiServer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpZy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYXBwLWNvbmZpZy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsZ0NBeUJDOzs7SUF4QkMseUJBTUU7O0lBQ0YsaUNBRUU7O0lBQ0YsNkJBR0U7O0lBQ0YseUJBS0U7O0lBQ0YsK0JBR0UiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElBcHBDb25maWcge1xyXG4gIGVudjoge1xyXG4gICAgYXBpVXJsOiBzdHJpbmc7XHJcbiAgICB2ZXJzaW9uOiBzdHJpbmc7XHJcbiAgICBjb21wYW55TG9nb1VybDogc3RyaW5nO1xyXG4gICAgYXBwVGl0bGU6IHN0cmluZztcclxuICAgIGVycFVybDogc3RyaW5nO1xyXG4gIH07XHJcbiAgYXBwSW5zaWdodHM/OiB7XHJcbiAgICBpbnN0cnVtZW50YXRpb25LZXk6IHN0cmluZztcclxuICB9O1xyXG4gIGxvZ2dpbmc/OiB7XHJcbiAgICBjb25zb2xlOiBib29sZWFuO1xyXG4gICAgYXBwSW5zaWdodHM6IGJvb2xlYW47XHJcbiAgfTtcclxuICBhYWQ/OiB7XHJcbiAgICByZXF1aXJlQXV0aDogYm9vbGVhbjtcclxuICAgIHRlbmFudDogc3RyaW5nO1xyXG4gICAgY2xpZW50SWQ6IHN0cmluZztcclxuXHJcbiAgfTtcclxuICBhcGlTZXJ2ZXI/OiB7XHJcbiAgICBtZXRhZGF0YTogc3RyaW5nO1xyXG4gICAgcnVsZXM6IHN0cmluZztcclxuICB9O1xyXG59XHJcbiJdfQ==