/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GenericExpression() { }
if (false) {
    /** @type {?|undefined} */
    GenericExpression.prototype.PageSize;
    /** @type {?|undefined} */
    GenericExpression.prototype.PageNumber;
    /** @type {?|undefined} */
    GenericExpression.prototype.FilterGroups;
    /** @type {?|undefined} */
    GenericExpression.prototype.Sort;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VuZXJpYy1leHByZXNzaW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9nZW5lcmljLWV4cHJlc3Npb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUdBLHVDQUtDOzs7SUFKRyxxQ0FBa0I7O0lBQ2xCLHVDQUFvQjs7SUFDcEIseUNBQTZCOztJQUM3QixpQ0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpbHRlckdyb3VwIH0gZnJvbSAnLi9maWx0ZXItZ3JvdXAubW9kZWwnO1xyXG5pbXBvcnQgeyBTb3J0IH0gZnJvbSAnLi9zb3J0Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgR2VuZXJpY0V4cHJlc3Npb24ge1xyXG4gICAgUGFnZVNpemU/OiBudW1iZXI7XHJcbiAgICBQYWdlTnVtYmVyPzogbnVtYmVyO1xyXG4gICAgRmlsdGVyR3JvdXBzPzogRmlsdGVyR3JvdXBbXTtcclxuICAgIFNvcnQ/OiBTb3J0W107XHJcbn1cclxuIl19