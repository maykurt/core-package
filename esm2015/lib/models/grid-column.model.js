/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GridColumn() { }
if (false) {
    /** @type {?} */
    GridColumn.prototype.field;
    /** @type {?|undefined} */
    GridColumn.prototype.headerName;
    /** @type {?} */
    GridColumn.prototype.columnType;
    /** @type {?|undefined} */
    GridColumn.prototype.visible;
    /** @type {?|undefined} */
    GridColumn.prototype.dataSource;
    /** @type {?|undefined} */
    GridColumn.prototype.validation;
    /** @type {?|undefined} */
    GridColumn.prototype.settings;
    /** @type {?|undefined} */
    GridColumn.prototype.hideFilter;
    /** @type {?|undefined} */
    GridColumn.prototype.defaultValue;
    /** @type {?|undefined} */
    GridColumn.prototype.editable;
    /** @type {?|undefined} */
    GridColumn.prototype.customFieldForFilter;
    /** @type {?|undefined} */
    GridColumn.prototype.customFieldForSort;
    /** @type {?|undefined} */
    GridColumn.prototype.customEditableFunction;
    /** @type {?|undefined} */
    GridColumn.prototype.customDisplayFunction;
    /** @type {?|undefined} */
    GridColumn.prototype.customFilterFunction;
    /** @type {?|undefined} */
    GridColumn.prototype.customFunctionForRowValueChanges;
    /** @type {?|undefined} */
    GridColumn.prototype.minWidth;
    /** @type {?|undefined} */
    GridColumn.prototype.maxWidth;
    /** @type {?|undefined} */
    GridColumn.prototype.getDataSourceAfterDataLoaded;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1jb2x1bW4ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2dyaWQtY29sdW1uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFRQSxnQ0FvQkM7OztJQW5CQywyQkFBYzs7SUFDZCxnQ0FBb0I7O0lBQ3BCLGdDQUErQjs7SUFDL0IsNkJBQWtCOztJQUNsQixnQ0FBaUI7O0lBQ2pCLGdDQUE0Qjs7SUFDNUIsOEJBQXVGOztJQUN2RixnQ0FBcUI7O0lBQ3JCLGtDQUFtQjs7SUFDbkIsOEJBQW1COztJQUNuQiwwQ0FBOEI7O0lBQzlCLHdDQUE0Qjs7SUFDNUIsNENBQW9EOztJQUNwRCwyQ0FBbUQ7O0lBQ25ELDBDQUFpRDs7SUFDakQsc0RBQTBEOztJQUMxRCw4QkFBa0I7O0lBQ2xCLDhCQUFrQjs7SUFDbEIsa0RBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29yZUdyaWRDb2x1bW5UeXBlIH0gZnJvbSAnLi4vZW51bXMvY29yZS1ncmlkLWNvbHVtbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBHcmlkVmFsaWRhdGlvbiB9IGZyb20gJy4vZ3JpZC12YWxpZGF0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgTG92U2V0dGluZ3MgfSBmcm9tICcuL2xvdi1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IFNlbGVjdG9yU2V0dGluZ3MgfSBmcm9tICcuL3NlbGVjdG9yLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0ZVNldHRpbmdzIH0gZnJvbSAnLi9kYXRlLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgTWFza1NldHRpbmdzIH0gZnJvbSAnLi9tYXNrLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgVGV4dFNldHRpbmdzIH0gZnJvbSAnLi90ZXh0LXNldHRpbmdzLm1vZGVsJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgR3JpZENvbHVtbiB7XHJcbiAgZmllbGQ6IHN0cmluZztcclxuICBoZWFkZXJOYW1lPzogc3RyaW5nO1xyXG4gIGNvbHVtblR5cGU6IENvcmVHcmlkQ29sdW1uVHlwZTtcclxuICB2aXNpYmxlPzogYm9vbGVhbjtcclxuICBkYXRhU291cmNlPzogYW55O1xyXG4gIHZhbGlkYXRpb24/OiBHcmlkVmFsaWRhdGlvbjtcclxuICBzZXR0aW5ncz86IExvdlNldHRpbmdzIHwgU2VsZWN0b3JTZXR0aW5ncyB8IERhdGVTZXR0aW5ncyB8IE1hc2tTZXR0aW5ncyB8IFRleHRTZXR0aW5ncztcclxuICBoaWRlRmlsdGVyPzogYm9vbGVhbjtcclxuICBkZWZhdWx0VmFsdWU/OiBhbnk7XHJcbiAgZWRpdGFibGU/OiBib29sZWFuO1xyXG4gIGN1c3RvbUZpZWxkRm9yRmlsdGVyPzogc3RyaW5nO1xyXG4gIGN1c3RvbUZpZWxkRm9yU29ydD86IHN0cmluZztcclxuICBjdXN0b21FZGl0YWJsZUZ1bmN0aW9uPzogKHJvd1ZhbHVlOiBhbnkpID0+IGJvb2xlYW47XHJcbiAgY3VzdG9tRGlzcGxheUZ1bmN0aW9uPzogKGRpc3BsYXlWYWx1ZTogYW55KSA9PiBhbnk7XHJcbiAgY3VzdG9tRmlsdGVyRnVuY3Rpb24/OiAoZmlsdGVyVmFsdWU6IGFueSkgPT4gYW55O1xyXG4gIGN1c3RvbUZ1bmN0aW9uRm9yUm93VmFsdWVDaGFuZ2VzPzogKGNvbnRleHQ6IGFueSkgPT4gdm9pZDtcclxuICBtaW5XaWR0aD86IG51bWJlcjtcclxuICBtYXhXaWR0aD86IG51bWJlcjtcclxuICBnZXREYXRhU291cmNlQWZ0ZXJEYXRhTG9hZGVkPzogYm9vbGVhbjtcclxufVxyXG4iXX0=