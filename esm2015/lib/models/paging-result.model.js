/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function PagingResult() { }
if (false) {
    /** @type {?|undefined} */
    PagingResult.prototype.TotalCount;
    /** @type {?|undefined} */
    PagingResult.prototype.CurrentPage;
    /** @type {?|undefined} */
    PagingResult.prototype.PageSize;
    /** @type {?|undefined} */
    PagingResult.prototype.TotalPages;
    /** @type {?|undefined} */
    PagingResult.prototype.PreviousePage;
    /** @type {?|undefined} */
    PagingResult.prototype.NextPage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5nLXJlc3VsdC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvcGFnaW5nLXJlc3VsdC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsa0NBT0M7OztJQU5HLGtDQUFvQjs7SUFDcEIsbUNBQXFCOztJQUNyQixnQ0FBa0I7O0lBQ2xCLGtDQUFvQjs7SUFDcEIscUNBQXdCOztJQUN4QixnQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIFBhZ2luZ1Jlc3VsdCB7XHJcbiAgICBUb3RhbENvdW50PzogbnVtYmVyO1xyXG4gICAgQ3VycmVudFBhZ2U/OiBudW1iZXI7XHJcbiAgICBQYWdlU2l6ZT86IG51bWJlcjtcclxuICAgIFRvdGFsUGFnZXM/OiBudW1iZXI7XHJcbiAgICBQcmV2aW91c2VQYWdlPzogYm9vbGVhbjtcclxuICAgIE5leHRQYWdlPzogYm9vbGVhbjtcclxufVxyXG4iXX0=