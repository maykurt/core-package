/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template T
 */
export function CoreHttpResponse() { }
if (false) {
    /** @type {?|undefined} */
    CoreHttpResponse.prototype.pagingResult;
    /** @type {?|undefined} */
    CoreHttpResponse.prototype.serviceResult;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1odHRwLXJlc3BvbnNlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9odHRwL2NvcmUtaHR0cC1yZXNwb25zZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLHNDQUdDOzs7SUFGRyx3Q0FBNEI7O0lBQzVCLHlDQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBhZ2luZ1Jlc3VsdCB9IGZyb20gJy4uL3BhZ2luZy1yZXN1bHQubW9kZWwnO1xyXG5pbXBvcnQgeyBTZXJ2aWNlUmVzdWx0IH0gZnJvbSAnLi9zZXJ2aWNlLXJlc3VsdC5tb2RlbCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIENvcmVIdHRwUmVzcG9uc2U8VD4ge1xyXG4gICAgcGFnaW5nUmVzdWx0PzogUGFnaW5nUmVzdWx0O1xyXG4gICAgc2VydmljZVJlc3VsdD86IFNlcnZpY2VSZXN1bHQ8VD47XHJcbn1cclxuIl19