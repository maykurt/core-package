/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class GetListByResult {
}
if (false) {
    /** @type {?} */
    GetListByResult.prototype.CurrentPage;
    /** @type {?} */
    GetListByResult.prototype.NextPage;
    /** @type {?} */
    GetListByResult.prototype.PageSize;
    /** @type {?} */
    GetListByResult.prototype.PreviousPage;
    /** @type {?} */
    GetListByResult.prototype.TotalCount;
    /** @type {?} */
    GetListByResult.prototype.TotalPages;
    /** @type {?} */
    GetListByResult.prototype.Data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0bGlzdGJ5LXNlcnZpY2UtcmVzdWx0Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9odHRwL2dldGxpc3RieS1zZXJ2aWNlLXJlc3VsdC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsTUFBTSxPQUFPLGVBQWU7Q0FRM0I7OztJQVBHLHNDQUFvQjs7SUFDcEIsbUNBQWtCOztJQUNsQixtQ0FBaUI7O0lBQ2pCLHVDQUFzQjs7SUFDdEIscUNBQW1COztJQUNuQixxQ0FBbUI7O0lBQ25CLCtCQUFVIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEdldExpc3RCeVJlc3VsdCB7XHJcbiAgICBDdXJyZW50UGFnZTogbnVtYmVyO1xyXG4gICAgTmV4dFBhZ2U6IGJvb2xlYW47XHJcbiAgICBQYWdlU2l6ZTogbnVtYmVyO1xyXG4gICAgUHJldmlvdXNQYWdlOiBib29sZWFuO1xyXG4gICAgVG90YWxDb3VudDogbnVtYmVyO1xyXG4gICAgVG90YWxQYWdlczogbnVtYmVyO1xyXG4gICAgRGF0YTogYW55O1xyXG59XHJcbiJdfQ==