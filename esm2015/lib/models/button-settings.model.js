/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ButtonSettings() { }
if (false) {
    /** @type {?|undefined} */
    ButtonSettings.prototype.showAddButton;
    /** @type {?|undefined} */
    ButtonSettings.prototype.showEditButton;
    /** @type {?|undefined} */
    ButtonSettings.prototype.showReloadButton;
    /** @type {?|undefined} */
    ButtonSettings.prototype.showRowDeleteButton;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLXNldHRpbmdzLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9idXR0b24tc2V0dGluZ3MubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBLG9DQUtDOzs7SUFKQyx1Q0FBd0I7O0lBQ3hCLHdDQUF5Qjs7SUFDekIsMENBQTJCOztJQUMzQiw2Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEJ1dHRvblNldHRpbmdzIHtcclxuICBzaG93QWRkQnV0dG9uPzogYm9vbGVhbjtcclxuICBzaG93RWRpdEJ1dHRvbj86IGJvb2xlYW47XHJcbiAgc2hvd1JlbG9hZEJ1dHRvbj86IGJvb2xlYW47XHJcbiAgc2hvd1Jvd0RlbGV0ZUJ1dHRvbj86IGJvb2xlYW47XHJcbn1cclxuIl19