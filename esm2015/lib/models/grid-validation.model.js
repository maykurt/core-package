/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GridValidation() { }
if (false) {
    /** @type {?|undefined} */
    GridValidation.prototype.required;
    /** @type {?|undefined} */
    GridValidation.prototype.email;
    /** @type {?|undefined} */
    GridValidation.prototype.requiredTrue;
    /** @type {?|undefined} */
    GridValidation.prototype.minValue;
    /** @type {?|undefined} */
    GridValidation.prototype.maxValue;
    /** @type {?|undefined} */
    GridValidation.prototype.minLength;
    /** @type {?|undefined} */
    GridValidation.prototype.maxLength;
    /** @type {?|undefined} */
    GridValidation.prototype.regex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC12YWxpZGF0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9ncmlkLXZhbGlkYXRpb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBLG9DQVdDOzs7SUFWQyxrQ0FBbUI7O0lBQ25CLCtCQUFnQjs7SUFDaEIsc0NBQXVCOztJQUN2QixrQ0FBa0I7O0lBQ2xCLGtDQUFrQjs7SUFDbEIsbUNBQW1COztJQUNuQixtQ0FBbUI7O0lBQ25CLCtCQUFlIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBHcmlkVmFsaWRhdGlvbiB7XHJcbiAgcmVxdWlyZWQ/OiBib29sZWFuOyAvLyBab3J1bmx1IEFsYW5cclxuICBlbWFpbD86IGJvb2xlYW47IC8vIEUtUG9zdGFcclxuICByZXF1aXJlZFRydWU/OiBib29sZWFuOyAvLyBDaGVja2JveCfEsW4gdHJ1ZSBvbG1hIHpvcnVubHVsdcSfdVxyXG4gIG1pblZhbHVlPzogbnVtYmVyOyAvLyBNaW4gVmFsdWVcclxuICBtYXhWYWx1ZT86IG51bWJlcjsgLy8gTWF4IFZhbHVlXHJcbiAgbWluTGVuZ3RoPzogbnVtYmVyOyAvLyBNaW4gVXp1bmx1a1xyXG4gIG1heExlbmd0aD86IG51bWJlcjsgLy8gTWF4IFV6dW5sdWtcclxuICByZWdleD86IHN0cmluZztcclxuICAvLyBSZWd1bGFyRXhwcmVzc2lvbiBFLVBvc3RhIMOWcm5lazogXlthLXpBLVowLTkuISMkJSZcXCcqKy89P15fYHt8fX4tXStAW2EtekEtWjAtOV0oPzpbYS16QS1aMC05LV1cclxuICAvLyB7MCw2MX1bYS16QS1aMC05XSk/KD86XFwuW2EtekEtWjAtOV0oPzpbYS16QS1aMC05LV17MCw2MX1bYS16QS1aMC05XSk/KSokXHJcbn1cclxuIl19