/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { DateSelectionMode } from '../enums/date-selection-mode.enum';
export class DateSettings {
    constructor() {
        this.disableUTC = false;
        this.showTimeOnly = false;
        // current year
        this.defaultYear = (new Date()).getFullYear();
        // 0 = january
        this.defaultMonth = 0;
        // 1
        this.defaultDay = 1;
        //'dd.mm.yy'
        this.dateFormat = 'dd.mm.yy';
        this.showYear = false;
        this.showTime = false;
        this.hideCalendarButton = false;
        //  today and clear buttons
        this.showButtonBar = true;
        this.showMonthPicker = false;
        this.enableSeconds = false;
        this.selectionMode = DateSelectionMode.Single;
    }
}
if (false) {
    /** @type {?} */
    DateSettings.prototype.disableUTC;
    /** @type {?} */
    DateSettings.prototype.showTimeOnly;
    /** @type {?} */
    DateSettings.prototype.defaultYear;
    /** @type {?} */
    DateSettings.prototype.defaultMonth;
    /** @type {?} */
    DateSettings.prototype.defaultDay;
    /** @type {?} */
    DateSettings.prototype.dateFormat;
    /** @type {?} */
    DateSettings.prototype.showYear;
    /** @type {?} */
    DateSettings.prototype.showTime;
    /** @type {?} */
    DateSettings.prototype.hideCalendarButton;
    /** @type {?} */
    DateSettings.prototype.showButtonBar;
    /** @type {?} */
    DateSettings.prototype.showMonthPicker;
    /** @type {?} */
    DateSettings.prototype.minDate;
    /** @type {?} */
    DateSettings.prototype.maxDate;
    /** @type {?} */
    DateSettings.prototype.enableSeconds;
    /** @type {?} */
    DateSettings.prototype.selectionMode;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1zZXR0aW5ncy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZGF0ZS1zZXR0aW5ncy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFFdEUsTUFBTSxPQUFPLFlBQVk7SUFBekI7UUFDRSxlQUFVLEdBQWEsS0FBSyxDQUFDO1FBQzdCLGlCQUFZLEdBQWEsS0FBSyxDQUFDOztRQUUvQixnQkFBVyxHQUFZLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDOztRQUVsRCxpQkFBWSxHQUFZLENBQUMsQ0FBQzs7UUFFMUIsZUFBVSxHQUFZLENBQUMsQ0FBQzs7UUFFeEIsZUFBVSxHQUFZLFVBQVUsQ0FBQztRQUNqQyxhQUFRLEdBQWEsS0FBSyxDQUFDO1FBQzNCLGFBQVEsR0FBYSxLQUFLLENBQUM7UUFDM0IsdUJBQWtCLEdBQWEsS0FBSyxDQUFDOztRQUVyQyxrQkFBYSxHQUFhLElBQUksQ0FBQztRQUMvQixvQkFBZSxHQUFhLEtBQUssQ0FBQztRQUdsQyxrQkFBYSxHQUFhLEtBQUssQ0FBQztRQUNoQyxrQkFBYSxHQUF1QixpQkFBaUIsQ0FBQyxNQUFNLENBQUM7SUFDL0QsQ0FBQztDQUFBOzs7SUFwQkMsa0NBQTZCOztJQUM3QixvQ0FBK0I7O0lBRS9CLG1DQUFrRDs7SUFFbEQsb0NBQTBCOztJQUUxQixrQ0FBd0I7O0lBRXhCLGtDQUFpQzs7SUFDakMsZ0NBQTJCOztJQUMzQixnQ0FBMkI7O0lBQzNCLDBDQUFxQzs7SUFFckMscUNBQStCOztJQUMvQix1Q0FBa0M7O0lBQ2xDLCtCQUFpQjs7SUFDakIsK0JBQWlCOztJQUNqQixxQ0FBZ0M7O0lBQ2hDLHFDQUE2RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERhdGVTZWxlY3Rpb25Nb2RlIH0gZnJvbSAnLi4vZW51bXMvZGF0ZS1zZWxlY3Rpb24tbW9kZS5lbnVtJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlU2V0dGluZ3Mge1xyXG4gIGRpc2FibGVVVEM/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2hvd1RpbWVPbmx5PzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIC8vIGN1cnJlbnQgeWVhclxyXG4gIGRlZmF1bHRZZWFyPzogbnVtYmVyID0gKG5ldyBEYXRlKCkpLmdldEZ1bGxZZWFyKCk7XHJcbiAgLy8gMCA9IGphbnVhcnlcclxuICBkZWZhdWx0TW9udGg/OiBudW1iZXIgPSAwO1xyXG4gIC8vIDFcclxuICBkZWZhdWx0RGF5PzogbnVtYmVyID0gMTtcclxuICAvLydkZC5tbS55eSdcclxuICBkYXRlRm9ybWF0Pzogc3RyaW5nID0gJ2RkLm1tLnl5JztcclxuICBzaG93WWVhcj86IGJvb2xlYW4gPSBmYWxzZTtcclxuICBzaG93VGltZT86IGJvb2xlYW4gPSBmYWxzZTtcclxuICBoaWRlQ2FsZW5kYXJCdXR0b24/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgLy8gIHRvZGF5IGFuZCBjbGVhciBidXR0b25zXHJcbiAgc2hvd0J1dHRvbkJhcj86IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHNob3dNb250aFBpY2tlcj86IGJvb2xlYW4gPSBmYWxzZTtcclxuICBtaW5EYXRlPzogc3RyaW5nO1xyXG4gIG1heERhdGU/OiBzdHJpbmc7XHJcbiAgZW5hYmxlU2Vjb25kcz86IGJvb2xlYW4gPSBmYWxzZTtcclxuICBzZWxlY3Rpb25Nb2RlPzogRGF0ZVNlbGVjdGlvbk1vZGUgPSBEYXRlU2VsZWN0aW9uTW9kZS5TaW5nbGU7XHJcbn1cclxuIl19