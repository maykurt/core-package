/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { map, concatMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { SweetAlertService } from '../custom/sweet.alert.confirm.service';
import { PreventUnsavedChangesService } from '../custom/prevent-unsaved-changes.service';
import * as i0 from "@angular/core";
import * as i1 from "../custom/sweet.alert.confirm.service";
import * as i2 from "../custom/prevent-unsaved-changes.service";
export class PreventUnsavedChangesGuard {
    /**
     * @param {?} sweetAlert
     * @param {?} preventUnsavedChangesService
     */
    constructor(sweetAlert, preventUnsavedChangesService) {
        this.sweetAlert = sweetAlert;
        this.preventUnsavedChangesService = preventUnsavedChangesService;
    }
    /**
     * @return {?}
     */
    canDeactivate() {
        try {
            setTimeout(() => {
                this.preventUnsavedChangesService.setStatus('waiting');
            }, 100);
            return this.preventUnsavedChangesService.getStatus()
                .pipe(concatMap((result) => {
                if (result === 'finished') {
                    /** @type {?} */
                    const isChanged = this.preventUnsavedChangesService.getIsChanged();
                    if (isChanged) {
                        return this.sweetAlert.preventUnsavedChangedPopup()
                            .pipe(map((alertResult) => {
                            // console.log('canDeactivate in pipe', alertResult);
                            if (alertResult.value) {
                                this.preventUnsavedChangesService.setIsChanged(false);
                                return true;
                            }
                            else {
                                return false;
                            }
                        }));
                    }
                    else {
                        this.preventUnsavedChangesService.setIsChanged(false);
                        return of(true);
                    }
                }
                // return result;
            }), catchError((error) => {
                console.error(error);
                return of(true);
            }));
        }
        catch (err) {
            console.error(err);
            return true;
        }
    }
}
PreventUnsavedChangesGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PreventUnsavedChangesGuard.ctorParameters = () => [
    { type: SweetAlertService },
    { type: PreventUnsavedChangesService }
];
/** @nocollapse */ PreventUnsavedChangesGuard.ngInjectableDef = i0.defineInjectable({ factory: function PreventUnsavedChangesGuard_Factory() { return new PreventUnsavedChangesGuard(i0.inject(i1.SweetAlertService), i0.inject(i2.PreventUnsavedChangesService)); }, token: PreventUnsavedChangesGuard, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesGuard.prototype.sweetAlert;
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesGuard.prototype.preventUnsavedChangesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmVudC11bnNhdmVkLWNoYW5nZXMuZ3VhcmQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvZ3VhcmRzL3ByZXZlbnQtdW5zYXZlZC1jaGFuZ2VzLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzVELE9BQU8sRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFHMUIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDMUUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sMkNBQTJDLENBQUM7Ozs7QUFNekYsTUFBTSxPQUFPLDBCQUEwQjs7Ozs7SUFFckMsWUFBb0IsVUFBNkIsRUFBVSw0QkFBMEQ7UUFBakcsZUFBVSxHQUFWLFVBQVUsQ0FBbUI7UUFBVSxpQ0FBNEIsR0FBNUIsNEJBQTRCLENBQThCO0lBQ3JILENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsSUFBSTtZQUNGLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6RCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFFUixPQUFPLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxTQUFTLEVBQUU7aUJBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFXLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxNQUFNLEtBQUssVUFBVSxFQUFFOzswQkFDbkIsU0FBUyxHQUFZLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLEVBQUU7b0JBQzNFLElBQUksU0FBUyxFQUFFO3dCQUNiLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQywwQkFBMEIsRUFBRTs2QkFDaEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQXdCLEVBQUUsRUFBRTs0QkFDckMscURBQXFEOzRCQUNyRCxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUU7Z0NBQ3JCLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQ3RELE9BQU8sSUFBSSxDQUFDOzZCQUNiO2lDQUFNO2dDQUNMLE9BQU8sS0FBSyxDQUFDOzZCQUNkO3dCQUNILENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ1A7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdEQsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2pCO2lCQUNGO2dCQUNELGlCQUFpQjtZQUNuQixDQUFDLENBQUMsRUFDQSxVQUFVLENBQUMsQ0FBQyxLQUFVLEVBQU8sRUFBRTtnQkFDN0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsQ0FBQyxDQUFDLENBQ0gsQ0FBQztTQUNMO1FBQUMsT0FBTyxHQUFHLEVBQUU7WUFDWixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDOzs7WUE3Q0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBTlEsaUJBQWlCO1lBQ2pCLDRCQUE0Qjs7Ozs7Ozs7SUFRdkIsZ0RBQXFDOzs7OztJQUFFLGtFQUFrRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FuRGVhY3RpdmF0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IG1hcCwgY29uY2F0TWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBvZiB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgVW5zYXZlZENoYW5nZXMgfSBmcm9tICcuLi8uLi9tb2RlbHMvdW5zYXZlZC1jaGFuZ2VzLm1vZGVsJztcclxuaW1wb3J0IHsgU3dlZXRBbGVydFNlcnZpY2UgfSBmcm9tICcuLi9jdXN0b20vc3dlZXQuYWxlcnQuY29uZmlybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZSB9IGZyb20gJy4uL2N1c3RvbS9wcmV2ZW50LXVuc2F2ZWQtY2hhbmdlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWxlcnRSZXN1bHQgfSBmcm9tICcuLi8uLi9tb2RlbHMvYWxlcnQtcmVzdWx0Lm1vZGVsJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFByZXZlbnRVbnNhdmVkQ2hhbmdlc0d1YXJkIGltcGxlbWVudHMgQ2FuRGVhY3RpdmF0ZTxVbnNhdmVkQ2hhbmdlcz4ge1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN3ZWV0QWxlcnQ6IFN3ZWV0QWxlcnRTZXJ2aWNlLCBwcml2YXRlIHByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2U6IFByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2UpIHtcclxuICB9XHJcblxyXG4gIGNhbkRlYWN0aXZhdGUoKTogYW55IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5zZXRTdGF0dXMoJ3dhaXRpbmcnKTtcclxuICAgICAgfSwgMTAwKTtcclxuXHJcbiAgICAgIHJldHVybiB0aGlzLnByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2UuZ2V0U3RhdHVzKClcclxuICAgICAgICAucGlwZShjb25jYXRNYXAoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzdWx0ID09PSAnZmluaXNoZWQnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzQ2hhbmdlZDogYm9vbGVhbiA9IHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5nZXRJc0NoYW5nZWQoKTtcclxuICAgICAgICAgICAgaWYgKGlzQ2hhbmdlZCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiB0aGlzLnN3ZWV0QWxlcnQucHJldmVudFVuc2F2ZWRDaGFuZ2VkUG9wdXAoKVxyXG4gICAgICAgICAgICAgICAgLnBpcGUobWFwKChhbGVydFJlc3VsdDogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ2NhbkRlYWN0aXZhdGUgaW4gcGlwZScsIGFsZXJ0UmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgaWYgKGFsZXJ0UmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLnNldElzQ2hhbmdlZChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLnNldElzQ2hhbmdlZChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIG9mKHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAvLyByZXR1cm4gcmVzdWx0O1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3I6IGFueSk6IGFueSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICByZXR1cm4gb2YodHJ1ZSk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19