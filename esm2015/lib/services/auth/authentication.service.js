/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import moment from 'moment';
import { tap, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AppConfig, StorageService } from '../custom/index';
import { BaseService } from '../http/index';
import { Headers, Storage, StorageType, Module } from '../../enums/index';
export class AuthenticationService extends BaseService {
    // @Inject(StorageService) private storageService: StorageService
    /**
     * @param {?} httpClient
     * @param {?} storageService
     * @param {?} router
     */
    constructor(httpClient, storageService, router) {
        super(httpClient);
        this.httpClient = httpClient;
        this.storageService = storageService;
        this.router = router;
        this.chagePasswordEndPoint = 'User/ChangePassword';
        this.changePasswordWithTokenEndPoint = 'User/ChangePasswordWithToken';
        this.isSuccess = new Subject();
        this.afterLoginUrl = '/dashboard';
        this.beforeLoginUrl = '/login';
        this.jwtHelperService = new JwtHelperService();
    }
    /**
     * @param {?} credentials
     * @param {?=} disableNavigation
     * @return {?}
     */
    login(credentials, disableNavigation) {
        /** @type {?} */
        const headerParameters = [];
        if (disableNavigation) {
            headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
        }
        /** @type {?} */
        const data = {
            grant_type: 'password',
            username: credentials.username,
            password: credentials.password,
            client_id: AppConfig.settings.aad.clientId
        };
        return this.create(data, Module.SignIn, headerParameters)
            .pipe(map((response) => {
            this.handdleTokenResponse(response);
        }));
    }
    /**
     * @param {?} response
     * @return {?}
     */
    handdleTokenResponse(response) {
        if (response && response.serviceResult.IsSuccess) {
            this.storageService.store(Storage.AccessToken, response.serviceResult.Result.access_token.token);
            this.storageService.store(Storage.RefreshToken, response.serviceResult.Result.refresh_token);
            /** @type {?} */
            const decodedToken = this.jwtHelperService.decodeToken(response.serviceResult.Result.access_token.token);
            /** @type {?} */
            const expirationDate = this.jwtHelperService.getTokenExpirationDate(response.serviceResult.Result.access_token.token);
            this.storageService.store(Storage.Expires, expirationDate);
            /** @type {?} */
            const userData = {
                username: decodedToken.sub,
                employeeId: decodedToken.employee_id,
                id: decodedToken.id
            };
            this.setUserAfterToken(userData);
        }
    }
    /**
     * @param {?=} disableNavigation
     * @param {?=} disableLoading
     * @return {?}
     */
    refreshToken(disableNavigation, disableLoading) {
        /** @type {?} */
        const headerParameters = [];
        /** @type {?} */
        const token = this.storageService.getStored(Storage.RefreshToken);
        if (disableNavigation) {
            headerParameters.push({ header: Headers.DisableNavigation, value: Headers.DisableNavigation });
        }
        if (disableLoading) {
            headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
        }
        /** @type {?} */
        const data = {
            grant_type: 'refresh_token',
            client_id: AppConfig.settings.aad.clientId,
            refresh_token: token
        };
        return this.create(data, Module.SignIn, headerParameters)
            .pipe(tap((response) => {
            this.handdleTokenResponse(response);
        }));
    }
    /**
     * @return {?}
     */
    loggedIn() {
        /** @type {?} */
        const token = this.storageService.getStored(Storage.AccessToken);
        if (token) {
            /** @type {?} */
            const isExpired = this.jwtHelperService.isTokenExpired(token);
            return !isExpired;
        }
        else {
            this.cleanLocalStorage();
            return false;
        }
    }
    /**
     * @return {?}
     */
    getTimeoutMiliseconds() {
        /** @type {?} */
        const expires = this.storageService.getStored(Storage.Expires);
        return moment(expires).valueOf() - moment().valueOf() - 120000;
    }
    /**
     * @return {?}
     */
    getAuthToken() {
        return this.storageService.getStored(Storage.AccessToken);
    }
    /**
     * @return {?}
     */
    cleanLocalStorage() {
        this.storageService.clear(Storage.AccessToken);
        this.storageService.clear(Storage.RefreshToken);
        this.storageService.clear(Storage.Expires);
        this.storageService.clear(Storage.User);
        this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
    }
    /**
     * @param {?} userData
     * @return {?}
     */
    setUserAfterToken(userData) {
        this.storageService.store(Storage.User, userData);
    }
    /**
     * @return {?}
     */
    getUserDataLoggedIn() {
        return this.storageService.getStored(Storage.User);
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    forgotPassword(formValue) {
        /** @type {?} */
        const email = formValue.email;
        /** @type {?} */
        const isLocked = formValue.isLocked;
        this.httpClient.get(AppConfig.settings.env.apiUrl + Module.Auth + 'User/ForgotPassword?email=' + email + '&isLocked=' + isLocked)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
                // token alınacak
            }
        });
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    changePassword(formValue) {
        this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.chagePasswordEndPoint, formValue)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
            }
        });
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    changePasswordWithToken(formValue) {
        this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.changePasswordWithTokenEndPoint, formValue)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
            }
        });
    }
    /**
     * @param {?=} disableNavigation
     * @return {?}
     */
    logout(disableNavigation) {
        // disableNavigation it is for session-expired
        this.storageService.clear(Storage.AccessToken);
        this.storageService.clear(Storage.RefreshToken);
        this.storageService.clear(Storage.Expires);
        this.storageService.clear(Storage.User);
        if (!disableNavigation) {
            // it makes to nagivate signin when cleared in master.app.ts
            this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
            this.router.navigate([this.beforeLoginUrl]);
        }
    }
}
AuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationService.ctorParameters = () => [
    { type: HttpClient },
    { type: StorageService },
    { type: Router }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.chagePasswordEndPoint;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.changePasswordWithTokenEndPoint;
    /** @type {?} */
    AuthenticationService.prototype.isSuccess;
    /** @type {?} */
    AuthenticationService.prototype.afterLoginUrl;
    /** @type {?} */
    AuthenticationService.prototype.beforeLoginUrl;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.jwtHelperService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.storageService;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9hdXRoL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QyxPQUFPLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMxQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRS9CLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXRELE9BQU8sRUFDTCxTQUFTLEVBQ1QsY0FBYyxFQUNmLE1BQU0saUJBQWlCLENBQUM7QUFFekIsT0FBTyxFQUNMLFdBQVcsRUFDWixNQUFNLGVBQWUsQ0FBQztBQU92QixPQUFPLEVBQ0wsT0FBTyxFQUNQLE9BQU8sRUFDUCxXQUFXLEVBQ1gsTUFBTSxFQUNQLE1BQU0sbUJBQW1CLENBQUM7QUFJM0IsTUFBTSxPQUFPLHFCQUFzQixTQUFRLFdBQVc7Ozs7Ozs7SUFZcEQsWUFDWSxVQUFzQixFQUN4QixjQUE4QixFQUM5QixNQUFjO1FBRXRCLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUpSLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFkaEIsMEJBQXFCLEdBQUcscUJBQXFCLENBQUM7UUFDOUMsb0NBQStCLEdBQUcsOEJBQThCLENBQUM7UUFFbEUsY0FBUyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFjL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7UUFDbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUM7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7SUFHRCxLQUFLLENBQUMsV0FBZ0IsRUFBRSxpQkFBMkI7O2NBQzNDLGdCQUFnQixHQUFzQixFQUFFO1FBRTlDLElBQUksaUJBQWlCLEVBQUU7WUFDckIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1NBQzFGOztjQUVLLElBQUksR0FBUTtZQUNoQixVQUFVLEVBQUUsVUFBVTtZQUN0QixRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVE7WUFDOUIsUUFBUSxFQUFFLFdBQVcsQ0FBQyxRQUFRO1lBQzlCLFNBQVMsRUFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRO1NBQzNDO1FBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFNLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxFQUFFLGdCQUFnQixDQUFDO2FBQzNELElBQUksQ0FDSCxHQUFHLENBQUMsQ0FBQyxRQUErQixFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUNILENBQUM7SUFDTixDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFFBQStCO1FBQ2xELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFO1lBRWhELElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7O2tCQUV2RixZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDOztrQkFDbEcsY0FBYyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO1lBRXJILElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsY0FBYyxDQUFDLENBQUM7O2tCQUVyRCxRQUFRLEdBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxZQUFZLENBQUMsR0FBRztnQkFDMUIsVUFBVSxFQUFFLFlBQVksQ0FBQyxXQUFXO2dCQUNwQyxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7YUFDcEI7WUFFRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7Ozs7SUFHRCxZQUFZLENBQUMsaUJBQTJCLEVBQUUsY0FBd0I7O2NBQzFELGdCQUFnQixHQUFzQixFQUFFOztjQUN4QyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztRQUVqRSxJQUFJLGlCQUFpQixFQUFFO1lBQ3JCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7U0FDaEc7UUFFRCxJQUFJLGNBQWMsRUFBRTtZQUNsQixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7U0FDMUY7O2NBRUssSUFBSSxHQUFRO1lBQ2hCLFVBQVUsRUFBRSxlQUFlO1lBQzNCLFNBQVMsRUFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRO1lBQzFDLGFBQWEsRUFBRSxLQUFLO1NBQ3JCO1FBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFNLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxFQUFFLGdCQUFnQixDQUFDO2FBQzNELElBQUksQ0FDSCxHQUFHLENBQ0QsQ0FBQyxRQUErQixFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FJRixDQUNGLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsUUFBUTs7Y0FDQSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUNoRSxJQUFJLEtBQUssRUFBRTs7a0JBQ0gsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO1lBQzdELE9BQU8sQ0FBQyxTQUFTLENBQUM7U0FDbkI7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3pCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDOzs7O0lBRUQscUJBQXFCOztjQUNiLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQzlELE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFHLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxHQUFHLE1BQU0sQ0FBQztJQUNqRSxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDZixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDL0UsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxRQUFhO1FBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7OztJQUVELG1CQUFtQjtRQUNqQixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxTQUFTOztjQUNoQixLQUFLLEdBQUcsU0FBUyxDQUFDLEtBQUs7O2NBQ3ZCLFFBQVEsR0FBRyxTQUFTLENBQUMsUUFBUTtRQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyw0QkFBNEIsR0FBRyxLQUFLLEdBQUcsWUFBWSxHQUFHLFFBQVEsQ0FBQzthQUM5SCxTQUFTLENBQUMsQ0FBQyxHQUEwQixFQUFFLEVBQUU7WUFDeEMsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUV0QyxpQkFBaUI7YUFDbEI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLFNBQVM7UUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixFQUFFLFNBQVMsQ0FBQzthQUN2RyxTQUFTLENBQUMsQ0FBQyxHQUEwQixFQUFFLEVBQUU7WUFDeEMsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2FBRXZDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELHVCQUF1QixDQUFDLFNBQVM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLCtCQUErQixFQUFFLFNBQVMsQ0FBQzthQUNqSCxTQUFTLENBQUMsQ0FBQyxHQUEwQixFQUFFLEVBQUU7WUFDeEMsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2FBQ3ZDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxpQkFBMkI7UUFDaEMsOENBQThDO1FBQzlDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV4QyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDdEIsNERBQTREO1lBQzVELElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzdFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7U0FDN0M7SUFDSCxDQUFDOzs7WUFwTEYsVUFBVTs7OztZQS9CRixVQUFVO1lBV2pCLGNBQWM7WUFWUCxNQUFNOzs7Ozs7O0lBZ0NiLHNEQUFzRDs7Ozs7SUFDdEQsZ0VBQXlFOztJQUV6RSwwQ0FBaUM7O0lBRWpDLDhDQUE2Qjs7SUFDN0IsK0NBQThCOzs7OztJQUU5QixpREFBMkM7Ozs7O0lBSXpDLDJDQUFnQzs7Ozs7SUFDaEMsK0NBQXNDOzs7OztJQUN0Qyx1Q0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgdGFwLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEp3dEhlbHBlclNlcnZpY2UgfSBmcm9tICdAYXV0aDAvYW5ndWxhci1qd3QnO1xyXG5cclxuaW1wb3J0IHtcclxuICBBcHBDb25maWcsXHJcbiAgU3RvcmFnZVNlcnZpY2VcclxufSBmcm9tICcuLi9jdXN0b20vaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICBCYXNlU2VydmljZVxyXG59IGZyb20gJy4uL2h0dHAvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICBDb3JlSHR0cFJlc3BvbnNlLFxyXG4gIEhlYWRlclBhcmFtZXRlcixcclxufSBmcm9tICcuLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICBIZWFkZXJzLFxyXG4gIFN0b3JhZ2UsXHJcbiAgU3RvcmFnZVR5cGUsXHJcbiAgTW9kdWxlXHJcbn0gZnJvbSAnLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uU2VydmljZSBleHRlbmRzIEJhc2VTZXJ2aWNlIHtcclxuICBwcml2YXRlIGNoYWdlUGFzc3dvcmRFbmRQb2ludCA9ICdVc2VyL0NoYW5nZVBhc3N3b3JkJztcclxuICBwcml2YXRlIGNoYW5nZVBhc3N3b3JkV2l0aFRva2VuRW5kUG9pbnQgPSAnVXNlci9DaGFuZ2VQYXNzd29yZFdpdGhUb2tlbic7XHJcblxyXG4gIHB1YmxpYyBpc1N1Y2Nlc3MgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICBwdWJsaWMgYWZ0ZXJMb2dpblVybDogc3RyaW5nO1xyXG4gIHB1YmxpYyBiZWZvcmVMb2dpblVybDogc3RyaW5nO1xyXG5cclxuICBwcml2YXRlIGp3dEhlbHBlclNlcnZpY2U6IEp3dEhlbHBlclNlcnZpY2U7XHJcblxyXG4gIC8vIEBJbmplY3QoU3RvcmFnZVNlcnZpY2UpIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcm90ZWN0ZWQgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxyXG4gICkge1xyXG4gICAgc3VwZXIoaHR0cENsaWVudCk7XHJcbiAgICB0aGlzLmFmdGVyTG9naW5VcmwgPSAnL2Rhc2hib2FyZCc7XHJcbiAgICB0aGlzLmJlZm9yZUxvZ2luVXJsID0gJy9sb2dpbic7XHJcbiAgICB0aGlzLmp3dEhlbHBlclNlcnZpY2UgPSBuZXcgSnd0SGVscGVyU2VydmljZSgpO1xyXG4gIH1cclxuXHJcblxyXG4gIGxvZ2luKGNyZWRlbnRpYWxzOiBhbnksIGRpc2FibGVOYXZpZ2F0aW9uPzogYm9vbGVhbikge1xyXG4gICAgY29uc3QgaGVhZGVyUGFyYW1ldGVyczogSGVhZGVyUGFyYW1ldGVyW10gPSBbXTtcclxuXHJcbiAgICBpZiAoZGlzYWJsZU5hdmlnYXRpb24pIHtcclxuICAgICAgaGVhZGVyUGFyYW1ldGVycy5wdXNoKHsgaGVhZGVyOiBIZWFkZXJzLkRpc2FibGVMb2FkaW5nLCB2YWx1ZTogSGVhZGVycy5EaXNhYmxlTG9hZGluZyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBkYXRhOiBhbnkgPSB7XHJcbiAgICAgIGdyYW50X3R5cGU6ICdwYXNzd29yZCcsXHJcbiAgICAgIHVzZXJuYW1lOiBjcmVkZW50aWFscy51c2VybmFtZSxcclxuICAgICAgcGFzc3dvcmQ6IGNyZWRlbnRpYWxzLnBhc3N3b3JkLFxyXG4gICAgICBjbGllbnRfaWQ6IEFwcENvbmZpZy5zZXR0aW5ncy5hYWQuY2xpZW50SWRcclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuY3JlYXRlPGFueT4oZGF0YSwgTW9kdWxlLlNpZ25JbiwgaGVhZGVyUGFyYW1ldGVycylcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgbWFwKChyZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmhhbmRkbGVUb2tlblJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgaGFuZGRsZVRva2VuUmVzcG9uc2UocmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55Pik6IHZvaWQge1xyXG4gICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuSXNTdWNjZXNzKSB7XHJcblxyXG4gICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnN0b3JlKFN0b3JhZ2UuQWNjZXNzVG9rZW4sIHJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0LmFjY2Vzc190b2tlbi50b2tlbik7XHJcbiAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2Uuc3RvcmUoU3RvcmFnZS5SZWZyZXNoVG9rZW4sIHJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0LnJlZnJlc2hfdG9rZW4pO1xyXG5cclxuICAgICAgY29uc3QgZGVjb2RlZFRva2VuID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmRlY29kZVRva2VuKHJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0LmFjY2Vzc190b2tlbi50b2tlbik7XHJcbiAgICAgIGNvbnN0IGV4cGlyYXRpb25EYXRlID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmdldFRva2VuRXhwaXJhdGlvbkRhdGUocmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQuYWNjZXNzX3Rva2VuLnRva2VuKTtcclxuXHJcbiAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2Uuc3RvcmUoU3RvcmFnZS5FeHBpcmVzLCBleHBpcmF0aW9uRGF0ZSk7XHJcblxyXG4gICAgICBjb25zdCB1c2VyRGF0YTogYW55ID0ge1xyXG4gICAgICAgIHVzZXJuYW1lOiBkZWNvZGVkVG9rZW4uc3ViLFxyXG4gICAgICAgIGVtcGxveWVlSWQ6IGRlY29kZWRUb2tlbi5lbXBsb3llZV9pZCxcclxuICAgICAgICBpZDogZGVjb2RlZFRva2VuLmlkXHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLnNldFVzZXJBZnRlclRva2VuKHVzZXJEYXRhKTtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxuICByZWZyZXNoVG9rZW4oZGlzYWJsZU5hdmlnYXRpb24/OiBib29sZWFuLCBkaXNhYmxlTG9hZGluZz86IGJvb2xlYW4pIHtcclxuICAgIGNvbnN0IGhlYWRlclBhcmFtZXRlcnM6IEhlYWRlclBhcmFtZXRlcltdID0gW107XHJcbiAgICBjb25zdCB0b2tlbiA9IHRoaXMuc3RvcmFnZVNlcnZpY2UuZ2V0U3RvcmVkKFN0b3JhZ2UuUmVmcmVzaFRva2VuKTtcclxuXHJcbiAgICBpZiAoZGlzYWJsZU5hdmlnYXRpb24pIHtcclxuICAgICAgaGVhZGVyUGFyYW1ldGVycy5wdXNoKHsgaGVhZGVyOiBIZWFkZXJzLkRpc2FibGVOYXZpZ2F0aW9uLCB2YWx1ZTogSGVhZGVycy5EaXNhYmxlTmF2aWdhdGlvbiB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoZGlzYWJsZUxvYWRpbmcpIHtcclxuICAgICAgaGVhZGVyUGFyYW1ldGVycy5wdXNoKHsgaGVhZGVyOiBIZWFkZXJzLkRpc2FibGVMb2FkaW5nLCB2YWx1ZTogSGVhZGVycy5EaXNhYmxlTG9hZGluZyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBkYXRhOiBhbnkgPSB7XHJcbiAgICAgIGdyYW50X3R5cGU6ICdyZWZyZXNoX3Rva2VuJyxcclxuICAgICAgY2xpZW50X2lkOiBBcHBDb25maWcuc2V0dGluZ3MuYWFkLmNsaWVudElkLFxyXG4gICAgICByZWZyZXNoX3Rva2VuOiB0b2tlblxyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gdGhpcy5jcmVhdGU8YW55PihkYXRhLCBNb2R1bGUuU2lnbkluLCBoZWFkZXJQYXJhbWV0ZXJzKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICB0YXAoXHJcbiAgICAgICAgICAocmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmhhbmRkbGVUb2tlblJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAvLyAoZXJyb3I6IGFueSkgPT4ge1xyXG4gICAgICAgICAgLy8gICAvLyBjb25zb2xlLmxvZygnYXV0aC1zZXJ2aWNlJywgZXJyb3IpO1xyXG4gICAgICAgICAgLy8gfVxyXG4gICAgICAgIClcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIGxvZ2dlZEluKCkge1xyXG4gICAgY29uc3QgdG9rZW4gPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldFN0b3JlZChTdG9yYWdlLkFjY2Vzc1Rva2VuKTtcclxuICAgIGlmICh0b2tlbikge1xyXG4gICAgICBjb25zdCBpc0V4cGlyZWQgPSB0aGlzLmp3dEhlbHBlclNlcnZpY2UuaXNUb2tlbkV4cGlyZWQodG9rZW4pO1xyXG4gICAgICByZXR1cm4gIWlzRXhwaXJlZDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2xlYW5Mb2NhbFN0b3JhZ2UoKTtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0VGltZW91dE1pbGlzZWNvbmRzKCk6IG51bWJlciB7XHJcbiAgICBjb25zdCBleHBpcmVzID0gdGhpcy5zdG9yYWdlU2VydmljZS5nZXRTdG9yZWQoU3RvcmFnZS5FeHBpcmVzKTtcclxuICAgIHJldHVybiBtb21lbnQoZXhwaXJlcykudmFsdWVPZigpIC0gbW9tZW50KCkudmFsdWVPZigpIC0gMTIwMDAwO1xyXG4gIH1cclxuXHJcbiAgZ2V0QXV0aFRva2VuKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5zdG9yYWdlU2VydmljZS5nZXRTdG9yZWQoU3RvcmFnZS5BY2Nlc3NUb2tlbik7XHJcbiAgfVxyXG5cclxuICBjbGVhbkxvY2FsU3RvcmFnZSgpIHtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5BY2Nlc3NUb2tlbik7XHJcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmNsZWFyKFN0b3JhZ2UuUmVmcmVzaFRva2VuKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5FeHBpcmVzKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5Vc2VyKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5TeXN0ZW1SZWZEYXRlLCBTdG9yYWdlVHlwZS5TZXNzaW9uU3RvcmFnZSk7XHJcbiAgfVxyXG5cclxuICBzZXRVc2VyQWZ0ZXJUb2tlbih1c2VyRGF0YTogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnN0b3JlKFN0b3JhZ2UuVXNlciwgdXNlckRhdGEpO1xyXG4gIH1cclxuXHJcbiAgZ2V0VXNlckRhdGFMb2dnZWRJbigpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuc3RvcmFnZVNlcnZpY2UuZ2V0U3RvcmVkKFN0b3JhZ2UuVXNlcik7XHJcbiAgfVxyXG5cclxuICBmb3Jnb3RQYXNzd29yZChmb3JtVmFsdWUpIHtcclxuICAgIGNvbnN0IGVtYWlsID0gZm9ybVZhbHVlLmVtYWlsO1xyXG4gICAgY29uc3QgaXNMb2NrZWQgPSBmb3JtVmFsdWUuaXNMb2NrZWQ7XHJcbiAgICB0aGlzLmh0dHBDbGllbnQuZ2V0KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgTW9kdWxlLkF1dGggKyAnVXNlci9Gb3Jnb3RQYXNzd29yZD9lbWFpbD0nICsgZW1haWwgKyAnJmlzTG9ja2VkPScgKyBpc0xvY2tlZClcclxuICAgICAgLnN1YnNjcmliZSgocmVzOiBDb3JlSHR0cFJlc3BvbnNlPGFueT4pID0+IHtcclxuICAgICAgICBpZiAocmVzWydJc1N1Y2Nlc3MnXSkge1xyXG4gICAgICAgICAgdGhpcy5pc1N1Y2Nlc3MubmV4dChyZXNbJ0lzU3VjY2VzcyddKTtcclxuXHJcbiAgICAgICAgICAvLyB0b2tlbiBhbMSxbmFjYWtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2hhbmdlUGFzc3dvcmQoZm9ybVZhbHVlKSB7XHJcbiAgICB0aGlzLmh0dHBDbGllbnQucGF0Y2goQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBNb2R1bGUuQXV0aCArIHRoaXMuY2hhZ2VQYXNzd29yZEVuZFBvaW50LCBmb3JtVmFsdWUpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlczogQ29yZUh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XHJcbiAgICAgICAgaWYgKHJlc1snSXNTdWNjZXNzJ10pIHtcclxuICAgICAgICAgIHRoaXMuaXNTdWNjZXNzLm5leHQocmVzWydJc1N1Y2Nlc3MnXSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VQYXNzd29yZFdpdGhUb2tlbihmb3JtVmFsdWUpIHtcclxuICAgIHRoaXMuaHR0cENsaWVudC5wYXRjaChBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIE1vZHVsZS5BdXRoICsgdGhpcy5jaGFuZ2VQYXNzd29yZFdpdGhUb2tlbkVuZFBvaW50LCBmb3JtVmFsdWUpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlczogQ29yZUh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XHJcbiAgICAgICAgaWYgKHJlc1snSXNTdWNjZXNzJ10pIHtcclxuICAgICAgICAgIHRoaXMuaXNTdWNjZXNzLm5leHQocmVzWydJc1N1Y2Nlc3MnXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGxvZ291dChkaXNhYmxlTmF2aWdhdGlvbj86IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIC8vIGRpc2FibGVOYXZpZ2F0aW9uIGl0IGlzIGZvciBzZXNzaW9uLWV4cGlyZWRcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5BY2Nlc3NUb2tlbik7XHJcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmNsZWFyKFN0b3JhZ2UuUmVmcmVzaFRva2VuKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5FeHBpcmVzKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoU3RvcmFnZS5Vc2VyKTtcclxuXHJcbiAgICBpZiAoIWRpc2FibGVOYXZpZ2F0aW9uKSB7XHJcbiAgICAgIC8vIGl0IG1ha2VzIHRvIG5hZ2l2YXRlIHNpZ25pbiB3aGVuIGNsZWFyZWQgaW4gbWFzdGVyLmFwcC50c1xyXG4gICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmNsZWFyKFN0b3JhZ2UuU3lzdGVtUmVmRGF0ZSwgU3RvcmFnZVR5cGUuU2Vzc2lvblN0b3JhZ2UpO1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5iZWZvcmVMb2dpblVybF0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=