/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
export class PreventUnsavedChangesService {
    constructor() {
        this.status = new Subject();
    }
    /**
     * @return {?}
     */
    getIsChanged() {
        return this.isChanged;
    }
    /**
     * @param {?} isChanged
     * @return {?}
     */
    setIsChanged(isChanged) {
        this.isChanged = isChanged;
    }
    /**
     * @return {?}
     */
    getStatus() {
        return this.status.asObservable();
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setStatus(status) {
        this.status.next(status);
    }
}
PreventUnsavedChangesService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PreventUnsavedChangesService.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesService.prototype.isChanged;
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesService.prototype.status;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmVudC11bnNhdmVkLWNoYW5nZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9jdXN0b20vcHJldmVudC11bnNhdmVkLWNoYW5nZXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBRzNDLE1BQU0sT0FBTyw0QkFBNEI7SUFLdkM7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsU0FBa0I7UUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELFNBQVM7UUFDUCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDcEMsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsTUFBYztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7WUF4QkYsVUFBVTs7Ozs7Ozs7O0lBRVQsaURBQTJCOzs7OztJQUUzQiw4Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2Uge1xyXG4gIHByaXZhdGUgaXNDaGFuZ2VkOiBib29sZWFuO1xyXG4gIC8vICd3YWl0LCBmaW5pc2hlZCdcclxuICBwcml2YXRlIHN0YXR1czogU3ViamVjdDxzdHJpbmc+O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuc3RhdHVzID0gbmV3IFN1YmplY3QoKTtcclxuICB9XHJcblxyXG4gIGdldElzQ2hhbmdlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzQ2hhbmdlZDtcclxuICB9XHJcblxyXG4gIHNldElzQ2hhbmdlZChpc0NoYW5nZWQ6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIHRoaXMuaXNDaGFuZ2VkID0gaXNDaGFuZ2VkO1xyXG4gIH1cclxuXHJcbiAgZ2V0U3RhdHVzKCk6IE9ic2VydmFibGU8c3RyaW5nPiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdGF0dXMuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZXRTdGF0dXMoc3RhdHVzOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMuc3RhdHVzLm5leHQoc3RhdHVzKTtcclxuICB9XHJcbn1cclxuIl19