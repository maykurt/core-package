/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as i0 from "@angular/core";
export class CoreModalService {
    constructor() {
        this.isModalDisplayed = new BehaviorSubject(false);
    }
    /**
     * @return {?}
     */
    getModalDisplayStatus() {
        return this.isModalDisplayed.asObservable();
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setModalDisplayStatus(status) {
        this.isModalDisplayed.next(status || false);
    }
}
CoreModalService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreModalService.ctorParameters = () => [];
/** @nocollapse */ CoreModalService.ngInjectableDef = i0.defineInjectable({ factory: function CoreModalService_Factory() { return new CoreModalService(); }, token: CoreModalService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoreModalService.prototype.isModalDisplayed;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1tb2RhbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9jb3JlLW1vZGFsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDOztBQU12RCxNQUFNLE9BQU8sZ0JBQWdCO0lBR3pCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7SUFFRCxxQkFBcUI7UUFDakIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDaEQsQ0FBQzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxNQUFlO1FBQ2pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7OztZQWhCSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7Ozs7Ozs7SUFFRyw0Q0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMvQmVoYXZpb3JTdWJqZWN0JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVNb2RhbFNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBpc01vZGFsRGlzcGxheWVkOiBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj47XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5pc01vZGFsRGlzcGxheWVkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TW9kYWxEaXNwbGF5U3RhdHVzKCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTW9kYWxEaXNwbGF5ZWQuYXNPYnNlcnZhYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TW9kYWxEaXNwbGF5U3RhdHVzKHN0YXR1czogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuaXNNb2RhbERpc3BsYXllZC5uZXh0KHN0YXR1cyB8fCBmYWxzZSk7XHJcbiAgICB9XHJcblxyXG5cclxufVxyXG4iXX0=