/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';
export class LoadingService {
    constructor() {
        this.loadingRequests = [];
        this.loadingBarDisplayStatus = new BehaviorSubject(false);
    }
    /**
     * @return {?}
     */
    getLoadingBarDisplayStatus() {
        return this.loadingBarDisplayStatus.asObservable();
    }
    /**
     * @param {?} request
     * @return {?}
     */
    insertLoadingRequest(request) {
        if (!_.isNull(request)) {
            this.loadingRequests.push(request);
            this.checkLoadingDisplayStatus();
        }
    }
    /**
     * @param {?} request
     * @return {?}
     */
    removeLoadingRequest(request) {
        if (!_.isNull(request)) {
            /** @type {?} */
            const requestIndex = this.loadingRequests.indexOf(request);
            if (requestIndex !== -1) {
                this.loadingRequests.splice(requestIndex, 1);
            }
            this.checkLoadingDisplayStatus();
        }
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setLoadingBarSetting(status) {
        this.loadingBarDisplayStatus.next(status);
    }
    /**
     * @return {?}
     */
    checkLoadingDisplayStatus() {
        if (this.loadingRequests && this.loadingRequests.length > 0) {
            this.setLoadingBarSetting(true);
        }
        else {
            this.setLoadingBarSetting(false);
        }
    }
}
LoadingService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LoadingService.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    LoadingService.prototype.loadingBarDisplayStatus;
    /**
     * @type {?}
     * @private
     */
    LoadingService.prototype.loadingRequests;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9sb2FkaW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXZELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRzVCLE1BQU0sT0FBTyxjQUFjO0lBS3pCO1FBQ0UsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO0lBQ3JFLENBQUM7Ozs7SUFFRCwwQkFBMEI7UUFDeEIsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxPQUFlO1FBQ2xDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxPQUFlO1FBQ2xDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFOztrQkFDaEIsWUFBWSxHQUFXLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUVsRSxJQUFJLFlBQVksS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7OztJQUVBLG9CQUFvQixDQUFDLE1BQU07UUFDMUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUEseUJBQXlCO1FBQ3hCLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pDO2FBQU07WUFDTCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7WUEzQ0YsVUFBVTs7Ozs7O0lBR1QsaURBQXlEOzs7OztJQUN6RCx5Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMvQmVoYXZpb3JTdWJqZWN0JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExvYWRpbmdTZXJ2aWNlIHtcclxuXHJcbiAgcHVibGljIGxvYWRpbmdCYXJEaXNwbGF5U3RhdHVzOiBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj47XHJcbiAgcHJpdmF0ZSBsb2FkaW5nUmVxdWVzdHM6IHN0cmluZ1tdO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMubG9hZGluZ1JlcXVlc3RzID0gW107XHJcbiAgICB0aGlzLmxvYWRpbmdCYXJEaXNwbGF5U3RhdHVzID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XHJcbiAgfVxyXG5cclxuICBnZXRMb2FkaW5nQmFyRGlzcGxheVN0YXR1cygpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgIHJldHVybiB0aGlzLmxvYWRpbmdCYXJEaXNwbGF5U3RhdHVzLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgaW5zZXJ0TG9hZGluZ1JlcXVlc3QocmVxdWVzdDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIV8uaXNOdWxsKHJlcXVlc3QpKSB7XHJcbiAgICAgIHRoaXMubG9hZGluZ1JlcXVlc3RzLnB1c2gocmVxdWVzdCk7XHJcbiAgICAgIHRoaXMuY2hlY2tMb2FkaW5nRGlzcGxheVN0YXR1cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlTG9hZGluZ1JlcXVlc3QocmVxdWVzdDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIV8uaXNOdWxsKHJlcXVlc3QpKSB7XHJcbiAgICAgIGNvbnN0IHJlcXVlc3RJbmRleDogbnVtYmVyID0gdGhpcy5sb2FkaW5nUmVxdWVzdHMuaW5kZXhPZihyZXF1ZXN0KTtcclxuXHJcbiAgICAgIGlmIChyZXF1ZXN0SW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nUmVxdWVzdHMuc3BsaWNlKHJlcXVlc3RJbmRleCwgMSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jaGVja0xvYWRpbmdEaXNwbGF5U3RhdHVzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAgc2V0TG9hZGluZ0JhclNldHRpbmcoc3RhdHVzKSB7XHJcbiAgICB0aGlzLmxvYWRpbmdCYXJEaXNwbGF5U3RhdHVzLm5leHQoc3RhdHVzKTtcclxuICB9XHJcblxyXG4gICBjaGVja0xvYWRpbmdEaXNwbGF5U3RhdHVzKCkge1xyXG4gICAgaWYgKHRoaXMubG9hZGluZ1JlcXVlc3RzICYmIHRoaXMubG9hZGluZ1JlcXVlc3RzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5zZXRMb2FkaW5nQmFyU2V0dGluZyh0cnVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2V0TG9hZGluZ0JhclNldHRpbmcoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=