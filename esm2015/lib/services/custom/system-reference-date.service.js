/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StorageService } from './storage.service';
import { Storage } from '../../enums/storage.enum';
import { StorageType } from '../../enums/storage-type.enum';
export class SystemReferenceDateService {
    /**
     * @param {?} storageService
     */
    constructor(storageService) {
        this.storageService = storageService;
        this.systemDate = '';
        this.dateChange = new BehaviorSubject('');
    }
    /**
     * @param {?} newDate
     * @return {?}
     */
    setSelectedReferenceDate(newDate) {
        this.systemDate = newDate;
        this.storageService.store(Storage.SystemRefDate, newDate, StorageType.SessionStorage);
        this.dateChange.next(newDate);
    }
    /**
     * @return {?}
     */
    checkIsNullAndChangeCurrentDateWithStorage() {
        /** @type {?} */
        const systemDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (systemDate && systemDate !== this.systemDate) {
            this.systemDate = systemDate;
            this.dateChange.next(systemDate);
        }
        else if (!systemDate) {
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    getSelectedReferenceDate() {
        return this.dateChange.asObservable();
    }
    /**
     * @return {?}
     */
    getSystemDate() {
        return this.systemDate;
    }
    /**
     * @return {?}
     */
    getCurrentDateAsISO() {
        return this.convertDateToISO(new Date);
    }
    /**
     * @param {?} date
     * @return {?}
     */
    convertDateToISO(date) {
        /** @type {?} */
        const newDate = new Date(date);
        /** @type {?} */
        const year = newDate.getFullYear();
        /** @type {?} */
        let month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        let day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    }
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    convertDateToDateUTC(dateAsString) {
        /** @type {?} */
        const newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    }
}
SystemReferenceDateService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SystemReferenceDateService.ctorParameters = () => [
    { type: StorageService }
];
if (false) {
    /** @type {?} */
    SystemReferenceDateService.prototype.dateChange;
    /** @type {?} */
    SystemReferenceDateService.prototype.systemDate;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXJlZmVyZW5jZS1kYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY3VzdG9tL3N5c3RlbS1yZWZlcmVuY2UtZGF0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUV2RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUk1RCxNQUFNLE9BQU8sMEJBQTBCOzs7O0lBS3JDLFlBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksZUFBZSxDQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7O0lBRUQsd0JBQXdCLENBQUMsT0FBZTtRQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELDBDQUEwQzs7Y0FDbEMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLGNBQWMsQ0FBQztRQUNuRyxJQUFJLFVBQVUsSUFBSSxVQUFVLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDdEIsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELHdCQUF3QjtRQUN0QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELG1CQUFtQjtRQUNqQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsSUFBVTs7Y0FDbkIsT0FBTyxHQUFTLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQzs7Y0FDbEMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUU7O1lBQzFCLEtBQUssR0FBRyxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDOztZQUN2QyxHQUFHLEdBQUcsRUFBRSxHQUFHLE9BQU8sQ0FBQyxPQUFPLEVBQUU7UUFFOUIsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwQixLQUFLLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQztTQUNyQjtRQUNELElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbEIsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUM7U0FDakI7UUFFRCxPQUFPLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsZ0JBQWdCLENBQUM7SUFDekQsQ0FBQzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxZQUFvQjs7Y0FDakMsT0FBTyxHQUFTLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM1QyxPQUFPLElBQUksSUFBSSxDQUNiLE9BQU8sQ0FBQyxjQUFjLEVBQUUsRUFDeEIsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUNyQixPQUFPLENBQUMsVUFBVSxFQUFFLEVBQ3BCLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFDckIsT0FBTyxDQUFDLGFBQWEsRUFBRSxFQUN2QixPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDOzs7WUFqRUYsVUFBVTs7OztZQUxGLGNBQWM7Ozs7SUFRckIsZ0RBQTJDOztJQUMzQyxnREFBMEI7Ozs7O0lBRWQsb0RBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzL0JlaGF2aW9yU3ViamVjdCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJy4vc3RvcmFnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RvcmFnZSB9IGZyb20gJy4uLy4uL2VudW1zL3N0b3JhZ2UuZW51bSc7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlIH0gZnJvbSAnLi4vLi4vZW51bXMvc3RvcmFnZS10eXBlLmVudW0nO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlIHtcclxuXHJcbiAgcHVibGljIGRhdGVDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+O1xyXG4gIHB1YmxpYyBzeXN0ZW1EYXRlOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLnN5c3RlbURhdGUgPSAnJztcclxuICAgIHRoaXMuZGF0ZUNoYW5nZSA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPignJyk7XHJcbiAgfVxyXG5cclxuICBzZXRTZWxlY3RlZFJlZmVyZW5jZURhdGUobmV3RGF0ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnN5c3RlbURhdGUgPSBuZXdEYXRlO1xyXG4gICAgdGhpcy5zdG9yYWdlU2VydmljZS5zdG9yZShTdG9yYWdlLlN5c3RlbVJlZkRhdGUsIG5ld0RhdGUsIFN0b3JhZ2VUeXBlLlNlc3Npb25TdG9yYWdlKTtcclxuICAgIHRoaXMuZGF0ZUNoYW5nZS5uZXh0KG5ld0RhdGUpO1xyXG4gIH1cclxuXHJcbiAgY2hlY2tJc051bGxBbmRDaGFuZ2VDdXJyZW50RGF0ZVdpdGhTdG9yYWdlKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3Qgc3lzdGVtRGF0ZSA9IHRoaXMuc3RvcmFnZVNlcnZpY2UuZ2V0U3RvcmVkKFN0b3JhZ2UuU3lzdGVtUmVmRGF0ZSwgU3RvcmFnZVR5cGUuU2Vzc2lvblN0b3JhZ2UpO1xyXG4gICAgaWYgKHN5c3RlbURhdGUgJiYgc3lzdGVtRGF0ZSAhPT0gdGhpcy5zeXN0ZW1EYXRlKSB7XHJcbiAgICAgIHRoaXMuc3lzdGVtRGF0ZSA9IHN5c3RlbURhdGU7XHJcbiAgICAgIHRoaXMuZGF0ZUNoYW5nZS5uZXh0KHN5c3RlbURhdGUpO1xyXG4gICAgfSBlbHNlIGlmICghc3lzdGVtRGF0ZSkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUmVmZXJlbmNlRGF0ZSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZGF0ZUNoYW5nZS5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIGdldFN5c3RlbURhdGUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zeXN0ZW1EYXRlO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q3VycmVudERhdGVBc0lTTygpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udmVydERhdGVUb0lTTyhuZXcgRGF0ZSk7XHJcbiAgfVxyXG5cclxuICBjb252ZXJ0RGF0ZVRvSVNPKGRhdGU6IERhdGUpOiBzdHJpbmcge1xyXG4gICAgY29uc3QgbmV3RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKGRhdGUpLFxyXG4gICAgICB5ZWFyID0gbmV3RGF0ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgbGV0IG1vbnRoID0gJycgKyAobmV3RGF0ZS5nZXRNb250aCgpICsgMSksXHJcbiAgICAgIGRheSA9ICcnICsgbmV3RGF0ZS5nZXREYXRlKCk7XHJcblxyXG4gICAgaWYgKG1vbnRoLmxlbmd0aCA8IDIpIHtcclxuICAgICAgbW9udGggPSAnMCcgKyBtb250aDtcclxuICAgIH1cclxuICAgIGlmIChkYXkubGVuZ3RoIDwgMikge1xyXG4gICAgICBkYXkgPSAnMCcgKyBkYXk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIFt5ZWFyLCBtb250aCwgZGF5XS5qb2luKCctJykgKyAnVDAwOjAwOjAwLjAwMFonO1xyXG4gIH1cclxuXHJcbiAgY29udmVydERhdGVUb0RhdGVVVEMoZGF0ZUFzU3RyaW5nOiBzdHJpbmcpOiBEYXRlIHtcclxuICAgIGNvbnN0IG5ld0RhdGU6IERhdGUgPSBuZXcgRGF0ZShkYXRlQXNTdHJpbmcpO1xyXG4gICAgcmV0dXJuIG5ldyBEYXRlKFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ0Z1bGxZZWFyKCksXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDTW9udGgoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENEYXRlKCksXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDSG91cnMoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENNaW51dGVzKCksXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDU2Vjb25kcygpKTtcclxuICB9XHJcbn1cclxuIl19