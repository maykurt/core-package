/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/share';
import { StorageType } from '../../enums/storage-type.enum';
export class StorageService {
    constructor() {
        this.onSubject = new Subject();
        this.changes = this.onSubject.asObservable().share();
        this.start();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.stop();
    }
    /**
     * @param {?=} storageType
     * @return {?}
     */
    getStorage(storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        /** @type {?} */
        let s = [];
        for (let i = 0; i < localStorage.length; i++) {
            s.push({
                key: storage === StorageType.LocalStorage ? localStorage.key(i) : sessionStorage.key(i),
                value: storage === StorageType.LocalStorage ?
                    JSON.parse(localStorage.getItem(localStorage.key(i))) : JSON.parse(localStorage.getItem(sessionStorage.key(i)))
            });
        }
        return s;
    }
    /**
     * @param {?} key
     * @param {?} data
     * @param {?=} storageType
     * @return {?}
     */
    store(key, data, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.setItem(key, JSON.stringify(data));
        }
        else {
            sessionStorage.setItem(key, JSON.stringify(data));
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: data });
    }
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    getStored(key, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        try {
            if (storage === StorageType.LocalStorage) {
                return JSON.parse(localStorage.getItem(key));
            }
            else {
                return JSON.parse(sessionStorage.getItem(key));
            }
        }
        catch (e) {
            if (storage === StorageType.LocalStorage) {
                return localStorage.getItem(key);
            }
            else {
                return sessionStorage.getItem(key);
            }
        }
    }
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    clear(key, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.removeItem(key);
        }
        else {
            sessionStorage.removeItem(key);
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: null });
    }
    /**
     * @private
     * @return {?}
     */
    start() {
        window.addEventListener('storage', this.storageEventListener.bind(this));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    storageEventListener(event) {
        if (event.storageArea == localStorage) {
            /** @type {?} */
            let v;
            try {
                v = JSON.parse(event.newValue);
            }
            catch (e) {
                v = event.newValue;
            }
            this.onSubject.next({ key: event.key, value: v });
        }
    }
    /**
     * @private
     * @return {?}
     */
    stop() {
        window.removeEventListener('storage', this.storageEventListener.bind(this));
        this.onSubject.complete();
    }
}
StorageService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
StorageService.ctorParameters = () => [];
if (false) {
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.onSubject;
    /** @type {?} */
    StorageService.prototype.changes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9zdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN2QyxPQUFPLHlCQUF5QixDQUFDO0FBRWpDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUc1RCxNQUFNLE9BQU8sY0FBYztJQUl6QjtRQUhRLGNBQVMsR0FBRyxJQUFJLE9BQU8sRUFBK0IsQ0FBQztRQUN4RCxZQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUdyRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7O0lBRU0sVUFBVSxDQUFDLFdBQXlCOztZQUNyQyxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTs7WUFDOUQsQ0FBQyxHQUFHLEVBQUU7UUFDVixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNMLEdBQUcsRUFBRSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLEtBQUssRUFBRSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUMzQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEgsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7Ozs7Ozs7SUFFTSxLQUFLLENBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxXQUF5Qjs7WUFDeEQsT0FBTyxHQUFnQixXQUFXLElBQUksV0FBVyxDQUFDLFlBQVk7UUFDbEUsSUFBSSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksRUFBRTtZQUN4QyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDakQ7YUFBTTtZQUNMLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNuRDtRQUVELHlFQUF5RTtRQUN6RSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7O0lBRU0sU0FBUyxDQUFDLEdBQVcsRUFBRSxXQUF5Qjs7WUFDakQsT0FBTyxHQUFnQixXQUFXLElBQUksV0FBVyxDQUFDLFlBQVk7UUFDbEUsSUFBSTtZQUNGLElBQUksT0FBTyxLQUFLLFdBQVcsQ0FBQyxZQUFZLEVBQUU7Z0JBQ3hDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNoRDtTQUNGO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixJQUFJLE9BQU8sS0FBSyxXQUFXLENBQUMsWUFBWSxFQUFFO2dCQUN4QyxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEM7aUJBQU07Z0JBQ0wsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3BDO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7SUFFTSxLQUFLLENBQUMsR0FBRyxFQUFFLFdBQXlCOztZQUNyQyxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTtRQUNsRSxJQUFJLE9BQU8sS0FBSyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQ3hDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDOUI7YUFBTTtZQUNMLGNBQWMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7UUFFRCx5RUFBeUU7UUFDekUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7O0lBR08sS0FBSztRQUNYLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7Ozs7OztJQUVPLG9CQUFvQixDQUFDLEtBQW1CO1FBQzlDLElBQUksS0FBSyxDQUFDLFdBQVcsSUFBSSxZQUFZLEVBQUU7O2dCQUNqQyxDQUFDO1lBQ0wsSUFBSTtnQkFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7YUFBRTtZQUN2QyxPQUFPLENBQUMsRUFBRTtnQkFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQzthQUFFO1lBQ2pDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDbkQ7SUFDSCxDQUFDOzs7OztJQUVPLElBQUk7UUFDVixNQUFNLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzVCLENBQUM7OztZQXBGRixVQUFVOzs7Ozs7Ozs7SUFFVCxtQ0FBK0Q7O0lBQy9ELGlDQUF1RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcy9TdWJqZWN0JztcclxuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9zaGFyZSc7XHJcblxyXG5pbXBvcnQgeyBTdG9yYWdlVHlwZSB9IGZyb20gJy4uLy4uL2VudW1zL3N0b3JhZ2UtdHlwZS5lbnVtJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN0b3JhZ2VTZXJ2aWNlIGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuICBwcml2YXRlIG9uU3ViamVjdCA9IG5ldyBTdWJqZWN0PHsga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkgfT4oKTtcclxuICBwdWJsaWMgY2hhbmdlcyA9IHRoaXMub25TdWJqZWN0LmFzT2JzZXJ2YWJsZSgpLnNoYXJlKCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5zdGFydCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnN0b3AoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTdG9yYWdlKHN0b3JhZ2VUeXBlPzogU3RvcmFnZVR5cGUpIHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIGxldCBzID0gW107XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxvY2FsU3RvcmFnZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICBzLnB1c2goe1xyXG4gICAgICAgIGtleTogc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlID8gbG9jYWxTdG9yYWdlLmtleShpKSA6IHNlc3Npb25TdG9yYWdlLmtleShpKSxcclxuICAgICAgICB2YWx1ZTogc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlID9cclxuICAgICAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0obG9jYWxTdG9yYWdlLmtleShpKSkpIDogSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShzZXNzaW9uU3RvcmFnZS5rZXkoaSkpKVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBzO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0b3JlKGtleTogc3RyaW5nLCBkYXRhOiBhbnksIHN0b3JhZ2VUeXBlPzogU3RvcmFnZVR5cGUpOiB2b2lkIHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oa2V5LCBKU09OLnN0cmluZ2lmeShkYXRhKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKGtleSwgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHRoZSBsb2NhbCBhcHBsaWNhdGlvbiBkb2Vzbid0IHNlZW0gdG8gY2F0Y2ggY2hhbmdlcyB0byBsb2NhbFN0b3JhZ2UuLi5cclxuICAgIHRoaXMub25TdWJqZWN0Lm5leHQoeyBrZXk6IGtleSwgdmFsdWU6IGRhdGEgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0U3RvcmVkKGtleTogc3RyaW5nLCBzdG9yYWdlVHlwZT86IFN0b3JhZ2VUeXBlKTogYW55IHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSkpO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xlYXIoa2V5LCBzdG9yYWdlVHlwZT86IFN0b3JhZ2VUeXBlKSB7XHJcbiAgICBsZXQgc3RvcmFnZTogU3RvcmFnZVR5cGUgPSBzdG9yYWdlVHlwZSB8fCBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2U7XHJcbiAgICBpZiAoc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdGhlIGxvY2FsIGFwcGxpY2F0aW9uIGRvZXNuJ3Qgc2VlbSB0byBjYXRjaCBjaGFuZ2VzIHRvIGxvY2FsU3RvcmFnZS4uLlxyXG4gICAgdGhpcy5vblN1YmplY3QubmV4dCh7IGtleToga2V5LCB2YWx1ZTogbnVsbCB9KTtcclxuICB9XHJcblxyXG5cclxuICBwcml2YXRlIHN0YXJ0KCk6IHZvaWQge1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3N0b3JhZ2UnLCB0aGlzLnN0b3JhZ2VFdmVudExpc3RlbmVyLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdG9yYWdlRXZlbnRMaXN0ZW5lcihldmVudDogU3RvcmFnZUV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQuc3RvcmFnZUFyZWEgPT0gbG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgIGxldCB2O1xyXG4gICAgICB0cnkgeyB2ID0gSlNPTi5wYXJzZShldmVudC5uZXdWYWx1ZSk7IH1cclxuICAgICAgY2F0Y2ggKGUpIHsgdiA9IGV2ZW50Lm5ld1ZhbHVlOyB9XHJcbiAgICAgIHRoaXMub25TdWJqZWN0Lm5leHQoeyBrZXk6IGV2ZW50LmtleSwgdmFsdWU6IHYgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHN0b3AoKTogdm9pZCB7XHJcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc3RvcmFnZScsIHRoaXMuc3RvcmFnZUV2ZW50TGlzdGVuZXIuYmluZCh0aGlzKSk7XHJcbiAgICB0aGlzLm9uU3ViamVjdC5jb21wbGV0ZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=