/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class ValidationService {
    /**
     * @param {?} control
     * @return {?}
     */
    static tcValidate(control) {
        /** @type {?} */
        let value = control.value;
        // event.target.value;
        /** @type {?} */
        let isZero = false;
        value = String(value);
        // Sadece rakamlardan oluşup oluşmadığını ve 11 haneli olup olmadığını kontrol ediyoruz.
        /** @type {?} */
        const isEleven = /^[0-9]{11}$/.test(value);
        // İlk rakamın 0 ile başlama kontrolü
        /** @type {?} */
        const firstValue = value.substr(0, 1);
        if (firstValue === '0') {
            isZero = true;
        }
        else {
            isZero = false;
        }
        /** @type {?} */
        let totalX = 0;
        for (let i = 0; i < 10; i++) {
            totalX += Number(value.substr(i, 1));
        }
        // İlk 10 hanesinin toplamınınn 11. haneyi verme kontrolü
        /** @type {?} */
        const isRuleX = String(totalX % 10) === value.substr(10, 1);
        // tek ve çift hanelerin toplamlarının atanacağı degişkenleri tanımlıyoruz
        /** @type {?} */
        let totalY1 = 0;
        /** @type {?} */
        let totalY2 = 0;
        // tek hanelerdeki sayıları toplar
        for (let j = 0; j < 10; j += 2) {
            totalY1 += Number(value.substr(j, 1));
        }
        // çift hanelerdeki sayıları toplar
        for (let k = 1; k < 9; k += 2) {
            totalY2 += Number(value.substr(k, 1));
        }
        /** @type {?} */
        const isRuleY = String(((totalY1 * 7) - totalY2) % 10) === value.substr(9, 1);
        /** @type {?} */
        const deger = isEleven && !isZero && isRuleX && isRuleY;
        // this.isTrue = deger === true ? true : false;
        if (!(deger === true ? true : false)) {
            return { 'identificationNumberIsNotValid': true };
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} control
     * @return {?}
     */
    static spaceControl(control) {
        /** @type {?} */
        let value = control.value;
        value = String(value);
        /** @type {?} */
        let usingSpace = false;
        if (value === '' || value.indexOf(' ') > -1) {
            usingSpace = true;
        }
        if (usingSpace) {
            return { 'usingSpaceValid': true };
        }
        else {
            return null;
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS92YWxpZGF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBLE1BQU0sT0FBTyxpQkFBaUI7Ozs7O0lBRTVCLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBd0I7O1lBQ3BDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSzs7O1lBQ3JCLE1BQU0sR0FBRyxLQUFLO1FBQ2xCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7OztjQUVoQixRQUFRLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7OztjQUdwQyxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JDLElBQUksVUFBVSxLQUFLLEdBQUcsRUFBRTtZQUN0QixNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNMLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDaEI7O1lBRUcsTUFBTSxHQUFHLENBQUM7UUFDZCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzNCLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0Qzs7O2NBRUssT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDOzs7WUFFdkQsT0FBTyxHQUFHLENBQUM7O1lBQ1gsT0FBTyxHQUFHLENBQUM7UUFFZixrQ0FBa0M7UUFDbEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzlCLE9BQU8sSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN2QztRQUVELG1DQUFtQztRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDN0IsT0FBTyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3ZDOztjQUVLLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7O2NBQ3ZFLEtBQUssR0FBRyxRQUFRLElBQUksQ0FBQyxNQUFNLElBQUksT0FBTyxJQUFJLE9BQU87UUFDdkQsK0NBQStDO1FBRS9DLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDcEMsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLElBQUksRUFBRSxDQUFDO1NBQ25EO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQXdCOztZQUN0QyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUs7UUFDekIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs7WUFDbEIsVUFBVSxHQUFHLEtBQUs7UUFFdEIsSUFBSSxLQUFLLEtBQUssRUFBRSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDM0MsVUFBVSxHQUFHLElBQUksQ0FBQztTQUNuQjtRQUNELElBQUksVUFBVSxFQUFFO1lBQ2QsT0FBTyxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBRSxDQUFDO1NBQ3BDO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVmFsaWRhdGlvbkVycm9ycywgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFZhbGlkYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgc3RhdGljIHRjVmFsaWRhdGUoY29udHJvbDogQWJzdHJhY3RDb250cm9sKTogVmFsaWRhdGlvbkVycm9ycyB8IG51bGwge1xyXG4gICAgbGV0IHZhbHVlID0gY29udHJvbC52YWx1ZTsgLy8gZXZlbnQudGFyZ2V0LnZhbHVlO1xyXG4gICAgbGV0IGlzWmVybyA9IGZhbHNlO1xyXG4gICAgdmFsdWUgPSBTdHJpbmcodmFsdWUpO1xyXG4gICAgLy8gU2FkZWNlIHJha2FtbGFyZGFuIG9sdcWfdXAgb2x1xZ9tYWTEscSfxLFuxLEgdmUgMTEgaGFuZWxpIG9sdXAgb2xtYWTEscSfxLFuxLEga29udHJvbCBlZGl5b3J1ei5cclxuICAgIGNvbnN0IGlzRWxldmVuID0gL15bMC05XXsxMX0kLy50ZXN0KHZhbHVlKTtcclxuXHJcbiAgICAvLyDEsGxrIHJha2FtxLFuIDAgaWxlIGJhxZ9sYW1hIGtvbnRyb2zDvFxyXG4gICAgY29uc3QgZmlyc3RWYWx1ZSA9IHZhbHVlLnN1YnN0cigwLCAxKTtcclxuICAgIGlmIChmaXJzdFZhbHVlID09PSAnMCcpIHtcclxuICAgICAgaXNaZXJvID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlzWmVybyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB0b3RhbFggPSAwO1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCAxMDsgaSsrKSB7XHJcbiAgICAgIHRvdGFsWCArPSBOdW1iZXIodmFsdWUuc3Vic3RyKGksIDEpKTtcclxuICAgIH1cclxuICAgIC8vIMSwbGsgMTAgaGFuZXNpbmluIHRvcGxhbcSxbsSxbm4gMTEuIGhhbmV5aSB2ZXJtZSBrb250cm9sw7xcclxuICAgIGNvbnN0IGlzUnVsZVggPSBTdHJpbmcodG90YWxYICUgMTApID09PSB2YWx1ZS5zdWJzdHIoMTAsIDEpO1xyXG4gICAgLy8gdGVrIHZlIMOnaWZ0IGhhbmVsZXJpbiB0b3BsYW1sYXLEsW7EsW4gYXRhbmFjYcSfxLEgZGVnacWfa2VubGVyaSB0YW7EsW1sxLF5b3J1elxyXG4gICAgbGV0IHRvdGFsWTEgPSAwO1xyXG4gICAgbGV0IHRvdGFsWTIgPSAwO1xyXG5cclxuICAgIC8vIHRlayBoYW5lbGVyZGVraSBzYXnEsWxhcsSxIHRvcGxhclxyXG4gICAgZm9yIChsZXQgaiA9IDA7IGogPCAxMDsgaiArPSAyKSB7XHJcbiAgICAgIHRvdGFsWTEgKz0gTnVtYmVyKHZhbHVlLnN1YnN0cihqLCAxKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gw6dpZnQgaGFuZWxlcmRla2kgc2F5xLFsYXLEsSB0b3BsYXJcclxuICAgIGZvciAobGV0IGsgPSAxOyBrIDwgOTsgayArPSAyKSB7XHJcbiAgICAgIHRvdGFsWTIgKz0gTnVtYmVyKHZhbHVlLnN1YnN0cihrLCAxKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgaXNSdWxlWSA9IFN0cmluZygoKHRvdGFsWTEgKiA3KSAtIHRvdGFsWTIpICUgMTApID09PSB2YWx1ZS5zdWJzdHIoOSwgMSk7XHJcbiAgICBjb25zdCBkZWdlciA9IGlzRWxldmVuICYmICFpc1plcm8gJiYgaXNSdWxlWCAmJiBpc1J1bGVZO1xyXG4gICAgLy8gdGhpcy5pc1RydWUgPSBkZWdlciA9PT0gdHJ1ZSA/IHRydWUgOiBmYWxzZTtcclxuXHJcbiAgICBpZiAoIShkZWdlciA9PT0gdHJ1ZSA/IHRydWUgOiBmYWxzZSkpIHtcclxuICAgICAgcmV0dXJuIHsgJ2lkZW50aWZpY2F0aW9uTnVtYmVySXNOb3RWYWxpZCc6IHRydWUgfTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc3RhdGljIHNwYWNlQ29udHJvbChjb250cm9sOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHwgbnVsbCB7XHJcbiAgICBsZXQgdmFsdWUgPSBjb250cm9sLnZhbHVlO1xyXG4gICAgdmFsdWUgPSBTdHJpbmcodmFsdWUpO1xyXG4gICAgbGV0IHVzaW5nU3BhY2UgPSBmYWxzZTtcclxuXHJcbiAgICBpZiAodmFsdWUgPT09ICcnIHx8IHZhbHVlLmluZGV4T2YoJyAnKSA+IC0xKSB7XHJcbiAgICAgIHVzaW5nU3BhY2UgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHVzaW5nU3BhY2UpIHtcclxuICAgICAgcmV0dXJuIHsgJ3VzaW5nU3BhY2VWYWxpZCc6IHRydWUgfTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=