/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateHelperService } from './translate.helper.service';
// declare let toastr: any;
export class ToastrUtilsService {
    /**
     * @param {?} toastr
     * @param {?} translate
     */
    constructor(toastr, translate) {
        this.toastr = toastr;
        this.translate = translate;
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    success(message, title) {
        this.translateValue(message, 'success', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    info(message, title) {
        this.translateValue(message, 'info', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    warning(message, title) {
        this.translateValue(message, 'warning', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    error(message, title) {
        this.translateValue(message, 'error', title);
    }
    /**
     * @param {?} message
     * @param {?} messageType
     * @param {?=} title
     * @return {?}
     */
    translateValue(message, messageType, title) {
        title = title || '';
        message = message || '';
        this.translate.get([message, title]).subscribe((translatedResult) => {
            if (messageType === 'success') {
                this.toastr.success(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'info') {
                this.toastr.info(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'warning') {
                this.toastr.warning(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'error') {
                this.toastr.error(translatedResult[message] || message, translatedResult[title] || title);
            }
        }, (error) => {
        });
    }
}
ToastrUtilsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ToastrUtilsService.ctorParameters = () => [
    { type: ToastrService },
    { type: TranslateHelperService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ToastrUtilsService.prototype.toastr;
    /**
     * @type {?}
     * @private
     */
    ToastrUtilsService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3RyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY3VzdG9tL3RvYXN0ci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDM0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7O0FBS3BFLE1BQU0sT0FBTyxrQkFBa0I7Ozs7O0lBQzdCLFlBQW9CLE1BQXFCLEVBQVUsU0FBaUM7UUFBaEUsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQXdCO0lBQ3BGLENBQUM7Ozs7OztJQUVELE9BQU8sQ0FBQyxPQUFlLEVBQUUsS0FBYztRQUNyQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7O0lBRUQsSUFBSSxDQUFDLE9BQWUsRUFBRSxLQUFjO1FBQ2xDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7Ozs7SUFFRCxPQUFPLENBQUMsT0FBZSxFQUFFLEtBQWM7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7OztJQUVELEtBQUssQ0FBQyxPQUFlLEVBQUUsS0FBYztRQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7OztJQUVELGNBQWMsQ0FBQyxPQUFlLEVBQUUsV0FBbUIsRUFBRSxLQUFjO1FBQ2pFLEtBQUssR0FBRyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3BCLE9BQU8sR0FBRyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtZQUNsRSxJQUFJLFdBQVcsS0FBSyxTQUFTLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQzthQUM3RjtpQkFBTSxJQUFJLFdBQVcsS0FBSyxNQUFNLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQzthQUMxRjtpQkFBTSxJQUFJLFdBQVcsS0FBSyxTQUFTLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQzthQUM3RjtpQkFBTSxJQUFJLFdBQVcsS0FBSyxPQUFPLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQzthQUMzRjtRQUNILENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO1FBQ2IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7WUFwQ0YsVUFBVTs7OztZQUxGLGFBQWE7WUFDYixzQkFBc0I7Ozs7Ozs7SUFNakIsb0NBQTZCOzs7OztJQUFFLHVDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVG9hc3RyU2VydmljZSB9IGZyb20gJ25neC10b2FzdHInO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi90cmFuc2xhdGUuaGVscGVyLnNlcnZpY2UnO1xyXG5cclxuLy8gZGVjbGFyZSBsZXQgdG9hc3RyOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBUb2FzdHJVdGlsc1NlcnZpY2Uge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdG9hc3RyOiBUb2FzdHJTZXJ2aWNlLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlSGVscGVyU2VydmljZSkge1xyXG4gIH1cclxuXHJcbiAgc3VjY2VzcyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICdzdWNjZXNzJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgaW5mbyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICdpbmZvJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgd2FybmluZyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICd3YXJuaW5nJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgZXJyb3IobWVzc2FnZTogc3RyaW5nLCB0aXRsZT86IHN0cmluZykge1xyXG4gICAgdGhpcy50cmFuc2xhdGVWYWx1ZShtZXNzYWdlLCAnZXJyb3InLCB0aXRsZSk7XHJcbiAgfVxyXG5cclxuICB0cmFuc2xhdGVWYWx1ZShtZXNzYWdlOiBzdHJpbmcsIG1lc3NhZ2VUeXBlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aXRsZSA9IHRpdGxlIHx8ICcnO1xyXG4gICAgbWVzc2FnZSA9IG1lc3NhZ2UgfHwgJyc7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoW21lc3NhZ2UsIHRpdGxlXSkuc3Vic2NyaWJlKCh0cmFuc2xhdGVkUmVzdWx0KSA9PiB7XHJcbiAgICAgIGlmIChtZXNzYWdlVHlwZSA9PT0gJ3N1Y2Nlc3MnKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIuc3VjY2Vzcyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ2luZm8nKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIuaW5mbyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ3dhcm5pbmcnKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIud2FybmluZyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ2Vycm9yJykge1xyXG4gICAgICAgIHRoaXMudG9hc3RyLmVycm9yKHRyYW5zbGF0ZWRSZXN1bHRbbWVzc2FnZV0gfHwgbWVzc2FnZSwgdHJhbnNsYXRlZFJlc3VsdFt0aXRsZV0gfHwgdGl0bGUpO1xyXG4gICAgICB9XHJcbiAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=