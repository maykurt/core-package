/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class BreadcrumbService {
    constructor() {
        this.breadcrumbsWithCustomLabels = [];
    }
    /**
     * @param {?} label
     * @param {?} url
     * @param {?} customLabel
     * @return {?}
     */
    addCustomLabel(label, url, customLabel) {
        /** @type {?} */
        const indexOfBreadcrumbItem = this.breadcrumbsWithCustomLabels.findIndex(x => x.label === label && x.url === url);
        if (indexOfBreadcrumbItem !== -1) {
            this.breadcrumbsWithCustomLabels[indexOfBreadcrumbItem]['customLabel'] = customLabel;
        }
        else {
            /** @type {?} */
            const breadcrumb = {
                label,
                url,
                customLabel
            };
            this.breadcrumbsWithCustomLabels.push(breadcrumb);
        }
        // update the current list after send
        if (this.breadcrumbs) {
            /** @type {?} */
            const currentBreadcrumbItem = this.breadcrumbs.find(x => x.label === label && x.url === url);
            if (currentBreadcrumbItem) {
                currentBreadcrumbItem['customLabel'] = customLabel;
            }
        }
    }
}
BreadcrumbService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BreadcrumbService.ctorParameters = () => [];
/** @nocollapse */ BreadcrumbService.ngInjectableDef = i0.defineInjectable({ factory: function BreadcrumbService_Factory() { return new BreadcrumbService(); }, token: BreadcrumbService, providedIn: "root" });
if (false) {
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbs;
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbsWithCustomLabels;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9icmVhZGNydW1iLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxpQkFBaUI7SUFLNUI7UUFDRSxJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFDO0lBQ3hDLENBQUM7Ozs7Ozs7SUFFRCxjQUFjLENBQUMsS0FBYSxFQUFFLEdBQVcsRUFBRSxXQUFtQjs7Y0FDdEQscUJBQXFCLEdBQVcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDO1FBQ3pILElBQUkscUJBQXFCLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLHFCQUFxQixDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUcsV0FBVyxDQUFDO1NBQ3RGO2FBQU07O2tCQUNDLFVBQVUsR0FBUTtnQkFDdEIsS0FBSztnQkFDTCxHQUFHO2dCQUNILFdBQVc7YUFDWjtZQUNELElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbkQ7UUFDRCxxQ0FBcUM7UUFDckMsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOztrQkFDZCxxQkFBcUIsR0FBUSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDO1lBQ2pHLElBQUkscUJBQXFCLEVBQUU7Z0JBQ3pCLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxHQUFHLFdBQVcsQ0FBQzthQUNwRDtTQUNGO0lBQ0gsQ0FBQzs7O1lBL0JGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7OztJQUVDLHdDQUEwQjs7SUFFMUIsd0RBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQnJlYWRjcnVtYlNlcnZpY2Uge1xyXG4gIHB1YmxpYyBicmVhZGNydW1iczogYW55W107XHJcblxyXG4gIHB1YmxpYyBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbHM6IGFueVtdO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWxzID0gW107XHJcbiAgfVxyXG5cclxuICBhZGRDdXN0b21MYWJlbChsYWJlbDogc3RyaW5nLCB1cmw6IHN0cmluZywgY3VzdG9tTGFiZWw6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgY29uc3QgaW5kZXhPZkJyZWFkY3J1bWJJdGVtOiBudW1iZXIgPSB0aGlzLmJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVscy5maW5kSW5kZXgoeCA9PiB4LmxhYmVsID09PSBsYWJlbCAmJiB4LnVybCA9PT0gdXJsKTtcclxuICAgIGlmIChpbmRleE9mQnJlYWRjcnVtYkl0ZW0gIT09IC0xKSB7XHJcbiAgICAgIHRoaXMuYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWxzW2luZGV4T2ZCcmVhZGNydW1iSXRlbV1bJ2N1c3RvbUxhYmVsJ10gPSBjdXN0b21MYWJlbDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGJyZWFkY3J1bWI6IGFueSA9IHtcclxuICAgICAgICBsYWJlbCxcclxuICAgICAgICB1cmwsXHJcbiAgICAgICAgY3VzdG9tTGFiZWxcclxuICAgICAgfTtcclxuICAgICAgdGhpcy5icmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbHMucHVzaChicmVhZGNydW1iKTtcclxuICAgIH1cclxuICAgIC8vIHVwZGF0ZSB0aGUgY3VycmVudCBsaXN0IGFmdGVyIHNlbmRcclxuICAgIGlmICh0aGlzLmJyZWFkY3J1bWJzKSB7XHJcbiAgICAgIGNvbnN0IGN1cnJlbnRCcmVhZGNydW1iSXRlbTogYW55ID0gdGhpcy5icmVhZGNydW1icy5maW5kKHggPT4geC5sYWJlbCA9PT0gbGFiZWwgJiYgeC51cmwgPT09IHVybCk7XHJcbiAgICAgIGlmIChjdXJyZW50QnJlYWRjcnVtYkl0ZW0pIHtcclxuICAgICAgICBjdXJyZW50QnJlYWRjcnVtYkl0ZW1bJ2N1c3RvbUxhYmVsJ10gPSBjdXN0b21MYWJlbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=