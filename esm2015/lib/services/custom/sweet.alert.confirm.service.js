/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { ConfirmDialogOperationType, AlertType } from '../../enums/index';
import { TranslateHelperService } from './translate.helper.service';
export class SweetAlertService {
    /**
     * @param {?} translate
     */
    constructor(translate) {
        this.translate = translate;
        this._swal = Swal;
        // this.translate.setInitialLanguage('tr');
    }
    // openConfirmPopup() {
    //   const swalTitle = 'ConfirmDialogAreYouSure';
    //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
    //   const swalConfirmBtnText = 'ConfirmButtonText';
    //   const swalCancelBtnText = 'CancelModalButton';
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
    //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
    //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
    //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
    //   const swalCancelBtnText = 'CancelModalButton';
    //   const swalType = type || AlertType.Warning;
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
    // }
    /**
     * @param {?=} title
     * @param {?=} text
     * @param {?=} type
     * @param {?=} confirmButtonText
     * @param {?=} showCancelButton
     * @return {?}
     */
    preventUnsavedChangedPopup(title, text, type, confirmButtonText, showCancelButton = true) {
        /** @type {?} */
        const swalTitle = title ? title : 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        const swalText = text ? text : 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning, showCancelButton);
    }
    /**
     * @param {?} confirmDialogOperationType
     * @param {?=} confirmDialogSettings
     * @return {?}
     */
    manageRequestOperationWithConfirm(confirmDialogOperationType, confirmDialogSettings) {
        switch (confirmDialogOperationType) {
            case ConfirmDialogOperationType.Add:
                return this.getAddOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Update:
                return this.getUpdateOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Delete:
                return this.getDeleteOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Undo:
                return this.getUndoOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Cancel:
                return this.getCancelOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Refresh:
                return this.getRefreshOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Process:
                return this.getProcessOperationConfirmPopupWithConfirm(confirmDialogSettings);
            default:
                break;
        }
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getProcessOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogProcessCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogProcessInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogProcessOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogProcessCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getAddOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getUpdateOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getDeleteOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogDeleteRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogDeleteRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogDeleteRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogDeleteRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getUndoOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogNotAbleToRevertRecordCapiton';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogNotAbleToRevertRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogNotAbleToRevertRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogNotAbleToRevertRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getCancelOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getRefreshOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogRefreshCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogRefreshInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogRefreshsOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogRefreshCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @param {?} result
     * @param {?=} settings
     * @return {?}
     */
    confirmOperationPopup(result, settings = {}) {
        if (result.value) {
            /** @type {?} */
            const swalTitle = settings.title || 'Deleted!';
            /** @type {?} */
            const swalText = settings.text || 'Your file has been deleted.';
            /** @type {?} */
            const swalType = settings.type || AlertType.Success;
            return this.openCompletedConfirmDialog(swalTitle, swalText, swalType);
        }
        else if (result.dismiss === this._swal.DismissReason.cancel) {
            return this.openCompletedConfirmDialog('Cancelled', 'OperationCanceled', AlertType.Error);
        }
    }
    // loadingSweetAlert() {
    //   swal.showLoading();
    // }
    //
    // cancelLoadingSweetAlert() {
    //   swal.hideLoading();
    // }
    // cancelEditFormConfirmModal() {
    //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
    //   const swalText = 'ConfirmDialogLoseChangesInfo';
    //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
    //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?=} swalType
     * @return {?}
     */
    openCompletedConfirmDialog(swalTitle, swalText, swalType = AlertType.Info) {
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalType = swalType || AlertType.Info;
        return this.translate.get([swalTitle, swalText])
            .pipe(flatMap(translatedResult => from(Swal.fire(translatedResult[swalTitle], translatedResult[swalText], swalType)), (translatedResult, result) => {
            /** @type {?} */
            const alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?} swalConfirmBtnText
     * @param {?} swalCancelBtnText
     * @param {?} swalType
     * @param {?=} showCancelButton
     * @return {?}
     */
    openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton = true) {
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalConfirmBtnText = swalConfirmBtnText || '';
        swalCancelBtnText = swalCancelBtnText || '';
        return this.translate.get([swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText])
            .pipe(flatMap(translatedResult => from(Swal.fire({
            title: translatedResult[swalTitle],
            text: translatedResult[swalText],
            type: swalType ? swalType : AlertType.Warning,
            confirmButtonText: translatedResult[swalConfirmBtnText],
            showCancelButton: showCancelButton,
            cancelButtonText: translatedResult[swalCancelBtnText],
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-info mr-20',
            cancelButtonClass: 'btn btn-danger',
            customClass: 'sweet-confirm-dialog',
        })), (translatedResult, result) => {
            /** @type {?} */
            const alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    }
}
SweetAlertService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SweetAlertService.ctorParameters = () => [
    { type: TranslateHelperService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype._swal;
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype.swalOptions;
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dlZXQuYWxlcnQuY29uZmlybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9zd2VldC5hbGVydC5jb25maXJtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsT0FBTyxJQUFJLE1BQU0sYUFBYSxDQUFDO0FBRS9CLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUIsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pDLE9BQU8sRUFBc0IsMEJBQTBCLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFOUYsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHcEUsTUFBTSxPQUFPLGlCQUFpQjs7OztJQUs1QixZQUFvQixTQUFpQztRQUFqQyxjQUFTLEdBQVQsU0FBUyxDQUF3QjtRQUg3QyxVQUFLLEdBQVEsSUFBSSxDQUFDO1FBSXhCLDJDQUEyQztJQUM3QyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXFCRCwwQkFBMEIsQ0FBQyxLQUFjLEVBQUUsSUFBYSxFQUFFLElBQWdCLEVBQUUsaUJBQTBCLEVBQUUsZ0JBQWdCLEdBQUcsSUFBSTs7Y0FDdkgsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7O2NBQzdELFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsOEJBQThCOztjQUN2RCxrQkFBa0IsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLDRCQUE0Qjs7Y0FDekYsaUJBQWlCLEdBQUcsZ0NBQWdDO1FBQzFELE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pJLENBQUM7Ozs7OztJQUVELGlDQUFpQyxDQUFDLDBCQUFzRCxFQUFFLHFCQUE2QztRQUNySSxRQUFRLDBCQUEwQixFQUFFO1lBQ2xDLEtBQUssMEJBQTBCLENBQUMsR0FBRztnQkFDakMsT0FBTyxJQUFJLENBQUMsc0NBQXNDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM1RSxLQUFLLDBCQUEwQixDQUFDLE1BQU07Z0JBQ3BDLE9BQU8sSUFBSSxDQUFDLHlDQUF5QyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsS0FBSywwQkFBMEIsQ0FBQyxNQUFNO2dCQUNwQyxPQUFPLElBQUksQ0FBQyx5Q0FBeUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLEtBQUssMEJBQTBCLENBQUMsSUFBSTtnQkFDbEMsT0FBTyxJQUFJLENBQUMsdUNBQXVDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxLQUFLLDBCQUEwQixDQUFDLE1BQU07Z0JBQ3BDLE9BQU8sSUFBSSxDQUFDLHlDQUF5QyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsS0FBSywwQkFBMEIsQ0FBQyxPQUFPO2dCQUNyQyxPQUFPLElBQUksQ0FBQywwQ0FBMEMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hGLEtBQUssMEJBQTBCLENBQUMsT0FBTztnQkFDckMsT0FBTyxJQUFJLENBQUMsMENBQTBDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNoRjtnQkFDRSxNQUFNO1NBQ1Q7SUFDSCxDQUFDOzs7Ozs7SUFFTywwQ0FBMEMsQ0FBQyxXQUFrQyxFQUFFOztjQUMvRSxTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssSUFBSSw2QkFBNkI7O2NBQzNELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDBCQUEwQjs7Y0FDdEQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLGNBQWMsSUFBSSx3QkFBd0I7O2NBQ3hFLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLElBQUksNEJBQTRCO1FBQ2hGLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVHLENBQUM7Ozs7OztJQUVPLHNDQUFzQyxDQUFDLFdBQWtDLEVBQUU7O2NBQzNFLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxJQUFJLGdDQUFnQzs7Y0FDOUQsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLElBQUksNkJBQTZCOztjQUN6RCxrQkFBa0IsR0FBRyxRQUFRLENBQUMsY0FBYyxJQUFJLDJCQUEyQjs7Y0FDM0UsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLGFBQWEsSUFBSSwrQkFBK0I7UUFFbkYsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUcsQ0FBQzs7Ozs7O0lBRU8seUNBQXlDLENBQUMsV0FBa0MsRUFBRTs7Y0FDOUUsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksZ0NBQWdDOztjQUM5RCxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksSUFBSSw2QkFBNkI7O2NBQ3pELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLElBQUksMkJBQTJCOztjQUMzRSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsYUFBYSxJQUFJLCtCQUErQjtRQUVuRixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1RyxDQUFDOzs7Ozs7SUFFTyx5Q0FBeUMsQ0FBQyxXQUFrQyxFQUFFOztjQUM5RSxTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssSUFBSSxrQ0FBa0M7O2NBQ2hFLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLCtCQUErQjs7Y0FDM0Qsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLGNBQWMsSUFBSSw2QkFBNkI7O2NBQzdFLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLElBQUksaUNBQWlDO1FBQ3JGLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9HLENBQUM7Ozs7OztJQUVPLHVDQUF1QyxDQUFDLFdBQWtDLEVBQUU7O2NBQzVFLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxJQUFJLDJDQUEyQzs7Y0FDekUsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLElBQUksd0NBQXdDOztjQUNwRSxrQkFBa0IsR0FBRyxRQUFRLENBQUMsY0FBYyxJQUFJLHNDQUFzQzs7Y0FDdEYsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLGFBQWEsSUFBSSwwQ0FBMEM7UUFDOUYsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0csQ0FBQzs7Ozs7O0lBRU8seUNBQXlDLENBQUMsV0FBa0MsRUFBRTs7Y0FDOUUsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksaUNBQWlDOztjQUMvRCxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksSUFBSSw4QkFBOEI7O2NBQzFELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLElBQUksNEJBQTRCOztjQUM1RSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsYUFBYSxJQUFJLGdDQUFnQztRQUNwRixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRyxDQUFDOzs7Ozs7SUFFTywwQ0FBMEMsQ0FBQyxXQUFrQyxFQUFFOztjQUMvRSxTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssSUFBSSw2QkFBNkI7O2NBQzNELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDBCQUEwQjs7Y0FDdEQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLGNBQWMsSUFBSSx5QkFBeUI7O2NBQ3pFLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLElBQUksNEJBQTRCO1FBQ2hGLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9HLENBQUM7Ozs7OztJQUVELHFCQUFxQixDQUFDLE1BQU0sRUFBRSxXQUFnQixFQUFFO1FBQzlDLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTs7a0JBQ1YsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksVUFBVTs7a0JBQ3hDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDZCQUE2Qjs7a0JBQ3pELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxPQUFPO1lBRW5ELE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FFdkU7YUFBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQzdELE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0Y7SUFDSCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBa0JPLDBCQUEwQixDQUFDLFNBQWlCLEVBQUUsUUFBZ0IsRUFBRSxXQUFzQixTQUFTLENBQUMsSUFBSTtRQUMxRyxTQUFTLEdBQUcsU0FBUyxJQUFJLEVBQUUsQ0FBQztRQUM1QixRQUFRLEdBQUcsUUFBUSxJQUFJLEVBQUUsQ0FBQztRQUMxQixRQUFRLEdBQUcsUUFBUSxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDdEMsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUNsSCxDQUFDLGdCQUFnQixFQUFFLE1BQXdCLEVBQUUsRUFBRTs7a0JBQ3ZDLFdBQVcsR0FBZ0IsRUFBRTtZQUNuQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hCLFdBQVcsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzthQUNsQztZQUNELElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsV0FBVyxDQUFDLE9BQU8sR0FBRyxtQkFBb0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBQSxDQUFDO2FBQ3JFO1lBQ0QsT0FBTyxXQUFXLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7Ozs7O0lBRU8saUJBQWlCLENBQ3ZCLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLGtCQUEwQixFQUMxQixpQkFBeUIsRUFDekIsUUFBbUIsRUFDbkIsbUJBQTRCLElBQUk7UUFDaEMsU0FBUyxHQUFHLFNBQVMsSUFBSSxFQUFFLENBQUM7UUFDNUIsUUFBUSxHQUFHLFFBQVEsSUFBSSxFQUFFLENBQUM7UUFDMUIsa0JBQWtCLEdBQUcsa0JBQWtCLElBQUksRUFBRSxDQUFDO1FBQzlDLGlCQUFpQixHQUFHLGlCQUFpQixJQUFJLEVBQUUsQ0FBQztRQUU1QyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2FBQ3BGLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQy9DLEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7WUFDbEMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUNoQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPO1lBQzdDLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO1lBQ3ZELGdCQUFnQixFQUFFLGdCQUFnQjtZQUNsQyxnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQztZQUNyRCxjQUFjLEVBQUUsS0FBSztZQUNyQixrQkFBa0IsRUFBRSxvQkFBb0I7WUFDeEMsaUJBQWlCLEVBQUUsZ0JBQWdCO1lBQ25DLFdBQVcsRUFBRSxzQkFBc0I7U0FDcEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxNQUF3QixFQUFFLEVBQUU7O2tCQUM1QyxXQUFXLEdBQWdCLEVBQUU7WUFDbkMsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO2dCQUNoQixXQUFXLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDbEM7WUFDRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQ2xCLFdBQVcsQ0FBQyxPQUFPLEdBQUcsbUJBQW9CLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUEsQ0FBQzthQUNyRTtZQUNELE9BQU8sV0FBVyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7WUFyTUYsVUFBVTs7OztZQUZGLHNCQUFzQjs7Ozs7OztJQUs3QixrQ0FBMEI7Ozs7O0lBQzFCLHdDQUF5Qjs7Ozs7SUFFYixzQ0FBeUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBbGVydFJlc3VsdCB9IGZyb20gJy4uLy4uL21vZGVscy9hbGVydC1yZXN1bHQubW9kZWwnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN3ZWV0QWxlcnRSZXN1bHQgfSBmcm9tICdzd2VldGFsZXJ0Mic7XHJcblxyXG5pbXBvcnQgU3dhbCBmcm9tICdzd2VldGFsZXJ0Mic7XHJcblxyXG5pbXBvcnQgeyBmcm9tIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGZsYXRNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IEFsZXJ0RGlzbWlzc1JlYXNvbiwgQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUsIEFsZXJ0VHlwZSB9IGZyb20gJy4uLy4uL2VudW1zL2luZGV4JztcclxuaW1wb3J0IHsgQ29uZmlybURpYWxvZ1NldHRpbmdzIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NvbmZpcm0tZGlhbG9nLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlSGVscGVyU2VydmljZSB9IGZyb20gJy4vdHJhbnNsYXRlLmhlbHBlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN3ZWV0QWxlcnRTZXJ2aWNlIHtcclxuXHJcbiAgcHJpdmF0ZSBfc3dhbDogYW55ID0gU3dhbDtcclxuICBwcml2YXRlIHN3YWxPcHRpb25zOiBhbnk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVIZWxwZXJTZXJ2aWNlKSB7XHJcbiAgICAvLyB0aGlzLnRyYW5zbGF0ZS5zZXRJbml0aWFsTGFuZ3VhZ2UoJ3RyJyk7XHJcbiAgfVxyXG5cclxuICAvLyBvcGVuQ29uZmlybVBvcHVwKCkge1xyXG4gIC8vICAgY29uc3Qgc3dhbFRpdGxlID0gJ0NvbmZpcm1EaWFsb2dBcmVZb3VTdXJlJztcclxuICAvLyAgIGNvbnN0IHN3YWxUZXh0ID0gJ0NvbmZpcm1EaWFsb2dOb3RBYmxlVG9SZXZlcnRSZWNvcmQnO1xyXG4gIC8vICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gJ0NvbmZpcm1CdXR0b25UZXh0JztcclxuICAvLyAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gJ0NhbmNlbE1vZGFsQnV0dG9uJztcclxuICAvL1xyXG4gIC8vICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLldhcm5pbmcpO1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gb3BlbkRpcnR5Rm9ybVBvcHVwKHRpdGxlPzogc3RyaW5nLCB0ZXh0Pzogc3RyaW5nLCB0eXBlPzogQWxlcnRUeXBlLCBjb25maXJtQnV0dG9uVGV4dD86IHN0cmluZywgc2hvd0NhbmNlbEJ1dHRvbiA9IHRydWUpIHtcclxuICAvLyAgIGNvbnN0IHN3YWxUaXRsZSA9IHRpdGxlID8gdGl0bGUgOiAnQ29uZmlybURpYWxvZ0FyZVlvdVN1cmUnO1xyXG4gIC8vICAgY29uc3Qgc3dhbFRleHQgPSB0ZXh0ID8gdGV4dCA6ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXMnO1xyXG4gIC8vICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gY29uZmlybUJ1dHRvblRleHQgPyBjb25maXJtQnV0dG9uVGV4dCA6ICdDb25maXJtQnV0dG9uQ29udGludWUnO1xyXG4gIC8vICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSAnQ2FuY2VsTW9kYWxCdXR0b24nO1xyXG4gIC8vICAgY29uc3Qgc3dhbFR5cGUgPSB0eXBlIHx8IEFsZXJ0VHlwZS5XYXJuaW5nO1xyXG4gIC8vXHJcbiAgLy8gICByZXR1cm4gdGhpcy5vcGVuQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0LCBzd2FsVHlwZSwgc2hvd0NhbmNlbEJ1dHRvbik7XHJcbiAgLy8gfVxyXG5cclxuICBwcmV2ZW50VW5zYXZlZENoYW5nZWRQb3B1cCh0aXRsZT86IHN0cmluZywgdGV4dD86IHN0cmluZywgdHlwZT86IEFsZXJ0VHlwZSwgY29uZmlybUJ1dHRvblRleHQ/OiBzdHJpbmcsIHNob3dDYW5jZWxCdXR0b24gPSB0cnVlKSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSB0aXRsZSA/IHRpdGxlIDogJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc0NhcHRpb24nO1xyXG4gICAgY29uc3Qgc3dhbFRleHQgPSB0ZXh0ID8gdGV4dCA6ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IGNvbmZpcm1CdXR0b25UZXh0ID8gY29uZmlybUJ1dHRvblRleHQgOiAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nLCBzaG93Q2FuY2VsQnV0dG9uKTtcclxuICB9XHJcblxyXG4gIG1hbmFnZVJlcXVlc3RPcGVyYXRpb25XaXRoQ29uZmlybShjb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZTogQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUsIGNvbmZpcm1EaWFsb2dTZXR0aW5ncz86IENvbmZpcm1EaWFsb2dTZXR0aW5ncykge1xyXG4gICAgc3dpdGNoIChjb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZSkge1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkFkZDpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRBZGRPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVwZGF0ZTpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRVcGRhdGVPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkRlbGV0ZTpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXREZWxldGVPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVuZG86XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VW5kb09wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKGNvbmZpcm1EaWFsb2dTZXR0aW5ncyk7XHJcbiAgICAgIGNhc2UgQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuQ2FuY2VsOlxyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENhbmNlbE9wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKGNvbmZpcm1EaWFsb2dTZXR0aW5ncyk7XHJcbiAgICAgIGNhc2UgQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuUmVmcmVzaDpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRSZWZyZXNoT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ1NldHRpbmdzKTtcclxuICAgICAgY2FzZSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5Qcm9jZXNzOlxyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb2Nlc3NPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRQcm9jZXNzT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ1Byb2Nlc3NDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1Byb2Nlc3NJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nUHJvY2Vzc09rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gc2V0dGluZ3MuQ2FuY2VsQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1Byb2Nlc3NDYW5jZWwnO1xyXG4gICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLkluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRBZGRPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShzZXR0aW5nczogQ29uZmlybURpYWxvZ1NldHRpbmdzID0ge30pIHtcclxuICAgIGNvbnN0IHN3YWxUaXRsZSA9IHNldHRpbmdzLlRpdGxlIHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZENhcHRpb24nO1xyXG4gICAgY29uc3Qgc3dhbFRleHQgPSBzZXR0aW5ncy5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZEluZm8nO1xyXG4gICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gc2V0dGluZ3MuQ29uZmlybUJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dTYXZlUmVjb3JkT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZENhbmNlbCc7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLkluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRVcGRhdGVPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShzZXR0aW5nczogQ29uZmlybURpYWxvZ1NldHRpbmdzID0ge30pIHtcclxuICAgIGNvbnN0IHN3YWxUaXRsZSA9IHNldHRpbmdzLlRpdGxlIHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZENhcHRpb24nO1xyXG4gICAgY29uc3Qgc3dhbFRleHQgPSBzZXR0aW5ncy5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZEluZm8nO1xyXG4gICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gc2V0dGluZ3MuQ29uZmlybUJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dTYXZlUmVjb3JkT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZENhbmNlbCc7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLkluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXREZWxldGVPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShzZXR0aW5nczogQ29uZmlybURpYWxvZ1NldHRpbmdzID0ge30pIHtcclxuICAgIGNvbnN0IHN3YWxUaXRsZSA9IHNldHRpbmdzLlRpdGxlIHx8ICdDb25maXJtRGlhbG9nRGVsZXRlUmVjb3JkQ2FwdGlvbic7XHJcbiAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLlRleHQgfHwgJ0NvbmZpcm1EaWFsb2dEZWxldGVSZWNvcmRJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nRGVsZXRlUmVjb3JkT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nRGVsZXRlUmVjb3JkQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0VW5kb09wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKHNldHRpbmdzOiBDb25maXJtRGlhbG9nU2V0dGluZ3MgPSB7fSkge1xyXG4gICAgY29uc3Qgc3dhbFRpdGxlID0gc2V0dGluZ3MuVGl0bGUgfHwgJ0NvbmZpcm1EaWFsb2dOb3RBYmxlVG9SZXZlcnRSZWNvcmRDYXBpdG9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ05vdEFibGVUb1JldmVydFJlY29yZEluZm8nO1xyXG4gICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gc2V0dGluZ3MuQ29uZmlybUJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dOb3RBYmxlVG9SZXZlcnRSZWNvcmRPayc7XHJcbiAgICBjb25zdCBzd2FsQ2FuY2VsQnRuVGV4dCA9IHNldHRpbmdzLkNhbmNlbEJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dOb3RBYmxlVG9SZXZlcnRSZWNvcmRDYW5jZWwnO1xyXG4gICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLldhcm5pbmcpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRDYW5jZWxPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShzZXR0aW5nczogQ29uZmlybURpYWxvZ1NldHRpbmdzID0ge30pIHtcclxuICAgIGNvbnN0IHN3YWxUaXRsZSA9IHNldHRpbmdzLlRpdGxlIHx8ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzSW5mbyc7XHJcbiAgICBjb25zdCBzd2FsQ29uZmlybUJ0blRleHQgPSBzZXR0aW5ncy5Db25maXJtQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNDYW5jZWwnO1xyXG4gICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLldhcm5pbmcpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRSZWZyZXNoT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ1JlZnJlc2hDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1JlZnJlc2hJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nUmVmcmVzaHNPayc7XHJcbiAgICBjb25zdCBzd2FsQ2FuY2VsQnRuVGV4dCA9IHNldHRpbmdzLkNhbmNlbEJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dSZWZyZXNoQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nKTtcclxuICB9XHJcblxyXG4gIGNvbmZpcm1PcGVyYXRpb25Qb3B1cChyZXN1bHQsIHNldHRpbmdzOiBhbnkgPSB7fSkge1xyXG4gICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG4gICAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy50aXRsZSB8fCAnRGVsZXRlZCEnO1xyXG4gICAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLnRleHQgfHwgJ1lvdXIgZmlsZSBoYXMgYmVlbiBkZWxldGVkLic7XHJcbiAgICAgIGNvbnN0IHN3YWxUeXBlID0gc2V0dGluZ3MudHlwZSB8fCBBbGVydFR5cGUuU3VjY2VzcztcclxuXHJcbiAgICAgIHJldHVybiB0aGlzLm9wZW5Db21wbGV0ZWRDb25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxUeXBlKTtcclxuXHJcbiAgICB9IGVsc2UgaWYgKHJlc3VsdC5kaXNtaXNzID09PSB0aGlzLl9zd2FsLkRpc21pc3NSZWFzb24uY2FuY2VsKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLm9wZW5Db21wbGV0ZWRDb25maXJtRGlhbG9nKCdDYW5jZWxsZWQnLCAnT3BlcmF0aW9uQ2FuY2VsZWQnLCBBbGVydFR5cGUuRXJyb3IpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gbG9hZGluZ1N3ZWV0QWxlcnQoKSB7XHJcbiAgLy8gICBzd2FsLnNob3dMb2FkaW5nKCk7XHJcbiAgLy8gfVxyXG4gIC8vXHJcbiAgLy8gY2FuY2VsTG9hZGluZ1N3ZWV0QWxlcnQoKSB7XHJcbiAgLy8gICBzd2FsLmhpZGVMb2FkaW5nKCk7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBjYW5jZWxFZGl0Rm9ybUNvbmZpcm1Nb2RhbCgpIHtcclxuICAvLyAgIGNvbnN0IHN3YWxUaXRsZSA9ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNDYXB0aW9uJztcclxuICAvLyAgIGNvbnN0IHN3YWxUZXh0ID0gJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc0luZm8nO1xyXG4gIC8vICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc09rJztcclxuICAvLyAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc0NhbmNlbCc7XHJcbiAgLy8gICByZXR1cm4gdGhpcy5vcGVuQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0LCBBbGVydFR5cGUuV2FybmluZyk7XHJcbiAgLy8gfVxyXG5cclxuICBwcml2YXRlIG9wZW5Db21wbGV0ZWRDb25maXJtRGlhbG9nKHN3YWxUaXRsZTogc3RyaW5nLCBzd2FsVGV4dDogc3RyaW5nLCBzd2FsVHlwZTogQWxlcnRUeXBlID0gQWxlcnRUeXBlLkluZm8pIHtcclxuICAgIHN3YWxUaXRsZSA9IHN3YWxUaXRsZSB8fCAnJztcclxuICAgIHN3YWxUZXh0ID0gc3dhbFRleHQgfHwgJyc7XHJcbiAgICBzd2FsVHlwZSA9IHN3YWxUeXBlIHx8IEFsZXJ0VHlwZS5JbmZvO1xyXG4gICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlLmdldChbc3dhbFRpdGxlLCBzd2FsVGV4dF0pXHJcbiAgICAgIC5waXBlKGZsYXRNYXAodHJhbnNsYXRlZFJlc3VsdCA9PiBmcm9tKFN3YWwuZmlyZSh0cmFuc2xhdGVkUmVzdWx0W3N3YWxUaXRsZV0sIHRyYW5zbGF0ZWRSZXN1bHRbc3dhbFRleHRdLCBzd2FsVHlwZSkpLFxyXG4gICAgICAgICh0cmFuc2xhdGVkUmVzdWx0LCByZXN1bHQ6IFN3ZWV0QWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgIGNvbnN0IGFsZXJ0UmVzdWx0OiBBbGVydFJlc3VsdCA9IHt9O1xyXG4gICAgICAgICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG4gICAgICAgICAgICBhbGVydFJlc3VsdC52YWx1ZSA9IHJlc3VsdC52YWx1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChyZXN1bHQuZGlzbWlzcykge1xyXG4gICAgICAgICAgICBhbGVydFJlc3VsdC5kaXNtaXNzID0gPEFsZXJ0RGlzbWlzc1JlYXNvbj5yZXN1bHQuZGlzbWlzcy50b1N0cmluZygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIGFsZXJ0UmVzdWx0O1xyXG4gICAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgb3BlbkNvbmZpcm1EaWFsb2coXHJcbiAgICBzd2FsVGl0bGU6IHN0cmluZyxcclxuICAgIHN3YWxUZXh0OiBzdHJpbmcsXHJcbiAgICBzd2FsQ29uZmlybUJ0blRleHQ6IHN0cmluZyxcclxuICAgIHN3YWxDYW5jZWxCdG5UZXh0OiBzdHJpbmcsXHJcbiAgICBzd2FsVHlwZTogQWxlcnRUeXBlLFxyXG4gICAgc2hvd0NhbmNlbEJ1dHRvbjogYm9vbGVhbiA9IHRydWUpIHtcclxuICAgIHN3YWxUaXRsZSA9IHN3YWxUaXRsZSB8fCAnJztcclxuICAgIHN3YWxUZXh0ID0gc3dhbFRleHQgfHwgJyc7XHJcbiAgICBzd2FsQ29uZmlybUJ0blRleHQgPSBzd2FsQ29uZmlybUJ0blRleHQgfHwgJyc7XHJcbiAgICBzd2FsQ2FuY2VsQnRuVGV4dCA9IHN3YWxDYW5jZWxCdG5UZXh0IHx8ICcnO1xyXG5cclxuICAgIHJldHVybiB0aGlzLnRyYW5zbGF0ZS5nZXQoW3N3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHRdKVxyXG4gICAgICAucGlwZShmbGF0TWFwKHRyYW5zbGF0ZWRSZXN1bHQgPT4gZnJvbShTd2FsLmZpcmUoe1xyXG4gICAgICAgIHRpdGxlOiB0cmFuc2xhdGVkUmVzdWx0W3N3YWxUaXRsZV0sXHJcbiAgICAgICAgdGV4dDogdHJhbnNsYXRlZFJlc3VsdFtzd2FsVGV4dF0sXHJcbiAgICAgICAgdHlwZTogc3dhbFR5cGUgPyBzd2FsVHlwZSA6IEFsZXJ0VHlwZS5XYXJuaW5nLFxyXG4gICAgICAgIGNvbmZpcm1CdXR0b25UZXh0OiB0cmFuc2xhdGVkUmVzdWx0W3N3YWxDb25maXJtQnRuVGV4dF0sXHJcbiAgICAgICAgc2hvd0NhbmNlbEJ1dHRvbjogc2hvd0NhbmNlbEJ1dHRvbixcclxuICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiB0cmFuc2xhdGVkUmVzdWx0W3N3YWxDYW5jZWxCdG5UZXh0XSxcclxuICAgICAgICBidXR0b25zU3R5bGluZzogZmFsc2UsXHJcbiAgICAgICAgY29uZmlybUJ1dHRvbkNsYXNzOiAnYnRuIGJ0bi1pbmZvIG1yLTIwJyxcclxuICAgICAgICBjYW5jZWxCdXR0b25DbGFzczogJ2J0biBidG4tZGFuZ2VyJyxcclxuICAgICAgICBjdXN0b21DbGFzczogJ3N3ZWV0LWNvbmZpcm0tZGlhbG9nJyxcclxuICAgICAgfSkpLCAodHJhbnNsYXRlZFJlc3VsdCwgcmVzdWx0OiBTd2VldEFsZXJ0UmVzdWx0KSA9PiB7XHJcbiAgICAgICAgY29uc3QgYWxlcnRSZXN1bHQ6IEFsZXJ0UmVzdWx0ID0ge307XHJcbiAgICAgICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG4gICAgICAgICAgYWxlcnRSZXN1bHQudmFsdWUgPSByZXN1bHQudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChyZXN1bHQuZGlzbWlzcykge1xyXG4gICAgICAgICAgYWxlcnRSZXN1bHQuZGlzbWlzcyA9IDxBbGVydERpc21pc3NSZWFzb24+cmVzdWx0LmRpc21pc3MudG9TdHJpbmcoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFsZXJ0UmVzdWx0O1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==