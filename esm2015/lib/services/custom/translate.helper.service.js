/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './storage.service';
import { Storage } from '../../enums/storage.enum';
export class TranslateHelperService {
    /**
     * @param {?} translate
     * @param {?} storageService
     */
    constructor(translate, storageService) {
        this.translate = translate;
        this.storageService = storageService;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    setInitialLanguage(lang) {
        this.translate.setDefaultLang(lang);
    }
    /**
     * @param {?} language
     * @return {?}
     */
    setSelectedLanguage(language) {
        this.translate.use(language);
        this.storageService.store(Storage.CurrentLanguage, language);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    instant(key) {
        /** @type {?} */
        const result = new Promise(resolve => {
            this.translate.get(key).toPromise();
        });
        return result;
    }
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return this.translate.get(key);
    }
}
TranslateHelperService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
TranslateHelperService.ctorParameters = () => [
    { type: TranslateService },
    { type: StorageService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslateHelperService.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    TranslateHelperService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLmhlbHBlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS90cmFuc2xhdGUuaGVscGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUluRCxNQUFNLE9BQU8sc0JBQXNCOzs7OztJQUVqQyxZQUNVLFNBQTJCLEVBQzNCLGNBQThCO1FBRDlCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUV4QyxDQUFDOzs7OztJQUVELGtCQUFrQixDQUFDLElBQUk7UUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxRQUFnQjtRQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLEdBQVc7O2NBQ1gsTUFBTSxHQUFHLElBQUksT0FBTyxDQUFTLE9BQU8sQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3RDLENBQUMsQ0FBQztRQUVGLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsR0FBRyxDQUFDLEdBQXNCO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7O1lBNUJGLFVBQVU7Ozs7WUFMRixnQkFBZ0I7WUFDaEIsY0FBYzs7Ozs7OztJQVFuQiwyQ0FBbUM7Ozs7O0lBQ25DLGdEQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJy4vc3RvcmFnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RvcmFnZSB9IGZyb20gJy4uLy4uL2VudW1zL3N0b3JhZ2UuZW51bSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVHJhbnNsYXRlSGVscGVyU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSkge1xyXG5cclxuICB9XHJcblxyXG4gIHNldEluaXRpYWxMYW5ndWFnZShsYW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5zZXREZWZhdWx0TGFuZyhsYW5nKTtcclxuICB9XHJcblxyXG4gIHNldFNlbGVjdGVkTGFuZ3VhZ2UobGFuZ3VhZ2U6IHN0cmluZykge1xyXG4gICAgdGhpcy50cmFuc2xhdGUudXNlKGxhbmd1YWdlKTtcclxuICAgIHRoaXMuc3RvcmFnZVNlcnZpY2Uuc3RvcmUoU3RvcmFnZS5DdXJyZW50TGFuZ3VhZ2UsIGxhbmd1YWdlKTtcclxuICB9XHJcblxyXG4gIGluc3RhbnQoa2V5OiBzdHJpbmcpIHtcclxuICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBQcm9taXNlPHN0cmluZz4ocmVzb2x2ZSA9PiB7XHJcbiAgICAgIHRoaXMudHJhbnNsYXRlLmdldChrZXkpLnRvUHJvbWlzZSgpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxuICB9XHJcblxyXG4gIGdldChrZXk6IHN0cmluZyB8IHN0cmluZ1tdKSB7XHJcbiAgICByZXR1cm4gdGhpcy50cmFuc2xhdGUuZ2V0KGtleSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==