/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export class AppConfig {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @return {?}
     */
    load() {
        return new Promise((resolve, reject) => {
            /** @type {?} */
            const http = this.injector.get(HttpClient);
            http.get('/assets/config/config.json')
                .toPromise()
                .then((response) => {
                AppConfig.settings = (/** @type {?} */ (response));
                resolve();
            }).catch((response) => {
                console.log(response);
                reject('Could not load file /assets/config.json');
            });
        });
    }
}
AppConfig.settings = (/** @type {?} */ ({}));
AppConfig.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AppConfig.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /** @type {?} */
    AppConfig.settings;
    /**
     * @type {?}
     * @private
     */
    AppConfig.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9jdXN0b20vYXBwLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBSWxELE1BQU0sT0FBTyxTQUFTOzs7O0lBRXBCLFlBQW9CLFFBQWtCO1FBQWxCLGFBQVEsR0FBUixRQUFRLENBQVU7SUFBSSxDQUFDOzs7O0lBRTNDLElBQUk7UUFDRixPQUFPLElBQUksT0FBTyxDQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztrQkFDckMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQztZQUMxQyxJQUFJLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDO2lCQUNuQyxTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLENBQUMsUUFBb0IsRUFBRSxFQUFFO2dCQUM3QixTQUFTLENBQUMsUUFBUSxHQUFHLG1CQUFZLFFBQVEsRUFBQSxDQUFDO2dCQUMxQyxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN0QixNQUFNLENBQUMseUNBQXlDLENBQUMsQ0FBQztZQUNwRCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQzs7QUFqQmEsa0JBQVEsR0FBZSxtQkFBWSxFQUFFLEVBQUEsQ0FBQzs7WUFGckQsVUFBVTs7OztZQUpVLFFBQVE7Ozs7SUFNM0IsbUJBQW9EOzs7OztJQUN4Qyw2QkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJQXBwQ29uZmlnIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcENvbmZpZyB7XHJcbiAgcHVibGljIHN0YXRpYyBzZXR0aW5nczogSUFwcENvbmZpZyA9IDxJQXBwQ29uZmlnPnt9O1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yKSB7IH1cclxuXHJcbiAgbG9hZCgpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTx2b2lkPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGNvbnN0IGh0dHAgPSB0aGlzLmluamVjdG9yLmdldChIdHRwQ2xpZW50KTtcclxuICAgICAgaHR0cC5nZXQoJy9hc3NldHMvY29uZmlnL2NvbmZpZy5qc29uJylcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbigocmVzcG9uc2U6IElBcHBDb25maWcpID0+IHtcclxuICAgICAgICAgIEFwcENvbmZpZy5zZXR0aW5ncyA9IDxJQXBwQ29uZmlnPnJlc3BvbnNlO1xyXG4gICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH0pLmNhdGNoKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgICByZWplY3QoJ0NvdWxkIG5vdCBsb2FkIGZpbGUgL2Fzc2V0cy9jb25maWcuanNvbicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gIH1cclxufVxyXG4iXX0=