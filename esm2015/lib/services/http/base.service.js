/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Headers } from '../../enums/headers.enum';
import { BaseHttpService } from './base-http.service';
import { AppConfig } from '../custom/app.config';
export class BaseService extends BaseHttpService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        super(httpClient);
        this.httpClient = httpClient;
        this.endpoint = '';
        this.moduleUrl = '';
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    create(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPost(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    update(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPatch(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    delete(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    deleteById(id, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getList(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getListBy(filterQuery, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/GetListBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    get(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, null, headerParameters);
    }
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getById(id, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, null, headerParameters);
    }
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getBy(filterQuery, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/GetBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    bulkOperation(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/BulkOperation';
        return this.update(resource, endPointUrl, headerParameters)
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @private
     * @template T
     * @param {?} url
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    callHttpGet(url, filterQuery, headerParameters) {
        /** @type {?} */
        const isFileOPeration = this.isFileOPeration(headerParameters);
        if (isFileOPeration) {
            return this.httpGetFile(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map((response) => {
                return this.wrapHttpResponse(response);
            }));
        }
        else {
            return this.httpGet(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map((response) => {
                return this.wrapHttpResponse(response);
            }));
        }
    }
    /**
     * @private
     * @param {?=} headerParameters
     * @return {?}
     */
    isFileOPeration(headerParameters) {
        if (headerParameters) {
            /** @type {?} */
            const fileOPeration = headerParameters.find(x => x && x.header === Headers.FileOperation);
            return fileOPeration ? true : false;
        }
        else {
            return false;
        }
    }
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    wrapHttpResponse(response) {
        /** @type {?} */
        let pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        const responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        const coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    }
}
BaseService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
BaseService.ctorParameters = () => [
    { type: HttpClient }
];
if (false) {
    /** @type {?} */
    BaseService.prototype.endpoint;
    /** @type {?} */
    BaseService.prototype.moduleUrl;
    /**
     * @type {?}
     * @protected
     */
    BaseService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2h0dHAvYmFzZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFckMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBU25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFLakQsTUFBTSxPQUFPLFdBQVksU0FBUSxlQUFlOzs7O0lBSTlDLFlBQXNCLFVBQXNCO1FBQzFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQURFLGVBQVUsR0FBVixVQUFVLENBQVk7UUFINUMsYUFBUSxHQUFXLEVBQUUsQ0FBQztRQUN0QixjQUFTLEdBQVcsRUFBRSxDQUFDO0lBSXZCLENBQUM7Ozs7Ozs7O0lBRUQsTUFBTSxDQUFJLFFBQVEsRUFBRSxXQUFvQixFQUFFLGdCQUFvQztRQUM1RSxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUN4SSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQsTUFBTSxDQUFJLFFBQVEsRUFBRSxXQUFvQixFQUFFLGdCQUFvQztRQUM1RSxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUN6SSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFFRCxNQUFNLENBQUksV0FBb0IsRUFBRSxnQkFBb0M7UUFDbEUsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUNoSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQsVUFBVSxDQUFJLEVBQU8sRUFBRSxXQUFvQixFQUFFLGdCQUFvQztRQUMvRSxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsR0FBRyxHQUFHLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDM0ksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7O0lBRUQsT0FBTyxDQUFJLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQ25FLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMzQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDN0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELFNBQVMsQ0FBSSxXQUE4QixFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQ3JHLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUM7UUFDMUQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3BJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7OztJQUVELEdBQUcsQ0FBSSxXQUFvQixFQUFFLGdCQUFvQztRQUMvRCxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUNuSCxDQUFDOzs7Ozs7OztJQUVELE9BQU8sQ0FBSSxFQUFFLEVBQUUsV0FBb0IsRUFBRSxnQkFBb0M7UUFDdkUsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUM5SCxDQUFDOzs7Ozs7OztJQUVELEtBQUssQ0FBSSxXQUE4QixFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQ2pHLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDdEQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3BJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7Ozs7SUFFRCxhQUFhLENBQUksUUFBYSxFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQ3hGLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUM5RCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUksUUFBUSxFQUFFLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQzthQUMzRCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7OztJQUVPLFdBQVcsQ0FBSSxHQUFXLEVBQUUsV0FBK0IsRUFBRSxnQkFBb0M7O2NBQ2pHLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDO1FBRTlELElBQUksZUFBZSxFQUFFO1lBQ25CLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztpQkFDL0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO2dCQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ1A7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztpQkFDM0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO2dCQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ1A7SUFDSCxDQUFDOzs7Ozs7SUFFTyxlQUFlLENBQUMsZ0JBQW9DO1FBQzFELElBQUksZ0JBQWdCLEVBQUU7O2tCQUNkLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxPQUFPLENBQUMsYUFBYSxDQUFDO1lBQ3pGLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUVyQzthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7Ozs7Ozs7SUFFTyxnQkFBZ0IsQ0FBSSxRQUFhOztZQUNuQyxZQUFZO1FBRWhCLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFBRTtZQUMxRSxZQUFZLEdBQUcsbUJBQWMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFBLENBQUM7U0FDN0Y7O2NBRUssWUFBWSxHQUFHLG1CQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLEVBQUE7O2NBQzVELGdCQUFnQixHQUF3QjtZQUM1QyxhQUFhLEVBQUUsWUFBWTtTQUM1QjtRQUVELElBQUksWUFBWSxFQUFFO1lBQ2hCLGdCQUFnQixDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7U0FDOUM7UUFFRCxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7OztZQTlIRixVQUFVOzs7O1lBcEJGLFVBQVU7Ozs7SUFzQmpCLCtCQUFzQjs7SUFDdEIsZ0NBQXVCOzs7OztJQUVYLGlDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XHJcblxyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBIZWFkZXJzIH0gZnJvbSAnLi4vLi4vZW51bXMvaGVhZGVycy5lbnVtJztcclxuXHJcbmltcG9ydCB7XHJcbiAgSGVhZGVyUGFyYW1ldGVyLFxyXG4gIEdlbmVyaWNFeHByZXNzaW9uLFxyXG4gIENvcmVIdHRwUmVzcG9uc2UsXHJcbiAgU2VydmljZVJlc3VsdCxcclxuICBQYWdpbmdSZXN1bHRcclxufSBmcm9tICcuLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5pbXBvcnQgeyBCYXNlSHR0cFNlcnZpY2UgfSBmcm9tICcuL2Jhc2UtaHR0cC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnIH0gZnJvbSAnLi4vY3VzdG9tL2FwcC5jb25maWcnO1xyXG5cclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBCYXNlU2VydmljZSBleHRlbmRzIEJhc2VIdHRwU2VydmljZSB7XHJcbiAgZW5kcG9pbnQ6IHN0cmluZyA9ICcnO1xyXG4gIG1vZHVsZVVybDogc3RyaW5nID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBodHRwQ2xpZW50OiBIdHRwQ2xpZW50KSB7XHJcbiAgICBzdXBlcihodHRwQ2xpZW50KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZTxUPihyZXNvdXJjZSwgZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50O1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cFBvc3Q8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsLCByZXNvdXJjZSwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlPFQ+KHJlc291cmNlLCBlbmRQb2ludFVybD86IHN0cmluZywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogT2JzZXJ2YWJsZTxDb3JlSHR0cFJlc3BvbnNlPFQ+PiB7XHJcbiAgICBlbmRQb2ludFVybCA9IGVuZFBvaW50VXJsIHx8IHRoaXMuZW5kcG9pbnQ7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwUGF0Y2g8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsLCByZXNvdXJjZSwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlPFQ+KGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludDtcclxuICAgIHJldHVybiB0aGlzLmh0dHBEZWxldGU8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVCeUlkPFQ+KGlkOiBhbnksIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludDtcclxuICAgIHJldHVybiB0aGlzLmh0dHBEZWxldGU8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsICsgJy8nICsgaWQsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGdldExpc3Q8VD4oZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50O1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cEdldDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGdldExpc3RCeTxUPihmaWx0ZXJRdWVyeTogR2VuZXJpY0V4cHJlc3Npb24sIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludCArICcvR2V0TGlzdEJ5JztcclxuICAgIHJldHVybiB0aGlzLmh0dHBHZXQ8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsLCB0aGlzLmdldEh0dHBIZWFkZXIoZmlsdGVyUXVlcnksIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0PFQ+KGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludDtcclxuICAgIHJldHVybiB0aGlzLmNhbGxIdHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgdGhpcy5tb2R1bGVVcmwgKyBlbmRQb2ludFVybCwgbnVsbCwgaGVhZGVyUGFyYW1ldGVycyk7XHJcbiAgfVxyXG5cclxuICBnZXRCeUlkPFQ+KGlkLCBlbmRQb2ludFVybD86IHN0cmluZywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICBlbmRQb2ludFVybCA9IGVuZFBvaW50VXJsIHx8IHRoaXMuZW5kcG9pbnQ7XHJcbiAgICByZXR1cm4gdGhpcy5jYWxsSHR0cEdldDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwgKyAnLycgKyBpZCwgbnVsbCwgaGVhZGVyUGFyYW1ldGVycyk7XHJcbiAgfVxyXG5cclxuICBnZXRCeTxUPihmaWx0ZXJRdWVyeTogR2VuZXJpY0V4cHJlc3Npb24sIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludCArICcvR2V0QnknO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cEdldDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihmaWx0ZXJRdWVyeSwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBidWxrT3BlcmF0aW9uPFQ+KHJlc291cmNlOiBhbnksIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludCArICcvQnVsa09wZXJhdGlvbic7XHJcbiAgICByZXR1cm4gdGhpcy51cGRhdGU8VD4ocmVzb3VyY2UsIGVuZFBvaW50VXJsLCBoZWFkZXJQYXJhbWV0ZXJzKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjYWxsSHR0cEdldDxUPih1cmw6IHN0cmluZywgZmlsdGVyUXVlcnk/OiBHZW5lcmljRXhwcmVzc2lvbiwgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICBjb25zdCBpc0ZpbGVPUGVyYXRpb24gPSB0aGlzLmlzRmlsZU9QZXJhdGlvbihoZWFkZXJQYXJhbWV0ZXJzKTtcclxuXHJcbiAgICBpZiAoaXNGaWxlT1BlcmF0aW9uKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmh0dHBHZXRGaWxlPFQ+KHVybCwgdGhpcy5nZXRIdHRwSGVhZGVyKGZpbHRlclF1ZXJ5LCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmh0dHBHZXQ8VD4odXJsLCB0aGlzLmdldEh0dHBIZWFkZXIoZmlsdGVyUXVlcnksIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpc0ZpbGVPUGVyYXRpb24oaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogYm9vbGVhbiB7XHJcbiAgICBpZiAoaGVhZGVyUGFyYW1ldGVycykge1xyXG4gICAgICBjb25zdCBmaWxlT1BlcmF0aW9uID0gaGVhZGVyUGFyYW1ldGVycy5maW5kKHggPT4geCAmJiB4LmhlYWRlciA9PT0gSGVhZGVycy5GaWxlT3BlcmF0aW9uKTtcclxuICAgICAgcmV0dXJuIGZpbGVPUGVyYXRpb24gPyB0cnVlIDogZmFsc2U7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSB3cmFwSHR0cFJlc3BvbnNlPFQ+KHJlc3BvbnNlOiBhbnkpIHtcclxuICAgIGxldCBwYWdpbmdSZXN1bHQ7XHJcblxyXG4gICAgaWYgKHJlc3BvbnNlLmhlYWRlcnMgJiYgcmVzcG9uc2UuaGVhZGVycy5nZXQoSGVhZGVycy5QYWdpbmdSZXNwb25zZUhlYWRlcikpIHtcclxuICAgICAgcGFnaW5nUmVzdWx0ID0gPFBhZ2luZ1Jlc3VsdD5KU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KEhlYWRlcnMuUGFnaW5nUmVzcG9uc2VIZWFkZXIpKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCByZXNwb25zZUJvZHkgPSA8U2VydmljZVJlc3VsdDxUPj4ocmVzcG9uc2UuYm9keSB8fCByZXNwb25zZSk7XHJcbiAgICBjb25zdCBjb3JlSHR0cFJlc3BvbnNlOiBDb3JlSHR0cFJlc3BvbnNlPFQ+ID0ge1xyXG4gICAgICBzZXJ2aWNlUmVzdWx0OiByZXNwb25zZUJvZHlcclxuICAgIH07XHJcblxyXG4gICAgaWYgKHBhZ2luZ1Jlc3VsdCkge1xyXG4gICAgICBjb3JlSHR0cFJlc3BvbnNlLnBhZ2luZ1Jlc3VsdCA9IHBhZ2luZ1Jlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gY29yZUh0dHBSZXNwb25zZTtcclxuICB9XHJcbn1cclxuIl19