/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AppConfig } from '../custom/app.config';
import { BaseHttpService } from './base-http.service';
import { Headers } from '../../enums/headers.enum';
export class DataService extends BaseHttpService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        super(httpClient);
        this.httpClient = httpClient;
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    create(resource, endPointUrl = '', headerParameters) {
        return this.httpPost(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    update(resource, endPointUrl = '', headerParameters) {
        return this.httpPatch(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    delete(Id, endPointUrl = '', headerParameters) {
        return this.httpDelete(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getList(endPointUrl = '', headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    getListBy(endPointUrl = '', filterQuery, headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    get(Id, endPointUrl = '', headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    getBy(endPointUrl = '', filterQuery, headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    wrapHttpResponse(response) {
        /** @type {?} */
        let pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        const responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        const coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    }
}
DataService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2h0dHAvZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFckMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQVV0RCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFHbkQsTUFBTSxPQUFPLFdBQVksU0FBUSxlQUFlOzs7O0lBRTlDLFlBQXNCLFVBQXNCO1FBQzFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQURFLGVBQVUsR0FBVixVQUFVLENBQVk7SUFFNUMsQ0FBQzs7Ozs7Ozs7SUFFRCxNQUFNLENBQUksUUFBUSxFQUFFLGNBQXNCLEVBQUUsRUFBRSxnQkFBb0M7UUFDaEYsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxXQUFXLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDdkgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELE1BQU0sQ0FBSSxRQUFRLEVBQUUsY0FBc0IsRUFBRSxFQUFFLGdCQUFvQztRQUNoRixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLFdBQVcsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUN4SCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQsTUFBTSxDQUFJLEVBQUUsRUFBRSxjQUFzQixFQUFFLEVBQUUsZ0JBQW9DO1FBQzFFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxHQUFHLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUMxSCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFFRCxPQUFPLENBQUksY0FBc0IsRUFBRSxFQUFFLGdCQUFvQztRQUN2RSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQzVHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7Ozs7SUFFRCxTQUFTLENBQUksY0FBc0IsRUFBRSxFQUFFLFdBQThCLEVBQUUsZ0JBQW9DO1FBQ3pHLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDbkgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELEdBQUcsQ0FBSSxFQUFFLEVBQUUsY0FBc0IsRUFBRSxFQUFFLGdCQUFvQztRQUN2RSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLFdBQVcsR0FBRyxHQUFHLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDdkgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQWEsRUFBRSxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELEtBQUssQ0FBSSxjQUFzQixFQUFFLEVBQUUsV0FBOEIsRUFBRSxnQkFBb0M7UUFDckcsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUNuSCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFHTyxnQkFBZ0IsQ0FBSSxRQUFhOztZQUNuQyxZQUFZO1FBRWhCLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFBRTtZQUMxRSxZQUFZLEdBQUcsbUJBQWMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFBLENBQUM7U0FDN0Y7O2NBRUssWUFBWSxHQUFHLG1CQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLEVBQUE7O2NBQzVELGdCQUFnQixHQUF3QjtZQUM1QyxhQUFhLEVBQUUsWUFBWTtTQUM1QjtRQUVELElBQUksWUFBWSxFQUFFO1lBQ2hCLGdCQUFnQixDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7U0FDOUM7UUFFRCxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7OztZQTFFRixVQUFVOzs7O1lBbEJGLFVBQVU7Ozs7Ozs7SUFxQkwsaUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29uZmlnIH0gZnJvbSAnLi4vY3VzdG9tL2FwcC5jb25maWcnO1xyXG5pbXBvcnQgeyBCYXNlSHR0cFNlcnZpY2UgfSBmcm9tICcuL2Jhc2UtaHR0cC5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7XHJcbiAgSGVhZGVyUGFyYW1ldGVyLFxyXG4gIFNlcnZpY2VSZXN1bHQsXHJcbiAgUGFnaW5nUmVzdWx0LFxyXG4gIENvcmVIdHRwUmVzcG9uc2UsXHJcbiAgR2VuZXJpY0V4cHJlc3Npb25cclxufSBmcm9tICcuLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgSGVhZGVycyB9IGZyb20gJy4uLy4uL2VudW1zL2hlYWRlcnMuZW51bSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBEYXRhU2VydmljZSBleHRlbmRzIEJhc2VIdHRwU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBodHRwQ2xpZW50OiBIdHRwQ2xpZW50KSB7XHJcbiAgICBzdXBlcihodHRwQ2xpZW50KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZTxUPihyZXNvdXJjZSwgZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBQb3N0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgZW5kUG9pbnRVcmwsIHJlc291cmNlLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGU8VD4ocmVzb3VyY2UsIGVuZFBvaW50VXJsOiBzdHJpbmcgPSAnJywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogT2JzZXJ2YWJsZTxDb3JlSHR0cFJlc3BvbnNlPFQ+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwUGF0Y2g8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCwgcmVzb3VyY2UsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZTxUPihJZCwgZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBEZWxldGU8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCArICcvJyArIElkLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBnZXRMaXN0PFQ+KGVuZFBvaW50VXJsOiBzdHJpbmcgPSAnJywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogT2JzZXJ2YWJsZTxDb3JlSHR0cFJlc3BvbnNlPFQ+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGdldExpc3RCeTxUPihlbmRQb2ludFVybDogc3RyaW5nID0gJycsIGZpbHRlclF1ZXJ5OiBHZW5lcmljRXhwcmVzc2lvbiwgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihmaWx0ZXJRdWVyeSwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBnZXQ8VD4oSWQsIGVuZFBvaW50VXJsOiBzdHJpbmcgPSAnJywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgZW5kUG9pbnRVcmwgKyAnLycgKyBJZCwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Qnk8VD4oZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBmaWx0ZXJRdWVyeTogR2VuZXJpY0V4cHJlc3Npb24sIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cEdldDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIGVuZFBvaW50VXJsLCB0aGlzLmdldEh0dHBIZWFkZXIoZmlsdGVyUXVlcnksIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcblxyXG4gIHByaXZhdGUgd3JhcEh0dHBSZXNwb25zZTxUPihyZXNwb25zZTogYW55KSB7XHJcbiAgICBsZXQgcGFnaW5nUmVzdWx0O1xyXG5cclxuICAgIGlmIChyZXNwb25zZS5oZWFkZXJzICYmIHJlc3BvbnNlLmhlYWRlcnMuZ2V0KEhlYWRlcnMuUGFnaW5nUmVzcG9uc2VIZWFkZXIpKSB7XHJcbiAgICAgIHBhZ2luZ1Jlc3VsdCA9IDxQYWdpbmdSZXN1bHQ+SlNPTi5wYXJzZShyZXNwb25zZS5oZWFkZXJzLmdldChIZWFkZXJzLlBhZ2luZ1Jlc3BvbnNlSGVhZGVyKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcmVzcG9uc2VCb2R5ID0gPFNlcnZpY2VSZXN1bHQ8VD4+KHJlc3BvbnNlLmJvZHkgfHwgcmVzcG9uc2UpO1xyXG4gICAgY29uc3QgY29yZUh0dHBSZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxUPiA9IHtcclxuICAgICAgc2VydmljZVJlc3VsdDogcmVzcG9uc2VCb2R5XHJcbiAgICB9O1xyXG5cclxuICAgIGlmIChwYWdpbmdSZXN1bHQpIHtcclxuICAgICAgY29yZUh0dHBSZXNwb25zZS5wYWdpbmdSZXN1bHQgPSBwYWdpbmdSZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGNvcmVIdHRwUmVzcG9uc2U7XHJcbiAgfVxyXG59XHJcbiJdfQ==