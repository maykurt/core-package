/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { DataService } from '../../services/http/data.service';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "../../services/http/data.service";
export class SelectorService {
    /**
     * @param {?} dataService
     */
    constructor(dataService) {
        this.dataService = dataService;
        this.listStateChanged = new Subject();
        this.counter = 0;
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    setLoadUrlSetting(urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    }
    /**
     * @return {?}
     */
    getLoadUrlSetting() {
        return (/** @type {?} */ ({
            moduleUrl: this.moduleUrl,
            endPointUrl: this.endPointUrl
        }));
    }
    /**
     * @param {?} headerParameters
     * @return {?}
     */
    setHeaders(headerParameters) {
        this.headerParameters = headerParameters;
    }
    /**
     * @return {?}
     */
    getHeaders() {
        return this.headerParameters;
    }
    /**
     * @param {?} expression
     * @return {?}
     */
    loadDataByFilterAndPageSetting(expression) {
        /** @type {?} */
        const reqUrl = `${this.moduleUrl}${this.endPointUrl}`;
        this.dataService.getListBy(reqUrl, expression, this.headerParameters)
            .subscribe((httpResponse) => {
            this.listStateChanged.next(httpResponse.serviceResult.Result.slice());
        });
    }
}
SelectorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SelectorService.ctorParameters = () => [
    { type: DataService }
];
/** @nocollapse */ SelectorService.ngInjectableDef = i0.defineInjectable({ factory: function SelectorService_Factory() { return new SelectorService(i0.inject(i1.DataService)); }, token: SelectorService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SelectorService.prototype.listStateChanged;
    /** @type {?} */
    SelectorService.prototype.counter;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.moduleUrl;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.endPointUrl;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.headerParameters;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3NlbGVjdG9yL3NlbGVjdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7OztBQVcvQixNQUFNLE9BQU8sZUFBZTs7OztJQVMxQixZQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVJyQyxxQkFBZ0IsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO1FBQ3hDLFlBQU8sR0FBRyxDQUFDLENBQUM7SUFPNkIsQ0FBQzs7Ozs7SUFFakQsaUJBQWlCLENBQUMsVUFBc0I7UUFDdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2YsT0FBTyxtQkFBWTtZQUNqQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7WUFDekIsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQzlCLEVBQUEsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLGdCQUFtQztRQUM1QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELFVBQVU7UUFDUixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELDhCQUE4QixDQUFDLFVBQTZCOztjQUNwRCxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUU7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDbEUsU0FBUyxDQUFDLENBQUMsWUFBcUMsRUFBRSxFQUFFO1lBQ25ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7OztZQXhDRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFYUSxXQUFXOzs7OztJQWFsQiwyQ0FBK0M7O0lBQy9DLGtDQUFtQjs7Ozs7SUFFbkIsb0NBQXVCOzs7OztJQUN2QixzQ0FBeUI7Ozs7O0lBRXpCLDJDQUE0Qzs7Ozs7SUFFaEMsc0NBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2h0dHAvZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQge1xyXG4gIEhlYWRlclBhcmFtZXRlcixcclxuICBDb3JlSHR0cFJlc3BvbnNlLFxyXG4gIEdlbmVyaWNFeHByZXNzaW9uLFxyXG4gIFVybE9wdGlvbnNcclxufSBmcm9tICcuLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0b3JTZXJ2aWNlIHtcclxuICBwdWJsaWMgbGlzdFN0YXRlQ2hhbmdlZCA9IG5ldyBTdWJqZWN0PGFueVtdPigpO1xyXG4gIHB1YmxpYyBjb3VudGVyID0gMDtcclxuXHJcbiAgcHJpdmF0ZSBtb2R1bGVVcmw6IGFueTtcclxuICBwcml2YXRlIGVuZFBvaW50VXJsOiBhbnk7XHJcblxyXG4gIHByaXZhdGUgaGVhZGVyUGFyYW1ldGVyczogSGVhZGVyUGFyYW1ldGVyW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGF0YVNlcnZpY2U6IERhdGFTZXJ2aWNlKSB7IH1cclxuXHJcbiAgc2V0TG9hZFVybFNldHRpbmcodXJsT3B0aW9uczogVXJsT3B0aW9ucykge1xyXG4gICAgdGhpcy5tb2R1bGVVcmwgPSB1cmxPcHRpb25zLm1vZHVsZVVybDtcclxuICAgIHRoaXMuZW5kUG9pbnRVcmwgPSB1cmxPcHRpb25zLmVuZFBvaW50VXJsO1xyXG4gIH1cclxuXHJcbiAgZ2V0TG9hZFVybFNldHRpbmcoKTogVXJsT3B0aW9ucyB7XHJcbiAgICByZXR1cm4gPFVybE9wdGlvbnM+e1xyXG4gICAgICBtb2R1bGVVcmw6IHRoaXMubW9kdWxlVXJsLFxyXG4gICAgICBlbmRQb2ludFVybDogdGhpcy5lbmRQb2ludFVybFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHNldEhlYWRlcnMoaGVhZGVyUGFyYW1ldGVyczogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIHRoaXMuaGVhZGVyUGFyYW1ldGVycyA9IGhlYWRlclBhcmFtZXRlcnM7XHJcbiAgfVxyXG5cclxuICBnZXRIZWFkZXJzKCk6IEhlYWRlclBhcmFtZXRlcltdIHtcclxuICAgIHJldHVybiB0aGlzLmhlYWRlclBhcmFtZXRlcnM7XHJcbiAgfVxyXG5cclxuICBsb2FkRGF0YUJ5RmlsdGVyQW5kUGFnZVNldHRpbmcoZXhwcmVzc2lvbjogR2VuZXJpY0V4cHJlc3Npb24pIHtcclxuICAgIGNvbnN0IHJlcVVybCA9IGAke3RoaXMubW9kdWxlVXJsfSR7dGhpcy5lbmRQb2ludFVybH1gO1xyXG4gICAgdGhpcy5kYXRhU2VydmljZS5nZXRMaXN0QnkocmVxVXJsLCBleHByZXNzaW9uLCB0aGlzLmhlYWRlclBhcmFtZXRlcnMpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGh0dHBSZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxhbnlbXT4pID0+IHtcclxuICAgICAgICB0aGlzLmxpc3RTdGF0ZUNoYW5nZWQubmV4dChodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQuc2xpY2UoKSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=