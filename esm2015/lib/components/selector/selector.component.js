/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectorService } from './selector.service';
import * as _ from 'lodash';
export class SelectorComponent {
    /**
     * @param {?} selectorService
     */
    constructor(selectorService) {
        this.selectorService = selectorService;
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
        this.value = 'Id';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.selectorService.listStateChanged
            .subscribe((result) => {
            if (result && result.length > 0 && this._labelField === 'CombinedFields') {
                result.map((item) => {
                    /** @type {?} */
                    let combinedValue = '';
                    ((/** @type {?} */ (this.labelField))).forEach((field) => {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -this.seperator.length) : null;
                    return item;
                });
            }
            this.listData = _.orderBy(result, [
                'OrderNo',
                z => z[this._labelField] && (typeof z[this._labelField] === 'string') ? z[this._labelField].toLowerCase() : z[this._labelField],
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                this._labelField
            ], ['asc']);
            this.findReadonlyValue();
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.requestOptions && this.requestOptions) {
            this.urlOptions = this.requestOptions.UrlOptions;
            this.customFilter = this.requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(this.requestOptions.HeaderParameters || []);
            this.load();
        }
    }
    /**
     * @param {?} requestOptions
     * @param {?=} value
     * @param {?=} labelField
     * @param {?=} seperator
     * @return {?}
     */
    setRequestOptions(requestOptions, value = null, labelField = null, seperator = ' ') {
        if (value) {
            this.value = value;
            this.model = value;
        }
        if (labelField) {
            this.labelField = labelField;
        }
        if (seperator) {
            this.seperator = seperator;
        }
        if (this.labelField instanceof Array) {
            this._labelField = 'CombinedFields';
        }
        else if (this.labelField) {
            this._labelField = (/** @type {?} */ (this.labelField));
        }
        if (requestOptions) {
            this.urlOptions = requestOptions.UrlOptions;
            this.customFilter = requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(requestOptions.HeaderParameters || []);
        }
    }
    /**
     * @return {?}
     */
    load() {
        this.getDataSource();
    }
    /**
     * @return {?}
     */
    reload() {
        this.getDataSource();
    }
    /**
     * @return {?}
     */
    reset() {
        this.listData = [];
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get Model() {
        return this.model;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set Model(val) {
        this.model = val;
        this.onChange(val);
        this.onTouched();
        if (val) {
            if (this.listData) {
                /** @type {?} */
                const data = this.listData.find(x => x[this.value] === this.model);
                if (data) {
                    this.changed.emit(data);
                    this.readOnlyValue = data[this._labelField];
                }
            }
            else {
                /** @type {?} */
                const emitData = {};
                emitData[this.value] = this.model;
                this.readOnlyValue = this.model;
                this.changed.emit(emitData);
            }
        }
        else {
            this.changed.emit(null);
            this.readOnlyValue = null;
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.model = value;
        this.findReadonlyValue();
    }
    /**
     * @return {?}
     */
    findReadonlyValue() {
        this.readOnlyValue = null;
        if (this.listData) {
            if (this.model || this.model === 0) {
                /** @type {?} */
                const data = this.listData.find(x => x[this.value] === this.model);
                if (data) {
                    this.readOnlyValue = data[this._labelField];
                    if (!this.isFirstValueSend) {
                        this.firstValue.emit(data);
                        this.isFirstValueSend = true;
                    }
                }
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    getDataSource() {
        this.selectorService.loadDataByFilterAndPageSetting(this.customFilter);
    }
}
SelectorComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-selector',
                template: `
    <ng-select *ngIf="!isReadOnly"
      class="custom-ng-select form-selector"
      [items]="listData"
      [searchable]="searchable"
			[clearable]="clearable"
      [bindValue]="value"
      [bindLabel]="_labelField"
      [(ngModel)]="Model"
      [disabled]="isDisabled"
    >
    </ng-select>
    <layout-form-text-input class="readonly-text-input" *ngIf="isReadOnly"
      [isReadOnly]="isReadOnly"
      [isDisabled]="isDisabled"
      [(ngModel)]="readOnlyValue"></layout-form-text-input>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => SelectorComponent),
                        multi: true
                    },
                    SelectorService
                ],
                styles: [".custom-ng-select{height:30px!important;min-width:100px;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
            }] }
];
/** @nocollapse */
SelectorComponent.ctorParameters = () => [
    { type: SelectorService }
];
SelectorComponent.propDecorators = {
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    searchable: [{ type: Input }],
    clearable: [{ type: Input }],
    requestOptions: [{ type: Input }],
    changed: [{ type: Output }],
    firstValue: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SelectorComponent.prototype.isDisabled;
    /** @type {?} */
    SelectorComponent.prototype.isReadOnly;
    /** @type {?} */
    SelectorComponent.prototype.searchable;
    /** @type {?} */
    SelectorComponent.prototype.clearable;
    /** @type {?} */
    SelectorComponent.prototype.requestOptions;
    /** @type {?} */
    SelectorComponent.prototype.changed;
    /** @type {?} */
    SelectorComponent.prototype.firstValue;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.isFirstValueSend;
    /** @type {?} */
    SelectorComponent.prototype.listData;
    /** @type {?} */
    SelectorComponent.prototype.value;
    /** @type {?} */
    SelectorComponent.prototype.labelField;
    /** @type {?} */
    SelectorComponent.prototype._labelField;
    /** @type {?} */
    SelectorComponent.prototype.seperator;
    /** @type {?} */
    SelectorComponent.prototype.readOnlyValue;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.model;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.customFilter;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.urlOptions;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.selectorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc2VsZWN0b3Ivc2VsZWN0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDN0csT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBR3pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQVFyRCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQStCNUIsTUFBTSxPQUFPLGlCQUFpQjs7OztJQTJCNUIsWUFBb0IsZUFBZ0M7UUFBaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBekIzQyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBRWhCLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDOztRQUdsQyxlQUFVLEdBQXVCLElBQUksWUFBWSxFQUFPLENBQUM7UUFJNUQsVUFBSyxHQUFHLElBQUksQ0FBQztRQUViLGdCQUFXLEdBQUcsWUFBWSxDQUFDO1FBRWxDLGNBQVMsR0FBRyxHQUFHLENBQUM7UUFVZCxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQjthQUNsQyxTQUFTLENBQUMsQ0FBQyxNQUFhLEVBQUUsRUFBRTtZQUMzQixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLGdCQUFnQixFQUFFO2dCQUN4RSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7O3dCQUNuQixhQUFhLEdBQUcsRUFBRTtvQkFDdEIsQ0FBQyxtQkFBVSxJQUFJLENBQUMsVUFBVSxFQUFBLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFhLEVBQUUsRUFBRTt3QkFDcEQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7NEJBQ2YsYUFBYSxHQUFHLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQzt5QkFDOUQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBRUgsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDL0YsT0FBTyxJQUFJLENBQUM7Z0JBQ2QsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQzlCO2dCQUNFLFNBQVM7Z0JBQ1QsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDL0gsbUhBQW1IO2dCQUNuSCxJQUFJLENBQUMsV0FBVzthQUNqQixFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztZQUNqRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3JELElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLElBQUksRUFBRSxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDOzs7Ozs7OztJQUVELGlCQUFpQixDQUFDLGNBQThCLEVBQUUsUUFBZ0IsSUFBSSxFQUFFLGFBQWtCLElBQUksRUFBRSxZQUFvQixHQUFHO1FBQ3JILElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDcEI7UUFFRCxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1NBQzlCO1FBRUQsSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztTQUM1QjtRQUdELElBQUksSUFBSSxDQUFDLFVBQVUsWUFBWSxLQUFLLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxnQkFBZ0IsQ0FBQztTQUNyQzthQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLG1CQUFRLElBQUksQ0FBQyxVQUFVLEVBQUEsQ0FBQztTQUM1QztRQUVELElBQUksY0FBYyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxVQUFVLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQztZQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLGNBQWMsQ0FBQyxZQUFZLENBQUM7WUFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLGdCQUFnQixJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQ3hFO0lBQ0gsQ0FBQzs7OztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCxTQUFTO0lBQ1QsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNwQixDQUFDOzs7OztJQUVELElBQUksS0FBSyxDQUFDLEdBQUc7UUFDWCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUVqQixJQUFJLEdBQUcsRUFBRTtZQUNQLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7c0JBQ1gsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUNsRSxJQUFJLElBQUksRUFBRTtvQkFDUixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUM3QzthQUNGO2lCQUFNOztzQkFDQyxRQUFRLEdBQVEsRUFBRTtnQkFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzdCO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzNCO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxFQUFPO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsRUFBTztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLFVBQW1CO0lBQ3BDLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQVU7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDM0IsQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNmLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLEVBQUU7O3NCQUM1QixJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ2xFLElBQUksSUFBSSxFQUFFO29CQUNSLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzNCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7cUJBQzlCO2lCQUNGO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7O0lBRU8sYUFBYTtRQUNuQixJQUFJLENBQUMsZUFBZSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUN6RSxDQUFDOzs7WUExTUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7OztHQWdCVDtnQkFFRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDaEQsS0FBSyxFQUFFLElBQUk7cUJBQ1o7b0JBQ0QsZUFBZTtpQkFDaEI7O2FBQ0Y7Ozs7WUF0Q1EsZUFBZTs7O3lCQXlDckIsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7d0JBQ0wsS0FBSzs2QkFDTCxLQUFLO3NCQUNMLE1BQU07eUJBR04sTUFBTTs7OztJQVJQLHVDQUE0Qjs7SUFDNUIsdUNBQTRCOztJQUM1Qix1Q0FBMkI7O0lBQzNCLHNDQUEwQjs7SUFDMUIsMkNBQXlDOztJQUN6QyxvQ0FBNEM7O0lBRzVDLHVDQUFtRTs7Ozs7SUFDbkUsNkNBQWtDOztJQUVsQyxxQ0FBdUI7O0lBQ3ZCLGtDQUFvQjs7SUFDcEIsdUNBQXFDOztJQUNyQyx3Q0FBa0M7O0lBRWxDLHNDQUFnQjs7SUFFaEIsMENBQTZCOzs7OztJQUU3QixrQ0FBYzs7Ozs7SUFDZCx5Q0FBd0M7Ozs7O0lBQ3hDLHVDQUErQjs7Ozs7SUFHbkIsNENBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSW5wdXQsIFNpbXBsZUNoYW5nZXMsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5cclxuaW1wb3J0IHsgU2VsZWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi9zZWxlY3Rvci5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7XHJcbiAgUmVxdWVzdE9wdGlvbnMsXHJcbiAgVXJsT3B0aW9ucyxcclxuICBHZW5lcmljRXhwcmVzc2lvblxyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1zZWxlY3RvcicsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxuZy1zZWxlY3QgKm5nSWY9XCIhaXNSZWFkT25seVwiXHJcbiAgICAgIGNsYXNzPVwiY3VzdG9tLW5nLXNlbGVjdCBmb3JtLXNlbGVjdG9yXCJcclxuICAgICAgW2l0ZW1zXT1cImxpc3REYXRhXCJcclxuICAgICAgW3NlYXJjaGFibGVdPVwic2VhcmNoYWJsZVwiXHJcblx0XHRcdFtjbGVhcmFibGVdPVwiY2xlYXJhYmxlXCJcclxuICAgICAgW2JpbmRWYWx1ZV09XCJ2YWx1ZVwiXHJcbiAgICAgIFtiaW5kTGFiZWxdPVwiX2xhYmVsRmllbGRcIlxyXG4gICAgICBbKG5nTW9kZWwpXT1cIk1vZGVsXCJcclxuICAgICAgW2Rpc2FibGVkXT1cImlzRGlzYWJsZWRcIlxyXG4gICAgPlxyXG4gICAgPC9uZy1zZWxlY3Q+XHJcbiAgICA8bGF5b3V0LWZvcm0tdGV4dC1pbnB1dCBjbGFzcz1cInJlYWRvbmx5LXRleHQtaW5wdXRcIiAqbmdJZj1cImlzUmVhZE9ubHlcIlxyXG4gICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgW2lzRGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICAgIFsobmdNb2RlbCldPVwicmVhZE9ubHlWYWx1ZVwiPjwvbGF5b3V0LWZvcm0tdGV4dC1pbnB1dD5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL3NlbGVjdG9yLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBTZWxlY3RvckNvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9LFxyXG4gICAgU2VsZWN0b3JTZXJ2aWNlXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0b3JDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBzZWFyY2hhYmxlID0gdHJ1ZTtcclxuICBASW5wdXQoKSBjbGVhcmFibGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHJlcXVlc3RPcHRpb25zPzogUmVxdWVzdE9wdGlvbnM7XHJcbiAgQE91dHB1dCgpIGNoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgLy8gaXQgaXMgZm9yIGdldHRpbmcgdGhlIGZpcnN0IHNlbGVjdGVkIHZhbHVlXHJcbiAgQE91dHB1dCgpIGZpcnN0VmFsdWU/OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIHByaXZhdGUgaXNGaXJzdFZhbHVlU2VuZDogYm9vbGVhbjtcclxuXHJcbiAgcHVibGljIGxpc3REYXRhOiBhbnlbXTtcclxuICBwdWJsaWMgdmFsdWUgPSAnSWQnO1xyXG4gIHB1YmxpYyBsYWJlbEZpZWxkOiBzdHJpbmcgfCBzdHJpbmdbXTtcclxuICBwdWJsaWMgX2xhYmVsRmllbGQgPSAnRGVmaW5pdGlvbic7XHJcblxyXG4gIHNlcGVyYXRvciA9ICcgJztcclxuXHJcbiAgcHVibGljIHJlYWRPbmx5VmFsdWU6IHN0cmluZztcclxuXHJcbiAgcHJpdmF0ZSBtb2RlbDtcclxuICBwcml2YXRlIGN1c3RvbUZpbHRlcjogR2VuZXJpY0V4cHJlc3Npb247XHJcbiAgcHJpdmF0ZSB1cmxPcHRpb25zOiBVcmxPcHRpb25zO1xyXG5cclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZWxlY3RvclNlcnZpY2U6IFNlbGVjdG9yU2VydmljZSkge1xyXG4gICAgdGhpcy5zZWxlY3RvclNlcnZpY2UubGlzdFN0YXRlQ2hhbmdlZFxyXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQ6IGFueVtdKSA9PiB7XHJcbiAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQubGVuZ3RoID4gMCAmJiB0aGlzLl9sYWJlbEZpZWxkID09PSAnQ29tYmluZWRGaWVsZHMnKSB7XHJcbiAgICAgICAgICByZXN1bHQubWFwKChpdGVtOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgbGV0IGNvbWJpbmVkVmFsdWUgPSAnJztcclxuICAgICAgICAgICAgKDxzdHJpbmdbXT50aGlzLmxhYmVsRmllbGQpLmZvckVhY2goKGZpZWxkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXRlbVtmaWVsZF0pIHtcclxuICAgICAgICAgICAgICAgIGNvbWJpbmVkVmFsdWUgPSBjb21iaW5lZFZhbHVlICsgaXRlbVtmaWVsZF0gKyB0aGlzLnNlcGVyYXRvcjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaXRlbVsnQ29tYmluZWRGaWVsZHMnXSA9IGNvbWJpbmVkVmFsdWUgPyBjb21iaW5lZFZhbHVlLnNsaWNlKDAsIC10aGlzLnNlcGVyYXRvci5sZW5ndGgpIDogbnVsbDtcclxuICAgICAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5saXN0RGF0YSA9IF8ub3JkZXJCeShyZXN1bHQsXHJcbiAgICAgICAgICBbXHJcbiAgICAgICAgICAgICdPcmRlck5vJyxcclxuICAgICAgICAgICAgeiA9PiB6W3RoaXMuX2xhYmVsRmllbGRdICYmICh0eXBlb2Ygelt0aGlzLl9sYWJlbEZpZWxkXSA9PT0gJ3N0cmluZycpID8gelt0aGlzLl9sYWJlbEZpZWxkXS50b0xvd2VyQ2FzZSgpIDogelt0aGlzLl9sYWJlbEZpZWxkXSxcclxuICAgICAgICAgICAgLy8geSA9PiB5WydEZWZpbml0aW9uJ10gJiYgKHR5cGVvZiB5WydEZWZpbml0aW9uJ10gPT09ICdzdHJpbmcnKSA/IHlbJ0RlZmluaXRpb24nXS50b0xvd2VyQ2FzZSgpIDogeVsnRGVmaW5pdGlvbiddLFxyXG4gICAgICAgICAgICB0aGlzLl9sYWJlbEZpZWxkXHJcbiAgICAgICAgICBdLCBbJ2FzYyddKTtcclxuICAgICAgICB0aGlzLmZpbmRSZWFkb25seVZhbHVlKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMucmVxdWVzdE9wdGlvbnMgJiYgdGhpcy5yZXF1ZXN0T3B0aW9ucykge1xyXG4gICAgICB0aGlzLnVybE9wdGlvbnMgPSB0aGlzLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnM7XHJcbiAgICAgIHRoaXMuY3VzdG9tRmlsdGVyID0gdGhpcy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXI7XHJcbiAgICAgIHRoaXMuc2VsZWN0b3JTZXJ2aWNlLnNldExvYWRVcmxTZXR0aW5nKHRoaXMudXJsT3B0aW9ucyk7XHJcbiAgICAgIHRoaXMuc2VsZWN0b3JTZXJ2aWNlLnNldEhlYWRlcnModGhpcy5yZXF1ZXN0T3B0aW9ucy5IZWFkZXJQYXJhbWV0ZXJzIHx8IFtdKTtcclxuICAgICAgdGhpcy5sb2FkKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRSZXF1ZXN0T3B0aW9ucyhyZXF1ZXN0T3B0aW9uczogUmVxdWVzdE9wdGlvbnMsIHZhbHVlOiBzdHJpbmcgPSBudWxsLCBsYWJlbEZpZWxkOiBhbnkgPSBudWxsLCBzZXBlcmF0b3I6IHN0cmluZyA9ICcgJykge1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcclxuICAgICAgdGhpcy5tb2RlbCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChsYWJlbEZpZWxkKSB7XHJcbiAgICAgIHRoaXMubGFiZWxGaWVsZCA9IGxhYmVsRmllbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNlcGVyYXRvcikge1xyXG4gICAgICB0aGlzLnNlcGVyYXRvciA9IHNlcGVyYXRvcjtcclxuICAgIH1cclxuXHJcblxyXG4gICAgaWYgKHRoaXMubGFiZWxGaWVsZCBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgIHRoaXMuX2xhYmVsRmllbGQgPSAnQ29tYmluZWRGaWVsZHMnO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmxhYmVsRmllbGQpIHtcclxuICAgICAgdGhpcy5fbGFiZWxGaWVsZCA9IDxzdHJpbmc+dGhpcy5sYWJlbEZpZWxkO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChyZXF1ZXN0T3B0aW9ucykge1xyXG4gICAgICB0aGlzLnVybE9wdGlvbnMgPSByZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zO1xyXG4gICAgICB0aGlzLmN1c3RvbUZpbHRlciA9IHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlcjtcclxuICAgICAgdGhpcy5zZWxlY3RvclNlcnZpY2Uuc2V0TG9hZFVybFNldHRpbmcodGhpcy51cmxPcHRpb25zKTtcclxuICAgICAgdGhpcy5zZWxlY3RvclNlcnZpY2Uuc2V0SGVhZGVycyhyZXF1ZXN0T3B0aW9ucy5IZWFkZXJQYXJhbWV0ZXJzIHx8IFtdKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGxvYWQoKSB7XHJcbiAgICB0aGlzLmdldERhdGFTb3VyY2UoKTtcclxuICB9XHJcblxyXG4gIHJlbG9hZCgpIHtcclxuICAgIHRoaXMuZ2V0RGF0YVNvdXJjZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVzZXQoKSB7XHJcbiAgICB0aGlzLmxpc3REYXRhID0gW107XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZSh2YWw6IGFueSkge1xyXG4gIH1cclxuXHJcbiAgb25Ub3VjaGVkKCkge1xyXG4gIH1cclxuXHJcbiAgZ2V0IE1vZGVsKCkge1xyXG4gICAgcmV0dXJuIHRoaXMubW9kZWw7XHJcbiAgfVxyXG5cclxuICBzZXQgTW9kZWwodmFsKSB7XHJcbiAgICB0aGlzLm1vZGVsID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuXHJcbiAgICBpZiAodmFsKSB7XHJcbiAgICAgIGlmICh0aGlzLmxpc3REYXRhKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMubGlzdERhdGEuZmluZCh4ID0+IHhbdGhpcy52YWx1ZV0gPT09IHRoaXMubW9kZWwpO1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICB0aGlzLmNoYW5nZWQuZW1pdChkYXRhKTtcclxuICAgICAgICAgIHRoaXMucmVhZE9ubHlWYWx1ZSA9IGRhdGFbdGhpcy5fbGFiZWxGaWVsZF07XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IGVtaXREYXRhOiBhbnkgPSB7fTtcclxuICAgICAgICBlbWl0RGF0YVt0aGlzLnZhbHVlXSA9IHRoaXMubW9kZWw7XHJcbiAgICAgICAgdGhpcy5yZWFkT25seVZhbHVlID0gdGhpcy5tb2RlbDtcclxuICAgICAgICB0aGlzLmNoYW5nZWQuZW1pdChlbWl0RGF0YSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hhbmdlZC5lbWl0KG51bGwpO1xyXG4gICAgICB0aGlzLnJlYWRPbmx5VmFsdWUgPSBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbjogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbjogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xyXG4gIH1cclxuXHJcbiAgc2V0RGlzYWJsZWRTdGF0ZShpc0Rpc2FibGVkOiBib29sZWFuKTogdm9pZCB7XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMubW9kZWwgPSB2YWx1ZTtcclxuICAgIHRoaXMuZmluZFJlYWRvbmx5VmFsdWUoKTtcclxuICB9XHJcblxyXG4gIGZpbmRSZWFkb25seVZhbHVlKCk6IHZvaWQge1xyXG4gICAgdGhpcy5yZWFkT25seVZhbHVlID0gbnVsbDtcclxuICAgIGlmICh0aGlzLmxpc3REYXRhKSB7XHJcbiAgICAgIGlmICh0aGlzLm1vZGVsIHx8IHRoaXMubW9kZWwgPT09IDApIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5saXN0RGF0YS5maW5kKHggPT4geFt0aGlzLnZhbHVlXSA9PT0gdGhpcy5tb2RlbCk7XHJcbiAgICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICAgIHRoaXMucmVhZE9ubHlWYWx1ZSA9IGRhdGFbdGhpcy5fbGFiZWxGaWVsZF07XHJcbiAgICAgICAgICBpZiAoIXRoaXMuaXNGaXJzdFZhbHVlU2VuZCkge1xyXG4gICAgICAgICAgICB0aGlzLmZpcnN0VmFsdWUuZW1pdChkYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5pc0ZpcnN0VmFsdWVTZW5kID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0RGF0YVNvdXJjZSgpIHtcclxuICAgIHRoaXMuc2VsZWN0b3JTZXJ2aWNlLmxvYWREYXRhQnlGaWx0ZXJBbmRQYWdlU2V0dGluZyh0aGlzLmN1c3RvbUZpbHRlcik7XHJcbiAgfVxyXG59XHJcbiJdfQ==