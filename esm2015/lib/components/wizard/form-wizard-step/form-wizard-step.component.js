/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormWizardComponent } from '../form-wizard/form-wizard.component';
export class FormWizardStepComponent {
    /**
     * @param {?} parent
     */
    constructor(parent) {
        this.parent = parent;
        this.title = '';
        this.isValid = true;
        // this.step = 0;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.isValid) {
            console.log(this.isValid);
            this.parent.isValid = this.isValid;
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.step = this.parent.addStep(this.title);
        this.isCurrent = this.step === this.parent.step;
        this.stepChangeSubcriotion = this.parent.stepChange.subscribe(step => {
            this.isCurrent = this.step === step;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.stepChangeSubcriotion) {
            this.stepChangeSubcriotion.unsubscribe();
        }
    }
}
FormWizardStepComponent.decorators = [
    { type: Component, args: [{
                selector: 'wizard-step',
                host: {
                    '[style.display]': 'isCurrent ? "flex" : "none"',
                },
                template: `
    <ng-content></ng-content>
  `
            }] }
];
/** @nocollapse */
FormWizardStepComponent.ctorParameters = () => [
    { type: FormWizardComponent }
];
FormWizardStepComponent.propDecorators = {
    title: [{ type: Input }],
    isValid: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormWizardStepComponent.prototype.isCurrent;
    /** @type {?} */
    FormWizardStepComponent.prototype.step;
    /** @type {?} */
    FormWizardStepComponent.prototype.stepChangeSubcriotion;
    /** @type {?} */
    FormWizardStepComponent.prototype.title;
    /** @type {?} */
    FormWizardStepComponent.prototype.isValid;
    /**
     * @type {?}
     * @private
     */
    FormWizardStepComponent.prototype.parent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS13aXphcmQtc3RlcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy93aXphcmQvZm9ybS13aXphcmQtc3RlcC9mb3JtLXdpemFyZC1zdGVwLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQStDLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBWTNFLE1BQU0sT0FBTyx1QkFBdUI7Ozs7SUFTbEMsWUFBb0IsTUFBMkI7UUFBM0IsV0FBTSxHQUFOLE1BQU0sQ0FBcUI7UUFIdEMsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUNuQixZQUFPLEdBQVksSUFBSSxDQUFDO1FBRy9CLGlCQUFpQjtJQUNuQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEVBQUU7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7SUFJRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBRWhELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7O1lBNUNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsSUFBSSxFQUFFO29CQUNKLGlCQUFpQixFQUFFLDZCQUE2QjtpQkFDakQ7Z0JBQ0QsUUFBUSxFQUFFOztHQUVUO2FBQ0Y7Ozs7WUFYUSxtQkFBbUI7OztvQkFrQnpCLEtBQUs7c0JBQ0wsS0FBSzs7OztJQU5OLDRDQUFtQjs7SUFDbkIsdUNBQWE7O0lBQ2Isd0RBQW9DOztJQUdwQyx3Q0FBNEI7O0lBQzVCLDBDQUFpQzs7Ozs7SUFFckIseUNBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtV2l6YXJkQ29tcG9uZW50IH0gZnJvbSAnLi4vZm9ybS13aXphcmQvZm9ybS13aXphcmQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3dpemFyZC1zdGVwJyxcclxuICBob3N0OiB7XHJcbiAgICAnW3N0eWxlLmRpc3BsYXldJzogJ2lzQ3VycmVudCA/IFwiZmxleFwiIDogXCJub25lXCInLFxyXG4gIH0sXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICBgLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybVdpemFyZFN0ZXBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcclxuICBpc0N1cnJlbnQ6IGJvb2xlYW47XHJcbiAgc3RlcDogbnVtYmVyO1xyXG4gIHN0ZXBDaGFuZ2VTdWJjcmlvdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHJcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZyA9ICcnO1xyXG4gIEBJbnB1dCgpIGlzVmFsaWQ6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhcmVudDogRm9ybVdpemFyZENvbXBvbmVudCkge1xyXG4gICAgLy8gdGhpcy5zdGVwID0gMDtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmIChjaGFuZ2VzLmlzVmFsaWQpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5pc1ZhbGlkKTtcclxuICAgICAgdGhpcy5wYXJlbnQuaXNWYWxpZCA9IHRoaXMuaXNWYWxpZDtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnN0ZXAgPSB0aGlzLnBhcmVudC5hZGRTdGVwKHRoaXMudGl0bGUpO1xyXG4gICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnN0ZXAgPT09IHRoaXMucGFyZW50LnN0ZXA7XHJcblxyXG4gICAgdGhpcy5zdGVwQ2hhbmdlU3ViY3Jpb3Rpb24gPSB0aGlzLnBhcmVudC5zdGVwQ2hhbmdlLnN1YnNjcmliZShzdGVwID0+IHtcclxuICAgICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnN0ZXAgPT09IHN0ZXA7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMuc3RlcENoYW5nZVN1YmNyaW90aW9uKSB7XHJcbiAgICAgIHRoaXMuc3RlcENoYW5nZVN1YmNyaW90aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==