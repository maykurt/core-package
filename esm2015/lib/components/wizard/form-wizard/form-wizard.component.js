/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToastrUtilsService } from '../../../services/index';
export class FormWizardComponent {
    /**
     * @param {?} toastrUtilsService
     */
    constructor(toastrUtilsService) {
        this.toastrUtilsService = toastrUtilsService;
        this.finishText = 'Finish';
        this.step = 1;
        this.finish = new EventEmitter();
        this.stepChange = new EventEmitter();
        this.steps = [];
        this.isValid = true;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.step) {
            this.stepChange.emit(this.step);
        }
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getClassActive(index) {
        return this.step === index + 1;
    }
    /**
     * @param {?} isNext
     * @return {?}
     */
    onStepChanged(isNext) {
        if (isNext) {
            if (this.isValid == true) {
                this.step = this.step + 1;
                this.stepChange.emit(this.step);
            }
            else {
                this.toastrUtilsService.error('FormIsNotValid', 'NotValid');
            }
        }
        else {
            this.step = this.step - 1;
            this.stepChange.emit(this.step);
        }
    }
    /**
     * @return {?}
     */
    isOnFirstStep() {
        return this.step === 1;
    }
    /**
     * @return {?}
     */
    isOnFinalStep() {
        return this.step === (this.steps ? this.steps.length : 0);
    }
    /**
     * @return {?}
     */
    getClassNameAToSteps() {
        if (this.steps && this.steps.length > 0) {
            return 'wizard wizard-steps-' + this.steps.length;
        }
        else {
            return 'wizard wizard-steps-1';
        }
    }
    /**
     * @param {?} title
     * @return {?}
     */
    addStep(title) {
        /** @type {?} */
        const newStep = {
            Header: title
        };
        this.steps.push(newStep);
        return this.steps.length;
    }
}
FormWizardComponent.decorators = [
    { type: Component, args: [{
                selector: 'form-wizard',
                template: "<ul *ngIf=\"steps\" [className]=\"getClassNameAToSteps()\">\r\n  <li *ngFor=\"let step of steps; let i=index;\">\r\n    <div *ngIf=\"step.Header\" class=\"step-header\">\r\n      {{step.Header}}\r\n    </div>\r\n    <div class=\"step\" [class.complete]=\"step.Completed\" [class.active]=\"getClassActive(i)\">\r\n      <span>{{i + 1}}</span>\r\n    </div>\r\n  </li>\r\n</ul>\r\n<div class=\" col-sm-12 prl-0 ptb-10\">\r\n  <ng-content></ng-content>\r\n</div>\r\n<div class=\"my-wizard__footer\">\r\n  <button class=\"btn btn-warning\" [style.visibility]=\"isOnFirstStep() ? 'hidden' : 'visible'\"\r\n    (click)=\"onStepChanged(false)\">\r\n    <core-icon icon=\"arrow-alt-circle-left\"></core-icon> Previous\r\n  </button>\r\n  <!-- {{step}} / {{steps.length}} -->\r\n  <button class=\"btn btn-info\" *ngIf=\"!isOnFinalStep()\" (click)=\"onStepChanged(true)\">\r\n    Next <core-icon icon=\"arrow-alt-circle-right\"></core-icon>\r\n  </button>\r\n  <!-- <button\r\n    *ngIf=\"isOnFinalStep()\"\r\n    (click)=\"finish.emit(step + 1)\">\r\n    {{finishText}}\r\n  </button> -->\r\n</div>",
                styles: [":host{display:flex;flex-direction:column}.my-wizard__footer{display:flex;align-items:center;justify-content:space-between;flex-shrink:0}.wizard{padding:0;margin:0;list-style:none;width:100%;display:table;position:relative}.wizard .step-header{color:#000;position:relative;width:100%;text-align:center;margin-top:-15px;top:-10px}.wizard>*{display:table-cell;text-align:center}.wizard>* .step{width:30px;height:30px;border-radius:30px;border:1px solid #bdc3c7;color:#bdc3c7;font-weight:300;background:#fff;text-align:center;display:inline-block;position:relative;box-sizing:content-box;cursor:default}.wizard>* .step.complete{border-color:#1abc9c;color:#1abc9c}.wizard>* .step.active{border-color:#3498db;color:#3498db;font-weight:500;border-width:2px;margin-top:-1px}.wizard>* .step:after{content:\" \";display:block;width:30px;height:30px;background-color:#fff;position:absolute;z-index:-1;border:10px solid #fff;top:-10px;left:-10px;box-sizing:content-box}.wizard>* .step>*{line-height:30px}.wizard:after{content:\" \";border-bottom:1px dotted rgba(0,0,0,.2);position:absolute;z-index:-2;top:50%}.wizard-steps-1>*{width:100%}.wizard-steps-1:after{left:50%;right:50%}.wizard-steps-2>*{width:50%}.wizard-steps-2:after{left:25%;right:25%}.wizard-steps-3>*{width:33.33333%}.wizard-steps-3:after{left:16.66667%;right:16.66667%}.wizard-steps-4>*{width:25%}.wizard-steps-4:after{left:12.5%;right:12.5%}.wizard-steps-5>*{width:20%}.wizard-steps-5:after{left:10%;right:10%}.wizard-steps-6>*{width:16.66667%}.wizard-steps-6:after{left:8.33333%;right:8.33333%}.wizard-steps-7>*{width:14.28571%}.wizard-steps-7:after{left:7.14286%;right:7.14286%}.wizard-steps-8>*{width:12.5%}.wizard-steps-8:after{left:6.25%;right:6.25%}.wizard-steps-9>*{width:11.11111%}.wizard-steps-9:after{left:5.55556%;right:5.55556%}.wizard-steps-10>*{width:10%}.wizard-steps-10:after{left:5%;right:5%}.wizard-steps-11>*{width:9.09091%}.wizard-steps-11:after{left:4.54545%;right:4.54545%}.wizard-steps-12>*{width:8.33333%}.wizard-steps-12:after{left:4.16667%;right:4.16667%}.wizard-steps-13>*{width:7.69231%}.wizard-steps-13:after{left:3.84615%;right:3.84615%}.wizard-steps-14>*{width:7.14286%}.wizard-steps-14:after{left:3.57143%;right:3.57143%}.wizard-steps-15>*{width:6.66667%}.wizard-steps-15:after{left:3.33333%;right:3.33333%}.wizard-steps-16>*{width:6.25%}.wizard-steps-16:after{left:3.125%;right:3.125%}.wizard-steps-17>*{width:5.88235%}.wizard-steps-17:after{left:2.94118%;right:2.94118%}.wizard-steps-18>*{width:5.55556%}.wizard-steps-18:after{left:2.77778%;right:2.77778%}.wizard-steps-19>*{width:5.26316%}.wizard-steps-19:after{left:2.63158%;right:2.63158%}.wizard-steps-20>*{width:5%}.wizard-steps-20:after{left:2.5%;right:2.5%}"]
            }] }
];
/** @nocollapse */
FormWizardComponent.ctorParameters = () => [
    { type: ToastrUtilsService }
];
FormWizardComponent.propDecorators = {
    finishText: [{ type: Input }],
    step: [{ type: Input }],
    finish: [{ type: Output }],
    stepChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    FormWizardComponent.prototype.finishText;
    /** @type {?} */
    FormWizardComponent.prototype.step;
    /** @type {?} */
    FormWizardComponent.prototype.finish;
    /** @type {?} */
    FormWizardComponent.prototype.stepChange;
    /** @type {?} */
    FormWizardComponent.prototype.steps;
    /** @type {?} */
    FormWizardComponent.prototype.isValid;
    /**
     * @type {?}
     * @private
     */
    FormWizardComponent.prototype.toastrUtilsService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS13aXphcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvd2l6YXJkL2Zvcm0td2l6YXJkL2Zvcm0td2l6YXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFhLFlBQVksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDakcsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFPN0QsTUFBTSxPQUFPLG1CQUFtQjs7OztJQVM5QixZQUFvQixrQkFBc0M7UUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQVJqRCxlQUFVLEdBQUcsUUFBUSxDQUFDO1FBQ3RCLFNBQUksR0FBVyxDQUFDLENBQUM7UUFDaEIsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDNUIsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFNeEMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQWE7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsTUFBZTtRQUMzQixJQUFJLE1BQU0sRUFBRTtZQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQzdEO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7SUFFRCxvQkFBb0I7UUFDbEIsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN2QyxPQUFPLHNCQUFzQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1NBQ25EO2FBQU07WUFDTCxPQUFPLHVCQUF1QixDQUFDO1NBQ2hDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsS0FBYTs7Y0FDYixPQUFPLEdBQUc7WUFDZCxNQUFNLEVBQUUsS0FBSztTQUNkO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUMzQixDQUFDOzs7WUFqRUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2Qiw2a0NBQTJDOzthQUU1Qzs7OztZQU5RLGtCQUFrQjs7O3lCQVF4QixLQUFLO21CQUNMLEtBQUs7cUJBQ0wsTUFBTTt5QkFDTixNQUFNOzs7O0lBSFAseUNBQStCOztJQUMvQixtQ0FBMEI7O0lBQzFCLHFDQUFzQzs7SUFDdEMseUNBQTBDOztJQUMxQyxvQ0FBYTs7SUFFYixzQ0FBaUI7Ozs7O0lBRUwsaURBQThDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBPbkNoYW5nZXMsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUb2FzdHJVdGlsc1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2Zvcm0td2l6YXJkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS13aXphcmQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydmb3JtLXdpemFyZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtV2l6YXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICBASW5wdXQoKSBmaW5pc2hUZXh0ID0gJ0ZpbmlzaCc7XHJcbiAgQElucHV0KCkgc3RlcDogbnVtYmVyID0gMTtcclxuICBAT3V0cHV0KCkgZmluaXNoID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBzdGVwQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIHN0ZXBzOiBhbnlbXTtcclxuXHJcbiAgaXNWYWxpZDogYm9vbGVhbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSB0b2FzdHJVdGlsc1NlcnZpY2U6IFRvYXN0clV0aWxzU2VydmljZSkge1xyXG4gICAgdGhpcy5zdGVwcyA9IFtdO1xyXG4gICAgdGhpcy5pc1ZhbGlkID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzLnN0ZXApIHtcclxuICAgICAgdGhpcy5zdGVwQ2hhbmdlLmVtaXQodGhpcy5zdGVwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldENsYXNzQWN0aXZlKGluZGV4OiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLnN0ZXAgPT09IGluZGV4ICsgMTtcclxuICB9XHJcblxyXG4gIG9uU3RlcENoYW5nZWQoaXNOZXh0OiBib29sZWFuKTogdm9pZCB7XHJcbiAgICBpZiAoaXNOZXh0KSB7XHJcbiAgICAgIGlmICh0aGlzLmlzVmFsaWQgPT0gdHJ1ZSkge1xyXG4gICAgICAgIHRoaXMuc3RlcCA9IHRoaXMuc3RlcCArIDE7XHJcbiAgICAgICAgdGhpcy5zdGVwQ2hhbmdlLmVtaXQodGhpcy5zdGVwKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnRvYXN0clV0aWxzU2VydmljZS5lcnJvcignRm9ybUlzTm90VmFsaWQnLCAnTm90VmFsaWQnKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zdGVwID0gdGhpcy5zdGVwIC0gMTtcclxuICAgICAgdGhpcy5zdGVwQ2hhbmdlLmVtaXQodGhpcy5zdGVwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzT25GaXJzdFN0ZXAoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdGVwID09PSAxO1xyXG4gIH1cclxuXHJcbiAgaXNPbkZpbmFsU3RlcCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLnN0ZXAgPT09ICh0aGlzLnN0ZXBzID8gdGhpcy5zdGVwcy5sZW5ndGggOiAwKTtcclxuICB9XHJcblxyXG4gIGdldENsYXNzTmFtZUFUb1N0ZXBzKCk6IHN0cmluZyB7XHJcbiAgICBpZiAodGhpcy5zdGVwcyAmJiB0aGlzLnN0ZXBzLmxlbmd0aCA+IDApIHtcclxuICAgICAgcmV0dXJuICd3aXphcmQgd2l6YXJkLXN0ZXBzLScgKyB0aGlzLnN0ZXBzLmxlbmd0aDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiAnd2l6YXJkIHdpemFyZC1zdGVwcy0xJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGFkZFN0ZXAodGl0bGU6IHN0cmluZykge1xyXG4gICAgY29uc3QgbmV3U3RlcCA9IHtcclxuICAgICAgSGVhZGVyOiB0aXRsZVxyXG4gICAgfTtcclxuICAgIHRoaXMuc3RlcHMucHVzaChuZXdTdGVwKTtcclxuICAgIHJldHVybiB0aGlzLnN0ZXBzLmxlbmd0aDtcclxuICB9XHJcbn1cclxuIl19