/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class CoreIconComponent {
}
CoreIconComponent.decorators = [
    { type: Component, args: [{
                template: `
        <fa-icon *ngIf="icon" [icon]="icon"></fa-icon>
    `,
                selector: 'core-icon'
            }] }
];
CoreIconComponent.propDecorators = {
    icon: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CoreIconComponent.prototype.icon;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtaWNvbi9jb3JlLWljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVFqRCxNQUFNLE9BQU8saUJBQWlCOzs7WUFON0IsU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRTs7S0FFVDtnQkFDRCxRQUFRLEVBQUUsV0FBVzthQUN4Qjs7O21CQUVJLEtBQUs7Ozs7SUFBTixpQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGZhLWljb24gKm5nSWY9XCJpY29uXCIgW2ljb25dPVwiaWNvblwiPjwvZmEtaWNvbj5cclxuICAgIGAsXHJcbiAgICBzZWxlY3RvcjogJ2NvcmUtaWNvbidcclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVJY29uQ29tcG9uZW50IHtcclxuICAgIEBJbnB1dCgpIGljb246IHN0cmluZztcclxufVxyXG4iXX0=