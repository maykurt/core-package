/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter, forwardRef, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
export class StaticSelectorComponent {
    constructor() {
        this.valueField = 'Id';
        this.labelField = 'Definition';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.dataSource && this.selector && this.model) {
            if (this.dataSource && this.dataSource.length > 500) {
                this.virtualScroll = true;
            }
            // this.dataSource = [...this.dataSource, changes.dataSource];
            /** @type {?} */
            const index = _.findIndex(this.dataSource, ['Id', this.Model]);
            if (index === -1) {
                this.model = null;
            }
        }
        if (changes.dataSource && this.dataSource) {
            this.dataSource = _.orderBy(this.dataSource, [
                'OrderNo',
                z => z[this._labelField] && (typeof z[this._labelField] === 'string') ? z[this._labelField].toLowerCase() : z[this._labelField],
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                this._labelField
            ], ['asc']);
            this.findDisplayValue();
        }
        if (changes.labelField && this.labelField) {
            if (changes.labelField.currentValue instanceof Array) {
                this._labelField = 'CombinedFields';
            }
            else {
                this._labelField = (/** @type {?} */ (this.labelField));
            }
        }
        if (changes.dataSource || changes.labelField || changes.seperator) {
            if (this.dataSource && this.dataSource.length > 0 && this._labelField === 'CombinedFields') {
                this.dataSource.map((item) => {
                    /** @type {?} */
                    let combinedValue = '';
                    ((/** @type {?} */ (this.labelField))).forEach((field) => {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -this.seperator.length) : null;
                    return item;
                });
                this.findDisplayValue();
            }
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get Model() {
        return this.model;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set Model(val) {
        this.model = val;
        this.onChange(val);
        this.onTouched();
        if (val) {
            if (this.dataSource) {
                /** @type {?} */
                const data = this.dataSource.find(x => x[this.valueField] === this.model);
                if (data) {
                    this.value = data[this._labelField];
                    this.changed.emit(data);
                }
            }
            else {
                /** @type {?} */
                const emitData = {};
                emitData[this.valueField] = this.model;
                this.changed.emit(emitData);
            }
        }
        else {
            this.changed.emit(null);
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.model = value;
        this.findDisplayValue();
    }
    /**
     * @return {?}
     */
    findDisplayValue() {
        // if (this.isReadOnly)
        if (this.dataSource && this.dataSource.length > 0 && (this.model || this.model === 0)) {
            /** @type {?} */
            const data = this.dataSource.find(x => x[this.valueField] === this.model);
            if (data) {
                this.value = data[this._labelField];
                if (!this.isFirstValueSend) {
                    this.firstValue.emit(data);
                }
            }
        }
    }
}
StaticSelectorComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-static-selector',
                template: `
	<ng-select *ngIf="!isReadOnly"
		#selector
      	class="custom-ng-select form-selector"
		[items]="dataSource"
		[searchable]="searchable"
		[clearable]="clearable"
      	[(ngModel)]="Model"
		[bindValue]="valueField"
		[bindLabel]="_labelField"
		[disabled]="isDisabled"
    appendTo= "body"
    [virtualScroll]="virtualScroll"
    placeholder="{{placeholder|translate}}"
		ngDefaultControl
	>
	</ng-select>
	<layout-form-text-input class="readonly-text-input" *ngIf="isReadOnly" [isReadOnly]="isReadOnly" [isDisabled]="isDisabled" [(ngModel)]="value"></layout-form-text-input>
  	`,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => StaticSelectorComponent),
                        multi: true
                    }
                ],
                styles: [".custom-ng-select{height:30px!important;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
            }] }
];
StaticSelectorComponent.propDecorators = {
    dataSource: [{ type: Input }],
    valueField: [{ type: Input }],
    labelField: [{ type: Input }],
    seperator: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    searchable: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }],
    placeholder: [{ type: Input }],
    selector: [{ type: ViewChild, args: ['selector',] }],
    firstValue: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    StaticSelectorComponent.prototype.dataSource;
    /** @type {?} */
    StaticSelectorComponent.prototype.valueField;
    /** @type {?} */
    StaticSelectorComponent.prototype.labelField;
    /** @type {?} */
    StaticSelectorComponent.prototype._labelField;
    /** @type {?} */
    StaticSelectorComponent.prototype.seperator;
    /** @type {?} */
    StaticSelectorComponent.prototype.isDisabled;
    /** @type {?} */
    StaticSelectorComponent.prototype.isReadOnly;
    /** @type {?} */
    StaticSelectorComponent.prototype.searchable;
    /** @type {?} */
    StaticSelectorComponent.prototype.clearable;
    /** @type {?} */
    StaticSelectorComponent.prototype.changed;
    /** @type {?} */
    StaticSelectorComponent.prototype.placeholder;
    /** @type {?} */
    StaticSelectorComponent.prototype.selector;
    /** @type {?} */
    StaticSelectorComponent.prototype.virtualScroll;
    /** @type {?} */
    StaticSelectorComponent.prototype.value;
    /** @type {?} */
    StaticSelectorComponent.prototype.label;
    /** @type {?} */
    StaticSelectorComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    StaticSelectorComponent.prototype.model;
    /** @type {?} */
    StaticSelectorComponent.prototype.firstValue;
    /**
     * @type {?}
     * @private
     */
    StaticSelectorComponent.prototype.isFirstValueSend;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljLXNlbGVjdG9yLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N0YXRpYy1zZWxlY3Rvci9zdGF0aWMtc2VsZWN0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQ3hILE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQWdDNUIsTUFBTSxPQUFPLHVCQUF1QjtJQTlCcEM7UUFpQ1csZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixlQUFVLEdBQXNCLFlBQVksQ0FBQztRQUN0RCxnQkFBVyxHQUFHLFlBQVksQ0FBQztRQUNsQixjQUFTLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDaEIsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7O1FBY2xDLGVBQVUsR0FBdUIsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQWlIckUsQ0FBQzs7Ozs7SUE3R0MsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFFckQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEdBQUcsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7YUFDM0I7OztrQkFHSyxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM5RCxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDbkI7U0FDRjtRQUVELElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUN6QztnQkFDRSxTQUFTO2dCQUNULENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQy9ILG1IQUFtSDtnQkFDbkgsSUFBSSxDQUFDLFdBQVc7YUFDakIsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDZCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUN6QjtRQUVELElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3pDLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLFlBQVksS0FBSyxFQUFFO2dCQUNwRCxJQUFJLENBQUMsV0FBVyxHQUFHLGdCQUFnQixDQUFDO2FBQ3JDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsbUJBQVEsSUFBSSxDQUFDLFVBQVUsRUFBQSxDQUFDO2FBQzVDO1NBQ0Y7UUFFRCxJQUFJLE9BQU8sQ0FBQyxVQUFVLElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO1lBQ2pFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxnQkFBZ0IsRUFBRTtnQkFDMUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTs7d0JBQzVCLGFBQWEsR0FBRyxFQUFFO29CQUN0QixDQUFDLG1CQUFVLElBQUksQ0FBQyxVQUFVLEVBQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQWEsRUFBRSxFQUFFO3dCQUNwRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTs0QkFDZixhQUFhLEdBQUcsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO3lCQUM5RDtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFFSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUMvRixPQUFPLElBQUksQ0FBQztnQkFDZCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUN6QjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQsU0FBUztJQUNULENBQUM7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQzs7Ozs7SUFFRCxJQUFJLEtBQUssQ0FBQyxHQUFHO1FBQ1gsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFHakIsSUFBSSxHQUFHLEVBQUU7WUFDUCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7O3NCQUNiLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDekUsSUFBSSxJQUFJLEVBQUU7b0JBQ1IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekI7YUFDRjtpQkFBTTs7c0JBQ0MsUUFBUSxHQUFRLEVBQUU7Z0JBQ3hCLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDN0I7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDekI7SUFDSCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEVBQU87UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxFQUFPO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQVU7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNkLHVCQUF1QjtRQUN2QixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFOztrQkFDL0UsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQ3pFLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzVCO2FBQ0Y7U0FDRjtJQUNILENBQUM7OztZQXZLRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFrQlI7Z0JBRUYsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsdUJBQXVCLENBQUM7d0JBQ3RELEtBQUssRUFBRSxJQUFJO3FCQUNaO2lCQUNGOzthQUNGOzs7eUJBR0UsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7d0JBRUwsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7eUJBQ0wsS0FBSzt3QkFDTCxLQUFLO3NCQUNMLE1BQU07MEJBQ04sS0FBSzt1QkFFTCxTQUFTLFNBQUMsVUFBVTt5QkFXcEIsTUFBTTs7OztJQXZCUCw2Q0FBMkI7O0lBQzNCLDZDQUEyQjs7SUFDM0IsNkNBQXNEOztJQUN0RCw4Q0FBMkI7O0lBQzNCLDRDQUF5Qjs7SUFDekIsNkNBQTRCOztJQUM1Qiw2Q0FBNEI7O0lBQzVCLDZDQUEyQjs7SUFDM0IsNENBQTBCOztJQUMxQiwwQ0FBNEM7O0lBQzVDLDhDQUE4Qjs7SUFFOUIsMkNBQW1EOztJQUVuRCxnREFBOEI7O0lBRTlCLHdDQUFxQjs7SUFDckIsd0NBQXFCOztJQUNyQiwyQ0FBdUI7Ozs7O0lBRXZCLHdDQUFjOztJQUdkLDZDQUFtRTs7Ozs7SUFDbkUsbURBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgVmlld0NoaWxkLCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTmdTZWxlY3RDb21wb25lbnQgfSBmcm9tICdAbmctc2VsZWN0L25nLXNlbGVjdCc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LXN0YXRpYy1zZWxlY3RvcicsXHJcbiAgdGVtcGxhdGU6IGBcclxuXHQ8bmctc2VsZWN0ICpuZ0lmPVwiIWlzUmVhZE9ubHlcIlxyXG5cdFx0I3NlbGVjdG9yXHJcbiAgICAgIFx0Y2xhc3M9XCJjdXN0b20tbmctc2VsZWN0IGZvcm0tc2VsZWN0b3JcIlxyXG5cdFx0W2l0ZW1zXT1cImRhdGFTb3VyY2VcIlxyXG5cdFx0W3NlYXJjaGFibGVdPVwic2VhcmNoYWJsZVwiXHJcblx0XHRbY2xlYXJhYmxlXT1cImNsZWFyYWJsZVwiXHJcbiAgICAgIFx0WyhuZ01vZGVsKV09XCJNb2RlbFwiXHJcblx0XHRbYmluZFZhbHVlXT1cInZhbHVlRmllbGRcIlxyXG5cdFx0W2JpbmRMYWJlbF09XCJfbGFiZWxGaWVsZFwiXHJcblx0XHRbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICBhcHBlbmRUbz0gXCJib2R5XCJcclxuICAgIFt2aXJ0dWFsU2Nyb2xsXT1cInZpcnR1YWxTY3JvbGxcIlxyXG4gICAgcGxhY2Vob2xkZXI9XCJ7e3BsYWNlaG9sZGVyfHRyYW5zbGF0ZX19XCJcclxuXHRcdG5nRGVmYXVsdENvbnRyb2xcclxuXHQ+XHJcblx0PC9uZy1zZWxlY3Q+XHJcblx0PGxheW91dC1mb3JtLXRleHQtaW5wdXQgY2xhc3M9XCJyZWFkb25seS10ZXh0LWlucHV0XCIgKm5nSWY9XCJpc1JlYWRPbmx5XCIgW2lzUmVhZE9ubHldPVwiaXNSZWFkT25seVwiIFtpc0Rpc2FibGVkXT1cImlzRGlzYWJsZWRcIiBbKG5nTW9kZWwpXT1cInZhbHVlXCI+PC9sYXlvdXQtZm9ybS10ZXh0LWlucHV0PlxyXG4gIFx0YCxcclxuICBzdHlsZVVybHM6IFsnLi9zdGF0aWMtc2VsZWN0b3IuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFN0YXRpY1NlbGVjdG9yQ29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdGF0aWNTZWxlY3RvckNvbXBvbmVudCBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBPbkNoYW5nZXMge1xyXG5cclxuICBASW5wdXQoKSBkYXRhU291cmNlOiBhbnlbXTtcclxuICBASW5wdXQoKSB2YWx1ZUZpZWxkID0gJ0lkJztcclxuICBASW5wdXQoKSBsYWJlbEZpZWxkOiBzdHJpbmcgfCBzdHJpbmdbXSA9ICdEZWZpbml0aW9uJztcclxuICBfbGFiZWxGaWVsZCA9ICdEZWZpbml0aW9uJztcclxuICBASW5wdXQoKSBzZXBlcmF0b3IgPSAnICc7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBzZWFyY2hhYmxlID0gdHJ1ZTtcclxuICBASW5wdXQoKSBjbGVhcmFibGUgPSB0cnVlO1xyXG4gIEBPdXRwdXQoKSBjaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3NlbGVjdG9yJykgc2VsZWN0b3I6IE5nU2VsZWN0Q29tcG9uZW50O1xyXG5cclxuICBwdWJsaWMgdmlydHVhbFNjcm9sbDogYm9vbGVhbjtcclxuXHJcbiAgcHVibGljIHZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIGxhYmVsOiBzdHJpbmc7XHJcbiAgcHVibGljIGxpc3REYXRhOiBhbnlbXTtcclxuXHJcbiAgcHJpdmF0ZSBtb2RlbDtcclxuXHJcbiAgLy8gaXQgaXMgZm9yIGdldHRpbmcgdGhlIGZpcnN0IHNlbGVjdGVkIHZhbHVlXHJcbiAgQE91dHB1dCgpIGZpcnN0VmFsdWU/OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIHByaXZhdGUgaXNGaXJzdFZhbHVlU2VuZDogYm9vbGVhbjtcclxuXHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmIChjaGFuZ2VzLmRhdGFTb3VyY2UgJiYgdGhpcy5zZWxlY3RvciAmJiB0aGlzLm1vZGVsKSB7XHJcblxyXG4gICAgICBpZiAodGhpcy5kYXRhU291cmNlICYmIHRoaXMuZGF0YVNvdXJjZS5sZW5ndGggPiA1MDApIHtcclxuICAgICAgICB0aGlzLnZpcnR1YWxTY3JvbGwgPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICAgIC8vIHRoaXMuZGF0YVNvdXJjZSA9IFsuLi50aGlzLmRhdGFTb3VyY2UsIGNoYW5nZXMuZGF0YVNvdXJjZV07XHJcblxyXG4gICAgICBjb25zdCBpbmRleCA9IF8uZmluZEluZGV4KHRoaXMuZGF0YVNvdXJjZSwgWydJZCcsIHRoaXMuTW9kZWxdKTtcclxuICAgICAgaWYgKGluZGV4ID09PSAtMSkge1xyXG4gICAgICAgIHRoaXMubW9kZWwgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNoYW5nZXMuZGF0YVNvdXJjZSAmJiB0aGlzLmRhdGFTb3VyY2UpIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlID0gXy5vcmRlckJ5KHRoaXMuZGF0YVNvdXJjZSxcclxuICAgICAgICBbXHJcbiAgICAgICAgICAnT3JkZXJObycsXHJcbiAgICAgICAgICB6ID0+IHpbdGhpcy5fbGFiZWxGaWVsZF0gJiYgKHR5cGVvZiB6W3RoaXMuX2xhYmVsRmllbGRdID09PSAnc3RyaW5nJykgPyB6W3RoaXMuX2xhYmVsRmllbGRdLnRvTG93ZXJDYXNlKCkgOiB6W3RoaXMuX2xhYmVsRmllbGRdLFxyXG4gICAgICAgICAgLy8geSA9PiB5WydEZWZpbml0aW9uJ10gJiYgKHR5cGVvZiB5WydEZWZpbml0aW9uJ10gPT09ICdzdHJpbmcnKSA/IHlbJ0RlZmluaXRpb24nXS50b0xvd2VyQ2FzZSgpIDogeVsnRGVmaW5pdGlvbiddLFxyXG4gICAgICAgICAgdGhpcy5fbGFiZWxGaWVsZFxyXG4gICAgICAgIF0sIFsnYXNjJ10pO1xyXG4gICAgICB0aGlzLmZpbmREaXNwbGF5VmFsdWUoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoY2hhbmdlcy5sYWJlbEZpZWxkICYmIHRoaXMubGFiZWxGaWVsZCkge1xyXG4gICAgICBpZiAoY2hhbmdlcy5sYWJlbEZpZWxkLmN1cnJlbnRWYWx1ZSBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgdGhpcy5fbGFiZWxGaWVsZCA9ICdDb21iaW5lZEZpZWxkcyc7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5fbGFiZWxGaWVsZCA9IDxzdHJpbmc+dGhpcy5sYWJlbEZpZWxkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNoYW5nZXMuZGF0YVNvdXJjZSB8fCBjaGFuZ2VzLmxhYmVsRmllbGQgfHwgY2hhbmdlcy5zZXBlcmF0b3IpIHtcclxuICAgICAgaWYgKHRoaXMuZGF0YVNvdXJjZSAmJiB0aGlzLmRhdGFTb3VyY2UubGVuZ3RoID4gMCAmJiB0aGlzLl9sYWJlbEZpZWxkID09PSAnQ29tYmluZWRGaWVsZHMnKSB7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlLm1hcCgoaXRlbTogYW55KSA9PiB7XHJcbiAgICAgICAgICBsZXQgY29tYmluZWRWYWx1ZSA9ICcnO1xyXG4gICAgICAgICAgKDxzdHJpbmdbXT50aGlzLmxhYmVsRmllbGQpLmZvckVhY2goKGZpZWxkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKGl0ZW1bZmllbGRdKSB7XHJcbiAgICAgICAgICAgICAgY29tYmluZWRWYWx1ZSA9IGNvbWJpbmVkVmFsdWUgKyBpdGVtW2ZpZWxkXSArIHRoaXMuc2VwZXJhdG9yO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICBpdGVtWydDb21iaW5lZEZpZWxkcyddID0gY29tYmluZWRWYWx1ZSA/IGNvbWJpbmVkVmFsdWUuc2xpY2UoMCwgLXRoaXMuc2VwZXJhdG9yLmxlbmd0aCkgOiBudWxsO1xyXG4gICAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5maW5kRGlzcGxheVZhbHVlKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgTW9kZWwoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5tb2RlbDtcclxuICB9XHJcblxyXG4gIHNldCBNb2RlbCh2YWwpIHtcclxuICAgIHRoaXMubW9kZWwgPSB2YWw7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG5cclxuXHJcbiAgICBpZiAodmFsKSB7XHJcbiAgICAgIGlmICh0aGlzLmRhdGFTb3VyY2UpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5kYXRhU291cmNlLmZpbmQoeCA9PiB4W3RoaXMudmFsdWVGaWVsZF0gPT09IHRoaXMubW9kZWwpO1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICB0aGlzLnZhbHVlID0gZGF0YVt0aGlzLl9sYWJlbEZpZWxkXTtcclxuICAgICAgICAgIHRoaXMuY2hhbmdlZC5lbWl0KGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBlbWl0RGF0YTogYW55ID0ge307XHJcbiAgICAgICAgZW1pdERhdGFbdGhpcy52YWx1ZUZpZWxkXSA9IHRoaXMubW9kZWw7XHJcbiAgICAgICAgdGhpcy5jaGFuZ2VkLmVtaXQoZW1pdERhdGEpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNoYW5nZWQuZW1pdChudWxsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm46IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm46IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5tb2RlbCA9IHZhbHVlO1xyXG4gICAgdGhpcy5maW5kRGlzcGxheVZhbHVlKCk7XHJcbiAgfVxyXG5cclxuICBmaW5kRGlzcGxheVZhbHVlKCk6IHZvaWQge1xyXG4gICAgLy8gaWYgKHRoaXMuaXNSZWFkT25seSlcclxuICAgIGlmICh0aGlzLmRhdGFTb3VyY2UgJiYgdGhpcy5kYXRhU291cmNlLmxlbmd0aCA+IDAgJiYgKHRoaXMubW9kZWwgfHwgdGhpcy5tb2RlbCA9PT0gMCkpIHtcclxuICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGF0YVNvdXJjZS5maW5kKHggPT4geFt0aGlzLnZhbHVlRmllbGRdID09PSB0aGlzLm1vZGVsKTtcclxuICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICB0aGlzLnZhbHVlID0gZGF0YVt0aGlzLl9sYWJlbEZpZWxkXTtcclxuICAgICAgICBpZiAoIXRoaXMuaXNGaXJzdFZhbHVlU2VuZCkge1xyXG4gICAgICAgICAgdGhpcy5maXJzdFZhbHVlLmVtaXQoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==