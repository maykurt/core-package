/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SystemReferenceDateService, StorageService } from '../../services/index';
import { StorageType, Storage } from '../../enums/index';
export class SystemReferenceDateComponent {
    /**
     * @param {?} systemReferenceDateService
     * @param {?} storageService
     * @param {?} fb
     * @param {?} translate
     */
    constructor(systemReferenceDateService, storageService, fb, translate) {
        this.systemReferenceDateService = systemReferenceDateService;
        this.storageService = storageService;
        this.fb = fb;
        this.translate = translate;
        /** @type {?} */
        const currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        this._calendarDateValue = currentsystemRefDate ?
            this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
            this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.locale = event.lang === 'tr' ? this.localeTr : this.localEn;
            }
        });
        this.systemReferenceDateService.getSelectedReferenceDate()
            .subscribe((selectedRefDate) => {
            if (selectedRefDate !== '') {
                this._calendarDateValue = this.systemReferenceDateService.convertDateToDateUTC(selectedRefDate);
            }
        });
    }
    /**
     * @return {?}
     */
    get calendarDateValue() {
        return this._calendarDateValue;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set calendarDateValue(value) {
        this._calendarDateValue = value;
        // this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
    }
    /**
     * @param {?} selectedDateISO
     * @return {?}
     */
    setSelectedDate(selectedDateISO) {
        this.systemReferenceDateService.setSelectedReferenceDate(selectedDateISO);
    }
    /**
     * @return {?}
     */
    onClose() {
        this.checkAndApplyDate();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.checkAndApplyDate();
            if (this._calendarDateValue && this.systemRefDatePicker && this.systemRefDatePicker.overlayVisible) {
                this.systemRefDatePicker.overlayVisible = false;
            }
        }
    }
    /**
     * @return {?}
     */
    checkAndApplyDate() {
        /** @type {?} */
        const currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (this._calendarDateValue) {
            /** @type {?} */
            const selectedDate = this.systemReferenceDateService.convertDateToISO(this._calendarDateValue);
            if (currentsystemRefDate !== selectedDate) {
                this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
            }
        }
        else {
            this._calendarDateValue = currentsystemRefDate ?
                this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
                this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
        if (this.sysRefDateSubscription) {
            this.sysRefDateSubscription.unsubscribe();
        }
    }
}
SystemReferenceDateComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-system-reference-date',
                template: " <div class=\"system-reference-container\">\r\n   <p-calendar #systemRefDatePicker class=\"system-reference-datepicker\" [(ngModel)]=\"calendarDateValue\"\r\n     [dateFormat]=\"'dd.mm.yy'\" [showIcon]=\"false\" [showButtonBar]=\"true\" [style]=\"{'width':'100%', 'height': '30px'}\"\r\n     monthNavigator=\"true\" yearNavigator=\"true\" yearRange=\"1930:2030\" [locale]=\"locale\" (onClose)=\"onClose()\"\r\n     (keydown)=\"keyDownFunction($event)\" appendTo=\"body\">\r\n   </p-calendar>\r\n </div>\r\n",
                styles: [":host /deep/ .ui-calendar .ui-inputtext{height:30px!important;font-size:inherit;width:calc(100%);color:#fff;border:none;cursor:pointer;background:#e30a3a!important;border-color:rgba(255,255,255,.2)!important}:host /deep/ .ui-datepicker table td{padding:.2em!important;font-size:12px}:host /deep/ .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}:host /deep/ .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}"]
            }] }
];
/** @nocollapse */
SystemReferenceDateComponent.ctorParameters = () => [
    { type: SystemReferenceDateService },
    { type: StorageService },
    { type: FormBuilder },
    { type: TranslateService }
];
SystemReferenceDateComponent.propDecorators = {
    systemRefDatePicker: [{ type: ViewChild, args: ['systemRefDatePicker',] }]
};
if (false) {
    /** @type {?} */
    SystemReferenceDateComponent.prototype.systemRefDatePicker;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.sysRefDateFormGroup;
    /** @type {?} */
    SystemReferenceDateComponent.prototype._calendarDateValue;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.locale;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.localeTr;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.localEn;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.translateSubscription;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.sysRefDateSubscription;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.systemReferenceDateService;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.storageService;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXJlZmVyZW5jZS1kYXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N5c3RlbS1kYXRlLXBpY2tlci9zeXN0ZW0tcmVmZXJlbmNlLWRhdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFhLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRSxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdkQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLGNBQWMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFRekQsTUFBTSxPQUFPLDRCQUE0Qjs7Ozs7OztJQWF2QyxZQUNVLDBCQUFzRCxFQUN0RCxjQUE4QixFQUM5QixFQUFlLEVBQ2YsU0FBMkI7UUFIM0IsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLGNBQVMsR0FBVCxTQUFTLENBQWtCOztjQUM3QixvQkFBb0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDN0csSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsMEJBQTBCLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNuSCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBRWhHLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsY0FBYyxFQUFFLENBQUM7WUFDakIsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDO1lBQ3hGLGFBQWEsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNoRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7WUFDdkQsVUFBVSxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUM7WUFDdEksZUFBZSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDckcsS0FBSyxFQUFFLE9BQU87WUFDZCxLQUFLLEVBQUUsT0FBTztTQUNmLENBQUM7UUFDRiwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRztZQUNkLGNBQWMsRUFBRSxDQUFDO1lBQ2pCLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQztZQUNyRixhQUFhLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDaEUsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQ3ZELFVBQVUsRUFBRSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDO1lBQzNILGVBQWUsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDO1lBQ3JHLEtBQUssRUFBRSxPQUFPO1lBQ2QsS0FBSyxFQUFFLFNBQVM7U0FDakIsQ0FBQztRQUNGLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUNyRCxTQUFTLENBQUMsQ0FBQyxLQUFVLEVBQUUsRUFBRTtZQUN4QixJQUFJLEtBQUssRUFBRTtnQkFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ2xFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLENBQUMsMEJBQTBCLENBQUMsd0JBQXdCLEVBQUU7YUFDdkQsU0FBUyxDQUFDLENBQUMsZUFBdUIsRUFBRSxFQUFFO1lBQ3JDLElBQUksZUFBZSxLQUFLLEVBQUUsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNqRztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELElBQUksaUJBQWlCO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsSUFBSSxpQkFBaUIsQ0FBQyxLQUFXO1FBQy9CLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDaEMsbUdBQW1HO0lBQ3JHLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLGVBQXVCO1FBQ3JDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyx3QkFBd0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM1RSxDQUFDOzs7O0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBR0QsZUFBZSxDQUFDLEtBQVU7UUFDeEIsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLEVBQUUsRUFBRTtZQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUN6QixJQUFJLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRTtnQkFDbEcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7YUFDakQ7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxpQkFBaUI7O2NBQ1Qsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsY0FBYyxDQUFDO1FBQzdHLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFOztrQkFDckIsWUFBWSxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDOUYsSUFBSSxvQkFBb0IsS0FBSyxZQUFZLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7YUFDakc7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7Z0JBQzVFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3BIO0lBQ0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7UUFDRCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUMvQixJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDM0M7SUFDSCxDQUFDOzs7WUFwSEYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSw4QkFBOEI7Z0JBQ3hDLHNnQkFBcUQ7O2FBRXREOzs7O1lBUlEsMEJBQTBCO1lBQUUsY0FBYztZQUovQixXQUFXO1lBRXRCLGdCQUFnQjs7O2tDQVl0QixTQUFTLFNBQUMscUJBQXFCOzs7O0lBQWhDLDJEQUFzRDs7SUFFdEQsMkRBQStCOztJQUMvQiwwREFBeUI7O0lBRXpCLDhDQUFZOztJQUNaLGdEQUFjOztJQUNkLCtDQUFhOztJQUNiLDZEQUFvQzs7SUFDcEMsOERBQXFDOzs7OztJQUluQyxrRUFBOEQ7Ozs7O0lBQzlELHNEQUFzQzs7Ozs7SUFDdEMsMENBQXVCOzs7OztJQUN2QixpREFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBTeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZSwgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9pbmRleCc7XHJcbmltcG9ydCB7IFN0b3JhZ2VUeXBlLCBTdG9yYWdlIH0gZnJvbSAnLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LXN5c3RlbS1yZWZlcmVuY2UtZGF0ZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3N5c3RlbS1yZWZlcmVuY2UtZGF0ZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc3lzdGVtLXJlZmVyZW5jZS1kYXRlLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFN5c3RlbVJlZmVyZW5jZURhdGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG4gIEBWaWV3Q2hpbGQoJ3N5c3RlbVJlZkRhdGVQaWNrZXInKSBzeXN0ZW1SZWZEYXRlUGlja2VyO1xyXG5cclxuICBzeXNSZWZEYXRlRm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcbiAgX2NhbGVuZGFyRGF0ZVZhbHVlOiBEYXRlO1xyXG5cclxuICBsb2NhbGU6IGFueTtcclxuICBsb2NhbGVUcjogYW55O1xyXG4gIGxvY2FsRW46IGFueTtcclxuICB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBzeXNSZWZEYXRlU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2U6IFN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBzdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XHJcbiAgICBjb25zdCBjdXJyZW50c3lzdGVtUmVmRGF0ZSA9IHRoaXMuc3RvcmFnZVNlcnZpY2UuZ2V0U3RvcmVkKFN0b3JhZ2UuU3lzdGVtUmVmRGF0ZSwgU3RvcmFnZVR5cGUuU2Vzc2lvblN0b3JhZ2UpO1xyXG4gICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSBjdXJyZW50c3lzdGVtUmVmRGF0ZSA/XHJcbiAgICAgIHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0RhdGVVVEMoY3VycmVudHN5c3RlbVJlZkRhdGUpIDpcclxuICAgICAgdGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLmNvbnZlcnREYXRlVG9JU08obmV3IERhdGUpKTtcclxuICAgIHRoaXMuc2V0U2VsZWN0ZWREYXRlKHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0lTTyh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkpO1xyXG5cclxuICAgIC8vIHVudGlsIHRoZSBiZXR0ZXIgc29sdXRpb24gZm9yIHRyYW5zbGF0ZS5cclxuICAgIHRoaXMubG9jYWxFbiA9IHtcclxuICAgICAgZmlyc3REYXlPZldlZWs6IDEsXHJcbiAgICAgIGRheU5hbWVzOiBbJ1N1bmRheScsICdNb25kYXknLCAnVHVlc2RheScsICdXZWRuZXNkYXknLCAnVGh1cnNkYXknLCAnRnJpZGF5JywgJ1NhdHVyZGF5J10sXHJcbiAgICAgIGRheU5hbWVzU2hvcnQ6IFsnU3VuJywgJ01vbicsICdUdWUnLCAnV2VkJywgJ1RodScsICdGcmknLCAnU2F0J10sXHJcbiAgICAgIGRheU5hbWVzTWluOiBbJ1N1JywgJ01vJywgJ1R1JywgJ1dlJywgJ1RoJywgJ0ZyJywgJ1NhJ10sXHJcbiAgICAgIG1vbnRoTmFtZXM6IFsnSmFudWFyeScsICdGZWJydWFyeScsICdNYXJjaCcsICdBcHJpbCcsICdNYXknLCAnSnVuZScsICdKdWx5JywgJ0F1Z3VzdCcsICdTZXB0ZW1iZXInLCAnT2N0b2JlcicsICdOb3ZlbWJlcicsICdEZWNlbWJlciddLFxyXG4gICAgICBtb250aE5hbWVzU2hvcnQ6IFsnSmFuJywgJ0ZlYicsICdNYXInLCAnQXByJywgJ01heScsICdKdW4nLCAnSnVsJywgJ0F1ZycsICdTZXAnLCAnT2N0JywgJ05vdicsICdEZWMnXSxcclxuICAgICAgdG9kYXk6ICdUb2RheScsXHJcbiAgICAgIGNsZWFyOiAnQ2xlYXInXHJcbiAgICB9O1xyXG4gICAgLy8gdW50aWwgdGhlIGJldHRlciBzb2x1dGlvbiBmb3IgdHJhbnNsYXRlLlxyXG4gICAgdGhpcy5sb2NhbGVUciA9IHtcclxuICAgICAgZmlyc3REYXlPZldlZWs6IDEsXHJcbiAgICAgIGRheU5hbWVzOiBbJ1BhemFyJywgJ1BhemFydGVzaScsICdTYWzEsScsICfDh2FyxZ9hbWJhJywgJ1BlcsWfZW1iZScsICdDdW1hJywgJ0N1bWFydGVzaSddLFxyXG4gICAgICBkYXlOYW1lc1Nob3J0OiBbJ1BheicsICdQdHMnLCAnU2FsJywgJ8OHYXInLCAnUGVyJywgJ0N1bScsICdDdHMnXSxcclxuICAgICAgZGF5TmFtZXNNaW46IFsnUHonLCAnUHQnLCAnU2wnLCAnw4dyJywgJ1ByJywgJ0NtJywgJ0N0J10sXHJcbiAgICAgIG1vbnRoTmFtZXM6IFsnT2NhaycsICfFnnViYXQnLCAnTWFydCcsICdOaXNhbicsICdNYXnEsXMnLCAnSGF6aXJhbicsICdUZW1tdXonLCAnQcSfdXN0b3MnLCAnRXlsw7xsJywgJ0VraW0nLCAnS2FzxLFtJywgJ0FyYWzEsWsnXSxcclxuICAgICAgbW9udGhOYW1lc1Nob3J0OiBbJ09jYScsICfFnnViJywgJ01hcicsICdOaXMnLCAnTWF5JywgJ0hheicsICdUZW0nLCAnQcSfdScsICdFeWwnLCAnRWtpJywgJ0thcycsICdBcmEnXSxcclxuICAgICAgdG9kYXk6ICdCdWfDvG4nLFxyXG4gICAgICBjbGVhcjogJ1RlbWl6bGUnXHJcbiAgICB9O1xyXG4gICAgLy8gdW50aWwgdGhlIGJldHRlciBzb2x1dGlvbiBmb3IgdHJhbnNsYXRlLlxyXG4gICAgdGhpcy5sb2NhbGUgPSB0aGlzLmxvY2FsZVRyO1xyXG4gICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24gPSB0aGlzLnRyYW5zbGF0ZS5vbkxhbmdDaGFuZ2VcclxuICAgICAgLnN1YnNjcmliZSgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgdGhpcy5sb2NhbGUgPSBldmVudC5sYW5nID09PSAndHInID8gdGhpcy5sb2NhbGVUciA6IHRoaXMubG9jYWxFbjtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuZ2V0U2VsZWN0ZWRSZWZlcmVuY2VEYXRlKClcclxuICAgICAgLnN1YnNjcmliZSgoc2VsZWN0ZWRSZWZEYXRlOiBzdHJpbmcpID0+IHtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRSZWZEYXRlICE9PSAnJykge1xyXG4gICAgICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSB0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLmNvbnZlcnREYXRlVG9EYXRlVVRDKHNlbGVjdGVkUmVmRGF0ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldCBjYWxlbmRhckRhdGVWYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBjYWxlbmRhckRhdGVWYWx1ZSh2YWx1ZTogRGF0ZSkge1xyXG4gICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSB2YWx1ZTtcclxuICAgIC8vIHRoaXMuc2V0U2VsZWN0ZWREYXRlKHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0lTTyh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkpO1xyXG4gIH1cclxuXHJcbiAgc2V0U2VsZWN0ZWREYXRlKHNlbGVjdGVkRGF0ZUlTTzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLnNldFNlbGVjdGVkUmVmZXJlbmNlRGF0ZShzZWxlY3RlZERhdGVJU08pO1xyXG4gIH1cclxuXHJcbiAgb25DbG9zZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2hlY2tBbmRBcHBseURhdGUoKTtcclxuICB9XHJcblxyXG5cclxuICBrZXlEb3duRnVuY3Rpb24oZXZlbnQ6IGFueSkge1xyXG4gICAgaWYgKGV2ZW50LmtleUNvZGUgPT09IDEzKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tBbmRBcHBseURhdGUoKTtcclxuICAgICAgaWYgKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlICYmIHRoaXMuc3lzdGVtUmVmRGF0ZVBpY2tlciAmJiB0aGlzLnN5c3RlbVJlZkRhdGVQaWNrZXIub3ZlcmxheVZpc2libGUpIHtcclxuICAgICAgICB0aGlzLnN5c3RlbVJlZkRhdGVQaWNrZXIub3ZlcmxheVZpc2libGUgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY2hlY2tBbmRBcHBseURhdGUoKTogdm9pZCB7XHJcbiAgICBjb25zdCBjdXJyZW50c3lzdGVtUmVmRGF0ZSA9IHRoaXMuc3RvcmFnZVNlcnZpY2UuZ2V0U3RvcmVkKFN0b3JhZ2UuU3lzdGVtUmVmRGF0ZSwgU3RvcmFnZVR5cGUuU2Vzc2lvblN0b3JhZ2UpO1xyXG4gICAgaWYgKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IHNlbGVjdGVkRGF0ZSA9IHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0lTTyh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSk7XHJcbiAgICAgIGlmIChjdXJyZW50c3lzdGVtUmVmRGF0ZSAhPT0gc2VsZWN0ZWREYXRlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTZWxlY3RlZERhdGUodGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvSVNPKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlID0gY3VycmVudHN5c3RlbVJlZkRhdGUgP1xyXG4gICAgICAgIHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0RhdGVVVEMoY3VycmVudHN5c3RlbVJlZkRhdGUpIDpcclxuICAgICAgICB0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLmNvbnZlcnREYXRlVG9EYXRlVVRDKHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0lTTyhuZXcgRGF0ZSkpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnN5c1JlZkRhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5zeXNSZWZEYXRlU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==