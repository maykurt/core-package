/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output, Input, NgZone } from '@angular/core';
// @Inject(DOCUMENT) private document: Document
export class CoreOrgChartComponent {
    /**
     * @param {?} ngZone
     */
    constructor(ngZone) {
        this.ngZone = ngZone;
        this.clickedExport = new EventEmitter();
    }
    /**
     * @param {?} simpleChanges
     * @return {?}
     */
    ngOnChanges(simpleChanges) {
        if (simpleChanges.export && this.export) {
            this.onExport();
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.datasource = {
            name: 'A',
            title: 'Yönetim Kurulu Başkanı',
            className: 'org-node-blue',
            gender: 'M',
            office: 'Ankara',
            imgUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAvCAYAAABOmTCPAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAEmJJREFUeJztnQtUU1e6gK3WKgVR3oS8ICQQEgLhaZAY5SUvBQGFKmqtiIooUh+oKFqtU2d0zbTT1lerrk7vmtVqOzNt1207Tqe3qzO3M7Vzrd65szq9vY7kvPMgBAStWvXcvU8IQnJyEsAAsfxr7RUXyb/3f/79nX+//nOcNGlCJmRCHgipLTxEyNSfEgnpv2cruET1KZmqvdC5tknnbVuun3rd31hU/htkRoQZi074FyaWsxe+jKCy87c56mMiVQIWLf8O/Kbdpa5AhhLSpHYiRZvlqG9pPRCPy1P+jvGlqCt9NCoWxWVJ7abyJWXe9gdVvDQfF8gITBjn8npwsRzDoxVfYGJFlLft8SkhYhP/jIYIaSw8mrWggZE0Lk6gO5atXu5tW3qOnQg05pVcQZ4IorEoKY3xYtlLmJgmM3RHHPWR8Og0lBfjWg8WcE24UE4TiowCR31ry/5kXKK8C+t3pY+GiGgAFG0sKVvlbX8YcxZVMf0QwX1NaKSkE42IEXvbHp8S4JQLIALaQGIpaIiAxmNVdEdtXYW3bek5fjLAmF/6V2RaMM1lE+xsMnPe8476SGSsGkTUOy71YAGQ4KIEmlBm5jjqW3fuV+GxiT0MTK78EQqgj06gjaXly7ztD0NeWRkDdaTE9fVAm3ixGChCb9vjU4LwJJ9wOi24H+xKb9vykMD+YQLsCZlEZuo+w4KFLkGaAHsCbJ8Uo27Bn9BZggmwJ02A/UgJlamzLR4nwPZlsNEJsB2ESM76AoW7AOMA7N4TJwMB2N95BHaG7iVH/ZGC3dmyLxWATeMegG0qLVvjbX9QmvwaBmoOsHEb2LeRSEmMt+3xKSHU2QBs0bgAuxtEbEN+yZv6aUH/jfKllwCk7CVcfIWcnbvJUX+kYHft3h8HwP5PJFx82VXbSJjoMhot/5oqqSz1tj+My1bngfauoBHRX7u0JyoW2Cr9BOXLeN62x6cEl6d+yTn0jiLYUIj8Ur9r04Nn6gVSfz0/lr1EiGYYsoumOuqOFOwbew9OxqQq/2vhogCXbYcLA5DohACsqGaKt31hffkXk/XBUYH6cLFLe65FSWaggjg/MlrxmLft8SnBpaqvmHkcF9jSJLqjspYVbGJuwePEvAV+hDbXj8wueHy07R8oIwV7Qh4IpcmdbK7bwDM1bk0xrducbV6/Kd3U0urxqGAsr5lmfbY1xlD9dKa5EeivbUw1Vq8K96bNgwSXJbkHO0ZJW5av6QfbUFIZb6zbsI7KLngVT9Z8hKdoLuJJmReJJM2HBl3hcUNpdZOxqlZjrm+YNhRbbr569gmwYFrS7jdrCyqUNqKCWPYSKd6szyrSOOqPFGxz66EQTJZUj0aINrlqG0TsTWiMvNFYUhXnyTUZ122aQWbqVIZFNSuMRVVtVMa8X1KFC08bK6rP4uqs49T8wkNUbvEqQ/lSpzmyde/hGCRUsBmJELv2BV+yGUTs1YhIMcNRv+vgIb5l+y5tx47dGtby7HadZc++tO4LFwL67a1aLuh4um4dmZL9OzBSk5gw/g4mkN0Dnz9gsYkkqc1701zX4HRq26+/ZEUaVVy5n0jK/DNYi1gxYdxdTAj0BXF3QIBEyPmFZ81bd+R64rsRCSqQ/RfG41hxz+LTwKgfeo68OK/zhZ8qiaycX+PihOvozEgaDQLfhYL5eYjQdtQMFqHwb8zfeRKazM77jMwpWmNpevYJT2zpOX4CLh6/9WxXRPeio/7Id0X2pcDFo2e7ImV1XNdCli0XE3Ny28is3C/x6IS7cOcJDYpi/IXO5NFoQAT4N/BXsJBGZvFoImXONx0NzaGD6phTUOPhrsgtlGXxSGpy2tBw0B4vhrUgAWGg3SxL1yuvhnbUN042Ln6qDZMmIkhgJBPQ+o/zYfsRElv/QtuDBLShuOLgwLaMzS0CQ27pafD7u4x+qBDoRD/QhZ99KRoIqJssXnzGsn1PgKPND03QyJhLnCDA3AqR/IaxsOxjXKKkEL9QALLI9h2Ej61EgRIOHOcfwegTaXO+ImtWuR36H5XtPqK4fCMmS9ZjoAMZgOGuE4e/UNgeX9Zh1BUP2rIb6T62IX1eG7xxsLBo1oL4h9OkOktv3b6rFE/M+JABclbUA1vZ2oP2ArCRJ8Npy/rG3bCd7qNHs1FxwjUU1IeGRQ/ggE0/juFHP3UmvDl+8/35cx4FvSELypNwg81nPu8j8E6F0ZvLaBYnQEigA5FZgu+tO1pXc9nyKIBtqd/4cyQwgolsGExS8sBfEGww7GPmhdWDMvRGDLYmtxXlmGYyRSTvxWJV3f2JcJ70rTCeRgLB9cUoblp3tu4kU7Pa0SfDuG+IgUUQB/wootunBNLWHS173EI6HAFz7Ctcc+z+AjvJE5hZC7jLgSOQgMj7vadPVruyxdfBtjbvaNBPC2Eimsc3vx1sSSJmLl06umDDoAXqhpD134Se9mlf0IJTUGZU4A+RCQg3GB1QUXzX9b37ZJ4T66EQiRn/QD29U0dS7Hd5eEx3z7HjKjZbfBnsnpdf5ZGJ6RQyPYTptKH4ZszAfhhlJAEP+AmO5pY161qGTq4bIRRp/zMqYPfBrQfQGhcs/OyHP/7eaR/Yl8E2121cq/cLGVpHM/nUA6Yivgj2SAozXxfSZNb8PwyPXg4ZVbAZKMGiEixSrAcOVTna4stgG6trTyDTPIjWsDPhzsgMMA+HOyVgOEb8w+AC3QzAHrRP/MiDDQu4Nlyi/HZ49HIIEaf+BsI2ZLCddkI814XDD5Vb+ImjLb4KNpGR/TguV3+AhbkJEHxb+2DBdoeau+B9SpPzAjU37wyePPsyIVZgFm3hoC07nwPbfu1DmW+DUQvc1O3DJ9iF4NKkfzL7jJ4aDp0MFxtw4QAXSXDhACNQqNjji4G7K3hcUk/nrlb1QFt8FWxMkTIVi1X+O7NvG+VmBwREafPKZ3YMbNeyfsss85aWLP1zh4MG/n1UwYaPmMFtSeZMQkh7zITdBnhTQz24mIR1QEag3e6Cng3sa8Nhl1NwmYdgwyG0b4Mej0tuJ9LnHrfUb9xqqlm5G1dnvYtLVT0IBD3CffRn6gE3RMe6DYMSmXwVbAaigoVn4JSC2afluvZgAW1tafEogWpUwIZ+Br+BfQLm+RYiWfNXIjH9G2gnVzrzIH3oEzilyNS9ZVq/eaX5mfV1aKzyU2b/3h3cYwp2X6SGHQfmkr80b93plDNg2bRV0x4i+htzYsVxkmm/GLjBbyyrOjuwDl8Gu2P9pia3i0cYHMBoReUV/an35DG3SVSjAjbc7gM2k6rM8x2NzQnMtezZP920uq4VE8qYE1N3QQqMVHese/YPemuAZc8+f+PCqj8g00PHKdh98yUIG4jQr3DVdf3s6TAqO+8qnENzOhM6ZAaPptLnft65paW/g3tOnPJZsHt/e04OAOlmrp1rAcmT2La4Vq05465vvL+PLaMRsIglNPP/YWna7nQCaF5e+y7iF8Z9s4KRF6wv2i07WwOd/Hn4aBU82GOmruMPbHDx4OJwVcbVO19+EeSuPutPD9cgwYK7zIKUq87ASJpM116xNG/vr9OXwYbS2dj0QvtjMx4cK7uK2iAKwlM3y8bmw1y+HI2IjQbzaTJF+z5b+6blzzQxbHAlycF+lqmuGgoKZzrqm5c/rcOi5beYvJFRBztG+Z07x+mnh9CmiprXPKmv98yZIEKRhrsbwrAQMKeLT/6OKCgS9Oueet2nwb715edPUEVlH+unzOQ+noYHE2BKon98Jt21/9B2V74cFbBBNCWUsy+wtY/Hp61nroEjYjM3sUx1zVReEeLUH0Hi+UD/BucuidfAFidc5TIcfgc7k9TltXpSX2fbwamkOusSyjX8wKgVEE4Tadnt5m27+re4ek++5tNgQ+l6+XgokTr3PPJkKM1A5WpaAo+UYTJRYOT9ntPHWBeTowI2CECESsMKNljgb+T0pRuw0fDYHLDeuslZh9fAFiX8ixNsUJCACLjQc0oTZZOuX70RQKg1V5ltQC6wYWZZWrbeumOXxK7b8wiAbRfjqrW7QVS+z2yJcsEdGEXjArmx9+Rpp7TTsQYbkygaUOZ3w4vYaKRsDMGWKP+P03FwPgxAIzQ5l25dueg2xfD7U6fyQH230TDufW2UWXSk/K+ppJxv132UwIZiKCpbhYvj76Eh3KMXPLE0Lq0976Q/xmBTGu065neRvgh2gvrvnNl9cH4Ej8Gnh9Ldx17a7K6+juLy/9DDLR43215wNU6mzf26s3mXv13X1xePrP6oW/dvcI3CBSeM6mDqdrv76ItzBuqONdiGovJ622kph+3jFWwyc+6HTEThPAq25TeAC7zZuXtfPVs9XceOhxpKK34NgXWbBgv3sacH06bSqncH1tFz7MQjB7b5mboyNFzMvUsEzwjANZuqV54cqDvWYHc0PbvGdjDH4Y/xCra5uPIIkxTvLnnHvtgBhUjN/oTUFjR0bN6abVpVX0DOyTtAJGX+sz8yCdwn1iMBkbS1oal5oC2+DLb54pXJbP4FgaMB+O6eu0MwJn8mc97l3heO9D8uNdZgU4uXrkT50vuc233jFezOnburmTxprsbtxZ6ZNosHFjzgRohW3MCjFbdtz7LBOjxLVoe/xaWq611HX1QMtMWXTx57P74wuWPDljYiXfscVV5d2HnwULZhyYq1WIyCsNXHvUDvexuA2bywMtFe51iDjYjkNeA3dzn1xyvYN869G4GJlRSEdUgJ8jC/AHayPR/AUz0YncAc3FhWdc7RFl8GG4q58qkL7ZMDmCNq5rf294x7EjDASIjHJd+gKqv7X0g/1mCTReWV8EFuzvXBeAUbimVD40/0kwOHBOewSl8HgqlKb+9rp5Ic7fB5sJfWvqeHL62HYA8lOw76hYnYqm5qUWW6vb7ROaARujygAaPOIkyccJvrTGJcg3378wsRWFzyNf2UWUN+rGkoncc82Dsjku7e27aFzY5HAuypQcPyDXz6n8zQXun6+Ut+9vpGA2xkZhRNZeV8zNYfZMnifEwYd9Mnd0XsAiJoFXxsCx7GuFv8DbkwTyWLmSmIoaLmlCsbfrRgAzjgMXzXrtZBp7ujAjYINIa84o/Y+gNXpukACzc43zsz3sGG0rW3dTvchmNSTx9W5O6bfsAXpVDzik93NG1z+Rq0HyXYIIjA6Quhyrh069z5QYlEowU2Nb+QFezOfc9p8FhFD8Zx2OYTYEOxbNm6AQkT9zJvcxriopANIuYRe77sprF29T53bf+owO5LLkJgzow689tbH33g9AqCUQEbjNBULnvEtu7ck4LFKLvR8EcAbCjGhi1ZRELqV3CIRMEcbNB7JzjSMZlPmLgOn76wv+YsXfvHzpa9c9y3+jDAlowp2KbF1Rc8WoQz26Zi5i1RhCLt4u0P3pOy+cPrYIM+Zd4aUPkUa9oqKs9QgpHbig0zuw+NkBZ48tAJLpSbXDHx0KVjxVp/Y1HlZjxp9mVMGH8X9Q+zPVkdLLQ9NcE8ORHT97os2zv74LDGPDomSbQSyZrPDEtqa4bSZs8rxwIN8wra9ZOetO2X9z1/51RAG4Q6y+mhB/3MqEwkhO9ar++pcLgYwqXJJY761m17UsFiyfZ+Ohf6zDVGSWjDgmKnE9iOlWve0T8WwCR4wYDg8uaEIxlP0mMsWnzMWLbUZY67QVu4jBnxgrivCb4LDwniS5z0U7U/Q6C+K12gp5/kR5sqqv/C1r6pfqMCF8Uz00iXbQcCX4nivjeWLAxz1Ed48flolLQXBJt74PMOa+FJ7oGIjbjygdfEsnn7FNPSlYVUec1REqyecblaj0kSb+NiOQM1/D8gAcjXcdXsK+TcgreoqmXbTPWb0obTVvcbb/oZl654HhPL3yFS5rwN4GUviRnnDMWVTjcNkqqLBt+/5VIPFlXmOTJd9zaVU5roqG9+/oiYnJ3zBqzflT6uTD9HZuje7qhdo3XU72o7oCSyC/aQugXHSbXmsqtjdObxsNwi1ig5UIzL1mZAe4mk2a6vR605h6vnnMBTtKGO+tSi6goc6rvSTda8DcB9x7x2I+sLa3rfez+cml/0Oi5Pce2PxMzzVFbOy50rnnZ626telBiE8KW5CF9SjPBj81kLL6YQi1V4NKJ7VQz5pSJCmaGm8hdqjA3NWtCJmcDxSkNFbfBY2zaexHLwJ/XMCzwdh3GY324D2ymbb0ImZNyL5fCRjcxawRXYOYW/G2sbJ2RChiyWfQebEban1h+A/duxtnFCJmTIYmk70ML8n/D2XQ17gQtHsOCi5i1gPcaekMHy/wuA7v+MZ1yPAAAAAElFTkSuQmCC',
            children: []
        };
        /** @type {?} */
        const nodeTemplate = (data) => {
            /** @type {?} */
            let office = ``;
            /** @type {?} */
            const title = `<div class="title">${data.name}</div>`;
            /** @type {?} */
            let content = `<div class="content" title="${data.title}"> ${data.title} </div>`;
            if (data.gender) {
                if (data.gender === 'F') {
                    content = `
          <div class="content">
          <img src="./assets/images/user-female.png" class="org-node-pic"> ${data.title}
          </div>`;
                }
                else {
                    content = `
          <div class="content">
          <img src="./assets/images/user.png" class="org-node-pic"> ${data.title}
          </div>`;
                }
            }
            if (data.imgUrl) {
                content = `
        <div class="content">
        <img src="${data.imgUrl}" class="org-node-pic"> ${data.title}
        </div>`;
            }
            if (data.office) {
                office = `<span class="office">${data.office}</span>`;
            }
            /** @type {?} */
            let template = '';
            template += office;
            template += title;
            template += content;
            return template;
        };
        // dragable false çünkü ekrandaki order bozuluyor sapıtıyor
        this.orgChart = $('.chart-container').orgchart({
            data: this.datasource,
            nodeTemplate,
            verticalLevel: 5,
            visibleLevel: 4,
            nodeContent: 'title',
            exportButton: false,
            // exportFilename: 'MyOrgChart',
            // exportFileextension: 'png',
            draggable: false,
            pan: false,
            zoom: false,
        });
    }
    /**
     * @param {?} orgChart
     * @param {?} datasource
     * @return {?}
     */
    refreshOrgChart(orgChart, datasource) {
        // $('.chart-container').empty();
        orgChart.init({ data: JSON.parse(datasource) });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    fileChange(event) {
        /** @type {?} */
        const fileList = event.target.files;
        if (fileList.length > 0) {
            /** @type {?} */
            const file = fileList[0];
            /** @type {?} */
            const formData = new FormData();
            formData.append('uploadFile', file, file.name);
            if (file) {
                this.readFile(file, this.refreshOrgChart, this.orgChart);
            }
        }
    }
    /**
     * @param {?} file
     * @param {?} refreshOrgChart
     * @param {?} orgChart
     * @return {?}
     */
    readFile(file, refreshOrgChart, orgChart) {
        /** @type {?} */
        const reader = new FileReader();
        reader.readAsText(file, 'UTF-8');
        reader.onload = (evt) => {
            /** @type {?} */
            const datasource = ((/** @type {?} */ ((evt.target)))).result;
            this.datasource = datasource;
            refreshOrgChart(orgChart, datasource);
        };
        reader.onerror = (evt) => {
            console.log('error reading file');
        };
    }
    /**
     * @param {?} zoomType
     * @return {?}
     */
    onClickZoom(zoomType) {
        console.log(zoomType);
        console.log(this.orgChart);
        /** @type {?} */
        let currentZoom = parseFloat($('.orgchart').css('zoom'));
        this.orgChart.setChartScale(this.orgChart.$chart, zoomType === '+' ? currentZoom += 0.2 : currentZoom -= 0.2);
    }
    /**
     * @return {?}
     */
    onClickExport() {
        this.clickedExport.emit({ data: true });
    }
    /**
     * @return {?}
     */
    onExport() {
        this.orgChart.export('organization-schema', 'png');
    }
    /**
     * @return {?}
     */
    onClickResetAll() {
        this.ngZone.run(() => {
            $('.orgchart').css('transform', ''); // remove the tansform settings
            this.orgChart.init({ data: this.datasource });
        });
    }
}
CoreOrgChartComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-org-chart',
                template: "<input id=\"file-upload\" type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".json\">\r\n\r\n\r\n\r\n<div class='chart-container'>\r\n</div>\r\n\r\n<div class=\"fixed-operation-bar\">\r\n\r\n  <!-- <label for=\"file-upload\" class=\"btn btn-primary   fl-l\">\r\n    <core-icon class=\"main-icon\" icon=\"upload\"></core-icon>\r\n    {{ 'FileUpload' | translate}}\r\n  </label> -->\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickResetAll()\" class=\"btn btn-danger ml-10 fl-r\" title=\"{{'Reset'| translate}}\">\r\n    <core-icon icon=\"times\"></core-icon> {{'ResetAll' | translate}}\r\n  </button> -->\r\n\r\n  <button type=\"button\" (click)=\"onClickExport()\" class=\"btn btn-primary ml-10 fl-r\" title=\"{{'Export'| translate}}\">\r\n    <core-icon icon=\"download\"></core-icon> {{'Export' | translate}}\r\n  </button>\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickZoom('-')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomOut'| translate}}\">\r\n    <core-icon icon=\"search-minus\"></core-icon> {{'ZoomOut' | translate}}\r\n  </button>\r\n\r\n  <button type=\"button\" (click)=\"onClickZoom('+')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomIn'| translate}}\">\r\n    <core-icon icon=\"search-plus\"></core-icon> {{'ZoomIn' | translate}}\r\n  </button> -->\r\n</div>",
                styles: [".chart-container.canvasContainer{position:initial}:host /deep/ .orgchart{background:#fff}input[type=file]{display:none}.btn{cursor:pointer}:host /deep/ .chart-container.canvasContainer{position:initial}:host /deep/ .orgchart .node .content{height:auto;padding:5px}:host /deep/ .orgchart .node .content .org-node-pic{width:40px}:host /deep/ .orgchart .node{width:auto}:host /deep/ .orgchart .node .office{font-weight:600}:host /deep/ .orgchart .node .title{padding:1px 5px}:host /deep/ .orgchart .node .title .symbol{margin-top:2px;margin-left:0}:host /deep/ .orgchart .node .title .fa-group:before,:host /deep/ .orgchart .node .title .fa-users:before{padding-right:4px}.chart-container{zoom:1}:host /deep/ .org-node-blue .title{background-color:#007bff}:host /deep/ .org-node-blue .content{border-color:#007bff}:host /deep/ .org-node-blue-light .title{background-color:#e9f5ff}:host /deep/ .org-node-blue-light .content{border-color:#e9f5ff}:host /deep/ .org-node-blue-dark .title{background-color:#166dba}:host /deep/ .org-node-blue-dark .content{border-color:#166dba}:host /deep/ .org-node-blue-extra-dark .title{background-color:#15233d}:host /deep/ .org-node-blue-extra-dark .content{border-color:#15233d}:host /deep/ .org-node-purple .title{background-color:#8622cd}:host /deep/ .org-node-purple .content{border-color:#8622cd}:host /deep/ .org-node-pink .title{background-color:#e83e8c}:host /deep/ .org-node-pink .content{border-color:#e83e8c}:host /deep/ .org-node-red .title{background-color:#e30a3a}:host /deep/ .org-node-red .content{border-color:#e30a3a}:host /deep/ .org-node-orange .title{background-color:#fd7e14}:host /deep/ .org-node-orange .content{border-color:#fd7e14}:host /deep/ .org-node-orange-light .title{background-color:#ffb822}:host /deep/ .org-node-orange-light .content{border-color:#ffb822}:host /deep/ .org-node-yellow .title{background-color:#ffc107}:host /deep/ .org-node-yellow .content{border-color:#ffc107}:host /deep/ .org-node-yellow-light .title{background-color:#fff589}:host /deep/ .org-node-yellow-light .content{border-color:#fff589}:host /deep/ .org-node-green .title{background-color:#28a745}:host /deep/ .org-node-green .content{border-color:#28a745}:host /deep/ .org-node-indigo .title{background-color:#6610f2}:host /deep/ .org-node-indigo .content{border-color:#6610f2}:host /deep/ .org-node-teal .title{background-color:#20c997}:host /deep/ .org-node-teal .content{border-color:#20c997}:host /deep/ .org-node-cyan .title{background-color:#17a2b8}:host /deep/ .org-node-cyan .content{border-color:#17a2b8}:host /deep/ .org-node-gray-dark .title{background-color:#343a40}:host /deep/ .org-node-gray-dark .content{border-color:#343a40}"]
            }] }
];
/** @nocollapse */
CoreOrgChartComponent.ctorParameters = () => [
    { type: NgZone }
];
CoreOrgChartComponent.propDecorators = {
    export: [{ type: Input }],
    clickedExport: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CoreOrgChartComponent.prototype.export;
    /** @type {?} */
    CoreOrgChartComponent.prototype.clickedExport;
    /** @type {?} */
    CoreOrgChartComponent.prototype.datasource;
    /** @type {?} */
    CoreOrgChartComponent.prototype.orgChart;
    /**
     * @type {?}
     * @private
     */
    CoreOrgChartComponent.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1vcmctY2hhcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1vcmctY2hhcnQvY29yZS1vcmctY2hhcnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBV2pILE1BQU0sT0FBTyxxQkFBcUI7Ozs7SUFPaEMsWUFBb0IsTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7UUFMeEIsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBT2xELENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLGFBQTRCO1FBQ3RDLElBQUksYUFBYSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFVBQVUsR0FBRztZQUNoQixJQUFJLEVBQUUsR0FBRztZQUNULEtBQUssRUFBRSx3QkFBd0I7WUFDL0IsU0FBUyxFQUFFLGVBQWU7WUFDMUIsTUFBTSxFQUFFLEdBQUc7WUFDWCxNQUFNLEVBQUUsUUFBUTtZQUNoQixNQUFNLEVBQUUsb3hNQUFveE07WUFDNXhNLFFBQVEsRUFBRSxFQUNUO1NBQ0YsQ0FBQzs7Y0FHSSxZQUFZLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7Z0JBQ3hCLE1BQU0sR0FBRyxFQUFFOztrQkFDVCxLQUFLLEdBQUcsc0JBQXNCLElBQUksQ0FBQyxJQUFJLFFBQVE7O2dCQUNqRCxPQUFPLEdBQUcsK0JBQStCLElBQUksQ0FBQyxLQUFLLE1BQU0sSUFBSSxDQUFDLEtBQUssU0FBUztZQUVoRixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtvQkFDdkIsT0FBTyxHQUFHOzs2RUFFeUQsSUFBSSxDQUFDLEtBQUs7aUJBQ3RFLENBQUM7aUJBQ1Q7cUJBQU07b0JBQ0wsT0FBTyxHQUFHOztzRUFFa0QsSUFBSSxDQUFDLEtBQUs7aUJBQy9ELENBQUM7aUJBQ1Q7YUFDRjtZQUVELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixPQUFPLEdBQUc7O29CQUVFLElBQUksQ0FBQyxNQUFNLDJCQUEyQixJQUFJLENBQUMsS0FBSztlQUNyRCxDQUFDO2FBQ1Q7WUFHRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsTUFBTSxHQUFHLHdCQUF3QixJQUFJLENBQUMsTUFBTSxTQUFTLENBQUM7YUFDdkQ7O2dCQUVHLFFBQVEsR0FBRyxFQUFFO1lBQ2pCLFFBQVEsSUFBSSxNQUFNLENBQUM7WUFDbkIsUUFBUSxJQUFJLEtBQUssQ0FBQztZQUNsQixRQUFRLElBQUksT0FBTyxDQUFDO1lBRXBCLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUM7UUFHRCwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDN0MsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3JCLFlBQVk7WUFDWixhQUFhLEVBQUUsQ0FBQztZQUNoQixZQUFZLEVBQUUsQ0FBQztZQUNmLFdBQVcsRUFBRSxPQUFPO1lBQ3BCLFlBQVksRUFBRSxLQUFLOzs7WUFHbkIsU0FBUyxFQUFFLEtBQUs7WUFDaEIsR0FBRyxFQUFFLEtBQUs7WUFDVixJQUFJLEVBQUUsS0FBSztTQUVaLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVELGVBQWUsQ0FBQyxRQUFhLEVBQUUsVUFBZTtRQUM1QyxpQ0FBaUM7UUFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLOztjQUNSLFFBQVEsR0FBYSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7UUFDN0MsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7a0JBQ2pCLElBQUksR0FBUyxRQUFRLENBQUMsQ0FBQyxDQUFDOztrQkFDeEIsUUFBUSxHQUFhLElBQUksUUFBUSxFQUFFO1lBQ3pDLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDMUQ7U0FFRjtJQUNILENBQUM7Ozs7Ozs7SUFFRCxRQUFRLENBQUMsSUFBUyxFQUFFLGVBQW9CLEVBQUUsUUFBYTs7Y0FDL0MsTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFO1FBQy9CLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRTs7a0JBRWhCLFVBQVUsR0FBRyxDQUFDLG1CQUFBLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFjLENBQUMsQ0FBQyxNQUFNO1lBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1lBQzdCLGVBQWUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBQ0YsTUFBTSxDQUFDLE9BQU8sR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUM7SUFDSixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxRQUFnQjtRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztZQUN2QixXQUFXLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsUUFBUSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ2hILENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFO1lBQ25CLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsK0JBQStCO1lBQ3BFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBaEpGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixzMENBQThDOzthQUUvQzs7OztZQVZrRixNQUFNOzs7cUJBWXRGLEtBQUs7NEJBQ0wsTUFBTTs7OztJQURQLHVDQUFzQjs7SUFDdEIsOENBQWtEOztJQUVsRCwyQ0FBdUI7O0lBQ3ZCLHlDQUFxQjs7Ozs7SUFFVCx1Q0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuZGVjbGFyZSB2YXIgJDogYW55O1xyXG5cclxuLy8gQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBkb2N1bWVudDogRG9jdW1lbnRcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnY29yZS1vcmctY2hhcnQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb3JlLW9yZy1jaGFydC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29yZS1vcmctY2hhcnQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZU9yZ0NoYXJ0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gIEBJbnB1dCgpIGV4cG9ydD86IGFueTtcclxuICBAT3V0cHV0KCkgY2xpY2tlZEV4cG9ydCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBwdWJsaWMgZGF0YXNvdXJjZTogYW55O1xyXG4gIHB1YmxpYyBvcmdDaGFydDogYW55O1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIG5nWm9uZTogTmdab25lKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoc2ltcGxlQ2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKHNpbXBsZUNoYW5nZXMuZXhwb3J0ICYmIHRoaXMuZXhwb3J0KSB7XHJcbiAgICAgIHRoaXMub25FeHBvcnQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5kYXRhc291cmNlID0ge1xyXG4gICAgICBuYW1lOiAnQScsXHJcbiAgICAgIHRpdGxlOiAnWcO2bmV0aW0gS3VydWx1IEJhxZ9rYW7EsScsXHJcbiAgICAgIGNsYXNzTmFtZTogJ29yZy1ub2RlLWJsdWUnLFxyXG4gICAgICBnZW5kZXI6ICdNJyxcclxuICAgICAgb2ZmaWNlOiAnQW5rYXJhJyxcclxuICAgICAgaW1nVXJsOiAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFMWUFBQUF2Q0FZQUFBQk9tVENQQUFBQUFYTlNSMElCMmNrc2Z3QUFBQWx3U0ZsekFBQUxFd0FBQ3hNQkFKcWNHQUFBRW1KSlJFRlVlSnp0blF0VVUxZTZnSzNXS2dWUjNvUzhJQ1FRRWdMaGFaQVk1U1V2QlFHRkttcXRpSW9vVWgrb0tGcXRVMmQwemJUVDFsZXJyazd2bXRWcU96TnQxMjA3VHFlM3F6TzNNN1Z6cmQ2NXN6cTl2WTdrdlBNZ0JBU3RXdlhjdlU4SVFuSnlFc0FBc2Z4cjdSVVh5Yi8zZi83OW5YKy8vbk9jTkdsQ0ptUkNIZ2lwTFR4RXlOU2ZFZ25wdjJjcnVFVDFLWm1xdmRDNXRrbm5iVnV1bjNyZDMxaFUvaHRrUm9RWmkwNzRGeWFXc3hlK2pLQ3k4N2M1Nm1NaVZRSVdMZjhPL0tiZHBhNUFoaExTcEhZaVJadmxxRzlwUFJDUHkxUCtqdkdscUN0OU5Db1d4V1ZKN2FieUpXWGU5Z2RWdkRRZkY4Z0lUQmpuOG5wd3NSekRveFZmWUdKRmxMZnQ4U2toWWhQL2pJWUlhU3c4bXJXZ2daRTBMazZnTzVhdFh1NXRXM3FPblFnMDVwVmNRWjRJb3JFb0tZM3hZdGxMbUpnbU0zUkhIUFdSOE9nMGxCZmpXZzhXY0UyNFVFNFRpb3dDUjMxcnkvNWtYS0s4Qyt0M3BZK0dpR2dBRkcwc0tWdmxiWDhZY3haVk1mMFF3WDFOYUtTa0U0MklFWHZiSHA4UzRKUUxJQUxhUUdJcGFJaUF4bU5WZEVkdFhZVzNiZWs1ZmpMQW1GLzZWMlJhTU0xbEUreHNNblBlODQ3NlNHU3NHa1RVT3k3MVlBR1E0S0lFbWxCbTVqanFXM2Z1VitHeGlUME1USzc4RVFxZ2owNmdqYVhseTd6dEQwTmVXUmtEZGFURTlmVkFtM2l4R0NoQ2I5dmpVNEx3Sko5d09pMjRIK3hLYjl2eWtNRCtZUUxzQ1psRVp1byt3NEtGTGtHYUFIc0NiSjhVbzI3Qm45QlpnZ213SjAyQS9VZ0psYW16TFI0bndQWmxzTkVKc0IyRVNNNzZBb1c3QU9NQTdONFRKd01CMk45NUJIYUc3aVZIL1pHQzNkbXlMeFdBVGVNZWdHMHFMVnZqYlg5UW12d2FCbW9Pc0hFYjJMZVJTRW1NdCszeEtTSFUyUUJzMGJnQXV4dEViRU4reVp2NmFVSC9qZktsbHdDazdDVmNmSVdjbmJ2SlVYK2tZSGZ0M2g4SHdQNVBKRng4MlZYYlNKam9NaG90LzVvcXFTejF0aitNeTFibmdmYXVvQkhSWDd1MEp5b1cyQ3I5Qk9YTGVONjJ4NmNFbDZkK3lUbjBqaUxZVUlqOFVyOXIwNE5uNmdWU2Z6MC9scjFFaUdZWXNvdW1PdXFPRk93YmV3OU94cVFxLzJ2aG9nQ1hiWWNMQTVEb2hBQ3NxR2FLdDMxaGZma1hrL1hCVVlINmNMRkxlNjVGU1dhZ2dqZy9NbHJ4bUxmdDhTbkJwYXF2bUhrY0Y5alNKTHFqc3BZVmJHSnV3ZVBFdkFWK2hEYlhqOHd1ZUh5MDdSOG9Jd1Y3UWg0SXBjbWRiSzdid0RNMWJrMHhyZHVjYlY2L0tkM1UwdXJ4cUdBc3I1bG1mYlkxeGxEOWRLYTVFZWl2YlV3MVZxOEs5NmJOZ3dTWEpia0hPMFpKVzVhdjZRZmJVRklaYjZ6YnNJN0tMbmdWVDlaOGhLZG9MdUpKbVJlSkpNMkhCbDNoY1VOcGRaT3hxbFpqcm0rWU5oUmJicjU2OWdtd1lGclM3amRyQ3lxVU5xS0NXUFlTS2Q2c3p5clNPT3FQRkd4ejY2RVFUSlpVajBhSU5ybHFHMFRzVFdpTXZORllVaFhueVRVWjEyMmFRV2JxVklaRk5TdU1SVlZ0Vk1hOFgxS0ZDMDhiSzZyUDR1cXM0OVQ4d2tOVWJ2RXFRL2xTcHpteWRlL2hHQ1JVc0JtSkVMdjJCVit5R1VUczFZaElNY05Sdit2Z0liNWwreTV0eDQ3ZEd0Ynk3SGFkWmMrK3RPNExGd0w2N2ExYUx1aDR1bTRkbVpMOU96QlNrNWd3L2c0bWtOMERuejlnc1lra3FjMTcwMXpYNEhScTI2Ky9aRVVhVlZ5NW4waksvRE5ZaTFneFlkeGRUQWowQlhGM1FJQkV5UG1GWjgxYmQrUjY0cnNSQ1NxUS9SZkc0MWh4eitMVHdLZ2ZlbzY4T0svemhaOHFpYXljWCtQaWhPdm96RWdhRFFMZmhZTDVlWWpRZHRRTUZxSHdiOHpmZVJLYXpNNzdqTXdwV21OcGV2WUpUMnpwT1g0Q0xoNi85V3hYUlBlaW8vN0lkMFgycGNERm8yZTdJbVYxWE5kQ2xpMFhFM055MjhpczNDL3g2SVM3Y09jSkRZcGkvSVhPNU5Gb1FBVDROL0JYc0pCR1p2Rm9JbVhPTngwTnphR0Q2cGhUVU9QaHJzZ3RsR1h4U0dweTJ0QncwQjR2aHJVZ0FXR2czU3hMMXl1dmhuYlVOMDQyTG42cURaTW1Ja2hnSkJQUStvL3pZZnNSRWx2L1F0dURCTFNodU9MZ3dMYU16UzBDUTI3cGFmRDd1NHgrcUJEb1JEL1FoWjk5S1JvSXFKc3NYbnpHc24xUGdLUE5EMDNReUpoTG5DREEzQXFSL0lheHNPeGpYS0trRUw5UUFMTEk5aDJFajYxRWdSSU9IT2Nmd2VnVGFYTytJbXRXdVIzNkg1WHRQcUs0ZkNNbVM5WmpvQU1aZ09HdUU0ZS9VTmdlWDlaaDFCVVAyckliNlQ2MklYMWVHN3h4c0xCbzFvTDRoOU9rT2t0djNiNnJGRS9NK0pBQmNsYlVBMXZaMm9QMkFyQ1JKOE5weS9yRzNiQ2Q3cU5IczFGeHdqVVUxSWVHUlEvZ2dFMC9qdUZIUDNVbXZEbCs4LzM1Y3g0RnZTRUx5cE53ZzgxblB1OGo4RTZGMFp2TGFCWW5RRWlnQTVGWmd1K3RPMXBYYzlueUtJQnRxZC80Y3lRd2dvbHNHRXhTOHNCZkVHd3c3R1BtaGRXRE12UkdETFltdHhYbG1HWXlSU1R2eFdKVjNmMkpjSjcwclRDZVJnTEI5Y1VvYmxwM3R1NGtVN1BhMFNmRHVHK0lnVVVRQi93b290dW5CTkxXSFMxNzNFSTZIQUZ6N0N0Y2MreitBanZKRTVoWkM3akxnU09RZ01qN3ZhZFBWcnV5eGRmQnRqYnZhTkJQQzJFaW1zYzN2eDFzU1NKbUxsMDZ1bUREb0FYcWhwRDEzNFNlOW1sZjBJSlRVR1pVNEErUkNRZzNHQjFRVVh6WDliMzdaSjRUNjZFUWlSbi9RRDI5VTBkUzdIZDVlRXgzejdIaktqWmJmQm5zbnBkZjVaR0o2UlF5UFlUcHRLSDRac3pBZmhobEpBRVArQW1PNXBZMTYxcUdUcTRiSVJScC96TXFZUGZCclFmUUdoY3MvT3lIUC83ZWFSL1lsOEUyMTIxY3EvY0xHVnBITS9uVUE2WWl2Z2oyU0Fvelh4ZlNaTmI4UHd5UFhnNFpWYkFaS01HaUVpeFNyQWNPVlRuYTRzdGdHNnRyVHlEVFBJaldzRFBoenNnTU1BK0hPeVZnT0ViOHcrQUMzUXpBSHJSUC9NaUREUXU0Tmx5aS9IWjQ5SElJRWFmK0JzSTJaTENkZGtJODE0WERENVZiK0ltakxiNEtOcEdSL1RndVYzK0FoYmtKRUh4YisyREJkb2VhdStCOVNwUHpBalUzN3d5ZVBQc3lJVlpnRm0zaG9DMDdud1BiZnUxRG1XK0RVUXZjMU8zREo5aUY0TktrZnpMN2pKNGFEcDBNRnh0dzRRQVhTWERoQUNOUXFOamppNEc3SzNoY1VrL25ybGIxUUZ0OEZXeE1rVElWaTFYK083TnZHK1ZtQndSRWFmUEtaM1lNYk5leWZzc3M4NWFXTFAxemg0TUcvbjFVd1lhUG1NRnRTZVpNUWtoN3pJVGRCbmhUUXoyNG1JUjFRRWFnM2U2Q25nM3NhOE5obDFOd21ZZGd3eUcwYjRNZWowdHVKOUxuSHJmVWI5eHFxbG01RzFkbnZZdExWVDBJQkQzQ2ZmUm42Z0UzUk1lNkRZTVNtWHdWYkFhaWdvVm40SlNDMmFmbHV2WmdBVzF0YWZFb2dXcFV3SVorQnIrQmZRTG0rUllpV2ZOWElqSDlHMmduVnpyeklIM29FemlseU5TOVpWcS9lYVg1bWZWMWFLenlVMmIvM2gzY1l3cDJYNlNHSFFmbWtyODBiOTNwbEROZzJiUlYweDRpK2h0ellzVnhrbW0vR0xqQmJ5eXJPanV3RGw4R3UyUDlwaWEzaTBjWUhNQm9SZVVWL2FuMzVERzNTVlNqQWpiYzdnTTJrNnJNOHgyTnpRbk10ZXpaUDkyMHVxNFZFOHFZRTFOM1FRcU1WSGVzZS9ZUGVtdUFaYzgrZitQQ3FqOGcwMFBIS2RoOTh5VUlHNGpRcjNEVmRmM3M2VEFxTys4cW5FTnpPaE02WkFhUHB0TG5mdDY1cGFXL2czdE9uUEpac0h0L2UwNE9BT2xtcnAxckFjbVQyTGE0VnEwNTQ2NXZ2TCtQTGFNUnNJZ2xOUFAvWVduYTduUUNhRjVlK3k3aUY4WjlzNEtSRjZ3djJpMDdXd09kL0huNGFCVTgyR09tcnVNUGJIRHg0T0p3VmNiVk8xOStFZVN1UHV0UEQ5Y2d3WUs3eklLVXE4N0FTSnBNMTE2eE5HL3ZyOU9Yd1liUzJkajBRdnRqTXg0Y0s3dUsyaUFLd2xNM3k4Ym13MXkrSEkySWpRYnphVEpGK3o1Yis2Ymx6elF4YkhBbHljRitscW11R2dvS1p6cnFtNWMvcmNPaTViZVl2SkZSQnp0RytaMDd4K21uaDlDbWlwclhQS212OTh5WklFS1JocnNid3JBUU1LZUxULzZPS0NnUzlPdWVldDJud2I3MTVlZFBVRVZsSCt1bnpPUStub1lIRTJCS29uOThKdDIxLzlCMlY3NGNGYkJCTkNXVXN5K3d0WS9IcDYxbnJvRWpZak0zc1V4MXpWUmVFZUxVSDBIaStVRC9CdWN1aWRmQUZpZGM1VEljZmdjN2s5VGx0WHBTWDJmYndhbWtPdXNTeWpYOHdLZ1ZFRTRUYWRudDVtMjcrcmU0ZWsrKzV0TmdRK2w2K1hnb2tUcjNQUEprS00xQTVXcGFBbytVWVRKUllPVDludFBIV0JlVG93STJDRUNFU3NNS05samdiK1QwcFJ1dzBmRFlITERldXNsWmg5ZkFGaVg4aXhOc1VKQ0FDTGpRYzBvVFpaT3VYNzBSUUtnMVY1bHRRQzZ3WVdaWldyYmV1bU9YeEs3Yjh3aUFiUmZqcXJXN1FWUyt6MnlKY3NFZEdFWGpBcm14OStScHA3VFRzUVlia3lnYVVPWjN3NHZZYUtSc0RNR1dLUCtQMDNGd1BneEFJelE1bDI1ZHVlZzJ4ZkQ3VTZmeVFIMjMwVER1ZlcyVVdYU2svSytwcEp4djEzMlV3SVppS0NwYmhZdmo3NkVoM0tNWFBMRTBMcTA5NzZRL3htQlRHdTA2NW5lUnZnaDJndnJ2bk5sOWNINEVqOEduaDlMZHgxN2E3SzYranVMeS85RERMUjQzMjE1d05VNm16ZjI2czNtWHYxM1gxeGVQclA2b1cvZHZjSTNDQlNlTTZtRHFkcnY3Nkl0ekJ1cU9OZGlHb3ZKNjIya3BoKzNqRld3eWMrNkhURVRoUEFxMjVUZUFDN3padVh0ZlBWczlYY2VPaHhwS0szNE5nWFdiQmd2M3NhY0gwNmJTcW5jSDF0Rno3TVFqQjdiNW1ib3lORnpNdlVzRXp3akFOWnVxVjU0Y3FEdldZSGMwUGJ2R2RqREg0WS94Q3JhNXVQSUlreFR2TG5uSHZ0Z0JoVWpOL29UVUZqUjBiTjZhYlZwVlgwRE95VHRBSkdYK3N6OHlDZHduMWlNQmtiUzFvYWw1b0MyK0RMYjU0cFhKYlA0RmdhTUIrTzZldTBNd0puOG1jOTdsM2hlTzlEOHVOZFpnVTR1WHJrVDUwdnVjMjMzakZlek9uYnVybVR4cHJzYnR4WjZaTm9zSEZqemdSb2hXM01DakZiZHR6N0xCT2p4TFZvZS94YVdxNjExSFgxUU10TVdYVHg1N1A3NHd1V1BEbGpZaVhmc2NWVjVkMkhud1VMWmh5WXExV0l5Q3NOWEh2VUR2ZXh1QTJieXdNdEZlNTFpRGpZamtOZUEzZHpuMXh5dllOODY5RzRHSmxSU0VkVWdKOGpDL0FIYXlQUi9BVXowWW5jQWMzRmhXZGM3UkZsOEdHNHE1OHFrTDdaTURtQ05xNXJmMjk0eDdFakRBU0lqSEpkK2dLcXY3WDBnLzFtQ1RSZVdWOEVGdXp2WEJlQVViaW1WRDQwLzBrd09IQk9ld1NsOEhncWxLYis5cnA1SWM3ZkI1c0pmV3ZxZUhMNjJIWUE4bE93NzZoWW5ZcW01cVVXVzZ2YjdST2FBUnVqeWdBYVBPSWt5Y2NKdnJUR0pjZzMzNzh3c1JXRnp5TmYyVVdVTityR2tvbmNjODJEc2prdTdlMjdhRnpZNUhBdXlwUWNQeURYejZuOHpRWHVuNitVdCs5dnBHQTJ4a1poUk5aZVY4ek5ZZlpNbmlmRXdZZDlNbmQwWHNBaUpvRlh4c0N4N0d1RnY4RGJrd1R5V0xtU21Jb2FMbWxDc2JmclJnQXpqZ01YelhydFpCcDd1akFqWUlOSWE4NG8vWStnTlhwdWtBQ3pjNDN6c3ozc0dHMHJXM2RUdmNobU5TVHg5VzVPNmJmc0FYcFZEemlrOTNORzF6K1JxMEh5WFlJSWpBNlF1aHlyaDA2OXo1UVlsRW93VTJOYitRRmV6T2ZjOXA4RmhGRDhaeDJPWVRZRU94Yk5tNkFRa1Q5ekp2Y3hyaW9wQU5JdVlSZTc3c3ByRjI5VDUzYmYrb3dPNUxMa0pnem93Njg5dGJIMzNnOUFxQ1VRRWJqTkJVTG52RXR1N2NrNExGS0x2UjhFY0FiQ2pHaGkxWlJFTHFWM0NJUk1FY2JOQjdKempTTVpsUG1MZ09uNzZ3ditZc1hmdkh6cGE5Yzl5MytqREFsb3dwMktiRjFSYzhXb1F6MjZaaTVpMVJoQ0x0NHUwUDNwT3krY1ByWUlNK1pkNGFVUGtVYTlvcUtzOVFncEhiaWcwenV3K05rQlo0OHRBSkxwU2JYREh4MEtWanhWcC9ZMUhsWmp4cDltVk1HSDhYOVErelBWa2RMTFE5TmNFOE9SSFQ5N29zMnp2NzRMREdQRG9tU2JRU3laclBERXRxYTRiU1pzOHJ4d0lOOHdyYTlaT2V0TzJYOXoxLzUxUkFHNFE2eSttaEIvM01xRXdraE85YXIrK3BjTGdZd3FYSkpZNzYxbTE3VXNGaXlmWitPaGY2ekRWR1NXakRnbUtuRTlpT2xXdmUwVDhXd0NSNHdZRGc4dWFFSXhsUDBtTXNXbnpNV0xiVVpZNjdRVnU0akJueGdyaXZDYjRMRHduaVM1ejBVN1UvUTZDK0sxMmdwNS9rUjVzcXF2L0MxcjZwZnFNQ0Y4VXowMGlYYlFjQ1g0bml2amVXTEF4ejFFZDQ4ZmxvbExRWEJKdDc0UE1PYStGSjdvR0lqYmp5Z2RmRXNubjdGTlBTbFlWVWVjMVJFcXllY2JsYWowa1NiK05pT1FNMS9EOGdBY2pYY2RYc0srVGNncmVvcW1YYlRQV2Iwb2JUVnZjYmIvb1psNjU0SGhQTDN5RlM1cndONEdVdmlSbm5ETVdWVGpjTmtxcUxCdCsvNVZJUEZsWG1PVEpkOXphVlU1cm9xRzkrL29pWW5KM3pCcXpmbFQ2dVREOUhadWplN3FoZG8zWFU3Mm83b0NTeUMvYVF1Z1hIU2JYbXNxdGpkT2J4c053aTFpZzVVSXpMMW1aQWU0bWsyYTZ2UjYwNWg2dm5uTUJUdEtHTyt0U2k2Z29jNnJ2U1RkYThEY0I5eDd4Mkkrc0xhM3JmZXorY21sLzBPaTVQY2UyUHhNenpWRmJPeTUwcm5uWjYyNnRlbEJpRThLVzVDRjlTalBCajgxa0xMNllRaTFWNE5LSjdWUXo1cFNKQ21hR204aGRxakEzTld0Q0ptY0R4U2tORmJmQlkyemFleEhMd0ovWE1DendkaDNHWTMyNEQyeW1iYjBJbVpOeUw1ZkNSamN4YXdSWFlPWVcvRzJzYkoyUkNoaXlXZlFlYkViYW4xaCtBL2R1eHRuRkNKbVRJWW1rNzBNTDhuL0QyWFExN2dRdEhzT0NpNWkxZ1BjYWVrTUh5L3d1QTd2K01aMXlQQUFBQUFFbEZUa1N1UW1DQycsXHJcbiAgICAgIGNoaWxkcmVuOiBbXHJcbiAgICAgIF1cclxuICAgIH07XHJcblxyXG5cclxuICAgIGNvbnN0IG5vZGVUZW1wbGF0ZSA9IChkYXRhKSA9PiB7XHJcbiAgICAgIGxldCBvZmZpY2UgPSBgYDtcclxuICAgICAgY29uc3QgdGl0bGUgPSBgPGRpdiBjbGFzcz1cInRpdGxlXCI+JHtkYXRhLm5hbWV9PC9kaXY+YDtcclxuICAgICAgbGV0IGNvbnRlbnQgPSBgPGRpdiBjbGFzcz1cImNvbnRlbnRcIiB0aXRsZT1cIiR7ZGF0YS50aXRsZX1cIj4gJHtkYXRhLnRpdGxlfSA8L2Rpdj5gO1xyXG5cclxuICAgICAgaWYgKGRhdGEuZ2VuZGVyKSB7XHJcbiAgICAgICAgaWYgKGRhdGEuZ2VuZGVyID09PSAnRicpIHtcclxuICAgICAgICAgIGNvbnRlbnQgPSBgXHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIuL2Fzc2V0cy9pbWFnZXMvdXNlci1mZW1hbGUucG5nXCIgY2xhc3M9XCJvcmctbm9kZS1waWNcIj4gJHtkYXRhLnRpdGxlfVxyXG4gICAgICAgICAgPC9kaXY+YDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29udGVudCA9IGBcclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi4vYXNzZXRzL2ltYWdlcy91c2VyLnBuZ1wiIGNsYXNzPVwib3JnLW5vZGUtcGljXCI+ICR7ZGF0YS50aXRsZX1cclxuICAgICAgICAgIDwvZGl2PmA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZGF0YS5pbWdVcmwpIHtcclxuICAgICAgICBjb250ZW50ID0gYFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICAgICAgPGltZyBzcmM9XCIke2RhdGEuaW1nVXJsfVwiIGNsYXNzPVwib3JnLW5vZGUtcGljXCI+ICR7ZGF0YS50aXRsZX1cclxuICAgICAgICA8L2Rpdj5gO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgaWYgKGRhdGEub2ZmaWNlKSB7XHJcbiAgICAgICAgb2ZmaWNlID0gYDxzcGFuIGNsYXNzPVwib2ZmaWNlXCI+JHtkYXRhLm9mZmljZX08L3NwYW4+YDtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IHRlbXBsYXRlID0gJyc7XHJcbiAgICAgIHRlbXBsYXRlICs9IG9mZmljZTtcclxuICAgICAgdGVtcGxhdGUgKz0gdGl0bGU7XHJcbiAgICAgIHRlbXBsYXRlICs9IGNvbnRlbnQ7XHJcblxyXG4gICAgICByZXR1cm4gdGVtcGxhdGU7XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICAvLyBkcmFnYWJsZSBmYWxzZSDDp8O8bmvDvCBla3JhbmRha2kgb3JkZXIgYm96dWx1eW9yIHNhcMSxdMSxeW9yXHJcbiAgICB0aGlzLm9yZ0NoYXJ0ID0gJCgnLmNoYXJ0LWNvbnRhaW5lcicpLm9yZ2NoYXJ0KHtcclxuICAgICAgZGF0YTogdGhpcy5kYXRhc291cmNlLFxyXG4gICAgICBub2RlVGVtcGxhdGUsXHJcbiAgICAgIHZlcnRpY2FsTGV2ZWw6IDUsXHJcbiAgICAgIHZpc2libGVMZXZlbDogNCxcclxuICAgICAgbm9kZUNvbnRlbnQ6ICd0aXRsZScsXHJcbiAgICAgIGV4cG9ydEJ1dHRvbjogZmFsc2UsXHJcbiAgICAgIC8vIGV4cG9ydEZpbGVuYW1lOiAnTXlPcmdDaGFydCcsXHJcbiAgICAgIC8vIGV4cG9ydEZpbGVleHRlbnNpb246ICdwbmcnLFxyXG4gICAgICBkcmFnZ2FibGU6IGZhbHNlLFxyXG4gICAgICBwYW46IGZhbHNlLFxyXG4gICAgICB6b29tOiBmYWxzZSxcclxuICAgICAgLy8gZGlyZWN0aW9uOiAnbDJyJ1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoT3JnQ2hhcnQob3JnQ2hhcnQ6IGFueSwgZGF0YXNvdXJjZTogYW55KTogdm9pZCB7XHJcbiAgICAvLyAkKCcuY2hhcnQtY29udGFpbmVyJykuZW1wdHkoKTtcclxuICAgIG9yZ0NoYXJ0LmluaXQoeyBkYXRhOiBKU09OLnBhcnNlKGRhdGFzb3VyY2UpIH0pO1xyXG4gIH1cclxuXHJcbiAgZmlsZUNoYW5nZShldmVudCkge1xyXG4gICAgY29uc3QgZmlsZUxpc3Q6IEZpbGVMaXN0ID0gZXZlbnQudGFyZ2V0LmZpbGVzO1xyXG4gICAgaWYgKGZpbGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgY29uc3QgZmlsZTogRmlsZSA9IGZpbGVMaXN0WzBdO1xyXG4gICAgICBjb25zdCBmb3JtRGF0YTogRm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgZm9ybURhdGEuYXBwZW5kKCd1cGxvYWRGaWxlJywgZmlsZSwgZmlsZS5uYW1lKTtcclxuICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICB0aGlzLnJlYWRGaWxlKGZpbGUsIHRoaXMucmVmcmVzaE9yZ0NoYXJ0LCB0aGlzLm9yZ0NoYXJ0KTtcclxuICAgICAgfVxyXG5cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlYWRGaWxlKGZpbGU6IGFueSwgcmVmcmVzaE9yZ0NoYXJ0OiBhbnksIG9yZ0NoYXJ0OiBhbnkpOiB2b2lkIHtcclxuICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICByZWFkZXIucmVhZEFzVGV4dChmaWxlLCAnVVRGLTgnKTtcclxuICAgIHJlYWRlci5vbmxvYWQgPSAoZXZ0KSA9PiB7XHJcblxyXG4gICAgICBjb25zdCBkYXRhc291cmNlID0gKChldnQudGFyZ2V0KSBhcyBGaWxlUmVhZGVyKS5yZXN1bHQ7XHJcbiAgICAgIHRoaXMuZGF0YXNvdXJjZSA9IGRhdGFzb3VyY2U7XHJcbiAgICAgIHJlZnJlc2hPcmdDaGFydChvcmdDaGFydCwgZGF0YXNvdXJjZSk7XHJcbiAgICB9O1xyXG4gICAgcmVhZGVyLm9uZXJyb3IgPSAoZXZ0KSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKCdlcnJvciByZWFkaW5nIGZpbGUnKTtcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBvbkNsaWNrWm9vbSh6b29tVHlwZTogc3RyaW5nKSB7XHJcbiAgICBjb25zb2xlLmxvZyh6b29tVHlwZSk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLm9yZ0NoYXJ0KTtcclxuICAgIGxldCBjdXJyZW50Wm9vbSA9IHBhcnNlRmxvYXQoJCgnLm9yZ2NoYXJ0JykuY3NzKCd6b29tJykpO1xyXG4gICAgdGhpcy5vcmdDaGFydC5zZXRDaGFydFNjYWxlKHRoaXMub3JnQ2hhcnQuJGNoYXJ0LCB6b29tVHlwZSA9PT0gJysnID8gY3VycmVudFpvb20gKz0gMC4yIDogY3VycmVudFpvb20gLT0gMC4yKTtcclxuICB9XHJcblxyXG4gIG9uQ2xpY2tFeHBvcnQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsaWNrZWRFeHBvcnQuZW1pdCh7IGRhdGE6IHRydWUgfSk7XHJcbiAgfVxyXG5cclxuICBvbkV4cG9ydCgpIHtcclxuICAgIHRoaXMub3JnQ2hhcnQuZXhwb3J0KCdvcmdhbml6YXRpb24tc2NoZW1hJywgJ3BuZycpO1xyXG4gIH1cclxuXHJcbiAgb25DbGlja1Jlc2V0QWxsKCk6IHZvaWQge1xyXG4gICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgJCgnLm9yZ2NoYXJ0JykuY3NzKCd0cmFuc2Zvcm0nLCAnJyk7IC8vIHJlbW92ZSB0aGUgdGFuc2Zvcm0gc2V0dGluZ3NcclxuICAgICAgdGhpcy5vcmdDaGFydC5pbml0KHsgZGF0YTogdGhpcy5kYXRhc291cmNlIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==