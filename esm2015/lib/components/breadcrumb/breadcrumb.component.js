/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, NgZone, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import 'rxjs/add/operator/filter';
import { BreadcrumbService } from '../../services/custom/breadcrumb.service';
/**
 * @record
 */
function IBreadcrumb() { }
if (false) {
    /** @type {?} */
    IBreadcrumb.prototype.label;
    /** @type {?} */
    IBreadcrumb.prototype.params;
    /** @type {?} */
    IBreadcrumb.prototype.url;
}
export class BreadcrumbComponent {
    /**
     * @param {?} breadcrumbService
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} ngZone
     */
    constructor(breadcrumbService, router, activatedRoute, ngZone) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.ngZone = ngZone;
        // subscribe to the NavigationEnd event 
        this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
            .subscribe(event => {
            // set breadcrumbs
            /** @type {?} */
            const root = this.activatedRoute.root;
            this.breadcrumbService.breadcrumbs = this.getBreadcrumbs(root);
        });
    }
    /**
     * @private
     * @param {?} route
     * @param {?=} url
     * @param {?=} breadcrumbs
     * @return {?}
     */
    getBreadcrumbs(route, url = '', breadcrumbs = []) {
        /** @type {?} */
        const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
        // get the child routes
        /** @type {?} */
        const children = route.children;
        // return if there are no more children
        if (children.length === 0) {
            return breadcrumbs;
        }
        // iterate over each children
        for (const child of children) {
            // verify primary route
            if (child.outlet !== PRIMARY_OUTLET) {
                continue;
            }
            // verify the custom data property 'breadcrumb' is specified on the route
            if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
                return this.getBreadcrumbs(child, url, breadcrumbs);
            }
            // get the route's URL segment
            /** @type {?} */
            const routeURL = child.snapshot.url.map(segment => segment.path).join('/');
            // append route URL to URL
            url += `/${routeURL}`;
            /** @type {?} */
            let currentUrl = this.router.url;
            // /organization/operations/company
            /** @type {?} */
            const indexUrl = currentUrl.indexOf(url);
            /** @type {?} */
            let parentUrl = null;
            if (indexUrl !== -1) {
                if (indexUrl === 0 && url === '/') {
                    currentUrl = currentUrl.substring(1);
                    /** @type {?} */
                    const secondIndex = currentUrl.indexOf('/');
                    if (secondIndex !== -1) {
                        parentUrl = currentUrl.substring(0, secondIndex);
                    }
                }
                else {
                    parentUrl = currentUrl.substring(0, indexUrl);
                }
            }
            url = url.replace('//', parentUrl ? parentUrl + '/' : '/');
            if (url === '/' && parentUrl) {
                url = '/' + parentUrl;
            }
            // url = url.replace('//', this.mainRout ? '/' + this.mainRout + '/' : '/');
            // if (url === '/' && this.mainRout) {
            //   url = '/' + this.mainRout;
            // }
            // add breadcrumb
            /** @type {?} */
            const breadcrumb = {
                label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
                url: this.getFullPath(child.snapshot),
                customLabel: null
            };
            if (breadcrumbs.findIndex(x => x.label === breadcrumb.label && x.url === breadcrumb.url) === -1) {
                breadcrumbs.push(breadcrumb);
            }
            if (child.snapshot.params && child.snapshot.params.id) {
                /** @type {?} */
                const moreBreadcrumb = {
                    label: child.snapshot.params.id,
                    url: this.getFullPath(child.snapshot),
                    customLabel: null
                };
                // find from list and update moreBreadcrumb according to that
                /** @type {?} */
                const breadcrumbsWithCustomLabel = this.breadcrumbService.breadcrumbsWithCustomLabels.find(x => x.label === moreBreadcrumb.label && x.url === moreBreadcrumb.url);
                if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    moreBreadcrumb['customLabel'] = breadcrumbsWithCustomLabel.customLabel;
                }
                /** @type {?} */
                const foundBreadcrumb = breadcrumbs.find(x => x.label === moreBreadcrumb.label && x.url === moreBreadcrumb.url);
                // check exist if not push, if exist then maybe customLabel was changed and update it 
                if (!foundBreadcrumb) {
                    breadcrumbs.push(moreBreadcrumb);
                }
                else if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    foundBreadcrumb.customLabel = breadcrumbsWithCustomLabel.customLabel;
                }
            }
            // recursive
            return this.getBreadcrumbs(child, url, breadcrumbs);
        }
        // we should never get here, but just in case
        return breadcrumbs;
    }
    /**
     * @param {?} route
     * @return {?}
     */
    getFullPath(route) {
        /** @type {?} */
        const relativePath = (segments) => segments.reduce((a, v) => a += '/' + v.path, '');
        /** @type {?} */
        const fullPath = (routes) => routes.reduce((a, v) => a += relativePath(v.url), '');
        return fullPath(route.pathFromRoot);
    }
    /**
     * @param {?} url
     * @return {?}
     */
    onClick(url) {
        this.ngZone.run(() => {
            this.router.navigate([url]);
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    }
}
BreadcrumbComponent.decorators = [
    { type: Component, args: [{
                template: `
  <ol *ngIf="breadcrumbService.breadcrumbs && breadcrumbService.breadcrumbs.length > 0" class="breadcrumb" [class.fixed-breadcrumb]="isFixed">
    <li *ngFor="let breadcrumb of breadcrumbService.breadcrumbs" (click) ="onClick(breadcrumb.url)"  title="{{breadcrumb.label | translate}}">
      <a><span *ngIf="!breadcrumb.customLabel">{{breadcrumb.label | translate}}</span><span *ngIf="breadcrumb.customLabel">{{breadcrumb.customLabel | translate}}</span></a>
    </li>
  </ol>
  `,
                selector: 'breadcrumb',
                styles: [".breadcrumb{margin-bottom:0;background-color:transparent;padding:0 0 0 2px;font-size:10px;font-weight:600}.breadcrumb li{cursor:pointer;color:#333!important}.breadcrumb li a{color:#333!important}.breadcrumb li:not(:last-child):after{content:'\\f054';font-family:FontAwesome;font-style:normal;font-weight:400;text-decoration:inherit;margin-left:10px;margin-right:10px;color:#455a64!important;position:relative;top:1px}.breadcrumb li:last-child{font-weight:600}.fixed-breadcrumb{height:35px;width:calc(100%);background-color:#fff;position:fixed;margin-left:-22px;z-index:49;top:45px;box-shadow:0 5px 5px -2px rgba(0,0,0,.2);padding-left:30px;padding-top:10px}"]
            }] }
];
/** @nocollapse */
BreadcrumbComponent.ctorParameters = () => [
    { type: BreadcrumbService },
    { type: Router },
    { type: ActivatedRoute },
    { type: NgZone }
];
BreadcrumbComponent.propDecorators = {
    isFixed: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.routingSubscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFixed;
    /** @type {?} */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9icmVhZGNydW1iL2JyZWFkY3J1bWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFhLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQUUsYUFBYSxFQUFVLGNBQWMsRUFBc0MsTUFBTSxpQkFBaUIsQ0FBQztBQUNwSSxPQUFPLDBCQUEwQixDQUFDO0FBRWxDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDOzs7O0FBRTdFLDBCQUlDOzs7SUFIQyw0QkFBYzs7SUFDZCw2QkFBZTs7SUFDZiwwQkFBWTs7QUFjZCxNQUFNLE9BQU8sbUJBQW1COzs7Ozs7O0lBSzlCLFlBQ1MsaUJBQW9DLEVBQ25DLE1BQWMsRUFDZCxjQUE4QixFQUM5QixNQUFjO1FBSGYsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNuQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDdEIsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxDQUFDO2FBQzFGLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTs7O2tCQUVYLElBQUksR0FBbUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJO1lBQ3JELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBSU8sY0FBYyxDQUFDLEtBQXFCLEVBQUUsTUFBYyxFQUFFLEVBQUUsY0FBcUIsRUFBRTs7Y0FDL0UscUJBQXFCLEdBQUcsWUFBWTs7O2NBR3BDLFFBQVEsR0FBcUIsS0FBSyxDQUFDLFFBQVE7UUFFakQsdUNBQXVDO1FBQ3ZDLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDekIsT0FBTyxXQUFXLENBQUM7U0FDcEI7UUFFRCw2QkFBNkI7UUFDN0IsS0FBSyxNQUFNLEtBQUssSUFBSSxRQUFRLEVBQUU7WUFDNUIsdUJBQXVCO1lBQ3ZCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxjQUFjLEVBQUU7Z0JBQ25DLFNBQVM7YUFDVjtZQUVELHlFQUF5RTtZQUN6RSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0JBQzlELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2FBQ3JEOzs7a0JBR0ssUUFBUSxHQUFXLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBRWxGLDBCQUEwQjtZQUMxQixHQUFHLElBQUksSUFBSSxRQUFRLEVBQUUsQ0FBQzs7Z0JBRWxCLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7OztrQkFHMUIsUUFBUSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDOztnQkFDcEMsU0FBUyxHQUFHLElBQUk7WUFFcEIsSUFBSSxRQUFRLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ25CLElBQUksUUFBUSxLQUFLLENBQUMsSUFBSSxHQUFHLEtBQUssR0FBRyxFQUFFO29CQUNqQyxVQUFVLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7MEJBQy9CLFdBQVcsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQztvQkFDM0MsSUFBSSxXQUFXLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3RCLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztxQkFDbEQ7aUJBQ0Y7cUJBQU07b0JBQ0wsU0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2lCQUMvQzthQUNGO1lBRUQsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFM0QsSUFBSSxHQUFHLEtBQUssR0FBRyxJQUFJLFNBQVMsRUFBRTtnQkFFNUIsR0FBRyxHQUFHLEdBQUcsR0FBRyxTQUFTLENBQUM7YUFDdkI7Ozs7Ozs7a0JBUUssVUFBVSxHQUFRO2dCQUN0QixLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUM7Z0JBQ2pELEdBQUcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7Z0JBQ3JDLFdBQVcsRUFBRSxJQUFJO2FBQ2xCO1lBSUQsSUFBSSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssVUFBVSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUMvRixXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzlCO1lBR0QsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7O3NCQUMvQyxjQUFjLEdBQVE7b0JBQzFCLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUMvQixHQUFHLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO29CQUNyQyxXQUFXLEVBQUUsSUFBSTtpQkFDbEI7OztzQkFHSywwQkFBMEIsR0FBUSxJQUFJLENBQUMsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxjQUFjLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssY0FBYyxDQUFDLEdBQUcsQ0FBQztnQkFFdEssSUFBSSwwQkFBMEIsSUFBSSwwQkFBMEIsQ0FBQyxXQUFXLEVBQUU7b0JBQ3hFLGNBQWMsQ0FBQyxhQUFhLENBQUMsR0FBRywwQkFBMEIsQ0FBQyxXQUFXLENBQUM7aUJBQ3hFOztzQkFFSyxlQUFlLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssY0FBYyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLGNBQWMsQ0FBQyxHQUFHLENBQUM7Z0JBQy9HLHNGQUFzRjtnQkFDdEYsSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDcEIsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDbEM7cUJBQU0sSUFBSSwwQkFBMEIsSUFBSSwwQkFBMEIsQ0FBQyxXQUFXLEVBQUU7b0JBQy9FLGVBQWUsQ0FBQyxXQUFXLEdBQUcsMEJBQTBCLENBQUMsV0FBVyxDQUFDO2lCQUN0RTthQUNGO1lBRUQsWUFBWTtZQUNaLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ3JEO1FBRUQsNkNBQTZDO1FBQzdDLE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRU0sV0FBVyxDQUFDLEtBQTZCOztjQUN4QyxZQUFZLEdBQUcsQ0FBQyxRQUFzQixFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQzs7Y0FDM0YsUUFBUSxHQUFHLENBQUMsTUFBZ0MsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQztRQUU1RyxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsR0FBVztRQUNqQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUU7WUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM1QixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDeEM7SUFDSCxDQUFDOzs7WUF6SkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRTs7Ozs7O0dBTVQ7Z0JBQ0QsUUFBUSxFQUFFLFlBQVk7O2FBRXZCOzs7O1lBbEJRLGlCQUFpQjtZQUhqQixNQUFNO1lBQUUsY0FBYztZQURBLE1BQU07OztzQkF5QmxDLEtBQUs7Ozs7Ozs7SUFETixrREFBMEM7O0lBQzFDLHNDQUEyQjs7SUFJekIsZ0RBQTJDOzs7OztJQUMzQyxxQ0FBc0I7Ozs7O0lBQ3RCLDZDQUFzQzs7Ozs7SUFDdEMscUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3ksIE5nWm9uZSwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgTmF2aWdhdGlvbkVuZCwgUGFyYW1zLCBQUklNQVJZX09VVExFVCwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgVXJsU2VnbWVudCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvZmlsdGVyJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY3VzdG9tL2JyZWFkY3J1bWIuc2VydmljZSc7XHJcblxyXG5pbnRlcmZhY2UgSUJyZWFkY3J1bWIge1xyXG4gIGxhYmVsOiBzdHJpbmc7XHJcbiAgcGFyYW1zOiBQYXJhbXM7XHJcbiAgdXJsOiBzdHJpbmc7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPG9sICpuZ0lmPVwiYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnMgJiYgYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnMubGVuZ3RoID4gMFwiIGNsYXNzPVwiYnJlYWRjcnVtYlwiIFtjbGFzcy5maXhlZC1icmVhZGNydW1iXT1cImlzRml4ZWRcIj5cclxuICAgIDxsaSAqbmdGb3I9XCJsZXQgYnJlYWRjcnVtYiBvZiBicmVhZGNydW1iU2VydmljZS5icmVhZGNydW1ic1wiIChjbGljaykgPVwib25DbGljayhicmVhZGNydW1iLnVybClcIiAgdGl0bGU9XCJ7e2JyZWFkY3J1bWIubGFiZWwgfCB0cmFuc2xhdGV9fVwiPlxyXG4gICAgICA8YT48c3BhbiAqbmdJZj1cIiFicmVhZGNydW1iLmN1c3RvbUxhYmVsXCI+e3ticmVhZGNydW1iLmxhYmVsIHwgdHJhbnNsYXRlfX08L3NwYW4+PHNwYW4gKm5nSWY9XCJicmVhZGNydW1iLmN1c3RvbUxhYmVsXCI+e3ticmVhZGNydW1iLmN1c3RvbUxhYmVsIHwgdHJhbnNsYXRlfX08L3NwYW4+PC9hPlxyXG4gICAgPC9saT5cclxuICA8L29sPlxyXG4gIGAsXHJcbiAgc2VsZWN0b3I6ICdicmVhZGNydW1iJyxcclxuICBzdHlsZVVybHM6IFsnLi9icmVhZGNydW1iLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEJyZWFkY3J1bWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG4gIHByaXZhdGUgcm91dGluZ1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIEBJbnB1dCgpIGlzRml4ZWQ/OiBib29sZWFuO1xyXG5cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYnJlYWRjcnVtYlNlcnZpY2U6IEJyZWFkY3J1bWJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSkge1xyXG4gICAgLy8gc3Vic2NyaWJlIHRvIHRoZSBOYXZpZ2F0aW9uRW5kIGV2ZW50IFxyXG4gICAgdGhpcy5yb3V0aW5nU3Vic2NyaXB0aW9uID0gdGhpcy5yb3V0ZXIuZXZlbnRzLmZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpXHJcbiAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4ge1xyXG4gICAgICAgIC8vIHNldCBicmVhZGNydW1ic1xyXG4gICAgICAgIGNvbnN0IHJvb3Q6IEFjdGl2YXRlZFJvdXRlID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5yb290O1xyXG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnMgPSB0aGlzLmdldEJyZWFkY3J1bWJzKHJvb3QpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgcHJpdmF0ZSBnZXRCcmVhZGNydW1icyhyb3V0ZTogQWN0aXZhdGVkUm91dGUsIHVybDogc3RyaW5nID0gJycsIGJyZWFkY3J1bWJzOiBhbnlbXSA9IFtdKTogYW55W10ge1xyXG4gICAgY29uc3QgUk9VVEVfREFUQV9CUkVBRENSVU1CID0gJ2JyZWFkY3J1bWInO1xyXG5cclxuICAgIC8vIGdldCB0aGUgY2hpbGQgcm91dGVzXHJcbiAgICBjb25zdCBjaGlsZHJlbjogQWN0aXZhdGVkUm91dGVbXSA9IHJvdXRlLmNoaWxkcmVuO1xyXG5cclxuICAgIC8vIHJldHVybiBpZiB0aGVyZSBhcmUgbm8gbW9yZSBjaGlsZHJlblxyXG4gICAgaWYgKGNoaWxkcmVuLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICByZXR1cm4gYnJlYWRjcnVtYnM7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gaXRlcmF0ZSBvdmVyIGVhY2ggY2hpbGRyZW5cclxuICAgIGZvciAoY29uc3QgY2hpbGQgb2YgY2hpbGRyZW4pIHtcclxuICAgICAgLy8gdmVyaWZ5IHByaW1hcnkgcm91dGVcclxuICAgICAgaWYgKGNoaWxkLm91dGxldCAhPT0gUFJJTUFSWV9PVVRMRVQpIHtcclxuICAgICAgICBjb250aW51ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gdmVyaWZ5IHRoZSBjdXN0b20gZGF0YSBwcm9wZXJ0eSAnYnJlYWRjcnVtYicgaXMgc3BlY2lmaWVkIG9uIHRoZSByb3V0ZVxyXG4gICAgICBpZiAoIWNoaWxkLnNuYXBzaG90LmRhdGEuaGFzT3duUHJvcGVydHkoUk9VVEVfREFUQV9CUkVBRENSVU1CKSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEJyZWFkY3J1bWJzKGNoaWxkLCB1cmwsIGJyZWFkY3J1bWJzKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gZ2V0IHRoZSByb3V0ZSdzIFVSTCBzZWdtZW50XHJcbiAgICAgIGNvbnN0IHJvdXRlVVJMOiBzdHJpbmcgPSBjaGlsZC5zbmFwc2hvdC51cmwubWFwKHNlZ21lbnQgPT4gc2VnbWVudC5wYXRoKS5qb2luKCcvJyk7XHJcblxyXG4gICAgICAvLyBhcHBlbmQgcm91dGUgVVJMIHRvIFVSTFxyXG4gICAgICB1cmwgKz0gYC8ke3JvdXRlVVJMfWA7XHJcblxyXG4gICAgICBsZXQgY3VycmVudFVybCA9IHRoaXMucm91dGVyLnVybDsgLy8gL29yZ2FuaXphdGlvbi9vcGVyYXRpb25zL2NvbXBhbnlcclxuXHJcblxyXG4gICAgICBjb25zdCBpbmRleFVybCA9IGN1cnJlbnRVcmwuaW5kZXhPZih1cmwpO1xyXG4gICAgICBsZXQgcGFyZW50VXJsID0gbnVsbDtcclxuXHJcbiAgICAgIGlmIChpbmRleFVybCAhPT0gLTEpIHtcclxuICAgICAgICBpZiAoaW5kZXhVcmwgPT09IDAgJiYgdXJsID09PSAnLycpIHtcclxuICAgICAgICAgIGN1cnJlbnRVcmwgPSBjdXJyZW50VXJsLnN1YnN0cmluZygxKTtcclxuICAgICAgICAgIGNvbnN0IHNlY29uZEluZGV4ID0gY3VycmVudFVybC5pbmRleE9mKCcvJyk7XHJcbiAgICAgICAgICBpZiAoc2Vjb25kSW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHBhcmVudFVybCA9IGN1cnJlbnRVcmwuc3Vic3RyaW5nKDAsIHNlY29uZEluZGV4KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcGFyZW50VXJsID0gY3VycmVudFVybC5zdWJzdHJpbmcoMCwgaW5kZXhVcmwpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgdXJsID0gdXJsLnJlcGxhY2UoJy8vJywgcGFyZW50VXJsID8gcGFyZW50VXJsICsgJy8nIDogJy8nKTtcclxuXHJcbiAgICAgIGlmICh1cmwgPT09ICcvJyAmJiBwYXJlbnRVcmwpIHtcclxuXHJcbiAgICAgICAgdXJsID0gJy8nICsgcGFyZW50VXJsO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyB1cmwgPSB1cmwucmVwbGFjZSgnLy8nLCB0aGlzLm1haW5Sb3V0ID8gJy8nICsgdGhpcy5tYWluUm91dCArICcvJyA6ICcvJyk7XHJcbiAgICAgIC8vIGlmICh1cmwgPT09ICcvJyAmJiB0aGlzLm1haW5Sb3V0KSB7XHJcbiAgICAgIC8vICAgdXJsID0gJy8nICsgdGhpcy5tYWluUm91dDtcclxuICAgICAgLy8gfVxyXG5cclxuICAgICAgLy8gYWRkIGJyZWFkY3J1bWJcclxuICAgICAgY29uc3QgYnJlYWRjcnVtYjogYW55ID0ge1xyXG4gICAgICAgIGxhYmVsOiBjaGlsZC5zbmFwc2hvdC5kYXRhW1JPVVRFX0RBVEFfQlJFQURDUlVNQl0sXHJcbiAgICAgICAgdXJsOiB0aGlzLmdldEZ1bGxQYXRoKGNoaWxkLnNuYXBzaG90KSxcclxuICAgICAgICBjdXN0b21MYWJlbDogbnVsbFxyXG4gICAgICB9O1xyXG5cclxuXHJcblxyXG4gICAgICBpZiAoYnJlYWRjcnVtYnMuZmluZEluZGV4KHggPT4geC5sYWJlbCA9PT0gYnJlYWRjcnVtYi5sYWJlbCAmJiB4LnVybCA9PT0gYnJlYWRjcnVtYi51cmwpID09PSAtMSkge1xyXG4gICAgICAgIGJyZWFkY3J1bWJzLnB1c2goYnJlYWRjcnVtYik7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICBpZiAoY2hpbGQuc25hcHNob3QucGFyYW1zICYmIGNoaWxkLnNuYXBzaG90LnBhcmFtcy5pZCkge1xyXG4gICAgICAgIGNvbnN0IG1vcmVCcmVhZGNydW1iOiBhbnkgPSB7XHJcbiAgICAgICAgICBsYWJlbDogY2hpbGQuc25hcHNob3QucGFyYW1zLmlkLFxyXG4gICAgICAgICAgdXJsOiB0aGlzLmdldEZ1bGxQYXRoKGNoaWxkLnNuYXBzaG90KSxcclxuICAgICAgICAgIGN1c3RvbUxhYmVsOiBudWxsXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gZmluZCBmcm9tIGxpc3QgYW5kIHVwZGF0ZSBtb3JlQnJlYWRjcnVtYiBhY2NvcmRpbmcgdG8gdGhhdFxyXG4gICAgICAgIGNvbnN0IGJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVsOiBhbnkgPSB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVscy5maW5kKHggPT4geC5sYWJlbCA9PT0gbW9yZUJyZWFkY3J1bWIubGFiZWwgJiYgeC51cmwgPT09IG1vcmVCcmVhZGNydW1iLnVybCk7XHJcblxyXG4gICAgICAgIGlmIChicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbCAmJiBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbC5jdXN0b21MYWJlbCkge1xyXG4gICAgICAgICAgbW9yZUJyZWFkY3J1bWJbJ2N1c3RvbUxhYmVsJ10gPSBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbC5jdXN0b21MYWJlbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZvdW5kQnJlYWRjcnVtYiA9IGJyZWFkY3J1bWJzLmZpbmQoeCA9PiB4LmxhYmVsID09PSBtb3JlQnJlYWRjcnVtYi5sYWJlbCAmJiB4LnVybCA9PT0gbW9yZUJyZWFkY3J1bWIudXJsKTtcclxuICAgICAgICAvLyBjaGVjayBleGlzdCBpZiBub3QgcHVzaCwgaWYgZXhpc3QgdGhlbiBtYXliZSBjdXN0b21MYWJlbCB3YXMgY2hhbmdlZCBhbmQgdXBkYXRlIGl0IFxyXG4gICAgICAgIGlmICghZm91bmRCcmVhZGNydW1iKSB7XHJcbiAgICAgICAgICBicmVhZGNydW1icy5wdXNoKG1vcmVCcmVhZGNydW1iKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVsICYmIGJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVsLmN1c3RvbUxhYmVsKSB7XHJcbiAgICAgICAgICBmb3VuZEJyZWFkY3J1bWIuY3VzdG9tTGFiZWwgPSBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbC5jdXN0b21MYWJlbDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIHJlY3Vyc2l2ZVxyXG4gICAgICByZXR1cm4gdGhpcy5nZXRCcmVhZGNydW1icyhjaGlsZCwgdXJsLCBicmVhZGNydW1icyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gd2Ugc2hvdWxkIG5ldmVyIGdldCBoZXJlLCBidXQganVzdCBpbiBjYXNlXHJcbiAgICByZXR1cm4gYnJlYWRjcnVtYnM7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0RnVsbFBhdGgocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpOiBzdHJpbmcge1xyXG4gICAgY29uc3QgcmVsYXRpdmVQYXRoID0gKHNlZ21lbnRzOiBVcmxTZWdtZW50W10pID0+IHNlZ21lbnRzLnJlZHVjZSgoYSwgdikgPT4gYSArPSAnLycgKyB2LnBhdGgsICcnKTtcclxuICAgIGNvbnN0IGZ1bGxQYXRoID0gKHJvdXRlczogQWN0aXZhdGVkUm91dGVTbmFwc2hvdFtdKSA9PiByb3V0ZXMucmVkdWNlKChhLCB2KSA9PiBhICs9IHJlbGF0aXZlUGF0aCh2LnVybCksICcnKTtcclxuXHJcbiAgICByZXR1cm4gZnVsbFBhdGgocm91dGUucGF0aEZyb21Sb290KTtcclxuICB9XHJcblxyXG4gIG9uQ2xpY2sodXJsOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt1cmxdKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5yb3V0aW5nU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19