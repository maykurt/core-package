/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { CoreGridService } from './services/core-grid.service';
import { SweetAlertService } from '../../services/index';
import { ConfirmDialogOperationType } from '../../enums/index';
import { GridLoadingService } from './services/grid-loading.service';
export class CoreGridComponent {
    /**
     * @param {?} gridService
     * @param {?} sweetAlertService
     */
    constructor(gridService, sweetAlertService) {
        this.gridService = gridService;
        this.sweetAlertService = sweetAlertService;
        this.loadOnInit = true;
        this.selectedChanged = new EventEmitter();
        this.doubleClickSelectedChanged = new EventEmitter();
        this.recordEvents = new EventEmitter();
        this.retrievedDataSourceEvent = new EventEmitter();
        this.paginationSelectorSource = [];
        this.paginationSelectorSource = [
            { Id: 1, Definition: 1 },
            { Id: 5, Definition: 5 },
            { Id: 10, Definition: 10 },
            { Id: 25, Definition: 25 },
            { Id: 50, Definition: 50 }
        ];
        this.gridStyle = {
            'height': '500px',
            'margin-top': '-1px'
        };
        this.recordEventsSubscription = this.gridService.recordEventsSubject
            .subscribe((data) => {
            this.recordEvents.emit(data);
        });
        this.retrievedDataSourceEventSubscription = this.gridService.retrievedDataSourceSubject
            .subscribe((data) => {
            this.retrievedDataSourceEvent.emit(data);
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.gridService.gridOptions = this.gridOptions;
        this.gridService.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.isInsideModal && this.isInsideModal) {
            /** @type {?} */
            const height = window.innerHeight * 60 / 100;
            this.gridStyle = {
                'height': height > 500 ? '500px' : height + 'px',
                'margin-top': '-1px'
            };
        }
    }
    /**
     * @return {?}
     */
    load() {
        this.gridService.init(this.gridOptions, true);
    }
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    setColumnDataSource(columnName, dataSource) {
        this.gridService.setColumnDataSource(columnName, dataSource);
    }
    /**
     * @return {?}
     */
    refreshGridDataSource() {
        this.gridService.refreshGridData(true);
    }
    /**
     * @return {?}
     */
    reloadColumnDefs() {
        this.gridService.reloadColumnDefs();
    }
    /**
     * @return {?}
     */
    isDataUpdated() {
        return true;
    }
    /**
     * @return {?}
     */
    addRow() {
        this.gridService.addRow();
    }
    /**
     * @return {?}
     */
    revertDataSource() {
        this.gridService.revertData();
    }
    /**
     * @return {?}
     */
    getBulkOperationData() {
        return this.gridService.getBulkOperationData();
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    removeRow(params, rowId, key) {
        this.gridService.removeRow(params, rowId, key);
    }
    /**
     * @return {?}
     */
    refreshAndRevertChanges() {
        this.gridService.refreshAndRevertChanges();
    }
    /**
     * @return {?}
     */
    validateForm() {
        return this.gridService.validateForm();
    }
    /**
     * @return {?}
     */
    showAddButton() {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showAddButton !== undefined) {
                return this.gridOptions.buttonSettings.showAddButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    /**
     * @return {?}
     */
    showReloadButton() {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showReloadButton !== undefined) {
                return this.gridOptions.buttonSettings.showReloadButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    /**
     * @return {?}
     */
    showRevertButton() {
        return this.isChanged() && this.isEditable();
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.gridOptions && this.gridOptions.editable;
    }
    /**
     * @return {?}
     */
    isChanged() {
        return this.gridService.isChanged();
    }
    /**
     * @return {?}
     */
    getTotalCount() {
        return this.gridService.getTotalCount();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSelectionChanged(event) {
        /** @type {?} */
        const selectedRows = this.gridService.getSelectedRows();
        this.selectedChanged.emit(selectedRows);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onRowDoubleClick(event) {
        /** @type {?} */
        const selectedRows = this.gridService.getSelectedRows();
        this.doubleClickSelectedChanged.emit(selectedRows);
    }
    /**
     * @return {?}
     */
    getSelectedRows() {
        return this.gridService.getSelectedRows();
    }
    /**
     * @return {?}
     */
    getSelectedRow() {
        return _.head(this.gridService.getSelectedRows());
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onFirstValue(data) {
        if (data.Id) {
            this.prevItemsPerPage = data.Id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onChangeItemsPerPage(event) {
        if (this.gridOptions && this.gridOptions.editable) {
            /** @type {?} */
            const bulkData = this.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                    .subscribe((res) => {
                    if (res && res.value) {
                        this.gridService.onChangeItemsPerPage(event);
                        this.prevItemsPerPage = event.Id;
                    }
                    else {
                        this.gridService.pagingResult.PageSize = this.prevItemsPerPage;
                    }
                });
            }
            else {
                this.gridService.onChangeItemsPerPage(event);
                this.prevItemsPerPage = event.Id;
            }
        }
        else {
            this.gridService.onChangeItemsPerPage(event);
            this.prevItemsPerPage = event.Id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onPageChanged(event) {
        if (event.page !== this.currentPage) {
            if (this.gridOptions && this.gridOptions.editable) {
                /** @type {?} */
                const bulkData = this.getBulkOperationData();
                if (bulkData && bulkData.length > 0) {
                    this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                        .subscribe((res) => {
                        if (res && res.value) {
                            this.gridService.onPageChanged(event);
                        }
                        else {
                            this.currentPage = this.gridService.pagingResult.CurrentPage;
                        }
                    });
                }
                else {
                    this.gridService.onPageChanged(event);
                }
            }
            else {
                this.gridService.onPageChanged(event);
            }
        }
    }
    /**
     * @return {?}
     */
    getColumnState() {
        this.gridService.getColumnState();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.recordEventsSubscription) {
            this.recordEventsSubscription.unsubscribe();
        }
        if (this.retrievedDataSourceEventSubscription) {
            this.retrievedDataSourceEventSubscription.unsubscribe();
        }
    }
}
CoreGridComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid',
                template: "<div class=\"core-grid\">\r\n  <div class=\"core-grid-header\">\r\n    <h5>{{gridService.gridOptions.title | translate}}</h5>\r\n\r\n    <button *ngIf=\"showReloadButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\"{{'Refresh' | translate}}\"\r\n            (click)=\"refreshGridDataSource()\">\r\n      <core-icon icon=\"sync\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showAddButton()\" type=\"button\" class=\"btn btn-sm btn-info\" title=\"{{'Add' | translate}}\"\r\n            (click)=\"addRow()\">\r\n      <core-icon icon=\"plus\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showRevertButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\" {{'Revert' | translate}}\"\r\n            (click)=\"revertDataSource()\">\r\n      <core-icon icon=\"undo\"></core-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n  <!-- suppressColumnVirtualisation = Set to true so that the grid doesn't virtualise the columns.\r\n  So if you have 100 columns, but only 10 visible due to scrolling, all 100 will always be rendered. -->\r\n\r\n  <ag-grid-angular #coreGrid [ngStyle]=\"gridStyle\" class=\"ag-theme-balham\" [rowHeight]=\"40\"\r\n                   [floatingFiltersHeight]=\"40\" [suppressRowTransform]=\"true\" [sortingOrder]=\"['asc', 'desc']\"\r\n                   [sortable]=\"gridService.gridOptions.enableSorting\" [filter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowBuffer]=\"50\" [suppressColumnVirtualisation]=\"true\" [suppressDragLeaveHidesColumns]=\"true\"\r\n                   [ensureDomOrder]=\"true\" [rowData]=\"gridService.data\" [columnDefs]=\"gridService.columnDefs\"\r\n                   [accentedSort]=\"true\" [floatingFilter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowSelection]=\"gridService.gridOptions.rowSelection\" [context]=\"gridService.getContext(gridService)\"\r\n                   [frameworkComponents]=\"gridService.getComponents()\"\r\n                   [noRowsOverlayComponent]=\"gridService.getNoRowsOverlayComponent()\"\r\n                   [loadingOverlayComponent]=\"gridService.getLoadingOverlayComponent()\"\r\n                   (rowDataChanged)=\"gridService.refreshFormControls()\" (gridReady)=\"gridService.onGridReady($event)\"\r\n                   (columnEverythingChanged)=\"gridService.onColumnEverythingChanged($event)\"\r\n                   (gridSizeChanged)=\"gridService.onGridSizeChanged()\"\r\n                   (cellValueChanged)=\"gridService.onCellValueChanged($event)\"\r\n                   (filterChanged)=\"gridService.onFilterChanged($event)\" (selectionChanged)=\"onSelectionChanged($event)\"\r\n                   (rowDoubleClicked)=\"onRowDoubleClick($event)\">\r\n  </ag-grid-angular>\r\n\r\n  <!-- (sortChanged)=\"gridService.onSortChanged($event)\"  -->\r\n\r\n  <div #inputRef class=\"under-grid-footer\">\r\n    <ng-content select=\"[footer]\"></ng-content>\r\n  </div>\r\n\r\n  <div *ngIf=\"gridService && gridService.pagingResult && (gridService.pagingResult.PageSize || gridService.pagingResult.PageSize === 0)\"\r\n       class=\"core-grid-pager\">\r\n    <div>\r\n      <layout-static-selector class=\"pull-left grid-static-layout-selector\" [dataSource]=\"paginationSelectorSource\"\r\n                              [searchable]=\"false\" [clearable]=\"false\" [(ngModel)]=\"gridService.pagingResult.PageSize\"\r\n                              (changed)=\"onChangeItemsPerPage($event)\" (firstValue)=\"onFirstValue($event)\"\r\n                              ngDefaultControl>\r\n      </layout-static-selector>\r\n\r\n      <pagination class=\"grid-pagination\" [totalItems]=\"gridService.pagingResult.TotalCount\"\r\n                  [itemsPerPage]=\"gridService.pagingResult.PageSize\" [(ngModel)]=\"currentPage\" [maxSize]=\"5\"\r\n                  [rotate]=\"true\" [boundaryLinks]=\"true\" previousText=\"{{'PreviousePage'|translate}}\"\r\n                  nextText=\"{{'NextPage'|translate}}\" firstText=\"{{'FirstPage'|translate}}\"\r\n                  lastText=\"{{'LastPage'|translate}}\" (pageChanged)=\"onPageChanged($event)\">\r\n      </pagination>\r\n\r\n    </div>\r\n\r\n    <div>\r\n      <span> <b>{{'TotalRowCount'|translate}}: </b>{{gridService.pagingResult.TotalCount}}</span>\r\n    </div>\r\n  </div>\r\n</div>",
                encapsulation: ViewEncapsulation.None,
                providers: [GridLoadingService, CoreGridService],
                styles: [".ag-cell{overflow:visible!important}.core-grid-header{font-size:12px!important;background-color:#166dba!important;border:none!important;padding:5px;height:37px;border-radius:2px 2px 0 0}.core-grid-header h5{float:left;margin-top:4px;color:#fff;font-size:14px;padding-left:5px}.btn-sm{float:right;margin-left:8px}.ag-header-cell{background-color:#fff!important;color:#175169!important;font-weight:700!important}.ag-cell,.ag-floating-filter-body{padding-top:1px}.ag-cell input,.ag-floating-filter-body input{height:30px!important;margin-top:3px}.ag-floating-filter-body input{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;height:30px!important;margin-top:4px}.ag-cell ng-select input,.ag-floating-filter-body ng-select input{height:19px!important}.ag-floating-filter-button{padding-top:3px}.core-grid-pager{background:#fff;display:flex;align-items:center;overflow:hidden;font-size:14px;margin-top:-1px;border:1px solid #bdc3c7;justify-content:space-between;padding:5px 12px}.grid-pagination{padding:0;float:left;margin-bottom:0}.grid-static-layout-selector{margin-right:12px;margin-bottom:2px}.ag-floating-filter-button{display:none}.grid-filter-icon{position:absolute;right:-22px;top:2px}.under-grid-footer{background-color:#fff;border-right:1px solid #d7dfe3;border-left:1px solid #d7dfe3;border-top:0}.grid-pagination .page-item.active .page-link{background-color:#166dba!important;border-color:#166dba!important}"]
            }] }
];
/** @nocollapse */
CoreGridComponent.ctorParameters = () => [
    { type: CoreGridService },
    { type: SweetAlertService }
];
CoreGridComponent.propDecorators = {
    gridOptions: [{ type: Input }],
    loadOnInit: [{ type: Input }],
    selectedChanged: [{ type: Output }],
    doubleClickSelectedChanged: [{ type: Output }],
    recordEvents: [{ type: Output }],
    retrievedDataSourceEvent: [{ type: Output }],
    loadColumnDataSourceOnInit: [{ type: Input }],
    isInsideModal: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CoreGridComponent.prototype.gridOptions;
    /** @type {?} */
    CoreGridComponent.prototype.loadOnInit;
    /** @type {?} */
    CoreGridComponent.prototype.selectedChanged;
    /** @type {?} */
    CoreGridComponent.prototype.doubleClickSelectedChanged;
    /** @type {?} */
    CoreGridComponent.prototype.recordEvents;
    /** @type {?} */
    CoreGridComponent.prototype.retrievedDataSourceEvent;
    /** @type {?} */
    CoreGridComponent.prototype.paginationSelectorSource;
    /** @type {?} */
    CoreGridComponent.prototype.loadColumnDataSourceOnInit;
    /** @type {?} */
    CoreGridComponent.prototype.currentPage;
    /** @type {?} */
    CoreGridComponent.prototype.prevItemsPerPage;
    /** @type {?} */
    CoreGridComponent.prototype.gridStyle;
    /** @type {?} */
    CoreGridComponent.prototype.isInsideModal;
    /** @type {?} */
    CoreGridComponent.prototype.recordEventsSubscription;
    /** @type {?} */
    CoreGridComponent.prototype.retrievedDataSourceEventSubscription;
    /** @type {?} */
    CoreGridComponent.prototype.gridService;
    /**
     * @type {?}
     * @private
     */
    CoreGridComponent.prototype.sweetAlertService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jb3JlLWdyaWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULEtBQUssRUFFTCxpQkFBaUIsRUFDakIsTUFBTSxFQUNOLFlBQVksRUFJYixNQUFNLGVBQWUsQ0FBQztBQUd2QixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFL0QsT0FBTyxFQUNMLGlCQUFpQixFQUNsQixNQUFNLHNCQUFzQixDQUFDO0FBTzlCLE9BQU8sRUFDTCwwQkFBMEIsRUFDM0IsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQVVyRSxNQUFNLE9BQU8saUJBQWlCOzs7OztJQW9CNUIsWUFBbUIsV0FBNEIsRUFBVSxpQkFBb0M7UUFBMUUsZ0JBQVcsR0FBWCxXQUFXLENBQWlCO1FBQVUsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQWxCcEYsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNqQixvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7UUFDNUMsK0JBQTBCLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUN2RCxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdkMsNkJBQXdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3RCw2QkFBd0IsR0FBUSxFQUFFLENBQUM7UUFjakMsSUFBSSxDQUFDLHdCQUF3QixHQUFHO1lBQzlCLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFO1lBQ3hCLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFO1lBQ3hCLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFO1lBQzFCLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFO1lBQzFCLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFO1NBQzNCLENBQUM7UUFFRixJQUFJLENBQUMsU0FBUyxHQUFHO1lBQ2YsUUFBUSxFQUFFLE9BQU87WUFDakIsWUFBWSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUVGLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQjthQUNqRSxTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksQ0FBQyxvQ0FBb0MsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQjthQUNwRixTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQztJQUM1RixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTs7a0JBQ3pDLE1BQU0sR0FBVyxNQUFNLENBQUMsV0FBVyxHQUFHLEVBQUUsR0FBRyxHQUFHO1lBRXBELElBQUksQ0FBQyxTQUFTLEdBQUc7Z0JBQ2YsUUFBUSxFQUFFLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUk7Z0JBQ2hELFlBQVksRUFBRSxNQUFNO2FBQ3JCLENBQUM7U0FDSDtJQUNILENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxVQUFrQixFQUFFLFVBQWU7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDL0QsQ0FBQzs7OztJQUVELHFCQUFxQjtRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELG9CQUFvQjtRQUNsQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7O0lBRUQsU0FBUyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsR0FBRztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7SUFFRCx1QkFBdUI7UUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFO1lBQ3ZELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsYUFBYSxLQUFLLFNBQVMsRUFBRTtnQkFDL0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUM7YUFDdEQ7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNkLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUN2RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGdCQUFnQixLQUFLLFNBQVMsRUFBRTtnQkFDbEUsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQzthQUN6RDtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDOzs7O0lBRUQsZ0JBQWdCO1FBQ2QsT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQy9DLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO0lBQ3ZELENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsS0FBSzs7Y0FDaEIsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFO1FBQ3ZELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsS0FBSzs7Y0FDZCxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUU7UUFDdkQsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUQsY0FBYztRQUNaLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Ozs7SUFHRCxZQUFZLENBQUMsSUFBUztRQUNwQixJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDWCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsS0FBVTtRQUM3QixJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUU7O2tCQUMzQyxRQUFRLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzVDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDO3FCQUN0RixTQUFTLENBQUMsQ0FBQyxHQUFnQixFQUFFLEVBQUU7b0JBQzlCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7d0JBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDO3FCQUNsQzt5QkFBTTt3QkFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO3FCQUNoRTtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDO2FBQ2xDO1NBRUY7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUM7U0FDbEM7SUFFSCxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFVO1FBQ3RCLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ25DLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTs7c0JBQzNDLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzVDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDO3lCQUN0RixTQUFTLENBQUMsQ0FBQyxHQUFnQixFQUFFLEVBQUU7d0JBQzlCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7NEJBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUN2Qzs2QkFBTTs0QkFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQzt5QkFDOUQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZDO2FBRUY7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkM7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLG9DQUFvQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxvQ0FBb0MsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6RDtJQUNILENBQUM7OztZQTdPRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsZ3ZJQUF5QztnQkFFekMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLFNBQVMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQzs7YUFDakQ7Ozs7WUF4QlEsZUFBZTtZQUd0QixpQkFBaUI7OzswQkF1QmhCLEtBQUs7eUJBQ0wsS0FBSzs4QkFDTCxNQUFNO3lDQUNOLE1BQU07MkJBQ04sTUFBTTt1Q0FDTixNQUFNO3lDQUdOLEtBQUs7NEJBTUwsS0FBSzs7OztJQWROLHdDQUFrQzs7SUFDbEMsdUNBQTJCOztJQUMzQiw0Q0FBc0Q7O0lBQ3RELHVEQUFpRTs7SUFDakUseUNBQWlEOztJQUNqRCxxREFBNkQ7O0lBQzdELHFEQUFtQzs7SUFFbkMsdURBQThDOztJQUU5Qyx3Q0FBb0I7O0lBQ3BCLDZDQUF5Qjs7SUFFekIsc0NBQXNCOztJQUN0QiwwQ0FBaUM7O0lBRWpDLHFEQUF1Qzs7SUFDdkMsaUVBQW1EOztJQUV2Qyx3Q0FBbUM7Ozs7O0lBQUUsOENBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgSW5wdXQsXHJcbiAgT25Jbml0LFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgU2ltcGxlQ2hhbmdlcyxcclxuICBPbkNoYW5nZXMsXHJcbiAgT25EZXN0cm95XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgQ29yZUdyaWRTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb3JlLWdyaWQuc2VydmljZSc7XHJcblxyXG5pbXBvcnQge1xyXG4gIFN3ZWV0QWxlcnRTZXJ2aWNlXHJcbn0gZnJvbSAnLi4vLi4vc2VydmljZXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICBHcmlkT3B0aW9ucyxcclxuICBBbGVydFJlc3VsdFxyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlXHJcbn0gZnJvbSAnLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgR3JpZExvYWRpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9ncmlkLWxvYWRpbmcuc2VydmljZSc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29yZS1ncmlkLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb3JlLWdyaWQuY29tcG9uZW50LnNjc3MnXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHByb3ZpZGVyczogW0dyaWRMb2FkaW5nU2VydmljZSwgQ29yZUdyaWRTZXJ2aWNlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZUdyaWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcywgT25EZXN0cm95IHtcclxuICBASW5wdXQoKSBncmlkT3B0aW9uczogR3JpZE9wdGlvbnM7XHJcbiAgQElucHV0KCkgbG9hZE9uSW5pdCA9IHRydWU7XHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgQE91dHB1dCgpIGRvdWJsZUNsaWNrU2VsZWN0ZWRDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICBAT3V0cHV0KCkgcmVjb3JkRXZlbnRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHJldHJpZXZlZERhdGFTb3VyY2VFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIHBhZ2luYXRpb25TZWxlY3RvclNvdXJjZTogYW55ID0gW107XHJcblxyXG4gIEBJbnB1dCgpIGxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0PzogYm9vbGVhbjtcclxuXHJcbiAgY3VycmVudFBhZ2U6IG51bWJlcjtcclxuICBwcmV2SXRlbXNQZXJQYWdlOiBudW1iZXI7XHJcblxyXG4gIHB1YmxpYyBncmlkU3R5bGU6IGFueTtcclxuICBASW5wdXQoKSBpc0luc2lkZU1vZGFsPzogYm9vbGVhbjtcclxuXHJcbiAgcmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgcmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBncmlkU2VydmljZTogQ29yZUdyaWRTZXJ2aWNlLCBwcml2YXRlIHN3ZWV0QWxlcnRTZXJ2aWNlOiBTd2VldEFsZXJ0U2VydmljZSkge1xyXG4gICAgdGhpcy5wYWdpbmF0aW9uU2VsZWN0b3JTb3VyY2UgPSBbXHJcbiAgICAgIHsgSWQ6IDEsIERlZmluaXRpb246IDEgfSxcclxuICAgICAgeyBJZDogNSwgRGVmaW5pdGlvbjogNSB9LFxyXG4gICAgICB7IElkOiAxMCwgRGVmaW5pdGlvbjogMTAgfSxcclxuICAgICAgeyBJZDogMjUsIERlZmluaXRpb246IDI1IH0sXHJcbiAgICAgIHsgSWQ6IDUwLCBEZWZpbml0aW9uOiA1MCB9XHJcbiAgICBdO1xyXG5cclxuICAgIHRoaXMuZ3JpZFN0eWxlID0ge1xyXG4gICAgICAnaGVpZ2h0JzogJzUwMHB4JyxcclxuICAgICAgJ21hcmdpbi10b3AnOiAnLTFweCdcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yZWNvcmRFdmVudHNTdWJzY3JpcHRpb24gPSB0aGlzLmdyaWRTZXJ2aWNlLnJlY29yZEV2ZW50c1N1YmplY3RcclxuICAgICAgLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgdGhpcy5yZWNvcmRFdmVudHMuZW1pdChkYXRhKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgdGhpcy5yZXRyaWV2ZWREYXRhU291cmNlRXZlbnRTdWJzY3JpcHRpb24gPSB0aGlzLmdyaWRTZXJ2aWNlLnJldHJpZXZlZERhdGFTb3VyY2VTdWJqZWN0XHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgIHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50LmVtaXQoZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRTZXJ2aWNlLmdyaWRPcHRpb25zID0gdGhpcy5ncmlkT3B0aW9ucztcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuaW5pdCh0aGlzLmdyaWRPcHRpb25zLCB0aGlzLmxvYWRPbkluaXQsIHRoaXMubG9hZENvbHVtbkRhdGFTb3VyY2VPbkluaXQpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMuaXNJbnNpZGVNb2RhbCAmJiB0aGlzLmlzSW5zaWRlTW9kYWwpIHtcclxuICAgICAgY29uc3QgaGVpZ2h0OiBudW1iZXIgPSB3aW5kb3cuaW5uZXJIZWlnaHQgKiA2MCAvIDEwMDtcclxuXHJcbiAgICAgIHRoaXMuZ3JpZFN0eWxlID0ge1xyXG4gICAgICAgICdoZWlnaHQnOiBoZWlnaHQgPiA1MDAgPyAnNTAwcHgnIDogaGVpZ2h0ICsgJ3B4JyxcclxuICAgICAgICAnbWFyZ2luLXRvcCc6ICctMXB4J1xyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuaW5pdCh0aGlzLmdyaWRPcHRpb25zLCB0cnVlKTtcclxuICB9XHJcblxyXG4gIHNldENvbHVtbkRhdGFTb3VyY2UoY29sdW1uTmFtZTogc3RyaW5nLCBkYXRhU291cmNlOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2Uuc2V0Q29sdW1uRGF0YVNvdXJjZShjb2x1bW5OYW1lLCBkYXRhU291cmNlKTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hHcmlkRGF0YVNvdXJjZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UucmVmcmVzaEdyaWREYXRhKHRydWUpO1xyXG4gIH1cclxuXHJcbiAgcmVsb2FkQ29sdW1uRGVmcygpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UucmVsb2FkQ29sdW1uRGVmcygpO1xyXG4gIH1cclxuXHJcbiAgaXNEYXRhVXBkYXRlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgYWRkUm93KCk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5hZGRSb3coKTtcclxuICB9XHJcblxyXG4gIHJldmVydERhdGFTb3VyY2UoKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRTZXJ2aWNlLnJldmVydERhdGEoKTtcclxuICB9XHJcblxyXG4gIGdldEJ1bGtPcGVyYXRpb25EYXRhKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5nZXRCdWxrT3BlcmF0aW9uRGF0YSgpO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlUm93KHBhcmFtcywgcm93SWQsIGtleSk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5yZW1vdmVSb3cocGFyYW1zLCByb3dJZCwga2V5KTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hBbmRSZXZlcnRDaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5yZWZyZXNoQW5kUmV2ZXJ0Q2hhbmdlcygpO1xyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVGb3JtKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZ3JpZFNlcnZpY2UudmFsaWRhdGVGb3JtKCk7XHJcbiAgfVxyXG5cclxuICBzaG93QWRkQnV0dG9uKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncykge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93QWRkQnV0dG9uICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93QWRkQnV0dG9uO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNob3dSZWxvYWRCdXR0b24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzKSB7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSZWxvYWRCdXR0b24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSZWxvYWRCdXR0b247XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2hvd1JldmVydEJ1dHRvbigpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzQ2hhbmdlZCgpICYmIHRoaXMuaXNFZGl0YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGU7XHJcbiAgfVxyXG5cclxuICBpc0NoYW5nZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5pc0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIGdldFRvdGFsQ291bnQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLmdyaWRTZXJ2aWNlLmdldFRvdGFsQ291bnQoKTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0aW9uQ2hhbmdlZChldmVudCk6IHZvaWQge1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRSb3dzID0gdGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGFuZ2VkLmVtaXQoc2VsZWN0ZWRSb3dzKTtcclxuICB9XHJcblxyXG4gIG9uUm93RG91YmxlQ2xpY2soZXZlbnQpOiB2b2lkIHtcclxuICAgIGNvbnN0IHNlbGVjdGVkUm93cyA9IHRoaXMuZ3JpZFNlcnZpY2UuZ2V0U2VsZWN0ZWRSb3dzKCk7XHJcbiAgICB0aGlzLmRvdWJsZUNsaWNrU2VsZWN0ZWRDaGFuZ2VkLmVtaXQoc2VsZWN0ZWRSb3dzKTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93cygpOiBhbnlbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93KCk6IGFueVtdIHtcclxuICAgIHJldHVybiBfLmhlYWQodGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgb25GaXJzdFZhbHVlKGRhdGE6IGFueSk6IHZvaWQge1xyXG4gICAgaWYgKGRhdGEuSWQpIHtcclxuICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZGF0YS5JZDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGUpIHtcclxuICAgICAgY29uc3QgYnVsa0RhdGEgPSB0aGlzLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcbiAgICAgIGlmIChidWxrRGF0YSAmJiBidWxrRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuVW5kbylcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50KTtcclxuICAgICAgICAgICAgICB0aGlzLnByZXZJdGVtc1BlclBhZ2UgPSBldmVudC5JZDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLnBhZ2luZ1Jlc3VsdC5QYWdlU2l6ZSA9IHRoaXMucHJldkl0ZW1zUGVyUGFnZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ncmlkU2VydmljZS5vbkNoYW5nZUl0ZW1zUGVyUGFnZShldmVudCk7XHJcbiAgICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZXZlbnQuSWQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50KTtcclxuICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZXZlbnQuSWQ7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgb25QYWdlQ2hhbmdlZChldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAoZXZlbnQucGFnZSAhPT0gdGhpcy5jdXJyZW50UGFnZSkge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlKSB7XHJcbiAgICAgICAgY29uc3QgYnVsa0RhdGEgPSB0aGlzLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcbiAgICAgICAgaWYgKGJ1bGtEYXRhICYmIGJ1bGtEYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVuZG8pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICBpZiAocmVzICYmIHJlcy52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkU2VydmljZS5vblBhZ2VDaGFuZ2VkKGV2ZW50KTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHRoaXMuZ3JpZFNlcnZpY2UucGFnaW5nUmVzdWx0LkN1cnJlbnRQYWdlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuZ3JpZFNlcnZpY2Uub25QYWdlQ2hhbmdlZChldmVudCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uUGFnZUNoYW5nZWQoZXZlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRDb2x1bW5TdGF0ZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuZ2V0Q29sdW1uU3RhdGUoKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==