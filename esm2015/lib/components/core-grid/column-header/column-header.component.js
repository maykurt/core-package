/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef } from '@angular/core';
import { SweetAlertService } from '../../../services/custom/sweet.alert.confirm.service';
import { ConfirmDialogOperationType } from '../../../enums/confirm-dialog-operation-type.enum';
/**
 * @record
 */
function MyParams() { }
if (false) {
    /** @type {?} */
    MyParams.prototype.menuIcon;
}
export class ColumnHeaderComponent {
    /**
     * @param {?} elementRef
     * @param {?} sweetAlertService
     */
    constructor(elementRef, sweetAlertService) {
        this.sweetAlertService = sweetAlertService;
        this.elementRef = elementRef;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.coreGridService = this.params.context.componentParent;
        this.coldId = this.params.column.getColId();
        this.params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        this.onSortChanged();
    }
    /**
     * @return {?}
     */
    onMenuClick() {
        this.params.showColumnMenu(this.querySelector('.customHeaderMenuButton'));
    }
    /**
     * @param {?} order
     * @param {?} event
     * @return {?}
     */
    onSortRequested(order, event) {
        this.params.setSort(order, event.shiftKey);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onClickHeader(event) {
        if (this.params.enableSorting) {
            /** @type {?} */
            const coreGridService = this.params.context.componentParent;
            /** @type {?} */
            const bulkData = coreGridService.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Cancel)
                    .subscribe((res) => {
                    if (res && res.value) {
                        this.setSort(event);
                    }
                });
            }
            else {
                this.setSort(event);
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    setSort(event) {
        if (this.sorted === '') {
            this.sorted = 'desc';
        }
        else if (this.sorted === 'desc') {
            this.sorted = 'asc';
        }
        else if (this.sorted === 'asc') {
            this.sorted = 'desc';
        }
        /** @type {?} */
        const sortModel = {
            colId: this.coldId,
            sort: this.sorted
        };
        this.coreGridService.onSortChanged(sortModel);
        // this.params.setSort(this.sorted, event.shiftKey);
    }
    /**
     * @return {?}
     */
    onSortChanged() {
        if (this.params.column.isSortAscending()) {
            this.sorted = 'asc';
        }
        else if (this.params.column.isSortDescending()) {
            this.sorted = 'desc';
        }
        else {
            this.sorted = '';
        }
    }
    /**
     * @return {?}
     */
    isColumnSorted() {
        return this.coreGridService.currentSortedColId === this.coldId;
    }
    /**
     * @private
     * @param {?} selector
     * @return {?}
     */
    querySelector(selector) {
        return (/** @type {?} */ (this.elementRef.nativeElement.querySelector('.customHeaderMenuButton', selector)));
    }
}
ColumnHeaderComponent.decorators = [
    { type: Component, args: [{
                template: "<!-- <div  (click) = \"onClickHeader($event)\" style= \"height: 100%; width: 100%;\">\r\n    <div class=\"cutomLabelContainer\">\r\n        <div class=\"customHeaderLabel\">\r\n            {{params.displayName | translate}}\r\n        </div>\r\n\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'desc'\" class=\"{{'customSortDownLabel'+ (this.sorted === 'desc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('desc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-down\"></i>\r\n        </div>\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'asc'\"  class=\"{{'customSortUpLabel'+ (this.sorted === 'asc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('asc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-up\"></i>\r\n        </div>\r\n    </div>\r\n\r\n    <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->\r\n\r\n<div class=\"ag-cell-label-container\" role=\"presentation\">\r\n    <div class=\"ag-header-cell-label\" role=\"presentation\" (click)=\"onClickHeader($event); $event.stopPropagation();\">\r\n        <span class=\"ag-header-cell-text\" role=\"columnheader\">{{params.displayName | translate}}</span>\r\n        <!-- <span ref=\"eFilter\" class=\"ag-header-icon ag-filter-icon\"></span> -->\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'asc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-ascending-icon sort-icons\"></span>\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'desc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-descending-icon sort-icons\"></span>\r\n        <!-- <span ref=\"eSortNone\" class=\"ag-header-icon ag-sort-none-icon\" ></span> -->\r\n    </div>\r\n    <!-- <span [hidden]=\"!params.enableMenu\"  ref=\"eMenu\" class=\"ag-header-icon ag-header-cell-menu-button\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <span class=\"ag-icon ag-icon-menu\"></span>\r\n    </span> -->\r\n    <!-- <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->",
                styles: [".customHeaderMenuButton{position:absolute;right:15px;top:0}.customSortDownLabel{float:left;margin-left:7px;margin-top:1px}.customSortUpLabel{float:left;margin-left:7px}.customSortRemoveLabel{float:left;font-size:11px;margin-left:3px}.cutomLabelContainer{width:calc(100% - 30px);height:100%;overflow:hidden}.customHeaderLabel{margin-left:5px;float:left}.active{color:#6495ed}.sort-icons{margin-top:8px!important}"]
            }] }
];
/** @nocollapse */
ColumnHeaderComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: SweetAlertService }
];
if (false) {
    /** @type {?} */
    ColumnHeaderComponent.prototype.params;
    /** @type {?} */
    ColumnHeaderComponent.prototype.sorted;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.coreGridService;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.coldId;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.sweetAlertService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWhlYWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY29sdW1uLWhlYWRlci9jb2x1bW4taGVhZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLdEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDekYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sbURBQW1ELENBQUM7Ozs7QUFFL0YsdUJBRUM7OztJQURDLDRCQUFpQjs7QUFPbkIsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFPaEMsWUFBWSxVQUFzQixFQUFVLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQzlFLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUM7UUFDM0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUU1QyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztJQUM1RSxDQUFDOzs7Ozs7SUFFRCxlQUFlLENBQUMsS0FBSyxFQUFFLEtBQUs7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFLO1FBQ2pCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUU7O2tCQUN2QixlQUFlLEdBQW9CLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWU7O2tCQUN0RSxRQUFRLEdBQUcsZUFBZSxDQUFDLG9CQUFvQixFQUFFO1lBRXZELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsTUFBTSxDQUFDO3FCQUN4RixTQUFTLENBQUMsQ0FBQyxHQUFRLEVBQUUsRUFBRTtvQkFDdEIsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRTt3QkFDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDckI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3JCO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxLQUFVO1FBRWhCLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7WUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdEI7YUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO2FBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztTQUN0Qjs7Y0FFSyxTQUFTLEdBQUc7WUFDaEIsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTTtTQUNsQjtRQUdELElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlDLG9EQUFvRDtJQUN0RCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUU7WUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckI7YUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLEVBQUU7WUFDaEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdEI7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQzs7OztJQUVELGNBQWM7UUFDWixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNqRSxDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsUUFBZ0I7UUFDcEMsT0FBTyxtQkFBQSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQ2hELHlCQUF5QixFQUFFLFFBQVEsQ0FBQyxFQUFlLENBQUM7SUFDeEQsQ0FBQzs7O1lBeEZGLFNBQVMsU0FBQztnQkFDVCwyMUVBQTJDOzthQUU1Qzs7OztZQWZtQixVQUFVO1lBS3JCLGlCQUFpQjs7OztJQVl4Qix1Q0FBd0I7O0lBQ3hCLHVDQUFzQjs7Ozs7SUFDdEIsMkNBQStCOzs7OztJQUMvQixnREFBeUM7Ozs7O0lBQ3pDLHVDQUF1Qjs7Ozs7SUFFYSxrREFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSUhlYWRlclBhcmFtcyB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgSUhlYWRlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyL21haW4nO1xyXG5pbXBvcnQgeyBDb3JlR3JpZFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9jb3JlLWdyaWQuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBTd2VldEFsZXJ0U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2N1c3RvbS9zd2VldC5hbGVydC5jb25maXJtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZSB9IGZyb20gJy4uLy4uLy4uL2VudW1zL2NvbmZpcm0tZGlhbG9nLW9wZXJhdGlvbi10eXBlLmVudW0nO1xyXG5cclxuaW50ZXJmYWNlIE15UGFyYW1zIGV4dGVuZHMgSUhlYWRlclBhcmFtcyB7XHJcbiAgbWVudUljb246IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgdGVtcGxhdGVVcmw6ICdjb2x1bW4taGVhZGVyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnY29sdW1uLWhlYWRlci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb2x1bW5IZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBJSGVhZGVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBwYXJhbXM6IE15UGFyYW1zO1xyXG4gIHB1YmxpYyBzb3J0ZWQ6IHN0cmluZztcclxuICBwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWY7XHJcbiAgcHJpdmF0ZSBjb3JlR3JpZFNlcnZpY2U6IENvcmVHcmlkU2VydmljZTtcclxuICBwcml2YXRlIGNvbGRJZDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcihlbGVtZW50UmVmOiBFbGVtZW50UmVmLCBwcml2YXRlIHN3ZWV0QWxlcnRTZXJ2aWNlOiBTd2VldEFsZXJ0U2VydmljZSwgKSB7XHJcbiAgICB0aGlzLmVsZW1lbnRSZWYgPSBlbGVtZW50UmVmO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuXHJcbiAgICB0aGlzLmNvcmVHcmlkU2VydmljZSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50O1xyXG4gICAgdGhpcy5jb2xkSWQgPSB0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sSWQoKTtcclxuXHJcbiAgICB0aGlzLnBhcmFtcy5jb2x1bW4uYWRkRXZlbnRMaXN0ZW5lcignc29ydENoYW5nZWQnLCB0aGlzLm9uU29ydENoYW5nZWQuYmluZCh0aGlzKSk7XHJcbiAgICB0aGlzLm9uU29ydENoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIG9uTWVudUNsaWNrKCkge1xyXG4gICAgdGhpcy5wYXJhbXMuc2hvd0NvbHVtbk1lbnUodGhpcy5xdWVyeVNlbGVjdG9yKCcuY3VzdG9tSGVhZGVyTWVudUJ1dHRvbicpKTtcclxuICB9XHJcblxyXG4gIG9uU29ydFJlcXVlc3RlZChvcmRlciwgZXZlbnQpIHtcclxuICAgIHRoaXMucGFyYW1zLnNldFNvcnQob3JkZXIsIGV2ZW50LnNoaWZ0S2V5KTtcclxuICB9XHJcblxyXG4gIG9uQ2xpY2tIZWFkZXIoZXZlbnQpIHtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5lbmFibGVTb3J0aW5nKSB7XHJcbiAgICAgIGNvbnN0IGNvcmVHcmlkU2VydmljZTogQ29yZUdyaWRTZXJ2aWNlID0gdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQ7XHJcbiAgICAgIGNvbnN0IGJ1bGtEYXRhID0gY29yZUdyaWRTZXJ2aWNlLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcblxyXG4gICAgICBpZiAoYnVsa0RhdGEgJiYgYnVsa0RhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkNhbmNlbClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlczogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXMgJiYgcmVzLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5zZXRTb3J0KGV2ZW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZXRTb3J0KGV2ZW50KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2V0U29ydChldmVudDogYW55KTogdm9pZCB7XHJcblxyXG4gICAgaWYgKHRoaXMuc29ydGVkID09PSAnJykge1xyXG4gICAgICB0aGlzLnNvcnRlZCA9ICdkZXNjJztcclxuICAgIH0gZWxzZSBpZiAodGhpcy5zb3J0ZWQgPT09ICdkZXNjJykge1xyXG4gICAgICB0aGlzLnNvcnRlZCA9ICdhc2MnO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNvcnRlZCA9PT0gJ2FzYycpIHtcclxuICAgICAgdGhpcy5zb3J0ZWQgPSAnZGVzYyc7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3Qgc29ydE1vZGVsID0ge1xyXG4gICAgICBjb2xJZDogdGhpcy5jb2xkSWQsXHJcbiAgICAgIHNvcnQ6IHRoaXMuc29ydGVkXHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICB0aGlzLmNvcmVHcmlkU2VydmljZS5vblNvcnRDaGFuZ2VkKHNvcnRNb2RlbCk7XHJcbiAgICAvLyB0aGlzLnBhcmFtcy5zZXRTb3J0KHRoaXMuc29ydGVkLCBldmVudC5zaGlmdEtleSk7XHJcbiAgfVxyXG5cclxuICBvblNvcnRDaGFuZ2VkKCkge1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbHVtbi5pc1NvcnRBc2NlbmRpbmcoKSkge1xyXG4gICAgICB0aGlzLnNvcnRlZCA9ICdhc2MnO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uaXNTb3J0RGVzY2VuZGluZygpKSB7XHJcbiAgICAgIHRoaXMuc29ydGVkID0gJ2Rlc2MnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zb3J0ZWQgPSAnJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzQ29sdW1uU29ydGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29yZUdyaWRTZXJ2aWNlLmN1cnJlbnRTb3J0ZWRDb2xJZCA9PT0gdGhpcy5jb2xkSWQ7XHJcbiAgfVxyXG4gXHJcbiAgcHJpdmF0ZSBxdWVyeVNlbGVjdG9yKHNlbGVjdG9yOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKFxyXG4gICAgICAnLmN1c3RvbUhlYWRlck1lbnVCdXR0b24nLCBzZWxlY3RvcikgYXMgSFRNTEVsZW1lbnQ7XHJcbiAgfVxyXG59XHJcbiJdfQ==