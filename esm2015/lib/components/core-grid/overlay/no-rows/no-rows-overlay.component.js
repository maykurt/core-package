/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class NoRowsOverlayComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
    }
}
NoRowsOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'grid-no-rows-overlay',
                template: `<div>` +
                    `   {{ 'NoRows' | translate }}` +
                    `</div>`
            }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    NoRowsOverlayComponent.prototype.params;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tcm93cy1vdmVybGF5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9vdmVybGF5L25vLXJvd3Mvbm8tcm93cy1vdmVybGF5LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVMxQyxNQUFNLE9BQU8sc0JBQXNCOzs7OztJQUdqQyxNQUFNLENBQUMsTUFBTTtRQUNYLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7OztZQVhGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsc0JBQXNCO2dCQUNoQyxRQUFRLEVBQUUsT0FBTztvQkFDZiwrQkFBK0I7b0JBQy9CLFFBQVE7YUFDWDs7Ozs7OztJQUVDLHdDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJTm9Sb3dzT3ZlcmxheUFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZ3JpZC1uby1yb3dzLW92ZXJsYXknLFxyXG4gIHRlbXBsYXRlOiBgPGRpdj5gICtcclxuICAgIGAgICB7eyAnTm9Sb3dzJyB8IHRyYW5zbGF0ZSB9fWAgK1xyXG4gICAgYDwvZGl2PmBcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vUm93c092ZXJsYXlDb21wb25lbnQgaW1wbGVtZW50cyBJTm9Sb3dzT3ZlcmxheUFuZ3VsYXJDb21wIHtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG5cclxuICBhZ0luaXQocGFyYW1zKTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICB9XHJcbn1cclxuIl19