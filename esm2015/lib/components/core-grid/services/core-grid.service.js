/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
// Enums
import { OperationType, CoreGridCellType, SortType, CoreGridColumnType, ConfirmDialogOperationType, ToastrMessages, Headers, Comparison, Connector } from '../../../enums/index';
// Services
import { DataService, ToastrUtilsService, SweetAlertService } from '../../../services/index';
// components
import { CommandCellComponent } from '../cell/command/command-cell.component';
import { TextFilterCellComponent } from '../cell/filter/text-filter-cell/text-filter-cell.component';
import { SelectFilterCellComponent } from '../cell/filter/select-filter-cell/select-filter-cell.component';
import { DateFilterCellComponent } from '../cell/filter/date-filter-cell/date-filter-cell.component';
import { NumericFilterCellComponent } from '../cell/filter/numeric-filter-cell/numeric-filter-cell.component';
import { BooleanFilterCellComponent } from '../cell/filter/boolean-filter-cell/boolean-filter-cell.component';
import { CurrencyFilterCellComponent } from '../cell/filter/currency-filter-cell/currency-filter-cell.component';
import { LovFilterCellComponent } from '../cell/filter/lov-filter-cell/lov-filter-cell.component';
import { MaskFilterCellComponent } from '../cell/filter/mask-filter-cell/mask-filter-cell.component';
import { SelectCellComponent } from '../cell/read-only/select-cell/select-cell.component';
import { LabelCellComponent } from '../cell/read-only/label-cell/label-cell.component';
import { DateCellComponent } from '../cell/read-only/date-cell/date-cell.component';
import { BooleanCellComponent } from '../cell/read-only/boolean-cell/boolean-cell.component';
import { CurrencyCellComponent } from '../cell/read-only/currency-cell/currency-cell.component';
import { MaskCellComponent } from '../cell/read-only/mask-cell/mask-cell.component';
import { NumericCellComponent } from '../cell/read-only/numeric-cell/numeric-cell.component';
import { TextEditCellComponent } from '../cell/edit/text-edit-cell/text-edit-cell.component';
import { SelectEditCellComponent } from '../cell/edit/select-edit-cell/select-edit-cell.component';
import { NumericEditCellComponent } from '../cell/edit/numeric-edit-cell/numeric-edit-cell.component';
import { LovEditCellComponent } from '../cell/edit/lov-edit-cell/lov-edit-cell.component';
import { DateEditCellComponent } from '../cell/edit/date-edit-cell/date-edit-cell.component';
import { BooleanEditCellComponent } from '../cell/edit/boolean-edit-cell/boolean-edit-cell.component';
import { MaskEditCellComponent } from '../cell/edit/mask-edit-cell/mask-edit-cell.component';
import { CurrencyEditCellComponent } from '../cell/edit/currency-edit-cell/currency-edit-cell.component';
import { ColumnHeaderComponent } from '../column-header/column-header.component';
import { NoRowsOverlayComponent } from '../overlay/no-rows/no-rows-overlay.component';
import { LoadingOverlayComponent } from '../overlay/loading/loading-overlay.component';
import { GridLoadingService } from './grid-loading.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../services/http/data.service";
import * as i2 from "../../../services/custom/toastr.service";
import * as i3 from "./grid-loading.service";
import * as i4 from "../../../services/custom/sweet.alert.confirm.service";
import * as i5 from "@angular/router";
export class CoreGridService {
    /**
     * @param {?} dataService
     * @param {?} toastr
     * @param {?} loadingService
     * @param {?} sweetAlertService
     * @param {?} ngZone
     * @param {?} router
     * @param {?} route
     */
    constructor(dataService, toastr, loadingService, sweetAlertService, ngZone, router, route) {
        this.dataService = dataService;
        this.toastr = toastr;
        this.loadingService = loadingService;
        this.sweetAlertService = sweetAlertService;
        this.ngZone = ngZone;
        this.router = router;
        this.route = route;
        this.pagingResult = {};
        this.data = [];
        this.originalRowData = [];
        this.deletedData = [];
        this.filter = [];
        this.filterGroupList = [];
        this.gridForm = new FormGroup({});
        this.loadOnInit = true;
        this.isDataChanged = false;
        this.isLoad = false;
        this.columnDefs = [];
        this.allGridColumns = [];
        this.recordEventsSubject = new Subject();
        this.retrievedDataSourceSubject = new Subject();
        this.loadingDisplayStatusSubscription = this.loadingService.getLoadingBarDisplayStatus().subscribe((data) => {
            this.loadingDisplayStatus = data;
            this.loadingOverlay();
        });
    }
    /**
     * @param {?} options
     * @param {?} loadOnInit
     * @param {?=} loadColumnDataSourceOnInit
     * @return {?}
     */
    init(options, loadOnInit, loadColumnDataSourceOnInit) {
        this.loadOnInit = loadOnInit;
        this.loadColumnDataSourceOnInit = loadColumnDataSourceOnInit;
        this.gridOptions = options;
        if (this.gridApi) {
            this.pagingResult = {
                CurrentPage: 1,
                PageSize: options.requestOptions.CustomFilter.PageSize
            };
            this.reloadColumnDefs();
            this.setLoadUrlSetting();
            if (this.loadOnInit === true) {
                this.isLoad = true;
                this.refreshGridData();
            }
        }
    }
    /**
     * @return {?}
     */
    setLoadUrlSetting() {
        this.urlOptions = {
            moduleUrl: this.gridOptions.requestOptions.UrlOptions.moduleUrl,
            endPointUrl: this.gridOptions.requestOptions.UrlOptions.endPointUrl
        };
    }
    /**
     * @private
     * @return {?}
     */
    createColumns() {
        if (this.gridOptions && this.gridOptions.enableCheckboxSelection) {
            /** @type {?} */
            const selectColumn = {
                headerName: 'Select',
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: true,
                checkboxSelection: true,
                minWidth: 40,
                maxWidth: 40,
                cellStyle: { 'padding-top': '5px' },
                showRowGroup: false,
                enableRowGroup: false,
                enablePivot: false,
                suppressFilter: true,
                menuTabs: []
            };
            this.columnDefs.push(selectColumn);
        }
        this.gridOptions.columns.forEach(item => {
            /** @type {?} */
            let cellRenderer;
            /** @type {?} */
            let columEditable = this.gridOptions.editable;
            if (item.editable !== undefined) {
                columEditable = item.editable;
            }
            if (columEditable) {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreTextEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreLOVEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyEditCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
            }
            else {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.TextSelect) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
            }
            if (item.visible !== false) {
                item.visible = true;
            }
            else {
                item.visible = false;
            }
            /** @type {?} */
            let headerName = item.field;
            if (this.gridOptions
                && this.gridOptions.requestOptions
                && this.gridOptions.requestOptions.UrlOptions
                && this.gridOptions.requestOptions.UrlOptions.endPointUrl) {
                /** @type {?} */
                const endPointUrl = this.gridOptions.requestOptions.UrlOptions.endPointUrl;
                if (endPointUrl.indexOf('/') !== -1) {
                    headerName = endPointUrl.split('/')[0] + item.field;
                }
                else {
                    headerName = endPointUrl + item.field;
                }
            }
            /** @type {?} */
            const column = {
                field: item.field,
                headerName: item.headerName || headerName,
                cellRenderer,
                hide: !item.visible,
                suppressSizeToFit: null,
                filter: this.gridOptions.enableFilter || false,
                sortable: this.gridOptions.enableSorting || false,
                resizable: true,
                cellEditorParams: {
                    dataSource: null,
                    settings: null,
                    data: null,
                    customDisplayFunction: item.customDisplayFunction || null,
                    customEditableFunction: item.customEditableFunction || null,
                    customFunctionForRowValueChanges: item.customFunctionForRowValueChanges || null
                },
                floatingFilterComponentParams: {
                    hideFilter: item.hideFilter || false,
                    serviceAccess: this,
                    field: item.field,
                    customFilterFunction: item.customFilterFunction || null
                },
                customFieldForFilter: item.customFieldForFilter,
                customFieldForSort: item.customFieldForSort,
                getDataSourceAfterDataLoaded: item.getDataSourceAfterDataLoaded,
                columnType: item.columnType
            };
            if (item.field === this.gridOptions.keyField) {
                column.suppressSizeToFit = true;
            }
            if (item.columnType === CoreGridColumnType.Date && (this.gridOptions.editable || column['editable'])) {
                column['minWidth'] = 135;
            }
            if (item.minWidth) {
                column['minWidth'] = item.minWidth;
            }
            if (item.maxWidth) {
                column['maxWidth'] = item.maxWidth;
            }
            if (item.dataSource) {
                column.cellEditorParams.dataSource = item.dataSource;
            }
            if (item.settings) {
                column.cellEditorParams.settings = item.settings;
            }
            if ((item.columnType === CoreGridColumnType.Select || cellRenderer === CoreGridCellType.CoreSelectCell)
                && item.settings) {
                column.cellEditorParams.data = new Subject();
                if (!item.getDataSourceAfterDataLoaded) {
                    this.getColumnDataSource(column.cellEditorParams.data, column);
                }
            }
            /** @type {?} */
            const index = _.findIndex(this.columnDefs, ['field', item.field]);
            if (index < 0) {
                this.columnDefs.push(column);
            }
            else {
                this.columnDefs[index] = column;
            }
        });
        this.setFilterComponents();
        this.addCommandColumn();
    }
    /**
     * @return {?}
     */
    reloadColumnDefs() {
        this.createColumns();
        this.reloadGridColumnDefs();
    }
    /**
     * @return {?}
     */
    getColumnState() {
        console.log(this.columnApi.getColumnState());
    }
    /**
     * @param {?} data
     * @param {?} column
     * @param {?=} extraFilters
     * @return {?}
     */
    getColumnDataSource(data, column, extraFilters) {
        if (this.loadOnInit || this.loadColumnDataSourceOnInit) {
            /** @type {?} */
            const settings = (/** @type {?} */ (column.cellEditorParams.settings));
            /** @type {?} */
            let requestOptions;
            if (settings) {
                if (column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.TextSelect) {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).requestOptions));
                }
                else {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).options.requestOptions));
                }
            }
            if (requestOptions) {
                /** @type {?} */
                const reqUrl = requestOptions.UrlOptions.moduleUrl + requestOptions.UrlOptions.endPointUrl;
                /** @type {?} */
                let disableGeneralLoading = false;
                if (!this.gridOptions.disableSelfLoading) {
                    disableGeneralLoading = true;
                    this.loadingService.insertLoadingRequest(reqUrl);
                }
                /** @type {?} */
                const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                if (extraFilters && extraFilters.length > 0) {
                    if (requestOptions.CustomFilter && requestOptions.CustomFilter.FilterGroups) {
                        if (requestOptions.CustomFilter.FilterGroups.length === 0) {
                            requestOptions.CustomFilter.FilterGroups = [{
                                    Filters: []
                                }];
                        }
                        else if (requestOptions.CustomFilter.FilterGroups.length > 0) {
                            requestOptions.CustomFilter.FilterGroups.unshift({
                                Filters: []
                            });
                        }
                    }
                    else if (!requestOptions.CustomFilter) {
                        requestOptions.CustomFilter = {
                            FilterGroups: [{
                                    Filters: []
                                }]
                        };
                    }
                    else if (requestOptions.CustomFilter && !requestOptions.CustomFilter.FilterGroups) {
                        requestOptions.CustomFilter.FilterGroups = [{
                                Filters: []
                            }];
                    }
                    if (requestOptions.CustomFilter &&
                        requestOptions.CustomFilter.FilterGroups &&
                        requestOptions.CustomFilter.FilterGroups.length > 0) {
                        requestOptions.CustomFilter.FilterGroups[0].Filters = extraFilters;
                    }
                }
                this.dataService.getListBy(reqUrl, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                    .pipe(finalize(() => {
                    if (!this.gridOptions.disableSelfLoading) {
                        this.loadingService.removeLoadingRequest(reqUrl);
                    }
                }))
                    .subscribe((httpResponse) => {
                    /** @type {?} */
                    const lovData = httpResponse.serviceResult.Result;
                    /** @type {?} */
                    const dataSource = lovData.map(x => Object.assign({}, x));
                    /** @type {?} */
                    const colIndex = _.findIndex(this.columnDefs, ['field', column.field]);
                    column.cellEditorParams.dataSource = dataSource;
                    this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                    data.next({ data: true });
                });
            }
        }
    }
    /**
     * @return {?}
     */
    getComponents() {
        /** @type {?} */
        const components = {};
        // Edit Cell
        components[CoreGridCellType.CoreBooleanEditCell] = BooleanEditCellComponent;
        components[CoreGridCellType.CoreDateEditCell] = DateEditCellComponent;
        components[CoreGridCellType.CoreLOVEditCell] = LovEditCellComponent;
        components[CoreGridCellType.CoreNumericEditCell] = NumericEditCellComponent;
        components[CoreGridCellType.CoreSelectEditCell] = SelectEditCellComponent;
        components[CoreGridCellType.CoreTextEditCell] = TextEditCellComponent;
        components[CoreGridCellType.CoreMaskEditCell] = MaskEditCellComponent;
        components[CoreGridCellType.CoreCurrencyEditCell] = CurrencyEditCellComponent;
        // Read-Only Cell
        components[CoreGridCellType.CoreBooleanCell] = BooleanCellComponent;
        components[CoreGridCellType.CoreLabelCell] = LabelCellComponent;
        components[CoreGridCellType.CoreDateCell] = DateCellComponent;
        components[CoreGridCellType.CoreSelectCell] = SelectCellComponent;
        components[CoreGridCellType.CoreCurrencyCell] = CurrencyCellComponent;
        components[CoreGridCellType.CoreMaskCell] = MaskCellComponent;
        components[CoreGridCellType.CoreNumericCell] = NumericCellComponent;
        // Filter Cell
        components[CoreGridCellType.CoreDateFilterCell] = DateFilterCellComponent;
        components[CoreGridCellType.CoreSelectFilterCell] = SelectFilterCellComponent;
        components[CoreGridCellType.CoreTextFilterCell] = TextFilterCellComponent;
        components[CoreGridCellType.CoreNumericFilterCell] = NumericFilterCellComponent;
        components[CoreGridCellType.CoreBooleanFilterCell] = BooleanFilterCellComponent;
        components[CoreGridCellType.CoreCurrencyFilterCell] = CurrencyFilterCellComponent;
        components[CoreGridCellType.CoreLOVFilterCell] = LovFilterCellComponent;
        components[CoreGridCellType.CoreMaskFilterCell] = MaskFilterCellComponent;
        // Command Cell
        components[CoreGridCellType.CoreCommandCell] = CommandCellComponent;
        components['agColumnHeader'] = ColumnHeaderComponent;
        components['customNoRowsOverlay'] = NoRowsOverlayComponent;
        components['customLoadingOverlay'] = LoadingOverlayComponent;
        return components;
    }
    /**
     * @return {?}
     */
    setFilterComponents() {
        this.columnDefs.forEach(item => {
            if (item.cellRenderer === CoreGridCellType.CoreBooleanEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLOVEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreLOVFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreTextEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreBooleanCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLabelCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell
                && item.columnType === CoreGridColumnType.TextSelect) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
        });
    }
    /**
     * @private
     * @param {?} genericExpression
     * @return {?}
     */
    loadDataByFilterAndPageSetting(genericExpression) {
        /** @type {?} */
        const reqUrl = `${this.urlOptions.moduleUrl}${this.urlOptions.endPointUrl}`;
        /** @type {?} */
        let headerParams = [];
        if (this.gridOptions && this.gridOptions.requestOptions && this.gridOptions.requestOptions.HeaderParameters) {
            headerParams = this.gridOptions.requestOptions.HeaderParameters;
        }
        if (!this.gridOptions.disableSelfLoading) {
            this.loadingService.insertLoadingRequest(reqUrl);
            /** @type {?} */
            const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            headerParams.push(disableLoadingHeader);
        }
        return this.dataService.getListBy(reqUrl, genericExpression, headerParams)
            .pipe(finalize(() => {
            if (!this.gridOptions.disableSelfLoading) {
                this.loadingService.removeLoadingRequest(reqUrl);
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    loadGridViewDataByOptionsUrlAndFilters() {
        if (this.gridOptions) {
            /** @type {?} */
            const searchFilter = _.cloneDeep(this.gridOptions.requestOptions.CustomFilter);
            if (this.pagingResult) {
                searchFilter.PageNumber = this.pagingResult.CurrentPage;
                searchFilter.PageSize = this.pagingResult.PageSize;
            }
            searchFilter.FilterGroups = searchFilter.FilterGroups || [];
            this.filter.forEach(element => {
                /** @type {?} */
                let name;
                /** @type {?} */
                const foundColumn = this.gridOptions.columns.find(x => x.field === element.key);
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (element.key.indexOf('Id') > 0 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = element.key.substr(0, element.key.indexOf('Id')) +
                            '.' + element.key.substr(element.key.indexOf('Id'), element.key.length);
                    }
                    else {
                        name = element.key;
                    }
                }
                /** @type {?} */
                const group = {
                    Filters: [{
                            PropertyName: name,
                            Value: element.value['filter'],
                            Comparison: element.value['type'],
                            OtherValue: element.value['other'] || null
                        }]
                };
                searchFilter.FilterGroups.push(group);
            });
            // it is for CoreGridColumnType.TextSelect
            this.filterGroupList.forEach((filterGroupItem) => {
                searchFilter.FilterGroups.unshift(filterGroupItem.value);
            });
            // if (searchFilter.FilterGroups.length > 0) {
            //   searchFilter.FilterGroups[searchFilter.FilterGroups.length - 1]['Connector'] = Connector.Or;
            // }
            this.loadDataByFilterAndPageSetting(searchFilter)
                .subscribe((httpResponse) => {
                this.pagingResult = httpResponse.pagingResult;
                this.data = httpResponse.serviceResult.Result;
                if (this.data) {
                    this.originalRowData = this.data.map(item => Object.assign({}, item));
                }
                this.retrievedDataSourceSubject.next(this.data);
                if (!this.data || (this.data && this.data.length === 0)) {
                    this.loadingOverlay();
                }
                this.loadDataSourceAfterGetGridData();
            });
        }
    }
    /**
     * @return {?}
     */
    loadDataSourceAfterGetGridData() {
        /** @type {?} */
        const needToLoadDataSourceColumns = this.columnDefs.filter(x => x.getDataSourceAfterDataLoaded === true);
        needToLoadDataSourceColumns.forEach(column => {
            /** @type {?} */
            const columnValues = this.data.map(x => x[column.field]);
            /** @type {?} */
            const newFilters = [];
            columnValues.forEach((columnValue) => {
                /** @type {?} */
                const newFilter = {
                    PropertyName: column.cellEditorParams.settings.valueField,
                    Value: columnValue,
                    Comparison: Comparison.EqualTo,
                    Connector: Connector.Or
                };
                /** @type {?} */
                let name = column.cellEditorParams.settings.valueField;
                // can be changable
                if (name) {
                    if (column.cellEditorParams.settings.valueField.indexOf('Id') !== -1 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = name.substr(0, name.indexOf('Id')) +
                            '.' + name.substr(name.indexOf('Id'), name.length);
                    }
                }
                newFilter.PropertyName = name;
                newFilters.push(newFilter);
            });
            if (newFilters.length > 0) {
                newFilters[newFilters.length - 1].Connector = Connector.And;
            }
            if (newFilters.length > 0) {
                this.getColumnDataSource(column.cellEditorParams.data, column, newFilters);
            }
        });
        // getColumnDataSource 
    }
    /**
     * @private
     * @return {?}
     */
    addCommandColumn() {
        /** @type {?} */
        let showRowDeleteButton = true;
        /** @type {?} */
        let showRowEditButton = true;
        /** @type {?} */
        let buttonCounts = 1;
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showRowDeleteButton !== undefined) {
                showRowDeleteButton = this.gridOptions.buttonSettings.showRowDeleteButton;
                if (showRowDeleteButton) {
                    buttonCounts++;
                }
            }
            if (this.gridOptions.buttonSettings.showEditButton !== undefined) {
                showRowEditButton = this.gridOptions.buttonSettings.showEditButton;
                if (showRowEditButton) {
                    buttonCounts++;
                }
            }
        }
        // it is for delete and revert for inline grid
        // if (this.gridOptions.editable && buttonCounts === 1) {
        //   buttonCounts = 2;
        // }
        if (showRowDeleteButton || showRowEditButton) {
            /** @type {?} */
            const index = _.findIndex(this.columnDefs, ['field', 'CommandColumn']);
            if (index < 0) {
                /** @type {?} */
                const column = {
                    headerName: '',
                    field: 'CommandColumn',
                    editable: false,
                    cellRenderer: CoreGridCellType.CoreCommandCell,
                    filter: false,
                    sortable: false,
                    minWidth: buttonCounts * 50,
                    maxWidth: buttonCounts * 50
                };
                this.columnDefs.push(column);
            }
            // this.columnDefs.push();
        }
    }
    /**
     * @private
     * @return {?}
     */
    createFormControls() {
        /** @type {?} */
        const columns = this.columnApi.getAllColumns();
        /** @type {?} */
        const gridFormGroup = ((/** @type {?} */ (this.gridForm)));
        // clear out old form group controls if switching between data sources
        /** @type {?} */
        const controlNames = Object.keys(gridFormGroup.controls);
        controlNames.forEach((controlName) => {
            gridFormGroup.removeControl(controlName);
        });
        this.gridApi.forEachNode((rowNode) => {
            /** @type {?} */
            const formArray = new FormArray([]);
            columns.filter((column) => column.getColDef().field !== 'CommandColumn')
                .forEach((column) => {
                /** @type {?} */
                const key = this.createKey(this.columnApi, column);
                // the cells will use this same createKey method
                /** @type {?} */
                const col = _.find(this.gridOptions.columns, ['field', column.getColDef().field]);
                /** @type {?} */
                const validation = [];
                if (col) {
                    if (col.validation) {
                        if (col.validation.required) {
                            validation.push(Validators.required);
                        }
                        if (col.validation.email) {
                            validation.push(Validators.email);
                        }
                        if (col.validation.requiredTrue) {
                            validation.push(Validators.requiredTrue);
                        }
                        if (col.validation.minValue) {
                            validation.push(Validators.min(col.validation.minValue));
                        }
                        if (col.validation.maxValue) {
                            validation.push(Validators.max(col.validation.maxValue));
                        }
                        if (col.validation.minLength) {
                            validation.push(Validators.minLength(col.validation.minLength));
                        }
                        if (col.validation.maxLength) {
                            validation.push(Validators.maxLength(col.validation.maxLength));
                        }
                        if (col.validation.regex) {
                            validation.push(Validators.pattern(col.validation.regex));
                        }
                    }
                }
                formArray.setControl((/** @type {?} */ (key)), new FormControl('', { validators: validation }));
            });
            gridFormGroup.addControl((/** @type {?} */ (rowNode.id)), formArray);
        });
    }
    /**
     * @private
     * @return {?}
     */
    reloadGridColumnDefs() {
        // this.columnDefs = columnDefs;
        if (this.gridApi) {
            this.gridApi.setColumnDefs(this.columnDefs);
            this.columnApi.resetColumnState();
        }
        this.refreshFormControls();
        if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    setColumnDataSource(columnName, dataSource) {
        /** @type {?} */
        const index = _.findIndex(this.gridOptions.columns, ['field', columnName]);
        this.gridOptions.columns[index].dataSource = dataSource;
        if (this.gridApi) {
            this.gridApi.refreshCells({ columns: [columnName], force: true });
        }
    }
    /**
     * @param {?} column
     * @return {?}
     */
    setColumnLovOptions(column) {
        if ((column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.Lov) && column.settings) {
            /** @type {?} */
            const settings = (/** @type {?} */ (column.settings));
            /** @type {?} */
            let requestOptions;
            if (column.columnType === CoreGridColumnType.Select) {
                requestOptions = ((/** @type {?} */ (settings))).requestOptions;
            }
            else {
                requestOptions = ((/** @type {?} */ (settings))).options.requestOptions;
            }
            /** @type {?} */
            const reqUrl = requestOptions.UrlOptions.moduleUrl + '/'
                + requestOptions.UrlOptions.endPointUrl;
            /** @type {?} */
            let disableGeneralLoading = false;
            if (!this.gridOptions.disableSelfLoading) {
                disableGeneralLoading = true;
                this.loadingService.insertLoadingRequest(reqUrl);
            }
            /** @type {?} */
            const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            this.dataService.getListBy(reqUrl, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                .pipe(finalize(() => {
                if (!this.gridOptions.disableSelfLoading) {
                    this.loadingService.removeLoadingRequest(reqUrl);
                }
            }))
                .subscribe((httpResponse) => {
                /** @type {?} */
                const lovData = httpResponse.serviceResult.Result;
                /** @type {?} */
                const dataSource = lovData.map(x => Object.assign({}, x));
                /** @type {?} */
                const colIndex = _.findIndex(this.columnDefs, ['field', column.field]);
                column.dataSource = dataSource;
                this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                this.columnDefs[colIndex].cellEditorParams.settings = settings;
                if (this.gridApi) {
                    this.gridApi.refreshCells({ columns: [column.field], force: true });
                }
            });
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    onGridReady(params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        this.refreshFormControls();
        if (this.loadingDisplayStatus) {
            this.gridApi.showLoadingOverlay();
        }
        if (this.isLoad === false) {
            this.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
        }
        this.gridApi.sizeColumnsToFit();
    }
    /**
     * @param {?} params
     * @return {?}
     */
    onColumnEverythingChanged(params) {
        this.allGridColumns = params.columnApi.getAllColumns();
    }
    /**
     * @return {?}
     */
    onGridSizeChanged() {
        if (this.gridApi) {
            if (this.gridOptions && !this.gridOptions.disableSizeToFit && this.gridApi) {
                this.gridApi.sizeColumnsToFit();
            }
            this.gridApi.refreshCells({ force: true });
        }
    }
    /**
     * @private
     * @param {?} columnApi
     * @param {?} column
     * @return {?}
     */
    createKey(columnApi, column) {
        return columnApi.getAllColumns().indexOf(column);
    }
    /**
     * @param {?} self
     * @return {?}
     */
    getContext(self) {
        return {
            componentParent: self,
            formGroup: this.gridForm,
            createKey: this.createKey
        };
    }
    /**
     * @return {?}
     */
    refreshFormControls() {
        if (this.gridApi) {
            this.createFormControls();
            this.gridApi.refreshCells({ force: true });
        }
    }
    /**
     * @param {?=} isCheckBulkData
     * @return {?}
     */
    refreshGridData(isCheckBulkData) {
        /** @type {?} */
        const bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0 && isCheckBulkData) {
            this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Refresh)
                .subscribe((res) => {
                if (res && res.value) {
                    this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
                    this.refreshGrid();
                    this.toastr.success(ToastrMessages.GridRefreshed);
                }
            });
        }
        else {
            this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
            this.refreshGrid();
        }
    }
    /**
     * @return {?}
     */
    refreshGrid() {
        this.deletedData = [];
        this.isDataChanged = false;
        this.loadGridViewDataByOptionsUrlAndFilters();
        if (this.isLoad === false) {
            this.init(this.gridOptions, true);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onCellValueChanged(item) {
        this.isDataChanged = true;
        if (item.data.OperationType !== OperationType.Created) {
            item.data.OperationType = OperationType.Updated;
        }
    }
    /**
     * @param {?} sortModel
     * @return {?}
     */
    onSortChanged(sortModel) {
        if (sortModel) {
            // const sortModel = _.head(this.gridApi.getSortModel());
            this.currentSortedColId = sortModel.colId;
            /** @type {?} */
            const columnDef = (/** @type {?} */ (this.gridApi.getColumnDef(sortModel.colId)));
            /** @type {?} */
            let name = columnDef.customFieldForSort;
            if (!name) {
                if (columnDef.field.indexOf('Id') > 0 &&
                    (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                    name = columnDef.field.substr(0, columnDef.field.indexOf('Id'));
                }
                else {
                    name = columnDef.field;
                }
            }
            /** @type {?} */
            const sort = [{
                    PropertyName: name,
                    Type: sortModel.sort === 'asc' ? SortType.Asc : SortType.Desc
                }];
            this.gridOptions.requestOptions.CustomFilter.Sort = sort;
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onFilterChanged(event) {
        // this.filter = Object.entries(this.gridApi.getFilterModel()).map(([key, value]) => ({ key, value }));
        this.loadGridViewDataByOptionsUrlAndFilters();
    }
    /**
     * @param {?} field
     * @param {?} model
     * @return {?}
     */
    applyChangesToFilter(field, model) {
        this.addToListForFilter(this.filter, model.filter || model.filter === 0 ? model : null, field);
    }
    /**
     * @param {?} listArray
     * @param {?} value
     * @param {?} key
     * @return {?}
     */
    addToListForFilter(listArray, value, key) {
        /** @type {?} */
        const foundDataIndex = listArray.findIndex(x => x.key === key);
        if (value) {
            /** @type {?} */
            const dataToSend = {
                key,
                value
            };
            if (foundDataIndex !== -1) {
                listArray[foundDataIndex] = dataToSend;
            }
            else {
                listArray.push(dataToSend);
            }
        }
        else {
            if (foundDataIndex !== -1) {
                listArray.splice(foundDataIndex, 1);
            }
        }
    }
    // called from text filter for CoreGridColumnType.TextSelect
    /**
     * @param {?} field
     * @param {?} settings
     * @param {?} model
     * @return {?}
     */
    textSelectFindValuesOnFilter(field, settings, model) {
        /** @type {?} */
        const foundColumn = this.gridOptions.columns.find(x => x.field === field);
        if (model.filter) {
            /** @type {?} */
            const filter = {
                PageSize: 100,
                FilterGroups: [
                    {
                        Filters: [
                            {
                                PropertyName: (/** @type {?} */ (settings.labelField)),
                                Value: model.filter,
                                Comparison: model.type,
                                Connector: Connector.And
                            },
                        ]
                    }
                ]
            };
            this.dataService.getListBy(settings.requestOptions.UrlOptions.moduleUrl + settings.requestOptions.UrlOptions.endPointUrl, filter)
                .subscribe((httpResponse) => {
                /** @type {?} */
                const result = httpResponse.serviceResult.Result || [];
                /** @type {?} */
                const idList = result.map(x => x[settings.valueField]).filter(this.onlyUnique);
                /** @type {?} */
                const newFilters = [];
                /** @type {?} */
                let name = '';
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (field.indexOf('Id') > 0 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = field.substr(0, field.indexOf('Id')) +
                            '.' + field.substr(field.indexOf('Id'), field.length);
                    }
                    else {
                        name = field;
                    }
                }
                idList.forEach((id) => {
                    newFilters.push({
                        PropertyName: name,
                        Value: id,
                        Comparison: Comparison.EqualTo,
                        Connector: Connector.Or
                    });
                });
                if (newFilters.length > 0) {
                    newFilters[newFilters.length - 1].Connector = Connector.And;
                }
                /** @type {?} */
                const filterGroup = {
                    Connector: Connector.And,
                    Filters: newFilters
                };
                if (newFilters.length > 0) {
                    this.addToListForFilter(this.filterGroupList, filterGroup, field);
                    this.loadGridViewDataByOptionsUrlAndFilters();
                }
                else {
                    // gridi boşalmak lazım
                    this.data = [];
                    this.pagingResult.TotalCount = 0;
                }
            });
        }
        else {
            this.addToListForFilter(this.filterGroupList, null, field);
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @param {?} value
     * @param {?} index
     * @param {?} self
     * @return {?}
     */
    onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onPageChanged(event) {
        this.pagingResult.CurrentPage = event.page;
        this.pagingResult.PageSize = event.itemsPerPage;
        this.loadGridViewDataByOptionsUrlAndFilters();
    }
    /**
     * @param {?} pageSizeEvent
     * @return {?}
     */
    onChangeItemsPerPage(pageSizeEvent) {
        if (pageSizeEvent.Id) {
            this.pagingResult.PageSize = pageSizeEvent.Id;
        }
        if (this.pagingResult.TotalCount > 0) {
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @return {?}
     */
    getBulkOperationData() {
        /** @type {?} */
        const resultData = [];
        if (this.gridOptions && this.gridOptions.editable) {
            this.validateForm();
            this.deletedData
                .forEach((obj) => {
                resultData.push(obj);
            });
            this.data.forEach(obj => {
                if (obj.OperationType !== OperationType.None) {
                    resultData.push(obj);
                }
            });
        }
        return resultData;
    }
    /**
     * @param {?=} formGroup
     * @return {?}
     */
    validateForm(formGroup = this.gridForm) {
        Object.keys(formGroup.controls).forEach(field => {
            /** @type {?} */
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                this.validateForm(control);
            }
            else if (control instanceof FormArray) {
                Object.keys(control.controls).forEach(c => {
                    /** @type {?} */
                    const ctrl = control.get(c);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
        });
        return formGroup.valid;
    }
    /**
     * @return {?}
     */
    revertData() {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe((res) => {
            if (res && res.value) {
                this.isDataChanged = false;
                this.deletedData = [];
                this.data = this.originalRowData.map(item => Object.assign({}, item));
                this.gridApi.setRowData(this.data);
                this.toastr.success(ToastrMessages.RecordReverted);
                this.recordEventsSubject.next({ type: 'RevertedGrid', record: this.data });
            }
        });
    }
    /**
     * @return {?}
     */
    addRow() {
        if (this.gridOptions.editable) {
            /** @type {?} */
            const addedObject = { Id: -1, OperationType: OperationType.Created };
            this.gridOptions.columns.filter(x => x.field !== 'Id').forEach(element => {
                addedObject[element.field] = element.defaultValue;
            });
            this.data.unshift(addedObject);
            this.isDataChanged = true;
            // this.gridApi.updateRowData({
            //   add: [addedObject],
            //   addIndex: 0
            // });
            this.gridApi.setRowData(this.data);
            this.refreshFormControls();
            this.recordEventsSubject.next({ type: 'Add', record: addedObject });
        }
        else {
            this.ngZone.run(() => {
                if (this.gridOptions.addUrl) {
                    this.router.navigate([this.gridOptions.addUrl]);
                }
                else {
                    this.router.navigate(['new'], { relativeTo: this.route });
                }
            });
        }
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    editRecord(params, rowId, key) {
        if (this.gridOptions.editUrlBlank) {
            if (this.gridOptions.editUrl) {
                window.open(window.location.origin + '/' + this.gridOptions.editUrl + '/' + params.data[this.gridOptions.keyField]);
            }
            else {
                window.open(this.router.url + '/' + 'edit/' + params.data[this.gridOptions.keyField]);
            }
        }
        else {
            this.ngZone.run(() => {
                if (this.gridOptions.editUrl) {
                    this.router.navigate([this.gridOptions.editUrl, params.data[this.gridOptions.keyField]]);
                }
                else {
                    this.router.navigate(['edit', params.data[this.gridOptions.keyField]], { relativeTo: this.route });
                }
                // this.handleBreadcrumbLabel(params.data);
            });
        }
    }
    // handleBreadcrumbLabel(rowData: any): void {
    //   if (this.gridOptions &&
    //     this.gridOptions.breadcrumbLabel &&
    //     rowData &&
    //     this.gridOptions.breadcrumbLabel &&
    //     this.gridOptions.keyField &&
    //     rowData[this.gridOptions.keyField]) {
    //     let editUrl: string;
    //     const keyFieldValue = rowData[this.gridOptions.keyField];
    //     if (this.gridOptions.editUrl) {
    //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
    //     } else {
    //       editUrl = this.router.url + '/edit/' + keyFieldValue;
    //     }
    //     let customLabel = '';
    //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
    //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
    //       if (foundColumn && foundColumn.colDef) {
    //         if (foundColumn.colDef.cellEditorParams &&
    //           foundColumn.colDef.cellEditorParams.dataSource &&
    //           foundColumn.colDef.cellEditorParams.settings) {
    //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
    //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
    //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
    //           customLabel += foundData[labelField] + ' ';
    //         } else {
    //           customLabel += rowData[columnField] + ' ';
    //         }
    //       }
    //     });
    //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
    //   }
    // }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    undoRecord(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe((result) => {
            if (result.value) {
                /** @type {?} */
                const revertedObject = this.data[rowId];
                /** @type {?} */
                const orginalObject = this.originalRowData
                    .find(x => revertedObject && x[this.gridOptions.keyField] === revertedObject[this.gridOptions.keyField]);
                this.data[rowId] = orginalObject ? JSON.parse(JSON.stringify(orginalObject)) : null;
                params.node.data = this.data[rowId];
                this.gridApi.redrawRows({ rowNodes: [params.node] });
                this.gridApi.refreshCells({ rowNodes: [params.node], force: true });
                params.context.formGroup.controls[rowId].markAsPristine();
            }
        });
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    deleteRecord(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe((result) => {
            if (result.value) {
                /** @type {?} */
                const deleteUrl = this.gridOptions.deleteEndPointUrl ? this.gridOptions.deleteEndPointUrl : this.urlOptions.endPointUrl;
                /** @type {?} */
                const reqUrl = `${this.urlOptions.moduleUrl}${deleteUrl}`;
                this.dataService.delete(this.data[rowId][this.gridOptions.keyField], reqUrl)
                    .subscribe((res) => {
                    this.recordEventsSubject.next({ type: 'Delete', record: this.data[rowId] });
                    // const indexOfData = this.data.indexOf(params.data);
                    if (this.pagingResult.TotalCount > 10) {
                        this.refreshAndRevertChanges();
                    }
                    else {
                        this.data.splice(rowId, 1);
                        if (this.pagingResult.TotalCount && this.pagingResult.TotalCount > 0) {
                            this.pagingResult.TotalCount--;
                        }
                        this.gridApi.setRowData(this.data);
                    }
                    this.toastr.success(ToastrMessages.RecordDeleted);
                });
            }
            else {
                this.sweetAlertService.confirmOperationPopup(result);
            }
        });
    }
    /**
     * @return {?}
     */
    callUndoConfirmAlert() {
        /** @type {?} */
        const bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0) {
            return this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo);
        }
        else {
            return of({ value: true });
        }
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    removeRow(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe((res) => {
            if (res && res.value) {
                // this.data[rowId] = Object.assign({}, this.originalRowData[rowId]);
                /** @type {?} */
                const deletedObject = this.data[rowId];
                this.data.splice(rowId, 1);
                this.recordEventsSubject.next({ type: 'Delete', record: deletedObject });
                if (params.data.OperationType !== OperationType.Created) {
                    /** @type {?} */
                    const orginalDeletedObject = this.originalRowData
                        .find(x => x[this.gridOptions.keyField] === deletedObject[this.gridOptions.keyField]);
                    if (orginalDeletedObject) {
                        orginalDeletedObject['OperationType'] = OperationType.Deleted;
                        this.deletedData.push(orginalDeletedObject);
                    }
                    else {
                        deletedObject['OperationType'] = OperationType.Deleted;
                        this.deletedData.push(deletedObject);
                    }
                }
                this.isDataChanged = true;
                this.gridApi.setRowData(this.data);
                // if (!this.gridOptions.editable) {
                //   this.toastr.success(ToastrMessages.RecordDeleted);
                // }
            }
        });
    }
    /**
     * @return {?}
     */
    refreshAndRevertChanges() {
        this.deletedData = [];
        this.isDataChanged = false;
        this.refreshGridData();
    }
    /**
     * @return {?}
     */
    isChanged() {
        return this.isDataChanged;
    }
    /**
     * @return {?}
     */
    getTotalCount() {
        return this.pagingResult.TotalCount - this.deletedData.length;
    }
    /**
     * @return {?}
     */
    getNoRowsOverlayComponent() {
        return 'customNoRowsOverlay';
    }
    /**
     * @return {?}
     */
    getLoadingOverlayComponent() {
        return 'customLoadingOverlay';
    }
    /**
     * @return {?}
     */
    getSelectedRows() {
        return this.gridApi.getSelectedRows();
    }
    /**
     * @return {?}
     */
    loadingOverlay() {
        if (this.gridApi) {
            if (this.loadingDisplayStatus) {
                this.gridApi.showLoadingOverlay();
            }
            else {
                this.gridApi.hideOverlay();
                if (!this.data || (this.data && this.data.length === 0)) {
                    this.gridApi.showNoRowsOverlay();
                }
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.loadingDisplayStatusSubscription) {
            this.loadingDisplayStatusSubscription.unsubscribe();
        }
    }
}
CoreGridService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreGridService.ctorParameters = () => [
    { type: DataService },
    { type: ToastrUtilsService },
    { type: GridLoadingService },
    { type: SweetAlertService },
    { type: NgZone },
    { type: Router },
    { type: ActivatedRoute }
];
/** @nocollapse */ CoreGridService.ngInjectableDef = i0.defineInjectable({ factory: function CoreGridService_Factory() { return new CoreGridService(i0.inject(i1.DataService), i0.inject(i2.ToastrUtilsService), i0.inject(i3.GridLoadingService), i0.inject(i4.SweetAlertService), i0.inject(i0.NgZone), i0.inject(i5.Router), i0.inject(i5.ActivatedRoute)); }, token: CoreGridService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CoreGridService.prototype.gridOptions;
    /** @type {?} */
    CoreGridService.prototype.pagingResult;
    /** @type {?} */
    CoreGridService.prototype.data;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.gridApi;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.columnApi;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.urlOptions;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.originalRowData;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.deletedData;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.filter;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.filterGroupList;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.gridForm;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadOnInit;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadColumnDataSourceOnInit;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.isDataChanged;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.isLoad;
    /** @type {?} */
    CoreGridService.prototype.columnDefs;
    /** @type {?} */
    CoreGridService.prototype.allGridColumns;
    /** @type {?} */
    CoreGridService.prototype.currentSortedColId;
    /** @type {?} */
    CoreGridService.prototype.loadingDisplayStatusSubscription;
    /** @type {?} */
    CoreGridService.prototype.loadingDisplayStatus;
    /** @type {?} */
    CoreGridService.prototype.recordEventsSubject;
    /** @type {?} */
    CoreGridService.prototype.retrievedDataSourceSubject;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.dataService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.toastr;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadingService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.sweetAlertService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.ngZone;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvc2VydmljZXMvY29yZS1ncmlkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWEsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBZSxNQUFNLGdCQUFnQixDQUFDO0FBRTVGLE9BQU8sRUFBZ0IsT0FBTyxFQUFjLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7O0FBSzVCLE9BQU8sRUFDTCxhQUFhLEVBQ2IsZ0JBQWdCLEVBQ2hCLFFBQVEsRUFDUixrQkFBa0IsRUFDbEIsMEJBQTBCLEVBQzFCLGNBQWMsRUFDZCxPQUFPLEVBQ1AsVUFBVSxFQUNWLFNBQVMsRUFDVixNQUFNLHNCQUFzQixDQUFDOztBQXNCOUIsT0FBTyxFQUNMLFdBQVcsRUFDWCxrQkFBa0IsRUFDbEIsaUJBQWlCLEVBQ2xCLE1BQU0seUJBQXlCLENBQUM7O0FBR2pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTlFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdFQUFnRSxDQUFDO0FBQzNHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBQzlHLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBQzlHLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLG9FQUFvRSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBRXJHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQzdGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBRTdGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ25HLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDhEQUE4RCxDQUFDO0FBR3pHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOzs7Ozs7O0FBTzVELE1BQU0sT0FBTyxlQUFlOzs7Ozs7Ozs7O0lBNEIxQixZQUNVLFdBQXdCLEVBQ3hCLE1BQTBCLEVBQzFCLGNBQWtDLEVBQ2xDLGlCQUFvQyxFQUNwQyxNQUFjLEVBQ2QsTUFBYyxFQUVkLEtBQXFCO1FBUHJCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLFdBQU0sR0FBTixNQUFNLENBQW9CO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFvQjtRQUNsQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRWQsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFsQ3hCLGlCQUFZLEdBQWlCLEVBQUUsQ0FBQztRQUNoQyxTQUFJLEdBQVEsRUFBRSxDQUFDO1FBS2Qsb0JBQWUsR0FBUSxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFDdEIsV0FBTSxHQUFRLEVBQUUsQ0FBQztRQUNqQixvQkFBZSxHQUFVLEVBQUUsQ0FBQztRQUM1QixhQUFRLEdBQUcsSUFBSSxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDN0IsZUFBVSxHQUFHLElBQUksQ0FBQztRQUVsQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGVBQVUsR0FBVSxFQUFFLENBQUM7UUFDdkIsbUJBQWMsR0FBVSxFQUFFLENBQUM7UUFNbEMsd0JBQW1CLEdBQWlCLElBQUksT0FBTyxFQUFPLENBQUM7UUFDdkQsK0JBQTBCLEdBQW1CLElBQUksT0FBTyxFQUFTLENBQUM7UUFZaEUsSUFBSSxDQUFDLGdDQUFnQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsMEJBQTBCLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUMxRyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFRCxJQUFJLENBQUMsT0FBb0IsRUFBRSxVQUFtQixFQUFFLDBCQUFvQztRQUNsRixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsMEJBQTBCLEdBQUcsMEJBQTBCLENBQUM7UUFDN0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7UUFDM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUc7Z0JBQ2xCLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFFBQVEsRUFBRSxPQUFPLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxRQUFRO2FBQ3ZELENBQUM7WUFFRixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUV6QixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO2dCQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRU0saUJBQWlCO1FBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUc7WUFDaEIsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTO1lBQy9ELFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVztTQUNwRSxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFTyxhQUFhO1FBQ25CLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLHVCQUF1QixFQUFFOztrQkFDMUQsWUFBWSxHQUFHO2dCQUNuQixVQUFVLEVBQUUsUUFBUTtnQkFDcEIsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsbUNBQW1DLEVBQUUsSUFBSTtnQkFDekMsaUJBQWlCLEVBQUUsSUFBSTtnQkFDdkIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLEVBQUU7Z0JBQ1osU0FBUyxFQUFFLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRTtnQkFDbkMsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLGNBQWMsRUFBRSxLQUFLO2dCQUNyQixXQUFXLEVBQUUsS0FBSztnQkFDbEIsY0FBYyxFQUFFLElBQUk7Z0JBQ3BCLFFBQVEsRUFBRSxFQUFFO2FBQ2I7WUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNwQztRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs7Z0JBQ2xDLFlBQThCOztnQkFDOUIsYUFBYSxHQUFZLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN0RCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO2dCQUMvQixhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUMvQjtZQUVELElBQUksYUFBYSxFQUFFO2dCQUNqQixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsS0FBSyxFQUFFO29CQUNoRCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsYUFBYSxDQUFDO2lCQUMvQztxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsSUFBSSxFQUFFO29CQUN0RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUM7aUJBQ2xEO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUU7b0JBQ3RELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDbEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtvQkFDeEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2lCQUNwRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsT0FBTyxFQUFFO29CQUN6RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUM7aUJBQ3JEO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7b0JBQ3pELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQztpQkFDckQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEdBQUcsRUFBRTtvQkFDckQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDO2lCQUNsRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsUUFBUSxFQUFFO29CQUMxRCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUM7aUJBQ3REO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUU7b0JBQzlELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUM7aUJBQ2pEO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEtBQUssRUFBRTtvQkFDaEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQztpQkFDL0M7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQztpQkFDL0M7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQztpQkFDOUM7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtvQkFDeEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztpQkFDaEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtvQkFDekQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtvQkFDekQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEdBQUcsRUFBRTtvQkFDckQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztpQkFDaEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQztpQkFDOUM7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLFFBQVEsRUFBRTtvQkFDMUQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDO2lCQUNsRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsWUFBWSxFQUFFO29CQUM5RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsZUFBZSxDQUFDO2lCQUNqRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsVUFBVSxFQUFFO29CQUM1RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsY0FBYyxDQUFDO2lCQUNoRDthQUNGO1lBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7O2dCQUVHLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSztZQUMzQixJQUFJLElBQUksQ0FBQyxXQUFXO21CQUNmLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYzttQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVTttQkFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRTs7c0JBQ3JELFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVztnQkFDMUUsSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUNuQyxVQUFVLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNyRDtxQkFBTTtvQkFDTCxVQUFVLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7aUJBQ3ZDO2FBQ0Y7O2tCQUVLLE1BQU0sR0FBRztnQkFDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxJQUFJLFVBQVU7Z0JBQ3pDLFlBQVk7Z0JBQ1osSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU87Z0JBQ25CLGlCQUFpQixFQUFFLElBQUk7Z0JBQ3ZCLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksSUFBSSxLQUFLO2dCQUM5QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLElBQUksS0FBSztnQkFDakQsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsZ0JBQWdCLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxJQUFJO29CQUNoQixRQUFRLEVBQUUsSUFBSTtvQkFDZCxJQUFJLEVBQUUsSUFBSTtvQkFDVixxQkFBcUIsRUFBRSxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSTtvQkFDekQsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixJQUFJLElBQUk7b0JBQzNELGdDQUFnQyxFQUFFLElBQUksQ0FBQyxnQ0FBZ0MsSUFBSSxJQUFJO2lCQUNoRjtnQkFDRCw2QkFBNkIsRUFBRTtvQkFDN0IsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSztvQkFDcEMsYUFBYSxFQUFFLElBQUk7b0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztvQkFDakIsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixJQUFJLElBQUk7aUJBQ3hEO2dCQUNELG9CQUFvQixFQUFFLElBQUksQ0FBQyxvQkFBb0I7Z0JBQy9DLGtCQUFrQixFQUFFLElBQUksQ0FBQyxrQkFBa0I7Z0JBQzNDLDRCQUE0QixFQUFFLElBQUksQ0FBQyw0QkFBNEI7Z0JBQy9ELFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTthQUM1QjtZQUVELElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTtnQkFDNUMsTUFBTSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQzthQUNqQztZQUVELElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtnQkFDcEcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQzthQUMxQjtZQUdELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDcEM7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQ3BDO1lBR0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNuQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDdEQ7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNsRDtZQUVELElBQ0UsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxZQUFZLEtBQUssZ0JBQWdCLENBQUMsY0FBYyxDQUFDO21CQUNoRyxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNsQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxHQUFHLElBQUksT0FBTyxFQUFPLENBQUM7Z0JBRWxELElBQUksQ0FBQyxJQUFJLENBQUMsNEJBQTRCLEVBQUU7b0JBQ3RDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUNoRTthQUNGOztrQkFFSyxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqRSxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELGNBQWM7UUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7Ozs7O0lBRUQsbUJBQW1CLENBQUMsSUFBa0IsRUFBRSxNQUFXLEVBQUUsWUFBdUI7UUFDMUUsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQywwQkFBMEIsRUFBRTs7a0JBQ2hELFFBQVEsR0FBRyxtQkFBQSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFrQzs7Z0JBQy9FLGNBQThCO1lBQ2xDLElBQUksUUFBUSxFQUFFO2dCQUNaLElBQUksTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUU7b0JBQzFHLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxtQkFBQSxRQUFRLEVBQW9CLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2lCQUM1RjtxQkFBTTtvQkFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsbUJBQUEsUUFBUSxFQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztpQkFDL0Y7YUFDRjtZQUVELElBQUksY0FBYyxFQUFFOztzQkFDWixNQUFNLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXOztvQkFFdEYscUJBQXFCLEdBQUcsS0FBSztnQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7b0JBQ3hDLHFCQUFxQixHQUFHLElBQUksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDbEQ7O3NCQUNLLG9CQUFvQixHQUFvQixFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFO2dCQUUvRyxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDM0MsSUFBSSxjQUFjLENBQUMsWUFBWSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFO3dCQUMzRSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7NEJBQ3pELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxHQUFHLENBQUM7b0NBQzFDLE9BQU8sRUFBRSxFQUFFO2lDQUNaLENBQUMsQ0FBQzt5QkFFSjs2QkFBTSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQzlELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQztnQ0FDL0MsT0FBTyxFQUFFLEVBQUU7NkJBQ1osQ0FBQyxDQUFDO3lCQUVKO3FCQUNGO3lCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFO3dCQUN2QyxjQUFjLENBQUMsWUFBWSxHQUFHOzRCQUM1QixZQUFZLEVBQUUsQ0FBQztvQ0FDYixPQUFPLEVBQUUsRUFBRTtpQ0FDWixDQUFDO3lCQUNILENBQUM7cUJBRUg7eUJBQU0sSUFBSSxjQUFjLENBQUMsWUFBWSxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUU7d0JBQ25GLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxHQUFHLENBQUM7Z0NBQzFDLE9BQU8sRUFBRSxFQUFFOzZCQUNaLENBQUMsQ0FBQztxQkFDSjtvQkFFRCxJQUFJLGNBQWMsQ0FBQyxZQUFZO3dCQUM3QixjQUFjLENBQUMsWUFBWSxDQUFDLFlBQVk7d0JBQ3hDLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3JELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUM7cUJBQ3BFO2lCQUNGO2dCQUVELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxjQUFjLENBQUMsWUFBWSxFQUFFLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztxQkFDbkgsSUFBSSxDQUNILFFBQVEsQ0FBQyxHQUFHLEVBQUU7b0JBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7d0JBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ2xEO2dCQUNILENBQUMsQ0FBQyxDQUFDO3FCQUNKLFNBQVMsQ0FBQyxDQUFDLFlBQW1DLEVBQUUsRUFBRTs7MEJBQzNDLE9BQU8sR0FBVSxZQUFZLENBQUMsYUFBYSxDQUFDLE1BQU07OzBCQUNsRCxVQUFVLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDOzswQkFDbkQsUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3RFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO29CQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7b0JBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDNUIsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELGFBQWE7O2NBQ0wsVUFBVSxHQUFHLEVBQUU7UUFDckIsWUFBWTtRQUNaLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLHdCQUF3QixDQUFDO1FBQzVFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBQ3RFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsR0FBRyxvQkFBb0IsQ0FBQztRQUNwRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsR0FBRyx3QkFBd0IsQ0FBQztRQUM1RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsR0FBRyx1QkFBdUIsQ0FBQztRQUMxRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsR0FBRyx5QkFBeUIsQ0FBQztRQUM5RSxpQkFBaUI7UUFDakIsVUFBVSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxHQUFHLG9CQUFvQixDQUFDO1FBQ3BFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsR0FBRyxrQkFBa0IsQ0FBQztRQUNoRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEdBQUcsaUJBQWlCLENBQUM7UUFDOUQsVUFBVSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxHQUFHLG1CQUFtQixDQUFDO1FBQ2xFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBQ3RFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsR0FBRyxpQkFBaUIsQ0FBQztRQUM5RCxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLEdBQUcsb0JBQW9CLENBQUM7UUFDcEUsY0FBYztRQUNkLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLHlCQUF5QixDQUFDO1FBQzlFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO1FBQ2hGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO1FBQ2hGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLDJCQUEyQixDQUFDO1FBQ2xGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLHNCQUFzQixDQUFDO1FBQ3hFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLGVBQWU7UUFDZixVQUFVLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLEdBQUcsb0JBQW9CLENBQUM7UUFFcEUsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEdBQUcscUJBQXFCLENBQUM7UUFDckQsVUFBVSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsc0JBQXNCLENBQUM7UUFDM0QsVUFBVSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsdUJBQXVCLENBQUM7UUFFN0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQzs7OztJQUVELG1CQUFtQjtRQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUU3QixJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzlELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQzthQUN2RTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2xFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQzthQUNwRTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsWUFBWSxFQUFFO2dCQUM5RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7YUFDcEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGVBQWUsRUFBRTtnQkFDakUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDO2FBQ25FO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDckUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDO2FBQ3ZFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDcEUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDO2FBQ3RFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDbEUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2FBQ3BFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUU7Z0JBQ2pFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQzthQUN2RTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsYUFBYSxFQUFFO2dCQUMvRCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7YUFDcEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO2dCQUNsRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7YUFDcEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLFlBQVksRUFBRTtnQkFDOUQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2FBQ3BFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDdEUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDO2FBQ3hFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDbEUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDO2FBQ3hFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUU7Z0JBQ2pFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQzthQUN2RTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsY0FBYzttQkFDM0QsSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQzthQUNwRTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsY0FBYyxFQUFFO2dCQUNoRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUM7YUFDdEU7UUFFSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLDhCQUE4QixDQUFDLGlCQUFvQzs7Y0FDbkUsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7O1lBRXZFLFlBQVksR0FBc0IsRUFBRTtRQUN4QyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7WUFDM0csWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDO1NBQ2pFO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7a0JBRTNDLG9CQUFvQixHQUFvQixFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFO1lBQy9HLFlBQVksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztTQUN6QztRQUVELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLGlCQUFpQixFQUFFLFlBQVksQ0FBQzthQUN2RSxJQUFJLENBQ0gsUUFBUSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFO2dCQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2xEO1FBQ0gsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRU8sc0NBQXNDO1FBQzVDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs7a0JBQ2QsWUFBWSxHQUFzQixDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUNqRyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3JCLFlBQVksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7Z0JBQ3hELFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7YUFDcEQ7WUFFRCxZQUFZLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDO1lBQzVELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFOztvQkFDeEIsSUFBWTs7c0JBQ1YsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQztnQkFDL0UsSUFBSSxXQUFXLEVBQUU7b0JBQ2YsSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDekM7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDVCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQy9CLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLEVBQUU7d0JBQzVILElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3JELEdBQUcsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUMzRTt5QkFBTTt3QkFDTCxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQztxQkFDcEI7aUJBQ0Y7O3NCQUVLLEtBQUssR0FBZ0I7b0JBQ3pCLE9BQU8sRUFBRSxDQUFDOzRCQUNSLFlBQVksRUFBRSxJQUFJOzRCQUNsQixLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7NEJBQzlCLFVBQVUsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs0QkFDakMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSTt5QkFDM0MsQ0FBQztpQkFDSDtnQkFFRCxZQUFZLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUVILDBDQUEwQztZQUMxQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGVBQW9CLEVBQUUsRUFBRTtnQkFDcEQsWUFBWSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNELENBQUMsQ0FBQyxDQUFDO1lBRUgsOENBQThDO1lBQzlDLGlHQUFpRztZQUNqRyxJQUFJO1lBRUosSUFBSSxDQUFDLDhCQUE4QixDQUFDLFlBQVksQ0FBQztpQkFDOUMsU0FBUyxDQUFDLENBQUMsWUFBbUMsRUFBRSxFQUFFO2dCQUNqRCxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7Z0JBQzlDLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDdkU7Z0JBQ0QsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRWhELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsRUFBRTtvQkFDdkQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2lCQUN2QjtnQkFFRCxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztZQUV4QyxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQzs7OztJQUVELDhCQUE4Qjs7Y0FDdEIsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsNEJBQTRCLEtBQUssSUFBSSxDQUFDO1FBQ3hHLDJCQUEyQixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTs7a0JBQ3JDLFlBQVksR0FBVSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7O2tCQUN6RCxVQUFVLEdBQWEsRUFBRTtZQUUvQixZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsV0FBZ0IsRUFBRSxFQUFFOztzQkFDbEMsU0FBUyxHQUFXO29CQUN4QixZQUFZLEVBQUUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUN6RCxLQUFLLEVBQUUsV0FBVztvQkFDbEIsVUFBVSxFQUFFLFVBQVUsQ0FBQyxPQUFPO29CQUM5QixTQUFTLEVBQUUsU0FBUyxDQUFDLEVBQUU7aUJBQ3hCOztvQkFFRyxJQUFJLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxVQUFVO2dCQUV0RCxtQkFBbUI7Z0JBQ25CLElBQUksSUFBSSxFQUFFO29CQUNSLElBQUksTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbEUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsRUFBRTt3QkFDNUgsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3ZDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN0RDtpQkFDRjtnQkFFRCxTQUFTLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFFOUIsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztZQUVILElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3pCLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDO2FBQzdEO1lBQ0QsSUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQzVFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCx1QkFBdUI7SUFDekIsQ0FBQzs7Ozs7SUFFTyxnQkFBZ0I7O1lBQ2xCLG1CQUFtQixHQUFHLElBQUk7O1lBQzFCLGlCQUFpQixHQUFHLElBQUk7O1lBQ3hCLFlBQVksR0FBRyxDQUFDO1FBRXBCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUN2RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLG1CQUFtQixLQUFLLFNBQVMsRUFBRTtnQkFDckUsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUM7Z0JBQzFFLElBQUksbUJBQW1CLEVBQUU7b0JBQ3ZCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1lBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO2dCQUNoRSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7Z0JBQ25FLElBQUksaUJBQWlCLEVBQUU7b0JBQ3JCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1NBQ0Y7UUFFRCw4Q0FBOEM7UUFDOUMseURBQXlEO1FBQ3pELHNCQUFzQjtRQUN0QixJQUFJO1FBRUosSUFBSSxtQkFBbUIsSUFBSSxpQkFBaUIsRUFBRTs7a0JBQ3RDLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdEUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFOztzQkFDUCxNQUFNLEdBQUc7b0JBQ2IsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsS0FBSyxFQUFFLGVBQWU7b0JBQ3RCLFFBQVEsRUFBRSxLQUFLO29CQUNmLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlO29CQUM5QyxNQUFNLEVBQUUsS0FBSztvQkFDYixRQUFRLEVBQUUsS0FBSztvQkFDZixRQUFRLEVBQUUsWUFBWSxHQUFHLEVBQUU7b0JBQzNCLFFBQVEsRUFBRSxZQUFZLEdBQUcsRUFBRTtpQkFDNUI7Z0JBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUI7WUFDRCwwQkFBMEI7U0FDM0I7SUFDSCxDQUFDOzs7OztJQUVPLGtCQUFrQjs7Y0FDbEIsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFOztjQUV4QyxhQUFhLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLENBQUMsUUFBUSxFQUFhLENBQUM7OztjQUU1QyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQ3hELFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUNuQyxhQUFhLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFnQixFQUFFLEVBQUU7O2tCQUV0QyxTQUFTLEdBQWMsSUFBSSxTQUFTLENBQUMsRUFBRSxDQUFDO1lBRTlDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFjLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDO2lCQUM3RSxPQUFPLENBQUMsQ0FBQyxNQUFjLEVBQUUsRUFBRTs7c0JBQ3BCLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDOzs7c0JBQzVDLEdBQUcsR0FBZSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7c0JBQ3ZGLFVBQVUsR0FBa0IsRUFBRTtnQkFDcEMsSUFBSSxHQUFHLEVBQUU7b0JBQ1AsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO3dCQUNsQixJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFOzRCQUMzQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFDdEM7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRTs0QkFDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ25DO3dCQUNELElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUU7NEJBQy9CLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO3lCQUMxQzt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFOzRCQUMzQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3lCQUMxRDt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFOzRCQUMzQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3lCQUMxRDt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFOzRCQUM1QixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO3lCQUNqRTt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFOzRCQUM1QixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO3lCQUNqRTt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFOzRCQUN4QixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3lCQUMzRDtxQkFDRjtpQkFDRjtnQkFDRCxTQUFTLENBQUMsVUFBVSxDQUFDLG1CQUFBLEdBQUcsRUFBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDcEYsQ0FBQyxDQUFDLENBQUM7WUFDTCxhQUFhLENBQUMsVUFBVSxDQUFDLG1CQUFBLE9BQU8sQ0FBQyxFQUFFLEVBQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU8sb0JBQW9CO1FBQzFCLGdDQUFnQztRQUNoQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUNuQztRQUNELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxVQUFrQixFQUFFLFVBQWU7O2NBQy9DLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDeEQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7U0FDbkU7SUFDSCxDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLE1BQWtCO1FBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7O2tCQUNsSCxRQUFRLEdBQUcsbUJBQUEsTUFBTSxDQUFDLFFBQVEsRUFBa0M7O2dCQUM5RCxjQUE4QjtZQUNsQyxJQUFJLE1BQU0sQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsTUFBTSxFQUFFO2dCQUNuRCxjQUFjLEdBQUcsQ0FBQyxtQkFBQSxRQUFRLEVBQW9CLENBQUMsQ0FBQyxjQUFjLENBQUM7YUFDaEU7aUJBQU07Z0JBQ0wsY0FBYyxHQUFHLENBQUMsbUJBQUEsUUFBUSxFQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDO2FBQ25FOztrQkFDSyxNQUFNLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsR0FBRztrQkFDcEQsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXOztnQkFFckMscUJBQXFCLEdBQUcsS0FBSztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2xEOztrQkFDSyxvQkFBb0IsR0FBb0IsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRTtZQUMvRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsY0FBYyxDQUFDLFlBQVksRUFBRSxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ25ILElBQUksQ0FDSCxRQUFRLENBQUMsR0FBRyxFQUFFO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFO29CQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNsRDtZQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKLFNBQVMsQ0FBQyxDQUFDLFlBQW1DLEVBQUUsRUFBRTs7c0JBQzNDLE9BQU8sR0FBVSxZQUFZLENBQUMsYUFBYSxDQUFDLE1BQU07O3NCQUNsRCxVQUFVLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDOztzQkFDbkQsUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RFLE1BQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUMvQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztnQkFDL0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDckU7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsTUFBTTtRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBRWxDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUNuQztRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7U0FDL0U7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCx5QkFBeUIsQ0FBQyxNQUFNO1FBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN6RCxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDMUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQ2pDO1lBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUM1QztJQUNILENBQUM7Ozs7Ozs7SUFFTyxTQUFTLENBQUMsU0FBb0IsRUFBRSxNQUFjO1FBQ3BELE9BQU8sU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxJQUFTO1FBQ2xCLE9BQU87WUFDTCxlQUFlLEVBQUUsSUFBSTtZQUNyQixTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDeEIsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1NBQzFCLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsbUJBQW1CO1FBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsZUFBeUI7O2NBQ2pDLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7UUFDNUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksZUFBZSxFQUFFO1lBQ3RELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUM7aUJBQ3pGLFNBQVMsQ0FBQyxDQUFDLEdBQWdCLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRTtvQkFDcEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7b0JBQ3ZFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUNuRDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjtJQUNILENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7UUFDOUMsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDbkM7SUFDSCxDQUFDOzs7OztJQUVELGtCQUFrQixDQUFDLElBQVM7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsT0FBTyxFQUFFO1lBQ3JELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7U0FDakQ7SUFDSCxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxTQUFjO1FBQzFCLElBQUksU0FBUyxFQUFFO1lBQ2IseURBQXlEO1lBRXpELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDOztrQkFFcEMsU0FBUyxHQUFHLG1CQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBYzs7Z0JBQ3RFLElBQUksR0FBVyxTQUFTLENBQUMsa0JBQWtCO1lBRS9DLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1QsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO29CQUNuQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLFNBQVMsQ0FBQyxFQUFFO29CQUM1SCxJQUFJLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2pFO3FCQUFNO29CQUNMLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO2lCQUN4QjthQUNGOztrQkFFSyxJQUFJLEdBQUcsQ0FBQztvQkFDWixZQUFZLEVBQUUsSUFBSTtvQkFDbEIsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSTtpQkFDOUQsQ0FBQztZQUVGLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRXpELElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDO1NBQy9DO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsS0FBVTtRQUN4Qix1R0FBdUc7UUFDdkcsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7SUFDaEQsQ0FBQzs7Ozs7O0lBRUQsb0JBQW9CLENBQUMsS0FBYSxFQUFFLEtBQVU7UUFDNUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQ2pDLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUNqRCxLQUFLLENBQUMsQ0FBQztJQUNYLENBQUM7Ozs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxTQUFnQixFQUFFLEtBQVUsRUFBRSxHQUFROztjQUNqRCxjQUFjLEdBQVcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDO1FBQ3RFLElBQUksS0FBSyxFQUFFOztrQkFDSCxVQUFVLEdBQVE7Z0JBQ3RCLEdBQUc7Z0JBQ0gsS0FBSzthQUNOO1lBQ0QsSUFBSSxjQUFjLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3pCLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxVQUFVLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0wsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM1QjtTQUNGO2FBQU07WUFDTCxJQUFJLGNBQWMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDekIsU0FBUyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDckM7U0FDRjtJQUNILENBQUM7Ozs7Ozs7O0lBSUQsNEJBQTRCLENBQUMsS0FBYSxFQUFFLFFBQTBCLEVBQUUsS0FBVTs7Y0FDMUUsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDO1FBRXpFLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTs7a0JBQ1YsTUFBTSxHQUFzQjtnQkFDaEMsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsWUFBWSxFQUFFO29CQUNaO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxZQUFZLEVBQUUsbUJBQUEsUUFBUSxDQUFDLFVBQVUsRUFBVTtnQ0FDM0MsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNO2dDQUNuQixVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUk7Z0NBQ3RCLFNBQVMsRUFBRSxTQUFTLENBQUMsR0FBRzs2QkFDekI7eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUVELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQ3RILE1BQU0sQ0FBQztpQkFDTixTQUFTLENBQUMsQ0FBQyxZQUFxQyxFQUFFLEVBQUU7O3NCQUM3QyxNQUFNLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksRUFBRTs7c0JBQ2hELE1BQU0sR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDOztzQkFDeEUsVUFBVSxHQUFhLEVBQUU7O29CQUUzQixJQUFJLEdBQUcsRUFBRTtnQkFDYixJQUFJLFdBQVcsRUFBRTtvQkFDZixJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDO2lCQUN6QztnQkFDRCxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNULElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUN6QixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLFNBQVMsQ0FBQyxFQUFFO3dCQUM1SCxJQUFJLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDekMsR0FBRyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ3pEO3lCQUFNO3dCQUNMLElBQUksR0FBRyxLQUFLLENBQUM7cUJBQ2Q7aUJBQ0Y7Z0JBRUQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQVUsRUFBRSxFQUFFO29CQUM1QixVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixLQUFLLEVBQUUsRUFBRTt3QkFDVCxVQUFVLEVBQUUsVUFBVSxDQUFDLE9BQU87d0JBQzlCLFNBQVMsRUFBRSxTQUFTLENBQUMsRUFBRTtxQkFDeEIsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3pCLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUM3RDs7c0JBR0ssV0FBVyxHQUFnQjtvQkFDL0IsU0FBUyxFQUFFLFNBQVMsQ0FBQyxHQUFHO29CQUN4QixPQUFPLEVBQUUsVUFBVTtpQkFDcEI7Z0JBRUQsSUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDekIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNsRSxJQUFJLENBQUMsc0NBQXNDLEVBQUUsQ0FBQztpQkFDL0M7cUJBQU07b0JBQ0wsdUJBQXVCO29CQUN2QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7aUJBQ2xDO1lBRUgsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDO1NBQy9DO0lBQ0gsQ0FBQzs7Ozs7OztJQUVELFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUk7UUFDM0IsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFVO1FBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQztRQUNoRCxJQUFJLENBQUMsc0NBQXNDLEVBQUUsQ0FBQztJQUNoRCxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLGFBQWtCO1FBQ3JDLElBQUksYUFBYSxDQUFDLEVBQUUsRUFBRTtZQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUMsRUFBRSxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxDQUFDLEVBQUU7WUFDcEMsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7U0FDL0M7SUFDSCxDQUFDOzs7O0lBRUQsb0JBQW9COztjQUNaLFVBQVUsR0FBRyxFQUFFO1FBQ3JCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTtZQUNqRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFFcEIsSUFBSSxDQUFDLFdBQVc7aUJBQ2IsT0FBTyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ2YsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztZQUVMLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLEdBQUcsQ0FBQyxhQUFhLEtBQUssYUFBYSxDQUFDLElBQUksRUFBRTtvQkFDNUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdEI7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsWUFBdUIsSUFBSSxDQUFDLFFBQVE7UUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFOztrQkFDeEMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3BDLElBQUksT0FBTyxZQUFZLFdBQVcsRUFBRTtnQkFDbEMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2FBQzNDO2lCQUFNLElBQUksT0FBTyxZQUFZLFNBQVMsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM1QjtpQkFBTSxJQUFJLE9BQU8sWUFBWSxTQUFTLEVBQUU7Z0JBQ3ZDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTs7MEJBQ2xDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLFNBQVMsQ0FBQyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDO2FBQ3RGLFNBQVMsQ0FBQyxDQUFDLEdBQWdCLEVBQUUsRUFBRTtZQUM5QixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsS0FBSyxFQUFFO2dCQUNwQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2FBQzVFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUU7O2tCQUN2QixXQUFXLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsYUFBYSxFQUFFLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDcEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3ZFLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUNwRCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRS9CLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBRTFCLCtCQUErQjtZQUMvQix3QkFBd0I7WUFDeEIsZ0JBQWdCO1lBQ2hCLE1BQU07WUFFTixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbkMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFFM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7U0FDckU7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRTtnQkFDbkIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtvQkFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7aUJBQ2pEO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7aUJBQzNEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBQzNCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUU7WUFDakMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtnQkFDNUIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2FBQ3JIO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUN2RjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUU7Z0JBQ25CLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUY7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7aUJBQ3BHO2dCQUVELDJDQUEyQztZQUM3QyxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQThDRCxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUM7YUFDdEYsU0FBUyxDQUFDLENBQUMsTUFBVyxFQUFFLEVBQUU7WUFDekIsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFOztzQkFDVixjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7O3NCQUNqQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWU7cUJBQ3ZDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLGNBQWMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsS0FBSyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFMUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3BGLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ3BFLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUMzRDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7OztJQUdELFlBQVksQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUc7UUFDN0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlDQUFpQyxDQUFDLDBCQUEwQixDQUFDLE1BQU0sQ0FBQzthQUN4RixTQUFTLENBQUMsQ0FBQyxNQUFXLEVBQUUsRUFBRTtZQUN6QixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7O3NCQUNWLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVc7O3NCQUNqSCxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxNQUFNLENBQUM7cUJBQ3pFLFNBQVMsQ0FBQyxDQUFDLEdBQVEsRUFBRSxFQUFFO29CQUN0QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBRTVFLHNEQUFzRDtvQkFFdEQsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxFQUFFLEVBQUU7d0JBQ3JDLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO3FCQUNoQzt5QkFBTTt3QkFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxFQUFFOzRCQUNwRSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDO3lCQUNoQzt3QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3BDO29CQUVELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFFcEQsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDdEQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxvQkFBb0I7O2NBQ1osUUFBUSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtRQUM1QyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsRzthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7Ozs7SUFFRCxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLENBQUM7YUFDeEYsU0FBUyxDQUFDLENBQUMsR0FBZ0IsRUFBRSxFQUFFO1lBQzlCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7OztzQkFFZCxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7Z0JBRXpFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssYUFBYSxDQUFDLE9BQU8sRUFBRTs7MEJBQ2pELG9CQUFvQixHQUFHLElBQUksQ0FBQyxlQUFlO3lCQUM5QyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsS0FBSyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFFdkYsSUFBSSxvQkFBb0IsRUFBRTt3QkFDeEIsb0JBQW9CLENBQUMsZUFBZSxDQUFDLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQzt3QkFDOUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztxQkFDN0M7eUJBQU07d0JBQ0wsYUFBYSxDQUFDLGVBQWUsQ0FBQyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7d0JBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUN0QztpQkFDRjtnQkFFRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztnQkFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVuQyxvQ0FBb0M7Z0JBQ3BDLHVEQUF1RDtnQkFDdkQsSUFBSTthQUNMO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsdUJBQXVCO1FBQ3JCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsU0FBUztRQUNQLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7SUFDaEUsQ0FBQzs7OztJQUVELHlCQUF5QjtRQUN2QixPQUFPLHFCQUFxQixDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCwwQkFBMEI7UUFDeEIsT0FBTyxzQkFBc0IsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsY0FBYztRQUNaLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsRUFBRTtvQkFDdkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2lCQUNsQzthQUNGO1NBQ0Y7SUFFSCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLGdDQUFnQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyRDtJQUNILENBQUM7OztZQXRyQ0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBM0NDLFdBQVc7WUFDWCxrQkFBa0I7WUFxQ1gsa0JBQWtCO1lBcEN6QixpQkFBaUI7WUEvQ2EsTUFBTTtZQUM3QixNQUFNO1lBQUUsY0FBYzs7Ozs7SUEwRjdCLHNDQUFnQzs7SUFDaEMsdUNBQXVDOztJQUN2QywrQkFBc0I7Ozs7O0lBRXRCLGtDQUF5Qjs7Ozs7SUFDekIsb0NBQTZCOzs7OztJQUM3QixxQ0FBK0I7Ozs7O0lBQy9CLDBDQUFrQzs7Ozs7SUFDbEMsc0NBQThCOzs7OztJQUM5QixpQ0FBeUI7Ozs7O0lBQ3pCLDBDQUFvQzs7Ozs7SUFDcEMsbUNBQXFDOzs7OztJQUNyQyxxQ0FBMEI7Ozs7O0lBQzFCLHFEQUE0Qzs7Ozs7SUFDNUMsd0NBQThCOzs7OztJQUM5QixpQ0FBdUI7O0lBQ3ZCLHFDQUE4Qjs7SUFDOUIseUNBQWtDOztJQUNsQyw2Q0FBa0M7O0lBRWxDLDJEQUErQzs7SUFDL0MsK0NBQThCOztJQUU5Qiw4Q0FBdUQ7O0lBQ3ZELHFEQUFrRTs7Ozs7SUFJaEUsc0NBQWdDOzs7OztJQUNoQyxpQ0FBa0M7Ozs7O0lBQ2xDLHlDQUEwQzs7Ozs7SUFDMUMsNENBQTRDOzs7OztJQUM1QyxpQ0FBc0I7Ozs7O0lBQ3RCLGlDQUFzQjs7Ozs7SUFFdEIsZ0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgT25EZXN0cm95LCBOZ1pvbmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUFycmF5LCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycywgVmFsaWRhdG9yRm4gfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24sIFN1YmplY3QsIE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGZpbmFsaXplIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgR3JpZEFwaSwgQ29sdW1uQXBpLCBDb2x1bW4sIFJvd05vZGUgfSBmcm9tICdhZy1ncmlkLWNvbW11bml0eSc7XHJcblxyXG4vLyBFbnVtc1xyXG5pbXBvcnQge1xyXG4gIE9wZXJhdGlvblR5cGUsXHJcbiAgQ29yZUdyaWRDZWxsVHlwZSxcclxuICBTb3J0VHlwZSxcclxuICBDb3JlR3JpZENvbHVtblR5cGUsXHJcbiAgQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUsXHJcbiAgVG9hc3RyTWVzc2FnZXMsXHJcbiAgSGVhZGVycyxcclxuICBDb21wYXJpc29uLFxyXG4gIENvbm5lY3RvclxyXG59IGZyb20gJy4uLy4uLy4uL2VudW1zL2luZGV4JztcclxuXHJcblxyXG4vLyBNb2RlbHMgXHJcbmltcG9ydCB7XHJcbiAgR3JpZE9wdGlvbnMsXHJcbiAgUGFnaW5nUmVzdWx0LFxyXG4gIFVybE9wdGlvbnMsXHJcbiAgR2VuZXJpY0V4cHJlc3Npb24sXHJcbiAgU29ydCxcclxuICBHcmlkQ29sdW1uLFxyXG4gIFNlbGVjdG9yU2V0dGluZ3MsXHJcbiAgTG92U2V0dGluZ3MsXHJcbiAgUmVxdWVzdE9wdGlvbnMsXHJcbiAgRmlsdGVyR3JvdXAsXHJcbiAgQWxlcnRSZXN1bHQsXHJcbiAgQ29yZUh0dHBSZXNwb25zZSxcclxuICBIZWFkZXJQYXJhbWV0ZXIsXHJcbiAgRmlsdGVyXHJcbn0gZnJvbSAnLi4vLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbi8vIFNlcnZpY2VzXHJcbmltcG9ydCB7XHJcbiAgRGF0YVNlcnZpY2UsXHJcbiAgVG9hc3RyVXRpbHNTZXJ2aWNlLFxyXG4gIFN3ZWV0QWxlcnRTZXJ2aWNlXHJcbn0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvaW5kZXgnO1xyXG5cclxuLy8gY29tcG9uZW50c1xyXG5pbXBvcnQgeyBDb21tYW5kQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvY29tbWFuZC9jb21tYW5kLWNlbGwuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IFRleHRGaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvdGV4dC1maWx0ZXItY2VsbC90ZXh0LWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlbGVjdEZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9zZWxlY3QtZmlsdGVyLWNlbGwvc2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVGaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvZGF0ZS1maWx0ZXItY2VsbC9kYXRlLWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE51bWVyaWNGaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvbnVtZXJpYy1maWx0ZXItY2VsbC9udW1lcmljLWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEJvb2xlYW5GaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvYm9vbGVhbi1maWx0ZXItY2VsbC9ib29sZWFuLWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEN1cnJlbmN5RmlsdGVyQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZmlsdGVyL2N1cnJlbmN5LWZpbHRlci1jZWxsL2N1cnJlbmN5LWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvdkZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9sb3YtZmlsdGVyLWNlbGwvbG92LWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hc2tGaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvbWFzay1maWx0ZXItY2VsbC9tYXNrLWZpbHRlci1jZWxsLmNvbXBvbmVudCc7XHJcblxyXG5pbXBvcnQgeyBTZWxlY3RDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvc2VsZWN0LWNlbGwvc2VsZWN0LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTGFiZWxDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvbGFiZWwtY2VsbC9sYWJlbC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvZGF0ZS1jZWxsL2RhdGUtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBCb29sZWFuQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvcmVhZC1vbmx5L2Jvb2xlYW4tY2VsbC9ib29sZWFuLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ3VycmVuY3lDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvY3VycmVuY3ktY2VsbC9jdXJyZW5jeS1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hc2tDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvbWFzay1jZWxsL21hc2stY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOdW1lcmljQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvcmVhZC1vbmx5L251bWVyaWMtY2VsbC9udW1lcmljLWNlbGwuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IFRleHRFZGl0Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZWRpdC90ZXh0LWVkaXQtY2VsbC90ZXh0LWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWxlY3RFZGl0Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZWRpdC9zZWxlY3QtZWRpdC1jZWxsL3NlbGVjdC1lZGl0LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTnVtZXJpY0VkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L251bWVyaWMtZWRpdC1jZWxsL251bWVyaWMtZWRpdC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvdkVkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L2xvdi1lZGl0LWNlbGwvbG92LWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEYXRlRWRpdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2VkaXQvZGF0ZS1lZGl0LWNlbGwvZGF0ZS1lZGl0LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQm9vbGVhbkVkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L2Jvb2xlYW4tZWRpdC1jZWxsL2Jvb2xlYW4tZWRpdC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hc2tFZGl0Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZWRpdC9tYXNrLWVkaXQtY2VsbC9tYXNrLWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDdXJyZW5jeUVkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L2N1cnJlbmN5LWVkaXQtY2VsbC9jdXJyZW5jeS1lZGl0LWNlbGwuY29tcG9uZW50JztcclxuXHJcblxyXG5pbXBvcnQgeyBDb2x1bW5IZWFkZXJDb21wb25lbnQgfSBmcm9tICcuLi9jb2x1bW4taGVhZGVyL2NvbHVtbi1oZWFkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTm9Sb3dzT3ZlcmxheUNvbXBvbmVudCB9IGZyb20gJy4uL292ZXJsYXkvbm8tcm93cy9uby1yb3dzLW92ZXJsYXkuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG9hZGluZ092ZXJsYXlDb21wb25lbnQgfSBmcm9tICcuLi9vdmVybGF5L2xvYWRpbmcvbG9hZGluZy1vdmVybGF5LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEdyaWRMb2FkaW5nU2VydmljZSB9IGZyb20gJy4vZ3JpZC1sb2FkaW5nLnNlcnZpY2UnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb3JlR3JpZFNlcnZpY2UgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG4gIHB1YmxpYyBncmlkT3B0aW9uczogR3JpZE9wdGlvbnM7XHJcbiAgcHVibGljIHBhZ2luZ1Jlc3VsdDogUGFnaW5nUmVzdWx0ID0ge307XHJcbiAgcHVibGljIGRhdGE6IGFueSA9IFtdO1xyXG5cclxuICBwcml2YXRlIGdyaWRBcGk6IEdyaWRBcGk7XHJcbiAgcHJpdmF0ZSBjb2x1bW5BcGk6IENvbHVtbkFwaTtcclxuICBwcml2YXRlIHVybE9wdGlvbnM6IFVybE9wdGlvbnM7XHJcbiAgcHJpdmF0ZSBvcmlnaW5hbFJvd0RhdGE6IGFueSA9IFtdO1xyXG4gIHByaXZhdGUgZGVsZXRlZERhdGE6IGFueSA9IFtdO1xyXG4gIHByaXZhdGUgZmlsdGVyOiBhbnkgPSBbXTtcclxuICBwcml2YXRlIGZpbHRlckdyb3VwTGlzdDogYW55W10gPSBbXTtcclxuICBwcml2YXRlIGdyaWRGb3JtID0gbmV3IEZvcm1Hcm91cCh7fSk7XHJcbiAgcHJpdmF0ZSBsb2FkT25Jbml0ID0gdHJ1ZTtcclxuICBwcml2YXRlIGxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0OiBib29sZWFuO1xyXG4gIHByaXZhdGUgaXNEYXRhQ2hhbmdlZCA9IGZhbHNlO1xyXG4gIHByaXZhdGUgaXNMb2FkID0gZmFsc2U7XHJcbiAgcHVibGljIGNvbHVtbkRlZnM6IGFueVtdID0gW107XHJcbiAgcHVibGljIGFsbEdyaWRDb2x1bW5zOiBhbnlbXSA9IFtdO1xyXG4gIHB1YmxpYyBjdXJyZW50U29ydGVkQ29sSWQ6IHN0cmluZztcclxuXHJcbiAgbG9hZGluZ0Rpc3BsYXlTdGF0dXNTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBsb2FkaW5nRGlzcGxheVN0YXR1czogYm9vbGVhbjtcclxuXHJcbiAgcmVjb3JkRXZlbnRzU3ViamVjdDogU3ViamVjdDxhbnk+ID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHJldHJpZXZlZERhdGFTb3VyY2VTdWJqZWN0OiBTdWJqZWN0PGFueVtdPiA9IG5ldyBTdWJqZWN0PGFueVtdPigpO1xyXG5cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGRhdGFTZXJ2aWNlOiBEYXRhU2VydmljZSxcclxuICAgIHByaXZhdGUgdG9hc3RyOiBUb2FzdHJVdGlsc1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIGxvYWRpbmdTZXJ2aWNlOiBHcmlkTG9hZGluZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN3ZWV0QWxlcnRTZXJ2aWNlOiBTd2VldEFsZXJ0U2VydmljZSxcclxuICAgIHByaXZhdGUgbmdab25lOiBOZ1pvbmUsXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgLy8gcHJpdmF0ZSBicmVhZGNydW1iU2VydmljZTogQnJlYWRjcnVtYlNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gICAgdGhpcy5sb2FkaW5nRGlzcGxheVN0YXR1c1N1YnNjcmlwdGlvbiA9IHRoaXMubG9hZGluZ1NlcnZpY2UuZ2V0TG9hZGluZ0JhckRpc3BsYXlTdGF0dXMoKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgdGhpcy5sb2FkaW5nRGlzcGxheVN0YXR1cyA9IGRhdGE7XHJcbiAgICAgIHRoaXMubG9hZGluZ092ZXJsYXkoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaW5pdChvcHRpb25zOiBHcmlkT3B0aW9ucywgbG9hZE9uSW5pdDogYm9vbGVhbiwgbG9hZENvbHVtbkRhdGFTb3VyY2VPbkluaXQ/OiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmxvYWRPbkluaXQgPSBsb2FkT25Jbml0O1xyXG4gICAgdGhpcy5sb2FkQ29sdW1uRGF0YVNvdXJjZU9uSW5pdCA9IGxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0O1xyXG4gICAgdGhpcy5ncmlkT3B0aW9ucyA9IG9wdGlvbnM7XHJcbiAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgIHRoaXMucGFnaW5nUmVzdWx0ID0ge1xyXG4gICAgICAgIEN1cnJlbnRQYWdlOiAxLFxyXG4gICAgICAgIFBhZ2VTaXplOiBvcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5QYWdlU2l6ZVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5yZWxvYWRDb2x1bW5EZWZzKCk7XHJcbiAgICAgIHRoaXMuc2V0TG9hZFVybFNldHRpbmcoKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmxvYWRPbkluaXQgPT09IHRydWUpIHtcclxuICAgICAgICB0aGlzLmlzTG9hZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoR3JpZERhdGEoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIHNldExvYWRVcmxTZXR0aW5nKCkge1xyXG4gICAgdGhpcy51cmxPcHRpb25zID0ge1xyXG4gICAgICBtb2R1bGVVcmw6IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5tb2R1bGVVcmwsXHJcbiAgICAgIGVuZFBvaW50VXJsOiB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmxcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZUNvbHVtbnMoKSB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmVuYWJsZUNoZWNrYm94U2VsZWN0aW9uKSB7XHJcbiAgICAgIGNvbnN0IHNlbGVjdENvbHVtbiA9IHtcclxuICAgICAgICBoZWFkZXJOYW1lOiAnU2VsZWN0JyxcclxuICAgICAgICBoZWFkZXJDaGVja2JveFNlbGVjdGlvbjogdHJ1ZSxcclxuICAgICAgICBoZWFkZXJDaGVja2JveFNlbGVjdGlvbkZpbHRlcmVkT25seTogdHJ1ZSxcclxuICAgICAgICBjaGVja2JveFNlbGVjdGlvbjogdHJ1ZSxcclxuICAgICAgICBtaW5XaWR0aDogNDAsXHJcbiAgICAgICAgbWF4V2lkdGg6IDQwLFxyXG4gICAgICAgIGNlbGxTdHlsZTogeyAncGFkZGluZy10b3AnOiAnNXB4JyB9LFxyXG4gICAgICAgIHNob3dSb3dHcm91cDogZmFsc2UsXHJcbiAgICAgICAgZW5hYmxlUm93R3JvdXA6IGZhbHNlLFxyXG4gICAgICAgIGVuYWJsZVBpdm90OiBmYWxzZSxcclxuICAgICAgICBzdXBwcmVzc0ZpbHRlcjogdHJ1ZSxcclxuICAgICAgICBtZW51VGFiczogW11cclxuICAgICAgfTtcclxuICAgICAgdGhpcy5jb2x1bW5EZWZzLnB1c2goc2VsZWN0Q29sdW1uKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgbGV0IGNlbGxSZW5kZXJlcjogQ29yZUdyaWRDZWxsVHlwZTtcclxuICAgICAgbGV0IGNvbHVtRWRpdGFibGU6IGJvb2xlYW4gPSB0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlO1xyXG4gICAgICBpZiAoaXRlbS5lZGl0YWJsZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY29sdW1FZGl0YWJsZSA9IGl0ZW0uZWRpdGFibGU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChjb2x1bUVkaXRhYmxlKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkxhYmVsKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMYWJlbENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5EYXRlKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVEYXRlRWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5TZWxlY3QpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdEVkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTnVtZXJpYykge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTnVtZXJpY0VkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuQm9vbGVhbikge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkVkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTG92KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMT1ZFZGl0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLk1hc2spIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU1hc2tFZGl0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkN1cnJlbmN5KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUVkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTnVtZXJpY0xhYmVsKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljQ2VsbDtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkxhYmVsKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMYWJlbENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMYWJlbENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5EYXRlKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVEYXRlQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlNlbGVjdCkge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLk51bWVyaWMpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuQm9vbGVhbikge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkNlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5Mb3YpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5NYXNrKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkN1cnJlbmN5KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUNlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5OdW1lcmljTGFiZWwpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuVGV4dFNlbGVjdCkge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0Q2VsbDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLnZpc2libGUgIT09IGZhbHNlKSB7XHJcbiAgICAgICAgaXRlbS52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpdGVtLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IGhlYWRlck5hbWUgPSBpdGVtLmZpZWxkO1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9uc1xyXG4gICAgICAgICYmIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnNcclxuICAgICAgICAmJiB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnNcclxuICAgICAgICAmJiB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmwpIHtcclxuICAgICAgICBjb25zdCBlbmRQb2ludFVybCA9IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5lbmRQb2ludFVybDtcclxuICAgICAgICBpZiAoZW5kUG9pbnRVcmwuaW5kZXhPZignLycpICE9PSAtMSkge1xyXG4gICAgICAgICAgaGVhZGVyTmFtZSA9IGVuZFBvaW50VXJsLnNwbGl0KCcvJylbMF0gKyBpdGVtLmZpZWxkO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBoZWFkZXJOYW1lID0gZW5kUG9pbnRVcmwgKyBpdGVtLmZpZWxkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgY29sdW1uID0ge1xyXG4gICAgICAgIGZpZWxkOiBpdGVtLmZpZWxkLFxyXG4gICAgICAgIGhlYWRlck5hbWU6IGl0ZW0uaGVhZGVyTmFtZSB8fCBoZWFkZXJOYW1lLFxyXG4gICAgICAgIGNlbGxSZW5kZXJlcixcclxuICAgICAgICBoaWRlOiAhaXRlbS52aXNpYmxlLFxyXG4gICAgICAgIHN1cHByZXNzU2l6ZVRvRml0OiBudWxsLFxyXG4gICAgICAgIGZpbHRlcjogdGhpcy5ncmlkT3B0aW9ucy5lbmFibGVGaWx0ZXIgfHwgZmFsc2UsXHJcbiAgICAgICAgc29ydGFibGU6IHRoaXMuZ3JpZE9wdGlvbnMuZW5hYmxlU29ydGluZyB8fCBmYWxzZSxcclxuICAgICAgICByZXNpemFibGU6IHRydWUsXHJcbiAgICAgICAgY2VsbEVkaXRvclBhcmFtczoge1xyXG4gICAgICAgICAgZGF0YVNvdXJjZTogbnVsbCxcclxuICAgICAgICAgIHNldHRpbmdzOiBudWxsLFxyXG4gICAgICAgICAgZGF0YTogbnVsbCxcclxuICAgICAgICAgIGN1c3RvbURpc3BsYXlGdW5jdGlvbjogaXRlbS5jdXN0b21EaXNwbGF5RnVuY3Rpb24gfHwgbnVsbCxcclxuICAgICAgICAgIGN1c3RvbUVkaXRhYmxlRnVuY3Rpb246IGl0ZW0uY3VzdG9tRWRpdGFibGVGdW5jdGlvbiB8fCBudWxsLFxyXG4gICAgICAgICAgY3VzdG9tRnVuY3Rpb25Gb3JSb3dWYWx1ZUNoYW5nZXM6IGl0ZW0uY3VzdG9tRnVuY3Rpb25Gb3JSb3dWYWx1ZUNoYW5nZXMgfHwgbnVsbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmxvYXRpbmdGaWx0ZXJDb21wb25lbnRQYXJhbXM6IHtcclxuICAgICAgICAgIGhpZGVGaWx0ZXI6IGl0ZW0uaGlkZUZpbHRlciB8fCBmYWxzZSxcclxuICAgICAgICAgIHNlcnZpY2VBY2Nlc3M6IHRoaXMsXHJcbiAgICAgICAgICBmaWVsZDogaXRlbS5maWVsZCxcclxuICAgICAgICAgIGN1c3RvbUZpbHRlckZ1bmN0aW9uOiBpdGVtLmN1c3RvbUZpbHRlckZ1bmN0aW9uIHx8IG51bGxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGN1c3RvbUZpZWxkRm9yRmlsdGVyOiBpdGVtLmN1c3RvbUZpZWxkRm9yRmlsdGVyLFxyXG4gICAgICAgIGN1c3RvbUZpZWxkRm9yU29ydDogaXRlbS5jdXN0b21GaWVsZEZvclNvcnQsXHJcbiAgICAgICAgZ2V0RGF0YVNvdXJjZUFmdGVyRGF0YUxvYWRlZDogaXRlbS5nZXREYXRhU291cmNlQWZ0ZXJEYXRhTG9hZGVkLFxyXG4gICAgICAgIGNvbHVtblR5cGU6IGl0ZW0uY29sdW1uVHlwZVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKGl0ZW0uZmllbGQgPT09IHRoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGQpIHtcclxuICAgICAgICBjb2x1bW4uc3VwcHJlc3NTaXplVG9GaXQgPSB0cnVlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuRGF0ZSAmJiAodGhpcy5ncmlkT3B0aW9ucy5lZGl0YWJsZSB8fCBjb2x1bW5bJ2VkaXRhYmxlJ10pKSB7XHJcbiAgICAgICAgY29sdW1uWydtaW5XaWR0aCddID0gMTM1O1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgaWYgKGl0ZW0ubWluV2lkdGgpIHtcclxuICAgICAgICBjb2x1bW5bJ21pbldpZHRoJ10gPSBpdGVtLm1pbldpZHRoO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXRlbS5tYXhXaWR0aCkge1xyXG4gICAgICAgIGNvbHVtblsnbWF4V2lkdGgnXSA9IGl0ZW0ubWF4V2lkdGg7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICBpZiAoaXRlbS5kYXRhU291cmNlKSB7XHJcbiAgICAgICAgY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSA9IGl0ZW0uZGF0YVNvdXJjZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoaXRlbS5zZXR0aW5ncykge1xyXG4gICAgICAgIGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzID0gaXRlbS5zZXR0aW5ncztcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKFxyXG4gICAgICAgIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5TZWxlY3QgfHwgY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RDZWxsKVxyXG4gICAgICAgICYmIGl0ZW0uc2V0dGluZ3MpIHtcclxuICAgICAgICBjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5kYXRhID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG5cclxuICAgICAgICBpZiAoIWl0ZW0uZ2V0RGF0YVNvdXJjZUFmdGVyRGF0YUxvYWRlZCkge1xyXG4gICAgICAgICAgdGhpcy5nZXRDb2x1bW5EYXRhU291cmNlKGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLmRhdGEsIGNvbHVtbik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBpbmRleCA9IF8uZmluZEluZGV4KHRoaXMuY29sdW1uRGVmcywgWydmaWVsZCcsIGl0ZW0uZmllbGRdKTtcclxuICAgICAgaWYgKGluZGV4IDwgMCkge1xyXG4gICAgICAgIHRoaXMuY29sdW1uRGVmcy5wdXNoKGNvbHVtbik7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jb2x1bW5EZWZzW2luZGV4XSA9IGNvbHVtbjtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLnNldEZpbHRlckNvbXBvbmVudHMoKTtcclxuICAgIHRoaXMuYWRkQ29tbWFuZENvbHVtbigpO1xyXG4gIH1cclxuXHJcbiAgcmVsb2FkQ29sdW1uRGVmcygpIHtcclxuICAgIHRoaXMuY3JlYXRlQ29sdW1ucygpO1xyXG4gICAgdGhpcy5yZWxvYWRHcmlkQ29sdW1uRGVmcygpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q29sdW1uU3RhdGUoKTogdm9pZCB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNvbHVtbkFwaS5nZXRDb2x1bW5TdGF0ZSgpKTtcclxuICB9XHJcblxyXG4gIGdldENvbHVtbkRhdGFTb3VyY2UoZGF0YTogU3ViamVjdDxhbnk+LCBjb2x1bW46IGFueSwgZXh0cmFGaWx0ZXJzPzogRmlsdGVyW10pIHtcclxuICAgIGlmICh0aGlzLmxvYWRPbkluaXQgfHwgdGhpcy5sb2FkQ29sdW1uRGF0YVNvdXJjZU9uSW5pdCkge1xyXG4gICAgICBjb25zdCBzZXR0aW5ncyA9IGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzIGFzIFNlbGVjdG9yU2V0dGluZ3MgfCBMb3ZTZXR0aW5ncztcclxuICAgICAgbGV0IHJlcXVlc3RPcHRpb25zOiBSZXF1ZXN0T3B0aW9ucztcclxuICAgICAgaWYgKHNldHRpbmdzKSB7XHJcbiAgICAgICAgaWYgKGNvbHVtbi5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuU2VsZWN0IHx8IGNvbHVtbi5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuVGV4dFNlbGVjdCkge1xyXG4gICAgICAgICAgcmVxdWVzdE9wdGlvbnMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KChzZXR0aW5ncyBhcyBTZWxlY3RvclNldHRpbmdzKS5yZXF1ZXN0T3B0aW9ucykpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXF1ZXN0T3B0aW9ucyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoKHNldHRpbmdzIGFzIExvdlNldHRpbmdzKS5vcHRpb25zLnJlcXVlc3RPcHRpb25zKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAocmVxdWVzdE9wdGlvbnMpIHtcclxuICAgICAgICBjb25zdCByZXFVcmwgPSByZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLm1vZHVsZVVybCArIHJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmw7XHJcblxyXG4gICAgICAgIGxldCBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICBpZiAoIXRoaXMuZ3JpZE9wdGlvbnMuZGlzYWJsZVNlbGZMb2FkaW5nKSB7XHJcbiAgICAgICAgICBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5pbnNlcnRMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBkaXNhYmxlTG9hZGluZ0hlYWRlcjogSGVhZGVyUGFyYW1ldGVyID0geyBoZWFkZXI6IEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcsIHZhbHVlOiBIZWFkZXJzLkRpc2FibGVMb2FkaW5nIH07XHJcblxyXG4gICAgICAgIGlmIChleHRyYUZpbHRlcnMgJiYgZXh0cmFGaWx0ZXJzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIgJiYgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcykge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzID0gW3tcclxuICAgICAgICAgICAgICAgIEZpbHRlcnM6IFtdXHJcbiAgICAgICAgICAgICAgfV07XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMudW5zaGlmdCh7XHJcbiAgICAgICAgICAgICAgICBGaWx0ZXJzOiBbXVxyXG4gICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIGlmICghcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlciA9IHtcclxuICAgICAgICAgICAgICBGaWx0ZXJHcm91cHM6IFt7XHJcbiAgICAgICAgICAgICAgICBGaWx0ZXJzOiBbXVxyXG4gICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIgJiYgIXJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMpIHtcclxuICAgICAgICAgICAgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3VwcyA9IFt7XHJcbiAgICAgICAgICAgICAgRmlsdGVyczogW11cclxuICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgaWYgKHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlciAmJlxyXG4gICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzICYmXHJcbiAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzWzBdLkZpbHRlcnMgPSBleHRyYUZpbHRlcnM7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmRhdGFTZXJ2aWNlLmdldExpc3RCeShyZXFVcmwsIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlciwgZGlzYWJsZUdlbmVyYWxMb2FkaW5nID8gW2Rpc2FibGVMb2FkaW5nSGVhZGVyXSA6IG51bGwpXHJcbiAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgZmluYWxpemUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgIGlmICghdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2VsZkxvYWRpbmcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UucmVtb3ZlTG9hZGluZ1JlcXVlc3QocmVxVXJsKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgLnN1YnNjcmliZSgoaHR0cFJlc3BvbnNlOiBDb3JlSHR0cFJlc3BvbnNlPGFueT4pID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbG92RGF0YTogYW55W10gPSBodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQ7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBsb3ZEYXRhLm1hcCh4ID0+IE9iamVjdC5hc3NpZ24oe30sIHgpKTtcclxuICAgICAgICAgICAgY29uc3QgY29sSW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmNvbHVtbkRlZnMsIFsnZmllbGQnLCBjb2x1bW4uZmllbGRdKTtcclxuICAgICAgICAgICAgY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgICAgICAgIHRoaXMuY29sdW1uRGVmc1tjb2xJbmRleF0uY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgICAgICAgZGF0YS5uZXh0KHsgZGF0YTogdHJ1ZSB9KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRDb21wb25lbnRzKCkge1xyXG4gICAgY29uc3QgY29tcG9uZW50cyA9IHt9O1xyXG4gICAgLy8gRWRpdCBDZWxsXHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUJvb2xlYW5FZGl0Q2VsbF0gPSBCb29sZWFuRWRpdENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVFZGl0Q2VsbF0gPSBEYXRlRWRpdENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUxPVkVkaXRDZWxsXSA9IExvdkVkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljRWRpdENlbGxdID0gTnVtZXJpY0VkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RFZGl0Q2VsbF0gPSBTZWxlY3RFZGl0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlVGV4dEVkaXRDZWxsXSA9IFRleHRFZGl0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0VkaXRDZWxsXSA9IE1hc2tFZGl0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lFZGl0Q2VsbF0gPSBDdXJyZW5jeUVkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgLy8gUmVhZC1Pbmx5IENlbGxcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkNlbGxdID0gQm9vbGVhbkNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUxhYmVsQ2VsbF0gPSBMYWJlbENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVDZWxsXSA9IERhdGVDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RDZWxsXSA9IFNlbGVjdENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5Q2VsbF0gPSBDdXJyZW5jeUNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZU1hc2tDZWxsXSA9IE1hc2tDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljQ2VsbF0gPSBOdW1lcmljQ2VsbENvbXBvbmVudDtcclxuICAgIC8vIEZpbHRlciBDZWxsXHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVGaWx0ZXJDZWxsXSA9IERhdGVGaWx0ZXJDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RGaWx0ZXJDZWxsXSA9IFNlbGVjdEZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZVRleHRGaWx0ZXJDZWxsXSA9IFRleHRGaWx0ZXJDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljRmlsdGVyQ2VsbF0gPSBOdW1lcmljRmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkZpbHRlckNlbGxdID0gQm9vbGVhbkZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5RmlsdGVyQ2VsbF0gPSBDdXJyZW5jeUZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZUxPVkZpbHRlckNlbGxdID0gTG92RmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0ZpbHRlckNlbGxdID0gTWFza0ZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICAvLyBDb21tYW5kIENlbGxcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ29tbWFuZENlbGxdID0gQ29tbWFuZENlbGxDb21wb25lbnQ7XHJcblxyXG4gICAgY29tcG9uZW50c1snYWdDb2x1bW5IZWFkZXInXSA9IENvbHVtbkhlYWRlckNvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbJ2N1c3RvbU5vUm93c092ZXJsYXknXSA9IE5vUm93c092ZXJsYXlDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzWydjdXN0b21Mb2FkaW5nT3ZlcmxheSddID0gTG9hZGluZ092ZXJsYXlDb21wb25lbnQ7XHJcblxyXG4gICAgcmV0dXJuIGNvbXBvbmVudHM7XHJcbiAgfVxyXG5cclxuICBzZXRGaWx0ZXJDb21wb25lbnRzKCkge1xyXG4gICAgdGhpcy5jb2x1bW5EZWZzLmZvckVhY2goaXRlbSA9PiB7XHJcblxyXG4gICAgICBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUJvb2xlYW5FZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVCb29sZWFuRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlRGF0ZUVkaXRDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVEYXRlQ2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVEYXRlRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTE9WRWRpdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTE9WRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTnVtZXJpY0VkaXRDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RFZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RWRpdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlVGV4dEZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUJvb2xlYW5DZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUJvb2xlYW5GaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMYWJlbENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlVGV4dEZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU1hc2tFZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0NlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0ZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5RWRpdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUNlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljQ2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0Q2VsbFxyXG4gICAgICAgICYmIGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlRleHRTZWxlY3QpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlVGV4dEZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0RmlsdGVyQ2VsbDtcclxuICAgICAgfVxyXG5cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBsb2FkRGF0YUJ5RmlsdGVyQW5kUGFnZVNldHRpbmcoZ2VuZXJpY0V4cHJlc3Npb246IEdlbmVyaWNFeHByZXNzaW9uKSB7XHJcbiAgICBjb25zdCByZXFVcmwgPSBgJHt0aGlzLnVybE9wdGlvbnMubW9kdWxlVXJsfSR7dGhpcy51cmxPcHRpb25zLmVuZFBvaW50VXJsfWA7XHJcblxyXG4gICAgbGV0IGhlYWRlclBhcmFtczogSGVhZGVyUGFyYW1ldGVyW10gPSBbXTtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5IZWFkZXJQYXJhbWV0ZXJzKSB7XHJcbiAgICAgIGhlYWRlclBhcmFtcyA9IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuSGVhZGVyUGFyYW1ldGVycztcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXRoaXMuZ3JpZE9wdGlvbnMuZGlzYWJsZVNlbGZMb2FkaW5nKSB7XHJcbiAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UuaW5zZXJ0TG9hZGluZ1JlcXVlc3QocmVxVXJsKTtcclxuXHJcbiAgICAgIGNvbnN0IGRpc2FibGVMb2FkaW5nSGVhZGVyOiBIZWFkZXJQYXJhbWV0ZXIgPSB7IGhlYWRlcjogSGVhZGVycy5EaXNhYmxlTG9hZGluZywgdmFsdWU6IEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcgfTtcclxuICAgICAgaGVhZGVyUGFyYW1zLnB1c2goZGlzYWJsZUxvYWRpbmdIZWFkZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0aGlzLmRhdGFTZXJ2aWNlLmdldExpc3RCeShyZXFVcmwsIGdlbmVyaWNFeHByZXNzaW9uLCBoZWFkZXJQYXJhbXMpXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGZpbmFsaXplKCgpID0+IHtcclxuICAgICAgICAgIGlmICghdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2VsZkxvYWRpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5yZW1vdmVMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbG9hZEdyaWRWaWV3RGF0YUJ5T3B0aW9uc1VybEFuZEZpbHRlcnMoKSB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucykge1xyXG4gICAgICBjb25zdCBzZWFyY2hGaWx0ZXI6IEdlbmVyaWNFeHByZXNzaW9uID0gXy5jbG9uZURlZXAodGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIpO1xyXG4gICAgICBpZiAodGhpcy5wYWdpbmdSZXN1bHQpIHtcclxuICAgICAgICBzZWFyY2hGaWx0ZXIuUGFnZU51bWJlciA9IHRoaXMucGFnaW5nUmVzdWx0LkN1cnJlbnRQYWdlO1xyXG4gICAgICAgIHNlYXJjaEZpbHRlci5QYWdlU2l6ZSA9IHRoaXMucGFnaW5nUmVzdWx0LlBhZ2VTaXplO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBzZWFyY2hGaWx0ZXIuRmlsdGVyR3JvdXBzID0gc2VhcmNoRmlsdGVyLkZpbHRlckdyb3VwcyB8fCBbXTtcclxuICAgICAgdGhpcy5maWx0ZXIuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICBsZXQgbmFtZTogc3RyaW5nO1xyXG4gICAgICAgIGNvbnN0IGZvdW5kQ29sdW1uID0gdGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zLmZpbmQoeCA9PiB4LmZpZWxkID09PSBlbGVtZW50LmtleSk7XHJcbiAgICAgICAgaWYgKGZvdW5kQ29sdW1uKSB7XHJcbiAgICAgICAgICBuYW1lID0gZm91bmRDb2x1bW4uY3VzdG9tRmllbGRGb3JGaWx0ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbmFtZSkge1xyXG4gICAgICAgICAgaWYgKGVsZW1lbnQua2V5LmluZGV4T2YoJ0lkJykgPiAwICYmXHJcbiAgICAgICAgICAgICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSBmYWxzZSB8fCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICAgIG5hbWUgPSBlbGVtZW50LmtleS5zdWJzdHIoMCwgZWxlbWVudC5rZXkuaW5kZXhPZignSWQnKSkgK1xyXG4gICAgICAgICAgICAgICcuJyArIGVsZW1lbnQua2V5LnN1YnN0cihlbGVtZW50LmtleS5pbmRleE9mKCdJZCcpLCBlbGVtZW50LmtleS5sZW5ndGgpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbmFtZSA9IGVsZW1lbnQua2V5O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZ3JvdXA6IEZpbHRlckdyb3VwID0ge1xyXG4gICAgICAgICAgRmlsdGVyczogW3tcclxuICAgICAgICAgICAgUHJvcGVydHlOYW1lOiBuYW1lLFxyXG4gICAgICAgICAgICBWYWx1ZTogZWxlbWVudC52YWx1ZVsnZmlsdGVyJ10sXHJcbiAgICAgICAgICAgIENvbXBhcmlzb246IGVsZW1lbnQudmFsdWVbJ3R5cGUnXSxcclxuICAgICAgICAgICAgT3RoZXJWYWx1ZTogZWxlbWVudC52YWx1ZVsnb3RoZXInXSB8fCBudWxsXHJcbiAgICAgICAgICB9XVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHNlYXJjaEZpbHRlci5GaWx0ZXJHcm91cHMucHVzaChncm91cCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gaXQgaXMgZm9yIENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0U2VsZWN0XHJcbiAgICAgIHRoaXMuZmlsdGVyR3JvdXBMaXN0LmZvckVhY2goKGZpbHRlckdyb3VwSXRlbTogYW55KSA9PiB7XHJcbiAgICAgICAgc2VhcmNoRmlsdGVyLkZpbHRlckdyb3Vwcy51bnNoaWZ0KGZpbHRlckdyb3VwSXRlbS52YWx1ZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gaWYgKHNlYXJjaEZpbHRlci5GaWx0ZXJHcm91cHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAvLyAgIHNlYXJjaEZpbHRlci5GaWx0ZXJHcm91cHNbc2VhcmNoRmlsdGVyLkZpbHRlckdyb3Vwcy5sZW5ndGggLSAxXVsnQ29ubmVjdG9yJ10gPSBDb25uZWN0b3IuT3I7XHJcbiAgICAgIC8vIH1cclxuXHJcbiAgICAgIHRoaXMubG9hZERhdGFCeUZpbHRlckFuZFBhZ2VTZXR0aW5nKHNlYXJjaEZpbHRlcilcclxuICAgICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgICAgdGhpcy5wYWdpbmdSZXN1bHQgPSBodHRwUmVzcG9uc2UucGFnaW5nUmVzdWx0O1xyXG4gICAgICAgICAgdGhpcy5kYXRhID0gaHR0cFJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0O1xyXG4gICAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLm9yaWdpbmFsUm93RGF0YSA9IHRoaXMuZGF0YS5tYXAoaXRlbSA9PiBPYmplY3QuYXNzaWduKHt9LCBpdGVtKSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLnJldHJpZXZlZERhdGFTb3VyY2VTdWJqZWN0Lm5leHQodGhpcy5kYXRhKTtcclxuXHJcbiAgICAgICAgICBpZiAoIXRoaXMuZGF0YSB8fCAodGhpcy5kYXRhICYmIHRoaXMuZGF0YS5sZW5ndGggPT09IDApKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ092ZXJsYXkoKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLmxvYWREYXRhU291cmNlQWZ0ZXJHZXRHcmlkRGF0YSgpO1xyXG5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGxvYWREYXRhU291cmNlQWZ0ZXJHZXRHcmlkRGF0YSgpOiB2b2lkIHtcclxuICAgIGNvbnN0IG5lZWRUb0xvYWREYXRhU291cmNlQ29sdW1ucyA9IHRoaXMuY29sdW1uRGVmcy5maWx0ZXIoeCA9PiB4LmdldERhdGFTb3VyY2VBZnRlckRhdGFMb2FkZWQgPT09IHRydWUpO1xyXG4gICAgbmVlZFRvTG9hZERhdGFTb3VyY2VDb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcclxuICAgICAgY29uc3QgY29sdW1uVmFsdWVzOiBhbnlbXSA9IHRoaXMuZGF0YS5tYXAoeCA9PiB4W2NvbHVtbi5maWVsZF0pO1xyXG4gICAgICBjb25zdCBuZXdGaWx0ZXJzOiBGaWx0ZXJbXSA9IFtdO1xyXG5cclxuICAgICAgY29sdW1uVmFsdWVzLmZvckVhY2goKGNvbHVtblZhbHVlOiBhbnkpID0+IHtcclxuICAgICAgICBjb25zdCBuZXdGaWx0ZXI6IEZpbHRlciA9IHtcclxuICAgICAgICAgIFByb3BlcnR5TmFtZTogY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MudmFsdWVGaWVsZCxcclxuICAgICAgICAgIFZhbHVlOiBjb2x1bW5WYWx1ZSxcclxuICAgICAgICAgIENvbXBhcmlzb246IENvbXBhcmlzb24uRXF1YWxUbyxcclxuICAgICAgICAgIENvbm5lY3RvcjogQ29ubmVjdG9yLk9yXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IG5hbWUgPSBjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy52YWx1ZUZpZWxkO1xyXG5cclxuICAgICAgICAvLyBjYW4gYmUgY2hhbmdhYmxlXHJcbiAgICAgICAgaWYgKG5hbWUpIHtcclxuICAgICAgICAgIGlmIChjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy52YWx1ZUZpZWxkLmluZGV4T2YoJ0lkJykgIT09IC0xICYmXHJcbiAgICAgICAgICAgICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSBmYWxzZSB8fCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICAgIG5hbWUgPSBuYW1lLnN1YnN0cigwLCBuYW1lLmluZGV4T2YoJ0lkJykpICtcclxuICAgICAgICAgICAgICAnLicgKyBuYW1lLnN1YnN0cihuYW1lLmluZGV4T2YoJ0lkJyksIG5hbWUubGVuZ3RoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG5ld0ZpbHRlci5Qcm9wZXJ0eU5hbWUgPSBuYW1lO1xyXG5cclxuICAgICAgICBuZXdGaWx0ZXJzLnB1c2gobmV3RmlsdGVyKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAobmV3RmlsdGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgbmV3RmlsdGVyc1tuZXdGaWx0ZXJzLmxlbmd0aCAtIDFdLkNvbm5lY3RvciA9IENvbm5lY3Rvci5BbmQ7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKG5ld0ZpbHRlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuZ2V0Q29sdW1uRGF0YVNvdXJjZShjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5kYXRhLCBjb2x1bW4sIG5ld0ZpbHRlcnMpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIC8vIGdldENvbHVtbkRhdGFTb3VyY2UgXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFkZENvbW1hbmRDb2x1bW4oKSB7XHJcbiAgICBsZXQgc2hvd1Jvd0RlbGV0ZUJ1dHRvbiA9IHRydWU7XHJcbiAgICBsZXQgc2hvd1Jvd0VkaXRCdXR0b24gPSB0cnVlO1xyXG4gICAgbGV0IGJ1dHRvbkNvdW50cyA9IDE7XHJcblxyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncykge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93Um93RGVsZXRlQnV0dG9uICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICBzaG93Um93RGVsZXRlQnV0dG9uID0gdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93Um93RGVsZXRlQnV0dG9uO1xyXG4gICAgICAgIGlmIChzaG93Um93RGVsZXRlQnV0dG9uKSB7XHJcbiAgICAgICAgICBidXR0b25Db3VudHMrKztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuYnV0dG9uU2V0dGluZ3Muc2hvd0VkaXRCdXR0b24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHNob3dSb3dFZGl0QnV0dG9uID0gdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93RWRpdEJ1dHRvbjtcclxuICAgICAgICBpZiAoc2hvd1Jvd0VkaXRCdXR0b24pIHtcclxuICAgICAgICAgIGJ1dHRvbkNvdW50cysrO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIGl0IGlzIGZvciBkZWxldGUgYW5kIHJldmVydCBmb3IgaW5saW5lIGdyaWRcclxuICAgIC8vIGlmICh0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlICYmIGJ1dHRvbkNvdW50cyA9PT0gMSkge1xyXG4gICAgLy8gICBidXR0b25Db3VudHMgPSAyO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIGlmIChzaG93Um93RGVsZXRlQnV0dG9uIHx8IHNob3dSb3dFZGl0QnV0dG9uKSB7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gXy5maW5kSW5kZXgodGhpcy5jb2x1bW5EZWZzLCBbJ2ZpZWxkJywgJ0NvbW1hbmRDb2x1bW4nXSk7XHJcbiAgICAgIGlmIChpbmRleCA8IDApIHtcclxuICAgICAgICBjb25zdCBjb2x1bW4gPSB7XHJcbiAgICAgICAgICBoZWFkZXJOYW1lOiAnJyxcclxuICAgICAgICAgIGZpZWxkOiAnQ29tbWFuZENvbHVtbicsXHJcbiAgICAgICAgICBlZGl0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXI6IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUNvbW1hbmRDZWxsLFxyXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgIHNvcnRhYmxlOiBmYWxzZSxcclxuICAgICAgICAgIG1pbldpZHRoOiBidXR0b25Db3VudHMgKiA1MCxcclxuICAgICAgICAgIG1heFdpZHRoOiBidXR0b25Db3VudHMgKiA1MFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5jb2x1bW5EZWZzLnB1c2goY29sdW1uKTtcclxuICAgICAgfVxyXG4gICAgICAvLyB0aGlzLmNvbHVtbkRlZnMucHVzaCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjcmVhdGVGb3JtQ29udHJvbHMoKSB7XHJcbiAgICBjb25zdCBjb2x1bW5zID0gdGhpcy5jb2x1bW5BcGkuZ2V0QWxsQ29sdW1ucygpO1xyXG5cclxuICAgIGNvbnN0IGdyaWRGb3JtR3JvdXAgPSAodGhpcy5ncmlkRm9ybSBhcyBGb3JtR3JvdXApO1xyXG4gICAgLy8gY2xlYXIgb3V0IG9sZCBmb3JtIGdyb3VwIGNvbnRyb2xzIGlmIHN3aXRjaGluZyBiZXR3ZWVuIGRhdGEgc291cmNlc1xyXG4gICAgY29uc3QgY29udHJvbE5hbWVzID0gT2JqZWN0LmtleXMoZ3JpZEZvcm1Hcm91cC5jb250cm9scyk7XHJcbiAgICBjb250cm9sTmFtZXMuZm9yRWFjaCgoY29udHJvbE5hbWUpID0+IHtcclxuICAgICAgZ3JpZEZvcm1Hcm91cC5yZW1vdmVDb250cm9sKGNvbnRyb2xOYW1lKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuZ3JpZEFwaS5mb3JFYWNoTm9kZSgocm93Tm9kZTogUm93Tm9kZSkgPT4ge1xyXG5cclxuICAgICAgY29uc3QgZm9ybUFycmF5OiBGb3JtQXJyYXkgPSBuZXcgRm9ybUFycmF5KFtdKTtcclxuXHJcbiAgICAgIGNvbHVtbnMuZmlsdGVyKChjb2x1bW46IENvbHVtbikgPT4gY29sdW1uLmdldENvbERlZigpLmZpZWxkICE9PSAnQ29tbWFuZENvbHVtbicpXHJcbiAgICAgICAgLmZvckVhY2goKGNvbHVtbjogQ29sdW1uKSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmNyZWF0ZUtleSh0aGlzLmNvbHVtbkFwaSwgY29sdW1uKTsgLy8gdGhlIGNlbGxzIHdpbGwgdXNlIHRoaXMgc2FtZSBjcmVhdGVLZXkgbWV0aG9kXHJcbiAgICAgICAgICBjb25zdCBjb2w6IEdyaWRDb2x1bW4gPSBfLmZpbmQodGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zLCBbJ2ZpZWxkJywgY29sdW1uLmdldENvbERlZigpLmZpZWxkXSk7XHJcbiAgICAgICAgICBjb25zdCB2YWxpZGF0aW9uOiBWYWxpZGF0b3JGbltdID0gW107XHJcbiAgICAgICAgICBpZiAoY29sKSB7XHJcbiAgICAgICAgICAgIGlmIChjb2wudmFsaWRhdGlvbikge1xyXG4gICAgICAgICAgICAgIGlmIChjb2wudmFsaWRhdGlvbi5yZXF1aXJlZCkge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbi5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24uZW1haWwpIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRpb24ucHVzaChWYWxpZGF0b3JzLmVtYWlsKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLnJlcXVpcmVkVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbi5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWRUcnVlKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLm1pblZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5taW4oY29sLnZhbGlkYXRpb24ubWluVmFsdWUpKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLm1heFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5tYXgoY29sLnZhbGlkYXRpb24ubWF4VmFsdWUpKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLm1pbkxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbi5wdXNoKFZhbGlkYXRvcnMubWluTGVuZ3RoKGNvbC52YWxpZGF0aW9uLm1pbkxlbmd0aCkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24ubWF4TGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5tYXhMZW5ndGgoY29sLnZhbGlkYXRpb24ubWF4TGVuZ3RoKSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlmIChjb2wudmFsaWRhdGlvbi5yZWdleCkge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbi5wdXNoKFZhbGlkYXRvcnMucGF0dGVybihjb2wudmFsaWRhdGlvbi5yZWdleCkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgZm9ybUFycmF5LnNldENvbnRyb2woa2V5IGFzIGFueSwgbmV3IEZvcm1Db250cm9sKCcnLCB7IHZhbGlkYXRvcnM6IHZhbGlkYXRpb24gfSkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICBncmlkRm9ybUdyb3VwLmFkZENvbnRyb2wocm93Tm9kZS5pZCBhcyBhbnksIGZvcm1BcnJheSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVsb2FkR3JpZENvbHVtbkRlZnMoKTogdm9pZCB7XHJcbiAgICAvLyB0aGlzLmNvbHVtbkRlZnMgPSBjb2x1bW5EZWZzO1xyXG4gICAgaWYgKHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICB0aGlzLmdyaWRBcGkuc2V0Q29sdW1uRGVmcyh0aGlzLmNvbHVtbkRlZnMpO1xyXG4gICAgICB0aGlzLmNvbHVtbkFwaS5yZXNldENvbHVtblN0YXRlKCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJlZnJlc2hGb3JtQ29udHJvbHMoKTtcclxuICAgIGlmICh0aGlzLmdyaWRBcGkpIHtcclxuICAgICAgdGhpcy5ncmlkQXBpLnNpemVDb2x1bW5zVG9GaXQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldENvbHVtbkRhdGFTb3VyY2UoY29sdW1uTmFtZTogc3RyaW5nLCBkYXRhU291cmNlOiBhbnkpIHtcclxuICAgIGNvbnN0IGluZGV4ID0gXy5maW5kSW5kZXgodGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zLCBbJ2ZpZWxkJywgY29sdW1uTmFtZV0pO1xyXG4gICAgdGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zW2luZGV4XS5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgIGlmICh0aGlzLmdyaWRBcGkpIHtcclxuICAgICAgdGhpcy5ncmlkQXBpLnJlZnJlc2hDZWxscyh7IGNvbHVtbnM6IFtjb2x1bW5OYW1lXSwgZm9yY2U6IHRydWUgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRDb2x1bW5Mb3ZPcHRpb25zKGNvbHVtbjogR3JpZENvbHVtbikge1xyXG4gICAgaWYgKChjb2x1bW4uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlNlbGVjdCB8fCBjb2x1bW4uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkxvdikgJiYgY29sdW1uLnNldHRpbmdzKSB7XHJcbiAgICAgIGNvbnN0IHNldHRpbmdzID0gY29sdW1uLnNldHRpbmdzIGFzIFNlbGVjdG9yU2V0dGluZ3MgfCBMb3ZTZXR0aW5ncztcclxuICAgICAgbGV0IHJlcXVlc3RPcHRpb25zOiBSZXF1ZXN0T3B0aW9ucztcclxuICAgICAgaWYgKGNvbHVtbi5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuU2VsZWN0KSB7XHJcbiAgICAgICAgcmVxdWVzdE9wdGlvbnMgPSAoc2V0dGluZ3MgYXMgU2VsZWN0b3JTZXR0aW5ncykucmVxdWVzdE9wdGlvbnM7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVxdWVzdE9wdGlvbnMgPSAoc2V0dGluZ3MgYXMgTG92U2V0dGluZ3MpLm9wdGlvbnMucmVxdWVzdE9wdGlvbnM7XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgcmVxVXJsID0gcmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5tb2R1bGVVcmwgKyAnLydcclxuICAgICAgICArIHJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmw7XHJcblxyXG4gICAgICBsZXQgZGlzYWJsZUdlbmVyYWxMb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgIGlmICghdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2VsZkxvYWRpbmcpIHtcclxuICAgICAgICBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UuaW5zZXJ0TG9hZGluZ1JlcXVlc3QocmVxVXJsKTtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBkaXNhYmxlTG9hZGluZ0hlYWRlcjogSGVhZGVyUGFyYW1ldGVyID0geyBoZWFkZXI6IEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcsIHZhbHVlOiBIZWFkZXJzLkRpc2FibGVMb2FkaW5nIH07XHJcbiAgICAgIHRoaXMuZGF0YVNlcnZpY2UuZ2V0TGlzdEJ5KHJlcVVybCwgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLCBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPyBbZGlzYWJsZUxvYWRpbmdIZWFkZXJdIDogbnVsbClcclxuICAgICAgICAucGlwZShcclxuICAgICAgICAgIGZpbmFsaXplKCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmdyaWRPcHRpb25zLmRpc2FibGVTZWxmTG9hZGluZykge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UucmVtb3ZlTG9hZGluZ1JlcXVlc3QocmVxVXJsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSkpXHJcbiAgICAgICAgLnN1YnNjcmliZSgoaHR0cFJlc3BvbnNlOiBDb3JlSHR0cFJlc3BvbnNlPGFueT4pID0+IHtcclxuICAgICAgICAgIGNvbnN0IGxvdkRhdGE6IGFueVtdID0gaHR0cFJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0O1xyXG4gICAgICAgICAgY29uc3QgZGF0YVNvdXJjZSA9IGxvdkRhdGEubWFwKHggPT4gT2JqZWN0LmFzc2lnbih7fSwgeCkpO1xyXG4gICAgICAgICAgY29uc3QgY29sSW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmNvbHVtbkRlZnMsIFsnZmllbGQnLCBjb2x1bW4uZmllbGRdKTtcclxuICAgICAgICAgIGNvbHVtbi5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgICAgIHRoaXMuY29sdW1uRGVmc1tjb2xJbmRleF0uY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgICAgIHRoaXMuY29sdW1uRGVmc1tjb2xJbmRleF0uY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncyA9IHNldHRpbmdzO1xyXG4gICAgICAgICAgaWYgKHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICAgICAgICB0aGlzLmdyaWRBcGkucmVmcmVzaENlbGxzKHsgY29sdW1uczogW2NvbHVtbi5maWVsZF0sIGZvcmNlOiB0cnVlIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25HcmlkUmVhZHkocGFyYW1zKSB7XHJcbiAgICB0aGlzLmdyaWRBcGkgPSBwYXJhbXMuYXBpO1xyXG4gICAgdGhpcy5jb2x1bW5BcGkgPSBwYXJhbXMuY29sdW1uQXBpO1xyXG5cclxuICAgIHRoaXMucmVmcmVzaEZvcm1Db250cm9scygpO1xyXG4gICAgaWYgKHRoaXMubG9hZGluZ0Rpc3BsYXlTdGF0dXMpIHtcclxuICAgICAgdGhpcy5ncmlkQXBpLnNob3dMb2FkaW5nT3ZlcmxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmlzTG9hZCA9PT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5pbml0KHRoaXMuZ3JpZE9wdGlvbnMsIHRoaXMubG9hZE9uSW5pdCwgdGhpcy5sb2FkQ29sdW1uRGF0YVNvdXJjZU9uSW5pdCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRBcGkuc2l6ZUNvbHVtbnNUb0ZpdCgpO1xyXG4gIH1cclxuXHJcbiAgb25Db2x1bW5FdmVyeXRoaW5nQ2hhbmdlZChwYXJhbXMpIHtcclxuICAgIHRoaXMuYWxsR3JpZENvbHVtbnMgPSBwYXJhbXMuY29sdW1uQXBpLmdldEFsbENvbHVtbnMoKTtcclxuICB9XHJcblxyXG4gIG9uR3JpZFNpemVDaGFuZ2VkKCkge1xyXG4gICAgaWYgKHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiAhdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2l6ZVRvRml0ICYmIHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICAgIHRoaXMuZ3JpZEFwaS5zaXplQ29sdW1uc1RvRml0KCk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5ncmlkQXBpLnJlZnJlc2hDZWxscyh7IGZvcmNlOiB0cnVlIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjcmVhdGVLZXkoY29sdW1uQXBpOiBDb2x1bW5BcGksIGNvbHVtbjogQ29sdW1uKTogYW55IHtcclxuICAgIHJldHVybiBjb2x1bW5BcGkuZ2V0QWxsQ29sdW1ucygpLmluZGV4T2YoY29sdW1uKTtcclxuICB9XHJcblxyXG4gIGdldENvbnRleHQoc2VsZjogYW55KSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBjb21wb25lbnRQYXJlbnQ6IHNlbGYsXHJcbiAgICAgIGZvcm1Hcm91cDogdGhpcy5ncmlkRm9ybSxcclxuICAgICAgY3JlYXRlS2V5OiB0aGlzLmNyZWF0ZUtleVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hGb3JtQ29udHJvbHMoKSB7XHJcbiAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgIHRoaXMuY3JlYXRlRm9ybUNvbnRyb2xzKCk7XHJcbiAgICAgIHRoaXMuZ3JpZEFwaS5yZWZyZXNoQ2VsbHMoeyBmb3JjZTogdHJ1ZSB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2hHcmlkRGF0YShpc0NoZWNrQnVsa0RhdGE/OiBib29sZWFuKSB7XHJcbiAgICBjb25zdCBidWxrRGF0YSA9IHRoaXMuZ2V0QnVsa09wZXJhdGlvbkRhdGEoKTtcclxuICAgIGlmIChidWxrRGF0YSAmJiBidWxrRGF0YS5sZW5ndGggPiAwICYmIGlzQ2hlY2tCdWxrRGF0YSkge1xyXG4gICAgICB0aGlzLnN3ZWV0QWxlcnRTZXJ2aWNlLm1hbmFnZVJlcXVlc3RPcGVyYXRpb25XaXRoQ29uZmlybShDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5SZWZyZXNoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgIGlmIChyZXMgJiYgcmVzLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVjb3JkRXZlbnRzU3ViamVjdC5uZXh0KHsgdHlwZTogJ1JlZnJlc2hlZEdyaWQnLCByZWNvcmQ6IG51bGwgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaEdyaWQoKTtcclxuICAgICAgICAgICAgdGhpcy50b2FzdHIuc3VjY2VzcyhUb2FzdHJNZXNzYWdlcy5HcmlkUmVmcmVzaGVkKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucmVjb3JkRXZlbnRzU3ViamVjdC5uZXh0KHsgdHlwZTogJ1JlZnJlc2hlZEdyaWQnLCByZWNvcmQ6IG51bGwgfSk7XHJcbiAgICAgIHRoaXMucmVmcmVzaEdyaWQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2hHcmlkKCk6IHZvaWQge1xyXG4gICAgdGhpcy5kZWxldGVkRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gZmFsc2U7XHJcbiAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgICBpZiAodGhpcy5pc0xvYWQgPT09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuaW5pdCh0aGlzLmdyaWRPcHRpb25zLCB0cnVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2VsbFZhbHVlQ2hhbmdlZChpdGVtOiBhbnkpIHtcclxuICAgIHRoaXMuaXNEYXRhQ2hhbmdlZCA9IHRydWU7XHJcbiAgICBpZiAoaXRlbS5kYXRhLk9wZXJhdGlvblR5cGUgIT09IE9wZXJhdGlvblR5cGUuQ3JlYXRlZCkge1xyXG4gICAgICBpdGVtLmRhdGEuT3BlcmF0aW9uVHlwZSA9IE9wZXJhdGlvblR5cGUuVXBkYXRlZDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU29ydENoYW5nZWQoc29ydE1vZGVsOiBhbnkpIHtcclxuICAgIGlmIChzb3J0TW9kZWwpIHtcclxuICAgICAgLy8gY29uc3Qgc29ydE1vZGVsID0gXy5oZWFkKHRoaXMuZ3JpZEFwaS5nZXRTb3J0TW9kZWwoKSk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRTb3J0ZWRDb2xJZCA9IHNvcnRNb2RlbC5jb2xJZDtcclxuXHJcbiAgICAgIGNvbnN0IGNvbHVtbkRlZiA9IHRoaXMuZ3JpZEFwaS5nZXRDb2x1bW5EZWYoc29ydE1vZGVsLmNvbElkKSBhcyBHcmlkQ29sdW1uO1xyXG4gICAgICBsZXQgbmFtZTogc3RyaW5nID0gY29sdW1uRGVmLmN1c3RvbUZpZWxkRm9yU29ydDtcclxuXHJcbiAgICAgIGlmICghbmFtZSkge1xyXG4gICAgICAgIGlmIChjb2x1bW5EZWYuZmllbGQuaW5kZXhPZignSWQnKSA+IDAgJiZcclxuICAgICAgICAgICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSBmYWxzZSB8fCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICBuYW1lID0gY29sdW1uRGVmLmZpZWxkLnN1YnN0cigwLCBjb2x1bW5EZWYuZmllbGQuaW5kZXhPZignSWQnKSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIG5hbWUgPSBjb2x1bW5EZWYuZmllbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBzb3J0ID0gW3tcclxuICAgICAgICBQcm9wZXJ0eU5hbWU6IG5hbWUsXHJcbiAgICAgICAgVHlwZTogc29ydE1vZGVsLnNvcnQgPT09ICdhc2MnID8gU29ydFR5cGUuQXNjIDogU29ydFR5cGUuRGVzY1xyXG4gICAgICB9XTtcclxuXHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLlNvcnQgPSBzb3J0O1xyXG5cclxuICAgICAgdGhpcy5sb2FkR3JpZFZpZXdEYXRhQnlPcHRpb25zVXJsQW5kRmlsdGVycygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25GaWx0ZXJDaGFuZ2VkKGV2ZW50OiBhbnkpIHtcclxuICAgIC8vIHRoaXMuZmlsdGVyID0gT2JqZWN0LmVudHJpZXModGhpcy5ncmlkQXBpLmdldEZpbHRlck1vZGVsKCkpLm1hcCgoW2tleSwgdmFsdWVdKSA9PiAoeyBrZXksIHZhbHVlIH0pKTtcclxuICAgIHRoaXMubG9hZEdyaWRWaWV3RGF0YUJ5T3B0aW9uc1VybEFuZEZpbHRlcnMoKTtcclxuICB9XHJcblxyXG4gIGFwcGx5Q2hhbmdlc1RvRmlsdGVyKGZpZWxkOiBzdHJpbmcsIG1vZGVsOiBhbnkpIHtcclxuICAgIHRoaXMuYWRkVG9MaXN0Rm9yRmlsdGVyKHRoaXMuZmlsdGVyLFxyXG4gICAgICBtb2RlbC5maWx0ZXIgfHwgbW9kZWwuZmlsdGVyID09PSAwID8gbW9kZWwgOiBudWxsLFxyXG4gICAgICBmaWVsZCk7XHJcbiAgfVxyXG5cclxuICBhZGRUb0xpc3RGb3JGaWx0ZXIobGlzdEFycmF5OiBhbnlbXSwgdmFsdWU6IGFueSwga2V5OiBhbnkpOiB2b2lkIHtcclxuICAgIGNvbnN0IGZvdW5kRGF0YUluZGV4OiBudW1iZXIgPSBsaXN0QXJyYXkuZmluZEluZGV4KHggPT4geC5rZXkgPT09IGtleSk7XHJcbiAgICBpZiAodmFsdWUpIHtcclxuICAgICAgY29uc3QgZGF0YVRvU2VuZDogYW55ID0ge1xyXG4gICAgICAgIGtleSxcclxuICAgICAgICB2YWx1ZVxyXG4gICAgICB9O1xyXG4gICAgICBpZiAoZm91bmREYXRhSW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgbGlzdEFycmF5W2ZvdW5kRGF0YUluZGV4XSA9IGRhdGFUb1NlbmQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGlzdEFycmF5LnB1c2goZGF0YVRvU2VuZCk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChmb3VuZERhdGFJbmRleCAhPT0gLTEpIHtcclxuICAgICAgICBsaXN0QXJyYXkuc3BsaWNlKGZvdW5kRGF0YUluZGV4LCAxKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcblxyXG4gIC8vIGNhbGxlZCBmcm9tIHRleHQgZmlsdGVyIGZvciBDb3JlR3JpZENvbHVtblR5cGUuVGV4dFNlbGVjdFxyXG4gIHRleHRTZWxlY3RGaW5kVmFsdWVzT25GaWx0ZXIoZmllbGQ6IHN0cmluZywgc2V0dGluZ3M6IFNlbGVjdG9yU2V0dGluZ3MsIG1vZGVsOiBhbnkpOiB2b2lkIHtcclxuICAgIGNvbnN0IGZvdW5kQ29sdW1uID0gdGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zLmZpbmQoeCA9PiB4LmZpZWxkID09PSBmaWVsZCk7XHJcblxyXG4gICAgaWYgKG1vZGVsLmZpbHRlcikge1xyXG4gICAgICBjb25zdCBmaWx0ZXI6IEdlbmVyaWNFeHByZXNzaW9uID0ge1xyXG4gICAgICAgIFBhZ2VTaXplOiAxMDAsXHJcbiAgICAgICAgRmlsdGVyR3JvdXBzOiBbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIEZpbHRlcnM6IFtcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBQcm9wZXJ0eU5hbWU6IHNldHRpbmdzLmxhYmVsRmllbGQgYXMgc3RyaW5nLFxyXG4gICAgICAgICAgICAgICAgVmFsdWU6IG1vZGVsLmZpbHRlcixcclxuICAgICAgICAgICAgICAgIENvbXBhcmlzb246IG1vZGVsLnR5cGUsXHJcbiAgICAgICAgICAgICAgICBDb25uZWN0b3I6IENvbm5lY3Rvci5BbmRcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgXVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5kYXRhU2VydmljZS5nZXRMaXN0Qnkoc2V0dGluZ3MucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5tb2R1bGVVcmwgKyBzZXR0aW5ncy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLmVuZFBvaW50VXJsLFxyXG4gICAgICAgIGZpbHRlcilcclxuICAgICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55W10+KSA9PiB7XHJcbiAgICAgICAgICBjb25zdCByZXN1bHQgPSBodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQgfHwgW107XHJcbiAgICAgICAgICBjb25zdCBpZExpc3QgPSByZXN1bHQubWFwKHggPT4geFtzZXR0aW5ncy52YWx1ZUZpZWxkXSkuZmlsdGVyKHRoaXMub25seVVuaXF1ZSk7XHJcbiAgICAgICAgICBjb25zdCBuZXdGaWx0ZXJzOiBGaWx0ZXJbXSA9IFtdO1xyXG5cclxuICAgICAgICAgIGxldCBuYW1lID0gJyc7XHJcbiAgICAgICAgICBpZiAoZm91bmRDb2x1bW4pIHtcclxuICAgICAgICAgICAgbmFtZSA9IGZvdW5kQ29sdW1uLmN1c3RvbUZpZWxkRm9yRmlsdGVyO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKCFuYW1lKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC5pbmRleE9mKCdJZCcpID4gMCAmJlxyXG4gICAgICAgICAgICAgICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSBmYWxzZSB8fCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICAgICAgbmFtZSA9IGZpZWxkLnN1YnN0cigwLCBmaWVsZC5pbmRleE9mKCdJZCcpKSArXHJcbiAgICAgICAgICAgICAgICAnLicgKyBmaWVsZC5zdWJzdHIoZmllbGQuaW5kZXhPZignSWQnKSwgZmllbGQubGVuZ3RoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBuYW1lID0gZmllbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZExpc3QuZm9yRWFjaCgoaWQ6IG51bWJlcikgPT4ge1xyXG4gICAgICAgICAgICBuZXdGaWx0ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgIFByb3BlcnR5TmFtZTogbmFtZSxcclxuICAgICAgICAgICAgICBWYWx1ZTogaWQsXHJcbiAgICAgICAgICAgICAgQ29tcGFyaXNvbjogQ29tcGFyaXNvbi5FcXVhbFRvLFxyXG4gICAgICAgICAgICAgIENvbm5lY3RvcjogQ29ubmVjdG9yLk9yXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgaWYgKG5ld0ZpbHRlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBuZXdGaWx0ZXJzW25ld0ZpbHRlcnMubGVuZ3RoIC0gMV0uQ29ubmVjdG9yID0gQ29ubmVjdG9yLkFuZDtcclxuICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgY29uc3QgZmlsdGVyR3JvdXA6IEZpbHRlckdyb3VwID0ge1xyXG4gICAgICAgICAgICBDb25uZWN0b3I6IENvbm5lY3Rvci5BbmQsXHJcbiAgICAgICAgICAgIEZpbHRlcnM6IG5ld0ZpbHRlcnNcclxuICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgaWYgKG5ld0ZpbHRlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmFkZFRvTGlzdEZvckZpbHRlcih0aGlzLmZpbHRlckdyb3VwTGlzdCwgZmlsdGVyR3JvdXAsIGZpZWxkKTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkR3JpZFZpZXdEYXRhQnlPcHRpb25zVXJsQW5kRmlsdGVycygpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gZ3JpZGkgYm/Fn2FsbWFrIGxhesSxbVxyXG4gICAgICAgICAgICB0aGlzLmRhdGEgPSBbXTtcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudCA9IDA7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hZGRUb0xpc3RGb3JGaWx0ZXIodGhpcy5maWx0ZXJHcm91cExpc3QsIG51bGwsIGZpZWxkKTtcclxuICAgICAgdGhpcy5sb2FkR3JpZFZpZXdEYXRhQnlPcHRpb25zVXJsQW5kRmlsdGVycygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25seVVuaXF1ZSh2YWx1ZSwgaW5kZXgsIHNlbGYpIHtcclxuICAgIHJldHVybiBzZWxmLmluZGV4T2YodmFsdWUpID09PSBpbmRleDtcclxuICB9XHJcblxyXG4gIG9uUGFnZUNoYW5nZWQoZXZlbnQ6IGFueSkge1xyXG4gICAgdGhpcy5wYWdpbmdSZXN1bHQuQ3VycmVudFBhZ2UgPSBldmVudC5wYWdlO1xyXG4gICAgdGhpcy5wYWdpbmdSZXN1bHQuUGFnZVNpemUgPSBldmVudC5pdGVtc1BlclBhZ2U7XHJcbiAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZUl0ZW1zUGVyUGFnZShwYWdlU2l6ZUV2ZW50OiBhbnkpIHtcclxuICAgIGlmIChwYWdlU2l6ZUV2ZW50LklkKSB7XHJcbiAgICAgIHRoaXMucGFnaW5nUmVzdWx0LlBhZ2VTaXplID0gcGFnZVNpemVFdmVudC5JZDtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50ID4gMCkge1xyXG4gICAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRCdWxrT3BlcmF0aW9uRGF0YSgpOiBhbnkge1xyXG4gICAgY29uc3QgcmVzdWx0RGF0YSA9IFtdO1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5lZGl0YWJsZSkge1xyXG4gICAgICB0aGlzLnZhbGlkYXRlRm9ybSgpO1xyXG5cclxuICAgICAgdGhpcy5kZWxldGVkRGF0YVxyXG4gICAgICAgIC5mb3JFYWNoKChvYmopID0+IHtcclxuICAgICAgICAgIHJlc3VsdERhdGEucHVzaChvYmopO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgdGhpcy5kYXRhLmZvckVhY2gob2JqID0+IHtcclxuICAgICAgICBpZiAob2JqLk9wZXJhdGlvblR5cGUgIT09IE9wZXJhdGlvblR5cGUuTm9uZSkge1xyXG4gICAgICAgICAgcmVzdWx0RGF0YS5wdXNoKG9iaik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiByZXN1bHREYXRhO1xyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVGb3JtKGZvcm1Hcm91cDogRm9ybUdyb3VwID0gdGhpcy5ncmlkRm9ybSk6IGJvb2xlYW4ge1xyXG4gICAgT2JqZWN0LmtleXMoZm9ybUdyb3VwLmNvbnRyb2xzKS5mb3JFYWNoKGZpZWxkID0+IHtcclxuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm1Hcm91cC5nZXQoZmllbGQpO1xyXG4gICAgICBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1Db250cm9sKSB7XHJcbiAgICAgICAgY29udHJvbC5tYXJrQXNUb3VjaGVkKHsgb25seVNlbGY6IHRydWUgfSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1Hcm91cCkge1xyXG4gICAgICAgIHRoaXMudmFsaWRhdGVGb3JtKGNvbnRyb2wpO1xyXG4gICAgICB9IGVsc2UgaWYgKGNvbnRyb2wgaW5zdGFuY2VvZiBGb3JtQXJyYXkpIHtcclxuICAgICAgICBPYmplY3Qua2V5cyhjb250cm9sLmNvbnRyb2xzKS5mb3JFYWNoKGMgPT4ge1xyXG4gICAgICAgICAgY29uc3QgY3RybCA9IGNvbnRyb2wuZ2V0KGMpO1xyXG4gICAgICAgICAgY3RybC5tYXJrQXNUb3VjaGVkKHsgb25seVNlbGY6IHRydWUgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGZvcm1Hcm91cC52YWxpZDtcclxuICB9XHJcblxyXG4gIHJldmVydERhdGEoKSB7XHJcbiAgICB0aGlzLnN3ZWV0QWxlcnRTZXJ2aWNlLm1hbmFnZVJlcXVlc3RPcGVyYXRpb25XaXRoQ29uZmlybShDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5VbmRvKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXM6IEFsZXJ0UmVzdWx0KSA9PiB7XHJcbiAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuaXNEYXRhQ2hhbmdlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgdGhpcy5kZWxldGVkRGF0YSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5kYXRhID0gdGhpcy5vcmlnaW5hbFJvd0RhdGEubWFwKGl0ZW0gPT4gT2JqZWN0LmFzc2lnbih7fSwgaXRlbSkpO1xyXG4gICAgICAgICAgdGhpcy5ncmlkQXBpLnNldFJvd0RhdGEodGhpcy5kYXRhKTtcclxuICAgICAgICAgIHRoaXMudG9hc3RyLnN1Y2Nlc3MoVG9hc3RyTWVzc2FnZXMuUmVjb3JkUmV2ZXJ0ZWQpO1xyXG4gICAgICAgICAgdGhpcy5yZWNvcmRFdmVudHNTdWJqZWN0Lm5leHQoeyB0eXBlOiAnUmV2ZXJ0ZWRHcmlkJywgcmVjb3JkOiB0aGlzLmRhdGEgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGFkZFJvdygpIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlKSB7XHJcbiAgICAgIGNvbnN0IGFkZGVkT2JqZWN0ID0geyBJZDogLTEsIE9wZXJhdGlvblR5cGU6IE9wZXJhdGlvblR5cGUuQ3JlYXRlZCB9O1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMuZmlsdGVyKHggPT4geC5maWVsZCAhPT0gJ0lkJykuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICBhZGRlZE9iamVjdFtlbGVtZW50LmZpZWxkXSA9IGVsZW1lbnQuZGVmYXVsdFZhbHVlO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHRoaXMuZGF0YS51bnNoaWZ0KGFkZGVkT2JqZWN0KTtcclxuXHJcbiAgICAgIHRoaXMuaXNEYXRhQ2hhbmdlZCA9IHRydWU7XHJcblxyXG4gICAgICAvLyB0aGlzLmdyaWRBcGkudXBkYXRlUm93RGF0YSh7XHJcbiAgICAgIC8vICAgYWRkOiBbYWRkZWRPYmplY3RdLFxyXG4gICAgICAvLyAgIGFkZEluZGV4OiAwXHJcbiAgICAgIC8vIH0pO1xyXG5cclxuICAgICAgdGhpcy5ncmlkQXBpLnNldFJvd0RhdGEodGhpcy5kYXRhKTtcclxuXHJcbiAgICAgIHRoaXMucmVmcmVzaEZvcm1Db250cm9scygpO1xyXG5cclxuICAgICAgdGhpcy5yZWNvcmRFdmVudHNTdWJqZWN0Lm5leHQoeyB0eXBlOiAnQWRkJywgcmVjb3JkOiBhZGRlZE9iamVjdCB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuYWRkVXJsKSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5ncmlkT3B0aW9ucy5hZGRVcmxdKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWyduZXcnXSwgeyByZWxhdGl2ZVRvOiB0aGlzLnJvdXRlIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBlZGl0UmVjb3JkKHBhcmFtcywgcm93SWQsIGtleSkge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybEJsYW5rKSB7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLmVkaXRVcmwpIHtcclxuICAgICAgICB3aW5kb3cub3Blbih3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgJy8nICsgdGhpcy5ncmlkT3B0aW9ucy5lZGl0VXJsICsgJy8nICsgcGFyYW1zLmRhdGFbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHdpbmRvdy5vcGVuKHRoaXMucm91dGVyLnVybCArICcvJyArICdlZGl0LycgKyBwYXJhbXMuZGF0YVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybCkge1xyXG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybCwgcGFyYW1zLmRhdGFbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF1dKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydlZGl0JywgcGFyYW1zLmRhdGFbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF1dLCB7IHJlbGF0aXZlVG86IHRoaXMucm91dGUgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB0aGlzLmhhbmRsZUJyZWFkY3J1bWJMYWJlbChwYXJhbXMuZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gaGFuZGxlQnJlYWRjcnVtYkxhYmVsKHJvd0RhdGE6IGFueSk6IHZvaWQge1xyXG4gIC8vICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiZcclxuICAvLyAgICAgdGhpcy5ncmlkT3B0aW9ucy5icmVhZGNydW1iTGFiZWwgJiZcclxuICAvLyAgICAgcm93RGF0YSAmJlxyXG4gIC8vICAgICB0aGlzLmdyaWRPcHRpb25zLmJyZWFkY3J1bWJMYWJlbCAmJlxyXG4gIC8vICAgICB0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkICYmXHJcbiAgLy8gICAgIHJvd0RhdGFbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF0pIHtcclxuXHJcbiAgLy8gICAgIGxldCBlZGl0VXJsOiBzdHJpbmc7XHJcbiAgLy8gICAgIGNvbnN0IGtleUZpZWxkVmFsdWUgPSByb3dEYXRhW3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdO1xyXG5cclxuICAvLyAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybCkge1xyXG4gIC8vICAgICAgIGVkaXRVcmwgPSB0aGlzLmdyaWRPcHRpb25zLmVkaXRVcmwgKyAnLycgKyBrZXlGaWVsZFZhbHVlO1xyXG4gIC8vICAgICB9IGVsc2Uge1xyXG4gIC8vICAgICAgIGVkaXRVcmwgPSB0aGlzLnJvdXRlci51cmwgKyAnL2VkaXQvJyArIGtleUZpZWxkVmFsdWU7XHJcbiAgLy8gICAgIH1cclxuXHJcblxyXG4gIC8vICAgICBsZXQgY3VzdG9tTGFiZWwgPSAnJztcclxuXHJcbiAgLy8gICAgIHRoaXMuZ3JpZE9wdGlvbnMuYnJlYWRjcnVtYkxhYmVsLmZvckVhY2goKGNvbHVtbkZpZWxkOiBzdHJpbmcpID0+IHtcclxuICAvLyAgICAgICBjb25zdCBmb3VuZENvbHVtbiA9IHRoaXMuYWxsR3JpZENvbHVtbnMuZmluZCh4ID0+IHguY29sSWQgPT09IGNvbHVtbkZpZWxkKTtcclxuXHJcbiAgLy8gICAgICAgaWYgKGZvdW5kQ29sdW1uICYmIGZvdW5kQ29sdW1uLmNvbERlZikge1xyXG5cclxuICAvLyAgICAgICAgIGlmIChmb3VuZENvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcyAmJlxyXG4gIC8vICAgICAgICAgICBmb3VuZENvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlICYmXHJcbiAgLy8gICAgICAgICAgIGZvdW5kQ29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgLy8gICAgICAgICAgIGNvbnN0IGxhYmVsRmllbGQgPSBmb3VuZENvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5sYWJlbEZpZWxkO1xyXG4gIC8vICAgICAgICAgICBjb25zdCB2YWx1ZUZpbGVkID0gZm91bmRDb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MudmFsdWVGaWVsZDtcclxuXHJcbiAgLy8gICAgICAgICAgIGNvbnN0IGZvdW5kRGF0YSA9IGZvdW5kQ29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UuZmluZCh5ID0+IHlbdmFsdWVGaWxlZF0gPT09IHJvd0RhdGFbY29sdW1uRmllbGRdKTtcclxuICAvLyAgICAgICAgICAgY3VzdG9tTGFiZWwgKz0gZm91bmREYXRhW2xhYmVsRmllbGRdICsgJyAnO1xyXG5cclxuICAvLyAgICAgICAgIH0gZWxzZSB7XHJcbiAgLy8gICAgICAgICAgIGN1c3RvbUxhYmVsICs9IHJvd0RhdGFbY29sdW1uRmllbGRdICsgJyAnO1xyXG4gIC8vICAgICAgICAgfVxyXG4gIC8vICAgICAgIH1cclxuICAvLyAgICAgfSk7XHJcblxyXG4gIC8vICAgICB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmFkZEN1c3RvbUxhYmVsKGtleUZpZWxkVmFsdWUudG9TdHJpbmcoKSwgZWRpdFVybCwgY3VzdG9tTGFiZWwpO1xyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxuXHJcbiAgdW5kb1JlY29yZChwYXJhbXMsIHJvd0lkLCBrZXkpIHtcclxuICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVuZG8pXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG4gICAgICAgICAgY29uc3QgcmV2ZXJ0ZWRPYmplY3QgPSB0aGlzLmRhdGFbcm93SWRdO1xyXG4gICAgICAgICAgY29uc3Qgb3JnaW5hbE9iamVjdCA9IHRoaXMub3JpZ2luYWxSb3dEYXRhXHJcbiAgICAgICAgICAgIC5maW5kKHggPT4gcmV2ZXJ0ZWRPYmplY3QgJiYgeFt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSA9PT0gcmV2ZXJ0ZWRPYmplY3RbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF0pO1xyXG5cclxuICAgICAgICAgIHRoaXMuZGF0YVtyb3dJZF0gPSBvcmdpbmFsT2JqZWN0ID8gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShvcmdpbmFsT2JqZWN0KSkgOiBudWxsO1xyXG4gICAgICAgICAgcGFyYW1zLm5vZGUuZGF0YSA9IHRoaXMuZGF0YVtyb3dJZF07XHJcbiAgICAgICAgICB0aGlzLmdyaWRBcGkucmVkcmF3Um93cyh7IHJvd05vZGVzOiBbcGFyYW1zLm5vZGVdIH0pO1xyXG4gICAgICAgICAgdGhpcy5ncmlkQXBpLnJlZnJlc2hDZWxscyh7IHJvd05vZGVzOiBbcGFyYW1zLm5vZGVdLCBmb3JjZTogdHJ1ZSB9KTtcclxuICAgICAgICAgIHBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1tyb3dJZF0ubWFya0FzUHJpc3RpbmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG4gIGRlbGV0ZVJlY29yZChwYXJhbXMsIHJvd0lkLCBrZXkpIHtcclxuICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkRlbGV0ZSlcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICBjb25zdCBkZWxldGVVcmwgPSB0aGlzLmdyaWRPcHRpb25zLmRlbGV0ZUVuZFBvaW50VXJsID8gdGhpcy5ncmlkT3B0aW9ucy5kZWxldGVFbmRQb2ludFVybCA6IHRoaXMudXJsT3B0aW9ucy5lbmRQb2ludFVybDtcclxuICAgICAgICAgIGNvbnN0IHJlcVVybCA9IGAke3RoaXMudXJsT3B0aW9ucy5tb2R1bGVVcmx9JHtkZWxldGVVcmx9YDtcclxuICAgICAgICAgIHRoaXMuZGF0YVNlcnZpY2UuZGVsZXRlKHRoaXMuZGF0YVtyb3dJZF1bdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF0sIHJlcVVybClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLnJlY29yZEV2ZW50c1N1YmplY3QubmV4dCh7IHR5cGU6ICdEZWxldGUnLCByZWNvcmQ6IHRoaXMuZGF0YVtyb3dJZF0gfSk7XHJcblxyXG4gICAgICAgICAgICAgIC8vIGNvbnN0IGluZGV4T2ZEYXRhID0gdGhpcy5kYXRhLmluZGV4T2YocGFyYW1zLmRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAodGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudCA+IDEwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2hBbmRSZXZlcnRDaGFuZ2VzKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5zcGxpY2Uocm93SWQsIDEpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucGFnaW5nUmVzdWx0LlRvdGFsQ291bnQgJiYgdGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudC0tO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkQXBpLnNldFJvd0RhdGEodGhpcy5kYXRhKTtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIHRoaXMudG9hc3RyLnN1Y2Nlc3MoVG9hc3RyTWVzc2FnZXMuUmVjb3JkRGVsZXRlZCk7XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5jb25maXJtT3BlcmF0aW9uUG9wdXAocmVzdWx0KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2FsbFVuZG9Db25maXJtQWxlcnQoKTogT2JzZXJ2YWJsZTxBbGVydFJlc3VsdD4ge1xyXG4gICAgY29uc3QgYnVsa0RhdGEgPSB0aGlzLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcbiAgICBpZiAoYnVsa0RhdGEgJiYgYnVsa0RhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuVW5kbyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gb2YoeyB2YWx1ZTogdHJ1ZSB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbW92ZVJvdyhwYXJhbXMsIHJvd0lkLCBrZXkpIHtcclxuICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkRlbGV0ZSlcclxuICAgICAgLnN1YnNjcmliZSgocmVzOiBBbGVydFJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGlmIChyZXMgJiYgcmVzLnZhbHVlKSB7XHJcbiAgICAgICAgICAvLyB0aGlzLmRhdGFbcm93SWRdID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5vcmlnaW5hbFJvd0RhdGFbcm93SWRdKTtcclxuICAgICAgICAgIGNvbnN0IGRlbGV0ZWRPYmplY3QgPSB0aGlzLmRhdGFbcm93SWRdO1xyXG4gICAgICAgICAgdGhpcy5kYXRhLnNwbGljZShyb3dJZCwgMSk7XHJcblxyXG4gICAgICAgICAgdGhpcy5yZWNvcmRFdmVudHNTdWJqZWN0Lm5leHQoeyB0eXBlOiAnRGVsZXRlJywgcmVjb3JkOiBkZWxldGVkT2JqZWN0IH0pO1xyXG5cclxuICAgICAgICAgIGlmIChwYXJhbXMuZGF0YS5PcGVyYXRpb25UeXBlICE9PSBPcGVyYXRpb25UeXBlLkNyZWF0ZWQpIHtcclxuICAgICAgICAgICAgY29uc3Qgb3JnaW5hbERlbGV0ZWRPYmplY3QgPSB0aGlzLm9yaWdpbmFsUm93RGF0YVxyXG4gICAgICAgICAgICAgIC5maW5kKHggPT4geFt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSA9PT0gZGVsZXRlZE9iamVjdFt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSk7XHJcblxyXG4gICAgICAgICAgICBpZiAob3JnaW5hbERlbGV0ZWRPYmplY3QpIHtcclxuICAgICAgICAgICAgICBvcmdpbmFsRGVsZXRlZE9iamVjdFsnT3BlcmF0aW9uVHlwZSddID0gT3BlcmF0aW9uVHlwZS5EZWxldGVkO1xyXG4gICAgICAgICAgICAgIHRoaXMuZGVsZXRlZERhdGEucHVzaChvcmdpbmFsRGVsZXRlZE9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgZGVsZXRlZE9iamVjdFsnT3BlcmF0aW9uVHlwZSddID0gT3BlcmF0aW9uVHlwZS5EZWxldGVkO1xyXG4gICAgICAgICAgICAgIHRoaXMuZGVsZXRlZERhdGEucHVzaChkZWxldGVkT2JqZWN0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuaXNEYXRhQ2hhbmdlZCA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLmRhdGEpO1xyXG5cclxuICAgICAgICAgIC8vIGlmICghdGhpcy5ncmlkT3B0aW9ucy5lZGl0YWJsZSkge1xyXG4gICAgICAgICAgLy8gICB0aGlzLnRvYXN0ci5zdWNjZXNzKFRvYXN0ck1lc3NhZ2VzLlJlY29yZERlbGV0ZWQpO1xyXG4gICAgICAgICAgLy8gfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoQW5kUmV2ZXJ0Q2hhbmdlcygpIHtcclxuICAgIHRoaXMuZGVsZXRlZERhdGEgPSBbXTtcclxuICAgIHRoaXMuaXNEYXRhQ2hhbmdlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5yZWZyZXNoR3JpZERhdGEoKTtcclxuICB9XHJcblxyXG4gIGlzQ2hhbmdlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzRGF0YUNoYW5nZWQ7XHJcbiAgfVxyXG5cclxuICBnZXRUb3RhbENvdW50KCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudCAtIHRoaXMuZGVsZXRlZERhdGEubGVuZ3RoO1xyXG4gIH1cclxuXHJcbiAgZ2V0Tm9Sb3dzT3ZlcmxheUNvbXBvbmVudCgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuICdjdXN0b21Ob1Jvd3NPdmVybGF5JztcclxuICB9XHJcblxyXG4gIGdldExvYWRpbmdPdmVybGF5Q29tcG9uZW50KCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gJ2N1c3RvbUxvYWRpbmdPdmVybGF5JztcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93cygpOiBhbnlbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkQXBpLmdldFNlbGVjdGVkUm93cygpO1xyXG4gIH1cclxuXHJcbiAgbG9hZGluZ092ZXJsYXkoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgIGlmICh0aGlzLmxvYWRpbmdEaXNwbGF5U3RhdHVzKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkQXBpLnNob3dMb2FkaW5nT3ZlcmxheSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZ3JpZEFwaS5oaWRlT3ZlcmxheSgpO1xyXG4gICAgICAgIGlmICghdGhpcy5kYXRhIHx8ICh0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLmxlbmd0aCA9PT0gMCkpIHtcclxuICAgICAgICAgIHRoaXMuZ3JpZEFwaS5zaG93Tm9Sb3dzT3ZlcmxheSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMubG9hZGluZ0Rpc3BsYXlTdGF0dXNTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5sb2FkaW5nRGlzcGxheVN0YXR1c1N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=