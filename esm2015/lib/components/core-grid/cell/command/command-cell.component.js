/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { OperationType } from '../../../../enums/operation-type.enum';
export class CommandCellComponent {
    constructor() {
        this.showRowDeleteButton = true;
        this.showRowEditButton = true;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
        this.editable = this.params.context.componentParent.gridOptions.editable;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        /** @type {?} */
        const buttonSettings = this.params.context.componentParent.gridOptions.buttonSettings;
        if (buttonSettings) {
            if (buttonSettings.showRowDeleteButton !== undefined) {
                this.showRowDeleteButton = buttonSettings.showRowDeleteButton;
            }
            if (buttonSettings.showEditButton !== undefined) {
                this.showRowEditButton = buttonSettings.showEditButton;
            }
        }
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
    /**
     * @return {?}
     */
    showUndoButton() {
        return this.params &&
            this.params.data &&
            this.params.data.OperationType !== OperationType.Created &&
            this.editable &&
            (!this.formGroup.pristine || this.params.data.OperationType === OperationType.Updated);
    }
    /**
     * @return {?}
     */
    removeRow() {
        this.params.context.componentParent.removeRow(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    deleteRecord() {
        this.params.context.componentParent.deleteRecord(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    editRecord() {
        this.params.context.componentParent.editRecord(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    undoRecord() {
        this.params.context.componentParent.undoRecord(this.params, this.rowId, this.key);
    }
}
CommandCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-command-cell',
                template: `
    <div *ngIf="formGroup" style="margin-top: 4px;display: flex;" >
      <span  class="command-cell-item">
      <button *ngIf="showUndoButton()"  type="button" style="height: 20px" (click)="undoRecord()" class="pull-right btn-grid-inline btn-grid-edit">
      <core-icon icon="undo"></core-icon>
      </button>

				<button *ngIf="!editable && showRowDeleteButton"  type="button" style="height: 20px" (click)="deleteRecord()" class="pull-right btn-grid-inline btn-grid-delete">
          <core-icon icon="trash-alt"></core-icon>
				</button>

				<button *ngIf="editable && showRowDeleteButton"  type="button" style="height: 20px" (click)="removeRow()" class="pull-right btn-grid-inline btn-grid-delete">
          <core-icon icon="trash-alt"></core-icon>
				</button>

				<button *ngIf="!editable && showRowEditButton"  type="button" style="height: 20px" (click)="editRecord()" class="pull-right btn-grid-inline btn-grid-edit">
        <core-icon icon="edit"></core-icon>
        </button>
      </span>
    </div>
  `,
                styles: [".command-cell-item{margin-left:auto;margin-right:auto}.btn-grid-inline{color:#166dba!important;background:0 0!important;border:none!important;cursor:pointer;font-size:16px}.btn-grid-inline:focus{outline:0}.btn-grid-delete:hover{color:#e30a3a!important}.btn-grid-edit:hover{color:#17a2b8!important}"]
            }] }
];
if (false) {
    /** @type {?} */
    CommandCellComponent.prototype.key;
    /** @type {?} */
    CommandCellComponent.prototype.value;
    /** @type {?} */
    CommandCellComponent.prototype.formGroup;
    /**
     * @type {?}
     * @private
     */
    CommandCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    CommandCellComponent.prototype.params;
    /** @type {?} */
    CommandCellComponent.prototype.editable;
    /** @type {?} */
    CommandCellComponent.prototype.showRowDeleteButton;
    /** @type {?} */
    CommandCellComponent.prototype.showRowEditButton;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWFuZC1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2NvbW1hbmQvY29tbWFuZC1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUcxQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUE2QnRFLE1BQU0sT0FBTyxvQkFBb0I7SUExQmpDO1FBa0NTLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixzQkFBaUIsR0FBRyxJQUFJLENBQUM7SUErQ2xDLENBQUM7Ozs7O0lBN0NDLE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7UUFDekUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Y0FDOUQsY0FBYyxHQUFtQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLGNBQWM7UUFDckcsSUFBSSxjQUFjLEVBQUU7WUFDbEIsSUFBSSxjQUFjLENBQUMsbUJBQW1CLEtBQUssU0FBUyxFQUFFO2dCQUNwRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDO2FBQy9EO1lBQ0QsSUFBSSxjQUFjLENBQUMsY0FBYyxLQUFLLFNBQVMsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQyxjQUFjLENBQUM7YUFDeEQ7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsY0FBYztRQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU07WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsT0FBTztZQUN4RCxJQUFJLENBQUMsUUFBUTtZQUNiLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzNGLENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25GLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7OztZQWpGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLCtCQUErQjtnQkFDekMsUUFBUSxFQUNOOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW9CRDs7YUFFRjs7OztJQUVDLG1DQUFXOztJQUNYLHFDQUFhOztJQUNiLHlDQUE0Qjs7Ozs7SUFFNUIscUNBQW1COzs7OztJQUNuQixzQ0FBb0I7O0lBQ3BCLHdDQUFnQjs7SUFDaEIsbURBQWtDOztJQUNsQyxpREFBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgRm9ybUFycmF5IH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBPcGVyYXRpb25UeXBlIH0gZnJvbSAnLi4vLi4vLi4vLi4vZW51bXMvb3BlcmF0aW9uLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IEJ1dHRvblNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbW9kZWxzL2J1dHRvbi1zZXR0aW5ncy5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtY29tbWFuZC1jZWxsJyxcclxuICB0ZW1wbGF0ZTpcclxuICAgIGBcclxuICAgIDxkaXYgKm5nSWY9XCJmb3JtR3JvdXBcIiBzdHlsZT1cIm1hcmdpbi10b3A6IDRweDtkaXNwbGF5OiBmbGV4O1wiID5cclxuICAgICAgPHNwYW4gIGNsYXNzPVwiY29tbWFuZC1jZWxsLWl0ZW1cIj5cclxuICAgICAgPGJ1dHRvbiAqbmdJZj1cInNob3dVbmRvQnV0dG9uKClcIiAgdHlwZT1cImJ1dHRvblwiIHN0eWxlPVwiaGVpZ2h0OiAyMHB4XCIgKGNsaWNrKT1cInVuZG9SZWNvcmQoKVwiIGNsYXNzPVwicHVsbC1yaWdodCBidG4tZ3JpZC1pbmxpbmUgYnRuLWdyaWQtZWRpdFwiPlxyXG4gICAgICA8Y29yZS1pY29uIGljb249XCJ1bmRvXCI+PC9jb3JlLWljb24+XHJcbiAgICAgIDwvYnV0dG9uPlxyXG5cclxuXHRcdFx0XHQ8YnV0dG9uICpuZ0lmPVwiIWVkaXRhYmxlICYmIHNob3dSb3dEZWxldGVCdXR0b25cIiAgdHlwZT1cImJ1dHRvblwiIHN0eWxlPVwiaGVpZ2h0OiAyMHB4XCIgKGNsaWNrKT1cImRlbGV0ZVJlY29yZCgpXCIgY2xhc3M9XCJwdWxsLXJpZ2h0IGJ0bi1ncmlkLWlubGluZSBidG4tZ3JpZC1kZWxldGVcIj5cclxuICAgICAgICAgIDxjb3JlLWljb24gaWNvbj1cInRyYXNoLWFsdFwiPjwvY29yZS1pY29uPlxyXG5cdFx0XHRcdDwvYnV0dG9uPlxyXG5cclxuXHRcdFx0XHQ8YnV0dG9uICpuZ0lmPVwiZWRpdGFibGUgJiYgc2hvd1Jvd0RlbGV0ZUJ1dHRvblwiICB0eXBlPVwiYnV0dG9uXCIgc3R5bGU9XCJoZWlnaHQ6IDIwcHhcIiAoY2xpY2spPVwicmVtb3ZlUm93KClcIiBjbGFzcz1cInB1bGwtcmlnaHQgYnRuLWdyaWQtaW5saW5lIGJ0bi1ncmlkLWRlbGV0ZVwiPlxyXG4gICAgICAgICAgPGNvcmUtaWNvbiBpY29uPVwidHJhc2gtYWx0XCI+PC9jb3JlLWljb24+XHJcblx0XHRcdFx0PC9idXR0b24+XHJcblxyXG5cdFx0XHRcdDxidXR0b24gKm5nSWY9XCIhZWRpdGFibGUgJiYgc2hvd1Jvd0VkaXRCdXR0b25cIiAgdHlwZT1cImJ1dHRvblwiIHN0eWxlPVwiaGVpZ2h0OiAyMHB4XCIgKGNsaWNrKT1cImVkaXRSZWNvcmQoKVwiIGNsYXNzPVwicHVsbC1yaWdodCBidG4tZ3JpZC1pbmxpbmUgYnRuLWdyaWQtZWRpdFwiPlxyXG4gICAgICAgIDxjb3JlLWljb24gaWNvbj1cImVkaXRcIj48L2NvcmUtaWNvbj5cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgPC9zcGFuPlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tYW5kLWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWFuZENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBrZXk7XHJcbiAgcHVibGljIHZhbHVlO1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuXHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIGVkaXRhYmxlO1xyXG4gIHB1YmxpYyBzaG93Um93RGVsZXRlQnV0dG9uID0gdHJ1ZTtcclxuICBwdWJsaWMgc2hvd1Jvd0VkaXRCdXR0b24gPSB0cnVlO1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcbiAgICB0aGlzLmVkaXRhYmxlID0gdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQuZ3JpZE9wdGlvbnMuZWRpdGFibGU7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgY29uc3QgYnV0dG9uU2V0dGluZ3M6IEJ1dHRvblNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQuZ3JpZE9wdGlvbnMuYnV0dG9uU2V0dGluZ3M7XHJcbiAgICBpZiAoYnV0dG9uU2V0dGluZ3MpIHtcclxuICAgICAgaWYgKGJ1dHRvblNldHRpbmdzLnNob3dSb3dEZWxldGVCdXR0b24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHRoaXMuc2hvd1Jvd0RlbGV0ZUJ1dHRvbiA9IGJ1dHRvblNldHRpbmdzLnNob3dSb3dEZWxldGVCdXR0b247XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGJ1dHRvblNldHRpbmdzLnNob3dFZGl0QnV0dG9uICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICB0aGlzLnNob3dSb3dFZGl0QnV0dG9uID0gYnV0dG9uU2V0dGluZ3Muc2hvd0VkaXRCdXR0b247XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2goKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBzaG93VW5kb0J1dHRvbigpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLnBhcmFtcyAmJlxyXG4gICAgICB0aGlzLnBhcmFtcy5kYXRhICYmXHJcbiAgICAgIHRoaXMucGFyYW1zLmRhdGEuT3BlcmF0aW9uVHlwZSAhPT0gT3BlcmF0aW9uVHlwZS5DcmVhdGVkICYmXHJcbiAgICAgIHRoaXMuZWRpdGFibGUgJiZcclxuICAgICAgKCF0aGlzLmZvcm1Hcm91cC5wcmlzdGluZSB8fCB0aGlzLnBhcmFtcy5kYXRhLk9wZXJhdGlvblR5cGUgPT09IE9wZXJhdGlvblR5cGUuVXBkYXRlZCk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVSb3coKSB7XHJcbiAgICB0aGlzLnBhcmFtcy5jb250ZXh0LmNvbXBvbmVudFBhcmVudC5yZW1vdmVSb3codGhpcy5wYXJhbXMsIHRoaXMucm93SWQsIHRoaXMua2V5KTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZVJlY29yZCgpIHtcclxuICAgIHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50LmRlbGV0ZVJlY29yZCh0aGlzLnBhcmFtcywgdGhpcy5yb3dJZCwgdGhpcy5rZXkpO1xyXG4gIH1cclxuXHJcbiAgZWRpdFJlY29yZCgpIHtcclxuICAgIHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50LmVkaXRSZWNvcmQodGhpcy5wYXJhbXMsIHRoaXMucm93SWQsIHRoaXMua2V5KTtcclxuICB9XHJcblxyXG4gIHVuZG9SZWNvcmQoKSB7XHJcbiAgICB0aGlzLnBhcmFtcy5jb250ZXh0LmNvbXBvbmVudFBhcmVudC51bmRvUmVjb3JkKHRoaXMucGFyYW1zLCB0aGlzLnJvd0lkLCB0aGlzLmtleSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==