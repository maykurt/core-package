/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject, from } from 'rxjs';
import { debounceTime, flatMap, distinctUntilChanged } from 'rxjs/operators';
import { ComparisonList, ComparisonType, NumberComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class NumericFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.field = params.field;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        if (params.column
            && params.column.colDef
            && params.column.colDef.cellEditorParams
            && params.column.colDef.cellEditorParams.settings
            && params.column.colDef.cellEditorParams.settings.fields
            && params.column.colDef.cellEditorParams.settings.fields.length > 0) {
            this.field = _.head(params.column.colDef.cellEditorParams.settings.fields);
        }
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000), distinctUntilChanged(), // https://rxjs-dev.firebaseapp.com/api/operators/distinctUntilChanged
        flatMap(filterValueResult => from(params.serviceAccess.callUndoConfirmAlert()), (filterValueResult, confirmrResult) => {
            return { filterValueResult, confirmrResult };
        }))
            .subscribe(result => {
            if (result.confirmrResult.value) {
                this.currentValue = result.filterValueResult;
                if (params.serviceAccess && params.serviceAccess.filter) {
                    params.serviceAccess.applyChangesToFilter(this.field, this.buildModel());
                }
                this.params.onFloatingFilterChanged({ model: this.buildModel() });
                this.prevValue = this.currentValue;
            }
            else {
                this.canceledValue = result.filterValueResult;
                this.currentValue = this.prevValue || this.prevValue === 0 ? this.prevValue : null;
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
        if (this.canceledValue === newValue) {
            this.onFilter();
        }
    }
    // fastest solution copy paste from top
    /**
     * @return {?}
     */
    onFilter() {
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.canceledValue = this.currentValue;
                    this.currentValue = this.prevValue || this.prevValue === 0 ? this.prevValue : null;
                }
            });
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'number',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue || (this.currentValue === 0) ?
                (this.params.customFilterFunction ? this.params.customFilterFunction(this.currentValue.toString()) : this.currentValue.toString()) : null
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
NumericFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-numeric-filter-cell',
                template: `
  <div class="input-group">
    <layout-form-number-input style="width: 100%;"
        [(ngModel)]="currentValue"
        [clearable] = "true"
        placeholder="{{filterKey|translate}}"
        (ngModelChange)="valueChanged($event)">
    </layout-form-number-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
NumericFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    NumericFilterCellComponent.prototype.currentValue;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.prevValue;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.canceledValue;
    /** @type {?} */
    NumericFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    NumericFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    NumericFilterCellComponent.prototype.field;
    /** @type {?} */
    NumericFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    NumericFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtZXJpYy1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvbnVtZXJpYy1maWx0ZXItY2VsbC9udW1lcmljLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFnQixJQUFJLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbkQsT0FBTyxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3RSxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzdHLE9BQU8sRUFBYSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUV2RCxzQ0FFQzs7O0lBREMsaUNBQTRCOzs7OztBQUc5QiwwQ0FFQzs7O0lBREMscUNBQWM7O0FBMkJoQixNQUFNLE9BQU8sMEJBQTBCOzs7OztJQXFCckMsWUFBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQWxCaEUsaUJBQVksR0FBb0IsSUFBSSxPQUFPLEVBQVUsQ0FBQztRQU05RCxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBUWQsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUs5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLE9BQU87U0FDNUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksTUFBTSxDQUFDLE1BQU07ZUFDWixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU07ZUFDcEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCO2VBQ3JDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7ZUFDOUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU07ZUFDckQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JFLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDNUU7UUFFRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2FBQ3JELFNBQVMsQ0FBQyxDQUFDLEtBQVUsRUFBRSxFQUFFO1lBQ3hCLElBQUksS0FBSyxFQUFFO2dCQUNULElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNqQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0M7UUFDRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQ3BELFlBQVksQ0FBQyxJQUFJLENBQUMsRUFDbEIsb0JBQW9CLEVBQUUsRUFBRSxzRUFBc0U7UUFDOUYsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLEVBQzVFLENBQUMsaUJBQWlCLEVBQUUsY0FBbUIsRUFBRSxFQUFFO1lBQ3pDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxjQUFjLEVBQUUsQ0FBQztRQUMvQyxDQUFDLENBQUMsQ0FDTDthQUNFLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNsQixJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFO2dCQUMvQixJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztnQkFFN0MsSUFBSSxNQUFNLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO29CQUN2RCxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7aUJBQzFFO2dCQUVELElBQUksQ0FBQyxNQUFNLENBQUMsdUJBQXVCLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDbEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2FBQ3BDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixDQUFDO2dCQUM5QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUNwRjtRQUVILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsUUFBUTtRQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssUUFBUSxFQUFFO1lBQ25DLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7Ozs7O0lBR0QsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO2dCQUN6QixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQ2hCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO3dCQUNqRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3FCQUMvRTtvQkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ2xFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztpQkFDcEM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUN2QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDcEY7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxXQUFpQztRQUVwRCxxQ0FBcUM7UUFHckMsc0JBQXNCO1FBQ3RCLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsNENBQTRDO1FBQzVDLElBQUk7SUFDTixDQUFDOzs7OztJQUdELE9BQU8sQ0FBQyxJQUFTO1FBQ2YsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQzs7OztJQUVELFVBQVU7UUFDUixPQUFPO1lBQ0wsVUFBVSxFQUFFLFFBQVE7WUFDcEIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSzs7WUFDbkQsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtTQUM1SSxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELHNCQUFzQjs7Y0FDZCxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDOztjQUUxRixVQUFVLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFFNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQ3hCLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFOztnQkFDaEIsT0FBTyxHQUFHLENBQUM7WUFDZixPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztzQkFDZCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzdDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7O1lBdkxGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsc0NBQXNDO2dCQUNoRCxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBb0JUO2FBQ0Y7Ozs7WUFuQ21CLFdBQVc7WUFFdEIsZ0JBQWdCOzs7Ozs7O0lBb0N2Qiw0Q0FBb0I7Ozs7O0lBQ3BCLGtEQUE4RDs7Ozs7SUFDOUQsOERBQStDOztJQUUvQyxrREFBNEI7Ozs7O0lBQzVCLCtDQUEwQjs7Ozs7SUFDMUIsbURBQThCOztJQUM5QixnREFBcUI7O0lBQ3JCLCtDQUFxQjs7SUFJckIsMkNBQWM7O0lBRWQsaURBQTJCOztJQUMzQiwrQ0FBZ0M7Ozs7O0lBRWhDLDJEQUE0Qzs7Ozs7SUFFaEMsd0NBQXVCOzs7OztJQUFFLCtDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFnRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgSUZsb2F0aW5nRmlsdGVyLCBJRmxvYXRpbmdGaWx0ZXJQYXJhbXMsIFNlcmlhbGl6ZWRUZXh0RmlsdGVyIH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHknO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBTdWJzY3JpcHRpb24sIGZyb20gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBmbGF0TWFwLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgQ29tcGFyaXNvbkxpc3QsIENvbXBhcmlzb25UeXBlLCBOdW1iZXJDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLW51bWVyaWMtZmlsdGVyLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICA8bGF5b3V0LWZvcm0tbnVtYmVyLWlucHV0IHN0eWxlPVwid2lkdGg6IDEwMCU7XCJcclxuICAgICAgICBbKG5nTW9kZWwpXT1cImN1cnJlbnRWYWx1ZVwiXHJcbiAgICAgICAgW2NsZWFyYWJsZV0gPSBcInRydWVcIlxyXG4gICAgICAgIHBsYWNlaG9sZGVyPVwie3tmaWx0ZXJLZXl8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCI+XHJcbiAgICA8L2xheW91dC1mb3JtLW51bWJlci1pbnB1dD5cclxuXHJcbiAgICA8ZGl2IGNsYXNzPVwiZ3JpZC1maWx0ZXItaWNvblwiPlxyXG4gICAgICA8Y29yZS1pY29uICNhY3R1YWxUYXJnZXQgaWNvbj1cImZpbHRlclwiIChjbGljayk9XCJvcC50b2dnbGUoJGV2ZW50KVwiPjwvY29yZS1pY29uPlxyXG4gICAgPC9kaXY+XHJcblxyXG4gICAgPHAtb3ZlcmxheVBhbmVsICNvcCBhcHBlbmRUbz1cImJvZHlcIiBkaXNtaXNzYWJsZT1cImZhbHNlXCI+XHJcbiAgICAgIDxkaXYgKm5nSWY9XCJmb3JtR3JvdXBcIiBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiIHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvciBmb3JtQ29udHJvbE5hbWU9XCJudW1iZXJGaWx0ZXJWYWx1ZVwiIChjaGFuZ2VkKT1cImNoYW5nZWQoJGV2ZW50KVwiIFtkYXRhU291cmNlXT1cImRhdGFTb3VyY2VcIiBbY2xlYXJhYmxlXT1cImZhbHNlXCIgW3NlYXJjaGFibGVdPVwiZmFsc2VcIj5cclxuICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9wLW92ZXJsYXlQYW5lbD5cclxuICA8L2Rpdj5cclxuICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOdW1lcmljRmlsdGVyQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElGbG9hdGluZ0ZpbHRlcjxTZXJpYWxpemVkVGV4dEZpbHRlcixcclxuVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZDogU3ViamVjdDxudW1iZXI+ID0gbmV3IFN1YmplY3Q8bnVtYmVyPigpO1xyXG4gIHByaXZhdGUgbW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IG51bWJlcjtcclxuICBwcml2YXRlIHByZXZWYWx1ZTogbnVtYmVyO1xyXG4gIHByaXZhdGUgY2FuY2VsZWRWYWx1ZTogbnVtYmVyO1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gIC8vIGZpZWxkXHJcbiAgLy8gc2VydmljZUFjY2Vzc1xyXG5cclxuICBmaWVsZDogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgcGxhY2Vob2xkZXI6IHN0cmluZztcclxuICBwdWJsaWMgZmlsdGVyS2V5ID0gJ0dyaWRGaWx0ZXInO1xyXG5cclxuICBwcml2YXRlIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgIG51bWJlckZpbHRlclZhbHVlOiBOdW1iZXJDb21wYXJpc29uLkVxdWFsVG9cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZmllbGQgPSBwYXJhbXMuZmllbGQ7XHJcblxyXG4gICAgdGhpcy5wbGFjZWhvbGRlciA9IHBhcmFtcy5jb2x1bW4uY29sRGVmLmhlYWRlck5hbWUgfHwgcGFyYW1zLmNvbHVtbi5jb2xEZWYuZmllbGQ7XHJcblxyXG4gICAgaWYgKHBhcmFtcy5jb2x1bW5cclxuICAgICAgJiYgcGFyYW1zLmNvbHVtbi5jb2xEZWZcclxuICAgICAgJiYgcGFyYW1zLmNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtc1xyXG4gICAgICAmJiBwYXJhbXMuY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzXHJcbiAgICAgICYmIHBhcmFtcy5jb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzXHJcbiAgICAgICYmIHBhcmFtcy5jb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5maWVsZCA9IF8uaGVhZChwYXJhbXMuY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpZWxkcyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIGlmICh0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24gPSB0aGlzLm1vZGVsQ2hhbmdlZC5waXBlKFxyXG4gICAgICBkZWJvdW5jZVRpbWUoMTAwMCksXHJcbiAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCksIC8vIGh0dHBzOi8vcnhqcy1kZXYuZmlyZWJhc2VhcHAuY29tL2FwaS9vcGVyYXRvcnMvZGlzdGluY3RVbnRpbENoYW5nZWRcclxuICAgICAgZmxhdE1hcChmaWx0ZXJWYWx1ZVJlc3VsdCA9PiBmcm9tKHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KCkpLFxyXG4gICAgICAgIChmaWx0ZXJWYWx1ZVJlc3VsdCwgY29uZmlybXJSZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHsgZmlsdGVyVmFsdWVSZXN1bHQsIGNvbmZpcm1yUmVzdWx0IH07XHJcbiAgICAgICAgfSlcclxuICAgIClcclxuICAgICAgLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgIGlmIChyZXN1bHQuY29uZmlybXJSZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gcmVzdWx0LmZpbHRlclZhbHVlUmVzdWx0O1xyXG5cclxuICAgICAgICAgIGlmIChwYXJhbXMuc2VydmljZUFjY2VzcyAmJiBwYXJhbXMuc2VydmljZUFjY2Vzcy5maWx0ZXIpIHtcclxuICAgICAgICAgICAgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuYXBwbHlDaGFuZ2VzVG9GaWx0ZXIodGhpcy5maWVsZCwgdGhpcy5idWlsZE1vZGVsKCkpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5jYW5jZWxlZFZhbHVlID0gcmVzdWx0LmZpbHRlclZhbHVlUmVzdWx0O1xyXG4gICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSB0aGlzLnByZXZWYWx1ZSB8fCB0aGlzLnByZXZWYWx1ZSA9PT0gMCA/IHRoaXMucHJldlZhbHVlIDogbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk6IHZvaWQge1xyXG4gICAgdGhpcy5tb2RlbENoYW5nZWQubmV4dChuZXdWYWx1ZSk7XHJcbiAgICBpZiAodGhpcy5jYW5jZWxlZFZhbHVlID09PSBuZXdWYWx1ZSkge1xyXG4gICAgICB0aGlzLm9uRmlsdGVyKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBmYXN0ZXN0IHNvbHV0aW9uIGNvcHkgcGFzdGUgZnJvbSB0b3BcclxuICBvbkZpbHRlcigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQpIHtcclxuICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5maWx0ZXIpIHtcclxuICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKHRoaXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wYXJhbXMub25GbG9hdGluZ0ZpbHRlckNoYW5nZWQoeyBtb2RlbDogdGhpcy5idWlsZE1vZGVsKCkgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmNhbmNlbGVkVmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSB0aGlzLnByZXZWYWx1ZSB8fCB0aGlzLnByZXZWYWx1ZSA9PT0gMCA/IHRoaXMucHJldlZhbHVlIDogbnVsbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uUGFyZW50TW9kZWxDaGFuZ2VkKHBhcmVudE1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcik6IHZvaWQge1xyXG5cclxuICAgIC8vICoqKioqd2lsbCBiZSBkb25lIHdoZW4gbmVlZGVkKioqKipcclxuXHJcblxyXG4gICAgLy8gaWYgKCFwYXJlbnRNb2RlbCkge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHBhcmVudE1vZGVsLmZpbHRlcjtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgZGF0YS5JZCAmJiB0aGlzLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZCh0aGlzLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKCk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICdudW1iZXInLFxyXG4gICAgICB0eXBlOiB0aGlzLmZvcm1Hcm91cC5nZXQoJ251bWJlckZpbHRlclZhbHVlJykudmFsdWUsIC8vIGNhbiBiZSBzZW5kIGluIGhlcmVcclxuICAgICAgZmlsdGVyOiB0aGlzLmN1cnJlbnRWYWx1ZSB8fCAodGhpcy5jdXJyZW50VmFsdWUgPT09IDApID9cclxuICAgICAgICAodGhpcy5wYXJhbXMuY3VzdG9tRmlsdGVyRnVuY3Rpb24gPyB0aGlzLnBhcmFtcy5jdXN0b21GaWx0ZXJGdW5jdGlvbih0aGlzLmN1cnJlbnRWYWx1ZS50b1N0cmluZygpKSA6IHRoaXMuY3VycmVudFZhbHVlLnRvU3RyaW5nKCkpIDogbnVsbFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTogdm9pZCB7XHJcbiAgICBjb25zdCBrZXlMaXN0ID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5OdW1iZXIpLm1hcCh4ID0+IHguRGVmaW5pdGlvbik7XHJcblxyXG4gICAgY29uc3QgZGF0YVNvdXJjZSA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuTnVtYmVyKTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoa2V5TGlzdClcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xyXG4gICAgICAgIGtleUxpc3QuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdHJhbnNsYXRlZFZhbHVlID0gcmVzdWx0W2tleV07XHJcbiAgICAgICAgICBkYXRhU291cmNlW2NvdW50ZXJdLkRlZmluaXRpb24gPSB0cmFuc2xhdGVkVmFsdWU7XHJcbiAgICAgICAgICBjb3VudGVyKys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=