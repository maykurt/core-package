/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ComparisonList, ComparisonType, NumberComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class MaskFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
                this.maskType = this.settings.maskType;
                this.maxLength = this.settings.maxLength;
            }
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.serviceAccess && params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                        }
                        this.params.onFloatingFilterChanged({ model: this.buildModel() });
                        this.prevValue = this.currentValue;
                    }
                    else {
                        this.currentValue = this.prevValue || '';
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
MaskFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-mask-filter-cell',
                template: `
  <div class="input-group">
  <layout-form-mask-input style="width: 100%;"
    [maskType]="maskType"
    [(ngModel)]="currentValue"
    [clearable] = "true"
    placeholder="{{filterKey|translate}}"
    (ngModelChange)="valueChanged($event)">
  </layout-form-mask-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
MaskFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    MaskFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    MaskFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    MaskFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    MaskFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    MaskFilterCellComponent.prototype.settings;
    /** @type {?} */
    MaskFilterCellComponent.prototype.maskType;
    /** @type {?} */
    MaskFilterCellComponent.prototype.maxLength;
    /** @type {?} */
    MaskFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    MaskFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvbWFzay1maWx0ZXItY2VsbC9tYXNrLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUM3QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM3RyxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQTRCaEIsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7SUFvQmxDLFlBQW9CLEVBQWUsRUFBVSxTQUEyQjtRQUFwRCxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFqQmhFLGlCQUFZLEdBQW9CLElBQUksT0FBTyxFQUFVLENBQUM7UUFNOUQsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQU9kLGNBQVMsR0FBRyxZQUFZLENBQUM7UUFLOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUM3QixpQkFBaUIsRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO1NBQzVDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7YUFDckQsU0FBUyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLEVBQUU7WUFDbkQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQzVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2dCQUN6RSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO2dCQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO2FBQzFDO1NBQ0Y7UUFFRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQ3BELFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FDbkIsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFFMUIsSUFBSSxNQUFNLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3JFLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7cUJBQ3hDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO29CQUN6QixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7d0JBQ2hCLElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTs0QkFDdkQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3lCQUM1RTt3QkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7d0JBQ2xFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztxQkFDcEM7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQztxQkFDMUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsUUFBUTtRQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFdBQWlDO1FBRXBELHFDQUFxQztRQUdyQyxzQkFBc0I7UUFDdEIsOEJBQThCO1FBQzlCLFdBQVc7UUFDWCw0Q0FBNEM7UUFDNUMsSUFBSTtJQUNOLENBQUM7Ozs7O0lBR0QsT0FBTyxDQUFDLElBQVM7UUFDZixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLE9BQU87WUFDTCxVQUFVLEVBQUUsTUFBTTtZQUNsQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxLQUFLOztZQUNuRCxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVk7U0FDMUIsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCxzQkFBc0I7O2NBQ2QsT0FBTyxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQzs7Y0FFMUYsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1FBRTVFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2hCLE9BQU8sR0FBRyxDQUFDO1lBQ2YsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQ2QsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO2dCQUNqRCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMxQztJQUNILENBQUM7OztZQXRKRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1DQUFtQztnQkFDN0MsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FxQlQ7YUFDRjs7OztZQXJDbUIsV0FBVztZQUd0QixnQkFBZ0I7Ozs7Ozs7SUFxQ3ZCLHlDQUFxQzs7Ozs7SUFDckMsK0NBQThEOzs7OztJQUM5RCwyREFBK0M7O0lBRS9DLCtDQUE0Qjs7SUFDNUIsNENBQXlCOztJQUV6Qiw2Q0FBcUI7O0lBQ3JCLDRDQUFxQjs7SUFDckIsMkNBQThCOztJQUM5QiwyQ0FBMEI7O0lBQzFCLDRDQUEwQjs7SUFFMUIsOENBQTJCOztJQUMzQiw0Q0FBZ0M7Ozs7O0lBRWhDLHdEQUE0Qzs7Ozs7SUFFaEMscUNBQXVCOzs7OztJQUFFLDRDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFnRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgSUZsb2F0aW5nRmlsdGVyLCBJRmxvYXRpbmdGaWx0ZXJQYXJhbXMsIFNlcmlhbGl6ZWRUZXh0RmlsdGVyIH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHknO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBDb21wYXJpc29uTGlzdCwgQ29tcGFyaXNvblR5cGUsIE51bWJlckNvbXBhcmlzb24gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9lbnVtcy9jb21wYXJpc29uLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1hc2tUeXBlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvbWFzay10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBNYXNrU2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvbWFzay1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLW1hc2stZmlsdGVyLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgPGxheW91dC1mb3JtLW1hc2staW5wdXQgc3R5bGU9XCJ3aWR0aDogMTAwJTtcIlxyXG4gICAgW21hc2tUeXBlXT1cIm1hc2tUeXBlXCJcclxuICAgIFsobmdNb2RlbCldPVwiY3VycmVudFZhbHVlXCJcclxuICAgIFtjbGVhcmFibGVdID0gXCJ0cnVlXCJcclxuICAgIHBsYWNlaG9sZGVyPVwie3tmaWx0ZXJLZXl8dHJhbnNsYXRlfX1cIlxyXG4gICAgKG5nTW9kZWxDaGFuZ2UpPVwidmFsdWVDaGFuZ2VkKCRldmVudClcIj5cclxuICA8L2xheW91dC1mb3JtLW1hc2staW5wdXQ+XHJcblxyXG4gICAgPGRpdiBjbGFzcz1cImdyaWQtZmlsdGVyLWljb25cIj5cclxuICAgICAgPGNvcmUtaWNvbiAjYWN0dWFsVGFyZ2V0IGljb249XCJmaWx0ZXJcIiAoY2xpY2spPVwib3AudG9nZ2xlKCRldmVudClcIj48L2NvcmUtaWNvbj5cclxuICAgIDwvZGl2PlxyXG5cclxuICAgIDxwLW92ZXJsYXlQYW5lbCAjb3AgYXBwZW5kVG89XCJib2R5XCIgZGlzbWlzc2FibGU9XCJmYWxzZVwiPlxyXG4gICAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgPGxheW91dC1zdGF0aWMtc2VsZWN0b3IgZm9ybUNvbnRyb2xOYW1lPVwibnVtYmVyRmlsdGVyVmFsdWVcIiAoY2hhbmdlZCk9XCJjaGFuZ2VkKCRldmVudClcIiBbZGF0YVNvdXJjZV09XCJkYXRhU291cmNlXCIgW2NsZWFyYWJsZV09XCJmYWxzZVwiIFtzZWFyY2hhYmxlXT1cImZhbHNlXCI+XHJcbiAgICAgICAgPC9sYXlvdXQtc3RhdGljLXNlbGVjdG9yPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvcC1vdmVybGF5UGFuZWw+XHJcbiAgPC9kaXY+XHJcbiAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFza0ZpbHRlckNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJRmxvYXRpbmdGaWx0ZXI8U2VyaWFsaXplZFRleHRGaWx0ZXIsXHJcblRleHRGaWx0ZXJDaGFuZ2UsIEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgQWdGcmFtZXdvcmtDb21wb25lbnQ8RmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBPbkRlc3Ryb3kge1xyXG4gIHByaXZhdGUgcGFyYW1zOiBGbG9hdGluZ0ZpbHRlclBhcmFtcztcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZDogU3ViamVjdDxzdHJpbmc+ID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xyXG4gIHByaXZhdGUgbW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgcHJldlZhbHVlOiBzdHJpbmc7XHJcblxyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gIHB1YmxpYyBzZXR0aW5nczogTWFza1NldHRpbmdzO1xyXG4gIHB1YmxpYyBtYXNrVHlwZTogTWFza1R5cGU7XHJcbiAgcHVibGljIG1heExlbmd0aD86IG51bWJlcjtcclxuXHJcbiAgcHVibGljIHBsYWNlaG9sZGVyOiBzdHJpbmc7XHJcbiAgcHVibGljIGZpbHRlcktleSA9ICdHcmlkRmlsdGVyJztcclxuXHJcbiAgcHJpdmF0ZSB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBudW1iZXJGaWx0ZXJWYWx1ZTogTnVtYmVyQ29tcGFyaXNvbi5FcXVhbFRvXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gcGFyYW1zLmNvbHVtbi5jb2xEZWYuaGVhZGVyTmFtZSB8fCBwYXJhbXMuY29sdW1uLmNvbERlZi5maWVsZDtcclxuXHJcbiAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgICAgICB0aGlzLm1hc2tUeXBlID0gdGhpcy5zZXR0aW5ncy5tYXNrVHlwZTtcclxuICAgICAgICB0aGlzLm1heExlbmd0aCA9IHRoaXMuc2V0dGluZ3MubWF4TGVuZ3RoO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24gPSB0aGlzLm1vZGVsQ2hhbmdlZC5waXBlKFxyXG4gICAgICBkZWJvdW5jZVRpbWUoMTAwMClcclxuICAgICkuc3Vic2NyaWJlKG1vZGVsID0+IHtcclxuICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBtb2RlbDtcclxuXHJcbiAgICAgIGlmIChwYXJhbXMuc2VydmljZUFjY2VzcyAmJiBwYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCkge1xyXG4gICAgICAgIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcihwYXJhbXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSB0aGlzLnByZXZWYWx1ZSB8fCAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdmFsdWVDaGFuZ2VkKG5ld1ZhbHVlKTogdm9pZCB7XHJcbiAgICB0aGlzLm1vZGVsQ2hhbmdlZC5uZXh0KG5ld1ZhbHVlKTtcclxuICB9XHJcblxyXG4gIG9uUGFyZW50TW9kZWxDaGFuZ2VkKHBhcmVudE1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcik6IHZvaWQge1xyXG5cclxuICAgIC8vICoqKioqd2lsbCBiZSBkb25lIHdoZW4gbmVlZGVkKioqKipcclxuXHJcblxyXG4gICAgLy8gaWYgKCFwYXJlbnRNb2RlbCkge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHBhcmVudE1vZGVsLmZpbHRlcjtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgZGF0YS5JZCAmJiB0aGlzLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZCh0aGlzLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKCk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICd0ZXh0JyxcclxuICAgICAgdHlwZTogdGhpcy5mb3JtR3JvdXAuZ2V0KCdudW1iZXJGaWx0ZXJWYWx1ZScpLnZhbHVlLCAvLyBjYW4gYmUgc2VuZCBpbiBoZXJlXHJcbiAgICAgIGZpbHRlcjogdGhpcy5jdXJyZW50VmFsdWVcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuTnVtYmVyKS5tYXAoeCA9PiB4LkRlZmluaXRpb24pO1xyXG5cclxuICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLk51bWJlcik7XHJcblxyXG4gICAgdGhpcy50cmFuc2xhdGUuZ2V0KGtleUxpc3QpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBrZXlMaXN0LmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRWYWx1ZSA9IHJlc3VsdFtrZXldO1xyXG4gICAgICAgICAgZGF0YVNvdXJjZVtjb3VudGVyXS5EZWZpbml0aW9uID0gdHJhbnNsYXRlZFZhbHVlO1xyXG4gICAgICAgICAgY291bnRlcisrO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19