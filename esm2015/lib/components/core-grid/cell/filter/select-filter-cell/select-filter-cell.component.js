/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ComparisonList, ComparisonType, DefaultComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class SelectFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            this.settings = this.params.column.getColDef().cellEditorParams.settings;
            this.getListData();
        }
        this.dataSubscription = this.params.column.getColDef().cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.column.getColDef().cellEditorParams.dataSource) {
            this.listData = this.params.column['colDef'].cellEditorParams.dataSource;
            // this.clearSubscription();
        }
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.currentValue = newValue && newValue[this.settings.valueField] ? newValue[this.settings.valueField] : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.settings && ((/** @type {?} */ (this.settings))).sameFieldWith ?
                            ((/** @type {?} */ (this.settings))).sameFieldWith : this.params.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.currentValue = null;
                    this.formGroup.patchValue({
                        filterValue: this.prevValue || (this.prevValue && parseInt(this.prevValue, 10) === 0) ? this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            /** @type {?} */
            const newValue = {};
            newValue[this.settings.valueField] = this.currentValue;
            this.valueChanged(newValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    clearSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearSubscription();
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
SelectFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-filter-cell',
                template: `
    <div class="input-group">
      <div *ngIf="settings" [formGroup]="formGroup" style="width:100%;    padding-top: 4px;">
        <layout-static-selector
          #selector
          formControlName="filterValue"
          (changed)="valueChanged($event)"
          [dataSource]="listData"
          placeholder="{{filterKey|translate}}"
          [valueField]="this.settings.valueField"
          [labelField]="this.settings.labelField"
        >
        </layout-static-selector>
      </div>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="selectorFormGroup" [formGroup]="selectorFormGroup" style="width:100%;">
          <layout-static-selector formControlName="selectFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>

  `,
                styles: [":host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container .ng-placeholder{font-weight:400;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#6c7591}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
            }] }
];
/** @nocollapse */
SelectFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
if (false) {
    /** @type {?} */
    SelectFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    SelectFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    SelectFilterCellComponent.prototype.listData;
    /** @type {?} */
    SelectFilterCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.params;
    /** @type {?} */
    SelectFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    SelectFilterCellComponent.prototype.selectorFormGroup;
    /** @type {?} */
    SelectFilterCellComponent.prototype.formGroup;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.dataSubscription;
    /** @type {?} */
    SelectFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    SelectFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2ZpbHRlci9zZWxlY3QtZmlsdGVyLWNlbGwvc2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRzlHLE9BQU8sRUFBYSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUV2RCxzQ0FFQzs7O0lBREMsaUNBQTRCOzs7OztBQUc5QiwwQ0FFQzs7O0lBREMscUNBQWM7O0FBbUNoQixNQUFNLE9BQU8seUJBQXlCOzs7OztJQW9CcEMsWUFBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQVZ4RSxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBTWQsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUs5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLFdBQVcsRUFBRSxJQUFJO1NBQ2xCLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNyQyxpQkFBaUIsRUFBRSxpQkFBaUIsQ0FBQyxPQUFPO1NBQzdDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7YUFDckQsU0FBUyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLEVBQUU7WUFDbkQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDekUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BCO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDekYsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRTtZQUM5RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztZQUN6RSw0QkFBNEI7U0FDN0I7SUFDSCxDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxRQUFRO1FBQ25CLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQy9HLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO2dCQUN6QixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQ2hCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO3dCQUNqRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FDNUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLG1CQUFrQixJQUFJLENBQUMsUUFBUSxFQUFBLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQzs0QkFDaEUsQ0FBQyxtQkFBa0IsSUFBSSxDQUFDLFFBQVEsRUFBQSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztxQkFDN0Y7b0JBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUVsRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7aUJBQ3BDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO29CQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3QkFDeEIsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJO3FCQUM5RyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDcEM7SUFFSCxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFdBQWlDO1FBQ3BELHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7O2tCQUNyRCxRQUFRLEdBQUcsRUFBRTtZQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDN0I7SUFDSCxDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLE9BQU87WUFDTCxVQUFVLEVBQUUsTUFBTTtZQUNsQixJQUFJLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEtBQUs7O1lBQzNELE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWTtTQUMxQixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNmLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyQztJQUNILENBQUM7Ozs7SUFFRCxzQkFBc0I7O2NBQ2QsT0FBTyxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQzs7Y0FFM0YsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDO1FBRTdFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2hCLE9BQU8sR0FBRyxDQUFDO1lBQ2YsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQ2QsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO2dCQUNqRCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMxQztJQUNILENBQUM7OztZQTlLRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHFDQUFxQztnQkFDL0MsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0EyQlQ7O2FBRUY7Ozs7WUEzQ21CLFdBQVc7WUFFdEIsZ0JBQWdCOzs7O0lBNkN2QixpREFBNEI7O0lBQzVCLDhDQUF5Qjs7SUFFekIsNkNBQXFCOztJQUNyQiw2Q0FBZ0Q7Ozs7O0lBRWhELDJDQUFvQjs7SUFDcEIsK0NBQXFCOztJQUNyQixzREFBNkI7O0lBQzdCLDhDQUFxQjs7Ozs7SUFDckIscURBQXVDOztJQUV2QyxnREFBMkI7O0lBQzNCLDhDQUFnQzs7Ozs7SUFFaEMsMERBQTRDOzs7OztJQUVoQyx1Q0FBdUI7Ozs7O0lBQUUsOENBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWdGcmFtZXdvcmtDb21wb25lbnQgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBJRmxvYXRpbmdGaWx0ZXIsIElGbG9hdGluZ0ZpbHRlclBhcmFtcywgU2VyaWFsaXplZFRleHRGaWx0ZXIgfSBmcm9tICdhZy1ncmlkLWNvbW11bml0eSc7XHJcbmltcG9ydCB7IENvbXBhcmlzb25MaXN0LCBDb21wYXJpc29uVHlwZSwgRGVmYXVsdENvbXBhcmlzb24gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9lbnVtcy9jb21wYXJpc29uLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IExvdlNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL2xvdi1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IFNlbGVjdG9yU2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvc2VsZWN0b3Itc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBUZXh0RmlsdGVyQ2hhbmdlIHtcclxuICBtb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmxvYXRpbmdGaWx0ZXJQYXJhbXMgZXh0ZW5kcyBJRmxvYXRpbmdGaWx0ZXJQYXJhbXM8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2U+IHtcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtc2VsZWN0LWZpbHRlci1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgIDxkaXYgKm5nSWY9XCJzZXR0aW5nc1wiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlOyAgICBwYWRkaW5nLXRvcDogNHB4O1wiPlxyXG4gICAgICAgIDxsYXlvdXQtc3RhdGljLXNlbGVjdG9yXHJcbiAgICAgICAgICAjc2VsZWN0b3JcclxuICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cImZpbHRlclZhbHVlXCJcclxuICAgICAgICAgIChjaGFuZ2VkKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCJcclxuICAgICAgICAgIFtkYXRhU291cmNlXT1cImxpc3REYXRhXCJcclxuICAgICAgICAgIHBsYWNlaG9sZGVyPVwie3tmaWx0ZXJLZXl8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgICAgW3ZhbHVlRmllbGRdPVwidGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXCJcclxuICAgICAgICAgIFtsYWJlbEZpZWxkXT1cInRoaXMuc2V0dGluZ3MubGFiZWxGaWVsZFwiXHJcbiAgICAgICAgPlxyXG4gICAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZ3JpZC1maWx0ZXItaWNvblwiPlxyXG4gICAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPHAtb3ZlcmxheVBhbmVsICNvcCBhcHBlbmRUbz1cImJvZHlcIiBkaXNtaXNzYWJsZT1cImZhbHNlXCI+XHJcbiAgICAgICAgPGRpdiAqbmdJZj1cInNlbGVjdG9yRm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJzZWxlY3RvckZvcm1Hcm91cFwiIHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgICAgICAgIDxsYXlvdXQtc3RhdGljLXNlbGVjdG9yIGZvcm1Db250cm9sTmFtZT1cInNlbGVjdEZpbHRlclZhbHVlXCIgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCIgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFtjbGVhcmFibGVdPVwiZmFsc2VcIiBbc2VhcmNoYWJsZV09XCJmYWxzZVwiPlxyXG4gICAgICAgICAgPC9sYXlvdXQtc3RhdGljLXNlbGVjdG9yPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L3Atb3ZlcmxheVBhbmVsPlxyXG4gICAgPC9kaXY+XHJcblxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdEZpbHRlckNlbGxDb21wb25lbnRcclxuICBpbXBsZW1lbnRzIElGbG9hdGluZ0ZpbHRlcjxTZXJpYWxpemVkVGV4dEZpbHRlciwgVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcblxyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgcHJldlZhbHVlOiBzdHJpbmc7XHJcblxyXG4gIHB1YmxpYyBsaXN0RGF0YTogYW55O1xyXG4gIHB1YmxpYyBzZXR0aW5nczogTG92U2V0dGluZ3MgfCBTZWxlY3RvclNldHRpbmdzO1xyXG5cclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIHNlbGVjdG9yRm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcbiAgZm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcbiAgcHJpdmF0ZSBkYXRhU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHB1YmxpYyBwbGFjZWhvbGRlcjogc3RyaW5nO1xyXG4gIHB1YmxpYyBmaWx0ZXJLZXkgPSAnR3JpZEZpbHRlcic7XHJcblxyXG4gIHByaXZhdGUgdHJhbnNsYXRlU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgZmlsdGVyVmFsdWU6IG51bGxcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc2VsZWN0b3JGb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgc2VsZWN0RmlsdGVyVmFsdWU6IERlZmF1bHRDb21wYXJpc29uLkVxdWFsVG9cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuXHJcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gcGFyYW1zLmNvbHVtbi5jb2xEZWYuaGVhZGVyTmFtZSB8fCBwYXJhbXMuY29sdW1uLmNvbERlZi5maWVsZDtcclxuXHJcbiAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgICAgdGhpcy5nZXRMaXN0RGF0YSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZGF0YVN1YnNjcmlwdGlvbiA9IHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5jZWxsRWRpdG9yUGFyYW1zLmRhdGEuc3Vic2NyaWJlKHggPT4ge1xyXG4gICAgICB0aGlzLmdldExpc3REYXRhKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG5cclxuICBnZXRMaXN0RGF0YSgpIHtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKSB7XHJcbiAgICAgIHRoaXMubGlzdERhdGEgPSB0aGlzLnBhcmFtcy5jb2x1bW5bJ2NvbERlZiddLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZTtcclxuICAgICAgLy8gdGhpcy5jbGVhclN1YnNjcmlwdGlvbigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdmFsdWVDaGFuZ2VkKG5ld1ZhbHVlKTogdm9pZCB7XHJcbiAgICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG5ld1ZhbHVlICYmIG5ld1ZhbHVlW3RoaXMuc2V0dGluZ3MudmFsdWVGaWVsZF0gPyBuZXdWYWx1ZVt0aGlzLnNldHRpbmdzLnZhbHVlRmllbGRdIDogbnVsbDtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQpIHtcclxuICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5maWx0ZXIpIHtcclxuICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXR0aW5ncyAmJiAoPFNlbGVjdG9yU2V0dGluZ3M+dGhpcy5zZXR0aW5ncykuc2FtZUZpZWxkV2l0aCA/XHJcbiAgICAgICAgICAgICAgICAgICg8U2VsZWN0b3JTZXR0aW5ncz50aGlzLnNldHRpbmdzKS5zYW1lRmllbGRXaXRoIDogdGhpcy5wYXJhbXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wYXJhbXMub25GbG9hdGluZ0ZpbHRlckNoYW5nZWQoeyBtb2RlbDogdGhpcy5idWlsZE1vZGVsKCkgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnByZXZWYWx1ZSA9IHRoaXMuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1Hcm91cC5wYXRjaFZhbHVlKHtcclxuICAgICAgICAgICAgICBmaWx0ZXJWYWx1ZTogdGhpcy5wcmV2VmFsdWUgfHwgKHRoaXMucHJldlZhbHVlICYmIHBhcnNlSW50KHRoaXMucHJldlZhbHVlLCAxMCkgPT09IDApID8gdGhpcy5wcmV2VmFsdWUgOiBudWxsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuICAgIC8vIGlmICghcGFyZW50TW9kZWwpIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBwYXJlbnRNb2RlbC5maWx0ZXI7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgKGRhdGEuSWQgfHwgZGF0YS5JZCA9PT0gMCkgJiYgdGhpcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgY29uc3QgbmV3VmFsdWUgPSB7fTtcclxuICAgICAgbmV3VmFsdWVbdGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXSA9IHRoaXMuY3VycmVudFZhbHVlO1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKCk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICd0ZXh0JyxcclxuICAgICAgdHlwZTogdGhpcy5zZWxlY3RvckZvcm1Hcm91cC5nZXQoJ3NlbGVjdEZpbHRlclZhbHVlJykudmFsdWUsIC8vIGNhbiBiZSBzZW5kIGluIGhlcmVcclxuICAgICAgZmlsdGVyOiB0aGlzLmN1cnJlbnRWYWx1ZVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGNsZWFyU3Vic2NyaXB0aW9uKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZGF0YVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmRhdGFTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTogdm9pZCB7XHJcbiAgICBjb25zdCBrZXlMaXN0ID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5EZWZhdWx0KS5tYXAoeCA9PiB4LkRlZmluaXRpb24pO1xyXG5cclxuICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLkRlZmF1bHQpO1xyXG5cclxuICAgIHRoaXMudHJhbnNsYXRlLmdldChrZXlMaXN0KVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcclxuICAgICAgICBsZXQgY291bnRlciA9IDA7XHJcbiAgICAgICAga2V5TGlzdC5mb3JFYWNoKGtleSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0cmFuc2xhdGVkVmFsdWUgPSByZXN1bHRba2V5XTtcclxuICAgICAgICAgIGRhdGFTb3VyY2VbY291bnRlcl0uRGVmaW5pdGlvbiA9IHRyYW5zbGF0ZWRWYWx1ZTtcclxuICAgICAgICAgIGNvdW50ZXIrKztcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbGVhclN1YnNjcmlwdGlvbigpO1xyXG5cclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=