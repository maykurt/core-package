/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { ComparisonList, ComparisonType, DefaultComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { ListOfValuesComponent } from '../../../../list-of-values/list-of-values.component';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class LovFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.hideFilter = this.params.hideFilter;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
            }
            else {
                console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
        }
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.settings) {
                this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField);
            }
        }
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.currentValue = newValue && newValue.id ? newValue.id : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.params.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.currentValue = null;
                    this.formGroup.patchValue({
                        filterValue: this.prevValue || (this.prevValue && parseInt(this.prevValue, 10) === 0) ? this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
LovFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-lov-filter-cell',
                template: `
    <div [hidden]="hideFilter" class="input-group">
      <div [formGroup]="formGroup" style="width: calc(100%);">
        <layout-list-of-values #listOfValue
        [id]="key"
        formControlName="filterValue"
        placeholder="{{filterKey|translate}}"
        (lovValueChanged)="valueChanged($event)"></layout-list-of-values>
      </div>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="selectorFormGroup" [formGroup]="selectorFormGroup" style="width:100%;">
          <layout-static-selector
          formControlName="selectFilterValue"
          (changed)="changed($event)"
          [dataSource]="dataSource"
          placeholder="{{filterKey|translate}}"
          [clearable]="false"
          [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>

  `,
                styles: [":host /deep/ .form-lov .btn-info{background-color:#175169!important;border-color:#175169!important}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
            }] }
];
/** @nocollapse */
LovFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
LovFilterCellComponent.propDecorators = {
    listOfValue: [{ type: ViewChild, args: ['listOfValue',] }]
};
if (false) {
    /** @type {?} */
    LovFilterCellComponent.prototype.listOfValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.key;
    /** @type {?} */
    LovFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.params;
    /** @type {?} */
    LovFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    LovFilterCellComponent.prototype.selectorFormGroup;
    /** @type {?} */
    LovFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    LovFilterCellComponent.prototype.filterKey;
    /** @type {?} */
    LovFilterCellComponent.prototype.hideFilter;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWZpbHRlci1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2ZpbHRlci9sb3YtZmlsdGVyLWNlbGwvbG92LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFHaEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RyxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFHNUYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQW9DaEIsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUFzQmpDLFlBQW9CLEVBQWUsRUFBVSxTQUEyQjtRQUFwRCxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFWeEUsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQUlkLGNBQVMsR0FBRyxZQUFZLENBQUM7UUFPOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUM3QixXQUFXLEVBQUUsSUFBSTtTQUNsQixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDckMsaUJBQWlCLEVBQUUsaUJBQWlCLENBQUMsT0FBTztTQUM3QyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFFekMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUNyRCxTQUFTLENBQUMsQ0FBQyxLQUFVLEVBQUUsRUFBRTtZQUN4QixJQUFJLEtBQUssRUFBRTtnQkFDVCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMvQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDNUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7YUFDMUU7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLEdBQUcsNEJBQTRCLENBQUMsQ0FBQzthQUNwRjtTQUNGO2FBQU07WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssR0FBRyw0QkFBNEIsQ0FBQyxDQUFDO1NBQ3BGO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDeEc7U0FDRjtJQUNILENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLFFBQVE7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLElBQUksUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBRWpFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO2dCQUN6QixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQ2hCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO3dCQUNqRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztxQkFDdEY7b0JBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUNsRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7aUJBQ3BDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO29CQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3QkFDeEIsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJO3FCQUM5RyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFdBQWlDO1FBQ3BELHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLE9BQU87WUFDTCxVQUFVLEVBQUUsTUFBTTtZQUNsQixJQUFJLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEtBQUs7O1lBQzNELE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWTtTQUMxQixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELHNCQUFzQjs7Y0FDZCxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDOztjQUUzRixVQUFVLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUM7UUFFN0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQ3hCLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFOztnQkFDaEIsT0FBTyxHQUFHLENBQUM7WUFDZixPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztzQkFDZCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFHRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7O1lBbktGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0NBQWtDO2dCQUM1QyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0E0QlQ7O2FBRUY7Ozs7WUE5Q21CLFdBQVc7WUFJdEIsZ0JBQWdCOzs7MEJBOEN0QixTQUFTLFNBQUMsYUFBYTs7OztJQUF4Qiw2Q0FBNkQ7O0lBRTdELHFDQUFXOztJQUNYLDhDQUE0Qjs7SUFDNUIsMkNBQXlCOztJQUN6QiwwQ0FBcUI7Ozs7O0lBQ3JCLDBDQUE4Qjs7Ozs7SUFFOUIsd0NBQW9COztJQUNwQiw0Q0FBcUI7O0lBQ3JCLG1EQUE2Qjs7SUFDN0IsMkNBQXFCOztJQUVyQiwyQ0FBZ0M7O0lBRWhDLDRDQUFvQjs7Ozs7SUFFcEIsdURBQTRDOzs7OztJQUVoQyxvQ0FBdUI7Ozs7O0lBQUUsMkNBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgQ29tcGFyaXNvbkxpc3QsIENvbXBhcmlzb25UeXBlLCBEZWZhdWx0Q29tcGFyaXNvbiB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2NvbXBhcmlzb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTGlzdE9mVmFsdWVzQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vLi4vbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG92U2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvbG92LXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWxvdi1maWx0ZXItY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgW2hpZGRlbl09XCJoaWRlRmlsdGVyXCIgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICA8ZGl2IFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJ3aWR0aDogY2FsYygxMDAlKTtcIj5cclxuICAgICAgICA8bGF5b3V0LWxpc3Qtb2YtdmFsdWVzICNsaXN0T2ZWYWx1ZVxyXG4gICAgICAgIFtpZF09XCJrZXlcIlxyXG4gICAgICAgIGZvcm1Db250cm9sTmFtZT1cImZpbHRlclZhbHVlXCJcclxuICAgICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAobG92VmFsdWVDaGFuZ2VkKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCI+PC9sYXlvdXQtbGlzdC1vZi12YWx1ZXM+XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPGRpdiBjbGFzcz1cImdyaWQtZmlsdGVyLWljb25cIj5cclxuICAgICAgICA8Y29yZS1pY29uICNhY3R1YWxUYXJnZXQgaWNvbj1cImZpbHRlclwiIChjbGljayk9XCJvcC50b2dnbGUoJGV2ZW50KVwiPjwvY29yZS1pY29uPlxyXG4gICAgICA8L2Rpdj5cclxuXHJcbiAgICAgIDxwLW92ZXJsYXlQYW5lbCAjb3AgYXBwZW5kVG89XCJib2R5XCIgZGlzbWlzc2FibGU9XCJmYWxzZVwiPlxyXG4gICAgICAgIDxkaXYgKm5nSWY9XCJzZWxlY3RvckZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwic2VsZWN0b3JGb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvclxyXG4gICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwic2VsZWN0RmlsdGVyVmFsdWVcIlxyXG4gICAgICAgICAgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCJcclxuICAgICAgICAgIFtkYXRhU291cmNlXT1cImRhdGFTb3VyY2VcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7e2ZpbHRlcktleXx0cmFuc2xhdGV9fVwiXHJcbiAgICAgICAgICBbY2xlYXJhYmxlXT1cImZhbHNlXCJcclxuICAgICAgICAgIFtzZWFyY2hhYmxlXT1cImZhbHNlXCI+XHJcbiAgICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvcC1vdmVybGF5UGFuZWw+XHJcbiAgICA8L2Rpdj5cclxuXHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9sb3YtZmlsdGVyLWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG92RmlsdGVyQ2VsbENvbXBvbmVudFxyXG4gIGltcGxlbWVudHMgSUZsb2F0aW5nRmlsdGVyPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLFxyXG4gIFRleHRGaWx0ZXJDaGFuZ2UsIEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgQWdGcmFtZXdvcmtDb21wb25lbnQ8RmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBPbkRlc3Ryb3kge1xyXG4gIEBWaWV3Q2hpbGQoJ2xpc3RPZlZhbHVlJykgbGlzdE9mVmFsdWU6IExpc3RPZlZhbHVlc0NvbXBvbmVudDtcclxuXHJcbiAgcHVibGljIGtleTtcclxuICBwdWJsaWMgY3VycmVudFZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIHByZXZWYWx1ZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBsaXN0RGF0YTogYW55O1xyXG4gIHByaXZhdGUgc2V0dGluZ3M6IExvdlNldHRpbmdzO1xyXG5cclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIHNlbGVjdG9yRm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcbiAgZm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcblxyXG4gIHB1YmxpYyBmaWx0ZXJLZXkgPSAnR3JpZEZpbHRlcic7XHJcblxyXG4gIGhpZGVGaWx0ZXI6IGJvb2xlYW47XHJcblxyXG4gIHByaXZhdGUgdHJhbnNsYXRlU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgZmlsdGVyVmFsdWU6IG51bGxcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc2VsZWN0b3JGb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgc2VsZWN0RmlsdGVyVmFsdWU6IERlZmF1bHRDb21wYXJpc29uLkVxdWFsVG9cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuaGlkZUZpbHRlciA9IHRoaXMucGFyYW1zLmhpZGVGaWx0ZXI7XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuZmllbGQgKyAnbG92IFNldHRpbmcgaXMgbm90IGRlZmluZWQnKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuZmllbGQgKyAnbG92IFNldHRpbmcgaXMgbm90IGRlZmluZWQnKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLmxpc3RPZlZhbHVlLnNldE9wdGlvbnModGhpcy5zZXR0aW5ncy5vcHRpb25zLCB0aGlzLnNldHRpbmdzLnZhbHVlRmllbGQsIHRoaXMuc2V0dGluZ3MubGFiZWxGaWVsZCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk6IHZvaWQge1xyXG4gICAgdGhpcy5jdXJyZW50VmFsdWUgPSBuZXdWYWx1ZSAmJiBuZXdWYWx1ZS5pZCA/IG5ld1ZhbHVlLmlkIDogbnVsbDtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuc2VydmljZUFjY2VzcyAmJiB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KSB7XHJcbiAgICAgIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcih0aGlzLnBhcmFtcy5maWVsZCwgdGhpcy5idWlsZE1vZGVsKCkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnBhcmFtcy5vbkZsb2F0aW5nRmlsdGVyQ2hhbmdlZCh7IG1vZGVsOiB0aGlzLmJ1aWxkTW9kZWwoKSB9KTtcclxuICAgICAgICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5mb3JtR3JvdXAucGF0Y2hWYWx1ZSh7XHJcbiAgICAgICAgICAgICAgZmlsdGVyVmFsdWU6IHRoaXMucHJldlZhbHVlIHx8ICh0aGlzLnByZXZWYWx1ZSAmJiBwYXJzZUludCh0aGlzLnByZXZWYWx1ZSwgMTApID09PSAwKSA/IHRoaXMucHJldlZhbHVlIDogbnVsbFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuICAgIC8vIGlmICghcGFyZW50TW9kZWwpIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBwYXJlbnRNb2RlbC5maWx0ZXI7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgKGRhdGEuSWQgfHwgZGF0YS5JZCA9PT0gMCkgJiYgdGhpcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQodGhpcy5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYnVpbGRNb2RlbCgpOiBTZXJpYWxpemVkVGV4dEZpbHRlciB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmaWx0ZXJUeXBlOiAndGV4dCcsXHJcbiAgICAgIHR5cGU6IHRoaXMuc2VsZWN0b3JGb3JtR3JvdXAuZ2V0KCdzZWxlY3RGaWx0ZXJWYWx1ZScpLnZhbHVlLCAvLyBjYW4gYmUgc2VuZCBpbiBoZXJlXHJcbiAgICAgIGZpbHRlcjogdGhpcy5jdXJyZW50VmFsdWVcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuRGVmYXVsdCkubWFwKHggPT4geC5EZWZpbml0aW9uKTtcclxuXHJcbiAgICBjb25zdCBkYXRhU291cmNlID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5EZWZhdWx0KTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoa2V5TGlzdClcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xyXG4gICAgICAgIGtleUxpc3QuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdHJhbnNsYXRlZFZhbHVlID0gcmVzdWx0W2tleV07XHJcbiAgICAgICAgICBkYXRhU291cmNlW2NvdW50ZXJdLkRlZmluaXRpb24gPSB0cmFuc2xhdGVkVmFsdWU7XHJcbiAgICAgICAgICBjb3VudGVyKys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==