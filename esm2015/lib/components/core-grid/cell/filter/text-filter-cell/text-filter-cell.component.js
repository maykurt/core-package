/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ComparisonList, ComparisonType, TextComparison } from '../../../../../enums/comparison-type.enum';
import { CoreGridColumnType } from '../../../../../enums/index';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class TextFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            textFilterValue: TextComparison.Contains
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        // this.dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.column.colDef.columnType === CoreGridColumnType.TextSelect) {
                            if (params.serviceAccess) {
                                params.serviceAccess.textSelectFindValuesOnFilter(params.field, params.column.colDef.cellEditorParams.settings, this.buildModel());
                            }
                        }
                        else {
                            if (params.serviceAccess && params.serviceAccess.filter) {
                                params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                            }
                            this.params.onFloatingFilterChanged({ model: this.buildModel() });
                            this.prevValue = this.currentValue;
                        }
                    }
                    else {
                        this.currentValue = this.prevValue || null;
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        /** @type {?} */
        let value;
        /** @type {?} */
        const type = this.formGroup.get('textFilterValue').value;
        if (this.currentValue) {
            value = this.currentValue;
        }
        else if (type === TextComparison.IsNullOrWhiteSpace ||
            type === TextComparison.IsNotNullNorWhiteSpace ||
            type === TextComparison.IsNull ||
            type === TextComparison.IsNotNull ||
            type === TextComparison.IsNotEmpty ||
            type === TextComparison.IsEmpty) {
            value = ' ';
        }
        return {
            filterType: 'string',
            type: type.toString(),
            filter: value
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Text).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
TextFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-text-filter-cell',
                template: `
    <div class="input-group">
      <layout-form-text-input style="width: 100%;"
          [(ngModel)]="currentValue"
          [clearable] = "true"
          placeholder="{{filterKey|translate}}"
          (ngModelChange)="valueChanged($event)">
      </layout-form-text-input>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
          <layout-static-selector formControlName="textFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>
  `
            }] }
];
/** @nocollapse */
TextFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    TextFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    TextFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    TextFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    TextFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    TextFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    TextFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvdGV4dC1maWx0ZXItY2VsbC90ZXh0LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUc5QyxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUMzRyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVoRSxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQTJCaEIsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7SUFpQmxDLFlBQW9CLEVBQWUsRUFBVSxTQUEyQjtRQUFwRCxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFiaEUsaUJBQVksR0FBb0IsSUFBSSxPQUFPLEVBQVUsQ0FBQztRQUs5RCxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBSWQsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUs5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLGVBQWUsRUFBRSxjQUFjLENBQUMsUUFBUTtTQUN6QyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUVqRixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUU5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2FBQ3JELFNBQVMsQ0FBQyxDQUFDLEtBQVUsRUFBRSxFQUFFO1lBQ3hCLElBQUksS0FBSyxFQUFFO2dCQUNULElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCw2RUFBNkU7UUFFN0UsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNwRCxZQUFZLENBQUMsSUFBSSxDQUFDLENBQ25CLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBRTFCLElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixFQUFFO2dCQUNyRSxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixFQUFFO3FCQUN4QyxTQUFTLENBQUMsQ0FBQyxNQUFXLEVBQUUsRUFBRTtvQkFDekIsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO3dCQUVoQixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUU7NEJBQ3JFLElBQUksTUFBTSxDQUFDLGFBQWEsRUFBRTtnQ0FDeEIsTUFBTSxDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUM1RCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7NkJBQ3RFO3lCQUVGOzZCQUFNOzRCQUNMLElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTtnQ0FDdkQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDOzZCQUM1RTs0QkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7NEJBQ2xFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQzt5QkFDcEM7cUJBQ0Y7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQztxQkFDNUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsUUFBUTtRQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFdBQWlDO1FBRXBELHFDQUFxQztRQUdyQyxzQkFBc0I7UUFDdEIsOEJBQThCO1FBQzlCLFdBQVc7UUFDWCw0Q0FBNEM7UUFDNUMsSUFBSTtJQUNOLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLElBQVM7UUFDZixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQzs7OztJQUVELFVBQVU7O1lBQ0osS0FBYTs7Y0FDWCxJQUFJLEdBQVcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxLQUFLO1FBQ2hFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUMzQjthQUFNLElBQUksSUFBSSxLQUFLLGNBQWMsQ0FBQyxrQkFBa0I7WUFDbkQsSUFBSSxLQUFLLGNBQWMsQ0FBQyxzQkFBc0I7WUFDOUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxNQUFNO1lBQzlCLElBQUksS0FBSyxjQUFjLENBQUMsU0FBUztZQUNqQyxJQUFJLEtBQUssY0FBYyxDQUFDLFVBQVU7WUFDbEMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDakMsS0FBSyxHQUFHLEdBQUcsQ0FBQztTQUNiO1FBRUQsT0FBTztZQUNMLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ3JCLE1BQU0sRUFBRSxLQUFLO1NBQ2QsQ0FBQztJQUNKLENBQUM7Ozs7SUFHRCxzQkFBc0I7O2NBQ2QsT0FBTyxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQzs7Y0FFeEYsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1FBRTFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2hCLE9BQU8sR0FBRyxDQUFDO1lBQ2YsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQ2QsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO2dCQUNqRCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMxQztJQUNILENBQUM7OztZQXBLRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1DQUFtQztnQkFDN0MsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW9CVDthQUNGOzs7O1lBcENtQixXQUFXO1lBR3RCLGdCQUFnQjs7Ozs7OztJQXFDdkIseUNBQW9COzs7OztJQUNwQiwrQ0FBOEQ7Ozs7O0lBQzlELDJEQUErQzs7SUFFL0MsK0NBQTRCOztJQUM1Qiw0Q0FBeUI7O0lBQ3pCLDZDQUFxQjs7SUFDckIsNENBQXFCOztJQUVyQiw4Q0FBMkI7O0lBQzNCLDRDQUFnQzs7Ozs7SUFFaEMsd0RBQTRDOzs7OztJQUVoQyxxQ0FBdUI7Ozs7O0lBQUUsNENBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWdGcmFtZXdvcmtDb21wb25lbnQgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBJRmxvYXRpbmdGaWx0ZXIsIElGbG9hdGluZ0ZpbHRlclBhcmFtcywgU2VyaWFsaXplZFRleHRGaWx0ZXIgfSBmcm9tICdhZy1ncmlkLWNvbW11bml0eSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IENvbXBhcmlzb25MaXN0LCBDb21wYXJpc29uVHlwZSwgVGV4dENvbXBhcmlzb24gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9lbnVtcy9jb21wYXJpc29uLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IENvcmVHcmlkQ29sdW1uVHlwZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2luZGV4JztcclxuXHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5pbXBvcnQgeyBUcmFuc2xhdGVIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvY3VzdG9tL3RyYW5zbGF0ZS5oZWxwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLXRleHQtZmlsdGVyLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj5cclxuICAgICAgPGxheW91dC1mb3JtLXRleHQtaW5wdXQgc3R5bGU9XCJ3aWR0aDogMTAwJTtcIlxyXG4gICAgICAgICAgWyhuZ01vZGVsKV09XCJjdXJyZW50VmFsdWVcIlxyXG4gICAgICAgICAgW2NsZWFyYWJsZV0gPSBcInRydWVcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7e2ZpbHRlcktleXx0cmFuc2xhdGV9fVwiXHJcbiAgICAgICAgICAobmdNb2RlbENoYW5nZSk9XCJ2YWx1ZUNoYW5nZWQoJGV2ZW50KVwiPlxyXG4gICAgICA8L2xheW91dC1mb3JtLXRleHQtaW5wdXQ+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZ3JpZC1maWx0ZXItaWNvblwiPlxyXG4gICAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPHAtb3ZlcmxheVBhbmVsICNvcCBhcHBlbmRUbz1cImJvZHlcIiBkaXNtaXNzYWJsZT1cImZhbHNlXCI+XHJcbiAgICAgICAgPGRpdiAqbmdJZj1cImZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgICAgICAgPGxheW91dC1zdGF0aWMtc2VsZWN0b3IgZm9ybUNvbnRyb2xOYW1lPVwidGV4dEZpbHRlclZhbHVlXCIgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCIgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFtjbGVhcmFibGVdPVwiZmFsc2VcIiBbc2VhcmNoYWJsZV09XCJmYWxzZVwiPlxyXG4gICAgICAgICAgPC9sYXlvdXQtc3RhdGljLXNlbGVjdG9yPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L3Atb3ZlcmxheVBhbmVsPlxyXG4gICAgPC9kaXY+XHJcbiAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGV4dEZpbHRlckNlbGxDb21wb25lbnRcclxuICBpbXBsZW1lbnRzIElGbG9hdGluZ0ZpbHRlcjxTZXJpYWxpemVkVGV4dEZpbHRlciwgVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcblxyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHJpdmF0ZSBtb2RlbENoYW5nZWQ6IFN1YmplY3Q8c3RyaW5nPiA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwdWJsaWMgY3VycmVudFZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIHByZXZWYWx1ZTogc3RyaW5nO1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG5cclxuICBwdWJsaWMgcGxhY2Vob2xkZXI6IHN0cmluZztcclxuICBwdWJsaWMgZmlsdGVyS2V5ID0gJ0dyaWRGaWx0ZXInO1xyXG5cclxuICBwcml2YXRlIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgIHRleHRGaWx0ZXJWYWx1ZTogVGV4dENvbXBhcmlzb24uQ29udGFpbnNcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuXHJcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gcGFyYW1zLmNvbHVtbi5jb2xEZWYuaGVhZGVyTmFtZSB8fCBwYXJhbXMuY29sdW1uLmNvbERlZi5maWVsZDtcclxuXHJcbiAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIHRoaXMuZGF0YVNvdXJjZSA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuVGV4dCk7XHJcblxyXG4gICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24gPSB0aGlzLm1vZGVsQ2hhbmdlZC5waXBlKFxyXG4gICAgICBkZWJvdW5jZVRpbWUoMTAwMClcclxuICAgICkuc3Vic2NyaWJlKG1vZGVsID0+IHtcclxuICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBtb2RlbDtcclxuXHJcbiAgICAgIGlmIChwYXJhbXMuc2VydmljZUFjY2VzcyAmJiBwYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCkge1xyXG4gICAgICAgIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuXHJcbiAgICAgICAgICAgICAgaWYgKHBhcmFtcy5jb2x1bW4uY29sRGVmLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0U2VsZWN0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MpIHtcclxuICAgICAgICAgICAgICAgICAgcGFyYW1zLnNlcnZpY2VBY2Nlc3MudGV4dFNlbGVjdEZpbmRWYWx1ZXNPbkZpbHRlcihwYXJhbXMuZmllbGQsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLmNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncywgdGhpcy5idWlsZE1vZGVsKCkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKHBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgICBwYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcihwYXJhbXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5vbkZsb2F0aW5nRmlsdGVyQ2hhbmdlZCh7IG1vZGVsOiB0aGlzLmJ1aWxkTW9kZWwoKSB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gdGhpcy5wcmV2VmFsdWUgfHwgbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdmFsdWVDaGFuZ2VkKG5ld1ZhbHVlKTogdm9pZCB7XHJcbiAgICB0aGlzLm1vZGVsQ2hhbmdlZC5uZXh0KG5ld1ZhbHVlKTtcclxuICB9XHJcblxyXG4gIG9uUGFyZW50TW9kZWxDaGFuZ2VkKHBhcmVudE1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcik6IHZvaWQge1xyXG5cclxuICAgIC8vICoqKioqd2lsbCBiZSBkb25lIHdoZW4gbmVlZGVkKioqKipcclxuXHJcblxyXG4gICAgLy8gaWYgKCFwYXJlbnRNb2RlbCkge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHBhcmVudE1vZGVsLmZpbHRlcjtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG4gIGNoYW5nZWQoZGF0YTogYW55KSB7XHJcbiAgICBpZiAoZGF0YSAmJiBkYXRhLklkKSB7XHJcbiAgICAgIHRoaXMudmFsdWVDaGFuZ2VkKHRoaXMuY3VycmVudFZhbHVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGJ1aWxkTW9kZWwoKTogU2VyaWFsaXplZFRleHRGaWx0ZXIge1xyXG4gICAgbGV0IHZhbHVlOiBzdHJpbmc7XHJcbiAgICBjb25zdCB0eXBlOiBudW1iZXIgPSB0aGlzLmZvcm1Hcm91cC5nZXQoJ3RleHRGaWx0ZXJWYWx1ZScpLnZhbHVlO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgIHZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT09IFRleHRDb21wYXJpc29uLklzTnVsbE9yV2hpdGVTcGFjZSB8fFxyXG4gICAgICB0eXBlID09PSBUZXh0Q29tcGFyaXNvbi5Jc05vdE51bGxOb3JXaGl0ZVNwYWNlIHx8XHJcbiAgICAgIHR5cGUgPT09IFRleHRDb21wYXJpc29uLklzTnVsbCB8fFxyXG4gICAgICB0eXBlID09PSBUZXh0Q29tcGFyaXNvbi5Jc05vdE51bGwgfHxcclxuICAgICAgdHlwZSA9PT0gVGV4dENvbXBhcmlzb24uSXNOb3RFbXB0eSB8fFxyXG4gICAgICB0eXBlID09PSBUZXh0Q29tcGFyaXNvbi5Jc0VtcHR5KSB7XHJcbiAgICAgIHZhbHVlID0gJyAnO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICB0eXBlOiB0eXBlLnRvU3RyaW5nKCksXHJcbiAgICAgIGZpbHRlcjogdmFsdWVcclxuICAgIH07XHJcbiAgfVxyXG5cclxuXHJcbiAgaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGtleUxpc3QgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLlRleHQpLm1hcCh4ID0+IHguRGVmaW5pdGlvbik7XHJcblxyXG4gICAgY29uc3QgZGF0YVNvdXJjZSA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuVGV4dCk7XHJcblxyXG4gICAgdGhpcy50cmFuc2xhdGUuZ2V0KGtleUxpc3QpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBrZXlMaXN0LmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRWYWx1ZSA9IHJlc3VsdFtrZXldO1xyXG4gICAgICAgICAgZGF0YVNvdXJjZVtjb3VudGVyXS5EZWZpbml0aW9uID0gdHJhbnNsYXRlZFZhbHVlO1xyXG4gICAgICAgICAgY291bnRlcisrO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19