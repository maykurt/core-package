/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ComparisonList, ComparisonType, NumberComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
export class CurrencyFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                        }
                        this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    }
                    else {
                        this.currentValue = null;
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
CurrencyFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-currency-filter-cell',
                template: `
  <div class="input-group">
    <layout-form-currency-input style="width: 100%;"
        [(ngModel)]="currentValue"
        clearable = "true"
        placeholder="{{filterKey|translate}}"
        (ngModelChange)="valueChanged($event)">
    </layout-form-currency-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
CurrencyFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    CurrencyFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    CurrencyFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    CurrencyFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    CurrencyFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    CurrencyFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    CurrencyFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3ktZmlsdGVyLWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZmlsdGVyL2N1cnJlbmN5LWZpbHRlci1jZWxsL2N1cnJlbmN5LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUM3QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM3RyxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQTJCaEIsTUFBTSxPQUFPLDJCQUEyQjs7Ozs7SUFldEMsWUFDVSxFQUFlLEVBQVUsU0FBMkI7UUFBcEQsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBYnRELGlCQUFZLEdBQW9CLElBQUksT0FBTyxFQUFVLENBQUM7UUFJOUQsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQUlkLGNBQVMsR0FBRyxZQUFZLENBQUM7UUFPOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUM3QixpQkFBaUIsRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO1NBQzVDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7YUFDckQsU0FBUyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDcEQsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUNuQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNsQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDckUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtxQkFDeEMsU0FBUyxDQUFDLENBQUMsTUFBVyxFQUFFLEVBQUU7b0JBQ3pCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTt3QkFDaEIsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTs0QkFDL0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3lCQUM1RTt3QkFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7cUJBQ25FO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO3FCQUMxQjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxRQUFRO1FBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsV0FBaUM7UUFFcEQscUNBQXFDO1FBR3JDLHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFHRCxPQUFPLENBQUMsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsT0FBTztZQUNMLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEtBQUs7O1lBQ25ELE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWTtTQUMxQixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELHNCQUFzQjs7Y0FDZCxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDOztjQUUxRixVQUFVLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFFNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQ3hCLFNBQVMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFOztnQkFDaEIsT0FBTyxHQUFHLENBQUM7WUFDZixPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztzQkFDZCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzdDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7O1lBeElGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsdUNBQXVDO2dCQUNqRCxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBb0JUO2FBQ0Y7Ozs7WUFsQ21CLFdBQVc7WUFDdEIsZ0JBQWdCOzs7Ozs7O0lBb0N2Qiw2Q0FBcUM7Ozs7O0lBQ3JDLG1EQUE4RDs7Ozs7SUFDOUQsK0RBQStDOztJQUUvQyxtREFBNEI7O0lBQzVCLGlEQUFxQjs7SUFDckIsZ0RBQXFCOztJQUVyQixrREFBMkI7O0lBQzNCLGdEQUFnQzs7Ozs7SUFFaEMsNERBQTRDOzs7OztJQUcxQyx5Q0FBdUI7Ozs7O0lBQUUsZ0RBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWdGcmFtZXdvcmtDb21wb25lbnQgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBJRmxvYXRpbmdGaWx0ZXIsIElGbG9hdGluZ0ZpbHRlclBhcmFtcywgU2VyaWFsaXplZFRleHRGaWx0ZXIgfSBmcm9tICdhZy1ncmlkLWNvbW11bml0eSc7XHJcbmltcG9ydCB7IFN1YmplY3QsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IENvbXBhcmlzb25MaXN0LCBDb21wYXJpc29uVHlwZSwgTnVtYmVyQ29tcGFyaXNvbiB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2NvbXBhcmlzb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBUZXh0RmlsdGVyQ2hhbmdlIHtcclxuICBtb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmxvYXRpbmdGaWx0ZXJQYXJhbXMgZXh0ZW5kcyBJRmxvYXRpbmdGaWx0ZXJQYXJhbXM8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2U+IHtcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtY3VycmVuY3ktZmlsdGVyLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICA8bGF5b3V0LWZvcm0tY3VycmVuY3ktaW5wdXQgc3R5bGU9XCJ3aWR0aDogMTAwJTtcIlxyXG4gICAgICAgIFsobmdNb2RlbCldPVwiY3VycmVudFZhbHVlXCJcclxuICAgICAgICBjbGVhcmFibGUgPSBcInRydWVcIlxyXG4gICAgICAgIHBsYWNlaG9sZGVyPVwie3tmaWx0ZXJLZXl8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCI+XHJcbiAgICA8L2xheW91dC1mb3JtLWN1cnJlbmN5LWlucHV0PlxyXG5cclxuICAgIDxkaXYgY2xhc3M9XCJncmlkLWZpbHRlci1pY29uXCI+XHJcbiAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICA8L2Rpdj5cclxuXHJcbiAgICA8cC1vdmVybGF5UGFuZWwgI29wIGFwcGVuZFRvPVwiYm9keVwiIGRpc21pc3NhYmxlPVwiZmFsc2VcIj5cclxuICAgICAgPGRpdiAqbmdJZj1cImZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgICAgIDxsYXlvdXQtc3RhdGljLXNlbGVjdG9yIGZvcm1Db250cm9sTmFtZT1cIm51bWJlckZpbHRlclZhbHVlXCIgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCIgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFtjbGVhcmFibGVdPVwiZmFsc2VcIiBbc2VhcmNoYWJsZV09XCJmYWxzZVwiPlxyXG4gICAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L3Atb3ZlcmxheVBhbmVsPlxyXG4gIDwvZGl2PlxyXG4gIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIEN1cnJlbmN5RmlsdGVyQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElGbG9hdGluZ0ZpbHRlcjxTZXJpYWxpemVkVGV4dEZpbHRlcixcclxuVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IEZsb2F0aW5nRmlsdGVyUGFyYW1zO1xyXG4gIHByaXZhdGUgbW9kZWxDaGFuZ2VkOiBTdWJqZWN0PHN0cmluZz4gPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XHJcbiAgcHJpdmF0ZSBtb2RlbENoYW5nZWRTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgcHVibGljIGN1cnJlbnRWYWx1ZTogc3RyaW5nO1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG5cclxuICBwdWJsaWMgcGxhY2Vob2xkZXI6IHN0cmluZztcclxuICBwdWJsaWMgZmlsdGVyS2V5ID0gJ0dyaWRGaWx0ZXInO1xyXG5cclxuICBwcml2YXRlIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgbnVtYmVyRmlsdGVyVmFsdWU6IE51bWJlckNvbXBhcmlzb24uRXF1YWxUb1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5wbGFjZWhvbGRlciA9IHBhcmFtcy5jb2x1bW4uY29sRGVmLmhlYWRlck5hbWUgfHwgcGFyYW1zLmNvbHVtbi5jb2xEZWYuZmllbGQ7XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIHRoaXMubW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uID0gdGhpcy5tb2RlbENoYW5nZWQucGlwZShcclxuICAgICAgZGVib3VuY2VUaW1lKDEwMDApXHJcbiAgICApLnN1YnNjcmliZShtb2RlbCA9PiB7XHJcbiAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gbW9kZWw7XHJcbiAgICAgIGlmIChwYXJhbXMuc2VydmljZUFjY2VzcyAmJiBwYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCkge1xyXG4gICAgICAgIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcihwYXJhbXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdmFsdWVDaGFuZ2VkKG5ld1ZhbHVlKTogdm9pZCB7XHJcbiAgICB0aGlzLm1vZGVsQ2hhbmdlZC5uZXh0KG5ld1ZhbHVlKTtcclxuICB9XHJcblxyXG4gIG9uUGFyZW50TW9kZWxDaGFuZ2VkKHBhcmVudE1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcik6IHZvaWQge1xyXG5cclxuICAgIC8vICoqKioqd2lsbCBiZSBkb25lIHdoZW4gbmVlZGVkKioqKipcclxuXHJcblxyXG4gICAgLy8gaWYgKCFwYXJlbnRNb2RlbCkge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHBhcmVudE1vZGVsLmZpbHRlcjtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgZGF0YS5JZCAmJiB0aGlzLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZCh0aGlzLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKCk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICd0ZXh0JyxcclxuICAgICAgdHlwZTogdGhpcy5mb3JtR3JvdXAuZ2V0KCdudW1iZXJGaWx0ZXJWYWx1ZScpLnZhbHVlLCAvLyBjYW4gYmUgc2VuZCBpbiBoZXJlXHJcbiAgICAgIGZpbHRlcjogdGhpcy5jdXJyZW50VmFsdWVcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuTnVtYmVyKS5tYXAoeCA9PiB4LkRlZmluaXRpb24pO1xyXG5cclxuICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLk51bWJlcik7XHJcblxyXG4gICAgdGhpcy50cmFuc2xhdGUuZ2V0KGtleUxpc3QpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBrZXlMaXN0LmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRWYWx1ZSA9IHJlc3VsdFtrZXldO1xyXG4gICAgICAgICAgZGF0YVNvdXJjZVtjb3VudGVyXS5EZWZpbml0aW9uID0gdHJhbnNsYXRlZFZhbHVlO1xyXG4gICAgICAgICAgY291bnRlcisrO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19