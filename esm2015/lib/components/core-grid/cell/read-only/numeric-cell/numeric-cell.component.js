/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class NumericCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this._value = this.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        // done for experimental,
        // use case is below
        // {
        // field: 'Department',
        // columnType: CoreGridColumnType.Numeric,
        // settings: {
        //   fields: ['DepartmentId'] //for this reason
        // },
        // headerName: 'DepartmentHistoryDepartmentId'
        // },
        // {
        //   field: 'DepartmentId',
        //   columnType: CoreGridColumnType.Lov,
        //   visible: true,
        //   settings: {
        //     options: this.departmentHistoryLovGridOptions,
        //     valueField: 'DepartmentId',
        //     labelField: ['Definition']
        //   }
        // }
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                    /** @type {?} */
                    const labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.fields));
                    /** @type {?} */
                    let value = '';
                    labels.forEach((columnName) => {
                        if (this.params.node.data[columnName]) {
                            value += this.params.node.data[columnName] + ' ';
                        }
                    });
                    if (value !== '') {
                        this.value = value;
                    }
                }
            }
        }
        this._value = this.value;
        if (this.params.colDef.cellEditorParams.customDisplayFunction) {
            this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
}
NumericCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-numeric-cell',
                template: `
  <div *ngIf="value || value === 0" class="grid-none-editable-numeric-cell user-select-text text-ellipsis" title="{{_value}}">
    {{ _value }}
  </div>
  `,
                styles: [`
    .grid-none-editable-numeric-cell {
      padding-top: 5px;
      white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    }
    `]
            }] }
];
if (false) {
    /** @type {?} */
    NumericCellComponent.prototype.formGroup;
    /** @type {?} */
    NumericCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    NumericCellComponent.prototype.params;
    /** @type {?} */
    NumericCellComponent.prototype.value;
    /** @type {?} */
    NumericCellComponent.prototype._value;
    /**
     * @type {?}
     * @private
     */
    NumericCellComponent.prototype.rowId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtZXJpYy1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9udW1lcmljLWNlbGwvbnVtZXJpYy1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQXNCMUMsTUFBTSxPQUFPLG9CQUFvQjs7Ozs7SUFRL0IsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwRSx5QkFBeUI7UUFDekIsb0JBQW9CO1FBQ3BCLElBQUk7UUFDSix1QkFBdUI7UUFDdkIsMENBQTBDO1FBQzFDLGNBQWM7UUFDZCwrQ0FBK0M7UUFDL0MsS0FBSztRQUNMLDhDQUE4QztRQUM5QyxLQUFLO1FBQ0wsSUFBSTtRQUNKLDJCQUEyQjtRQUMzQix3Q0FBd0M7UUFDeEMsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixxREFBcUQ7UUFDckQsa0NBQWtDO1FBQ2xDLGlDQUFpQztRQUNqQyxNQUFNO1FBQ04sSUFBSTtRQUVKLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU0sWUFBWSxLQUFLLEVBQUU7OzBCQUNsRSxNQUFNLEdBQWEsbUJBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBQTs7d0JBQ2xGLEtBQUssR0FBVyxFQUFFO29CQUN0QixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBa0IsRUFBRSxFQUFFO3dCQUNwQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTs0QkFDckMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLENBQUM7eUJBQ2xEO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTt3QkFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7cUJBQ3BCO2lCQUNGO2FBQ0Y7U0FDRjtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUV6QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixFQUFFO1lBQzdELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3JGO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7WUF2RkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwrQkFBK0I7Z0JBQ3pDLFFBQVEsRUFBRTs7OztHQUlUO3lCQUVDOzs7Ozs7O0tBT0M7YUFFSjs7OztJQUVDLHlDQUE0Qjs7SUFDNUIsbUNBQWdCOzs7OztJQUNoQixzQ0FBb0I7O0lBQ3BCLHFDQUFrQjs7SUFDbEIsc0NBQW1COzs7OztJQUNuQixxQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUFycmF5IH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLW51bWVyaWMtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICA8ZGl2ICpuZ0lmPVwidmFsdWUgfHwgdmFsdWUgPT09IDBcIiBjbGFzcz1cImdyaWQtbm9uZS1lZGl0YWJsZS1udW1lcmljLWNlbGwgdXNlci1zZWxlY3QtdGV4dCB0ZXh0LWVsbGlwc2lzXCIgdGl0bGU9XCJ7e192YWx1ZX19XCI+XHJcbiAgICB7eyBfdmFsdWUgfX1cclxuICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgLmdyaWQtbm9uZS1lZGl0YWJsZS1udW1lcmljLWNlbGwge1xyXG4gICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgfVxyXG4gICAgYFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE51bWVyaWNDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTogYW55O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIHZhbHVlOiBhbnk7XHJcbiAgcHVibGljIF92YWx1ZTogYW55O1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KSB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZTtcclxuICAgIHRoaXMuX3ZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG5cclxuICAgIC8vIGRvbmUgZm9yIGV4cGVyaW1lbnRhbCxcclxuICAgIC8vIHVzZSBjYXNlIGlzIGJlbG93XHJcbiAgICAvLyB7XHJcbiAgICAvLyBmaWVsZDogJ0RlcGFydG1lbnQnLFxyXG4gICAgLy8gY29sdW1uVHlwZTogQ29yZUdyaWRDb2x1bW5UeXBlLk51bWVyaWMsXHJcbiAgICAvLyBzZXR0aW5nczoge1xyXG4gICAgLy8gICBmaWVsZHM6IFsnRGVwYXJ0bWVudElkJ10gLy9mb3IgdGhpcyByZWFzb25cclxuICAgIC8vIH0sXHJcbiAgICAvLyBoZWFkZXJOYW1lOiAnRGVwYXJ0bWVudEhpc3RvcnlEZXBhcnRtZW50SWQnXHJcbiAgICAvLyB9LFxyXG4gICAgLy8ge1xyXG4gICAgLy8gICBmaWVsZDogJ0RlcGFydG1lbnRJZCcsXHJcbiAgICAvLyAgIGNvbHVtblR5cGU6IENvcmVHcmlkQ29sdW1uVHlwZS5Mb3YsXHJcbiAgICAvLyAgIHZpc2libGU6IHRydWUsXHJcbiAgICAvLyAgIHNldHRpbmdzOiB7XHJcbiAgICAvLyAgICAgb3B0aW9uczogdGhpcy5kZXBhcnRtZW50SGlzdG9yeUxvdkdyaWRPcHRpb25zLFxyXG4gICAgLy8gICAgIHZhbHVlRmllbGQ6ICdEZXBhcnRtZW50SWQnLFxyXG4gICAgLy8gICAgIGxhYmVsRmllbGQ6IFsnRGVmaW5pdGlvbiddXHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH1cclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpZWxkcyBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICBjb25zdCBsYWJlbHM6IHN0cmluZ1tdID0gPHN0cmluZ1tdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpZWxkcztcclxuICAgICAgICAgIGxldCB2YWx1ZTogU3RyaW5nID0gJyc7XHJcbiAgICAgICAgICBsYWJlbHMuZm9yRWFjaCgoY29sdW1uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBhcmFtcy5ub2RlLmRhdGFbY29sdW1uTmFtZV0pIHtcclxuICAgICAgICAgICAgICB2YWx1ZSArPSB0aGlzLnBhcmFtcy5ub2RlLmRhdGFbY29sdW1uTmFtZV0gKyAnICc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaWYgKHZhbHVlICE9PSAnJykge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5fdmFsdWUgPSB0aGlzLnZhbHVlO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21EaXNwbGF5RnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5fdmFsdWUgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21EaXNwbGF5RnVuY3Rpb24odGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZm9ybUdyb3VwKSB7XHJcbiAgICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxufVxyXG4iXX0=