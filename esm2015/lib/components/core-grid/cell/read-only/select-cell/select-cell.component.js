/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class SelectCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params
            && this.params.colDef
            && this.params.colDef.cellEditorParams
            && this.params.colDef.cellEditorParams.settings
            && this.params.colDef.cellEditorParams.settings.sameFieldWith) {
            // this.value = this.params.node.data[this.params.colDef.cellEditorParams.settings.sameFieldWith];
            /** @type {?} */
            const fields = this.params.colDef.cellEditorParams.settings.sameFieldWith.split('.');
            /** @type {?} */
            let data = Object.assign({}, this.params.node.data);
            fields.forEach((field) => {
                data = data[field] || null;
            });
            this.value = data;
        }
        // this.params.node.data["PagePanel"]["PageId"]
        // 58
        // this.params.node.data["PagePanel.PageId"]
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
        }
        return true;
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.colDef.cellEditorParams.dataSource) {
            this.listData = this.params.colDef.cellEditorParams.dataSource;
            // this.clearSubscription();
            /** @type {?} */
            const data = this.listData.find(x => x[this.params.colDef.cellEditorParams.settings.valueField] === this.value);
            if (data) {
                this.definition = '';
                if (this.params.colDef.cellEditorParams.settings.labelField instanceof Array) {
                    /** @type {?} */
                    const labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.labelField));
                    labels.forEach((columnName) => {
                        this.definition = this.definition + ' ' + data[columnName];
                    });
                }
                else if (this.params.colDef.cellEditorParams.settings.labelField) {
                    this.definition = data[this.params.colDef.cellEditorParams.settings.labelField];
                }
            }
            if (this.definition) {
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.definition);
                }
            }
        }
    }
    /**
     * @return {?}
     */
    clearSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearSubscription();
    }
}
SelectCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-cell',
                template: `
  <div *ngIf="definition" class="grid-none-editable-select-cell user-select-text text-ellipsis"  title="{{definition}}">
    {{ definition }}
  </div>
  `,
                styles: [`
      .grid-none-editable-select-cell {
        padding-top: 5px;
        white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
      }
    `]
            }] }
];
if (false) {
    /** @type {?} */
    SelectCellComponent.prototype.listData;
    /** @type {?} */
    SelectCellComponent.prototype.formGroup;
    /** @type {?} */
    SelectCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.params;
    /** @type {?} */
    SelectCellComponent.prototype.value;
    /** @type {?} */
    SelectCellComponent.prototype.definition;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.dataSubscription;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvcmVhZC1vbmx5L3NlbGVjdC1jZWxsL3NlbGVjdC1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQXVCckQsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7SUFVOUIsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDMUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFFakMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDN0UsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsTUFBVztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBFLElBQUksSUFBSSxDQUFDLE1BQU07ZUFDVixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07ZUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCO2VBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7ZUFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTs7O2tCQUV6RCxNQUFNLEdBQWEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztnQkFDMUYsSUFBSSxHQUFRLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN4RCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBYSxFQUFFLEVBQUU7Z0JBQy9CLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FHbkI7UUFDRCwrQ0FBK0M7UUFDL0MsS0FBSztRQUNMLDRDQUE0QztRQUU1QyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRTtZQUNsRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQzs7O2tCQUV6RCxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDL0csSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsWUFBWSxLQUFLLEVBQUU7OzBCQUN0RSxNQUFNLEdBQWEsbUJBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBQTtvQkFDMUYsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQWtCLEVBQUUsRUFBRTt3QkFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzdELENBQUMsQ0FBQyxDQUFDO2lCQUNKO3FCQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRTtvQkFDbEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNqRjthQUNGO1lBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNuQixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUN6RDthQUNGO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3JDO0lBQ0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7WUFyR0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSw4QkFBOEI7Z0JBQ3hDLFFBQVEsRUFBRTs7OztHQUlUO3lCQUVDOzs7Ozs7O0tBT0M7YUFFSjs7OztJQUVDLHVDQUFxQjs7SUFDckIsd0NBQTRCOztJQUM1QixrQ0FBVzs7Ozs7SUFDWCxxQ0FBb0I7O0lBQ3BCLG9DQUFhOztJQUNiLHlDQUFrQjs7Ozs7SUFDbEIsb0NBQW1COzs7OztJQUNuQiwrQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLXNlbGVjdC1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxkaXYgKm5nSWY9XCJkZWZpbml0aW9uXCIgY2xhc3M9XCJncmlkLW5vbmUtZWRpdGFibGUtc2VsZWN0LWNlbGwgdXNlci1zZWxlY3QtdGV4dCB0ZXh0LWVsbGlwc2lzXCIgIHRpdGxlPVwie3tkZWZpbml0aW9ufX1cIj5cclxuICAgIHt7IGRlZmluaXRpb24gfX1cclxuICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgICAuZ3JpZC1ub25lLWVkaXRhYmxlLXNlbGVjdC1jZWxsIHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIH1cclxuICAgIGBcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wLCBPbkRlc3Ryb3kge1xyXG4gIHB1YmxpYyBsaXN0RGF0YTogYW55O1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIHZhbHVlO1xyXG4gIHB1YmxpYyBkZWZpbml0aW9uO1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwcml2YXRlIGRhdGFTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KSB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZSA/IHRoaXMucGFyYW1zLnZhbHVlIDogbnVsbDtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIHRoaXMuZGF0YVN1YnNjcmlwdGlvbiA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGEuc3Vic2NyaWJlKHggPT4ge1xyXG4gICAgICB0aGlzLmdldExpc3REYXRhKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXNcclxuICAgICAgJiYgdGhpcy5wYXJhbXMuY29sRGVmXHJcbiAgICAgICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zXHJcbiAgICAgICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzXHJcbiAgICAgICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLnNhbWVGaWVsZFdpdGgpIHtcclxuICAgICAgLy8gdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLm5vZGUuZGF0YVt0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5zYW1lRmllbGRXaXRoXTtcclxuICAgICAgY29uc3QgZmllbGRzOiBzdHJpbmdbXSA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLnNhbWVGaWVsZFdpdGguc3BsaXQoJy4nKTtcclxuICAgICAgbGV0IGRhdGE6IGFueSA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMucGFyYW1zLm5vZGUuZGF0YSk7XHJcbiAgICAgIGZpZWxkcy5mb3JFYWNoKChmaWVsZDogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgZGF0YSA9IGRhdGFbZmllbGRdIHx8IG51bGw7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnZhbHVlID0gZGF0YTtcclxuXHJcblxyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5wYXJhbXMubm9kZS5kYXRhW1wiUGFnZVBhbmVsXCJdW1wiUGFnZUlkXCJdXHJcbiAgICAvLyA1OFxyXG4gICAgLy8gdGhpcy5wYXJhbXMubm9kZS5kYXRhW1wiUGFnZVBhbmVsLlBhZ2VJZFwiXVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICB0aGlzLmdldExpc3REYXRhKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXRMaXN0RGF0YSgpIHtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKSB7XHJcbiAgICAgIHRoaXMubGlzdERhdGEgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlO1xyXG4gICAgICAvLyB0aGlzLmNsZWFyU3Vic2NyaXB0aW9uKCk7XHJcbiAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmxpc3REYXRhLmZpbmQoeCA9PiB4W3RoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLnZhbHVlRmllbGRdID09PSB0aGlzLnZhbHVlKTtcclxuICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmRlZmluaXRpb24gPSAnJztcclxuICAgICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MubGFiZWxGaWVsZCBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICBjb25zdCBsYWJlbHM6IHN0cmluZ1tdID0gPHN0cmluZ1tdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmxhYmVsRmllbGQ7XHJcbiAgICAgICAgICBsYWJlbHMuZm9yRWFjaCgoY29sdW1uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmaW5pdGlvbiA9IHRoaXMuZGVmaW5pdGlvbiArICcgJyArIGRhdGFbY29sdW1uTmFtZV07XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmxhYmVsRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuZGVmaW5pdGlvbiA9IGRhdGFbdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MubGFiZWxGaWVsZF07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmRlZmluaXRpb24pIHtcclxuICAgICAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMuZGVmaW5pdGlvbik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjbGVhclN1YnNjcmlwdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmRhdGFTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5kYXRhU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xlYXJTdWJzY3JpcHRpb24oKTtcclxuICB9XHJcbn1cclxuIl19