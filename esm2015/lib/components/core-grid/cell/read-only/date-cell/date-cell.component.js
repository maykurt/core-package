/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import moment from 'moment';
export class DateCellComponent {
    constructor() {
        this.defaultDateFormat = 'DD.MM.YYYY';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        if (this.params && this.params.colDef && this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
        }
        if (this.settings) {
            if (this.settings.showYear) {
                this.defaultDateFormat = 'YYYY';
            }
            if (this.settings.showTimeOnly) {
                this.defaultDateFormat = 'HH:mm';
            }
            if (this.settings.showTime) {
                this.defaultDateFormat = 'DD.MM.YYYY HH:mm';
            }
            if (this.settings.dateFormat) {
                this.defaultDateFormat = this.settings.dateFormat;
            }
        }
        if (this.params.value) {
            this.value = moment(this.params.value).format(this.defaultDateFormat);
        }
        else {
            this.value = null;
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.value = this.params.value;
        if (this.defaultDateFormat && this.value) {
            this.value = moment(this.value).format(this.defaultDateFormat);
        }
        return true;
    }
}
DateCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-date-cell',
                template: `
      <div *ngIf="value" class="grid-none-editable-date-cell user-select-text text-ellipsis">
        {{ value }}
      </div>
  `,
                styles: [`
      .grid-none-editable-date-cell {
        padding-top: 5px;
      }
    `]
            }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.defaultDateFormat;
    /** @type {?} */
    DateCellComponent.prototype.value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9kYXRlLWNlbGwvZGF0ZS1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxQyxPQUFPLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFtQjVCLE1BQU0sT0FBTyxpQkFBaUI7SUFoQjlCO1FBbUJVLHNCQUFpQixHQUFHLFlBQVksQ0FBQztJQTZDM0MsQ0FBQzs7Ozs7SUExQ0MsTUFBTSxDQUFDLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQzVFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzthQUM5RDtTQUNGO1FBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7YUFDakM7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFO2dCQUM5QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDO2FBQ2xDO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGtCQUFrQixDQUFDO2FBQzdDO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO2FBQ25EO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ3ZFO2FBQU07WUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjtJQUNILENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMvQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDaEU7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7OztZQS9ERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsUUFBUSxFQUFFOzs7O0dBSVQ7eUJBRUM7Ozs7S0FJQzthQUVKOzs7Ozs7O0lBR0MsbUNBQW9COzs7OztJQUNwQixxQ0FBK0I7Ozs7O0lBQy9CLDhDQUF5Qzs7SUFDekMsa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgRGF0ZVNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL2RhdGUtc2V0dGluZ3MubW9kZWwnO1xyXG5cclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1kYXRlLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICAgIDxkaXYgKm5nSWY9XCJ2YWx1ZVwiIGNsYXNzPVwiZ3JpZC1ub25lLWVkaXRhYmxlLWRhdGUtY2VsbCB1c2VyLXNlbGVjdC10ZXh0IHRleHQtZWxsaXBzaXNcIj5cclxuICAgICAgICB7eyB2YWx1ZSB9fVxyXG4gICAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgICAuZ3JpZC1ub25lLWVkaXRhYmxlLWRhdGUtY2VsbCB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgfVxyXG4gICAgYFxyXG4gIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwcml2YXRlIHNldHRpbmdzOiBEYXRlU2V0dGluZ3M7XHJcbiAgcHJpdmF0ZSBkZWZhdWx0RGF0ZUZvcm1hdCA9ICdERC5NTS5ZWVlZJztcclxuICB2YWx1ZTogYW55O1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcyAmJiB0aGlzLnBhcmFtcy5jb2xEZWYgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuc2V0dGluZ3MpIHtcclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3Muc2hvd1llYXIpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRlRm9ybWF0ID0gJ1lZWVknO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5zaG93VGltZU9ubHkpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRlRm9ybWF0ID0gJ0hIOm1tJztcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3Muc2hvd1RpbWUpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRlRm9ybWF0ID0gJ0RELk1NLllZWVkgSEg6bW0nO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5kYXRlRm9ybWF0KSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0RGF0ZUZvcm1hdCA9IHRoaXMuc2V0dGluZ3MuZGF0ZUZvcm1hdDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy52YWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlID0gbW9tZW50KHRoaXMucGFyYW1zLnZhbHVlKS5mb3JtYXQodGhpcy5kZWZhdWx0RGF0ZUZvcm1hdCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnZhbHVlID0gbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlO1xyXG4gICAgaWYgKHRoaXMuZGVmYXVsdERhdGVGb3JtYXQgJiYgdGhpcy52YWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlID0gbW9tZW50KHRoaXMudmFsdWUpLmZvcm1hdCh0aGlzLmRlZmF1bHREYXRlRm9ybWF0KTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxufVxyXG4iXX0=