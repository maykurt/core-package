/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class SelectEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        this.gridService = this.params.context.componentParent;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
            this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
        }
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @return {?}
     */
    createListeningSubscription() {
        if (this.filterByField) {
            // clear if suscription has
            this.clearfilterByFieldSubscription();
            // if formgroup not exist, get formgroup
            if (!this.formGroup) {
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
            }
            if (this.formGroup) {
                // get key in order to track changes
                /** @type {?} */
                const columnKeyToListen = this.getFilterByFieldKey();
                // for initial value fill according to that
                /** @type {?} */
                const formgroupForKeyToListen = this.formGroup.at(columnKeyToListen);
                if (formgroupForKeyToListen && (formgroupForKeyToListen.value || formgroupForKeyToListen.value === 0)) {
                    this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                        .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                }
                // listen that cloumn changes at that field
                this.filterByFieldSubscription = this.formGroup.at(columnKeyToListen).valueChanges
                    .subscribe((data) => {
                    if (!data) {
                        // if the data set to null in that column, then set selft to null
                        this.setValue(null);
                        // clear list because data comes according to this data value
                        this.listData = [];
                    }
                    else {
                        // clear list in order to assign new ones
                        this.listData = [];
                        // fill the list according to value at that column
                        this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                            .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                        // look the current selected id, if it is exist in new listData,
                        /** @type {?} */
                        const foundData = this.listData.find(x => x[this.settings.valueField] === this.formGroup.at(this.key).value);
                        if (!foundData) {
                            // if it is not clear data of select
                            this.setValue(null);
                            // somehow lisData broken, in order to avoid this, I re assigned that
                            this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                                .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                        }
                    }
                });
            }
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            else {
                console.error(this.params.colDef.field + ' lov settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' data source or lov settings is not defined');
        }
        return true;
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.colDef.cellEditorParams.dataSource) {
            // get filterByField
            if (this.params.colDef.cellEditorParams.settings) {
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            if (!this.filterByField) {
                // if filterByField not exist, then assign orginal data source
                this.listData = this.params.colDef.cellEditorParams.dataSource;
            }
            else {
                // if exist, get the key, then filter according to that
                /** @type {?} */
                const columnKeyToListen = this.getFilterByFieldKey();
                this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                    .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
            }
            this.clearDataSubscription();
            this.createListeningSubscription();
        }
    }
    /**
     * @return {?}
     */
    getFilterByFieldKey() {
        // get find Column according to  filterByField
        /** @type {?} */
        const column = this.gridService.allGridColumns.find(x => x.colDef.field === this.filterByField);
        // createKey with that column in order to use for tracking
        /** @type {?} */
        const columnKeyToListen = this.params.context.createKey(this.params.columnApi, column);
        return columnKeyToListen;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onValueChanged(data) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.value = value;
        this.formGroup.at(this.key).patchValue(value);
        this.params.setValue(value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
    /**
     * @return {?}
     */
    clearDataSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    clearfilterByFieldSubscription() {
        if (this.filterByFieldSubscription) {
            this.filterByFieldSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearDataSubscription();
        this.clearfilterByFieldSubscription();
    }
}
SelectEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-edit-cell',
                template: `
    <div
      *ngIf="formGroup && this.settings && this.settings.valueField"
      [formGroup]="formGroup">
      <layout-static-selector
        #selector
				[class.not-valid]="!isValid()"
        [formControlName]="key"
        (changed)="onValueChanged($event)"
        [dataSource]="listData"
        [isReadOnly]="isReadOnly"
        [valueField]="this.settings.valueField"
        [labelField]="this.settings.labelField"
      >
      </layout-static-selector>
    </div>
  `,
                styles: [":host /deep/ .custom-ng-select{margin-top:2px}:host /deep/ .not-valid .custom-ng-select .ng-select-container{border:1px solid red}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:1px}"]
            }] }
];
if (false) {
    /** @type {?} */
    SelectEditCellComponent.prototype.formGroup;
    /** @type {?} */
    SelectEditCellComponent.prototype.key;
    /** @type {?} */
    SelectEditCellComponent.prototype.settings;
    /** @type {?} */
    SelectEditCellComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.dataSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.filterByFieldSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.gridService;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.filterByField;
    /** @type {?} */
    SelectEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9lZGl0L3NlbGVjdC1lZGl0LWNlbGwvc2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUE0QnJELE1BQU0sT0FBTyx1QkFBdUI7Ozs7O0lBa0JsQyxNQUFNLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDO1FBRXZELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLEVBQUU7WUFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakc7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtZQUM3RyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7U0FDakY7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUM3RSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsMkJBQTJCO1FBQ3pCLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QiwyQkFBMkI7WUFDM0IsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7WUFFdEMsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3JFO1lBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFOzs7c0JBRVosaUJBQWlCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFOzs7c0JBRzlDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDO2dCQUNwRSxJQUFJLHVCQUF1QixJQUFJLENBQUMsdUJBQXVCLENBQUMsS0FBSyxJQUFJLHVCQUF1QixDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtvQkFDckcsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBQSxDQUFDO3lCQUNwRSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RGO2dCQUdELDJDQUEyQztnQkFDM0MsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsWUFBWTtxQkFDL0UsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxJQUFJLEVBQUU7d0JBQ1QsaUVBQWlFO3dCQUNqRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQiw2REFBNkQ7d0JBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3FCQUNwQjt5QkFBTTt3QkFDTCx5Q0FBeUM7d0JBQ3pDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixrREFBa0Q7d0JBQ2xELElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQzs2QkFDcEUsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7OEJBRy9FLFNBQVMsR0FBUSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBQ2pILElBQUksQ0FBQyxTQUFTLEVBQUU7NEJBQ2Qsb0NBQW9DOzRCQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNwQixxRUFBcUU7NEJBQ3JFLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQztpQ0FDcEUsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUN0RjtxQkFDRjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNOO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUN2QyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2dCQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7YUFDakY7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsOEJBQThCLENBQUMsQ0FBQzthQUMxRTtTQUNGO2FBQU07WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyw2Q0FBNkMsQ0FBQyxDQUFDO1NBQ3pGO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFO1lBQ2xELG9CQUFvQjtZQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDO2FBQ2pGO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3ZCLDhEQUE4RDtnQkFDOUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7YUFDaEU7aUJBQU07OztzQkFFQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQztxQkFDcEUsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3RGO1lBRUQsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDN0IsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQsbUJBQW1COzs7Y0FFWCxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQzs7O2NBRXpGLGlCQUFpQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUM7UUFDdEYsT0FBTyxpQkFBaUIsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxJQUFTO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDM0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQVU7UUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ25CLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyQztJQUNILENBQUM7Ozs7SUFFRCw4QkFBOEI7UUFDNUIsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDbEMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzlDO0lBQ0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7WUFsTUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQ0FBbUM7Z0JBQzdDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7OztHQWdCVDs7YUFFRjs7OztJQUVDLDRDQUE0Qjs7SUFDNUIsc0NBQWdCOztJQUNoQiwyQ0FBa0M7O0lBQ2xDLDJDQUF1Qjs7Ozs7SUFFdkIsd0NBQW1COzs7OztJQUNuQix3Q0FBbUI7Ozs7O0lBQ25CLHlDQUFvQjs7Ozs7SUFDcEIsbURBQXVDOzs7OztJQUV2Qyw0REFBZ0Q7Ozs7O0lBRWhELDhDQUFxQzs7Ozs7SUFDckMsZ0RBQThCOztJQUU5Qiw2Q0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IFNlbGVjdG9yU2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvc2VsZWN0b3Itc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ29yZUdyaWRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvY29yZS1ncmlkLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLXNlbGVjdC1lZGl0LWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2XHJcbiAgICAgICpuZ0lmPVwiZm9ybUdyb3VwICYmIHRoaXMuc2V0dGluZ3MgJiYgdGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXCJcclxuICAgICAgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIj5cclxuICAgICAgPGxheW91dC1zdGF0aWMtc2VsZWN0b3JcclxuICAgICAgICAjc2VsZWN0b3JcclxuXHRcdFx0XHRbY2xhc3Mubm90LXZhbGlkXT1cIiFpc1ZhbGlkKClcIlxyXG4gICAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwia2V5XCJcclxuICAgICAgICAoY2hhbmdlZCk9XCJvblZhbHVlQ2hhbmdlZCgkZXZlbnQpXCJcclxuICAgICAgICBbZGF0YVNvdXJjZV09XCJsaXN0RGF0YVwiXHJcbiAgICAgICAgW2lzUmVhZE9ubHldPVwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgW3ZhbHVlRmllbGRdPVwidGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXCJcclxuICAgICAgICBbbGFiZWxGaWVsZF09XCJ0aGlzLnNldHRpbmdzLmxhYmVsRmllbGRcIlxyXG4gICAgICA+XHJcbiAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RFZGl0Q2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCwgT25EZXN0cm95IHtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTogYW55O1xyXG4gIHB1YmxpYyBzZXR0aW5nczogU2VsZWN0b3JTZXR0aW5ncztcclxuICBwdWJsaWMgbGlzdERhdGE6IGFueVtdO1xyXG5cclxuICBwcml2YXRlIHZhbHVlOiBhbnk7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHJpdmF0ZSBkYXRhU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHByaXZhdGUgZmlsdGVyQnlGaWVsZFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwcml2YXRlIGdyaWRTZXJ2aWNlOiBDb3JlR3JpZFNlcnZpY2U7XHJcbiAgcHJpdmF0ZSBmaWx0ZXJCeUZpZWxkOiBzdHJpbmc7XHJcblxyXG4gIHB1YmxpYyBpc1JlYWRPbmx5OiBib29sZWFuO1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlID8gK3RoaXMucGFyYW1zLnZhbHVlIDogbnVsbDtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gICAgdGhpcy5ncmlkU2VydmljZSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50O1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgdGhpcy5zZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzO1xyXG4gICAgICB0aGlzLmZpbHRlckJ5RmllbGQgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5maWx0ZXJCeUZpZWxkO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZGF0YVN1YnNjcmlwdGlvbiA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGEuc3Vic2NyaWJlKHggPT4ge1xyXG4gICAgICB0aGlzLmdldExpc3REYXRhKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZUxpc3RlbmluZ1N1YnNjcmlwdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmZpbHRlckJ5RmllbGQpIHtcclxuICAgICAgLy8gY2xlYXIgaWYgc3VzY3JpcHRpb24gaGFzXHJcbiAgICAgIHRoaXMuY2xlYXJmaWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uKCk7XHJcblxyXG4gICAgICAvLyBpZiBmb3JtZ3JvdXAgbm90IGV4aXN0LCBnZXQgZm9ybWdyb3VwXHJcbiAgICAgIGlmICghdGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgICAvLyBnZXQga2V5IGluIG9yZGVyIHRvIHRyYWNrIGNoYW5nZXNcclxuICAgICAgICBjb25zdCBjb2x1bW5LZXlUb0xpc3RlbiA9IHRoaXMuZ2V0RmlsdGVyQnlGaWVsZEtleSgpO1xyXG5cclxuICAgICAgICAvLyBmb3IgaW5pdGlhbCB2YWx1ZSBmaWxsIGFjY29yZGluZyB0byB0aGF0XHJcbiAgICAgICAgY29uc3QgZm9ybWdyb3VwRm9yS2V5VG9MaXN0ZW4gPSB0aGlzLmZvcm1Hcm91cC5hdChjb2x1bW5LZXlUb0xpc3Rlbik7XHJcbiAgICAgICAgaWYgKGZvcm1ncm91cEZvcktleVRvTGlzdGVuICYmIChmb3JtZ3JvdXBGb3JLZXlUb0xpc3Rlbi52YWx1ZSB8fCBmb3JtZ3JvdXBGb3JLZXlUb0xpc3Rlbi52YWx1ZSA9PT0gMCkpIHtcclxuICAgICAgICAgIHRoaXMubGlzdERhdGEgPSAoPGFueVtdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpXHJcbiAgICAgICAgICAgIC5maWx0ZXIoeCA9PiB4W3RoaXMuZmlsdGVyQnlGaWVsZF0gPT09IHRoaXMuZm9ybUdyb3VwLmF0KGNvbHVtbktleVRvTGlzdGVuKS52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgLy8gbGlzdGVuIHRoYXQgY2xvdW1uIGNoYW5nZXMgYXQgdGhhdCBmaWVsZFxyXG4gICAgICAgIHRoaXMuZmlsdGVyQnlGaWVsZFN1YnNjcmlwdGlvbiA9IHRoaXMuZm9ybUdyb3VwLmF0KGNvbHVtbktleVRvTGlzdGVuKS52YWx1ZUNoYW5nZXNcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIWRhdGEpIHtcclxuICAgICAgICAgICAgICAvLyBpZiB0aGUgZGF0YSBzZXQgdG8gbnVsbCBpbiB0aGF0IGNvbHVtbiwgdGhlbiBzZXQgc2VsZnQgdG8gbnVsbFxyXG4gICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWUobnVsbCk7XHJcbiAgICAgICAgICAgICAgLy8gY2xlYXIgbGlzdCBiZWNhdXNlIGRhdGEgY29tZXMgYWNjb3JkaW5nIHRvIHRoaXMgZGF0YSB2YWx1ZVxyXG4gICAgICAgICAgICAgIHRoaXMubGlzdERhdGEgPSBbXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAvLyBjbGVhciBsaXN0IGluIG9yZGVyIHRvIGFzc2lnbiBuZXcgb25lc1xyXG4gICAgICAgICAgICAgIHRoaXMubGlzdERhdGEgPSBbXTtcclxuICAgICAgICAgICAgICAvLyBmaWxsIHRoZSBsaXN0IGFjY29yZGluZyB0byB2YWx1ZSBhdCB0aGF0IGNvbHVtblxyXG4gICAgICAgICAgICAgIHRoaXMubGlzdERhdGEgPSAoPGFueVtdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpXHJcbiAgICAgICAgICAgICAgICAuZmlsdGVyKHggPT4geFt0aGlzLmZpbHRlckJ5RmllbGRdID09PSB0aGlzLmZvcm1Hcm91cC5hdChjb2x1bW5LZXlUb0xpc3RlbikudmFsdWUpO1xyXG5cclxuICAgICAgICAgICAgICAvLyBsb29rIHRoZSBjdXJyZW50IHNlbGVjdGVkIGlkLCBpZiBpdCBpcyBleGlzdCBpbiBuZXcgbGlzdERhdGEsXHJcbiAgICAgICAgICAgICAgY29uc3QgZm91bmREYXRhOiBhbnkgPSB0aGlzLmxpc3REYXRhLmZpbmQoeCA9PiB4W3RoaXMuc2V0dGluZ3MudmFsdWVGaWVsZF0gPT09IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgaWYgKCFmb3VuZERhdGEpIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIGl0IGlzIG5vdCBjbGVhciBkYXRhIG9mIHNlbGVjdFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICAgICAgICAgIC8vIHNvbWVob3cgbGlzRGF0YSBicm9rZW4sIGluIG9yZGVyIHRvIGF2b2lkIHRoaXMsIEkgcmUgYXNzaWduZWQgdGhhdFxyXG4gICAgICAgICAgICAgICAgdGhpcy5saXN0RGF0YSA9ICg8YW55W10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSlcclxuICAgICAgICAgICAgICAgICAgLmZpbHRlcih4ID0+IHhbdGhpcy5maWx0ZXJCeUZpZWxkXSA9PT0gdGhpcy5mb3JtR3JvdXAuYXQoY29sdW1uS2V5VG9MaXN0ZW4pLnZhbHVlKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIHRoaXMuZ2V0TGlzdERhdGEoKTtcclxuICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzO1xyXG4gICAgICAgIHRoaXMuZmlsdGVyQnlGaWVsZCA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpbHRlckJ5RmllbGQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2xEZWYuZmllbGQgKyAnIGxvdiBzZXR0aW5ncyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbERlZi5maWVsZCArICcgZGF0YSBzb3VyY2Ugb3IgbG92IHNldHRpbmdzIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBnZXRMaXN0RGF0YSgpIHtcclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKSB7XHJcbiAgICAgIC8vIGdldCBmaWx0ZXJCeUZpZWxkXHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuZmlsdGVyQnlGaWVsZCA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpbHRlckJ5RmllbGQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghdGhpcy5maWx0ZXJCeUZpZWxkKSB7XHJcbiAgICAgICAgLy8gaWYgZmlsdGVyQnlGaWVsZCBub3QgZXhpc3QsIHRoZW4gYXNzaWduIG9yZ2luYWwgZGF0YSBzb3VyY2VcclxuICAgICAgICB0aGlzLmxpc3REYXRhID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBpZiBleGlzdCwgZ2V0IHRoZSBrZXksIHRoZW4gZmlsdGVyIGFjY29yZGluZyB0byB0aGF0XHJcbiAgICAgICAgY29uc3QgY29sdW1uS2V5VG9MaXN0ZW4gPSB0aGlzLmdldEZpbHRlckJ5RmllbGRLZXkoKTtcclxuICAgICAgICB0aGlzLmxpc3REYXRhID0gKDxhbnlbXT50aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKVxyXG4gICAgICAgICAgLmZpbHRlcih4ID0+IHhbdGhpcy5maWx0ZXJCeUZpZWxkXSA9PT0gdGhpcy5mb3JtR3JvdXAuYXQoY29sdW1uS2V5VG9MaXN0ZW4pLnZhbHVlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5jbGVhckRhdGFTdWJzY3JpcHRpb24oKTtcclxuICAgICAgdGhpcy5jcmVhdGVMaXN0ZW5pbmdTdWJzY3JpcHRpb24oKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldEZpbHRlckJ5RmllbGRLZXkoKTogbnVtYmVyIHtcclxuICAgIC8vIGdldCBmaW5kIENvbHVtbiBhY2NvcmRpbmcgdG8gIGZpbHRlckJ5RmllbGRcclxuICAgIGNvbnN0IGNvbHVtbiA9IHRoaXMuZ3JpZFNlcnZpY2UuYWxsR3JpZENvbHVtbnMuZmluZCh4ID0+IHguY29sRGVmLmZpZWxkID09PSB0aGlzLmZpbHRlckJ5RmllbGQpO1xyXG4gICAgLy8gY3JlYXRlS2V5IHdpdGggdGhhdCBjb2x1bW4gaW4gb3JkZXIgdG8gdXNlIGZvciB0cmFja2luZ1xyXG4gICAgY29uc3QgY29sdW1uS2V5VG9MaXN0ZW4gPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIGNvbHVtbik7XHJcbiAgICByZXR1cm4gY29sdW1uS2V5VG9MaXN0ZW47XHJcbiAgfVxyXG5cclxuICBvblZhbHVlQ2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWUgPyArdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlIDogbnVsbDtcclxuICAgIHRoaXMucGFyYW1zLnNldFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgc2V0VmFsdWUodmFsdWU6IGFueSkge1xyXG4gICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodmFsdWUpO1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhKCh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZXJyb3JzICYmICh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZGlydHkgfHwgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnRvdWNoZWQpKSk7XHJcbiAgfVxyXG5cclxuICBjbGVhckRhdGFTdWJzY3JpcHRpb24oKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5kYXRhU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMuZGF0YVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY2xlYXJmaWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZmlsdGVyQnlGaWVsZFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmZpbHRlckJ5RmllbGRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbGVhckRhdGFTdWJzY3JpcHRpb24oKTtcclxuICAgIHRoaXMuY2xlYXJmaWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==