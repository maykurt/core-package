/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class CurrencyEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = (this.params.value || this.params.value === 0) ? this.params.value.toString() : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
CurrencyEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-currency-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-form-currency-input
        [class.not-valid]="!isValid()"
        [formControlName]="key"
        [isReadOnly]="isReadOnly"
        (changed)="onValueChanged()">
      </layout-form-currency-input>
    </div>
  `,
                styles: [""]
            }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CurrencyEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    CurrencyEditCellComponent.prototype.rowId;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.formGroup;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.key;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.value;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3ktZWRpdC1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2VkaXQvY3VycmVuY3ktZWRpdC1jZWxsL2N1cnJlbmN5LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFvQjFDLE1BQU0sT0FBTyx5QkFBeUI7Ozs7O0lBU3BDLE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDbEcsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFFakMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRTtZQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqRztJQUNILENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOzs7WUFyREYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQ0FBcUM7Z0JBQy9DLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7R0FXVDs7YUFFRjs7Ozs7OztJQUVDLDJDQUFvQjs7Ozs7SUFDcEIsMENBQW1COztJQUNuQiw4Q0FBNEI7O0lBQzVCLHdDQUFXOztJQUNYLDBDQUFhOztJQUViLCtDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtY3VycmVuY3ktZWRpdC1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdlxyXG4gICAgICAqbmdJZj1cImZvcm1Hcm91cFwiXHJcbiAgICAgIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCI+XHJcbiAgICAgIDxsYXlvdXQtZm9ybS1jdXJyZW5jeS1pbnB1dFxyXG4gICAgICAgIFtjbGFzcy5ub3QtdmFsaWRdPVwiIWlzVmFsaWQoKVwiXHJcbiAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICAgIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAgIChjaGFuZ2VkKT1cIm9uVmFsdWVDaGFuZ2VkKClcIj5cclxuICAgICAgPC9sYXlvdXQtZm9ybS1jdXJyZW5jeS1pbnB1dD5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY3VycmVuY3ktZWRpdC1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEN1cnJlbmN5RWRpdENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuXHJcbiAgcHVibGljIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgcGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gKHRoaXMucGFyYW1zLnZhbHVlIHx8IHRoaXMucGFyYW1zLnZhbHVlID09PSAwKSA/IHRoaXMucGFyYW1zLnZhbHVlLnRvU3RyaW5nKCkgOiBudWxsO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gIXRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24odGhpcy5wYXJhbXMuZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KSB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuXHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgaWYgKHRoaXMuZm9ybUdyb3VwKSB7XHJcbiAgICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBvblZhbHVlQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWU7XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICB9XHJcblxyXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISgodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmVycm9ycyAmJiAodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmRpcnR5IHx8IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS50b3VjaGVkKSkpO1xyXG4gIH1cclxufVxyXG4iXX0=