/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class DateEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = params.context.createKey(this.params.columnApi, this.params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @param {?} date
     * @return {?}
     */
    onDateChanges(date) {
        this.value = this.formGroup.at(this.key).value ? this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
DateEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-date-edit-cell',
                template: `
    <div *ngIf="formGroup"
      [formGroup]="formGroup"
      class="input-group" style="width:100%; height:30px;margin-top:2px;">
      <layout-form-datepicker-input style="width:100%;"
			[class.not-valid]="!isValid()"
      [formControlName]="key"
      [isReadOnly]="isReadOnly"
		  [settings]= "settings"
      (dateChanges)= "onDateChanges($event)"></layout-form-datepicker-input>
    </div>
  `,
                styles: [":host /deep/ .not-valid .ui-calendar input{border:1px solid red}"]
            }] }
];
if (false) {
    /** @type {?} */
    DateEditCellComponent.prototype.formGroup;
    /** @type {?} */
    DateEditCellComponent.prototype.settings;
    /** @type {?} */
    DateEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.params;
    /** @type {?} */
    DateEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC9kYXRlLWVkaXQtY2VsbC9kYXRlLWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFzQjFDLE1BQU0sT0FBTyxxQkFBcUI7Ozs7O0lBVWhDLE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzFELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBRWpDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLEVBQUU7WUFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakc7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtZQUM3RyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztTQUM5RDtJQUNILENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLElBQVk7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDMUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9ILENBQUM7OztZQXpERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGlDQUFpQztnQkFDM0MsUUFBUSxFQUFFOzs7Ozs7Ozs7OztHQVdUOzthQUVGOzs7O0lBRUMsMENBQTRCOztJQUM1Qix5Q0FBdUI7O0lBQ3ZCLG9DQUFnQjs7Ozs7SUFFaEIsc0NBQWM7Ozs7O0lBQ2Qsc0NBQW1COzs7OztJQUNuQix1Q0FBb0I7O0lBQ3BCLDJDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IERhdGVTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL21vZGVscy9kYXRlLXNldHRpbmdzLm1vZGVsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtZGF0ZS1lZGl0LWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCJcclxuICAgICAgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIlxyXG4gICAgICBjbGFzcz1cImlucHV0LWdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlOyBoZWlnaHQ6MzBweDttYXJnaW4tdG9wOjJweDtcIj5cclxuICAgICAgPGxheW91dC1mb3JtLWRhdGVwaWNrZXItaW5wdXQgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiXHJcblx0XHRcdFtjbGFzcy5ub3QtdmFsaWRdPVwiIWlzVmFsaWQoKVwiXHJcbiAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwia2V5XCJcclxuICAgICAgW2lzUmVhZE9ubHldPVwiaXNSZWFkT25seVwiXHJcblx0XHQgIFtzZXR0aW5nc109IFwic2V0dGluZ3NcIlxyXG4gICAgICAoZGF0ZUNoYW5nZXMpPSBcIm9uRGF0ZUNoYW5nZXMoJGV2ZW50KVwiPjwvbGF5b3V0LWZvcm0tZGF0ZXBpY2tlci1pbnB1dD5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZGF0ZS1lZGl0LWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGF0ZUVkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgc2V0dGluZ3M6IERhdGVTZXR0aW5ncztcclxuICBwdWJsaWMga2V5OiBhbnk7XHJcblxyXG4gIHByaXZhdGUgdmFsdWU7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHRoaXMucGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWUgPyB0aGlzLnBhcmFtcy52YWx1ZSA6IG51bGw7XHJcbiAgICB0aGlzLnJvd0lkID0gdGhpcy5wYXJhbXMubm9kZS5pZDtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRWRpdGFibGVGdW5jdGlvbikge1xyXG4gICAgICB0aGlzLmlzUmVhZE9ubHkgPSAhdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRWRpdGFibGVGdW5jdGlvbih0aGlzLnBhcmFtcy5kYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgb25EYXRlQ2hhbmdlcyhkYXRlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWUgPyB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWUgOiBudWxsO1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEoKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5lcnJvcnMgJiYgKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5kaXJ0eSB8fCB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudG91Y2hlZCkpKTtcclxuICB9XHJcbn1cclxuIl19