/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
export class LovEditCellComponent {
    /**
     * @param {?} component
     * @return {?}
     */
    set content(component) {
        if (component) {
            this.setLovComponent(component);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // console.log(params);
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        this.dataSource = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        // console.log(params);
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
            else {
                console.error(this.params.colDef.field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + 'lov Setting is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        // console.log('patch-value', this.value);
        if (this.formGroup && this.params.colDef.cellEditorParams.customFunctionForRowValueChanges) {
            this.clearRowChangesSubscription();
            this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
            setTimeout(() => {
            });
            this.rowChangesSubscription = this.formGroup.valueChanges
                .subscribe((rowData) => {
                this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
            });
        }
        return true;
    }
    /**
     * @param {?} component
     * @return {?}
     */
    setLovComponent(component) {
        this.listOfValue = component;
        if (this.settings) {
            this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField, this.settings.dontInitData);
        }
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    lovValueChanged(event) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        // console.log('lovValueChanged', this.params);
        if (event && this.listOfValue) {
            ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push({
                Id: this.listOfValue.listOfValuesService.endPointUrl,
                Value: event
            });
            this.dataSource = this.params.colDef.cellEditorParams.dataSource;
            // console.log('dataSource', this.dataSource);
        }
        this.params.setValue(this.value);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onAddToDataSource(data) {
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push(data);
        this.dataSource = this.params.colDef.cellEditorParams.dataSource;
    }
    /**
     * @return {?}
     */
    clearRowChangesSubscription() {
        if (this.rowChangesSubscription) {
            this.rowChangesSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearRowChangesSubscription();
    }
}
LovEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-lov-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-list-of-values
      [class.not-valid]="!isValid()"
      #listOfValue
      [dataSource]="dataSource"
      [isReadOnly]="isReadOnly"
      [formControlName]="key"
      (lovValueChanged)="lovValueChanged($event)"
      (addToDataSource)="onAddToDataSource($event)"
      ></layout-list-of-values>
    </div>
  `,
                styles: [".not-valid input{border:1px solid red}:host /deep/ .lov-btn-container{margin-top:3px}"]
            }] }
];
LovEditCellComponent.propDecorators = {
    content: [{ type: ViewChild, args: ['listOfValue',] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.listOfValue;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.settings;
    /** @type {?} */
    LovEditCellComponent.prototype.formGroup;
    /** @type {?} */
    LovEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.params;
    /** @type {?} */
    LovEditCellComponent.prototype.isReadOnly;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.rowChangesSubscription;
    /** @type {?} */
    LovEditCellComponent.prototype.dataSource;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWVkaXQtY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9lZGl0L2xvdi1lZGl0LWNlbGwvbG92LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBNEJoRSxNQUFNLE9BQU8sb0JBQW9COzs7OztJQUMvQixJQUE4QixPQUFPLENBQUMsU0FBYztRQUNsRCxJQUFJLFNBQVMsRUFBRTtZQUNiLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7OztJQWlCRCxNQUFNLENBQUMsTUFBVztRQUNoQix1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUVqQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBQSxDQUFDLENBQUM7SUFDNUUsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsTUFBVztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQix1QkFBdUI7UUFFdkIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUN2QyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7YUFDOUQ7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsNEJBQTRCLENBQUMsQ0FBQzthQUN4RTtTQUNGO2FBQU07WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyw0QkFBNEIsQ0FBQyxDQUFDO1NBQ3hFO1FBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7UUFFRCwwQ0FBMEM7UUFFMUMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxFQUFFO1lBQzFGLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTNFLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDaEIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2lCQUN0RCxTQUFTLENBQUMsQ0FBQyxPQUFZLEVBQUUsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsZ0NBQWdDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUdELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsU0FBZ0M7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUM7UUFDN0IsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDcEk7SUFDSCxDQUFDOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxLQUFVO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDM0YsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRTtZQUNuRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3JEO1FBQ0QsK0NBQStDO1FBRS9DLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDN0IsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDM0QsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsV0FBVztnQkFDcEQsS0FBSyxFQUFFLEtBQUs7YUFDYixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztZQUNqRSw4Q0FBOEM7U0FDL0M7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxJQUFTO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUU7WUFDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztTQUNyRDtRQUVELENBQUMsbUJBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7SUFDbkUsQ0FBQzs7OztJQUVELDJCQUEyQjtRQUN6QixJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUMvQixJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDM0M7SUFDSCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO0lBQ3JDLENBQUM7OztZQTNJRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdDQUFnQztnQkFDMUMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7OztHQWNUOzthQUVGOzs7c0JBRUUsU0FBUyxTQUFDLGFBQWE7Ozs7Ozs7SUFNeEIsMkNBQTJDOzs7OztJQUMzQyx3Q0FBOEI7O0lBRTlCLHlDQUE0Qjs7SUFDNUIsbUNBQVc7Ozs7O0lBQ1gscUNBQWM7Ozs7O0lBQ2QscUNBQW1COzs7OztJQUNuQixzQ0FBb0I7O0lBRXBCLDBDQUEyQjs7Ozs7SUFFM0Isc0RBQTZDOztJQUU3QywwQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IExpc3RPZlZhbHVlc0NvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uLy4uL2xpc3Qtb2YtdmFsdWVzL2xpc3Qtb2YtdmFsdWVzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvdlNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL2xvdi1zZXR0aW5ncy5tb2RlbCc7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1sb3YtZWRpdC1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdlxyXG4gICAgICAqbmdJZj1cImZvcm1Hcm91cFwiXHJcbiAgICAgIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCI+XHJcbiAgICAgIDxsYXlvdXQtbGlzdC1vZi12YWx1ZXNcclxuICAgICAgW2NsYXNzLm5vdC12YWxpZF09XCIhaXNWYWxpZCgpXCJcclxuICAgICAgI2xpc3RPZlZhbHVlXHJcbiAgICAgIFtkYXRhU291cmNlXT1cImRhdGFTb3VyY2VcIlxyXG4gICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICAobG92VmFsdWVDaGFuZ2VkKT1cImxvdlZhbHVlQ2hhbmdlZCgkZXZlbnQpXCJcclxuICAgICAgKGFkZFRvRGF0YVNvdXJjZSk9XCJvbkFkZFRvRGF0YVNvdXJjZSgkZXZlbnQpXCJcclxuICAgICAgPjwvbGF5b3V0LWxpc3Qtb2YtdmFsdWVzPlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9sb3YtZWRpdC1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIExvdkVkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wLCBPbkRlc3Ryb3kge1xyXG4gIEBWaWV3Q2hpbGQoJ2xpc3RPZlZhbHVlJykgc2V0IGNvbnRlbnQoY29tcG9uZW50OiBhbnkpIHtcclxuICAgIGlmIChjb21wb25lbnQpIHtcclxuICAgICAgdGhpcy5zZXRMb3ZDb21wb25lbnQoY29tcG9uZW50KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgbGlzdE9mVmFsdWU6IExpc3RPZlZhbHVlc0NvbXBvbmVudDtcclxuICBwcml2YXRlIHNldHRpbmdzOiBMb3ZTZXR0aW5ncztcclxuXHJcbiAgcHVibGljIGZvcm1Hcm91cDogRm9ybUFycmF5O1xyXG4gIHB1YmxpYyBrZXk7XHJcbiAgcHJpdmF0ZSB2YWx1ZTtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuXHJcbiAgcHVibGljIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcblxyXG4gIHByaXZhdGUgcm93Q2hhbmdlc1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwdWJsaWMgZGF0YVNvdXJjZTogYW55W107XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSkge1xyXG4gICAgLy8gY29uc29sZS5sb2cocGFyYW1zKTtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlID8gK3RoaXMucGFyYW1zLnZhbHVlIDogbnVsbDtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZGF0YVNvdXJjZSA9ICg8YW55W10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSk7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIC8vIGNvbnNvbGUubG9nKHBhcmFtcyk7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbERlZi5maWVsZCArICdsb3YgU2V0dGluZyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbERlZi5maWVsZCArICdsb3YgU2V0dGluZyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gY29uc29sZS5sb2coJ3BhdGNoLXZhbHVlJywgdGhpcy52YWx1ZSk7XHJcblxyXG4gICAgaWYgKHRoaXMuZm9ybUdyb3VwICYmIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUZ1bmN0aW9uRm9yUm93VmFsdWVDaGFuZ2VzKSB7XHJcbiAgICAgIHRoaXMuY2xlYXJSb3dDaGFuZ2VzU3Vic2NyaXB0aW9uKCk7XHJcbiAgICAgIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUZ1bmN0aW9uRm9yUm93VmFsdWVDaGFuZ2VzKHRoaXMpO1xyXG5cclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnJvd0NoYW5nZXNTdWJzY3JpcHRpb24gPSB0aGlzLmZvcm1Hcm91cC52YWx1ZUNoYW5nZXNcclxuICAgICAgICAuc3Vic2NyaWJlKChyb3dEYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUZ1bmN0aW9uRm9yUm93VmFsdWVDaGFuZ2VzKHRoaXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHNldExvdkNvbXBvbmVudChjb21wb25lbnQ6IExpc3RPZlZhbHVlc0NvbXBvbmVudCkge1xyXG4gICAgdGhpcy5saXN0T2ZWYWx1ZSA9IGNvbXBvbmVudDtcclxuICAgIGlmICh0aGlzLnNldHRpbmdzKSB7XHJcbiAgICAgIHRoaXMubGlzdE9mVmFsdWUuc2V0T3B0aW9ucyh0aGlzLnNldHRpbmdzLm9wdGlvbnMsIHRoaXMuc2V0dGluZ3MudmFsdWVGaWVsZCwgdGhpcy5zZXR0aW5ncy5sYWJlbEZpZWxkLCB0aGlzLnNldHRpbmdzLmRvbnRJbml0RGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEoKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5lcnJvcnMgJiYgKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5kaXJ0eSB8fCB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudG91Y2hlZCkpKTtcclxuICB9XHJcblxyXG4gIGxvdlZhbHVlQ2hhbmdlZChldmVudDogYW55KSB7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlID8gK3RoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZSA6IG51bGw7XHJcbiAgICBpZiAoIXRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpIHtcclxuICAgICAgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSA9IFtdO1xyXG4gICAgfVxyXG4gICAgLy8gY29uc29sZS5sb2coJ2xvdlZhbHVlQ2hhbmdlZCcsIHRoaXMucGFyYW1zKTtcclxuXHJcbiAgICBpZiAoZXZlbnQgJiYgdGhpcy5saXN0T2ZWYWx1ZSkge1xyXG4gICAgICAoPGFueVtdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpLnB1c2goe1xyXG4gICAgICAgIElkOiB0aGlzLmxpc3RPZlZhbHVlLmxpc3RPZlZhbHVlc1NlcnZpY2UuZW5kUG9pbnRVcmwsXHJcbiAgICAgICAgVmFsdWU6IGV2ZW50XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgdGhpcy5kYXRhU291cmNlID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZTtcclxuICAgICAgLy8gY29uc29sZS5sb2coJ2RhdGFTb3VyY2UnLCB0aGlzLmRhdGFTb3VyY2UpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBvbkFkZFRvRGF0YVNvdXJjZShkYXRhOiBhbnkpIHtcclxuICAgIGlmICghdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgKDxhbnlbXT50aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKS5wdXNoKGRhdGEpO1xyXG4gICAgdGhpcy5kYXRhU291cmNlID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZTtcclxuICB9XHJcblxyXG4gIGNsZWFyUm93Q2hhbmdlc1N1YnNjcmlwdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnJvd0NoYW5nZXNTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5yb3dDaGFuZ2VzU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xlYXJSb3dDaGFuZ2VzU3Vic2NyaXB0aW9uKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==