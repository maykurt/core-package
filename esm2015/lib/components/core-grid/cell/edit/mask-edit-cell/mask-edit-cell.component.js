/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class MaskEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value.toString() : '';
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.maskType = this.settings.maskType;
            }
            else {
                console.error(this.params.colDef.field + ' settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' settings is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
MaskEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-mask-edit-cell',
                template: `
    <div *ngIf="formGroup" [formGroup]="formGroup">
    <layout-form-mask-input
      [class.not-valid]="!isValid()"
      [formControlName]="key"
      [maskType]="maskType"
      [isReadOnly]="isReadOnly"
      (changed)="onValueChanged()"
      [isReadOnly]="true">
    </layout-form-mask-input>
    </div>
  `,
                styles: [""]
            }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaskEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    MaskEditCellComponent.prototype.rowId;
    /** @type {?} */
    MaskEditCellComponent.prototype.formGroup;
    /** @type {?} */
    MaskEditCellComponent.prototype.key;
    /** @type {?} */
    MaskEditCellComponent.prototype.value;
    /** @type {?} */
    MaskEditCellComponent.prototype.settings;
    /** @type {?} */
    MaskEditCellComponent.prototype.maskType;
    /** @type {?} */
    MaskEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC9tYXNrLWVkaXQtY2VsbC9tYXNrLWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUF3QjFDLE1BQU0sT0FBTyxxQkFBcUI7Ozs7O0lBV2hDLE1BQU0sQ0FBQyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ25FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBRWpDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLEVBQUU7WUFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakc7SUFDSCxDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2dCQUM3RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO2FBQ3hDO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLDBCQUEwQixDQUFDLENBQUM7YUFDdEU7U0FDRjthQUFNO1lBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsMEJBQTBCLENBQUMsQ0FBQztTQUN0RTtRQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsY0FBYztRQUNaLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELE9BQU87UUFDTCxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0gsQ0FBQzs7O1lBbEVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUNBQWlDO2dCQUMzQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7O0dBV1Q7O2FBRUY7Ozs7Ozs7SUFFQyx1Q0FBb0I7Ozs7O0lBQ3BCLHNDQUFtQjs7SUFDbkIsMENBQTRCOztJQUM1QixvQ0FBVzs7SUFDWCxzQ0FBYTs7SUFDYix5Q0FBOEI7O0lBQzlCLHlDQUEwQjs7SUFFMUIsMkNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IE1hc2tUeXBlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvbWFzay10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBNYXNrU2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvbWFzay1zZXR0aW5ncy5tb2RlbCc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLW1hc2stZWRpdC1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiAqbmdJZj1cImZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCI+XHJcbiAgICA8bGF5b3V0LWZvcm0tbWFzay1pbnB1dFxyXG4gICAgICBbY2xhc3Mubm90LXZhbGlkXT1cIiFpc1ZhbGlkKClcIlxyXG4gICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cImtleVwiXHJcbiAgICAgIFttYXNrVHlwZV09XCJtYXNrVHlwZVwiXHJcbiAgICAgIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAoY2hhbmdlZCk9XCJvblZhbHVlQ2hhbmdlZCgpXCJcclxuICAgICAgW2lzUmVhZE9ubHldPVwidHJ1ZVwiPlxyXG4gICAgPC9sYXlvdXQtZm9ybS1tYXNrLWlucHV0PlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9tYXNrLWVkaXQtY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXNrRWRpdENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuICBwdWJsaWMgc2V0dGluZ3M6IE1hc2tTZXR0aW5ncztcclxuICBwdWJsaWMgbWFza1R5cGU6IE1hc2tUeXBlO1xyXG5cclxuICBwdWJsaWMgaXNSZWFkT25seTogYm9vbGVhbjtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZSA/IHRoaXMucGFyYW1zLnZhbHVlLnRvU3RyaW5nKCkgOiAnJztcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgICAgICB0aGlzLm1hc2tUeXBlID0gdGhpcy5zZXR0aW5ncy5tYXNrVHlwZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbERlZi5maWVsZCArICcgc2V0dGluZ3MgaXMgbm90IGRlZmluZWQnKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2xEZWYuZmllbGQgKyAnIHNldHRpbmdzIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgb25WYWx1ZUNoYW5nZWQoKSB7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlO1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEoKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5lcnJvcnMgJiYgKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5kaXJ0eSB8fCB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudG91Y2hlZCkpKTtcclxuICB9XHJcbn1cclxuIl19