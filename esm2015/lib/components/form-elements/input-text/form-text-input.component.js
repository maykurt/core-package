/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export class FormTextInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
        this.numberOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeypress(event) {
        if (this.disableSpace) {
            if (event && event.code === 'Space') {
                return false;
            }
        }
        if (this.numberOnly) {
            /** @type {?} */
            const charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        return true;
    }
}
FormTextInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-text-input',
                template: `
  <div style="width:100%;">
    <input type="text"
           class="form-control form-control-sm fixed-height-input form-input"
           name="inputValue"
           autocomplete="off"
           [(ngModel)]="inputValue"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           [class.pr-25]="clearable && _value"
           [attr.maxlength]="maxLength"
           placeholder="{{placeholder|translate}}"
           [hidden]="hidden"
           (keypress)="onKeypress($event)"
           >
    <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormTextInputComponent),
                        multi: true
                    }
                ],
                styles: [""]
            }] }
];
FormTextInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    clearable: [{ type: Input }],
    disableSpace: [{ type: Input }],
    maxLength: [{ type: Input }],
    changed: [{ type: Output }],
    numberOnly: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormTextInputComponent.prototype.placeholder;
    /** @type {?} */
    FormTextInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormTextInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormTextInputComponent.prototype.hidden;
    /** @type {?} */
    FormTextInputComponent.prototype.clearable;
    /** @type {?} */
    FormTextInputComponent.prototype.disableSpace;
    /** @type {?} */
    FormTextInputComponent.prototype.maxLength;
    /** @type {?} */
    FormTextInputComponent.prototype.changed;
    /** @type {?} */
    FormTextInputComponent.prototype.numberOnly;
    /** @type {?} */
    FormTextInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS10ZXh0LWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtdGV4dC9mb3JtLXRleHQtaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQXdCLE1BQU0sZ0JBQWdCLENBQUM7QUE4QnpFLE1BQU0sT0FBTyxzQkFBc0I7SUE1Qm5DO1FBK0JXLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUdqQixZQUFPLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakQsZUFBVSxHQUFHLEtBQUssQ0FBQztJQWlEOUIsQ0FBQzs7Ozs7SUE1Q0MsUUFBUSxDQUFDLEdBQVE7SUFDakIsQ0FBQzs7OztJQUVELFNBQVM7SUFDVCxDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsSUFBSSxVQUFVLENBQUMsR0FBRztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBVTtRQUNuQixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxPQUFPLEVBQUU7Z0JBQ25DLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTs7a0JBQ2IsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTztZQUM1RCxJQUFJLFFBQVEsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRTtnQkFDckQsT0FBTyxLQUFLLENBQUM7YUFDZDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7WUF0RkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FpQlQ7Z0JBRUQsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsc0JBQXNCLENBQUM7d0JBQ3JELEtBQUssRUFBRSxJQUFJO3FCQUNaO2lCQUFDOzthQUNMOzs7MEJBR0UsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7cUJBQ0wsS0FBSzt3QkFDTCxLQUFLOzJCQUNMLEtBQUs7d0JBQ0wsS0FBSztzQkFDTCxNQUFNO3lCQUNOLEtBQUs7Ozs7SUFSTiw2Q0FBOEI7O0lBQzlCLDRDQUE0Qjs7SUFDNUIsNENBQTRCOztJQUM1Qix3Q0FBd0I7O0lBQ3hCLDJDQUEyQjs7SUFDM0IsOENBQWdDOztJQUNoQywyQ0FBNEI7O0lBQzVCLHlDQUEwRDs7SUFDMUQsNENBQTRCOztJQUc1Qix3Q0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgZm9yd2FyZFJlZiwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWZvcm0tdGV4dC1pbnB1dCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICA8ZGl2IHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZm9ybS1jb250cm9sLXNtIGZpeGVkLWhlaWdodC1pbnB1dCBmb3JtLWlucHV0XCJcclxuICAgICAgICAgICBuYW1lPVwiaW5wdXRWYWx1ZVwiXHJcbiAgICAgICAgICAgYXV0b2NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgIFtkaXNhYmxlZF09XCJpc0Rpc2FibGVkXCJcclxuICAgICAgICAgICBbcmVhZG9ubHldPVwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgICAgW2NsYXNzLnByLTI1XT1cImNsZWFyYWJsZSAmJiBfdmFsdWVcIlxyXG4gICAgICAgICAgIFthdHRyLm1heGxlbmd0aF09XCJtYXhMZW5ndGhcIlxyXG4gICAgICAgICAgIHBsYWNlaG9sZGVyPVwie3twbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXHJcbiAgICAgICAgICAgW2hpZGRlbl09XCJoaWRkZW5cIlxyXG4gICAgICAgICAgIChrZXlwcmVzcyk9XCJvbktleXByZXNzKCRldmVudClcIlxyXG4gICAgICAgICAgID5cclxuICAgIDxjb3JlLWljb24gaWNvbj1cInRpbWVzXCIgKm5nSWY9XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCIgY2xhc3M9XCJzZWFyY2hjbGVhclwiIChjbGljayk9XCJpbnB1dFZhbHVlID0gbnVsbFwiPjwvY29yZS1pY29uPlxyXG4gIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS10ZXh0LWlucHV0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtVGV4dElucHV0Q29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtVGV4dElucHV0Q29tcG9uZW50IGltcGxlbWVudHMgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG5cclxuICBASW5wdXQoKSBwbGFjZWhvbGRlcj86IHN0cmluZztcclxuICBASW5wdXQoKSBpc0Rpc2FibGVkID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGhpZGRlbiA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGNsZWFyYWJsZSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGRpc2FibGVTcGFjZT86IGJvb2xlYW47XHJcbiAgQElucHV0KCkgbWF4TGVuZ3RoPzogbnVtYmVyO1xyXG4gIEBPdXRwdXQoKSBjaGFuZ2VkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBASW5wdXQoKSBudW1iZXJPbmx5ID0gZmFsc2U7XHJcblxyXG5cclxuICBfdmFsdWU6IHN0cmluZztcclxuXHJcbiAgb25DaGFuZ2UodmFsOiBhbnkpIHtcclxuICB9XHJcblxyXG4gIG9uVG91Y2hlZCgpIHtcclxuICB9XHJcblxyXG4gIGdldCBpbnB1dFZhbHVlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IGlucHV0VmFsdWUodmFsKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbDtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsKTtcclxuICAgIHRoaXMub25Ub3VjaGVkKCk7XHJcbiAgICB0aGlzLmNoYW5nZWQuZW1pdCh0aGlzLl92YWx1ZSk7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICBvbktleXByZXNzKGV2ZW50OiBhbnkpOiBib29sZWFuIHtcclxuICAgIGlmICh0aGlzLmRpc2FibGVTcGFjZSkge1xyXG4gICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuY29kZSA9PT0gJ1NwYWNlJykge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLm51bWJlck9ubHkpIHtcclxuICAgICAgY29uc3QgY2hhckNvZGUgPSAoZXZlbnQud2hpY2gpID8gZXZlbnQud2hpY2ggOiBldmVudC5rZXlDb2RlO1xyXG4gICAgICBpZiAoY2hhckNvZGUgPiAzMSAmJiAoY2hhckNvZGUgPCA0OCB8fCBjaGFyQ29kZSA+IDU3KSkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG59XHJcbiJdfQ==