/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export class FormNumberInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.maxLength && this.maxLength) {
            /** @type {?} */
            let value = '';
            for (let i = 0; i < this.maxLength; i++) {
                value += '1';
            }
            this.maxValue = parseInt(value, 10) * 9;
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyUp(event) {
        if (this.maxValue && this.maxLength && this.inputValue && this.inputValue > this.maxValue) {
            this.inputValue = parseInt(this.inputValue.toString().slice(0, this.maxLength), 10);
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    numberOnly(event) {
        if (event && event.code === 'KeyE') {
            return false;
        }
        return true;
    }
}
FormNumberInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-number-input',
                template: `
    <div style="width:100%;">
      <input type="number"
             class="form-control form-control-sm fixed-height-input form-input"
             name="inputValue"
             autocomplete="off"
             [(ngModel)]="inputValue"
             [disabled]="isDisabled"
             [readonly]="isReadOnly"
             [class.pr-25]="clearable && _value"
             (keyup)="onKeyUp($event)"
             (keypress)="numberOnly($event)"
              placeholder="{{placeholder|translate}}"
             [hidden]="hidden">
      <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
    </div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormNumberInputComponent),
                        multi: true
                    }
                ],
                styles: ["input::-webkit-inner-spin-button,input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}"]
            }] }
];
FormNumberInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }],
    maxLength: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormNumberInputComponent.prototype.placeholder;
    /** @type {?} */
    FormNumberInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormNumberInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormNumberInputComponent.prototype.hidden;
    /** @type {?} */
    FormNumberInputComponent.prototype.clearable;
    /** @type {?} */
    FormNumberInputComponent.prototype.changed;
    /** @type {?} */
    FormNumberInputComponent.prototype.maxLength;
    /**
     * @type {?}
     * @private
     */
    FormNumberInputComponent.prototype.maxValue;
    /** @type {?} */
    FormNumberInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1udW1iZXItaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZm9ybS1lbGVtZW50cy9pbnB1dC1udW1iZXIvZm9ybS1udW1iZXItaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDN0csT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBNkJ6RSxNQUFNLE9BQU8sd0JBQXdCO0lBM0JyQztRQThCVyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDMUIsWUFBTyxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBNEQ1RCxDQUFDOzs7OztJQXBEQyxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7O2dCQUNuQyxLQUFLLEdBQUcsRUFBRTtZQUNkLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxLQUFLLElBQUksR0FBRyxDQUFDO2FBQ2Q7WUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQsU0FBUztJQUNULENBQUM7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxJQUFJLFVBQVUsQ0FBQyxHQUFHO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBYTtRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFHRCxPQUFPLENBQUMsS0FBVTtRQUNoQixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUN6RixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3JGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBVTtRQUNuQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtZQUNsQyxPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7WUE3RkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwwQkFBMEI7Z0JBQ3BDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7OztHQWdCVDtnQkFFRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDdkQsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQUM7O2FBQ0w7OzswQkFHRSxLQUFLO3lCQUNMLEtBQUs7eUJBQ0wsS0FBSztxQkFDTCxLQUFLO3dCQUNMLEtBQUs7c0JBQ0wsTUFBTTt3QkFFTixLQUFLOzs7O0lBUE4sK0NBQThCOztJQUM5Qiw4Q0FBNEI7O0lBQzVCLDhDQUE0Qjs7SUFDNUIsMENBQXdCOztJQUN4Qiw2Q0FBb0M7O0lBQ3BDLDJDQUEwRDs7SUFFMUQsNkNBQTRCOzs7OztJQUU1Qiw0Q0FBeUI7O0lBRXpCLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBmb3J3YXJkUmVmLCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiwgQ29udHJvbFZhbHVlQWNjZXNzb3IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1mb3JtLW51bWJlci1pbnB1dCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cIm51bWJlclwiXHJcbiAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbCBmb3JtLWNvbnRyb2wtc20gZml4ZWQtaGVpZ2h0LWlucHV0IGZvcm0taW5wdXRcIlxyXG4gICAgICAgICAgICAgbmFtZT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgICAgYXV0b2NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICAgIFsobmdNb2RlbCldPVwiaW5wdXRWYWx1ZVwiXHJcbiAgICAgICAgICAgICBbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICAgICAgICAgICBbcmVhZG9ubHldPVwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgICAgICBbY2xhc3MucHItMjVdPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiXHJcbiAgICAgICAgICAgICAoa2V5dXApPVwib25LZXlVcCgkZXZlbnQpXCJcclxuICAgICAgICAgICAgIChrZXlwcmVzcyk9XCJudW1iZXJPbmx5KCRldmVudClcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwie3twbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXHJcbiAgICAgICAgICAgICBbaGlkZGVuXT1cImhpZGRlblwiPlxyXG4gICAgICA8Y29yZS1pY29uIGljb249XCJ0aW1lc1wiICpuZ0lmPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiIGNsYXNzPVwic2VhcmNoY2xlYXJcIiAoY2xpY2spPVwiaW5wdXRWYWx1ZSA9IG51bGxcIj48L2NvcmUtaWNvbj5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS1udW1iZXItaW5wdXQuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1OdW1iZXJJbnB1dENvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybU51bWJlcklucHV0Q29tcG9uZW50IGltcGxlbWVudHMgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE9uQ2hhbmdlcyB7XHJcblxyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlzRGlzYWJsZWQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBpc1JlYWRPbmx5ID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaGlkZGVuID0gZmFsc2U7XHJcbiAgQElucHV0KCkgY2xlYXJhYmxlOiBCb29sZWFuID0gZmFsc2U7XHJcbiAgQE91dHB1dCgpIGNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBASW5wdXQoKSBtYXhMZW5ndGg/OiBudW1iZXI7XHJcblxyXG4gIHByaXZhdGUgbWF4VmFsdWU6IG51bWJlcjtcclxuXHJcbiAgX3ZhbHVlOiBudW1iZXI7XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzLm1heExlbmd0aCAmJiB0aGlzLm1heExlbmd0aCkge1xyXG4gICAgICBsZXQgdmFsdWUgPSAnJztcclxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1heExlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgdmFsdWUgKz0gJzEnO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMubWF4VmFsdWUgPSBwYXJzZUludCh2YWx1ZSwgMTApICogOTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgaW5wdXRWYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBpbnB1dFZhbHVlKHZhbCkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWw7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gICAgdGhpcy5jaGFuZ2VkLmVtaXQodGhpcy5fdmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgd3JpdGVWYWx1ZSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuXHJcbiAgb25LZXlVcChldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5tYXhWYWx1ZSAmJiB0aGlzLm1heExlbmd0aCAmJiB0aGlzLmlucHV0VmFsdWUgJiYgdGhpcy5pbnB1dFZhbHVlID4gdGhpcy5tYXhWYWx1ZSkge1xyXG4gICAgICB0aGlzLmlucHV0VmFsdWUgPSBwYXJzZUludCh0aGlzLmlucHV0VmFsdWUudG9TdHJpbmcoKS5zbGljZSgwLCB0aGlzLm1heExlbmd0aCksIDEwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG51bWJlck9ubHkoZXZlbnQ6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKGV2ZW50ICYmIGV2ZW50LmNvZGUgPT09ICdLZXlFJykge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbn1cclxuIl19