/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export class FormTextAreaComponent {
    constructor() {
        this.isReadOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    }
}
FormTextAreaComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-textarea',
                template: `
    <textarea name="inputValue" class="form-textarea"
              id="inputValue" style="min-width: 100%;"
              rows="5"
              [attr.maxlength]="maxLength"
              [class.readonly-input] = "isReadOnly"
              [readonly]="isReadOnly"
              [(ngModel)]="inputValue"
              (keydown)="onKeyDown($event)"
              placeholder="{{placeholder|translate}}"
              ></textarea>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormTextAreaComponent),
                        multi: true
                    }
                ],
                styles: [".readonly-input{background-color:#e9ecef;cursor:default}"]
            }] }
];
FormTextAreaComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    maxLength: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormTextAreaComponent.prototype.placeholder;
    /** @type {?} */
    FormTextAreaComponent.prototype.isReadOnly;
    /** @type {?} */
    FormTextAreaComponent.prototype.maxLength;
    /**
     * @type {?}
     * @private
     */
    FormTextAreaComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS10ZXh0YXJlYS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLWVsZW1lbnRzL2lucHV0LXRleHRhcmVhL2Zvcm0tdGV4dGFyZWEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBd0J6RSxNQUFNLE9BQU8scUJBQXFCO0lBdEJsQztRQXdCVyxlQUFVLEdBQUcsS0FBSyxDQUFDO0lBdUM5QixDQUFDOzs7OztJQWpDQyxRQUFRLENBQUMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQsU0FBUztJQUNULENBQUM7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxJQUFJLFVBQVUsQ0FBQyxHQUFHO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVU7UUFDbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7OztZQTlERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsUUFBUSxFQUFFOzs7Ozs7Ozs7OztHQVdUO2dCQUVELFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHFCQUFxQixDQUFDO3dCQUNwRCxLQUFLLEVBQUUsSUFBSTtxQkFDWjtpQkFBQzs7YUFDTDs7OzBCQUVFLEtBQUs7eUJBQ0wsS0FBSzt3QkFFTCxLQUFLOzs7O0lBSE4sNENBQXFCOztJQUNyQiwyQ0FBNEI7O0lBRTVCLDBDQUE0Qjs7Ozs7SUFFNUIsdUNBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWZvcm0tdGV4dGFyZWEnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8dGV4dGFyZWEgbmFtZT1cImlucHV0VmFsdWVcIiBjbGFzcz1cImZvcm0tdGV4dGFyZWFcIlxyXG4gICAgICAgICAgICAgIGlkPVwiaW5wdXRWYWx1ZVwiIHN0eWxlPVwibWluLXdpZHRoOiAxMDAlO1wiXHJcbiAgICAgICAgICAgICAgcm93cz1cIjVcIlxyXG4gICAgICAgICAgICAgIFthdHRyLm1heGxlbmd0aF09XCJtYXhMZW5ndGhcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5yZWFkb25seS1pbnB1dF0gPSBcImlzUmVhZE9ubHlcIlxyXG4gICAgICAgICAgICAgIFtyZWFkb25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgICAgIChrZXlkb3duKT1cIm9uS2V5RG93bigkZXZlbnQpXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7cGxhY2Vob2xkZXJ8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgICAgICAgID48L3RleHRhcmVhPlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS10ZXh0YXJlYS5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybVRleHRBcmVhQ29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtVGV4dEFyZWFDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG5cclxuICBASW5wdXQoKSBtYXhMZW5ndGg/OiBudW1iZXI7XHJcblxyXG4gIHByaXZhdGUgX3ZhbHVlO1xyXG5cclxuICBvbkNoYW5nZSh2YWw6IGFueSkge1xyXG4gIH1cclxuXHJcbiAgb25Ub3VjaGVkKCkge1xyXG4gIH1cclxuXHJcbiAgZ2V0IGlucHV0VmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgfVxyXG5cclxuICBzZXQgaW5wdXRWYWx1ZSh2YWwpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIG9uS2V5RG93bihldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5pc1JlYWRPbmx5KSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==