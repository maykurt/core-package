/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export class FormCheckboxComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.checkChanged = new EventEmitter();
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) { }
    /**
     * @return {?}
     */
    onTouched() { }
    /**
     * @return {?}
     */
    get inputValue() {
        return this.value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this.value = val;
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onCheckChanged(event) {
        this.checkChanged.emit(event);
    }
}
FormCheckboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-checkbox',
                template: `
	<div class="form-checkbox" [class.disable-selection]="isReadOnly">
    <p-checkbox
    [hidden]="hidden"
    [(ngModel)]="inputValue"
    [disabled]="isDisabled"
    binary="true"
    (onChange)="onCheckChanged($event)"
    >
    </p-checkbox>
	</div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormCheckboxComponent),
                        multi: true
                    }
                ],
                styles: [":host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-chkbox .ui-chkbox-box{opacity:.6;border:1px solid #a9a9a9!important;background-color:#ced4da!important;color:#000}"]
            }] }
];
FormCheckboxComponent.propDecorators = {
    value: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    checkChanged: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    FormCheckboxComponent.prototype.value;
    /** @type {?} */
    FormCheckboxComponent.prototype.isDisabled;
    /** @type {?} */
    FormCheckboxComponent.prototype.isReadOnly;
    /** @type {?} */
    FormCheckboxComponent.prototype.hidden;
    /** @type {?} */
    FormCheckboxComponent.prototype.checkChanged;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1jaGVja2JveC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLWVsZW1lbnRzL2lucHV0LWNoZWNrYm94L2Zvcm0tY2hlY2tib3guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzRixPQUFPLEVBQUUsaUJBQWlCLEVBQXdCLE1BQU0sZ0JBQWdCLENBQUM7QUF3QnpFLE1BQU0sT0FBTyxxQkFBcUI7SUF0QmxDO1FBeUJXLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2QsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO0lBZ0N2RCxDQUFDOzs7OztJQTdCQyxRQUFRLENBQUMsR0FBUSxJQUFJLENBQUM7Ozs7SUFFdEIsU0FBUyxLQUFLLENBQUM7Ozs7SUFFZixJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQzs7Ozs7SUFFRCxJQUFJLFVBQVUsQ0FBQyxHQUFHO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQWM7UUFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7O1lBM0RGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsc0JBQXNCO2dCQUNoQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7O0dBV1Q7Z0JBRUQsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMscUJBQXFCLENBQUM7d0JBQ3BELEtBQUssRUFBRSxJQUFJO3FCQUNaO2lCQUFDOzthQUNMOzs7b0JBR0UsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7cUJBQ0wsS0FBSzsyQkFDTCxNQUFNOzs7O0lBSlAsc0NBQWU7O0lBQ2YsMkNBQTRCOztJQUM1QiwyQ0FBNEI7O0lBQzVCLHVDQUF3Qjs7SUFDeEIsNkNBQXFEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBmb3J3YXJkUmVmLCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiwgQ29udHJvbFZhbHVlQWNjZXNzb3IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1mb3JtLWNoZWNrYm94JyxcclxuICB0ZW1wbGF0ZTogYFxyXG5cdDxkaXYgY2xhc3M9XCJmb3JtLWNoZWNrYm94XCIgW2NsYXNzLmRpc2FibGUtc2VsZWN0aW9uXT1cImlzUmVhZE9ubHlcIj5cclxuICAgIDxwLWNoZWNrYm94XHJcbiAgICBbaGlkZGVuXT1cImhpZGRlblwiXHJcbiAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgW2Rpc2FibGVkXT1cImlzRGlzYWJsZWRcIlxyXG4gICAgYmluYXJ5PVwidHJ1ZVwiXHJcbiAgICAob25DaGFuZ2UpPVwib25DaGVja0NoYW5nZWQoJGV2ZW50KVwiXHJcbiAgICA+XHJcbiAgICA8L3AtY2hlY2tib3g+XHJcblx0PC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9mb3JtLWNoZWNrYm94LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtQ2hlY2tib3hDb21wb25lbnQpLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1DaGVja2JveENvbXBvbmVudCBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuXHJcbiAgQElucHV0KCkgdmFsdWU7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBoaWRkZW4gPSBmYWxzZTtcclxuICBAT3V0cHV0KCkgY2hlY2tDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG5cclxuXHJcbiAgb25DaGFuZ2UodmFsOiBhbnkpIHsgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7IH1cclxuXHJcbiAgZ2V0IGlucHV0VmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy52YWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBpbnB1dFZhbHVlKHZhbCkge1xyXG4gICAgdGhpcy52YWx1ZSA9IHZhbDtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsKTtcclxuICAgIHRoaXMub25Ub3VjaGVkKCk7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XHJcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIG9uQ2hlY2tDaGFuZ2VkKGV2ZW50OiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmNoZWNrQ2hhbmdlZC5lbWl0KGV2ZW50KTtcclxuICB9XHJcbn1cclxuIl19