/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MaskType } from '../../../enums/mask-type.enum';
import emailMask from 'text-mask-addons/dist/emailMask';
export class FormMaskInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.maskType) {
            if (this.maskType === MaskType.Phone) {
                this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
                this.placeholder = '(__) ___-____';
            }
            else if (this.maskType === MaskType.Mail) {
                this.mask = emailMask;
                // this.placeholder = 'john@smith.com';
                this.placeholder = '';
            }
            else if (this.maskType === MaskType.PostCode) {
                this.mask = [/\d/, /\d/, /\d/, /\d/, /\d/];
                // this.placeholder = '38000';
                this.placeholder = '';
            }
        }
        if (changes.placeholder && changes.placeholder.currentValue) {
            this.placeholder = changes.placeholder.currentValue;
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        if (val) {
            if (this.maskType === MaskType.Phone) {
                val = val.replace(/\D+/g, '');
            }
        }
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
}
FormMaskInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-mask-input',
                template: `

  <div style="width:100%;">
    <input type="text"
           class="form-control form-control-sm fixed-height-input form-input"
           name="inputValue"
           autocomplete="off"
           type="text"
           [textMask]="{mask: mask}"
           [(ngModel)]="inputValue"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           [class.pr-25]="clearable && _value"
           [hidden]="hidden"
           [placeholder]="placeholder"
           >
    <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>
`,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormMaskInputComponent),
                        multi: true
                    }
                ]
            }] }
];
/** @nocollapse */
FormMaskInputComponent.ctorParameters = () => [];
FormMaskInputComponent.propDecorators = {
    maskType: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    placeholder: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    FormMaskInputComponent.prototype.maskType;
    /** @type {?} */
    FormMaskInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormMaskInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormMaskInputComponent.prototype.hidden;
    /** @type {?} */
    FormMaskInputComponent.prototype.placeholder;
    /** @type {?} */
    FormMaskInputComponent.prototype.clearable;
    /** @type {?} */
    FormMaskInputComponent.prototype.changed;
    /** @type {?} */
    FormMaskInputComponent.prototype._value;
    /** @type {?} */
    FormMaskInputComponent.prototype.mask;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1tYXNrLWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtbWFzay9mb3JtLW1hc2staW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQW9DLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckgsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUV6RCxPQUFPLFNBQVMsTUFBTSxpQ0FBaUMsQ0FBQztBQThCeEQsTUFBTSxPQUFPLHNCQUFzQjtJQWVqQztRQWJTLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBRWYsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMxQixZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQVM1QyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDcEIsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxLQUFLLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ2hHLElBQUksQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDO2FBQ3BDO2lCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsSUFBSSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztnQkFDdEIsdUNBQXVDO2dCQUN2QyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzthQUN2QjtpQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLFFBQVEsRUFBRTtnQkFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDM0MsOEJBQThCO2dCQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzthQUN2QjtTQUNGO1FBRUQsSUFBSSxPQUFPLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQzNELElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7U0FDckQ7SUFDSCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCxTQUFTO0lBQ1QsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELElBQUksVUFBVSxDQUFDLEdBQUc7UUFDaEIsSUFBSSxHQUFHLEVBQUU7WUFDUCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDcEMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQy9CO1NBQ0Y7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7O1lBcEdGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsd0JBQXdCO2dCQUNsQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQWtCWDtnQkFDQyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzt3QkFDckQsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQUM7YUFDTDs7Ozs7dUJBRUUsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7cUJBQ0wsS0FBSzswQkFDTCxLQUFLO3dCQUNMLEtBQUs7c0JBQ0wsTUFBTTs7OztJQU5QLDBDQUE0Qjs7SUFDNUIsNENBQTRCOztJQUM1Qiw0Q0FBNEI7O0lBQzVCLHdDQUF3Qjs7SUFDeEIsNkNBQThCOztJQUM5QiwyQ0FBb0M7O0lBQ3BDLHlDQUE0Qzs7SUFHNUMsd0NBQWU7O0lBQ2Ysc0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIFNpbXBsZUNoYW5nZXMsIE9uQ2hhbmdlcywgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXNrVHlwZSB9IGZyb20gJy4uLy4uLy4uL2VudW1zL21hc2stdHlwZS5lbnVtJztcclxuXHJcbmltcG9ydCBlbWFpbE1hc2sgZnJvbSAndGV4dC1tYXNrLWFkZG9ucy9kaXN0L2VtYWlsTWFzayc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1mb3JtLW1hc2staW5wdXQnLFxyXG4gIHRlbXBsYXRlOiBgXHJcblxyXG4gIDxkaXYgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbCBmb3JtLWNvbnRyb2wtc20gZml4ZWQtaGVpZ2h0LWlucHV0IGZvcm0taW5wdXRcIlxyXG4gICAgICAgICAgIG5hbWU9XCJpbnB1dFZhbHVlXCJcclxuICAgICAgICAgICBhdXRvY29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICBbdGV4dE1hc2tdPVwie21hc2s6IG1hc2t9XCJcclxuICAgICAgICAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgIFtkaXNhYmxlZF09XCJpc0Rpc2FibGVkXCJcclxuICAgICAgICAgICBbcmVhZG9ubHldPVwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgICAgW2NsYXNzLnByLTI1XT1cImNsZWFyYWJsZSAmJiBfdmFsdWVcIlxyXG4gICAgICAgICAgIFtoaWRkZW5dPVwiaGlkZGVuXCJcclxuICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwicGxhY2Vob2xkZXJcIlxyXG4gICAgICAgICAgID5cclxuICAgIDxjb3JlLWljb24gaWNvbj1cInRpbWVzXCIgKm5nSWY9XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCIgY2xhc3M9XCJzZWFyY2hjbGVhclwiIChjbGljayk9XCJpbnB1dFZhbHVlID0gbnVsbFwiPjwvY29yZS1pY29uPlxyXG4gIDwvZGl2PlxyXG5gLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybU1hc2tJbnB1dENvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybU1hc2tJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcywgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG4gIEBJbnB1dCgpIG1hc2tUeXBlOiBNYXNrVHlwZTtcclxuICBASW5wdXQoKSBpc0Rpc2FibGVkID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGhpZGRlbiA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGNsZWFyYWJsZTogQm9vbGVhbiA9IGZhbHNlO1xyXG4gIEBPdXRwdXQoKSBjaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG5cclxuICBfdmFsdWU6IHN0cmluZztcclxuICBtYXNrOiBhbnk7XHJcblxyXG5cclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICBpZiAoY2hhbmdlcy5tYXNrVHlwZSkge1xyXG4gICAgICBpZiAodGhpcy5tYXNrVHlwZSA9PT0gTWFza1R5cGUuUGhvbmUpIHtcclxuICAgICAgICB0aGlzLm1hc2sgPSBbJygnLCAvWzEtOV0vLCAvXFxkLywgL1xcZC8sICcpJywgJyAnLCAvXFxkLywgL1xcZC8sIC9cXGQvLCAnLScsIC9cXGQvLCAvXFxkLywgL1xcZC8sIC9cXGQvXTtcclxuICAgICAgICB0aGlzLnBsYWNlaG9sZGVyID0gJyhfXykgX19fLV9fX18nO1xyXG4gICAgICB9IGVsc2UgaWYgKHRoaXMubWFza1R5cGUgPT09IE1hc2tUeXBlLk1haWwpIHtcclxuICAgICAgICB0aGlzLm1hc2sgPSBlbWFpbE1hc2s7XHJcbiAgICAgICAgLy8gdGhpcy5wbGFjZWhvbGRlciA9ICdqb2huQHNtaXRoLmNvbSc7XHJcbiAgICAgICAgdGhpcy5wbGFjZWhvbGRlciA9ICcnO1xyXG4gICAgICB9IGVsc2UgaWYgKHRoaXMubWFza1R5cGUgPT09IE1hc2tUeXBlLlBvc3RDb2RlKSB7XHJcbiAgICAgICAgdGhpcy5tYXNrID0gWy9cXGQvLCAvXFxkLywgL1xcZC8sIC9cXGQvLCAvXFxkL107XHJcbiAgICAgICAgLy8gdGhpcy5wbGFjZWhvbGRlciA9ICczODAwMCc7XHJcbiAgICAgICAgdGhpcy5wbGFjZWhvbGRlciA9ICcnO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNoYW5nZXMucGxhY2Vob2xkZXIgJiYgY2hhbmdlcy5wbGFjZWhvbGRlci5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdGhpcy5wbGFjZWhvbGRlciA9IGNoYW5nZXMucGxhY2Vob2xkZXIuY3VycmVudFZhbHVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2UodmFsOiBhbnkpIHtcclxuICB9XHJcblxyXG4gIG9uVG91Y2hlZCgpIHtcclxuICB9XHJcblxyXG4gIGdldCBpbnB1dFZhbHVlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IGlucHV0VmFsdWUodmFsKSB7XHJcbiAgICBpZiAodmFsKSB7XHJcbiAgICAgIGlmICh0aGlzLm1hc2tUeXBlID09PSBNYXNrVHlwZS5QaG9uZSkge1xyXG4gICAgICAgIHZhbCA9IHZhbC5yZXBsYWNlKC9cXEQrL2csICcnKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICAgIHRoaXMuY2hhbmdlZC5lbWl0KHRoaXMuX3ZhbHVlKTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==