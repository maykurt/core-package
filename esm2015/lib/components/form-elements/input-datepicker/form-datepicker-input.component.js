/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import moment from 'moment';
import * as _ from 'lodash';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateSettings } from '../../../models/date-settings.model';
import { DateSelectionMode } from '../../../enums/date-selection-mode.enum';
export class FormDatepickerInputComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.isReadOnly = false;
        this.dateChanges = new EventEmitter();
        this._selectionMode = DateSelectionMode.Single;
        this.onChange = () => { };
        this.onTouched = () => { };
        this.view = 'date';
        this.yearDataSource = [];
        this.defaultSettings = new DateSettings();
        this.formGroup = this.fb.group({
            Year: null
        });
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange.subscribe((event) => {
            if (event) {
                this.locale = event.lang === 'tr' ? this.localeTr : this.localEn;
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.selectionMode && this.selectionMode) {
            this._selectionMode = this.selectionMode;
        }
        if (changes.settings && this.settings) {
            this.defaultSettings = _.assign({}, this.defaultSettings, this.settings);
            if (this.settings.selectionMode && !this.selectionMode) {
                this._selectionMode = this.settings.selectionMode;
            }
            if (this.settings.showYear) {
                this.defaultSettings.dateFormat = 'yy';
                this.initiateYearDataSource();
            }
            if (this.settings.showTimeOnly) {
                this.defaultSettings.disableUTC = true;
                this.defaultSettings.hideCalendarButton = true;
                this.defaultSettings.showButtonBar = false;
            }
            if (this.settings.showTime) {
                this.defaultSettings.disableUTC = true;
            }
            if (this.settings.showMonthPicker) {
                this.view = 'month';
            }
            if (this.settings.minDate) {
                this.minDate = this.convertDateToDateUTC(this.settings.minDate);
            }
            if (this.settings.maxDate) {
                this.maxDate = this.convertDateToDateUTC(this.settings.maxDate);
            }
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        if (value) {
            if (value.indexOf('T') !== -1 && !this.defaultSettings.disableUTC && !this.defaultSettings.showTimeOnly && !this.defaultSettings.showTime) {
                // can be done via moment
                /** @type {?} */
                const splittedValue = value.split('T');
                splittedValue[1] = '00:00:00.000Z';
                value = splittedValue.join('T');
            }
            if (this.selectedDateString !== value) {
                // due to UTC, without being influenced timezone changes, it creates date according to value
                // in order to avoid reflecting timezone changes to UI
                // console.log('value ', value);
                // console.log('moment(value).toDate() ', moment(value).toDate());
                // console.log('this.convertDateToDateUTC(value) ', this.convertDateToDateUTC(value));
                if (!this.usedMometToDate && (this.defaultSettings.showTimeOnly || this.defaultSettings.showTime)) {
                    // first initiation needs to be done this way
                    this._calendarDateValue = moment(value).toDate();
                    this.usedMometToDate = true;
                }
                else {
                    // after the initiation needs to be used this way
                    this._calendarDateValue = this.convertDateToDateUTC(value);
                }
                this.formGroup.setValue({
                    Year: moment(value).format('YYYY')
                });
                this.selectedDateString = value;
            }
        }
        else {
            this._calendarDateValue = null;
            this.selectedDateString = null;
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @return {?}
     */
    get selectedDate() {
        return this.selectedDateString;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set selectedDate(value) {
        this.selectedDateString = value;
        // console.log('selectedDate', this.selectedDateString);
        this.onChange(this.selectedDateString);
        this.dateChanges.emit(this.selectedDateString);
        this.onTouched();
    }
    /**
     * @return {?}
     */
    get calendarDateValue() {
        return this._calendarDateValue;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set calendarDateValue(value) {
        this._calendarDateValue = value;
        this.onChangeDate();
    }
    /**
     * @return {?}
     */
    onChangeDate() {
        if (this._selectionMode === DateSelectionMode.Range && this._calendarDateValue) {
            /** @type {?} */
            const dateRangeList = (/** @type {?} */ ((/** @type {?} */ (this._calendarDateValue))));
            /** @type {?} */
            const newSelectedDateList = [];
            dateRangeList.forEach((date) => {
                if (date) {
                    newSelectedDateList.push(this.convertDateToISO(date));
                }
            });
            if (newSelectedDateList.length === 2) {
                this.selectedDate = newSelectedDateList;
            }
        }
        else {
            if (this.selectedDateString && !this._calendarDateValue) {
                this.selectedDate = null;
            }
            else if (this._calendarDateValue) {
                if (this.defaultSettings.showYear) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                }
                if (this.defaultSettings.showTimeOnly) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                    this._calendarDateValue.setFullYear(this.defaultSettings.defaultYear);
                }
                if (!this.defaultSettings.enableSeconds && this._calendarDateValue.setSeconds) {
                    this._calendarDateValue.setSeconds(0);
                }
                /** @type {?} */
                let calendarDateValueISO = this.convertDateToISO(this._calendarDateValue);
                if (this.defaultSettings.disableUTC) {
                    calendarDateValueISO = moment(this._calendarDateValue).format('YYYY-MM-DDTHH:mm:ss');
                }
                if (calendarDateValueISO !== this.selectedDateString) {
                    this.selectedDate = calendarDateValueISO;
                }
            }
        }
    }
    /**
     * @return {?}
     */
    initiateYearDataSource() {
        /** @type {?} */
        const dataSource = [];
        for (let i = ((new Date()).getFullYear() + 30); i > 1922; i--) {
            dataSource.push({
                Value: i.toString(),
                Label: i.toString()
            });
        }
        dataSource.push({ Value: '2900', Label: '2900' });
        this.yearDataSource = dataSource;
    }
    /**
     * @param {?} selectedData
     * @return {?}
     */
    onYearValueChanged(selectedData) {
        if (selectedData) {
            /** @type {?} */
            let month = (this.defaultSettings.defaultMonth + 1).toString();
            if (month.length < 2) {
                month = '0' + month;
            }
            /** @type {?} */
            let day = (this.defaultSettings.defaultDay).toString();
            if (day.length < 2) {
                day = '0' + day;
            }
            this.selectedDate = selectedData['Value'] + '-' + month + '-' + day + 'T00:00:00.000Z';
        }
        else {
            this.selectedDate = null;
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    convertDateToISO(date) {
        /** @type {?} */
        const newDate = new Date(date);
        /** @type {?} */
        const year = newDate.getFullYear();
        /** @type {?} */
        let month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        let day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    }
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    convertDateToDateUTC(dateAsString) {
        /** @type {?} */
        const newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.translateSubscription.unsubscribe();
    }
}
FormDatepickerInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-datepicker-input',
                template: "<div class=\"dp-container form-datepicker-input\" [class.disable-selection]=\"isReadOnly\"\r\n     [class.hidden-btn]=\"defaultSettings.hideCalendarButton\">\r\n  <p-calendar class=\"calendar-fit\" *ngIf=\"!defaultSettings.showYear\" [(ngModel)]=\"calendarDateValue\"\r\n              [dateFormat]=\"defaultSettings.dateFormat\" [timeOnly]=\"defaultSettings.showTimeOnly\"\r\n              [disabled]=\"isDisabled\" [showTime]=\"defaultSettings.showTime\"\r\n              [showIcon]=\"!defaultSettings.hideCalendarButton\" [showButtonBar]=\"defaultSettings.showButtonBar\"\r\n              [minDate]=\"minDate\" [maxDate]=\"maxDate\" [locale]=\"locale\" [view]=\"view\" [selectionMode]=\"_selectionMode\"\r\n              [style]=\"{'width':'100%', 'height': '30px', 'top': '-2px'}\" monthNavigator=\"true\" yearNavigator=\"true\"\r\n              yearRange=\"1930:2030\" placeholder=\"{{placeholder|translate}}\" appendTo=\"body\"></p-calendar>\r\n  <!--\r\n  <div [hidden]=\"hideCalendarButton\" class=\"dp-btn-container\">\r\n    <button class=\"btn btn-success dp-btn\" type=\"button\" (click)=\"dpStart.show(); $event.stopPropagation();\">\r\n      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n    </button>\r\n  </div> -->\r\n</div>\r\n\r\n<div *ngIf=\"formGroup && defaultSettings.showYear\" [formGroup]=\"formGroup\" class=\"form-datepicker-input\"\r\n     [class.disable-selection]=\"isReadOnly\">\r\n  <layout-static-selector formControlName=\"Year\" [dataSource]=\"yearDataSource\" [valueField]=\"'Value'\"\r\n                          [labelField]=\"'Label'\" [isDisabled]=\"isDisabled\" [placeholder]=\"placeholder\"\r\n                          (changed)=\"onYearValueChanged($event)\"></layout-static-selector>\r\n</div>",
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormDatepickerInputComponent),
                        multi: true
                    }
                ],
                styles: [".dp-btn-container,.dp-container{display:flex}.dp-input{height:26px!important;border-radius:3px 0 0 3px}.dp-btn{border-radius:0 3px 3px 0;border:none;height:26px}.bs-timepicker-field{height:30px!important;width:50px!important}:host /deep/ .ui-calendar .ui-inputtext{border:1px solid #ced4da;height:30px!important;font-size:inherit;width:calc(100% - 33px);border-right:1px solid #ced4da;padding-left:10px}:host /deep/ .ui-calendar .ui-calendar-button{height:30px;top:2px;background-color:#3594e8;border-color:#3594e8}:host /deep/ .ui-calendar .ui-inputtext:disabled{background-color:#ebebe4}:host /deep/ .ui-calendar .ui-state-disabled,:host /deep/ .ui-calendar .ui-widget:disabled{opacity:1}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext{width:calc(100%)}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext:hover{border-right:1px solid #000!important}::ng-deep .ui-datepicker table td{padding:.2em!important;font-size:12px}::ng-deep .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}::ng-deep .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}.calendar-fit{width:calc(100%)}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-calendar .ui-inputtext{background-color:#e9ecef}"]
            }] }
];
/** @nocollapse */
FormDatepickerInputComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
FormDatepickerInputComponent.propDecorators = {
    settings: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    dateChanges: [{ type: Output }],
    placeholder: [{ type: Input }],
    selectionMode: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormDatepickerInputComponent.prototype._calendarDateValue;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.selectedDateString;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.usedMometToDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.settings;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.dateChanges;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.placeholder;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.selectionMode;
    /** @type {?} */
    FormDatepickerInputComponent.prototype._selectionMode;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.defaultSettings;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.view;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.yearDataSource;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.formGroup;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.minDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.maxDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.locale;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.localeTr;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.localEn;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.translateSubscription;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.onChange;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1kYXRlcGlja2VyLWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtZGF0ZXBpY2tlci9mb3JtLWRhdGVwaWNrZXItaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsVUFBVSxFQUFpQixZQUFZLEVBQUUsTUFBTSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hJLE9BQU8sRUFBRSxpQkFBaUIsRUFBd0IsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFhLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR3ZELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNuRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQWE1RSxNQUFNLE9BQU8sNEJBQTRCOzs7OztJQStCdkMsWUFBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQXpCL0QsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNsQixnQkFBVyxHQUF1QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBSS9ELG1CQUFjLEdBQXNCLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztRQWdCN0QsYUFBUSxHQUFRLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMxQixjQUFTLEdBQVEsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBSXpCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ25CLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLElBQUksRUFBRSxJQUFJO1NBQ1gsQ0FBQyxDQUFDO1FBRUgsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDYixjQUFjLEVBQUUsQ0FBQztZQUNqQixRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7WUFDeEYsYUFBYSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDO1lBQ2hFLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQztZQUN2RCxVQUFVLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQztZQUN0SSxlQUFlLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNyRyxLQUFLLEVBQUUsT0FBTztZQUNkLEtBQUssRUFBRSxPQUFPO1NBQ2YsQ0FBQztRQUNGLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ2QsY0FBYyxFQUFFLENBQUM7WUFDakIsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDO1lBQ3JGLGFBQWEsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNoRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7WUFDdkQsVUFBVSxFQUFFLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUM7WUFDM0gsZUFBZSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDckcsS0FBSyxFQUFFLE9BQU87WUFDZCxLQUFLLEVBQUUsU0FBUztTQUNqQixDQUFDO1FBQ0YsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM1QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBVSxFQUFFLEVBQUU7WUFDaEYsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUNsRTtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDL0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxPQUFPLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDckMsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUV6RSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDdEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQzthQUNuRDtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdkMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFO2dCQUM5QixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7YUFDNUM7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO2dCQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDeEM7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFO2dCQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQzthQUNyQjtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDakU7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFO2dCQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2pFO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBYTtRQUN0QixJQUFJLEtBQUssRUFBRTtZQUVULElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTs7O3NCQUVuSSxhQUFhLEdBQWEsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBQ2hELGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxlQUFlLENBQUM7Z0JBQ25DLEtBQUssR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssS0FBSyxFQUFFO2dCQUNyQyw0RkFBNEY7Z0JBQzVGLHNEQUFzRDtnQkFDdEQsZ0NBQWdDO2dCQUNoQyxrRUFBa0U7Z0JBQ2xFLHNGQUFzRjtnQkFDdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNqRyw2Q0FBNkM7b0JBQzdDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ2pELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO2lCQUM3QjtxQkFBTTtvQkFDTCxpREFBaUQ7b0JBQ2pELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVEO2dCQUVELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO29CQUN0QixJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7aUJBQ25DLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2FBQ2pDO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDL0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUNoQztJQUNILENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ2QsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxJQUFJLFlBQVksQ0FBQyxLQUF3QjtRQUN2QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLHdEQUF3RDtRQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7O0lBSUQsSUFBSSxpQkFBaUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxJQUFJLGlCQUFpQixDQUFDLEtBQVc7UUFDL0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssaUJBQWlCLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs7a0JBQ3hFLGFBQWEsR0FBRyxtQkFBQSxtQkFBQSxJQUFJLENBQUMsa0JBQWtCLEVBQVcsRUFBUzs7a0JBQzNELG1CQUFtQixHQUFhLEVBQUU7WUFDeEMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQVUsRUFBRSxFQUFFO2dCQUNuQyxJQUFJLElBQUksRUFBRTtvQkFDUixtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3ZEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQW1CLENBQUM7YUFDekM7U0FDRjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3ZELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2FBQzFCO2lCQUFNLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUNsQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUNqQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3BFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDbEU7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtvQkFDckMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNwRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ2pFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDdkU7Z0JBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUU7b0JBQzdFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3ZDOztvQkFFRyxvQkFBb0IsR0FBVyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dCQUNqRixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFO29CQUNuQyxvQkFBb0IsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7aUJBQ3RGO2dCQUVELElBQUksb0JBQW9CLEtBQUssSUFBSSxDQUFDLGtCQUFrQixFQUFFO29CQUNwRCxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFvQixDQUFDO2lCQUMxQzthQUNGO1NBQ0Y7SUFFSCxDQUFDOzs7O0lBRUQsc0JBQXNCOztjQUNkLFVBQVUsR0FBZSxFQUFFO1FBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdELFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsS0FBSyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ25CLEtBQUssRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFO2FBQ3BCLENBQUMsQ0FBQztTQUNKO1FBQ0QsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxZQUFpQjtRQUNsQyxJQUFJLFlBQVksRUFBRTs7Z0JBQ1osS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFO1lBQzlELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUssR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDO2FBQ3JCOztnQkFFRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUN0RCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQixHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQzthQUNqQjtZQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQztTQUN4RjthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDMUI7SUFFSCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLElBQVU7O2NBQ25CLE9BQU8sR0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7O2NBQ2xDLElBQUksR0FBRyxPQUFPLENBQUMsV0FBVyxFQUFFOztZQUMxQixLQUFLLEdBQUcsRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQzs7WUFDdkMsR0FBRyxHQUFHLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFO1FBRTlCLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEIsS0FBSyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7U0FDckI7UUFDRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO1NBQ2pCO1FBQ0QsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLGdCQUFnQixDQUFDO0lBQ3pELENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsWUFBb0I7O2NBQ2pDLE9BQU8sR0FBUyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDNUMsT0FBTyxJQUFJLElBQUksQ0FDYixPQUFPLENBQUMsY0FBYyxFQUFFLEVBQ3hCLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFDckIsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUNwQixPQUFPLENBQUMsV0FBVyxFQUFFLEVBQ3JCLE9BQU8sQ0FBQyxhQUFhLEVBQUUsRUFDdkIsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDM0MsQ0FBQzs7O1lBblNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsOEJBQThCO2dCQUN4QyxndURBQXFEO2dCQUVyRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQzt3QkFDM0QsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQUM7O2FBQ0w7Ozs7WUFqQm1CLFdBQVc7WUFDdEIsZ0JBQWdCOzs7dUJBcUJ0QixLQUFLO3lCQUNMLEtBQUs7eUJBQ0wsS0FBSzswQkFDTCxNQUFNOzBCQUNOLEtBQUs7NEJBQ0wsS0FBSzs7OztJQVJOLDBEQUF5Qjs7SUFDekIsMERBQXNDOzs7OztJQUN0Qyx1REFBaUM7O0lBQ2pDLGdEQUFpQzs7SUFDakMsa0RBQThCOztJQUM5QixrREFBNEI7O0lBQzVCLG1EQUErRDs7SUFDL0QsbURBQThCOztJQUM5QixxREFBMkM7O0lBRTNDLHNEQUE2RDs7SUFFN0QsdURBQThCOztJQUM5Qiw0Q0FBYTs7SUFFYixzREFBMkI7O0lBQzNCLGlEQUFxQjs7SUFFckIsK0NBQWM7O0lBQ2QsK0NBQWM7O0lBRWQsOENBQVk7O0lBQ1osZ0RBQWM7O0lBQ2QsK0NBQWE7O0lBQ2IsNkRBQW9DOztJQUVwQyxnREFBMEI7O0lBQzFCLGlEQUEyQjs7Ozs7SUFHZiwwQ0FBdUI7Ozs7O0lBQUUsaURBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uQ2hhbmdlcywgSW5wdXQsIGZvcndhcmRSZWYsIFNpbXBsZUNoYW5nZXMsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IERhdGVTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uL21vZGVscy9kYXRlLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0ZVNlbGVjdGlvbk1vZGUgfSBmcm9tICcuLi8uLi8uLi9lbnVtcy9kYXRlLXNlbGVjdGlvbi1tb2RlLmVudW0nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtZm9ybS1kYXRlcGlja2VyLWlucHV0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1kYXRlcGlja2VyLWlucHV0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mb3JtLWRhdGVwaWNrZXItaW5wdXQuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1EYXRlcGlja2VySW5wdXRDb21wb25lbnQpLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1EYXRlcGlja2VySW5wdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uRGVzdHJveSwgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG4gIF9jYWxlbmRhckRhdGVWYWx1ZTogRGF0ZTtcclxuICBzZWxlY3RlZERhdGVTdHJpbmc6IHN0cmluZyB8IHN0cmluZ1tdO1xyXG4gIHByaXZhdGUgdXNlZE1vbWV0VG9EYXRlOiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIHNldHRpbmdzPzogRGF0ZVNldHRpbmdzO1xyXG4gIEBJbnB1dCgpIGlzRGlzYWJsZWQ/OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBAT3V0cHV0KCkgZGF0ZUNoYW5nZXM/OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBASW5wdXQoKSBwbGFjZWhvbGRlcj86IHN0cmluZztcclxuICBASW5wdXQoKSBzZWxlY3Rpb25Nb2RlPzogRGF0ZVNlbGVjdGlvbk1vZGU7XHJcblxyXG4gIF9zZWxlY3Rpb25Nb2RlOiBEYXRlU2VsZWN0aW9uTW9kZSA9IERhdGVTZWxlY3Rpb25Nb2RlLlNpbmdsZTtcclxuXHJcbiAgZGVmYXVsdFNldHRpbmdzOiBEYXRlU2V0dGluZ3M7XHJcbiAgdmlldzogc3RyaW5nO1xyXG5cclxuICB5ZWFyRGF0YVNvdXJjZTogQXJyYXk8YW55PjtcclxuICBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuXHJcbiAgbWluRGF0ZTogRGF0ZTtcclxuICBtYXhEYXRlOiBEYXRlO1xyXG5cclxuICBsb2NhbGU6IGFueTtcclxuICBsb2NhbGVUcjogYW55O1xyXG4gIGxvY2FsRW46IGFueTtcclxuICB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgb25DaGFuZ2U6IGFueSA9ICgpID0+IHsgfTtcclxuICBvblRvdWNoZWQ6IGFueSA9ICgpID0+IHsgfTtcclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy52aWV3ID0gJ2RhdGUnO1xyXG4gICAgdGhpcy55ZWFyRGF0YVNvdXJjZSA9IFtdO1xyXG4gICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MgPSBuZXcgRGF0ZVNldHRpbmdzKCk7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBZZWFyOiBudWxsXHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyB1bnRpbCB0aGUgYmV0dGVyIHNvbHV0aW9uIGZvciB0cmFuc2xhdGUuXHJcbiAgICB0aGlzLmxvY2FsRW4gPSB7XHJcbiAgICAgIGZpcnN0RGF5T2ZXZWVrOiAxLFxyXG4gICAgICBkYXlOYW1lczogWydTdW5kYXknLCAnTW9uZGF5JywgJ1R1ZXNkYXknLCAnV2VkbmVzZGF5JywgJ1RodXJzZGF5JywgJ0ZyaWRheScsICdTYXR1cmRheSddLFxyXG4gICAgICBkYXlOYW1lc1Nob3J0OiBbJ1N1bicsICdNb24nLCAnVHVlJywgJ1dlZCcsICdUaHUnLCAnRnJpJywgJ1NhdCddLFxyXG4gICAgICBkYXlOYW1lc01pbjogWydTdScsICdNbycsICdUdScsICdXZScsICdUaCcsICdGcicsICdTYSddLFxyXG4gICAgICBtb250aE5hbWVzOiBbJ0phbnVhcnknLCAnRmVicnVhcnknLCAnTWFyY2gnLCAnQXByaWwnLCAnTWF5JywgJ0p1bmUnLCAnSnVseScsICdBdWd1c3QnLCAnU2VwdGVtYmVyJywgJ09jdG9iZXInLCAnTm92ZW1iZXInLCAnRGVjZW1iZXInXSxcclxuICAgICAgbW9udGhOYW1lc1Nob3J0OiBbJ0phbicsICdGZWInLCAnTWFyJywgJ0FwcicsICdNYXknLCAnSnVuJywgJ0p1bCcsICdBdWcnLCAnU2VwJywgJ09jdCcsICdOb3YnLCAnRGVjJ10sXHJcbiAgICAgIHRvZGF5OiAnVG9kYXknLFxyXG4gICAgICBjbGVhcjogJ0NsZWFyJ1xyXG4gICAgfTtcclxuICAgIC8vIHVudGlsIHRoZSBiZXR0ZXIgc29sdXRpb24gZm9yIHRyYW5zbGF0ZS5cclxuICAgIHRoaXMubG9jYWxlVHIgPSB7XHJcbiAgICAgIGZpcnN0RGF5T2ZXZWVrOiAxLFxyXG4gICAgICBkYXlOYW1lczogWydQYXphcicsICdQYXphcnRlc2knLCAnU2FsxLEnLCAnw4dhcsWfYW1iYScsICdQZXLFn2VtYmUnLCAnQ3VtYScsICdDdW1hcnRlc2knXSxcclxuICAgICAgZGF5TmFtZXNTaG9ydDogWydQYXonLCAnUHRzJywgJ1NhbCcsICfDh2FyJywgJ1BlcicsICdDdW0nLCAnQ3RzJ10sXHJcbiAgICAgIGRheU5hbWVzTWluOiBbJ1B6JywgJ1B0JywgJ1NsJywgJ8OHcicsICdQcicsICdDbScsICdDdCddLFxyXG4gICAgICBtb250aE5hbWVzOiBbJ09jYWsnLCAnxZ51YmF0JywgJ01hcnQnLCAnTmlzYW4nLCAnTWF5xLFzJywgJ0hhemlyYW4nLCAnVGVtbXV6JywgJ0HEn3VzdG9zJywgJ0V5bMO8bCcsICdFa2ltJywgJ0thc8SxbScsICdBcmFsxLFrJ10sXHJcbiAgICAgIG1vbnRoTmFtZXNTaG9ydDogWydPY2EnLCAnxZ51YicsICdNYXInLCAnTmlzJywgJ01heScsICdIYXonLCAnVGVtJywgJ0HEn3UnLCAnRXlsJywgJ0VraScsICdLYXMnLCAnQXJhJ10sXHJcbiAgICAgIHRvZGF5OiAnQnVnw7xuJyxcclxuICAgICAgY2xlYXI6ICdUZW1pemxlJ1xyXG4gICAgfTtcclxuICAgIC8vIHVudGlsIHRoZSBiZXR0ZXIgc29sdXRpb24gZm9yIHRyYW5zbGF0ZS5cclxuICAgIHRoaXMubG9jYWxlID0gdGhpcy5sb2NhbGVUcjtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlLnN1YnNjcmliZSgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmxvY2FsZSA9IGV2ZW50LmxhbmcgPT09ICd0cicgPyB0aGlzLmxvY2FsZVRyIDogdGhpcy5sb2NhbEVuO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzLnNlbGVjdGlvbk1vZGUgJiYgdGhpcy5zZWxlY3Rpb25Nb2RlKSB7XHJcbiAgICAgIHRoaXMuX3NlbGVjdGlvbk1vZGUgPSB0aGlzLnNlbGVjdGlvbk1vZGU7XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhbmdlcy5zZXR0aW5ncyAmJiB0aGlzLnNldHRpbmdzKSB7XHJcbiAgICAgIHRoaXMuZGVmYXVsdFNldHRpbmdzID0gXy5hc3NpZ24oe30sIHRoaXMuZGVmYXVsdFNldHRpbmdzLCB0aGlzLnNldHRpbmdzKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNlbGVjdGlvbk1vZGUgJiYgIXRoaXMuc2VsZWN0aW9uTW9kZSkge1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGlvbk1vZGUgPSB0aGlzLnNldHRpbmdzLnNlbGVjdGlvbk1vZGU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNob3dZZWFyKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MuZGF0ZUZvcm1hdCA9ICd5eSc7XHJcbiAgICAgICAgdGhpcy5pbml0aWF0ZVllYXJEYXRhU291cmNlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNob3dUaW1lT25seSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdFNldHRpbmdzLmRpc2FibGVVVEMgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdFNldHRpbmdzLmhpZGVDYWxlbmRhckJ1dHRvbiA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd0J1dHRvbkJhciA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5zaG93VGltZSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdFNldHRpbmdzLmRpc2FibGVVVEMgPSB0cnVlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5zaG93TW9udGhQaWNrZXIpIHtcclxuICAgICAgICB0aGlzLnZpZXcgPSAnbW9udGgnO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5taW5EYXRlKSB7XHJcbiAgICAgICAgdGhpcy5taW5EYXRlID0gdGhpcy5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh0aGlzLnNldHRpbmdzLm1pbkRhdGUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5tYXhEYXRlKSB7XHJcbiAgICAgICAgdGhpcy5tYXhEYXRlID0gdGhpcy5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh0aGlzLnNldHRpbmdzLm1heERhdGUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIGlmICh2YWx1ZSkge1xyXG5cclxuICAgICAgaWYgKHZhbHVlLmluZGV4T2YoJ1QnKSAhPT0gLTEgJiYgIXRoaXMuZGVmYXVsdFNldHRpbmdzLmRpc2FibGVVVEMgJiYgIXRoaXMuZGVmYXVsdFNldHRpbmdzLnNob3dUaW1lT25seSAmJiAhdGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd1RpbWUpIHtcclxuICAgICAgICAvLyBjYW4gYmUgZG9uZSB2aWEgbW9tZW50XHJcbiAgICAgICAgY29uc3Qgc3BsaXR0ZWRWYWx1ZTogc3RyaW5nW10gPSB2YWx1ZS5zcGxpdCgnVCcpO1xyXG4gICAgICAgIHNwbGl0dGVkVmFsdWVbMV0gPSAnMDA6MDA6MDAuMDAwWic7XHJcbiAgICAgICAgdmFsdWUgPSBzcGxpdHRlZFZhbHVlLmpvaW4oJ1QnKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nICE9PSB2YWx1ZSkge1xyXG4gICAgICAgIC8vIGR1ZSB0byBVVEMsIHdpdGhvdXQgYmVpbmcgaW5mbHVlbmNlZCB0aW1lem9uZSBjaGFuZ2VzLCBpdCBjcmVhdGVzIGRhdGUgYWNjb3JkaW5nIHRvIHZhbHVlXHJcbiAgICAgICAgLy8gaW4gb3JkZXIgdG8gYXZvaWQgcmVmbGVjdGluZyB0aW1lem9uZSBjaGFuZ2VzIHRvIFVJXHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ3ZhbHVlICcsIHZhbHVlKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnbW9tZW50KHZhbHVlKS50b0RhdGUoKSAnLCBtb21lbnQodmFsdWUpLnRvRGF0ZSgpKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygndGhpcy5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh2YWx1ZSkgJywgdGhpcy5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh2YWx1ZSkpO1xyXG4gICAgICAgIGlmICghdGhpcy51c2VkTW9tZXRUb0RhdGUgJiYgKHRoaXMuZGVmYXVsdFNldHRpbmdzLnNob3dUaW1lT25seSB8fCB0aGlzLmRlZmF1bHRTZXR0aW5ncy5zaG93VGltZSkpIHtcclxuICAgICAgICAgIC8vIGZpcnN0IGluaXRpYXRpb24gbmVlZHMgdG8gYmUgZG9uZSB0aGlzIHdheVxyXG4gICAgICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSBtb21lbnQodmFsdWUpLnRvRGF0ZSgpO1xyXG4gICAgICAgICAgdGhpcy51c2VkTW9tZXRUb0RhdGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAvLyBhZnRlciB0aGUgaW5pdGlhdGlvbiBuZWVkcyB0byBiZSB1c2VkIHRoaXMgd2F5XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IHRoaXMuY29udmVydERhdGVUb0RhdGVVVEModmFsdWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5mb3JtR3JvdXAuc2V0VmFsdWUoe1xyXG4gICAgICAgICAgWWVhcjogbW9tZW50KHZhbHVlKS5mb3JtYXQoJ1lZWVknKVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nID0gdmFsdWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlID0gbnVsbDtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGVTdHJpbmcgPSBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICBnZXQgc2VsZWN0ZWREYXRlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nO1xyXG4gIH1cclxuXHJcbiAgc2V0IHNlbGVjdGVkRGF0ZSh2YWx1ZTogc3RyaW5nIHwgc3RyaW5nW10pIHtcclxuICAgIHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nID0gdmFsdWU7XHJcbiAgICAvLyBjb25zb2xlLmxvZygnc2VsZWN0ZWREYXRlJywgdGhpcy5zZWxlY3RlZERhdGVTdHJpbmcpO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh0aGlzLnNlbGVjdGVkRGF0ZVN0cmluZyk7XHJcbiAgICB0aGlzLmRhdGVDaGFuZ2VzLmVtaXQodGhpcy5zZWxlY3RlZERhdGVTdHJpbmcpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgZ2V0IGNhbGVuZGFyRGF0ZVZhbHVlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IGNhbGVuZGFyRGF0ZVZhbHVlKHZhbHVlOiBEYXRlKSB7XHJcbiAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IHZhbHVlO1xyXG4gICAgdGhpcy5vbkNoYW5nZURhdGUoKTtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlRGF0ZSgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLl9zZWxlY3Rpb25Nb2RlID09PSBEYXRlU2VsZWN0aW9uTW9kZS5SYW5nZSAmJiB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkge1xyXG4gICAgICBjb25zdCBkYXRlUmFuZ2VMaXN0ID0gdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgYXMgdW5rbm93biBhcyBhbnlbXTtcclxuICAgICAgY29uc3QgbmV3U2VsZWN0ZWREYXRlTGlzdDogc3RyaW5nW10gPSBbXTtcclxuICAgICAgZGF0ZVJhbmdlTGlzdC5mb3JFYWNoKChkYXRlOiBEYXRlKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGUpIHtcclxuICAgICAgICAgIG5ld1NlbGVjdGVkRGF0ZUxpc3QucHVzaCh0aGlzLmNvbnZlcnREYXRlVG9JU08oZGF0ZSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAobmV3U2VsZWN0ZWREYXRlTGlzdC5sZW5ndGggPT09IDIpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGF0ZSA9IG5ld1NlbGVjdGVkRGF0ZUxpc3Q7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkRGF0ZVN0cmluZyAmJiAhdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGF0ZSA9IG51bGw7XHJcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUpIHtcclxuICAgICAgICBpZiAodGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd1llYXIpIHtcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldE1vbnRoKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHRNb250aCk7XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZS5zZXREYXRlKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHREYXkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd1RpbWVPbmx5KSB7XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZS5zZXRNb250aCh0aGlzLmRlZmF1bHRTZXR0aW5ncy5kZWZhdWx0TW9udGgpO1xyXG4gICAgICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUuc2V0RGF0ZSh0aGlzLmRlZmF1bHRTZXR0aW5ncy5kZWZhdWx0RGF5KTtcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldEZ1bGxZZWFyKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHRZZWFyKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5kZWZhdWx0U2V0dGluZ3MuZW5hYmxlU2Vjb25kcyAmJiB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZS5zZXRTZWNvbmRzKSB7XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZS5zZXRTZWNvbmRzKDApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGNhbGVuZGFyRGF0ZVZhbHVlSVNPOiBzdHJpbmcgPSB0aGlzLmNvbnZlcnREYXRlVG9JU08odGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUpO1xyXG4gICAgICAgIGlmICh0aGlzLmRlZmF1bHRTZXR0aW5ncy5kaXNhYmxlVVRDKSB7XHJcbiAgICAgICAgICBjYWxlbmRhckRhdGVWYWx1ZUlTTyA9IG1vbWVudCh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkuZm9ybWF0KCdZWVlZLU1NLUREVEhIOm1tOnNzJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2FsZW5kYXJEYXRlVmFsdWVJU08gIT09IHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nKSB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRGF0ZSA9IGNhbGVuZGFyRGF0ZVZhbHVlSVNPO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIGluaXRpYXRlWWVhckRhdGFTb3VyY2UoKTogdm9pZCB7XHJcbiAgICBjb25zdCBkYXRhU291cmNlOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICBmb3IgKGxldCBpID0gKChuZXcgRGF0ZSgpKS5nZXRGdWxsWWVhcigpICsgMzApOyBpID4gMTkyMjsgaS0tKSB7XHJcbiAgICAgIGRhdGFTb3VyY2UucHVzaCh7XHJcbiAgICAgICAgVmFsdWU6IGkudG9TdHJpbmcoKSxcclxuICAgICAgICBMYWJlbDogaS50b1N0cmluZygpXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZGF0YVNvdXJjZS5wdXNoKHsgVmFsdWU6ICcyOTAwJywgTGFiZWw6ICcyOTAwJyB9KTtcclxuICAgIHRoaXMueWVhckRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gIH1cclxuXHJcbiAgb25ZZWFyVmFsdWVDaGFuZ2VkKHNlbGVjdGVkRGF0YTogYW55KSB7XHJcbiAgICBpZiAoc2VsZWN0ZWREYXRhKSB7XHJcbiAgICAgIGxldCBtb250aCA9ICh0aGlzLmRlZmF1bHRTZXR0aW5ncy5kZWZhdWx0TW9udGggKyAxKS50b1N0cmluZygpO1xyXG4gICAgICBpZiAobW9udGgubGVuZ3RoIDwgMikge1xyXG4gICAgICAgIG1vbnRoID0gJzAnICsgbW9udGg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCBkYXkgPSAodGhpcy5kZWZhdWx0U2V0dGluZ3MuZGVmYXVsdERheSkudG9TdHJpbmcoKTtcclxuICAgICAgaWYgKGRheS5sZW5ndGggPCAyKSB7XHJcbiAgICAgICAgZGF5ID0gJzAnICsgZGF5O1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0ZSA9IHNlbGVjdGVkRGF0YVsnVmFsdWUnXSArICctJyArIG1vbnRoICsgJy0nICsgZGF5ICsgJ1QwMDowMDowMC4wMDBaJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBjb252ZXJ0RGF0ZVRvSVNPKGRhdGU6IERhdGUpOiBzdHJpbmcge1xyXG4gICAgY29uc3QgbmV3RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKGRhdGUpLFxyXG4gICAgICB5ZWFyID0gbmV3RGF0ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgbGV0IG1vbnRoID0gJycgKyAobmV3RGF0ZS5nZXRNb250aCgpICsgMSksXHJcbiAgICAgIGRheSA9ICcnICsgbmV3RGF0ZS5nZXREYXRlKCk7XHJcblxyXG4gICAgaWYgKG1vbnRoLmxlbmd0aCA8IDIpIHtcclxuICAgICAgbW9udGggPSAnMCcgKyBtb250aDtcclxuICAgIH1cclxuICAgIGlmIChkYXkubGVuZ3RoIDwgMikge1xyXG4gICAgICBkYXkgPSAnMCcgKyBkYXk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gW3llYXIsIG1vbnRoLCBkYXldLmpvaW4oJy0nKSArICdUMDA6MDA6MDAuMDAwWic7XHJcbiAgfVxyXG5cclxuICBjb252ZXJ0RGF0ZVRvRGF0ZVVUQyhkYXRlQXNTdHJpbmc6IHN0cmluZyk6IERhdGUge1xyXG4gICAgY29uc3QgbmV3RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKGRhdGVBc1N0cmluZyk7XHJcbiAgICByZXR1cm4gbmV3IERhdGUoXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDRnVsbFllYXIoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENNb250aCgpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ0RhdGUoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENIb3VycygpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ01pbnV0ZXMoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENTZWNvbmRzKCkpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=