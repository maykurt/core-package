/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export class FormCurrencyInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.changed = new EventEmitter();
        this.clearable = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    }
}
FormCurrencyInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-currency-input',
                template: `
  <div style="width:100%;">
    <input currencyMask
           class="form-control form-control-sm fixed-height-input form-input"
           autocomplete="off"
           name="inputValue"
           [(ngModel)]="inputValue"
           (keydown)="onKeyDown($event)"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           placeholder="{{placeholder|translate}}"
           [class.readonly-input] = "isReadOnly"
           [class.pr-25]="clearable && _value"
           [hidden]="hidden">
     <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>

  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormCurrencyInputComponent),
                        multi: true
                    }
                ],
                styles: [".readonly-input:focus{outline:0;box-shadow:none}"]
            }] }
];
FormCurrencyInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    changed: [{ type: Output }],
    clearable: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormCurrencyInputComponent.prototype.placeholder;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.hidden;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.changed;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.clearable;
    /** @type {?} */
    FormCurrencyInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1jdXJyZW5jeS1pbnB1dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLWVsZW1lbnRzL2lucHV0LWN1cnJlbmN5L2Zvcm0tY3VycmVuY3ktaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQXdCLE1BQU0sZ0JBQWdCLENBQUM7QUErQnpFLE1BQU0sT0FBTywwQkFBMEI7SUE3QnZDO1FBK0JXLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2QsWUFBTyxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBRWpELGNBQVMsR0FBWSxLQUFLLENBQUM7SUFzQ3RDLENBQUM7Ozs7O0lBbENDLFFBQVEsQ0FBQyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCxTQUFTO0lBQ1QsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELElBQUksVUFBVSxDQUFDLEdBQUc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVU7UUFDbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7OztZQXpFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7OztHQWlCVDtnQkFFRCxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQzt3QkFDekQsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQUM7O2FBQ0w7OzswQkFHRSxLQUFLO3lCQUNMLEtBQUs7eUJBQ0wsS0FBSztxQkFDTCxLQUFLO3NCQUNMLE1BQU07d0JBRU4sS0FBSzs7OztJQU5OLGlEQUE4Qjs7SUFDOUIsZ0RBQTRCOztJQUM1QixnREFBNEI7O0lBQzVCLDRDQUF3Qjs7SUFDeEIsNkNBQTBEOztJQUUxRCwrQ0FBb0M7O0lBRXBDLDRDQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBmb3J3YXJkUmVmLCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtZm9ybS1jdXJyZW5jeS1pbnB1dCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICA8ZGl2IHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgIDxpbnB1dCBjdXJyZW5jeU1hc2tcclxuICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbCBmb3JtLWNvbnRyb2wtc20gZml4ZWQtaGVpZ2h0LWlucHV0IGZvcm0taW5wdXRcIlxyXG4gICAgICAgICAgIGF1dG9jb21wbGV0ZT1cIm9mZlwiXHJcbiAgICAgICAgICAgbmFtZT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgIFsobmdNb2RlbCldPVwiaW5wdXRWYWx1ZVwiXHJcbiAgICAgICAgICAgKGtleWRvd24pPVwib25LZXlEb3duKCRldmVudClcIlxyXG4gICAgICAgICAgIFtkaXNhYmxlZF09XCJpc0Rpc2FibGVkXCJcclxuICAgICAgICAgICBbcmVhZG9ubHldPVwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7e3BsYWNlaG9sZGVyfHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAgICBbY2xhc3MucmVhZG9ubHktaW5wdXRdID0gXCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAgICBbY2xhc3MucHItMjVdPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiXHJcbiAgICAgICAgICAgW2hpZGRlbl09XCJoaWRkZW5cIj5cclxuICAgICA8Y29yZS1pY29uIGljb249XCJ0aW1lc1wiICpuZ0lmPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiIGNsYXNzPVwic2VhcmNoY2xlYXJcIiAoY2xpY2spPVwiaW5wdXRWYWx1ZSA9IG51bGxcIj48L2NvcmUtaWNvbj5cclxuICA8L2Rpdj5cclxuXHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9mb3JtLWN1cnJlbmN5LWlucHV0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGb3JtQ3VycmVuY3lJbnB1dENvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZvcm1DdXJyZW5jeUlucHV0Q29tcG9uZW50IGltcGxlbWVudHMgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlzRGlzYWJsZWQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBpc1JlYWRPbmx5ID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaGlkZGVuID0gZmFsc2U7XHJcbiAgQE91dHB1dCgpIGNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBASW5wdXQoKSBjbGVhcmFibGU6IEJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgX3ZhbHVlOiBhbnk7XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgaW5wdXRWYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBpbnB1dFZhbHVlKHZhbCkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWw7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gICAgdGhpcy5jaGFuZ2VkLmVtaXQodGhpcy5fdmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xyXG4gIH1cclxuXHJcbiAgb25LZXlEb3duKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmlzUmVhZE9ubHkpIHtcclxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19