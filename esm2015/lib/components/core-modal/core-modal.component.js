/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { CoreModalService } from '../../services/custom/core-modal.service';
export class CoreModalComponent {
    /**
     * @param {?} coreModalService
     */
    constructor(coreModalService) {
        this.coreModalService = coreModalService;
        this.lockBackground = true;
        this.closeOnEscape = true;
        this.maximizable = false;
        this.draggable = true;
        this.closable = true;
        this.showHeader = true;
        this.width = 50;
        this.modalWidth = (window.screen.width) * this.width / 100;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.displayData) {
            if (this.displayData && this.displayData.display) {
                this._display = true;
            }
            else {
                this._display = false;
            }
        }
        if (changes.width) {
            if (this.width) {
                this.modalWidth = (window.screen.width) * this.width / 100;
            }
            else {
                this.modalWidth = null;
            }
        }
    }
    /**
     * @return {?}
     */
    openModal() {
        this.displayData = {
            display: true
        };
        this._display = true;
    }
    /**
     * @return {?}
     */
    closeModal() {
        this.displayData = {
            display: false
        };
        this._display = false;
    }
    /**
     * @return {?}
     */
    onDialogShow() {
        this.coreModalService.setModalDisplayStatus(true);
    }
    /**
     * @return {?}
     */
    onDialogHide() {
        this.coreModalService.setModalDisplayStatus(false);
    }
}
CoreModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-modal',
                template: "<p-dialog appendTo=\"body\" focusOnShow=\"false\" [(visible)]=\"_display\" [(modal)]=\"lockBackground\"\r\n  [(maximizable)]=\"maximizable\" [(draggable)]=\"draggable\" [(closable)]=\"closable\" [(showHeader)]=\"showHeader\"\r\n  [(closeOnEscape)]=\"closeOnEscape\" [autoZIndex]=\"false\" [blockScroll]=\"true\" [baseZIndex]=\"1001\" [width]=\"modalWidth\"\r\n  (onShow)=\"onDialogShow()\" (onHide)=\"onDialogHide()\">\r\n  <p-header *ngIf=\"modalTitle\">\r\n    {{modalTitle | translate}}\r\n  </p-header>\r\n  <div>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</p-dialog>",
                styles: [".active-item:hover{z-index:2;color:#fff;background-color:#007bff;border-color:#007bff;cursor:pointer}::ng-deep .ui-dialog .ui-dialog-titlebar{background:#2d5f8b!important;color:#fff!important;font-weight:700!important;font-size:18px!important;padding:10px 15px!important;cursor:pointer!important;border:none!important}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:not(:hover){color:#fff!important;margin-top:2px}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:hover{margin-top:2px}::ng-deep .ui-dialog .ui-dialog-content{padding:30px!important}::ng-deep .ui-dialog{z-index:1001}::ng-deep .ui-widget-overlay{z-index:1000}:host /deep/ .ng-dropdown-panel.ng-select-bottom{position:fixed!important}"]
            }] }
];
/** @nocollapse */
CoreModalComponent.ctorParameters = () => [
    { type: CoreModalService }
];
CoreModalComponent.propDecorators = {
    modalTitle: [{ type: Input }],
    lockBackground: [{ type: Input }],
    closeOnEscape: [{ type: Input }],
    maximizable: [{ type: Input }],
    draggable: [{ type: Input }],
    closable: [{ type: Input }],
    showHeader: [{ type: Input }],
    displayData: [{ type: Input }],
    width: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CoreModalComponent.prototype.modalTitle;
    /** @type {?} */
    CoreModalComponent.prototype.lockBackground;
    /** @type {?} */
    CoreModalComponent.prototype.closeOnEscape;
    /** @type {?} */
    CoreModalComponent.prototype.maximizable;
    /** @type {?} */
    CoreModalComponent.prototype.draggable;
    /** @type {?} */
    CoreModalComponent.prototype.closable;
    /** @type {?} */
    CoreModalComponent.prototype.showHeader;
    /** @type {?} */
    CoreModalComponent.prototype.displayData;
    /** @type {?} */
    CoreModalComponent.prototype.width;
    /** @type {?} */
    CoreModalComponent.prototype.modalWidth;
    /** @type {?} */
    CoreModalComponent.prototype._display;
    /**
     * @type {?}
     * @private
     */
    CoreModalComponent.prototype.coreModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1tb2RhbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLW1vZGFsL2NvcmUtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQU81RSxNQUFNLE9BQU8sa0JBQWtCOzs7O0lBZTdCLFlBQW9CLGdCQUFrQztRQUFsQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBWjdDLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBRWxCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFFYixlQUFVLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO0lBSzdELENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLE9BQU87UUFDakIsSUFBSSxPQUFPLENBQUMsV0FBVyxFQUFFO1lBQ3ZCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDdEI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDdkI7U0FDRjtRQUVELElBQUksT0FBTyxDQUFDLEtBQUssRUFBRTtZQUNqQixJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7YUFDNUQ7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDeEI7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN2QixDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7OztJQUdELFlBQVk7UUFDVixJQUFJLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7O1lBL0RGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsNmtCQUEwQzs7YUFFM0M7Ozs7WUFOUSxnQkFBZ0I7Ozt5QkFTdEIsS0FBSzs2QkFDTCxLQUFLOzRCQUNMLEtBQUs7MEJBQ0wsS0FBSzt3QkFDTCxLQUFLO3VCQUNMLEtBQUs7eUJBQ0wsS0FBSzswQkFDTCxLQUFLO29CQUNMLEtBQUs7Ozs7SUFSTix3Q0FBNkI7O0lBQzdCLDRDQUErQjs7SUFDL0IsMkNBQThCOztJQUM5Qix5Q0FBNkI7O0lBQzdCLHVDQUEwQjs7SUFDMUIsc0NBQXlCOztJQUN6Qix3Q0FBMkI7O0lBQzNCLHlDQUEyQjs7SUFDM0IsbUNBQW9COztJQUVwQix3Q0FBNkQ7O0lBQzdELHNDQUF5Qjs7Ozs7SUFFYiw4Q0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29yZU1vZGFsU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2N1c3RvbS9jb3JlLW1vZGFsLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdjb3JlLW1vZGFsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29yZS1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29yZS1tb2RhbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG5cclxuICBASW5wdXQoKSBtb2RhbFRpdGxlPzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGxvY2tCYWNrZ3JvdW5kID0gdHJ1ZTtcclxuICBASW5wdXQoKSBjbG9zZU9uRXNjYXBlID0gdHJ1ZTtcclxuICBASW5wdXQoKSBtYXhpbWl6YWJsZSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGRyYWdnYWJsZSA9IHRydWU7XHJcbiAgQElucHV0KCkgY2xvc2FibGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHNob3dIZWFkZXIgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGRpc3BsYXlEYXRhPzogYW55O1xyXG4gIEBJbnB1dCgpIHdpZHRoID0gNTA7XHJcblxyXG4gIHB1YmxpYyBtb2RhbFdpZHRoID0gKHdpbmRvdy5zY3JlZW4ud2lkdGgpICogdGhpcy53aWR0aCAvIDEwMDtcclxuICBwdWJsaWMgX2Rpc3BsYXk6IGJvb2xlYW47XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29yZU1vZGFsU2VydmljZTogQ29yZU1vZGFsU2VydmljZSkge1xyXG5cclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmIChjaGFuZ2VzLmRpc3BsYXlEYXRhKSB7XHJcbiAgICAgIGlmICh0aGlzLmRpc3BsYXlEYXRhICYmIHRoaXMuZGlzcGxheURhdGEuZGlzcGxheSkge1xyXG4gICAgICAgIHRoaXMuX2Rpc3BsYXkgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX2Rpc3BsYXkgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjaGFuZ2VzLndpZHRoKSB7XHJcbiAgICAgIGlmICh0aGlzLndpZHRoKSB7XHJcbiAgICAgICAgdGhpcy5tb2RhbFdpZHRoID0gKHdpbmRvdy5zY3JlZW4ud2lkdGgpICogdGhpcy53aWR0aCAvIDEwMDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm1vZGFsV2lkdGggPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvcGVuTW9kYWwoKSB7XHJcbiAgICB0aGlzLmRpc3BsYXlEYXRhID0ge1xyXG4gICAgICBkaXNwbGF5OiB0cnVlXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fZGlzcGxheSA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBjbG9zZU1vZGFsKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHtcclxuICAgICAgZGlzcGxheTogZmFsc2VcclxuICAgIH07XHJcbiAgICB0aGlzLl9kaXNwbGF5ID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuXHJcbiAgb25EaWFsb2dTaG93KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jb3JlTW9kYWxTZXJ2aWNlLnNldE1vZGFsRGlzcGxheVN0YXR1cyh0cnVlKTtcclxuICB9XHJcblxyXG4gIG9uRGlhbG9nSGlkZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuY29yZU1vZGFsU2VydmljZS5zZXRNb2RhbERpc3BsYXlTdGF0dXMoZmFsc2UpO1xyXG4gIH1cclxufVxyXG4iXX0=