/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
export class TabsComponent {
    /**
     * @param {?} router
     * @param {?} activatedRoute
     */
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.tabChange = new EventEmitter();
        this.tabs = [];
        // this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
        //   .subscribe(event => {
        //     // set breadcrumbs
        //     const foundData = this.tabs.find(x => x.link === this.router.url);
        //     if (foundData) {
        //       this.onTabClick(foundData);
        //     }
        //   });
        this.routingSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
            /** @type {?} */
            const foundData = this.tabs.find(x => x.link === this.router.url);
            if (foundData) {
                this.onTabClick(foundData);
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.activeTab && !changes.activeTab.isFirstChange()) {
            this.foundData = this.tabs.find(x => x.title === this.activeTab);
            this.tabChange.emit(this.foundData);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    getClassActive(tab) {
        // return this.activeTab === tab.title && !tab.link;
        // (bu alan hep false dönüyo)
        return this.activeTab === tab.title && ((tab.link !== undefined && tab.link !== null) || !tab.link);
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    onTabClick(tab) {
        this.activeTab = tab.title;
        this.tabChange.emit(tab);
    }
    /**
     * @param {?} title
     * @param {?} link
     * @return {?}
     */
    addTabs(title, link) {
        this.tabs.push({ title, link });
        if (this.router && this.router.url && link === this.router.url) {
            this.activeTab = title;
            // this.tabChange.emit({ title, link });
        }
        // if (title === this.activeTab) {
        //   this.tabChange.emit(this.activeTab);
        //   if (link) {
        //     this.router.navigate([link]);
        //   }
        // }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-tabs',
                template: "<nav class=\"navbar navbar-expand-sm navbar-dark justify-content-md-center mb-5\">\r\n  <ul class=\"navbar-nav\">\r\n    <li class=\"nav-item\" *ngFor=\"let tab of tabs\" (click)=\"onTabClick(tab)\" [class.active]=\"getClassActive(tab)\">\r\n      <a *ngIf=\"tab.link\" class=\"nav-link\" [routerLinkActive]=\"['router-link-active']\"\r\n        [routerLink]=\"[tab.link]\">{{tab.title|translate}}</a>\r\n      <a *ngIf=\"!tab.link\" class=\"nav-link\">{{tab.title|translate}}</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n\r\n<ng-content></ng-content>",
                styles: [".navbar{margin:15px 0!important;padding:0}.navbar-nav{display:flex;width:100%;margin-bottom:0;padding-left:0;padding-right:0;margin-left:0;margin-right:0;box-shadow:0 5px 20px rgba(0,0,0,.05)}.nav-item{font-weight:600;flex:1;text-align:center;padding:0;border:1px solid #e3f1f8;border-left-width:0}.nav-item:first-child{border-left-width:1px}.nav-item a.nav-link{background:#fff;color:#788d96;line-height:24px;border-radius:0;transition:.2s ease-in-out;cursor:pointer;height:100%;display:flex;align-items:center;justify-content:center}.nav-item a.nav-link.router-link-active,.nav-item.active a{color:#175169;border-bottom:2px solid #409fdd}.nav-item a.nav-link:hover{background-color:#e3f1f8;color:#1a2b49}"]
            }] }
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
TabsComponent.propDecorators = {
    activeTab: [{ type: Input }],
    tabChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TabsComponent.prototype.activeTab;
    /** @type {?} */
    TabsComponent.prototype.tabChange;
    /** @type {?} */
    TabsComponent.prototype.tabs;
    /** @type {?} */
    TabsComponent.prototype.foundData;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.routingSubscription;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.activatedRoute;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YWJzL3RhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQWEsWUFBWSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQUM1RyxPQUFPLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUd4RSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFPeEMsTUFBTSxPQUFPLGFBQWE7Ozs7O0lBUXhCLFlBQW9CLE1BQWMsRUFBVSxjQUE4QjtRQUF0RCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTmhFLGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBT3ZDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBRWYsZ0dBQWdHO1FBQ2hHLDBCQUEwQjtRQUMxQix5QkFBeUI7UUFDekIseUVBQXlFO1FBQ3pFLHVCQUF1QjtRQUN2QixvQ0FBb0M7UUFDcEMsUUFBUTtRQUNSLFFBQVE7UUFFUixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNoRCxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxDQUFDLENBQ2hELENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTs7a0JBQ1QsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNqRSxJQUFJLFNBQVMsRUFBRTtnQkFDYixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzVCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUNELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxTQUFTLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQzNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDckM7SUFDSCxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxHQUFRO1FBQ3JCLG9EQUFvRDtRQUNwRCw2QkFBNkI7UUFDN0IsT0FBTyxJQUFJLENBQUMsU0FBUyxLQUFLLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEcsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsR0FBUTtRQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7O0lBRUQsT0FBTyxDQUFDLEtBQWEsRUFBRSxJQUFZO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFFaEMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRTtZQUM5RCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2Qix3Q0FBd0M7U0FDekM7UUFHRCxrQ0FBa0M7UUFDbEMseUNBQXlDO1FBQ3pDLGdCQUFnQjtRQUNoQixvQ0FBb0M7UUFDcEMsTUFBTTtRQUNOLElBQUk7SUFDTixDQUFDOzs7O0lBR0QsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN4QztJQUNILENBQUM7OztZQTFFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLCtpQkFBb0M7O2FBRXJDOzs7O1lBVFEsTUFBTTtZQUFFLGNBQWM7Ozt3QkFXNUIsS0FBSzt3QkFDTCxNQUFNOzs7O0lBRFAsa0NBQTJCOztJQUMzQixrQ0FBeUM7O0lBQ3pDLDZCQUFZOztJQUNaLGtDQUFlOzs7OztJQUVmLDRDQUEwQzs7Ozs7SUFFOUIsK0JBQXNCOzs7OztJQUFFLHVDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgT25DaGFuZ2VzLCBFdmVudEVtaXR0ZXIsIFNpbXBsZUNoYW5nZXMsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBOYXZpZ2F0aW9uRW5kIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2NvcmUtdGFicycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3RhYnMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWyd0YWJzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uRGVzdHJveSB7XHJcbiAgQElucHV0KCkgYWN0aXZlVGFiOiBzdHJpbmc7XHJcbiAgQE91dHB1dCgpIHRhYkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICB0YWJzOiBhbnlbXTtcclxuICBmb3VuZERhdGE6IGFueTtcclxuXHJcbiAgcHJpdmF0ZSByb3V0aW5nU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgICB0aGlzLnRhYnMgPSBbXTtcclxuXHJcbiAgICAvLyB0aGlzLnJvdXRpbmdTdWJzY3JpcHRpb24gPSB0aGlzLnJvdXRlci5ldmVudHMuZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZClcclxuICAgIC8vICAgLnN1YnNjcmliZShldmVudCA9PiB7XHJcbiAgICAvLyAgICAgLy8gc2V0IGJyZWFkY3J1bWJzXHJcbiAgICAvLyAgICAgY29uc3QgZm91bmREYXRhID0gdGhpcy50YWJzLmZpbmQoeCA9PiB4LmxpbmsgPT09IHRoaXMucm91dGVyLnVybCk7XHJcbiAgICAvLyAgICAgaWYgKGZvdW5kRGF0YSkge1xyXG4gICAgLy8gICAgICAgdGhpcy5vblRhYkNsaWNrKGZvdW5kRGF0YSk7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICB9KTtcclxuXHJcbiAgICB0aGlzLnJvdXRpbmdTdWJzY3JpcHRpb24gPSB0aGlzLnJvdXRlci5ldmVudHMucGlwZShcclxuICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZClcclxuICAgICkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgY29uc3QgZm91bmREYXRhID0gdGhpcy50YWJzLmZpbmQoeCA9PiB4LmxpbmsgPT09IHRoaXMucm91dGVyLnVybCk7XHJcbiAgICAgIGlmIChmb3VuZERhdGEpIHtcclxuICAgICAgICB0aGlzLm9uVGFiQ2xpY2soZm91bmREYXRhKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzLmFjdGl2ZVRhYiAmJiAhY2hhbmdlcy5hY3RpdmVUYWIuaXNGaXJzdENoYW5nZSgpKSB7XHJcbiAgICAgIHRoaXMuZm91bmREYXRhID0gdGhpcy50YWJzLmZpbmQoeCA9PiB4LnRpdGxlID09PSB0aGlzLmFjdGl2ZVRhYik7XHJcbiAgICAgIHRoaXMudGFiQ2hhbmdlLmVtaXQodGhpcy5mb3VuZERhdGEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0Q2xhc3NBY3RpdmUodGFiOiBhbnkpOiBib29sZWFuIHtcclxuICAgIC8vIHJldHVybiB0aGlzLmFjdGl2ZVRhYiA9PT0gdGFiLnRpdGxlICYmICF0YWIubGluaztcclxuICAgIC8vIChidSBhbGFuIGhlcCBmYWxzZSBkw7Zuw7x5bylcclxuICAgIHJldHVybiB0aGlzLmFjdGl2ZVRhYiA9PT0gdGFiLnRpdGxlICYmICgodGFiLmxpbmsgIT09IHVuZGVmaW5lZCAmJiB0YWIubGluayAhPT0gbnVsbCkgfHwgIXRhYi5saW5rKTtcclxuICB9XHJcblxyXG4gIG9uVGFiQ2xpY2sodGFiOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuYWN0aXZlVGFiID0gdGFiLnRpdGxlO1xyXG4gICAgdGhpcy50YWJDaGFuZ2UuZW1pdCh0YWIpO1xyXG4gIH1cclxuXHJcbiAgYWRkVGFicyh0aXRsZTogc3RyaW5nLCBsaW5rOiBzdHJpbmcpIHtcclxuICAgIHRoaXMudGFicy5wdXNoKHsgdGl0bGUsIGxpbmsgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucm91dGVyICYmIHRoaXMucm91dGVyLnVybCAmJiBsaW5rID09PSB0aGlzLnJvdXRlci51cmwpIHtcclxuICAgICAgdGhpcy5hY3RpdmVUYWIgPSB0aXRsZTtcclxuICAgICAgLy8gdGhpcy50YWJDaGFuZ2UuZW1pdCh7IHRpdGxlLCBsaW5rIH0pO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvLyBpZiAodGl0bGUgPT09IHRoaXMuYWN0aXZlVGFiKSB7XHJcbiAgICAvLyAgIHRoaXMudGFiQ2hhbmdlLmVtaXQodGhpcy5hY3RpdmVUYWIpO1xyXG4gICAgLy8gICBpZiAobGluaykge1xyXG4gICAgLy8gICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtsaW5rXSk7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH1cclxuICB9XHJcblxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnJvdXRpbmdTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5yb3V0aW5nU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==