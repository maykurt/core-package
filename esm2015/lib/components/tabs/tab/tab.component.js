/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TabsComponent } from '../tabs.component';
export class TabComponent {
    /**
     * @param {?} parent
     * @param {?} router
     * @param {?} _ngZone
     */
    constructor(parent, router, _ngZone) {
        this.parent = parent;
        this.router = router;
        this._ngZone = _ngZone;
        this.title = '';
        // this.step = 0;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.parent.addTabs(this.title, this.link);
        this.isCurrent = this.title === this.parent.activeTab;
        if (this.isCurrent && this.link) {
            this.router.navigate([this.link]);
        }
        this.tabChangeSubcriotion = this.parent.tabChange
            .subscribe((tab) => {
            this.isCurrent = this.title === tab.title;
            if (this.isCurrent && this.link) {
                this._ngZone.run(() => this.router.navigate([this.link]));
            }
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.tabChangeSubcriotion) {
            this.tabChangeSubcriotion.unsubscribe();
        }
    }
}
TabComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-tab',
                host: {
                    '[style.display]': 'isCurrent ? "flex" : "none"',
                },
                template: `
    <ng-content></ng-content>
  `
            }] }
];
/** @nocollapse */
TabComponent.ctorParameters = () => [
    { type: TabsComponent },
    { type: Router },
    { type: NgZone }
];
TabComponent.propDecorators = {
    title: [{ type: Input }],
    link: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    TabComponent.prototype.title;
    /** @type {?} */
    TabComponent.prototype.link;
    /** @type {?} */
    TabComponent.prototype.isCurrent;
    /** @type {?} */
    TabComponent.prototype.tabChangeSubcriotion;
    /** @type {?} */
    TabComponent.prototype.parent;
    /**
     * @type {?}
     * @private
     */
    TabComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    TabComponent.prototype._ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3RhYnMvdGFiL3RhYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQVlsRCxNQUFNLE9BQU8sWUFBWTs7Ozs7O0lBT3ZCLFlBQW1CLE1BQXFCLEVBQVUsTUFBYyxFQUFVLE9BQWU7UUFBdEUsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBTmhGLFVBQUssR0FBVyxFQUFFLENBQUM7UUFPMUIsaUJBQWlCO0lBQ25CLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ3RELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDbkM7UUFFRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO2FBQzlDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssS0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDM0Q7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFHRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQzs7O1lBekNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsSUFBSSxFQUFFO29CQUNKLGlCQUFpQixFQUFFLDZCQUE2QjtpQkFDakQ7Z0JBQ0QsUUFBUSxFQUFFOztHQUVUO2FBQ0Y7Ozs7WUFYUSxhQUFhO1lBRGIsTUFBTTtZQUQrQixNQUFNOzs7b0JBZWpELEtBQUs7bUJBQ0wsS0FBSzs7OztJQUROLDZCQUE0Qjs7SUFDNUIsNEJBQXVCOztJQUN2QixpQ0FBbUI7O0lBQ25CLDRDQUFtQzs7SUFHdkIsOEJBQTRCOzs7OztJQUFFLDhCQUFzQjs7Ozs7SUFBRSwrQkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFRhYnNDb21wb25lbnQgfSBmcm9tICcuLi90YWJzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdjb3JlLXRhYicsXHJcbiAgaG9zdDoge1xyXG4gICAgJ1tzdHlsZS5kaXNwbGF5XSc6ICdpc0N1cnJlbnQgPyBcImZsZXhcIiA6IFwibm9uZVwiJyxcclxuICB9LFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgYCxcclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nID0gJyc7XHJcbiAgQElucHV0KCkgbGluaz86IHN0cmluZztcclxuICBpc0N1cnJlbnQ6IGJvb2xlYW47XHJcbiAgdGFiQ2hhbmdlU3ViY3Jpb3Rpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBwYXJlbnQ6IFRhYnNDb21wb25lbnQsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgX25nWm9uZTogTmdab25lKSB7XHJcbiAgICAvLyB0aGlzLnN0ZXAgPSAwO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmVudC5hZGRUYWJzKHRoaXMudGl0bGUsIHRoaXMubGluayk7XHJcbiAgICB0aGlzLmlzQ3VycmVudCA9IHRoaXMudGl0bGUgPT09IHRoaXMucGFyZW50LmFjdGl2ZVRhYjtcclxuICAgIGlmICh0aGlzLmlzQ3VycmVudCAmJiB0aGlzLmxpbmspIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMubGlua10pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMudGFiQ2hhbmdlU3ViY3Jpb3Rpb24gPSB0aGlzLnBhcmVudC50YWJDaGFuZ2VcclxuICAgICAgLnN1YnNjcmliZSgodGFiKSA9PiB7XHJcbiAgICAgICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnRpdGxlID09PSB0YWIudGl0bGU7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDdXJyZW50ICYmIHRoaXMubGluaykge1xyXG4gICAgICAgICAgdGhpcy5fbmdab25lLnJ1bigoKSA9PiB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5saW5rXSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy50YWJDaGFuZ2VTdWJjcmlvdGlvbikge1xyXG4gICAgICB0aGlzLnRhYkNoYW5nZVN1YmNyaW90aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==