/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectorRef } from '@angular/core';
import { LoadingService } from '../../services/index';
export class LoadingComponent {
    /**
     * @param {?} loadingService
     * @param {?} changeDetector
     */
    constructor(loadingService, changeDetector) {
        this.loadingService = loadingService;
        this.changeDetector = changeDetector;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loadingService.getLoadingBarDisplayStatus()
            .subscribe((status) => {
            if (this.showLoading !== status) {
                this.showLoading = status;
                this.detectChanges();
            }
        });
    }
    /**
     * @return {?}
     */
    detectChanges() {
        setTimeout(() => {
            if (!this.changeDetector['destroyed']) {
                this.changeDetector.detectChanges();
            }
        }, 251);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.loadingService.loadingBarDisplayStatus.unsubscribe();
    }
}
LoadingComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-loading',
                template: "<div *ngIf=\"showLoading\" class=\"core-loader\">\r\n  <div id=\"loading-wrapper\">\r\n    <!-- <div id=\"loading-text\">Y\u00FCkleniyor</div> -->\r\n    <div class=\"loader\">\r\n      <div class=\"inner one\"></div>\r\n      <div class=\"inner two\"></div>\r\n      <div class=\"inner three\"></div>\r\n    </div>\r\n  </div>\r\n</div>",
                styles: ["#loading-wrapper{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;background-color:rgba(0,0,0,.05)}.core-loader{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;display:block;background-position:center center;background-repeat:no-repeat;background-color:transparent!important}.loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px;z-index:999}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
            }] }
];
/** @nocollapse */
LoadingComponent.ctorParameters = () => [
    { type: LoadingService },
    { type: ChangeDetectorRef }
];
if (false) {
    /** @type {?} */
    LoadingComponent.prototype.showLoading;
    /**
     * @type {?}
     * @private
     */
    LoadingComponent.prototype.loadingService;
    /** @type {?} */
    LoadingComponent.prototype.changeDetector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFPdEQsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7SUFHM0IsWUFDVSxjQUE4QixFQUMvQixjQUFpQztRQURoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDL0IsbUJBQWMsR0FBZCxjQUFjLENBQW1CO0lBQzFDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsRUFBRTthQUM3QyxTQUFTLENBQUMsQ0FBQyxNQUFlLEVBQUUsRUFBRTtZQUM3QixJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO2dCQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3RCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUNyQztRQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM1RCxDQUFDOzs7WUFqQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLDZWQUF1Qzs7YUFFeEM7Ozs7WUFOUSxjQUFjO1lBRGdCLGlCQUFpQjs7OztJQVN0RCx1Q0FBcUI7Ozs7O0lBR25CLDBDQUFzQzs7SUFDdEMsMENBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTG9hZGluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1sb2FkaW5nJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbG9hZGluZy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbG9hZGluZy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2FkaW5nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIHNob3dMb2FkaW5nOiBib29sZWFuO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgbG9hZGluZ1NlcnZpY2U6IExvYWRpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGNoYW5nZURldGVjdG9yOiBDaGFuZ2VEZXRlY3RvclJlZikge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmxvYWRpbmdTZXJ2aWNlLmdldExvYWRpbmdCYXJEaXNwbGF5U3RhdHVzKClcclxuICAgICAgLnN1YnNjcmliZSgoc3RhdHVzOiBib29sZWFuKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2hvd0xvYWRpbmcgIT09IHN0YXR1cykge1xyXG4gICAgICAgICAgdGhpcy5zaG93TG9hZGluZyA9IHN0YXR1cztcclxuICAgICAgICAgIHRoaXMuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBkZXRlY3RDaGFuZ2VzKCkge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIGlmICghdGhpcy5jaGFuZ2VEZXRlY3RvclsnZGVzdHJveWVkJ10pIHtcclxuICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgfVxyXG4gICAgfSwgMjUxKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5sb2FkaW5nU2VydmljZS5sb2FkaW5nQmFyRGlzcGxheVN0YXR1cy51bnN1YnNjcmliZSgpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19