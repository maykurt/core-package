/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { CoreGridComponent } from '../../core-grid/core-grid.component';
import * as _ from 'lodash';
export class ListOfValuesModalComponent {
    constructor() {
        this.getSelectedRow = new EventEmitter();
    }
    /**
     * @param {?} component
     * @return {?}
     */
    set content(component) {
        // console.error('setContent', component);
        if (component) {
            this.coreGrid = component;
            this.gridOptions = this.displayData.gridOptions;
            component.load();
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.displayData && this.displayData && this.displayData.gridOptions) {
            this.gridOptions = this.displayData.gridOptions;
        }
    }
    /**
     * @return {?}
     */
    save() {
        this.getSelectedRow.emit(this.coreGrid.getSelectedRow());
        this.displayData = { display: false };
    }
    /**
     * @return {?}
     */
    close() {
        this.displayData = { display: false };
    }
    /**
     * @param {?} selectedRows
     * @return {?}
     */
    selectedChanged(selectedRows) {
        this.getSelectedRow.emit(_.head(selectedRows));
        this.displayData = { display: false };
    }
    /**
     * @return {?}
     */
    gridReload() {
        this.coreGrid.refreshGridDataSource();
    }
}
ListOfValuesModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-list-of-values-modal',
                template: "<core-modal [displayData]=\"displayData\" [draggable]=\"false\" [showHeader]=\"false\" [closeOnEscape]=\"false\"\r\n  [closable]=\"false\" [width]=\"65\">\r\n  <layout-core-grid *ngIf=\"displayData && displayData.display\" #coreGrid [gridOptions]=\"gridOptions\"\r\n    [isInsideModal]=\"true\" (doubleClickSelectedChanged)=\"selectedChanged($event)\">\r\n  </layout-core-grid>\r\n  <div class=\"col-sm-12 prl-0 mt-10\">\r\n    <button class=\"btn btn-danger ml-10 fl-r\" type=\"button\" (click)=\"close()\">\r\n      <core-icon icon=\"times\"></core-icon> {{'CancelButton'|translate}}\r\n    </button>\r\n\r\n    <button type=\"button\" class=\"btn btn-success ml-10 fl-r\" (click)=\"save()\">\r\n      <core-icon icon=\"check\"></core-icon> {{'ChooseButton' | translate}}\r\n    </button>\r\n  </div>\r\n</core-modal>",
                styles: [".table-row-selection{cursor:pointer}.modal-pager{padding:20px 0!important;height:16px!important;display:flex;align-items:center;overflow:hidden;font-size:14px;justify-content:space-between;margin-bottom:10px}.modal-pagination{padding:0;margin-top:6px;margin-right:10px;float:left}.modal-static-layout-selector{margin-top:7px;float:left;margin-right:20px}"]
            }] }
];
/** @nocollapse */
ListOfValuesModalComponent.ctorParameters = () => [];
ListOfValuesModalComponent.propDecorators = {
    displayData: [{ type: Input }],
    getSelectedRow: [{ type: Output }],
    content: [{ type: ViewChild, args: ['coreGrid',] }]
};
if (false) {
    /** @type {?} */
    ListOfValuesModalComponent.prototype.displayData;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.getSelectedRow;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.coreGrid;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.gridOptions;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMtbW9kYWwvbGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFFNUcsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDeEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFPNUIsTUFBTSxPQUFPLDBCQUEwQjtJQWlCckM7UUFmVSxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFnQjlDLENBQUM7Ozs7O0lBZkQsSUFDSSxPQUFPLENBQUMsU0FBNEI7UUFDdEMsMENBQTBDO1FBQzFDLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztZQUNoRCxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDbEI7SUFDSCxDQUFDOzs7OztJQVNELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUMzRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDO1NBQ2pEO0lBQ0gsQ0FBQzs7OztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7UUFFekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsS0FBSztRQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsWUFBWTtRQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFFL0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxRQUFRLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7WUFqREYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSw2QkFBNkI7Z0JBQ3ZDLCt6QkFBb0Q7O2FBRXJEOzs7OzswQkFFRSxLQUFLOzZCQUNMLE1BQU07c0JBQ04sU0FBUyxTQUFDLFVBQVU7Ozs7SUFGckIsaURBQTBCOztJQUMxQixvREFBOEM7O0lBVzlDLDhDQUFtQzs7SUFFbkMsaURBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgSW5wdXQsIFZpZXdDaGlsZCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEdyaWRPcHRpb25zIH0gZnJvbSAnLi4vLi4vLi4vbW9kZWxzL2dyaWQtb3B0aW9ucy5tb2RlbCc7XHJcbmltcG9ydCB7IENvcmVHcmlkQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29yZS1ncmlkL2NvcmUtZ3JpZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1saXN0LW9mLXZhbHVlcy1tb2RhbCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2xpc3Qtb2YtdmFsdWVzLW1vZGFsLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9saXN0LW9mLXZhbHVlcy1tb2RhbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMaXN0T2ZWYWx1ZXNNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgQElucHV0KCkgZGlzcGxheURhdGE6IGFueTtcclxuICBAT3V0cHV0KCkgZ2V0U2VsZWN0ZWRSb3cgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQFZpZXdDaGlsZCgnY29yZUdyaWQnKVxyXG4gIHNldCBjb250ZW50KGNvbXBvbmVudDogQ29yZUdyaWRDb21wb25lbnQpIHtcclxuICAgIC8vIGNvbnNvbGUuZXJyb3IoJ3NldENvbnRlbnQnLCBjb21wb25lbnQpO1xyXG4gICAgaWYgKGNvbXBvbmVudCkge1xyXG4gICAgICB0aGlzLmNvcmVHcmlkID0gY29tcG9uZW50O1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zID0gdGhpcy5kaXNwbGF5RGF0YS5ncmlkT3B0aW9ucztcclxuICAgICAgY29tcG9uZW50LmxvYWQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBjb3JlR3JpZDogQ29yZUdyaWRDb21wb25lbnQ7XHJcblxyXG4gIHB1YmxpYyBncmlkT3B0aW9uczogR3JpZE9wdGlvbnM7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMuZGlzcGxheURhdGEgJiYgdGhpcy5kaXNwbGF5RGF0YSAmJiB0aGlzLmRpc3BsYXlEYXRhLmdyaWRPcHRpb25zKSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMgPSB0aGlzLmRpc3BsYXlEYXRhLmdyaWRPcHRpb25zO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2F2ZSgpIHtcclxuICAgIHRoaXMuZ2V0U2VsZWN0ZWRSb3cuZW1pdCh0aGlzLmNvcmVHcmlkLmdldFNlbGVjdGVkUm93KCkpO1xyXG5cclxuICAgIHRoaXMuZGlzcGxheURhdGEgPSB7IGRpc3BsYXk6IGZhbHNlIH07XHJcbiAgfVxyXG5cclxuICBjbG9zZSgpIHtcclxuICAgIHRoaXMuZGlzcGxheURhdGEgPSB7IGRpc3BsYXk6IGZhbHNlIH07XHJcbiAgfVxyXG5cclxuICBzZWxlY3RlZENoYW5nZWQoc2VsZWN0ZWRSb3dzKSB7XHJcbiAgICB0aGlzLmdldFNlbGVjdGVkUm93LmVtaXQoXy5oZWFkKHNlbGVjdGVkUm93cykpO1xyXG5cclxuICAgIHRoaXMuZGlzcGxheURhdGEgPSB7IGRpc3BsYXk6IGZhbHNlIH07XHJcbiAgfVxyXG5cclxuICBncmlkUmVsb2FkKCkge1xyXG4gICAgdGhpcy5jb3JlR3JpZC5yZWZyZXNoR3JpZERhdGFTb3VyY2UoKTtcclxuICB9XHJcbn1cclxuIl19