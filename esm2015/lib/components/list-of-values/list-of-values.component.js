/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Output, EventEmitter, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as _ from 'lodash';
import { TextComparison } from '../../enums/comparison-type.enum';
import { ListOfValuesService } from './list-of-values.service';
import { ListOfValuesModalComponent } from './list-of-values-modal/list-of-values-modal.component';
export class ListOfValuesComponent {
    /**
     * @param {?} listOfValuesService
     */
    constructor(listOfValuesService) {
        this.listOfValuesService = listOfValuesService;
        this.isReadOnly = false;
        // @ContentChild(FormTextInputComponent) formTextInputComponent;
        this.lovValueChanged = new EventEmitter();
        this.addToDataSource = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.listOfValuesService.lovServiceResultStateChanged
            .subscribe((result) => {
            this.initialData = result;
            this.setLovFields();
        });
    }
    /**
     * @return {?}
     */
    openModal() {
        this.displayData = {
            display: true,
            gridOptions: this.gridOptions
        };
    }
    /**
     * @param {?} selectedModel
     * @return {?}
     */
    getSelectedRow(selectedModel) {
        if (selectedModel) {
            this.KeyColumnValue = selectedModel[this.gridOptions.keyField];
            this.value = selectedModel[this.valueColumnName];
            this.label = '';
            if (this.labelColumnNames instanceof Array) {
                /** @type {?} */
                const labels = (/** @type {?} */ (this.labelColumnNames));
                labels.forEach((columnName) => {
                    this.label = this.label + ' ' + selectedModel[columnName];
                });
            }
            else if (this.labelColumnNames) {
                this.label = selectedModel[this.labelColumnNames.toString()];
            }
            // it will send the grid edit cell
            this.addToDataSource.emit({
                Id: this.listOfValuesService.endPointUrl,
                Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
            });
            this.hasSelectedItem = true;
            this.lovValueChanged.emit({ id: this.KeyColumnValue, value: this.value, label: this.label });
        }
    }
    /**
     * @return {?}
     */
    gridReload() {
        this.lovModal.gridReload();
    }
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @param {?=} dontInitData
     * @return {?}
     */
    setOptions(gridOptions, valueColumnName = 'Id', labelColumnNames = 'Definition', dontInitData) {
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
        if (!dontInitData && this.keyColumnValue) {
            this.initData();
        }
        else if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
        }
    }
    /**
     * @param {?} gridOptions
     * @param {?=} initData
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    setGridOptions(gridOptions, initData, valueColumnName, labelColumnNames) {
        this.gridOptions = gridOptions;
        if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
            if (initData) {
                this.initData();
            }
        }
        if (valueColumnName) {
            this.valueColumnName = valueColumnName;
        }
        if (labelColumnNames) {
            this.labelColumnNames = labelColumnNames;
        }
    }
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    setOptionsWithNoRequest(gridOptions, valueColumnName = 'Id', labelColumnNames = 'Definition') {
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
    }
    /**
     * @return {?}
     */
    initData() {
        if (this.gridOptions && this.keyColumnValue && this.keyColumnValue !== -1) {
            // console.log('initData - gridOptions', this.gridOptions);
            // console.log('initData - dataSource', this.dataSource);
            // console.log('initData - keyColumnValue', this.keyColumnValue);
            if (this.dataSource) {
                /** @type {?} */
                const selectedRowData = this.dataSource.find(x => x.Id === this.gridOptions.requestOptions.UrlOptions.endPointUrl
                    && x.Value.id === this.keyColumnValue);
                // console.log('initData - valueColumnName', this.valueColumnName);
                // console.log('initData - keyColumnValue', this.keyColumnValue);
                // console.log('initData - selectedRowData', selectedRowData);
                if (selectedRowData) {
                    setTimeout(() => {
                        this.label = selectedRowData.Value.label;
                        this.value = selectedRowData.Value.value;
                    }, 10);
                    return;
                }
            }
            if (this.gridOptions.requestOptions && this.gridOptions.requestOptions.UrlOptions) {
                this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                this.fieldIniData = this.gridOptions.keyField;
                if (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined) {
                    if (this.fieldIniData.indexOf('Id') > 0) {
                        this.fieldIniData = this.fieldIniData.substr(0, this.fieldIniData.indexOf('Id')) + '.' + this.fieldIniData.substr(this.fieldIniData.indexOf('Id'), this.fieldIniData.length);
                    }
                }
                this.genericExpression = {
                    FilterGroups: [
                        {
                            Filters: [
                                { PropertyName: this.fieldIniData, Value: this.keyColumnValue, Comparison: TextComparison.EqualTo }
                            ]
                        }
                    ],
                    PageSize: 1,
                    PageNumber: 1
                };
                if (this.gridOptions.requestOptions.CustomFilter) {
                    if (this.gridOptions.requestOptions.CustomFilter.FilterGroups) {
                        this.gridOptions.requestOptions.CustomFilter.FilterGroups.forEach(item => {
                            this.genericExpression.FilterGroups.push(item);
                        });
                    }
                }
                this.listOfValuesService.loadDataByFilterAndPageSetting(this.genericExpression, this.gridOptions.requestOptions.HeaderParameters);
            }
        }
    }
    /**
     * @param {?=} disableSetToKeyColumnValue
     * @return {?}
     */
    clearLov(disableSetToKeyColumnValue) {
        if (!disableSetToKeyColumnValue) {
            this.KeyColumnValue = null;
        }
        else {
            this.keyColumnValue = null;
        }
        this.value = null;
        this.label = null;
        this.hasSelectedItem = false;
        // if (this.formTextInputComponent) {
        //   this.formTextInputComponent.inputValue = null;
        // }
        if (!disableSetToKeyColumnValue) {
            this.lovValueChanged.emit(null);
        }
    }
    /**
     * @return {?}
     */
    showClearButton() {
        return !_.isNil(this.KeyColumnValue);
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get KeyColumnValue() {
        return this.keyColumnValue;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set KeyColumnValue(val) {
        this.keyColumnValue = val;
        this.setLovFields();
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        // console.log('writeValue1', value);
        // console.log('writeValue2', this.keyColumnValue);
        if (value) {
            if (value !== this.keyColumnValue) {
                this.keyColumnValue = value;
                this.initData();
            }
        }
        else {
            this.keyColumnValue = value;
            this.clearLov(true);
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @return {?}
     */
    setLovFields() {
        if (!_.isNil(this.keyColumnValue) && !_.isNil(this.gridOptions)) {
            /** @type {?} */
            const keyColumn = this.gridOptions.columns.find((item) => item.field === this.gridOptions.keyField);
            if (!_.isNil(this.initialData) && keyColumn && keyColumn.field) {
                /** @type {?} */
                const filteredObject = this.initialData.find((item) => item[keyColumn.field] === this.keyColumnValue);
                if (!_.isNil(filteredObject)) {
                    this.value = filteredObject[this.valueColumnName];
                    this.label = '';
                    this.freeText = '';
                    if (this.labelColumnNames instanceof Array) {
                        /** @type {?} */
                        const labels = (/** @type {?} */ (this.labelColumnNames));
                        labels.forEach((columnName) => {
                            this.label = this.label + ' ' + filteredObject[columnName];
                            this.freeText = this.freeText + ' ' + filteredObject[columnName];
                        });
                    }
                    else if (this.labelColumnNames) {
                        this.label = filteredObject[this.labelColumnNames.toString()];
                        this.freeText = filteredObject[this.labelColumnNames.toString()];
                    }
                    // it will send the grid edit cell
                    this.addToDataSource.emit({
                        Id: this.listOfValuesService.endPointUrl,
                        Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
                    });
                    this.hasSelectedItem = true;
                }
            }
        }
    }
    /**
     * @return {?}
     */
    getInputFiledWidth() {
        if (!this.isReadOnly) {
            if (this.showClearButton()) {
                return 'calc(60% - 76px)';
            }
            else {
                return 'calc(60% - 36px)';
            }
        }
        else {
            return 'calc(60% )';
        }
    }
}
ListOfValuesComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-list-of-values',
                template: "<div class=\"form-lov\">\r\n  <layout-form-text-input [isReadOnly]=\"true\" [(ngModel)]=\"KeyColumnValue\" [hidden]=\"true\"></layout-form-text-input>\r\n  <div class=\"row no-gutters\">\r\n    <div class=\"pr-2\" style=\"width: 40%;\">\r\n      <layout-form-text-input [class]=\"isReadOnly ? '' : 'showLikeEditable'\" [isReadOnly]=\"true\" [(ngModel)]=\"value\">\r\n      </layout-form-text-input>\r\n    </div>\r\n    <div [class.pr-2]=\"!isReadOnly\" [ngStyle]=\"{'width': getInputFiledWidth()}\">\r\n      <layout-form-text-input *ngIf=\"!inputRef.innerHTML.trim() || (inputRef.innerHTML.trim() && hasSelectedItem)\"\r\n        [isReadOnly]=\"true\" [placeholder]=\"placeholder\" [class]=\"isReadOnly ? '' : 'showLikeEditable'\"\r\n        [(ngModel)]=\"label\"></layout-form-text-input>\r\n      <div #inputRef [hidden]=\"hasSelectedItem\">\r\n        <ng-content select=\"layout-form-text-input\"></ng-content>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"!isReadOnly\" class=\"lov-btn-container\" [ngStyle]=\"{'width': showClearButton() ? '76px' : '36px'}\">\r\n      <button type=\"button\" class=\"btn btn-info\" title=\" {{'Search' | translate}}\" (click)=\"openModal()\">\r\n        <core-icon icon=\"search\"></core-icon>\r\n      </button>\r\n      <button type=\"button\" [hidden]=\"!showClearButton()\" class=\"btn btn-danger ml-1\" title=\" {{'Clear' | translate}}\"\r\n        (click)=\"clearLov()\" style=\"float:right;\">\r\n        <core-icon icon=\"times\"></core-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<layout-list-of-values-modal #lovModal [displayData]=\"displayData\" (getSelectedRow)=\"getSelectedRow($event)\">\r\n</layout-list-of-values-modal>",
                encapsulation: ViewEncapsulation.None,
                providers: [
                    ListOfValuesService,
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => ListOfValuesComponent),
                        multi: true
                    }
                ],
                styles: [".showLikeEditable>input{background-color:#fff!important}"]
            }] }
];
/** @nocollapse */
ListOfValuesComponent.ctorParameters = () => [
    { type: ListOfValuesService }
];
ListOfValuesComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    dataSource: [{ type: Input }],
    lovValueChanged: [{ type: Output }],
    addToDataSource: [{ type: Output }],
    lovModal: [{ type: ViewChild, args: ['lovModal',] }]
};
if (false) {
    /** @type {?} */
    ListOfValuesComponent.prototype.placeholder;
    /** @type {?} */
    ListOfValuesComponent.prototype.isReadOnly;
    /** @type {?} */
    ListOfValuesComponent.prototype.dataSource;
    /** @type {?} */
    ListOfValuesComponent.prototype.lovValueChanged;
    /** @type {?} */
    ListOfValuesComponent.prototype.addToDataSource;
    /** @type {?} */
    ListOfValuesComponent.prototype.lovModal;
    /** @type {?} */
    ListOfValuesComponent.prototype.value;
    /** @type {?} */
    ListOfValuesComponent.prototype.label;
    /** @type {?} */
    ListOfValuesComponent.prototype.freeText;
    /** @type {?} */
    ListOfValuesComponent.prototype.hasSelectedItem;
    /** @type {?} */
    ListOfValuesComponent.prototype.keyColumnValue;
    /** @type {?} */
    ListOfValuesComponent.prototype.valueColumnName;
    /** @type {?} */
    ListOfValuesComponent.prototype.labelColumnNames;
    /** @type {?} */
    ListOfValuesComponent.prototype.gridOptions;
    /** @type {?} */
    ListOfValuesComponent.prototype.initialData;
    /** @type {?} */
    ListOfValuesComponent.prototype.displayData;
    /** @type {?} */
    ListOfValuesComponent.prototype.genericExpression;
    /** @type {?} */
    ListOfValuesComponent.prototype.fieldIniData;
    /** @type {?} */
    ListOfValuesComponent.prototype.listOfValuesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekgsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBUTVCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUVsRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUUvRCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQWdCbkcsTUFBTSxPQUFPLHFCQUFxQjs7OztJQThCaEMsWUFBbUIsbUJBQXdDO1FBQXhDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUEzQmxELGVBQVUsR0FBRyxLQUFLLENBQUM7O1FBS2xCLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQWEsQ0FBQztRQUNoRCxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFzQnBELENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLG1CQUFtQixDQUFDLDRCQUE0QjthQUNsRCxTQUFTLENBQUMsQ0FBQyxNQUFhLEVBQUUsRUFBRTtZQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUMxQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsU0FBUztRQUNQLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsT0FBTyxFQUFFLElBQUk7WUFDYixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7U0FDOUIsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLGFBQWE7UUFDMUIsSUFBSSxhQUFhLEVBQUU7WUFDakIsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFFakQsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLFlBQVksS0FBSyxFQUFFOztzQkFDcEMsTUFBTSxHQUFhLG1CQUFVLElBQUksQ0FBQyxnQkFBZ0IsRUFBQTtnQkFDeEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQWtCLEVBQUUsRUFBRTtvQkFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzVELENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2FBQzlEO1lBRUQsa0NBQWtDO1lBQ2xDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDO2dCQUN4QixFQUFFLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVc7Z0JBQ3hDLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFO2FBQ3pFLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1NBQzlGO0lBQ0gsQ0FBQzs7OztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzdCLENBQUM7Ozs7Ozs7O0lBRUQsVUFBVSxDQUFDLFdBQXdCLEVBQUUsa0JBQTBCLElBQUksRUFBRSxtQkFBc0MsWUFBWSxFQUFFLFlBQXNCO1FBQzdJLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUV6QyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO2FBQU0sSUFBSSxXQUFXLEVBQUU7WUFDdEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3hGO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFRCxjQUFjLENBQUMsV0FBd0IsRUFBRSxRQUFrQixFQUFFLGVBQXdCLEVBQUUsZ0JBQXlCO1FBQzlHLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZGLElBQUksUUFBUSxFQUFFO2dCQUNaLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQjtTQUNGO1FBRUQsSUFBSSxlQUFlLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7U0FDeEM7UUFFRCxJQUFJLGdCQUFnQixFQUFFO1lBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztTQUMxQztJQUNILENBQUM7Ozs7Ozs7SUFFRCx1QkFBdUIsQ0FBQyxXQUF3QixFQUFFLGtCQUEwQixJQUFJLEVBQUUsbUJBQXNDLFlBQVk7UUFDbEksSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7UUFDdkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUN6RSwyREFBMkQ7WUFDM0QseURBQXlEO1lBQ3pELGlFQUFpRTtZQUVqRSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7O3NCQUNiLGVBQWUsR0FBUSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVc7dUJBQ2pILENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQ3hDLG1FQUFtRTtnQkFDbkUsaUVBQWlFO2dCQUNqRSw4REFBOEQ7Z0JBRzlELElBQUksZUFBZSxFQUFFO29CQUNuQixVQUFVLENBQUMsR0FBRyxFQUFFO3dCQUNkLElBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7d0JBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7b0JBQzNDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFFUCxPQUFPO2lCQUNSO2FBQ0Y7WUFFRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtnQkFDakYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2RixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO2dCQUM5QyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLFNBQVMsRUFBRTtvQkFDNUgsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQzlLO2lCQUNGO2dCQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRztvQkFDdkIsWUFBWSxFQUFFO3dCQUNaOzRCQUNFLE9BQU8sRUFBRTtnQ0FDUCxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFVBQVUsRUFBRSxjQUFjLENBQUMsT0FBTyxFQUFFOzZCQUNwRzt5QkFDRjtxQkFDRjtvQkFDRCxRQUFRLEVBQUUsQ0FBQztvQkFDWCxVQUFVLEVBQUUsQ0FBQztpQkFDZCxDQUFDO2dCQUVGLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFO29CQUNoRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUU7d0JBQzdELElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUN2RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDakQsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7aUJBQ0Y7Z0JBRUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ25JO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQywwQkFBb0M7UUFDM0MsSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQy9CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM1QjtRQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBRTdCLHFDQUFxQztRQUNyQyxtREFBbUQ7UUFDbkQsSUFBSTtRQUVKLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUMvQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2IsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEdBQVE7SUFDakIsQ0FBQzs7OztJQUVELFNBQVM7SUFDVCxDQUFDOzs7O0lBRUQsSUFBSSxjQUFjO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDOzs7OztJQUVELElBQUksY0FBYyxDQUFDLEdBQUc7UUFDcEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QscUNBQXFDO1FBQ3JDLG1EQUFtRDtRQUNuRCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2dCQUM1QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyQjtJQUNILENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFOztrQkFDekQsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztZQUVuRyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEVBQUU7O3NCQUN4RCxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDckcsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFFbEQsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUVuQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsWUFBWSxLQUFLLEVBQUU7OzhCQUNwQyxNQUFNLEdBQWEsbUJBQVUsSUFBSSxDQUFDLGdCQUFnQixFQUFBO3dCQUN4RCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBa0IsRUFBRSxFQUFFOzRCQUNwQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDM0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ25FLENBQUMsQ0FBQyxDQUFDO3FCQUNKO3lCQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO3dCQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQzt3QkFDOUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ2xFO29CQUdELGtDQUFrQztvQkFDbEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7d0JBQ3hCLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVzt3QkFDeEMsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7cUJBQ3pFLENBQUMsQ0FBQztvQkFFSCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDN0I7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtnQkFDMUIsT0FBTyxrQkFBa0IsQ0FBQzthQUMzQjtpQkFBTTtnQkFDTCxPQUFPLGtCQUFrQixDQUFDO2FBQzNCO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sWUFBWSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQzs7O1lBdFNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsdUJBQXVCO2dCQUNqQyxpckRBQThDO2dCQUU5QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsU0FBUyxFQUFFO29CQUNULG1CQUFtQjtvQkFDbkI7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQzt3QkFDcEQsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQ0Y7O2FBQ0Y7Ozs7WUFqQlEsbUJBQW1COzs7MEJBbUJ6QixLQUFLO3lCQUVMLEtBQUs7eUJBRUwsS0FBSzs4QkFHTCxNQUFNOzhCQUNOLE1BQU07dUJBRU4sU0FBUyxTQUFDLFVBQVU7Ozs7SUFWckIsNENBQThCOztJQUU5QiwyQ0FBNEI7O0lBRTVCLDJDQUE0Qjs7SUFHNUIsZ0RBQTBEOztJQUMxRCxnREFBb0Q7O0lBRXBELHlDQUE0RDs7SUFFNUQsc0NBQXFCOztJQUNyQixzQ0FBcUI7O0lBQ3JCLHlDQUF3Qjs7SUFDeEIsZ0RBQWdDOztJQUVoQywrQ0FBc0I7O0lBRXRCLGdEQUErQjs7SUFDL0IsaURBQTJDOztJQUUzQyw0Q0FBZ0M7O0lBQ2hDLDRDQUEwQjs7SUFFMUIsNENBQXdCOztJQUN4QixrREFBNEM7O0lBQzVDLDZDQUF5Qjs7SUFFYixvREFBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIElucHV0LCBWaWV3RW5jYXBzdWxhdGlvbiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbmltcG9ydCB7XHJcbiAgTG92UmVzdWx0LFxyXG4gIEdyaWRPcHRpb25zLFxyXG4gIEdlbmVyaWNFeHByZXNzaW9uXHJcbn0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbmltcG9ydCB7IFRleHRDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5cclxuaW1wb3J0IHsgTGlzdE9mVmFsdWVzU2VydmljZSB9IGZyb20gJy4vbGlzdC1vZi12YWx1ZXMuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBMaXN0T2ZWYWx1ZXNNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vbGlzdC1vZi12YWx1ZXMtbW9kYWwvbGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWxpc3Qtb2YtdmFsdWVzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2xpc3Qtb2YtdmFsdWVzLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIExpc3RPZlZhbHVlc1NlcnZpY2UsXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBMaXN0T2ZWYWx1ZXNDb21wb25lbnQpLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIExpc3RPZlZhbHVlc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKSBpc1JlYWRPbmx5ID0gZmFsc2U7XHJcblxyXG4gIEBJbnB1dCgpIGRhdGFTb3VyY2U/OiBhbnlbXTtcclxuXHJcbiAgLy8gQENvbnRlbnRDaGlsZChGb3JtVGV4dElucHV0Q29tcG9uZW50KSBmb3JtVGV4dElucHV0Q29tcG9uZW50O1xyXG4gIEBPdXRwdXQoKSBsb3ZWYWx1ZUNoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyPExvdlJlc3VsdD4oKTtcclxuICBAT3V0cHV0KCkgYWRkVG9EYXRhU291cmNlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ2xvdk1vZGFsJykgbG92TW9kYWw6IExpc3RPZlZhbHVlc01vZGFsQ29tcG9uZW50O1xyXG5cclxuICBwdWJsaWMgdmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgbGFiZWw6IHN0cmluZztcclxuICBwdWJsaWMgZnJlZVRleHQ6IHN0cmluZztcclxuICBwdWJsaWMgaGFzU2VsZWN0ZWRJdGVtOiBib29sZWFuO1xyXG5cclxuICBwdWJsaWMga2V5Q29sdW1uVmFsdWU7XHJcblxyXG4gIHB1YmxpYyB2YWx1ZUNvbHVtbk5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgbGFiZWxDb2x1bW5OYW1lczogc3RyaW5nIHwgc3RyaW5nW107XHJcblxyXG4gIHB1YmxpYyBncmlkT3B0aW9uczogR3JpZE9wdGlvbnM7XHJcbiAgcHVibGljIGluaXRpYWxEYXRhOiBhbnlbXTtcclxuXHJcbiAgcHVibGljIGRpc3BsYXlEYXRhOiBhbnk7XHJcbiAgcHVibGljIGdlbmVyaWNFeHByZXNzaW9uOiBHZW5lcmljRXhwcmVzc2lvbjtcclxuICBwdWJsaWMgZmllbGRJbmlEYXRhOiBhbnk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBsaXN0T2ZWYWx1ZXNTZXJ2aWNlOiBMaXN0T2ZWYWx1ZXNTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMubGlzdE9mVmFsdWVzU2VydmljZS5sb3ZTZXJ2aWNlUmVzdWx0U3RhdGVDaGFuZ2VkXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55W10pID0+IHtcclxuICAgICAgICB0aGlzLmluaXRpYWxEYXRhID0gcmVzdWx0O1xyXG4gICAgICAgIHRoaXMuc2V0TG92RmllbGRzKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgb3Blbk1vZGFsKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHtcclxuICAgICAgZGlzcGxheTogdHJ1ZSxcclxuICAgICAgZ3JpZE9wdGlvbnM6IHRoaXMuZ3JpZE9wdGlvbnNcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBnZXRTZWxlY3RlZFJvdyhzZWxlY3RlZE1vZGVsKSB7XHJcbiAgICBpZiAoc2VsZWN0ZWRNb2RlbCkge1xyXG4gICAgICB0aGlzLktleUNvbHVtblZhbHVlID0gc2VsZWN0ZWRNb2RlbFt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXTtcclxuICAgICAgdGhpcy52YWx1ZSA9IHNlbGVjdGVkTW9kZWxbdGhpcy52YWx1ZUNvbHVtbk5hbWVdO1xyXG5cclxuICAgICAgdGhpcy5sYWJlbCA9ICcnO1xyXG4gICAgICBpZiAodGhpcy5sYWJlbENvbHVtbk5hbWVzIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgICBjb25zdCBsYWJlbHM6IHN0cmluZ1tdID0gPHN0cmluZ1tdPnRoaXMubGFiZWxDb2x1bW5OYW1lcztcclxuICAgICAgICBsYWJlbHMuZm9yRWFjaCgoY29sdW1uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmxhYmVsID0gdGhpcy5sYWJlbCArICcgJyArIHNlbGVjdGVkTW9kZWxbY29sdW1uTmFtZV07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5sYWJlbENvbHVtbk5hbWVzKSB7XHJcbiAgICAgICAgdGhpcy5sYWJlbCA9IHNlbGVjdGVkTW9kZWxbdGhpcy5sYWJlbENvbHVtbk5hbWVzLnRvU3RyaW5nKCldO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBpdCB3aWxsIHNlbmQgdGhlIGdyaWQgZWRpdCBjZWxsXHJcbiAgICAgIHRoaXMuYWRkVG9EYXRhU291cmNlLmVtaXQoe1xyXG4gICAgICAgIElkOiB0aGlzLmxpc3RPZlZhbHVlc1NlcnZpY2UuZW5kUG9pbnRVcmwsXHJcbiAgICAgICAgVmFsdWU6IHsgaWQ6IHRoaXMuS2V5Q29sdW1uVmFsdWUsIHZhbHVlOiB0aGlzLnZhbHVlLCBsYWJlbDogdGhpcy5sYWJlbCB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgdGhpcy5oYXNTZWxlY3RlZEl0ZW0gPSB0cnVlO1xyXG4gICAgICB0aGlzLmxvdlZhbHVlQ2hhbmdlZC5lbWl0KHsgaWQ6IHRoaXMuS2V5Q29sdW1uVmFsdWUsIHZhbHVlOiB0aGlzLnZhbHVlLCBsYWJlbDogdGhpcy5sYWJlbCB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdyaWRSZWxvYWQoKSB7XHJcbiAgICB0aGlzLmxvdk1vZGFsLmdyaWRSZWxvYWQoKTtcclxuICB9XHJcblxyXG4gIHNldE9wdGlvbnMoZ3JpZE9wdGlvbnM6IEdyaWRPcHRpb25zLCB2YWx1ZUNvbHVtbk5hbWU6IHN0cmluZyA9ICdJZCcsIGxhYmVsQ29sdW1uTmFtZXM6IHN0cmluZyB8IHN0cmluZ1tdID0gJ0RlZmluaXRpb24nLCBkb250SW5pdERhdGE/OiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmdyaWRPcHRpb25zID0gZ3JpZE9wdGlvbnM7XHJcbiAgICB0aGlzLnZhbHVlQ29sdW1uTmFtZSA9IHZhbHVlQ29sdW1uTmFtZTtcclxuICAgIHRoaXMubGFiZWxDb2x1bW5OYW1lcyA9IGxhYmVsQ29sdW1uTmFtZXM7XHJcblxyXG4gICAgaWYgKCFkb250SW5pdERhdGEgJiYgdGhpcy5rZXlDb2x1bW5WYWx1ZSkge1xyXG4gICAgICB0aGlzLmluaXREYXRhKCk7XHJcbiAgICB9IGVsc2UgaWYgKGdyaWRPcHRpb25zKSB7XHJcbiAgICAgIHRoaXMubGlzdE9mVmFsdWVzU2VydmljZS5zZXRMb2FkVXJsU2V0dGluZyh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2V0R3JpZE9wdGlvbnMoZ3JpZE9wdGlvbnM6IEdyaWRPcHRpb25zLCBpbml0RGF0YT86IGJvb2xlYW4sIHZhbHVlQ29sdW1uTmFtZT86IHN0cmluZywgbGFiZWxDb2x1bW5OYW1lcz86IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkT3B0aW9ucyA9IGdyaWRPcHRpb25zO1xyXG4gICAgaWYgKGdyaWRPcHRpb25zKSB7XHJcbiAgICAgIHRoaXMubGlzdE9mVmFsdWVzU2VydmljZS5zZXRMb2FkVXJsU2V0dGluZyh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMpO1xyXG4gICAgICBpZiAoaW5pdERhdGEpIHtcclxuICAgICAgICB0aGlzLmluaXREYXRhKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodmFsdWVDb2x1bW5OYW1lKSB7XHJcbiAgICAgIHRoaXMudmFsdWVDb2x1bW5OYW1lID0gdmFsdWVDb2x1bW5OYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChsYWJlbENvbHVtbk5hbWVzKSB7XHJcbiAgICAgIHRoaXMubGFiZWxDb2x1bW5OYW1lcyA9IGxhYmVsQ29sdW1uTmFtZXM7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRPcHRpb25zV2l0aE5vUmVxdWVzdChncmlkT3B0aW9uczogR3JpZE9wdGlvbnMsIHZhbHVlQ29sdW1uTmFtZTogc3RyaW5nID0gJ0lkJywgbGFiZWxDb2x1bW5OYW1lczogc3RyaW5nIHwgc3RyaW5nW10gPSAnRGVmaW5pdGlvbicpIHtcclxuICAgIHRoaXMuZ3JpZE9wdGlvbnMgPSBncmlkT3B0aW9ucztcclxuICAgIHRoaXMudmFsdWVDb2x1bW5OYW1lID0gdmFsdWVDb2x1bW5OYW1lO1xyXG4gICAgdGhpcy5sYWJlbENvbHVtbk5hbWVzID0gbGFiZWxDb2x1bW5OYW1lcztcclxuICB9XHJcblxyXG4gIGluaXREYXRhKCkge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5rZXlDb2x1bW5WYWx1ZSAmJiB0aGlzLmtleUNvbHVtblZhbHVlICE9PSAtMSkge1xyXG4gICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSBncmlkT3B0aW9ucycsIHRoaXMuZ3JpZE9wdGlvbnMpO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSBkYXRhU291cmNlJywgdGhpcy5kYXRhU291cmNlKTtcclxuICAgICAgLy8gY29uc29sZS5sb2coJ2luaXREYXRhIC0ga2V5Q29sdW1uVmFsdWUnLCB0aGlzLmtleUNvbHVtblZhbHVlKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmRhdGFTb3VyY2UpIHtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZFJvd0RhdGE6IGFueSA9IHRoaXMuZGF0YVNvdXJjZS5maW5kKHggPT4geC5JZCA9PT0gdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLmVuZFBvaW50VXJsXHJcbiAgICAgICAgICAmJiB4LlZhbHVlLmlkID09PSB0aGlzLmtleUNvbHVtblZhbHVlKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSB2YWx1ZUNvbHVtbk5hbWUnLCB0aGlzLnZhbHVlQ29sdW1uTmFtZSk7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ2luaXREYXRhIC0ga2V5Q29sdW1uVmFsdWUnLCB0aGlzLmtleUNvbHVtblZhbHVlKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSBzZWxlY3RlZFJvd0RhdGEnLCBzZWxlY3RlZFJvd0RhdGEpO1xyXG5cclxuXHJcbiAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YSkge1xyXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubGFiZWwgPSBzZWxlY3RlZFJvd0RhdGEuVmFsdWUubGFiZWw7XHJcbiAgICAgICAgICAgIHRoaXMudmFsdWUgPSBzZWxlY3RlZFJvd0RhdGEuVmFsdWUudmFsdWU7XHJcbiAgICAgICAgICB9LCAxMCk7XHJcblxyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zKSB7XHJcbiAgICAgICAgdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLnNldExvYWRVcmxTZXR0aW5nKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucyk7XHJcbiAgICAgICAgdGhpcy5maWVsZEluaURhdGEgPSB0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkO1xyXG4gICAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSBmYWxzZSB8fCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUVuZFBvaW50ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgIGlmICh0aGlzLmZpZWxkSW5pRGF0YS5pbmRleE9mKCdJZCcpID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkSW5pRGF0YSA9IHRoaXMuZmllbGRJbmlEYXRhLnN1YnN0cigwLCB0aGlzLmZpZWxkSW5pRGF0YS5pbmRleE9mKCdJZCcpKSArICcuJyArIHRoaXMuZmllbGRJbmlEYXRhLnN1YnN0cih0aGlzLmZpZWxkSW5pRGF0YS5pbmRleE9mKCdJZCcpLCB0aGlzLmZpZWxkSW5pRGF0YS5sZW5ndGgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5nZW5lcmljRXhwcmVzc2lvbiA9IHtcclxuICAgICAgICAgIEZpbHRlckdyb3VwczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgRmlsdGVyczogW1xyXG4gICAgICAgICAgICAgICAgeyBQcm9wZXJ0eU5hbWU6IHRoaXMuZmllbGRJbmlEYXRhLCBWYWx1ZTogdGhpcy5rZXlDb2x1bW5WYWx1ZSwgQ29tcGFyaXNvbjogVGV4dENvbXBhcmlzb24uRXF1YWxUbyB9XHJcbiAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICBdLFxyXG4gICAgICAgICAgUGFnZVNpemU6IDEsXHJcbiAgICAgICAgICBQYWdlTnVtYmVyOiAxXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuZ2VuZXJpY0V4cHJlc3Npb24uRmlsdGVyR3JvdXBzLnB1c2goaXRlbSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLmxvYWREYXRhQnlGaWx0ZXJBbmRQYWdlU2V0dGluZyh0aGlzLmdlbmVyaWNFeHByZXNzaW9uLCB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkhlYWRlclBhcmFtZXRlcnMpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjbGVhckxvdihkaXNhYmxlU2V0VG9LZXlDb2x1bW5WYWx1ZT86IGJvb2xlYW4pIHtcclxuICAgIGlmICghZGlzYWJsZVNldFRvS2V5Q29sdW1uVmFsdWUpIHtcclxuICAgICAgdGhpcy5LZXlDb2x1bW5WYWx1ZSA9IG51bGw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmtleUNvbHVtblZhbHVlID0gbnVsbDtcclxuICAgIH1cclxuICAgIHRoaXMudmFsdWUgPSBudWxsO1xyXG4gICAgdGhpcy5sYWJlbCA9IG51bGw7XHJcbiAgICB0aGlzLmhhc1NlbGVjdGVkSXRlbSA9IGZhbHNlO1xyXG5cclxuICAgIC8vIGlmICh0aGlzLmZvcm1UZXh0SW5wdXRDb21wb25lbnQpIHtcclxuICAgIC8vICAgdGhpcy5mb3JtVGV4dElucHV0Q29tcG9uZW50LmlucHV0VmFsdWUgPSBudWxsO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIGlmICghZGlzYWJsZVNldFRvS2V5Q29sdW1uVmFsdWUpIHtcclxuICAgICAgdGhpcy5sb3ZWYWx1ZUNoYW5nZWQuZW1pdChudWxsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNob3dDbGVhckJ1dHRvbigpIHtcclxuICAgIHJldHVybiAhXy5pc05pbCh0aGlzLktleUNvbHVtblZhbHVlKTtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgS2V5Q29sdW1uVmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5rZXlDb2x1bW5WYWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBLZXlDb2x1bW5WYWx1ZSh2YWwpIHtcclxuICAgIHRoaXMua2V5Q29sdW1uVmFsdWUgPSB2YWw7XHJcbiAgICB0aGlzLnNldExvdkZpZWxkcygpO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKCd3cml0ZVZhbHVlMScsIHZhbHVlKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKCd3cml0ZVZhbHVlMicsIHRoaXMua2V5Q29sdW1uVmFsdWUpO1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIGlmICh2YWx1ZSAhPT0gdGhpcy5rZXlDb2x1bW5WYWx1ZSkge1xyXG4gICAgICAgIHRoaXMua2V5Q29sdW1uVmFsdWUgPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLmluaXREYXRhKCk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMua2V5Q29sdW1uVmFsdWUgPSB2YWx1ZTtcclxuICAgICAgdGhpcy5jbGVhckxvdih0cnVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xyXG4gIH1cclxuXHJcbiAgc2V0TG92RmllbGRzKCkge1xyXG4gICAgaWYgKCFfLmlzTmlsKHRoaXMua2V5Q29sdW1uVmFsdWUpICYmICFfLmlzTmlsKHRoaXMuZ3JpZE9wdGlvbnMpKSB7XHJcbiAgICAgIGNvbnN0IGtleUNvbHVtbiA9IHRoaXMuZ3JpZE9wdGlvbnMuY29sdW1ucy5maW5kKChpdGVtKSA9PiBpdGVtLmZpZWxkID09PSB0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkKTtcclxuXHJcbiAgICAgIGlmICghXy5pc05pbCh0aGlzLmluaXRpYWxEYXRhKSAmJiBrZXlDb2x1bW4gJiYga2V5Q29sdW1uLmZpZWxkKSB7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyZWRPYmplY3QgPSB0aGlzLmluaXRpYWxEYXRhLmZpbmQoKGl0ZW0pID0+IGl0ZW1ba2V5Q29sdW1uLmZpZWxkXSA9PT0gdGhpcy5rZXlDb2x1bW5WYWx1ZSk7XHJcbiAgICAgICAgaWYgKCFfLmlzTmlsKGZpbHRlcmVkT2JqZWN0KSkge1xyXG4gICAgICAgICAgdGhpcy52YWx1ZSA9IGZpbHRlcmVkT2JqZWN0W3RoaXMudmFsdWVDb2x1bW5OYW1lXTtcclxuXHJcbiAgICAgICAgICB0aGlzLmxhYmVsID0gJyc7XHJcbiAgICAgICAgICB0aGlzLmZyZWVUZXh0ID0gJyc7XHJcblxyXG4gICAgICAgICAgaWYgKHRoaXMubGFiZWxDb2x1bW5OYW1lcyBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxhYmVsczogc3RyaW5nW10gPSA8c3RyaW5nW10+dGhpcy5sYWJlbENvbHVtbk5hbWVzO1xyXG4gICAgICAgICAgICBsYWJlbHMuZm9yRWFjaCgoY29sdW1uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5sYWJlbCA9IHRoaXMubGFiZWwgKyAnICcgKyBmaWx0ZXJlZE9iamVjdFtjb2x1bW5OYW1lXTtcclxuICAgICAgICAgICAgICB0aGlzLmZyZWVUZXh0ID0gdGhpcy5mcmVlVGV4dCArICcgJyArIGZpbHRlcmVkT2JqZWN0W2NvbHVtbk5hbWVdO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5sYWJlbENvbHVtbk5hbWVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubGFiZWwgPSBmaWx0ZXJlZE9iamVjdFt0aGlzLmxhYmVsQ29sdW1uTmFtZXMudG9TdHJpbmcoKV07XHJcbiAgICAgICAgICAgIHRoaXMuZnJlZVRleHQgPSBmaWx0ZXJlZE9iamVjdFt0aGlzLmxhYmVsQ29sdW1uTmFtZXMudG9TdHJpbmcoKV07XHJcbiAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgIC8vIGl0IHdpbGwgc2VuZCB0aGUgZ3JpZCBlZGl0IGNlbGxcclxuICAgICAgICAgIHRoaXMuYWRkVG9EYXRhU291cmNlLmVtaXQoe1xyXG4gICAgICAgICAgICBJZDogdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLmVuZFBvaW50VXJsLFxyXG4gICAgICAgICAgICBWYWx1ZTogeyBpZDogdGhpcy5LZXlDb2x1bW5WYWx1ZSwgdmFsdWU6IHRoaXMudmFsdWUsIGxhYmVsOiB0aGlzLmxhYmVsIH1cclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgIHRoaXMuaGFzU2VsZWN0ZWRJdGVtID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldElucHV0RmlsZWRXaWR0aCgpOiBzdHJpbmcge1xyXG4gICAgaWYgKCF0aGlzLmlzUmVhZE9ubHkpIHtcclxuICAgICAgaWYgKHRoaXMuc2hvd0NsZWFyQnV0dG9uKCkpIHtcclxuICAgICAgICByZXR1cm4gJ2NhbGMoNjAlIC0gNzZweCknO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiAnY2FsYyg2MCUgLSAzNnB4KSc7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiAnY2FsYyg2MCUgKSc7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==