/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DataService } from '../../services/index';
import * as i0 from "@angular/core";
import * as i1 from "../../services/http/data.service";
export class ListOfValuesService {
    /**
     * @param {?} dataService
     */
    constructor(dataService) {
        this.dataService = dataService;
        this.lovServiceResultStateChanged = new Subject();
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    setLoadUrlSetting(urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    }
    /**
     * @param {?} expression
     * @param {?=} headerParameters
     * @return {?}
     */
    loadDataByFilterAndPageSetting(expression, headerParameters) {
        /** @type {?} */
        const reqUrl = `${this.moduleUrl}${this.endPointUrl}`;
        this.dataService.getListBy(reqUrl, expression, headerParameters)
            .subscribe((httpResponse) => {
            this.pagingResult = httpResponse.pagingResult;
            this.lovData = httpResponse.serviceResult.Result;
            this.lovServiceResultStateChanged.next(this.lovData);
        });
    }
}
ListOfValuesService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ListOfValuesService.ctorParameters = () => [
    { type: DataService }
];
/** @nocollapse */ ListOfValuesService.ngInjectableDef = i0.defineInjectable({ factory: function ListOfValuesService_Factory() { return new ListOfValuesService(i0.inject(i1.DataService)); }, token: ListOfValuesService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ListOfValuesService.prototype.lovServiceResultStateChanged;
    /** @type {?} */
    ListOfValuesService.prototype.lovData;
    /** @type {?} */
    ListOfValuesService.prototype.pagingResult;
    /** @type {?} */
    ListOfValuesService.prototype.moduleUrl;
    /** @type {?} */
    ListOfValuesService.prototype.endPointUrl;
    /**
     * @type {?}
     * @private
     */
    ListOfValuesService.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2xpc3Qtb2YtdmFsdWVzL2xpc3Qtb2YtdmFsdWVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQVUvQixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7OztBQUtuRCxNQUFNLE9BQU8sbUJBQW1COzs7O0lBUTlCLFlBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBUHJDLGlDQUE0QixHQUFHLElBQUksT0FBTyxFQUFTLENBQUM7SUFRM0QsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxVQUFzQjtRQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDO0lBQzVDLENBQUM7Ozs7OztJQUVELDhCQUE4QixDQUFDLFVBQTZCLEVBQUUsZ0JBQW9DOztjQUMxRixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUU7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQzthQUM3RCxTQUFTLENBQUMsQ0FBQyxZQUFtQyxFQUFFLEVBQUU7WUFDakQsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO1lBQzlDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDakQsSUFBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7WUEzQkYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBSlEsV0FBVzs7Ozs7SUFNbEIsMkRBQTJEOztJQUMzRCxzQ0FBc0I7O0lBQ3RCLDJDQUFrQzs7SUFFbEMsd0NBQXlCOztJQUN6QiwwQ0FBMkI7Ozs7O0lBRWYsMENBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQge1xyXG4gIENvcmVIdHRwUmVzcG9uc2UsXHJcbiAgUGFnaW5nUmVzdWx0LFxyXG4gIEdlbmVyaWNFeHByZXNzaW9uLFxyXG4gIEhlYWRlclBhcmFtZXRlcixcclxuICBVcmxPcHRpb25zXHJcbn0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbmltcG9ydCB7IERhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTGlzdE9mVmFsdWVzU2VydmljZSB7XHJcbiAgcHVibGljIGxvdlNlcnZpY2VSZXN1bHRTdGF0ZUNoYW5nZWQgPSBuZXcgU3ViamVjdDxhbnlbXT4oKTtcclxuICBwdWJsaWMgbG92RGF0YTogYW55W107XHJcbiAgcHVibGljIHBhZ2luZ1Jlc3VsdDogUGFnaW5nUmVzdWx0O1xyXG5cclxuICBwdWJsaWMgbW9kdWxlVXJsOiBzdHJpbmc7XHJcbiAgcHVibGljIGVuZFBvaW50VXJsOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGF0YVNlcnZpY2U6IERhdGFTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBzZXRMb2FkVXJsU2V0dGluZyh1cmxPcHRpb25zOiBVcmxPcHRpb25zKSB7XHJcbiAgICB0aGlzLm1vZHVsZVVybCA9IHVybE9wdGlvbnMubW9kdWxlVXJsO1xyXG4gICAgdGhpcy5lbmRQb2ludFVybCA9IHVybE9wdGlvbnMuZW5kUG9pbnRVcmw7XHJcbiAgfVxyXG5cclxuICBsb2FkRGF0YUJ5RmlsdGVyQW5kUGFnZVNldHRpbmcoZXhwcmVzc2lvbjogR2VuZXJpY0V4cHJlc3Npb24sIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgY29uc3QgcmVxVXJsID0gYCR7dGhpcy5tb2R1bGVVcmx9JHt0aGlzLmVuZFBvaW50VXJsfWA7XHJcbiAgICB0aGlzLmRhdGFTZXJ2aWNlLmdldExpc3RCeShyZXFVcmwsIGV4cHJlc3Npb24sIGhlYWRlclBhcmFtZXRlcnMpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGh0dHBSZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XHJcbiAgICAgICAgdGhpcy5wYWdpbmdSZXN1bHQgPSBodHRwUmVzcG9uc2UucGFnaW5nUmVzdWx0O1xyXG4gICAgICAgIHRoaXMubG92RGF0YSA9IGh0dHBSZXNwb25zZS5zZXJ2aWNlUmVzdWx0LlJlc3VsdDtcclxuICAgICAgICB0aGlzLmxvdlNlcnZpY2VSZXN1bHRTdGF0ZUNoYW5nZWQubmV4dCh0aGlzLmxvdkRhdGEpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbn1cclxuIl19