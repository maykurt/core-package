/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
export class BaseComponent {
    /**
     * @param {?} preventUnsavedChangesService
     */
    constructor(preventUnsavedChangesService) {
        this.preventUnsavedChangesService = preventUnsavedChangesService;
        this.subscriptions = [];
        this.isSaveOperationEnabled = true;
        this.safeSubscription(this.preventUnsavedChangesService.getStatus()
            .subscribe((status) => {
            if (status === 'waiting') {
                this.preventUnsavedChangesService.setIsChanged(this.skipUnsavedChanges ? false : this.hasUnsavedChanges());
                this.preventUnsavedChangesService.setStatus('finished');
            }
        }));
    }
    /**
     * @protected
     * @param {?} sub
     * @return {?}
     */
    safeSubscription(sub) {
        this.subscriptions.push(sub);
        return sub;
    }
    /**
     * @private
     * @return {?}
     */
    unsubscribeAll() {
        this.subscriptions.forEach((subs) => {
            if (subs) {
                subs.unsubscribe();
            }
        });
    }
    /**
     * @protected
     * @return {?}
     */
    baseNgOnDestroy() {
        this.unsubscribeAll();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    BaseComponent.prototype.subscriptions;
    /** @type {?} */
    BaseComponent.prototype.skipUnsavedChanges;
    /** @type {?} */
    BaseComponent.prototype.isSaveOperationEnabled;
    /**
     * @type {?}
     * @protected
     */
    BaseComponent.prototype.preventUnsavedChangesService;
    /**
     * @abstract
     * @return {?}
     */
    BaseComponent.prototype.hasUnsavedChanges = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC9iYXNlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsTUFBTSxPQUFnQixhQUFhOzs7O0lBS2pDLFlBQXNCLDRCQUEwRDtRQUExRCxpQ0FBNEIsR0FBNUIsNEJBQTRCLENBQThCO1FBSnhFLGtCQUFhLEdBQW1CLEVBQUUsQ0FBQztRQUVwQywyQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFHbkMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxTQUFTLEVBQUU7YUFDaEUsU0FBUyxDQUFDLENBQUMsTUFBYyxFQUFFLEVBQUU7WUFDNUIsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO2dCQUN4QixJQUFJLENBQUMsNEJBQTRCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO2dCQUMzRyxJQUFJLENBQUMsNEJBQTRCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3pEO1FBQ0gsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7OztJQUlTLGdCQUFnQixDQUFDLEdBQWlCO1FBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQzs7Ozs7SUFHTyxjQUFjO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBa0IsRUFBRSxFQUFFO1lBQ2hELElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFUyxlQUFlO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0NBQ0Y7Ozs7OztJQWpDQyxzQ0FBMkM7O0lBQzNDLDJDQUFtQzs7SUFDbkMsK0NBQXFDOzs7OztJQUV6QixxREFBb0U7Ozs7O0lBVWhGLDREQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY3VzdG9tL3ByZXZlbnQtdW5zYXZlZC1jaGFuZ2VzLnNlcnZpY2UnO1xyXG5cclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlQ29tcG9uZW50IHtcclxuICBwcml2YXRlIHN1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbltdID0gW107XHJcbiAgcHVibGljIHNraXBVbnNhdmVkQ2hhbmdlczogYm9vbGVhbjtcclxuICBwdWJsaWMgaXNTYXZlT3BlcmF0aW9uRW5hYmxlZCA9IHRydWU7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBwcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlOiBQcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLnNhZmVTdWJzY3JpcHRpb24odGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLmdldFN0YXR1cygpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHN0YXR1czogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgaWYgKHN0YXR1cyA9PT0gJ3dhaXRpbmcnKSB7XHJcbiAgICAgICAgICB0aGlzLnByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2Uuc2V0SXNDaGFuZ2VkKHRoaXMuc2tpcFVuc2F2ZWRDaGFuZ2VzID8gZmFsc2UgOiB0aGlzLmhhc1Vuc2F2ZWRDaGFuZ2VzKCkpO1xyXG4gICAgICAgICAgdGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLnNldFN0YXR1cygnZmluaXNoZWQnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGFic3RyYWN0IGhhc1Vuc2F2ZWRDaGFuZ2VzKCk6IGJvb2xlYW47XHJcblxyXG4gIHByb3RlY3RlZCBzYWZlU3Vic2NyaXB0aW9uKHN1YjogU3Vic2NyaXB0aW9uKTogU3Vic2NyaXB0aW9uIHtcclxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHN1Yik7XHJcbiAgICByZXR1cm4gc3ViO1xyXG4gIH1cclxuXHJcblxyXG4gIHByaXZhdGUgdW5zdWJzY3JpYmVBbGwoKSB7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaCgoc3ViczogU3Vic2NyaXB0aW9uKSA9PiB7XHJcbiAgICAgIGlmIChzdWJzKSB7XHJcbiAgICAgICAgc3Vicy51bnN1YnNjcmliZSgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBiYXNlTmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlQWxsKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==