export declare enum FormButtonType {
    Save = 1,
    Update = 2,
    Cancel = 3
}
