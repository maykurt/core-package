export declare enum MaskType {
    Phone = "phone",
    Mail = "mail",
    PostCode = "postCode"
}
