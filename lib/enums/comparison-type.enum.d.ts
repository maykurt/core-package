export declare enum Comparison {
    In = 0,
    LessThanOrEqualTo = 1,
    LessThan = 2,
    IsNullOrWhiteSpace = 3,
    IsNull = 4,
    IsNotNullNorWhiteSpace = 5,
    IsNotNull = 6,
    IsNotEmpty = 7,
    IsEmpty = 8,
    StartsWith = 9,
    GreaterThanOrEqualTo = 10,
    GreaterThan = 11,
    EqualTo = 12,
    EndsWith = 13,
    DoesNotContain = 14,
    Contains = 15,
    Between = 16,
    NotEqualTo = 17
}
export declare enum DefaultComparison {
    EqualTo = 12,
    NotEqualTo = 17
}
export declare enum TextComparison {
    Contains = 15,
    DoesNotContain = 14,
    EndsWith = 13,
    EqualTo = 12,
    IsEmpty = 8,
    IsNotEmpty = 7,
    IsNotNull = 6,
    IsNotNullNorWhiteSpace = 5,
    IsNull = 4,
    IsNullOrWhiteSpace = 3,
    NotEqualTo = 17,
    StartsWith = 9
}
export declare enum NumberComparison {
    Between = 16,
    EqualTo = 12,
    GreaterThan = 11,
    GreaterThanOrEqualTo = 10,
    LessThan = 2,
    LessThanOrEqualTo = 1,
    NotEqualTo = 17
}
export declare enum BooleanComparison {
    EqualTo = 12,
    NotEqualTo = 17
}
export declare enum DateComparison {
    Between = 16,
    EqualTo = 12,
    GreaterThan = 11,
    GreaterThanOrEqualTo = 10,
    LessThan = 2,
    LessThanOrEqualTo = 1,
    NotEqualTo = 17
}
export declare enum ComparisonType {
    Default = 1,
    Text = 2,
    Number = 3,
    Date = 4,
    Boolean = 5
}
export declare namespace ComparisonList {
    function values(comparisonType?: ComparisonType): string[];
    function getEnumAccordingToType(comparisonType?: ComparisonType): any;
    function createKeyLabelArray(comparisonType: ComparisonType, key?: string, label?: string): any;
}
