export declare enum AlertDismissReason {
    Cancel = "cancel",
    Backdrop = "backdrop",
    Close = "close",
    Esc = "esc",
    Timer = "timer"
}
