export declare enum Storage {
    AccessToken = "access_token",
    RefreshToken = "refresh_token",
    Expires = "expires",
    User = "user",
    SystemRefDate = "system_ref_date",
    CurrentLanguage = "current_language"
}
