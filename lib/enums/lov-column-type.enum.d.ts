export declare enum LovColumnType {
    Label = "label",
    Text = "text",
    Date = "date",
    Select = "select",
    Numeric = "numeric",
    Boolean = "boolean",
    Lov = "lov"
}
