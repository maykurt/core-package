export declare enum AlertType {
    Warning = "warning",
    Error = "error",
    Success = "success",
    Info = "info",
    Question = "question"
}
