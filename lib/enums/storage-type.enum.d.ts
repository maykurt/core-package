export declare enum StorageType {
    LocalStorage = "localStorage",
    SessionStorage = "sessionStorage"
}
