export declare enum CoreGridColumnType {
    Label = "label",
    Text = "text",
    Date = "date",
    Select = "select",
    Numeric = "numeric",
    Boolean = "boolean",
    Lov = "lov",
    Mask = "mask",
    Currency = "currency",
    NumericLabel = "numeric-label",
    TextSelect = "text-select"
}
