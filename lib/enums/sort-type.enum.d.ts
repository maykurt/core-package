export declare enum SortType {
    Asc = 0,
    Desc = 1,
    None = 2
}
