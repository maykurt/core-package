export declare enum ConfirmDialogOperationType {
    Add = "add",
    Update = "update",
    Delete = "delete",
    Undo = "undo",
    Cancel = "cancel",
    Refresh = "refresh",
    Process = "process"
}
