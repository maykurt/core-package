export declare enum DateSelectionMode {
    Single = "single",
    Multiple = "multiple",
    Range = "range"
}
