export declare enum CoreGridSelectionType {
    None = "none",
    Single = "single",
    Multiple = "multiple"
}
