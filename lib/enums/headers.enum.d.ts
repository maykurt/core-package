export declare enum Headers {
    CoreId = "X-CoreId",
    Behaviour = "X-Behaviour",
    ReferenceDate = "ReferenceDate",
    PagingHeader = "X-Paging-Header",
    PagingResponseHeader = "X-Paging-Response-Header",
    ReferenceDateHeader = "X-ReferenceDate-Header",
    DisableLoading = "disableGeneralLoading",
    DisableNavigation = "disableNavigation",
    FileOperation = "fileOperation",
    ContentType = "Content-Type",
    Authentication = "Authentication",
    BreadcrumbCustomLabel = "BreadcrumbCustomLabel"
}
