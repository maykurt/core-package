export declare enum OperationType {
    None = 0,
    Created = 1,
    Updated = 2,
    Deleted = 3
}
