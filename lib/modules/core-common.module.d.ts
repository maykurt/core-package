import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export declare const customCurrencyMaskConfig: {
    align: string;
    allowNegative: boolean;
    allowZero: boolean;
    decimal: string;
    precision: number;
    prefix: string;
    suffix: string;
    thousands: string;
    nullable: boolean;
};
export declare function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader;
export declare class CoreCommonModule {
}
