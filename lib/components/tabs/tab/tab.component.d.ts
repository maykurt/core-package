import { OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TabsComponent } from '../tabs.component';
import { Subscription } from 'rxjs';
export declare class TabComponent implements OnInit, OnDestroy {
    parent: TabsComponent;
    private router;
    private _ngZone;
    title: string;
    link?: string;
    isCurrent: boolean;
    tabChangeSubcriotion: Subscription;
    constructor(parent: TabsComponent, router: Router, _ngZone: NgZone);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
