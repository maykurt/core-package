import { OnChanges, EventEmitter, SimpleChanges, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
export declare class TabsComponent implements OnChanges, OnDestroy {
    private router;
    private activatedRoute;
    activeTab: string;
    tabChange: EventEmitter<{}>;
    tabs: any[];
    foundData: any;
    private routingSubscription;
    constructor(router: Router, activatedRoute: ActivatedRoute);
    ngOnChanges(changes: SimpleChanges): void;
    getClassActive(tab: any): boolean;
    onTabClick(tab: any): void;
    addTabs(title: string, link: string): void;
    ngOnDestroy(): void;
}
