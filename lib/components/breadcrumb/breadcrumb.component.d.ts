import { OnDestroy, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import 'rxjs/add/operator/filter';
import { BreadcrumbService } from '../../services/custom/breadcrumb.service';
export declare class BreadcrumbComponent implements OnDestroy {
    breadcrumbService: BreadcrumbService;
    private router;
    private activatedRoute;
    private ngZone;
    private routingSubscription;
    isFixed?: boolean;
    constructor(breadcrumbService: BreadcrumbService, router: Router, activatedRoute: ActivatedRoute, ngZone: NgZone);
    private getBreadcrumbs;
    getFullPath(route: ActivatedRouteSnapshot): string;
    onClick(url: string): void;
    ngOnDestroy(): void;
}
