import { DataService } from '../../services/http/data.service';
import { Subject } from 'rxjs';
import { HeaderParameter, GenericExpression, UrlOptions } from '../../models/index';
export declare class SelectorService {
    private dataService;
    listStateChanged: Subject<any[]>;
    counter: number;
    private moduleUrl;
    private endPointUrl;
    private headerParameters;
    constructor(dataService: DataService);
    setLoadUrlSetting(urlOptions: UrlOptions): void;
    getLoadUrlSetting(): UrlOptions;
    setHeaders(headerParameters: HeaderParameter[]): void;
    getHeaders(): HeaderParameter[];
    loadDataByFilterAndPageSetting(expression: GenericExpression): void;
}
