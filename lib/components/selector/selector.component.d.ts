import { EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { SelectorService } from './selector.service';
import { RequestOptions } from '../../models/index';
export declare class SelectorComponent implements ControlValueAccessor, OnChanges {
    private selectorService;
    isDisabled: boolean;
    isReadOnly: boolean;
    searchable: boolean;
    clearable: boolean;
    requestOptions?: RequestOptions;
    changed: EventEmitter<any>;
    firstValue?: EventEmitter<any>;
    private isFirstValueSend;
    listData: any[];
    value: string;
    labelField: string | string[];
    _labelField: string;
    seperator: string;
    readOnlyValue: string;
    private model;
    private customFilter;
    private urlOptions;
    constructor(selectorService: SelectorService);
    ngOnChanges(changes: SimpleChanges): void;
    setRequestOptions(requestOptions: RequestOptions, value?: string, labelField?: any, seperator?: string): void;
    load(): void;
    reload(): void;
    reset(): void;
    onChange(val: any): void;
    onTouched(): void;
    Model: any;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    setDisabledState(isDisabled: boolean): void;
    writeValue(value: any): void;
    findReadonlyValue(): void;
    private getDataSource;
}
