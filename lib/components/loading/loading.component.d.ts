import { OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { LoadingService } from '../../services/index';
export declare class LoadingComponent implements OnInit, OnDestroy {
    private loadingService;
    changeDetector: ChangeDetectorRef;
    showLoading: boolean;
    constructor(loadingService: LoadingService, changeDetector: ChangeDetectorRef);
    ngOnInit(): void;
    detectChanges(): void;
    ngOnDestroy(): void;
}
