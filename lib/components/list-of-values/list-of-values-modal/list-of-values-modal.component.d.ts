import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { GridOptions } from '../../../models/grid-options.model';
import { CoreGridComponent } from '../../core-grid/core-grid.component';
export declare class ListOfValuesModalComponent implements OnChanges {
    displayData: any;
    getSelectedRow: EventEmitter<{}>;
    content: CoreGridComponent;
    coreGrid: CoreGridComponent;
    gridOptions: GridOptions;
    constructor();
    ngOnChanges(changes: SimpleChanges): void;
    save(): void;
    close(): void;
    selectedChanged(selectedRows: any): void;
    gridReload(): void;
}
