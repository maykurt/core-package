import { Subject } from 'rxjs';
import { PagingResult, GenericExpression, HeaderParameter, UrlOptions } from '../../models/index';
import { DataService } from '../../services/index';
export declare class ListOfValuesService {
    private dataService;
    lovServiceResultStateChanged: Subject<any[]>;
    lovData: any[];
    pagingResult: PagingResult;
    moduleUrl: string;
    endPointUrl: string;
    constructor(dataService: DataService);
    setLoadUrlSetting(urlOptions: UrlOptions): void;
    loadDataByFilterAndPageSetting(expression: GenericExpression, headerParameters?: HeaderParameter[]): void;
}
