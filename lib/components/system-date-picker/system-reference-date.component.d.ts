import { OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { SystemReferenceDateService, StorageService } from '../../services/index';
export declare class SystemReferenceDateComponent implements OnDestroy {
    private systemReferenceDateService;
    private storageService;
    private fb;
    private translate;
    systemRefDatePicker: any;
    sysRefDateFormGroup: FormGroup;
    _calendarDateValue: Date;
    locale: any;
    localeTr: any;
    localEn: any;
    translateSubscription: Subscription;
    sysRefDateSubscription: Subscription;
    constructor(systemReferenceDateService: SystemReferenceDateService, storageService: StorageService, fb: FormBuilder, translate: TranslateService);
    calendarDateValue: Date;
    setSelectedDate(selectedDateISO: string): void;
    onClose(): void;
    keyDownFunction(event: any): void;
    checkAndApplyDate(): void;
    ngOnDestroy(): void;
}
