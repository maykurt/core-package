import { OnChanges, EventEmitter, SimpleChanges } from '@angular/core';
import { ToastrUtilsService } from '../../../services/index';
export declare class FormWizardComponent implements OnChanges {
    private toastrUtilsService;
    finishText: string;
    step: number;
    finish: EventEmitter<{}>;
    stepChange: EventEmitter<{}>;
    steps: any[];
    isValid: boolean;
    constructor(toastrUtilsService: ToastrUtilsService);
    ngOnChanges(changes: SimpleChanges): void;
    getClassActive(index: number): boolean;
    onStepChanged(isNext: boolean): void;
    isOnFirstStep(): boolean;
    isOnFinalStep(): boolean;
    getClassNameAToSteps(): string;
    addStep(title: string): number;
}
