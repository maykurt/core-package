import { OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { FormWizardComponent } from '../form-wizard/form-wizard.component';
import { Subscription } from 'rxjs';
export declare class FormWizardStepComponent implements OnInit, OnDestroy, OnChanges {
    private parent;
    isCurrent: boolean;
    step: number;
    stepChangeSubcriotion: Subscription;
    title: string;
    isValid: boolean;
    constructor(parent: FormWizardComponent);
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
}
