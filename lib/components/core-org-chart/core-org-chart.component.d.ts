import { OnInit, EventEmitter, OnChanges, SimpleChanges, NgZone } from '@angular/core';
export declare class CoreOrgChartComponent implements OnInit, OnChanges {
    private ngZone;
    export?: any;
    clickedExport: EventEmitter<any>;
    datasource: any;
    orgChart: any;
    constructor(ngZone: NgZone);
    ngOnChanges(simpleChanges: SimpleChanges): void;
    ngOnInit(): void;
    refreshOrgChart(orgChart: any, datasource: any): void;
    fileChange(event: any): void;
    readFile(file: any, refreshOrgChart: any, orgChart: any): void;
    onClickZoom(zoomType: string): void;
    onClickExport(): void;
    onExport(): void;
    onClickResetAll(): void;
}
