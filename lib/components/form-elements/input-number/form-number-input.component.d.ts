import { EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class FormNumberInputComponent implements ControlValueAccessor, OnChanges {
    placeholder?: string;
    isDisabled: boolean;
    isReadOnly: boolean;
    hidden: boolean;
    clearable: Boolean;
    changed: EventEmitter<any>;
    maxLength?: number;
    private maxValue;
    _value: number;
    ngOnChanges(changes: SimpleChanges): void;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: number;
    registerOnChange(fn: any): void;
    writeValue(value: number): void;
    registerOnTouched(fn: any): void;
    onKeyUp(event: any): void;
    numberOnly(event: any): boolean;
}
