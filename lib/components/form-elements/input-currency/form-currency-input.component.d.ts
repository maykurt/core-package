import { EventEmitter } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class FormCurrencyInputComponent implements ControlValueAccessor {
    placeholder?: string;
    isDisabled: boolean;
    isReadOnly: boolean;
    hidden: boolean;
    changed: EventEmitter<any>;
    clearable: Boolean;
    _value: any;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: any;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
    onKeyDown(event: any): void;
}
