import { EventEmitter } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class FormCheckboxComponent implements ControlValueAccessor {
    value: any;
    isDisabled: boolean;
    isReadOnly: boolean;
    hidden: boolean;
    checkChanged: EventEmitter<boolean>;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: any;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
    onCheckChanged(event: boolean): void;
}
