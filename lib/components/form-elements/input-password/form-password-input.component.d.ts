import { ControlValueAccessor } from '@angular/forms';
export declare class FormPasswordInputComponent implements ControlValueAccessor {
    placeholder: any;
    private _value;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: any;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
}
