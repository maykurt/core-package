import { SimpleChanges, OnChanges, EventEmitter } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { MaskType } from '../../../enums/mask-type.enum';
export declare class FormMaskInputComponent implements OnChanges, ControlValueAccessor {
    maskType: MaskType;
    isDisabled: boolean;
    isReadOnly: boolean;
    hidden: boolean;
    placeholder?: string;
    clearable: Boolean;
    changed: EventEmitter<any>;
    _value: string;
    mask: any;
    constructor();
    ngOnChanges(changes: SimpleChanges): void;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: string;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
}
