import { EventEmitter } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare class FormTextInputComponent implements ControlValueAccessor {
    placeholder?: string;
    isDisabled: boolean;
    isReadOnly: boolean;
    hidden: boolean;
    clearable: boolean;
    disableSpace?: boolean;
    maxLength?: number;
    changed: EventEmitter<any>;
    numberOnly: boolean;
    _value: string;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: string;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
    onKeypress(event: any): boolean;
}
