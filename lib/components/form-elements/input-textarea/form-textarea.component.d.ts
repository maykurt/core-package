import { ControlValueAccessor } from '@angular/forms';
export declare class FormTextAreaComponent implements ControlValueAccessor {
    placeholder: any;
    isReadOnly: boolean;
    maxLength?: number;
    private _value;
    onChange(val: any): void;
    onTouched(): void;
    inputValue: any;
    registerOnChange(fn: any): void;
    writeValue(value: any): void;
    registerOnTouched(fn: any): void;
    onKeyDown(event: any): void;
}
