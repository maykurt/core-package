import { OnChanges, SimpleChanges, EventEmitter, OnDestroy } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { DateSettings } from '../../../models/date-settings.model';
import { DateSelectionMode } from '../../../enums/date-selection-mode.enum';
export declare class FormDatepickerInputComponent implements OnChanges, OnDestroy, ControlValueAccessor {
    private fb;
    private translate;
    _calendarDateValue: Date;
    selectedDateString: string | string[];
    private usedMometToDate;
    settings?: DateSettings;
    isDisabled?: boolean;
    isReadOnly: boolean;
    dateChanges?: EventEmitter<any>;
    placeholder?: string;
    selectionMode?: DateSelectionMode;
    _selectionMode: DateSelectionMode;
    defaultSettings: DateSettings;
    view: string;
    yearDataSource: Array<any>;
    formGroup: FormGroup;
    minDate: Date;
    maxDate: Date;
    locale: any;
    localeTr: any;
    localEn: any;
    translateSubscription: Subscription;
    onChange: any;
    onTouched: any;
    constructor(fb: FormBuilder, translate: TranslateService);
    ngOnChanges(changes: SimpleChanges): void;
    registerOnChange(fn: any): void;
    writeValue(value: string): void;
    registerOnTouched(fn: any): void;
    selectedDate: string | string[];
    calendarDateValue: Date;
    onChangeDate(): void;
    initiateYearDataSource(): void;
    onYearValueChanged(selectedData: any): void;
    convertDateToISO(date: Date): string;
    convertDateToDateUTC(dateAsString: string): Date;
    ngOnDestroy(): void;
}
