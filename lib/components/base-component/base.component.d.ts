import { Subscription } from 'rxjs';
import { PreventUnsavedChangesService } from '../../services/custom/prevent-unsaved-changes.service';
export declare abstract class BaseComponent {
    protected preventUnsavedChangesService: PreventUnsavedChangesService;
    private subscriptions;
    skipUnsavedChanges: boolean;
    isSaveOperationEnabled: boolean;
    constructor(preventUnsavedChangesService: PreventUnsavedChangesService);
    abstract hasUnsavedChanges(): boolean;
    protected safeSubscription(sub: Subscription): Subscription;
    private unsubscribeAll;
    protected baseNgOnDestroy(): void;
}
