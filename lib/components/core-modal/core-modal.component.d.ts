import { OnChanges } from '@angular/core';
import { CoreModalService } from '../../services/custom/core-modal.service';
export declare class CoreModalComponent implements OnChanges {
    private coreModalService;
    modalTitle?: string;
    lockBackground: boolean;
    closeOnEscape: boolean;
    maximizable: boolean;
    draggable: boolean;
    closable: boolean;
    showHeader: boolean;
    displayData?: any;
    width: number;
    modalWidth: number;
    _display: boolean;
    constructor(coreModalService: CoreModalService);
    ngOnChanges(changes: any): void;
    openModal(): void;
    closeModal(): void;
    onDialogShow(): void;
    onDialogHide(): void;
}
