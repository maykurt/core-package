import { OnDestroy } from '@angular/core';
import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { SelectorSettings } from '../../../../../models/selector-settings.model';
export declare class SelectEditCellComponent implements ICellRendererAngularComp, OnDestroy {
    formGroup: FormArray;
    key: any;
    settings: SelectorSettings;
    listData: any[];
    private value;
    private rowId;
    private params;
    private dataSubscription;
    private filterByFieldSubscription;
    private gridService;
    private filterByField;
    isReadOnly: boolean;
    agInit(params: any): void;
    createListeningSubscription(): void;
    refresh(params: any): boolean;
    getListData(): void;
    getFilterByFieldKey(): number;
    onValueChanged(data: any): void;
    setValue(value: any): void;
    isValid(): boolean;
    clearDataSubscription(): void;
    clearfilterByFieldSubscription(): void;
    ngOnDestroy(): void;
}
