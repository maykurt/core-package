import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { DateSettings } from '../../../../../models/date-settings.model';
export declare class DateEditCellComponent implements ICellRendererAngularComp {
    formGroup: FormArray;
    settings: DateSettings;
    key: any;
    private value;
    private rowId;
    private params;
    isReadOnly: boolean;
    agInit(params: any): void;
    refresh(params: any): boolean;
    onDateChanges(date: string): void;
    isValid(): boolean;
}
