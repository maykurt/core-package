import { OnDestroy } from '@angular/core';
import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ListOfValuesComponent } from '../../../../list-of-values/list-of-values.component';
export declare class LovEditCellComponent implements ICellRendererAngularComp, OnDestroy {
    content: any;
    private listOfValue;
    private settings;
    formGroup: FormArray;
    key: any;
    private value;
    private rowId;
    private params;
    isReadOnly: boolean;
    private rowChangesSubscription;
    dataSource: any[];
    agInit(params: any): void;
    refresh(params: any): boolean;
    setLovComponent(component: ListOfValuesComponent): void;
    isValid(): boolean;
    lovValueChanged(event: any): void;
    onAddToDataSource(data: any): void;
    clearRowChangesSubscription(): void;
    ngOnDestroy(): void;
}
