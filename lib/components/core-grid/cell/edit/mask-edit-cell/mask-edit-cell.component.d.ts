import { ICellRendererAngularComp } from 'ag-grid-angular';
import { FormArray } from '@angular/forms';
import { MaskType } from '../../../../../enums/mask-type.enum';
import { MaskSettings } from '../../../../../models/mask-settings.model';
export declare class MaskEditCellComponent implements ICellRendererAngularComp {
    private params;
    private rowId;
    formGroup: FormArray;
    key: any;
    value: any;
    settings: MaskSettings;
    maskType: MaskType;
    isReadOnly: boolean;
    agInit(params: any): void;
    refresh(params: any): boolean;
    onValueChanged(): void;
    isValid(): boolean;
}
