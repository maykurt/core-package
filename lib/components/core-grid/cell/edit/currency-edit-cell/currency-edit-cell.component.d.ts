import { ICellRendererAngularComp } from 'ag-grid-angular';
import { FormArray } from '@angular/forms';
export declare class CurrencyEditCellComponent implements ICellRendererAngularComp {
    private params;
    private rowId;
    formGroup: FormArray;
    key: any;
    value: any;
    isReadOnly: boolean;
    agInit(params: any): void;
    refresh(params: any): boolean;
    onValueChanged(): void;
    isValid(): boolean;
}
