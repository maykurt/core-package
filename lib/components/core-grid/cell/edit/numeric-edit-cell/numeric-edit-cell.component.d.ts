import { ICellRendererAngularComp } from 'ag-grid-angular';
import { FormArray } from '@angular/forms';
export declare class NumericEditCellComponent implements ICellRendererAngularComp {
    formGroup: FormArray;
    key: any;
    private params;
    value: any;
    private rowId;
    isReadOnly: boolean;
    agInit(params: any): void;
    refresh(params: any): boolean;
    onValueChanged(): void;
    isValid(): boolean;
}
