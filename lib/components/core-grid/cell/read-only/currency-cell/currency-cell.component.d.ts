import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
export declare class CurrencyCellComponent implements ICellRendererAngularComp {
    formGroup: FormArray;
    key: any;
    private params;
    value: any;
    private rowId;
    agInit(params: any): void;
    refresh(params: any): boolean;
}
