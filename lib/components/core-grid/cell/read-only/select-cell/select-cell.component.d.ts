import { OnDestroy } from '@angular/core';
import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
export declare class SelectCellComponent implements ICellRendererAngularComp, OnDestroy {
    listData: any;
    formGroup: FormArray;
    key: any;
    private params;
    value: any;
    definition: any;
    private rowId;
    private dataSubscription;
    agInit(params: any): void;
    refresh(params: any): boolean;
    getListData(): void;
    clearSubscription(): void;
    ngOnDestroy(): void;
}
