import { ICellRendererAngularComp } from 'ag-grid-angular';
export declare class DateCellComponent implements ICellRendererAngularComp {
    private params;
    private settings;
    private defaultDateFormat;
    value: any;
    agInit(params: any): void;
    refresh(params: any): boolean;
}
