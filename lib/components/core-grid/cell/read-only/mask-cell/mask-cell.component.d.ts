import { FormArray } from '@angular/forms';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MaskType } from '../../../../../enums/mask-type.enum';
import { MaskSettings } from '../../../../../models/mask-settings.model';
export declare class MaskCellComponent implements ICellRendererAngularComp {
    formGroup: FormArray;
    key: any;
    private params;
    value: any;
    private rowId;
    settings: MaskSettings;
    maskType: MaskType;
    agInit(params: any): void;
    refresh(params: any): boolean;
}
