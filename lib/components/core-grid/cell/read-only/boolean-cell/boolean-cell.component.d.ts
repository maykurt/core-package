import { ICellRendererAngularComp } from 'ag-grid-angular';
import { FormArray } from '@angular/forms';
export declare class BooleanCellComponent implements ICellRendererAngularComp {
    formGroup: FormArray;
    key: any;
    private params;
    value: any;
    private rowId;
    agInit(params: any): void;
    refresh(params: any): boolean;
    onValueChanged(): void;
}
