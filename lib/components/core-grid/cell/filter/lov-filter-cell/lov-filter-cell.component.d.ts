import { OnDestroy } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { IFloatingFilter, IFloatingFilterParams, SerializedTextFilter } from 'ag-grid-community';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListOfValuesComponent } from '../../../../list-of-values/list-of-values.component';
import { TranslateService } from '@ngx-translate/core';
export interface TextFilterChange {
    model: SerializedTextFilter;
}
export interface FloatingFilterParams extends IFloatingFilterParams<SerializedTextFilter, TextFilterChange> {
    value: string;
}
export declare class LovFilterCellComponent implements IFloatingFilter<SerializedTextFilter, TextFilterChange, FloatingFilterParams>, AgFrameworkComponent<FloatingFilterParams>, OnDestroy {
    private fb;
    private translate;
    listOfValue: ListOfValuesComponent;
    key: any;
    currentValue: string;
    prevValue: string;
    listData: any;
    private settings;
    private params;
    dataSource: any;
    selectorFormGroup: FormGroup;
    formGroup: FormGroup;
    filterKey: string;
    hideFilter: boolean;
    private translateSubscription;
    constructor(fb: FormBuilder, translate: TranslateService);
    agInit(params: any): void;
    valueChanged(newValue: any): void;
    onParentModelChanged(parentModel: SerializedTextFilter): void;
    changed(data: any): void;
    buildModel(): SerializedTextFilter;
    initiateComparisonList(): void;
    ngOnDestroy(): void;
}
