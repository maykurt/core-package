import { OnDestroy } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { IFloatingFilter, IFloatingFilterParams, SerializedTextFilter } from 'ag-grid-community';
import { DateSettings } from '../../../../../models/date-settings.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateSelectionMode } from '../../../../../enums/date-selection-mode.enum';
export interface TextFilterChange {
    model: SerializedTextFilter;
}
export interface FloatingFilterParams extends IFloatingFilterParams<SerializedTextFilter, TextFilterChange> {
    value: string;
}
export declare class DateFilterCellComponent implements IFloatingFilter<SerializedTextFilter, TextFilterChange, FloatingFilterParams>, AgFrameworkComponent<FloatingFilterParams>, OnDestroy {
    private fb;
    private translate;
    settings: DateSettings;
    formGroup: FormGroup;
    selectorFormGroup: FormGroup;
    params: any;
    dataSource: any;
    placeholder: string;
    filterKey: string;
    private translateSubscription;
    selectionMode: DateSelectionMode;
    constructor(fb: FormBuilder, translate: TranslateService);
    agInit(params: any): void;
    valueChanged(newValue: any): void;
    onParentModelChanged(parentModel: SerializedTextFilter): void;
    changed(data: any): void;
    buildModel(newValue: string | string[]): SerializedTextFilter;
    initiateComparisonList(): void;
    ngOnDestroy(): void;
}
