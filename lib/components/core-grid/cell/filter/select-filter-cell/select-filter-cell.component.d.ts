import { OnDestroy } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { IFloatingFilter, IFloatingFilterParams, SerializedTextFilter } from 'ag-grid-community';
import { LovSettings } from '../../../../../models/lov-settings.model';
import { SelectorSettings } from '../../../../../models/selector-settings.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
export interface TextFilterChange {
    model: SerializedTextFilter;
}
export interface FloatingFilterParams extends IFloatingFilterParams<SerializedTextFilter, TextFilterChange> {
    value: string;
}
export declare class SelectFilterCellComponent implements IFloatingFilter<SerializedTextFilter, TextFilterChange, FloatingFilterParams>, AgFrameworkComponent<FloatingFilterParams>, OnDestroy {
    private fb;
    private translate;
    currentValue: string;
    prevValue: string;
    listData: any;
    settings: LovSettings | SelectorSettings;
    private params;
    dataSource: any;
    selectorFormGroup: FormGroup;
    formGroup: FormGroup;
    private dataSubscription;
    placeholder: string;
    filterKey: string;
    private translateSubscription;
    constructor(fb: FormBuilder, translate: TranslateService);
    agInit(params: any): void;
    getListData(): void;
    valueChanged(newValue: any): void;
    onParentModelChanged(parentModel: SerializedTextFilter): void;
    changed(data: any): void;
    buildModel(): SerializedTextFilter;
    clearSubscription(): void;
    initiateComparisonList(): void;
    ngOnDestroy(): void;
}
