import { OnDestroy } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { IFloatingFilter, IFloatingFilterParams, SerializedTextFilter } from 'ag-grid-community';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
export interface TextFilterChange {
    model: SerializedTextFilter;
}
export interface FloatingFilterParams extends IFloatingFilterParams<SerializedTextFilter, TextFilterChange> {
    value: string;
}
export declare class BooleanFilterCellComponent implements IFloatingFilter<SerializedTextFilter, TextFilterChange, FloatingFilterParams>, AgFrameworkComponent<FloatingFilterParams>, OnDestroy {
    private fb;
    private translate;
    key: any;
    currentValue: string;
    prevValue: any;
    listData: any[];
    private params;
    dataSource: any;
    formGroup: FormGroup;
    _model: any;
    placeholder: string;
    filterKey: string;
    private translateSubscription;
    constructor(fb: FormBuilder, translate: TranslateService);
    agInit(params: any): void;
    model: any;
    valueChanged(newValue: any): void;
    onParentModelChanged(parentModel: SerializedTextFilter): void;
    changed(data: any): void;
    buildModel(): SerializedTextFilter;
    initiateComparisonList(): void;
    ngOnDestroy(): void;
}
