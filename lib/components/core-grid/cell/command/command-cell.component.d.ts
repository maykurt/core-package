import { ICellRendererAngularComp } from 'ag-grid-angular';
import { FormArray } from '@angular/forms';
export declare class CommandCellComponent implements ICellRendererAngularComp {
    key: any;
    value: any;
    formGroup: FormArray;
    private rowId;
    private params;
    editable: any;
    showRowDeleteButton: boolean;
    showRowEditButton: boolean;
    agInit(params: any): void;
    refresh(): boolean;
    showUndoButton(): boolean;
    removeRow(): void;
    deleteRecord(): void;
    editRecord(): void;
    undoRecord(): void;
}
