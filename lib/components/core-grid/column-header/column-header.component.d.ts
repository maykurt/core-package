import { ElementRef } from '@angular/core';
import { IHeaderParams } from 'ag-grid-community';
import { IHeaderAngularComp } from 'ag-grid-angular/main';
import { SweetAlertService } from '../../../services/custom/sweet.alert.confirm.service';
interface MyParams extends IHeaderParams {
    menuIcon: string;
}
export declare class ColumnHeaderComponent implements IHeaderAngularComp {
    private sweetAlertService;
    params: MyParams;
    sorted: string;
    private elementRef;
    private coreGridService;
    private coldId;
    constructor(elementRef: ElementRef, sweetAlertService: SweetAlertService);
    agInit(params: any): void;
    onMenuClick(): void;
    onSortRequested(order: any, event: any): void;
    onClickHeader(event: any): void;
    setSort(event: any): void;
    onSortChanged(): void;
    isColumnSorted(): boolean;
    private querySelector;
}
export {};
