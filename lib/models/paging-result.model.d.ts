export interface PagingResult {
    TotalCount?: number;
    CurrentPage?: number;
    PageSize?: number;
    TotalPages?: number;
    PreviousePage?: boolean;
    NextPage?: boolean;
}
