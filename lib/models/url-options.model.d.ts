export interface UrlOptions {
    moduleUrl: string;
    endPointUrl: string;
}
