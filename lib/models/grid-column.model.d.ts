import { CoreGridColumnType } from '../enums/core-grid-column-type.enum';
import { GridValidation } from './grid-validation.model';
import { LovSettings } from './lov-settings.model';
import { SelectorSettings } from './selector-settings.model';
import { DateSettings } from './date-settings.model';
import { MaskSettings } from './mask-settings.model';
import { TextSettings } from './text-settings.model';
export interface GridColumn {
    field: string;
    headerName?: string;
    columnType: CoreGridColumnType;
    visible?: boolean;
    dataSource?: any;
    validation?: GridValidation;
    settings?: LovSettings | SelectorSettings | DateSettings | MaskSettings | TextSettings;
    hideFilter?: boolean;
    defaultValue?: any;
    editable?: boolean;
    customFieldForFilter?: string;
    customFieldForSort?: string;
    customEditableFunction?: (rowValue: any) => boolean;
    customDisplayFunction?: (displayValue: any) => any;
    customFilterFunction?: (filterValue: any) => any;
    customFunctionForRowValueChanges?: (context: any) => void;
    minWidth?: number;
    maxWidth?: number;
    getDataSourceAfterDataLoaded?: boolean;
}
