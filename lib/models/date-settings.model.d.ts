import { DateSelectionMode } from '../enums/date-selection-mode.enum';
export declare class DateSettings {
    disableUTC?: boolean;
    showTimeOnly?: boolean;
    defaultYear?: number;
    defaultMonth?: number;
    defaultDay?: number;
    dateFormat?: string;
    showYear?: boolean;
    showTime?: boolean;
    hideCalendarButton?: boolean;
    showButtonBar?: boolean;
    showMonthPicker?: boolean;
    minDate?: string;
    maxDate?: string;
    enableSeconds?: boolean;
    selectionMode?: DateSelectionMode;
}
