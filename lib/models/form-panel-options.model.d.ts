export interface FormPanelOptions {
    Display: boolean;
    IsReadOnly: boolean;
}
