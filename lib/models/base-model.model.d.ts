import { OperationType } from '../enums/operation-type.enum';
export declare class BaseModel {
    Id: number;
    FirmId?: number;
    InsertedBy?: number;
    InsertedDate?: Date;
    UpdatedBy?: number;
    UpdatedDate?: Date;
    OperationType?: OperationType;
}
