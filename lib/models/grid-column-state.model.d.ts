export interface GridColumnState {
    field: string;
    visible: boolean;
    order: number;
    width?: number;
}
