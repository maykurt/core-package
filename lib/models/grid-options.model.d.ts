import { CoreGridSelectionType } from '../enums/core-grid-selection-type.enum';
import { ButtonSettings } from './button-settings.model';
import { GridColumn } from './grid-column.model';
import { RequestOptions } from './request-options.model';
export interface GridOptions {
    keyField: string;
    editable: boolean;
    enableSorting: boolean;
    enableFilter: boolean;
    columns: GridColumn[];
    rowSelection: CoreGridSelectionType;
    deleteEndPointUrl?: string;
    title: string;
    requestOptions: RequestOptions;
    buttonSettings?: ButtonSettings;
    addUrl?: string;
    editUrl?: string;
    disableSizeToFit?: boolean;
    editUrlBlank?: boolean;
    disableSelfLoading?: boolean;
    enableCheckboxSelection?: boolean;
    breadcrumbLabel?: string[];
}
