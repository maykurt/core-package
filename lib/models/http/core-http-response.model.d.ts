import { PagingResult } from '../paging-result.model';
import { ServiceResult } from './service-result.model';
export interface CoreHttpResponse<T> {
    pagingResult?: PagingResult;
    serviceResult?: ServiceResult<T>;
}
