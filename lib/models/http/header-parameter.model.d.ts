import { Headers } from '../../enums/headers.enum';
export interface HeaderParameter {
    header: Headers;
    value: any;
}
