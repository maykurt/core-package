export declare class ServiceResult<T> {
    ErrorMessage: string | string[];
    HttpStatusCode: number;
    IsSuccess: boolean;
    Result: T;
}
