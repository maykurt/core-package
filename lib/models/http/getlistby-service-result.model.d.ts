export declare class GetListByResult {
    CurrentPage: number;
    NextPage: boolean;
    PageSize: number;
    PreviousPage: boolean;
    TotalCount: number;
    TotalPages: number;
    Data: any;
}
