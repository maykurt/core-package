import { FilterGroup } from './filter-group.model';
import { Sort } from './sort.model';
export interface GenericExpression {
    PageSize?: number;
    PageNumber?: number;
    FilterGroups?: FilterGroup[];
    Sort?: Sort[];
}
