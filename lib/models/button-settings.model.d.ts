export interface ButtonSettings {
    showAddButton?: boolean;
    showEditButton?: boolean;
    showReloadButton?: boolean;
    showRowDeleteButton?: boolean;
}
