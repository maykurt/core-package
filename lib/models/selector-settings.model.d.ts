import { RequestOptions } from './request-options.model';
export interface SelectorSettings {
    requestOptions: RequestOptions;
    valueField: string;
    labelField: string | string[];
    filterByField?: string;
    seperator?: string;
    sameFieldWith?: string;
}
