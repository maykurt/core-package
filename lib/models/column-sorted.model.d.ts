import { SortType } from '../enums/sort-type.enum';
export interface ColumnSortedEvent {
    SortColumn: string;
    SortDirection: SortType;
}
