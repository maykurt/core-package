export interface IAppConfig {
    env: {
        apiUrl: string;
        version: string;
        companyLogoUrl: string;
        appTitle: string;
        erpUrl: string;
    };
    appInsights?: {
        instrumentationKey: string;
    };
    logging?: {
        console: boolean;
        appInsights: boolean;
    };
    aad?: {
        requireAuth: boolean;
        tenant: string;
        clientId: string;
    };
    apiServer?: {
        metadata: string;
        rules: string;
    };
}
