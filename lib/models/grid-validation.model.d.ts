export interface GridValidation {
    required?: boolean;
    email?: boolean;
    requiredTrue?: boolean;
    minValue?: number;
    maxValue?: number;
    minLength?: number;
    maxLength?: number;
    regex?: string;
}
