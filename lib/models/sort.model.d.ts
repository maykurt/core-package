import { SortType } from '../enums/sort-type.enum';
export interface Sort {
    PropertyName: string;
    Type: SortType;
}
