import { GridOptions } from './grid-options.model';
export interface LovSettings {
    options: GridOptions;
    valueField: string;
    labelField: string | string[];
    dontInitData?: boolean;
}
