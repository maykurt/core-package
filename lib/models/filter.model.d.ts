import { TextComparison, DateComparison, NumberComparison, BooleanComparison, DefaultComparison, Comparison } from '../enums/comparison-type.enum';
import { Connector } from '../enums/connector.enum';
export declare class Filter {
    PropertyName: string;
    Value: any;
    OtherValue?: any;
    Comparison: TextComparison | DateComparison | NumberComparison | BooleanComparison | DefaultComparison | Comparison;
    Connector?: Connector;
}
