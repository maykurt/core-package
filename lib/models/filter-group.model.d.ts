import { Connector } from '../enums/connector.enum';
import { Filter } from './filter.model';
export declare class FilterGroup {
    Filters: Filter[];
    Connector?: Connector;
}
