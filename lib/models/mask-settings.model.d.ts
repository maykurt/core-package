import { MaskType } from '../enums/mask-type.enum';
export interface MaskSettings {
    maskType: MaskType;
    maxLength?: number;
    allowedChars?: string[];
}
