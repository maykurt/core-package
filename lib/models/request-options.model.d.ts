import { UrlOptions } from './url-options.model';
import { GenericExpression } from './generic-expression.model';
import { HeaderParameter } from './http/header-parameter.model';
export interface RequestOptions {
    UrlOptions: UrlOptions;
    CustomFilter?: GenericExpression;
    CustomEndPoint?: boolean;
    HeaderParameters?: HeaderParameter[];
}
