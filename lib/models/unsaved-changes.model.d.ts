import { Observable } from 'rxjs';
export interface UnsavedChanges {
    hasUnsavedChanges: () => Observable<boolean> | Promise<boolean> | boolean;
}
