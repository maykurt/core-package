import { AlertType } from '../enums/alert-type.enum';
export declare class ConfirmDialogSettings {
    Title?: string;
    Text?: string;
    ConfirmBtnText?: string;
    CancelBtnText?: string;
    Type?: AlertType;
}
