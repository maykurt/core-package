import { LovColumnType } from '../enums/lov-column-type.enum';
export declare class LovColumn {
    /** visible parameter default is true */
    constructor(name: string, headerName: string, type: LovColumnType, visible?: boolean, isKeyField?: boolean);
    Name: string;
    HeaderName: string;
    Type: LovColumnType;
    Visible: boolean;
    IsKeyField: boolean;
    SearchText: string;
}
