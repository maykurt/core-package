import { AlertDismissReason } from '../enums/alert-dismiss-reason.enum';
export interface AlertResult {
    value?: any;
    dismiss?: AlertDismissReason;
}
