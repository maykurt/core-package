import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BaseHttpService } from './base-http.service';
import { HeaderParameter, CoreHttpResponse, GenericExpression } from '../../models/index';
export declare class DataService extends BaseHttpService {
    protected httpClient: HttpClient;
    constructor(httpClient: HttpClient);
    create<T>(resource: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    update<T>(resource: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    delete<T>(Id: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    getList<T>(endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    getListBy<T>(endPointUrl: string, filterQuery: GenericExpression, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    get<T>(Id: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    getBy<T>(endPointUrl: string, filterQuery: GenericExpression, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    private wrapHttpResponse;
}
