import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HeaderParameter, GenericExpression, CoreHttpResponse } from '../../models/index';
import { BaseHttpService } from './base-http.service';
export declare class BaseService extends BaseHttpService {
    protected httpClient: HttpClient;
    endpoint: string;
    moduleUrl: string;
    constructor(httpClient: HttpClient);
    create<T>(resource: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    update<T>(resource: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    delete<T>(endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    deleteById<T>(id: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    getList<T>(endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<T>>;
    getListBy<T>(filterQuery: GenericExpression, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    get<T>(endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    getById<T>(id: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    getBy<T>(filterQuery: GenericExpression, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    bulkOperation<T>(resource: any, endPointUrl?: string, headerParameters?: HeaderParameter[]): Observable<CoreHttpResponse<{}>>;
    private callHttpGet;
    private isFileOPeration;
    private wrapHttpResponse;
}
