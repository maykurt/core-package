import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericExpression } from '../../models/generic-expression.model';
import { HeaderParameter } from '../../models/http/header-parameter.model';
export declare class BaseHttpService {
    protected httpClient: HttpClient;
    constructor(httpClient: HttpClient);
    httpGet<T>(requestUrl?: string, headers?: HttpHeaders): import("rxjs").Observable<import("@angular/common/http").HttpResponse<T>>;
    httpGetFile<T>(requestUrl?: string, headers?: HttpHeaders): import("rxjs").Observable<import("@angular/common/http").HttpResponse<Blob>>;
    httpPost<T>(requestUrl: string, data: any, headers?: HttpHeaders): import("rxjs").Observable<T>;
    httpPatch<T>(requestUrl: string, data: any, headers?: HttpHeaders): import("rxjs").Observable<T>;
    httpDelete<T>(requestUrl?: string, headers?: HttpHeaders): import("rxjs").Observable<T>;
    protected getHttpHeader(filterQuery?: GenericExpression, headerParameters?: HeaderParameter[]): HttpHeaders;
}
