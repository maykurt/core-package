import { CanDeactivate } from '@angular/router';
import { UnsavedChanges } from '../../models/unsaved-changes.model';
import { SweetAlertService } from '../custom/sweet.alert.confirm.service';
import { PreventUnsavedChangesService } from '../custom/prevent-unsaved-changes.service';
export declare class PreventUnsavedChangesGuard implements CanDeactivate<UnsavedChanges> {
    private sweetAlert;
    private preventUnsavedChangesService;
    constructor(sweetAlert: SweetAlertService, preventUnsavedChangesService: PreventUnsavedChangesService);
    canDeactivate(): any;
}
