import { OnDestroy } from '@angular/core';
import 'rxjs/add/operator/share';
import { StorageType } from '../../enums/storage-type.enum';
export declare class StorageService implements OnDestroy {
    private onSubject;
    changes: import("rxjs").Observable<{
        key: string;
        value: any;
    }>;
    constructor();
    ngOnDestroy(): void;
    getStorage(storageType?: StorageType): any[];
    store(key: string, data: any, storageType?: StorageType): void;
    getStored(key: string, storageType?: StorageType): any;
    clear(key: any, storageType?: StorageType): void;
    private start;
    private storageEventListener;
    private stop;
}
