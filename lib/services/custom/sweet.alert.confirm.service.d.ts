import { AlertResult } from '../../models/alert-result.model';
import { ConfirmDialogOperationType, AlertType } from '../../enums/index';
import { ConfirmDialogSettings } from '../../models/confirm-dialog-settings.model';
import { TranslateHelperService } from './translate.helper.service';
export declare class SweetAlertService {
    private translate;
    private _swal;
    private swalOptions;
    constructor(translate: TranslateHelperService);
    preventUnsavedChangedPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton?: boolean): import("rxjs").Observable<AlertResult>;
    manageRequestOperationWithConfirm(confirmDialogOperationType: ConfirmDialogOperationType, confirmDialogSettings?: ConfirmDialogSettings): import("rxjs").Observable<AlertResult>;
    private getProcessOperationConfirmPopupWithConfirm;
    private getAddOperationConfirmPopupWithConfirm;
    private getUpdateOperationConfirmPopupWithConfirm;
    private getDeleteOperationConfirmPopupWithConfirm;
    private getUndoOperationConfirmPopupWithConfirm;
    private getCancelOperationConfirmPopupWithConfirm;
    private getRefreshOperationConfirmPopupWithConfirm;
    confirmOperationPopup(result: any, settings?: any): import("rxjs").Observable<AlertResult>;
    private openCompletedConfirmDialog;
    private openConfirmDialog;
}
