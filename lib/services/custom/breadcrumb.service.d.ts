export declare class BreadcrumbService {
    breadcrumbs: any[];
    breadcrumbsWithCustomLabels: any[];
    constructor();
    addCustomLabel(label: string, url: string, customLabel: string): void;
}
