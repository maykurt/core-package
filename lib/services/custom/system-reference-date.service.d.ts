import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { StorageService } from './storage.service';
export declare class SystemReferenceDateService {
    private storageService;
    dateChange: BehaviorSubject<string>;
    systemDate: string;
    constructor(storageService: StorageService);
    setSelectedReferenceDate(newDate: string): void;
    checkIsNullAndChangeCurrentDateWithStorage(): boolean;
    getSelectedReferenceDate(): Observable<any>;
    getSystemDate(): string;
    getCurrentDateAsISO(): string;
    convertDateToISO(date: Date): string;
    convertDateToDateUTC(dateAsString: string): Date;
}
