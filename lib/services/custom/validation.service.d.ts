import { ValidationErrors, AbstractControl } from '@angular/forms';
export declare class ValidationService {
    static tcValidate(control: AbstractControl): ValidationErrors | null;
    static spaceControl(control: AbstractControl): ValidationErrors | null;
}
