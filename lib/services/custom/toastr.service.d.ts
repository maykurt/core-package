import { ToastrService } from 'ngx-toastr';
import { TranslateHelperService } from './translate.helper.service';
export declare class ToastrUtilsService {
    private toastr;
    private translate;
    constructor(toastr: ToastrService, translate: TranslateHelperService);
    success(message: string, title?: string): void;
    info(message: string, title?: string): void;
    warning(message: string, title?: string): void;
    error(message: string, title?: string): void;
    translateValue(message: string, messageType: string, title?: string): void;
}
