import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './storage.service';
export declare class TranslateHelperService {
    private translate;
    private storageService;
    constructor(translate: TranslateService, storageService: StorageService);
    setInitialLanguage(lang: any): void;
    setSelectedLanguage(language: string): void;
    instant(key: string): Promise<string>;
    get(key: string | string[]): import("rxjs").Observable<any>;
}
