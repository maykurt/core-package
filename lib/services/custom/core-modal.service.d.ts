import { Observable } from 'rxjs/Observable';
export declare class CoreModalService {
    private isModalDisplayed;
    constructor();
    getModalDisplayStatus(): Observable<boolean>;
    setModalDisplayStatus(status: boolean): void;
}
