import { Observable } from 'rxjs';
export declare class PreventUnsavedChangesService {
    private isChanged;
    private status;
    constructor();
    getIsChanged(): boolean;
    setIsChanged(isChanged: boolean): void;
    getStatus(): Observable<string>;
    setStatus(status: string): void;
}
