import { Injector } from '@angular/core';
import { IAppConfig } from '../../models/index';
export declare class AppConfig {
    private injector;
    static settings: IAppConfig;
    constructor(injector: Injector);
    load(): Promise<void>;
}
