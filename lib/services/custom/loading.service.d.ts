import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
export declare class LoadingService {
    loadingBarDisplayStatus: BehaviorSubject<boolean>;
    private loadingRequests;
    constructor();
    getLoadingBarDisplayStatus(): Observable<boolean>;
    insertLoadingRequest(request: string): void;
    removeLoadingRequest(request: string): void;
    setLoadingBarSetting(status: any): void;
    checkLoadingDisplayStatus(): void;
}
