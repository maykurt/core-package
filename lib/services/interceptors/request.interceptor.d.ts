import { Injector } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpUserEvent, HttpHeaderResponse, HttpSentEvent, HttpProgressEvent } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { LoadingService, ToastrUtilsService, StorageService } from '../custom/index';
import { Router } from '@angular/router';
export declare class RequestInterceptor implements HttpInterceptor {
    private injector;
    private loadingService;
    private storageService;
    private router;
    private toastr;
    private authenticationService;
    private breadcrumbService;
    isRefreshingToken: boolean;
    tokenSubject: BehaviorSubject<string>;
    constructor(injector: Injector, loadingService: LoadingService, storageService: StorageService, router: Router, toastr: ToastrUtilsService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any>;
    private handdleResponseForBreadcrumb;
    private addTokenToRequest;
    handle400Error(error: any, disableNavigation?: boolean): Observable<never>;
    private handle401Error;
    private showError;
}
