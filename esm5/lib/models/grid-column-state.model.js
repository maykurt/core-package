/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// export interface GridColumnStates {
//     field: string;
//     visible: boolean;
// }
/**
 * @record
 */
export function GridColumnState() { }
if (false) {
    /** @type {?} */
    GridColumnState.prototype.field;
    /** @type {?} */
    GridColumnState.prototype.visible;
    /** @type {?} */
    GridColumnState.prototype.order;
    /** @type {?|undefined} */
    GridColumnState.prototype.width;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1jb2x1bW4tc3RhdGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2dyaWQtY29sdW1uLXN0YXRlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBS0EscUNBS0M7OztJQUpHLGdDQUFjOztJQUNkLGtDQUFpQjs7SUFDakIsZ0NBQWM7O0lBQ2QsZ0NBQWUiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHBvcnQgaW50ZXJmYWNlIEdyaWRDb2x1bW5TdGF0ZXMge1xyXG4vLyAgICAgZmllbGQ6IHN0cmluZztcclxuLy8gICAgIHZpc2libGU6IGJvb2xlYW47XHJcbi8vIH1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgR3JpZENvbHVtblN0YXRlIHtcclxuICAgIGZpZWxkOiBzdHJpbmc7XHJcbiAgICB2aXNpYmxlOiBib29sZWFuO1xyXG4gICAgb3JkZXI6IG51bWJlcjtcclxuICAgIHdpZHRoPzogbnVtYmVyO1xyXG59XHJcbiJdfQ==