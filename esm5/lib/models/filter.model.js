/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Filter = /** @class */ (function () {
    function Filter() {
    }
    return Filter;
}());
export { Filter };
if (false) {
    /** @type {?} */
    Filter.prototype.PropertyName;
    /** @type {?} */
    Filter.prototype.Value;
    /** @type {?} */
    Filter.prototype.OtherValue;
    /** @type {?} */
    Filter.prototype.Comparison;
    /** @type {?} */
    Filter.prototype.Connector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9maWx0ZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQVVBO0lBQUE7SUFNQSxDQUFDO0lBQUQsYUFBQztBQUFELENBQUMsQUFORCxJQU1DOzs7O0lBTEMsOEJBQXFCOztJQUNyQix1QkFBVzs7SUFDWCw0QkFBaUI7O0lBQ2pCLDRCQUFvSDs7SUFDcEgsMkJBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBUZXh0Q29tcGFyaXNvbixcclxuICBEYXRlQ29tcGFyaXNvbixcclxuICBOdW1iZXJDb21wYXJpc29uLFxyXG4gIEJvb2xlYW5Db21wYXJpc29uLFxyXG4gIERlZmF1bHRDb21wYXJpc29uLFxyXG4gIENvbXBhcmlzb25cclxufSBmcm9tICcuLi9lbnVtcy9jb21wYXJpc29uLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IENvbm5lY3RvciB9IGZyb20gJy4uL2VudW1zL2Nvbm5lY3Rvci5lbnVtJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXIge1xyXG4gIFByb3BlcnR5TmFtZTogc3RyaW5nO1xyXG4gIFZhbHVlOiBhbnk7XHJcbiAgT3RoZXJWYWx1ZT86IGFueTtcclxuICBDb21wYXJpc29uOiBUZXh0Q29tcGFyaXNvbiB8IERhdGVDb21wYXJpc29uIHwgTnVtYmVyQ29tcGFyaXNvbiB8IEJvb2xlYW5Db21wYXJpc29uIHwgRGVmYXVsdENvbXBhcmlzb24gfCBDb21wYXJpc29uO1xyXG4gIENvbm5lY3Rvcj86IENvbm5lY3RvcjtcclxufVxyXG4iXX0=