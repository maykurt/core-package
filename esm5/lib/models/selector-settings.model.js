/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function SelectorSettings() { }
if (false) {
    /** @type {?} */
    SelectorSettings.prototype.requestOptions;
    /** @type {?} */
    SelectorSettings.prototype.valueField;
    /** @type {?} */
    SelectorSettings.prototype.labelField;
    /** @type {?|undefined} */
    SelectorSettings.prototype.filterByField;
    /** @type {?|undefined} */
    SelectorSettings.prototype.seperator;
    /** @type {?|undefined} */
    SelectorSettings.prototype.sameFieldWith;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3Itc2V0dGluZ3MubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL3NlbGVjdG9yLXNldHRpbmdzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFFQSxzQ0FPQzs7O0lBTkMsMENBQStCOztJQUMvQixzQ0FBbUI7O0lBQ25CLHNDQUE4Qjs7SUFDOUIseUNBQXVCOztJQUN2QixxQ0FBbUI7O0lBQ25CLHlDQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlcXVlc3RPcHRpb25zIH0gZnJvbSAnLi9yZXF1ZXN0LW9wdGlvbnMubW9kZWwnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWxlY3RvclNldHRpbmdzIHtcclxuICByZXF1ZXN0T3B0aW9uczogUmVxdWVzdE9wdGlvbnM7XHJcbiAgdmFsdWVGaWVsZDogc3RyaW5nO1xyXG4gIGxhYmVsRmllbGQ6IHN0cmluZyB8IHN0cmluZ1tdO1xyXG4gIGZpbHRlckJ5RmllbGQ/OiBzdHJpbmc7XHJcbiAgc2VwZXJhdG9yPzogc3RyaW5nO1xyXG4gIHNhbWVGaWVsZFdpdGg/OiBzdHJpbmc7IC8vIHVzZWQgZm9yIG9ubHkgcmVhZC1vbmx5IGdyaWQgYW5kIHNlbGVjdCwgZm9yIG11bHRwbHlpbmcgc2FtZSBjb2x1bW5cclxufVxyXG4iXX0=