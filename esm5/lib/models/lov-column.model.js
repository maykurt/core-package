/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
var LovColumn = /** @class */ (function () {
    /** visible parameter default is true */
    function LovColumn(name, headerName, type, visible, isKeyField) {
        this.Name = name;
        this.HeaderName = headerName;
        this.Type = type;
        this.Visible = _.isNil(visible) ? true : visible;
        this.IsKeyField = _.isNil(isKeyField) ? false : isKeyField;
    }
    return LovColumn;
}());
export { LovColumn };
if (false) {
    /** @type {?} */
    LovColumn.prototype.Name;
    /** @type {?} */
    LovColumn.prototype.HeaderName;
    /** @type {?} */
    LovColumn.prototype.Type;
    /** @type {?} */
    LovColumn.prototype.Visible;
    /** @type {?} */
    LovColumn.prototype.IsKeyField;
    /** @type {?} */
    LovColumn.prototype.SearchText;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWNvbHVtbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbG92LWNvbHVtbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFHNUI7SUFFRSx3Q0FBd0M7SUFDeEMsbUJBQVksSUFBWSxFQUFFLFVBQWtCLEVBQUUsSUFBbUIsRUFBRSxPQUFpQixFQUFFLFVBQW9CO1FBQ3hHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUM3RCxDQUFDO0lBUUgsZ0JBQUM7QUFBRCxDQUFDLEFBakJELElBaUJDOzs7O0lBTkMseUJBQW9COztJQUNwQiwrQkFBMEI7O0lBQzFCLHlCQUEyQjs7SUFDM0IsNEJBQXdCOztJQUN4QiwrQkFBMkI7O0lBQzNCLCtCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IExvdkNvbHVtblR5cGUgfSBmcm9tICcuLi9lbnVtcy9sb3YtY29sdW1uLXR5cGUuZW51bSc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTG92Q29sdW1uIHtcclxuXHJcbiAgLyoqIHZpc2libGUgcGFyYW1ldGVyIGRlZmF1bHQgaXMgdHJ1ZSAqL1xyXG4gIGNvbnN0cnVjdG9yKG5hbWU6IHN0cmluZywgaGVhZGVyTmFtZTogc3RyaW5nLCB0eXBlOiBMb3ZDb2x1bW5UeXBlLCB2aXNpYmxlPzogYm9vbGVhbiwgaXNLZXlGaWVsZD86IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuTmFtZSA9IG5hbWU7XHJcbiAgICB0aGlzLkhlYWRlck5hbWUgPSBoZWFkZXJOYW1lO1xyXG4gICAgdGhpcy5UeXBlID0gdHlwZTtcclxuICAgIHRoaXMuVmlzaWJsZSA9IF8uaXNOaWwodmlzaWJsZSkgPyB0cnVlIDogdmlzaWJsZTtcclxuICAgIHRoaXMuSXNLZXlGaWVsZCA9IF8uaXNOaWwoaXNLZXlGaWVsZCkgPyBmYWxzZSA6IGlzS2V5RmllbGQ7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgTmFtZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBIZWFkZXJOYW1lOiBzdHJpbmc7XHJcbiAgcHVibGljIFR5cGU6IExvdkNvbHVtblR5cGU7XHJcbiAgcHVibGljIFZpc2libGU6IGJvb2xlYW47XHJcbiAgcHVibGljIElzS2V5RmllbGQ6IGJvb2xlYW47XHJcbiAgcHVibGljIFNlYXJjaFRleHQ6IHN0cmluZztcclxufVxyXG4iXX0=