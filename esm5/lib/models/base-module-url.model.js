/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BaseModuleUrl = /** @class */ (function () {
    function BaseModuleUrl(url) {
        this.url = url;
        this.moduleUrl = url;
    }
    return BaseModuleUrl;
}());
export { BaseModuleUrl };
if (false) {
    /** @type {?} */
    BaseModuleUrl.prototype.moduleUrl;
    /**
     * @type {?}
     * @private
     */
    BaseModuleUrl.prototype.url;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1tb2R1bGUtdXJsLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9iYXNlLW1vZHVsZS11cmwubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBO0lBR0UsdUJBQW9CLEdBQVc7UUFBWCxRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO0lBQ3ZCLENBQUM7SUFDSCxvQkFBQztBQUFELENBQUMsQUFORCxJQU1DOzs7O0lBTEMsa0NBQXlCOzs7OztJQUViLDRCQUFtQiIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5leHBvcnQgY2xhc3MgQmFzZU1vZHVsZVVybCB7XHJcbiAgcHVibGljIG1vZHVsZVVybDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHVybDogc3RyaW5nKSB7XHJcbiAgICB0aGlzLm1vZHVsZVVybCA9IHVybDtcclxuICB9XHJcbn1cclxuIl19