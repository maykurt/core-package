/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function MaskSettings() { }
if (false) {
    /** @type {?} */
    MaskSettings.prototype.maskType;
    /** @type {?|undefined} */
    MaskSettings.prototype.maxLength;
    /** @type {?|undefined} */
    MaskSettings.prototype.allowedChars;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1zZXR0aW5ncy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbWFzay1zZXR0aW5ncy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUEsa0NBSUM7OztJQUhDLGdDQUFtQjs7SUFDbkIsaUNBQW1COztJQUNuQixvQ0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNYXNrVHlwZSB9IGZyb20gJy4uL2VudW1zL21hc2stdHlwZS5lbnVtJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTWFza1NldHRpbmdzIHtcclxuICBtYXNrVHlwZTogTWFza1R5cGU7XHJcbiAgbWF4TGVuZ3RoPzogbnVtYmVyO1xyXG4gIGFsbG93ZWRDaGFycz86IHN0cmluZ1tdO1xyXG59XHJcbiJdfQ==