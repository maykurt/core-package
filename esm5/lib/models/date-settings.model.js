/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { DateSelectionMode } from '../enums/date-selection-mode.enum';
var DateSettings = /** @class */ (function () {
    function DateSettings() {
        this.disableUTC = false;
        this.showTimeOnly = false;
        // current year
        this.defaultYear = (new Date()).getFullYear();
        // 0 = january
        this.defaultMonth = 0;
        // 1
        this.defaultDay = 1;
        //'dd.mm.yy'
        this.dateFormat = 'dd.mm.yy';
        this.showYear = false;
        this.showTime = false;
        this.hideCalendarButton = false;
        //  today and clear buttons
        this.showButtonBar = true;
        this.showMonthPicker = false;
        this.enableSeconds = false;
        this.selectionMode = DateSelectionMode.Single;
    }
    return DateSettings;
}());
export { DateSettings };
if (false) {
    /** @type {?} */
    DateSettings.prototype.disableUTC;
    /** @type {?} */
    DateSettings.prototype.showTimeOnly;
    /** @type {?} */
    DateSettings.prototype.defaultYear;
    /** @type {?} */
    DateSettings.prototype.defaultMonth;
    /** @type {?} */
    DateSettings.prototype.defaultDay;
    /** @type {?} */
    DateSettings.prototype.dateFormat;
    /** @type {?} */
    DateSettings.prototype.showYear;
    /** @type {?} */
    DateSettings.prototype.showTime;
    /** @type {?} */
    DateSettings.prototype.hideCalendarButton;
    /** @type {?} */
    DateSettings.prototype.showButtonBar;
    /** @type {?} */
    DateSettings.prototype.showMonthPicker;
    /** @type {?} */
    DateSettings.prototype.minDate;
    /** @type {?} */
    DateSettings.prototype.maxDate;
    /** @type {?} */
    DateSettings.prototype.enableSeconds;
    /** @type {?} */
    DateSettings.prototype.selectionMode;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1zZXR0aW5ncy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZGF0ZS1zZXR0aW5ncy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFFdEU7SUFBQTtRQUNFLGVBQVUsR0FBYSxLQUFLLENBQUM7UUFDN0IsaUJBQVksR0FBYSxLQUFLLENBQUM7O1FBRS9CLGdCQUFXLEdBQVksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7O1FBRWxELGlCQUFZLEdBQVksQ0FBQyxDQUFDOztRQUUxQixlQUFVLEdBQVksQ0FBQyxDQUFDOztRQUV4QixlQUFVLEdBQVksVUFBVSxDQUFDO1FBQ2pDLGFBQVEsR0FBYSxLQUFLLENBQUM7UUFDM0IsYUFBUSxHQUFhLEtBQUssQ0FBQztRQUMzQix1QkFBa0IsR0FBYSxLQUFLLENBQUM7O1FBRXJDLGtCQUFhLEdBQWEsSUFBSSxDQUFDO1FBQy9CLG9CQUFlLEdBQWEsS0FBSyxDQUFDO1FBR2xDLGtCQUFhLEdBQWEsS0FBSyxDQUFDO1FBQ2hDLGtCQUFhLEdBQXVCLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztJQUMvRCxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBckJELElBcUJDOzs7O0lBcEJDLGtDQUE2Qjs7SUFDN0Isb0NBQStCOztJQUUvQixtQ0FBa0Q7O0lBRWxELG9DQUEwQjs7SUFFMUIsa0NBQXdCOztJQUV4QixrQ0FBaUM7O0lBQ2pDLGdDQUEyQjs7SUFDM0IsZ0NBQTJCOztJQUMzQiwwQ0FBcUM7O0lBRXJDLHFDQUErQjs7SUFDL0IsdUNBQWtDOztJQUNsQywrQkFBaUI7O0lBQ2pCLCtCQUFpQjs7SUFDakIscUNBQWdDOztJQUNoQyxxQ0FBNkQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEYXRlU2VsZWN0aW9uTW9kZSB9IGZyb20gJy4uL2VudW1zL2RhdGUtc2VsZWN0aW9uLW1vZGUuZW51bSc7XHJcblxyXG5leHBvcnQgY2xhc3MgRGF0ZVNldHRpbmdzIHtcclxuICBkaXNhYmxlVVRDPzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dUaW1lT25seT86IGJvb2xlYW4gPSBmYWxzZTtcclxuICAvLyBjdXJyZW50IHllYXJcclxuICBkZWZhdWx0WWVhcj86IG51bWJlciA9IChuZXcgRGF0ZSgpKS5nZXRGdWxsWWVhcigpO1xyXG4gIC8vIDAgPSBqYW51YXJ5XHJcbiAgZGVmYXVsdE1vbnRoPzogbnVtYmVyID0gMDtcclxuICAvLyAxXHJcbiAgZGVmYXVsdERheT86IG51bWJlciA9IDE7XHJcbiAgLy8nZGQubW0ueXknXHJcbiAgZGF0ZUZvcm1hdD86IHN0cmluZyA9ICdkZC5tbS55eSc7XHJcbiAgc2hvd1llYXI/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2hvd1RpbWU/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgaGlkZUNhbGVuZGFyQnV0dG9uPzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIC8vICB0b2RheSBhbmQgY2xlYXIgYnV0dG9uc1xyXG4gIHNob3dCdXR0b25CYXI/OiBib29sZWFuID0gdHJ1ZTtcclxuICBzaG93TW9udGhQaWNrZXI/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgbWluRGF0ZT86IHN0cmluZztcclxuICBtYXhEYXRlPzogc3RyaW5nO1xyXG4gIGVuYWJsZVNlY29uZHM/OiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2VsZWN0aW9uTW9kZT86IERhdGVTZWxlY3Rpb25Nb2RlID0gRGF0ZVNlbGVjdGlvbk1vZGUuU2luZ2xlO1xyXG59XHJcbiJdfQ==