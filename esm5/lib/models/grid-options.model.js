/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GridOptions() { }
if (false) {
    /** @type {?} */
    GridOptions.prototype.keyField;
    /** @type {?} */
    GridOptions.prototype.editable;
    /** @type {?} */
    GridOptions.prototype.enableSorting;
    /** @type {?} */
    GridOptions.prototype.enableFilter;
    /** @type {?} */
    GridOptions.prototype.columns;
    /** @type {?} */
    GridOptions.prototype.rowSelection;
    /** @type {?|undefined} */
    GridOptions.prototype.deleteEndPointUrl;
    /** @type {?} */
    GridOptions.prototype.title;
    /** @type {?} */
    GridOptions.prototype.requestOptions;
    /** @type {?|undefined} */
    GridOptions.prototype.buttonSettings;
    /** @type {?|undefined} */
    GridOptions.prototype.addUrl;
    /** @type {?|undefined} */
    GridOptions.prototype.editUrl;
    /** @type {?|undefined} */
    GridOptions.prototype.disableSizeToFit;
    /** @type {?|undefined} */
    GridOptions.prototype.editUrlBlank;
    /** @type {?|undefined} */
    GridOptions.prototype.disableSelfLoading;
    /** @type {?|undefined} */
    GridOptions.prototype.enableCheckboxSelection;
    /** @type {?|undefined} */
    GridOptions.prototype.breadcrumbLabel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1vcHRpb25zLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9ncmlkLW9wdGlvbnMubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUtBLGlDQWtCQzs7O0lBakJDLCtCQUFpQjs7SUFDakIsK0JBQWtCOztJQUNsQixvQ0FBdUI7O0lBQ3ZCLG1DQUFzQjs7SUFDdEIsOEJBQXNCOztJQUN0QixtQ0FBb0M7O0lBQ3BDLHdDQUEyQjs7SUFDM0IsNEJBQWM7O0lBQ2QscUNBQStCOztJQUMvQixxQ0FBZ0M7O0lBQ2hDLDZCQUFnQjs7SUFDaEIsOEJBQWlCOztJQUNqQix1Q0FBMkI7O0lBQzNCLG1DQUF1Qjs7SUFDdkIseUNBQTZCOztJQUM3Qiw4Q0FBa0M7O0lBQ2xDLHNDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvcmVHcmlkU2VsZWN0aW9uVHlwZSB9IGZyb20gJy4uL2VudW1zL2NvcmUtZ3JpZC1zZWxlY3Rpb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgQnV0dG9uU2V0dGluZ3MgfSBmcm9tICcuL2J1dHRvbi1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IEdyaWRDb2x1bW4gfSBmcm9tICcuL2dyaWQtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgUmVxdWVzdE9wdGlvbnMgfSBmcm9tICcuL3JlcXVlc3Qtb3B0aW9ucy5tb2RlbCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEdyaWRPcHRpb25zIHtcclxuICBrZXlGaWVsZDogc3RyaW5nO1xyXG4gIGVkaXRhYmxlOiBib29sZWFuO1xyXG4gIGVuYWJsZVNvcnRpbmc6IGJvb2xlYW47XHJcbiAgZW5hYmxlRmlsdGVyOiBib29sZWFuO1xyXG4gIGNvbHVtbnM6IEdyaWRDb2x1bW5bXTtcclxuICByb3dTZWxlY3Rpb246IENvcmVHcmlkU2VsZWN0aW9uVHlwZTtcclxuICBkZWxldGVFbmRQb2ludFVybD86IHN0cmluZztcclxuICB0aXRsZTogc3RyaW5nO1xyXG4gIHJlcXVlc3RPcHRpb25zOiBSZXF1ZXN0T3B0aW9ucztcclxuICBidXR0b25TZXR0aW5ncz86IEJ1dHRvblNldHRpbmdzO1xyXG4gIGFkZFVybD86IHN0cmluZztcclxuICBlZGl0VXJsPzogc3RyaW5nO1xyXG4gIGRpc2FibGVTaXplVG9GaXQ/OiBib29sZWFuO1xyXG4gIGVkaXRVcmxCbGFuaz86IGJvb2xlYW47XHJcbiAgZGlzYWJsZVNlbGZMb2FkaW5nPzogYm9vbGVhbjtcclxuICBlbmFibGVDaGVja2JveFNlbGVjdGlvbj86IGJvb2xlYW47XHJcbiAgYnJlYWRjcnVtYkxhYmVsPzogc3RyaW5nW107XHJcbn1cclxuIl19