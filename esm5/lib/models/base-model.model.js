/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BaseModel = /** @class */ (function () {
    function BaseModel() {
    }
    return BaseModel;
}());
export { BaseModel };
if (false) {
    /** @type {?} */
    BaseModel.prototype.Id;
    /** @type {?} */
    BaseModel.prototype.FirmId;
    /** @type {?} */
    BaseModel.prototype.InsertedBy;
    /** @type {?} */
    BaseModel.prototype.InsertedDate;
    /** @type {?} */
    BaseModel.prototype.UpdatedBy;
    /** @type {?} */
    BaseModel.prototype.UpdatedDate;
    /** @type {?} */
    BaseModel.prototype.OperationType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1tb2RlbC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYmFzZS1tb2RlbC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUE7SUFBQTtJQVFBLENBQUM7SUFBRCxnQkFBQztBQUFELENBQUMsQUFSRCxJQVFDOzs7O0lBUEcsdUJBQVc7O0lBQ1gsMkJBQWdCOztJQUNoQiwrQkFBb0I7O0lBQ3BCLGlDQUFvQjs7SUFDcEIsOEJBQW1COztJQUNuQixnQ0FBbUI7O0lBQ25CLGtDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9wZXJhdGlvblR5cGUgfSBmcm9tICcuLi9lbnVtcy9vcGVyYXRpb24tdHlwZS5lbnVtJztcclxuXHJcbmV4cG9ydCBjbGFzcyBCYXNlTW9kZWwge1xyXG4gICAgSWQ6IG51bWJlcjtcclxuICAgIEZpcm1JZD86IG51bWJlcjtcclxuICAgIEluc2VydGVkQnk/OiBudW1iZXI7XHJcbiAgICBJbnNlcnRlZERhdGU/OiBEYXRlO1xyXG4gICAgVXBkYXRlZEJ5PzogbnVtYmVyO1xyXG4gICAgVXBkYXRlZERhdGU/OiBEYXRlO1xyXG4gICAgT3BlcmF0aW9uVHlwZT86IE9wZXJhdGlvblR5cGU7XHJcbn1cclxuIl19