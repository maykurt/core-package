/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function RequestOptions() { }
if (false) {
    /** @type {?} */
    RequestOptions.prototype.UrlOptions;
    /** @type {?|undefined} */
    RequestOptions.prototype.CustomFilter;
    /** @type {?|undefined} */
    RequestOptions.prototype.CustomEndPoint;
    /** @type {?|undefined} */
    RequestOptions.prototype.HeaderParameters;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWVzdC1vcHRpb25zLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9yZXF1ZXN0LW9wdGlvbnMubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUlBLG9DQUtDOzs7SUFKQyxvQ0FBdUI7O0lBQ3ZCLHNDQUFpQzs7SUFDakMsd0NBQXlCOztJQUN6QiwwQ0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVcmxPcHRpb25zIH0gZnJvbSAnLi91cmwtb3B0aW9ucy5tb2RlbCc7XHJcbmltcG9ydCB7IEdlbmVyaWNFeHByZXNzaW9uIH0gZnJvbSAnLi9nZW5lcmljLWV4cHJlc3Npb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBIZWFkZXJQYXJhbWV0ZXIgfSBmcm9tICcuL2h0dHAvaGVhZGVyLXBhcmFtZXRlci5tb2RlbCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFJlcXVlc3RPcHRpb25zIHtcclxuICBVcmxPcHRpb25zOiBVcmxPcHRpb25zO1xyXG4gIEN1c3RvbUZpbHRlcj86IEdlbmVyaWNFeHByZXNzaW9uO1xyXG4gIEN1c3RvbUVuZFBvaW50PzogYm9vbGVhbjtcclxuICBIZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW107XHJcbn1cclxuIl19