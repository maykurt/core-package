/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var /**
 * @template T
 */
ServiceResult = /** @class */ (function () {
    function ServiceResult() {
    }
    return ServiceResult;
}());
/**
 * @template T
 */
export { ServiceResult };
if (false) {
    /** @type {?} */
    ServiceResult.prototype.ErrorMessage;
    /** @type {?} */
    ServiceResult.prototype.HttpStatusCode;
    /** @type {?} */
    ServiceResult.prototype.IsSuccess;
    /** @type {?} */
    ServiceResult.prototype.Result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmljZS1yZXN1bHQubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2h0dHAvc2VydmljZS1yZXN1bHQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0lBQUE7SUFLQSxDQUFDO0lBQUQsb0JBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQzs7Ozs7OztJQUpDLHFDQUFnQzs7SUFDaEMsdUNBQXVCOztJQUN2QixrQ0FBbUI7O0lBQ25CLCtCQUFVIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFNlcnZpY2VSZXN1bHQ8VD4ge1xyXG4gIEVycm9yTWVzc2FnZTogc3RyaW5nIHwgc3RyaW5nW107XHJcbiAgSHR0cFN0YXR1c0NvZGU6IG51bWJlcjtcclxuICBJc1N1Y2Nlc3M6IGJvb2xlYW47XHJcbiAgUmVzdWx0OiBUO1xyXG59XHJcbiJdfQ==