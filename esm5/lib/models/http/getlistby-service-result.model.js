/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var GetListByResult = /** @class */ (function () {
    function GetListByResult() {
    }
    return GetListByResult;
}());
export { GetListByResult };
if (false) {
    /** @type {?} */
    GetListByResult.prototype.CurrentPage;
    /** @type {?} */
    GetListByResult.prototype.NextPage;
    /** @type {?} */
    GetListByResult.prototype.PageSize;
    /** @type {?} */
    GetListByResult.prototype.PreviousPage;
    /** @type {?} */
    GetListByResult.prototype.TotalCount;
    /** @type {?} */
    GetListByResult.prototype.TotalPages;
    /** @type {?} */
    GetListByResult.prototype.Data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0bGlzdGJ5LXNlcnZpY2UtcmVzdWx0Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9odHRwL2dldGxpc3RieS1zZXJ2aWNlLXJlc3VsdC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUE7SUFBQTtJQVFBLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFSRCxJQVFDOzs7O0lBUEcsc0NBQW9COztJQUNwQixtQ0FBa0I7O0lBQ2xCLG1DQUFpQjs7SUFDakIsdUNBQXNCOztJQUN0QixxQ0FBbUI7O0lBQ25CLHFDQUFtQjs7SUFDbkIsK0JBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgR2V0TGlzdEJ5UmVzdWx0IHtcclxuICAgIEN1cnJlbnRQYWdlOiBudW1iZXI7XHJcbiAgICBOZXh0UGFnZTogYm9vbGVhbjtcclxuICAgIFBhZ2VTaXplOiBudW1iZXI7XHJcbiAgICBQcmV2aW91c1BhZ2U6IGJvb2xlYW47XHJcbiAgICBUb3RhbENvdW50OiBudW1iZXI7XHJcbiAgICBUb3RhbFBhZ2VzOiBudW1iZXI7XHJcbiAgICBEYXRhOiBhbnk7XHJcbn1cclxuIl19