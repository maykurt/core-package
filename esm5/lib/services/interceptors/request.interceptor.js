/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, map, filter, take, finalize } from 'rxjs/operators';
import { AuthenticationService } from '../auth/authentication.service';
import { LoadingService, ToastrUtilsService, StorageService, BreadcrumbService } from '../custom/index';
import { Headers, Storage, StorageType } from '../../enums/index';
import { Router } from '@angular/router';
var RequestInterceptor = /** @class */ (function () {
    function RequestInterceptor(injector, loadingService, storageService, router, toastr) {
        this.injector = injector;
        this.loadingService = loadingService;
        this.storageService = storageService;
        this.router = router;
        this.toastr = toastr;
        this.isRefreshingToken = false;
        this.tokenSubject = new BehaviorSubject(null);
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    RequestInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        this.authenticationService = this.injector.get(AuthenticationService);
        this.loadingService = this.injector.get(LoadingService);
        // it is for disabling general loading
        /** @type {?} */
        var disableGeneralLoading;
        // it is for session-expired to avoid navitaion
        /** @type {?} */
        var disableNavigation;
        // resim gönderme de content type eklemin diye
        /** @type {?} */
        var fileOperation;
        // breadcrumba custom label gönderme
        /** @type {?} */
        var isBreadcrumbCustomLabel;
        /** @type {?} */
        var breadcrumbCustomLabel;
        // controls the header if it exist
        if (request.headers.has(Headers.DisableLoading)) {
            disableGeneralLoading = true;
            request = request.clone({ headers: request.headers.delete(Headers.DisableLoading) });
        }
        // controls the header if it exist
        if (request.headers.has(Headers.DisableNavigation)) {
            disableNavigation = true;
            request = request.clone({ headers: request.headers.delete(Headers.DisableNavigation) });
        }
        // controls the header if it exist
        if (request.headers.has(Headers.FileOperation)) {
            fileOperation = true;
            request = request.clone({ headers: request.headers.delete(Headers.FileOperation) });
        }
        // disabling loading when getting en tr
        if (request.url.indexOf('assets/i18n') !== -1) {
            disableGeneralLoading = true;
        }
        // controls the header if it exist
        if (request.headers.has(Headers.BreadcrumbCustomLabel)) {
            this.breadcrumbService = this.injector.get(BreadcrumbService);
            isBreadcrumbCustomLabel = true;
            breadcrumbCustomLabel = request.headers.getAll(Headers.BreadcrumbCustomLabel);
            request = request.clone({ headers: request.headers.delete(Headers.BreadcrumbCustomLabel) });
        }
        // if it is not set true then send request url to loading service
        if (!disableGeneralLoading) {
            this.loadingService.insertLoadingRequest(request.url);
        }
        if (!request.headers.has(Headers.ContentType) && !fileOperation) {
            request = request.clone({ headers: request.headers.set(Headers.ContentType, 'application/json') });
        }
        request = request.clone({
            headers: request.headers.set('Accept', 'application/json')
        });
        /** @type {?} */
        var refDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        request = request.clone({
            headers: request.headers.set(Headers.ReferenceDateHeader, refDate || new Date().toISOString())
        });
        return next.handle(this.addTokenToRequest(request, this.authenticationService.getAuthToken()))
            .pipe(map(function (event) {
            if (event instanceof HttpResponse) {
                /** @type {?} */
                var response = (/** @type {?} */ (event));
                if (response && response.body) {
                    if (response.body.hasOwnProperty('IsSuccess') && !response.body.IsSuccess) {
                        /** @type {?} */
                        var errorMessage = '0001: An Error Occurred';
                        if (response.body.ErrorMessage instanceof Array) {
                            errorMessage = (/** @type {?} */ (response.body.ErrorMessage));
                            errorMessage.forEach(function (message) {
                                _this.toastr.error(message);
                            });
                        }
                        else {
                            errorMessage = (/** @type {?} */ (response.body.ErrorMessage));
                            _this.toastr.error(response.body.ErrorMessage);
                        }
                        return throwError(errorMessage);
                    }
                    else if (isBreadcrumbCustomLabel) {
                        _this.handdleResponseForBreadcrumb(response.body, breadcrumbCustomLabel);
                    }
                }
            }
            return event;
        }), catchError(function (err) {
            if (!disableGeneralLoading) {
                _this.loadingService.removeLoadingRequest(request.url);
            }
            if (err instanceof HttpErrorResponse) {
                switch (((/** @type {?} */ (err))).status) {
                    case 0:
                        _this.showError(err);
                        break;
                    case 401:
                        return _this.handle401Error(request, next, err);
                    case 400:
                        return _this.handle400Error(err, disableNavigation);
                    case 500:
                        _this.showError(err);
                        break;
                }
            }
            return throwError(err);
        }), finalize(function () {
            if (!disableGeneralLoading) {
                _this.loadingService.removeLoadingRequest(request.url);
            }
        }));
    };
    /**
     * @private
     * @param {?} response
     * @param {?} breadcrumbCustomLabel
     * @return {?}
     */
    RequestInterceptor.prototype.handdleResponseForBreadcrumb = /**
     * @private
     * @param {?} response
     * @param {?} breadcrumbCustomLabel
     * @return {?}
     */
    function (response, breadcrumbCustomLabel) {
        if (response.IsSuccess && response.Result && this.breadcrumbService) {
            // came from headers
            /** @type {?} */
            var breadcrumbLabels = [];
            // resposne data
            /** @type {?} */
            var data_1;
            /** @type {?} */
            var generatedCustomLabel_1 = '';
            // checkh array or string and convert into array
            if (breadcrumbCustomLabel) {
                if (breadcrumbCustomLabel instanceof Array) {
                    if (breadcrumbCustomLabel.indexOf(Headers.BreadcrumbCustomLabel) === -1) {
                        breadcrumbLabels = breadcrumbCustomLabel;
                    }
                }
                else if (breadcrumbCustomLabel !== Headers.BreadcrumbCustomLabel) {
                    breadcrumbLabels.push(breadcrumbCustomLabel);
                }
            }
            // check result is array, and convert into single data object
            if (response.Result instanceof Array) {
                if (response.Result.length > 0) {
                    data_1 = response.Result[0];
                }
            }
            else {
                data_1 = response.Result;
            }
            if (data_1) {
                if (breadcrumbLabels && breadcrumbLabels.length > 0) {
                    // use header value
                    breadcrumbLabels.forEach(function (element) {
                        if (data_1[element]) {
                            generatedCustomLabel_1 += data_1[element] + ' ';
                        }
                    });
                }
                else if (data_1.hasOwnProperty('Definition') && data_1.Definition) {
                    // use default label
                    generatedCustomLabel_1 = data_1.Definition;
                }
                else if (data_1.hasOwnProperty('Description') && data_1.Description) {
                    // use default label
                    generatedCustomLabel_1 = data_1.Description;
                }
            }
            /** @type {?} */
            var splitedUrl = this.router.url.split('/');
            if (splitedUrl && generatedCustomLabel_1 !== '') {
                this.breadcrumbService.addCustomLabel(splitedUrl[splitedUrl.length - 1], this.router.url, generatedCustomLabel_1);
            }
        }
    };
    /**
     * @private
     * @param {?} request
     * @param {?} token
     * @return {?}
     */
    RequestInterceptor.prototype.addTokenToRequest = /**
     * @private
     * @param {?} request
     * @param {?} token
     * @return {?}
     */
    function (request, token) {
        // it is for not adding token to header when login or refresh token
        if (token && request.url.lastIndexOf('Token') === -1) {
            return request.clone({ setHeaders: { Authorization: "Bearer " + token } });
        }
        else {
            return request;
        }
    };
    /**
     * @param {?} error
     * @param {?=} disableNavigation
     * @return {?}
     */
    RequestInterceptor.prototype.handle400Error = /**
     * @param {?} error
     * @param {?=} disableNavigation
     * @return {?}
     */
    function (error, disableNavigation) {
        this.showError(error);
        this.authenticationService.logout(disableNavigation);
        return throwError(error);
    };
    /**
     * @private
     * @param {?} request
     * @param {?} next
     * @param {?} error
     * @return {?}
     */
    RequestInterceptor.prototype.handle401Error = /**
     * @private
     * @param {?} request
     * @param {?} next
     * @param {?} error
     * @return {?}
     */
    function (request, next, error) {
        var _this = this;
        /** @type {?} */
        var authToken = this.authenticationService.getAuthToken();
        // TOken var mı kontrolü yoksa login yapıyordur
        if (authToken) {
            if (!this.isRefreshingToken) {
                this.isRefreshingToken = true;
                // Reset here so that the following requests wait until the token
                // comes back from the refreshToken call.
                this.tokenSubject.next(null);
                return this.authenticationService.refreshToken()
                    .pipe(switchMap(function (response) {
                    if (response) {
                        _this.authenticationService.handdleTokenResponse(response);
                        /** @type {?} */
                        var token = _this.authenticationService.getAuthToken();
                        _this.tokenSubject.next(token);
                        return next.handle(_this.addTokenToRequest(request, token));
                    }
                    else {
                        return throwError(error);
                    }
                }), catchError(function (err) {
                    _this.showError(err);
                    _this.authenticationService.logout();
                    return throwError(err);
                }), finalize(function () {
                    _this.isRefreshingToken = false;
                }));
            }
            else {
                this.isRefreshingToken = false;
                return this.tokenSubject
                    .pipe(filter(function (token) { return token != null; }), take(1), switchMap(function (token) {
                    if (token) {
                        return next.handle(_this.addTokenToRequest(request, token));
                    }
                    else {
                        _this.authenticationService.logout();
                        return throwError(error);
                    }
                }));
            }
        }
        else {
            this.showError(error);
            this.authenticationService.logout();
            return throwError(error);
        }
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    RequestInterceptor.prototype.showError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        if (error && error.error && error.error.ErrorMessage) {
            this.toastr.error(error.error.ErrorMessage);
        }
        else if (error && error.message) {
            this.toastr.error(error.message);
        }
    };
    RequestInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    RequestInterceptor.ctorParameters = function () { return [
        { type: Injector },
        { type: LoadingService },
        { type: StorageService },
        { type: Router },
        { type: ToastrUtilsService }
    ]; };
    return RequestInterceptor;
}());
export { RequestInterceptor };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.authenticationService;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.breadcrumbService;
    /** @type {?} */
    RequestInterceptor.prototype.isRefreshingToken;
    /** @type {?} */
    RequestInterceptor.prototype.tokenSubject;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.injector;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.loadingService;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.storageService;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.router;
    /**
     * @type {?}
     * @private
     */
    RequestInterceptor.prototype.toastr;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWVzdC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9pbnRlcmNlcHRvcnMvcmVxdWVzdC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFckQsT0FBTyxFQUtMLFlBQVksRUFLWixpQkFBaUIsRUFDbEIsTUFBTSxzQkFBc0IsQ0FBQztBQUU5QixPQUFPLEVBQWMsVUFBVSxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVwRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUV2RSxPQUFPLEVBQ0wsY0FBYyxFQUNkLGtCQUFrQixFQUNsQixjQUFjLEVBQ2QsaUJBQWlCLEVBQ2xCLE1BQU0saUJBQWlCLENBQUM7QUFFekIsT0FBTyxFQUNMLE9BQU8sRUFDUCxPQUFPLEVBQ1AsV0FBVyxFQUNaLE1BQU0sbUJBQW1CLENBQUM7QUFHM0IsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBR3pDO0lBU0UsNEJBQ1UsUUFBa0IsRUFDbEIsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsTUFBYyxFQUNkLE1BQTBCO1FBSjFCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsV0FBTSxHQUFOLE1BQU0sQ0FBb0I7UUFScEMsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGlCQUFZLEdBQTRCLElBQUksZUFBZSxDQUFTLElBQUksQ0FBQyxDQUFDO0lBUTFFLENBQUM7Ozs7OztJQUVELHNDQUFTOzs7OztJQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUF0RCxpQkE4SEM7UUF2SEMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQzs7O1lBR3BELHFCQUE4Qjs7O1lBRTlCLGlCQUEwQjs7O1lBRzFCLGFBQXNCOzs7WUFHdEIsdUJBQWdDOztZQUNoQyxxQkFBd0M7UUFFNUMsa0NBQWtDO1FBQ2xDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQy9DLHFCQUFxQixHQUFHLElBQUksQ0FBQztZQUM3QixPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3RGO1FBRUQsa0NBQWtDO1FBQ2xDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDbEQsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUN6RjtRQUVELGtDQUFrQztRQUNsQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUM5QyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDckY7UUFFRCx1Q0FBdUM7UUFDdkMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUM3QyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7U0FDOUI7UUFFRCxrQ0FBa0M7UUFDbEMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsRUFBRTtZQUN0RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUU5RCx1QkFBdUIsR0FBRyxJQUFJLENBQUM7WUFDL0IscUJBQXFCLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDOUUsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBRTdGO1FBR0QsaUVBQWlFO1FBQ2pFLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN2RDtRQUVELElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDL0QsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNwRztRQUVELE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQ3RCLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUM7U0FDM0QsQ0FBQyxDQUFDOztZQUVHLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDaEcsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDdEIsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxPQUFPLElBQUksSUFBSSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMvRixDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQzthQUMzRixJQUFJLENBQ0gsR0FBRyxDQUFDLFVBQUMsS0FBcUI7WUFDeEIsSUFBSSxLQUFLLFlBQVksWUFBWSxFQUFFOztvQkFDM0IsUUFBUSxHQUFHLG1CQUFBLEtBQUssRUFBb0M7Z0JBQzFELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7b0JBQzdCLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTs7NEJBQ3JFLFlBQVksR0FBUSx5QkFBeUI7d0JBQ2pELElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLFlBQVksS0FBSyxFQUFFOzRCQUMvQyxZQUFZLEdBQUcsbUJBQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQVksQ0FBQzs0QkFDdEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQU87Z0NBQzNCLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUM3QixDQUFDLENBQUMsQ0FBQzt5QkFDSjs2QkFBTTs0QkFDTCxZQUFZLEdBQUcsbUJBQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQVUsQ0FBQzs0QkFDcEQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzt5QkFDL0M7d0JBQ0QsT0FBTyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7cUJBQ2pDO3lCQUFNLElBQUksdUJBQXVCLEVBQUU7d0JBQ2xDLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLHFCQUFxQixDQUFDLENBQUM7cUJBQ3pFO2lCQUNGO2FBQ0Y7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFDLEdBQVE7WUFDbEIsSUFBSSxDQUFDLHFCQUFxQixFQUFFO2dCQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN2RDtZQUVELElBQUksR0FBRyxZQUFZLGlCQUFpQixFQUFFO2dCQUNwQyxRQUFRLENBQUMsbUJBQUEsR0FBRyxFQUFxQixDQUFDLENBQUMsTUFBTSxFQUFFO29CQUN6QyxLQUFLLENBQUM7d0JBQ0osS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDcEIsTUFBTTtvQkFDUixLQUFLLEdBQUc7d0JBQ04sT0FBTyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQ2pELEtBQUssR0FBRzt3QkFDTixPQUFPLEtBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLGlCQUFpQixDQUFDLENBQUM7b0JBQ3JELEtBQUssR0FBRzt3QkFDTixLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUNwQixNQUFNO2lCQUNUO2FBQ0Y7WUFDRCxPQUFPLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsRUFDRixRQUFRLENBQUM7WUFDUCxJQUFJLENBQUMscUJBQXFCLEVBQUU7Z0JBQzFCLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3ZEO1FBQ0gsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7SUFFTyx5REFBNEI7Ozs7OztJQUFwQyxVQUFxQyxRQUE0QixFQUFFLHFCQUF3QztRQUN6RyxJQUFJLFFBQVEsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7OztnQkFHL0QsZ0JBQWdCLEdBQWEsRUFBRTs7O2dCQUcvQixNQUFTOztnQkFFVCxzQkFBb0IsR0FBRyxFQUFFO1lBRTdCLGdEQUFnRDtZQUNoRCxJQUFJLHFCQUFxQixFQUFFO2dCQUN6QixJQUFJLHFCQUFxQixZQUFZLEtBQUssRUFBRTtvQkFDMUMsSUFBSSxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3ZFLGdCQUFnQixHQUFHLHFCQUFxQixDQUFDO3FCQUMxQztpQkFDRjtxQkFBTSxJQUFJLHFCQUFxQixLQUFLLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRTtvQkFDbEUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7aUJBQzlDO2FBQ0Y7WUFFRCw2REFBNkQ7WUFDN0QsSUFBSSxRQUFRLENBQUMsTUFBTSxZQUFZLEtBQUssRUFBRTtnQkFDcEMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzlCLE1BQUksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMzQjthQUNGO2lCQUFNO2dCQUNMLE1BQUksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO2FBQ3hCO1lBRUQsSUFBSSxNQUFJLEVBQUU7Z0JBQ1IsSUFBSSxnQkFBZ0IsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNuRCxtQkFBbUI7b0JBQ25CLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87d0JBQzlCLElBQUksTUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFOzRCQUNqQixzQkFBb0IsSUFBSSxNQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDO3lCQUM3QztvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtxQkFBTSxJQUFJLE1BQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksTUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDL0Qsb0JBQW9CO29CQUNwQixzQkFBb0IsR0FBRyxNQUFJLENBQUMsVUFBVSxDQUFDO2lCQUN4QztxQkFBTSxJQUFJLE1BQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksTUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDakUsb0JBQW9CO29CQUNwQixzQkFBb0IsR0FBRyxNQUFJLENBQUMsV0FBVyxDQUFDO2lCQUN6QzthQUNGOztnQkFFSyxVQUFVLEdBQWEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQUV2RCxJQUFJLFVBQVUsSUFBSSxzQkFBb0IsS0FBSyxFQUFFLEVBQUU7Z0JBQzdDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsc0JBQW9CLENBQUMsQ0FBQzthQUNqSDtTQUNGO0lBQ0gsQ0FBQzs7Ozs7OztJQUVPLDhDQUFpQjs7Ozs7O0lBQXpCLFVBQTBCLE9BQXlCLEVBQUUsS0FBYTtRQUNoRSxtRUFBbUU7UUFDbkUsSUFBSSxLQUFLLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDcEQsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsVUFBVSxFQUFFLEVBQUUsYUFBYSxFQUFFLFlBQVUsS0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQzVFO2FBQU07WUFDTCxPQUFPLE9BQU8sQ0FBQztTQUNoQjtJQUNILENBQUM7Ozs7OztJQUVELDJDQUFjOzs7OztJQUFkLFVBQWUsS0FBSyxFQUFFLGlCQUEyQjtRQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNyRCxPQUFPLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7Ozs7OztJQUVPLDJDQUFjOzs7Ozs7O0lBQXRCLFVBQXVCLE9BQXlCLEVBQUUsSUFBaUIsRUFBRSxLQUF3QjtRQUE3RixpQkFxREM7O1lBcERPLFNBQVMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFO1FBQzNELCtDQUErQztRQUMvQyxJQUFJLFNBQVMsRUFBRTtZQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBRTlCLGlFQUFpRTtnQkFDakUseUNBQXlDO2dCQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFN0IsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFO3FCQUM3QyxJQUFJLENBQ0gsU0FBUyxDQUFDLFVBQUMsUUFBK0I7b0JBQ3hDLElBQUksUUFBUSxFQUFFO3dCQUNaLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7NEJBQ3BELEtBQUssR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFO3dCQUN2RCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDOUIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztxQkFDNUQ7eUJBQU07d0JBQ0wsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQzFCO2dCQUNILENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEdBQUc7b0JBQ1osS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDcEIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUNwQyxPQUFPLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLEVBQ0YsUUFBUSxDQUFDO29CQUNQLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxDQUNILENBQUM7YUFDTDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO2dCQUUvQixPQUFPLElBQUksQ0FBQyxZQUFZO3FCQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxJQUFJLElBQUksRUFBYixDQUFhLENBQUMsRUFDbEMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUNQLFNBQVMsQ0FBQyxVQUFBLEtBQUs7b0JBQ2IsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztxQkFDNUQ7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNwQyxPQUFPLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDMUI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNUO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3BDLE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzFCO0lBRUgsQ0FBQzs7Ozs7O0lBRU8sc0NBQVM7Ozs7O0lBQWpCLFVBQWtCLEtBQXdCO1FBQ3hDLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7WUFDcEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUM3QzthQUFNLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQzs7Z0JBclJGLFVBQVU7Ozs7Z0JBckNVLFFBQVE7Z0JBcUIzQixjQUFjO2dCQUVkLGNBQWM7Z0JBV1AsTUFBTTtnQkFaYixrQkFBa0I7O0lBcVNwQix5QkFBQztDQUFBLEFBdFJELElBc1JDO1NBclJZLGtCQUFrQjs7Ozs7O0lBRTdCLG1EQUFxRDs7Ozs7SUFDckQsK0NBQTZDOztJQUU3QywrQ0FBMEI7O0lBQzFCLDBDQUEwRTs7Ozs7SUFHeEUsc0NBQTBCOzs7OztJQUMxQiw0Q0FBc0M7Ozs7O0lBQ3RDLDRDQUFzQzs7Ozs7SUFDdEMsb0NBQXNCOzs7OztJQUN0QixvQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHtcclxuICBIdHRwSW50ZXJjZXB0b3IsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cEV2ZW50LFxyXG4gIEh0dHBSZXNwb25zZSxcclxuICBIdHRwVXNlckV2ZW50LFxyXG4gIEh0dHBIZWFkZXJSZXNwb25zZSxcclxuICBIdHRwU2VudEV2ZW50LFxyXG4gIEh0dHBQcm9ncmVzc0V2ZW50LFxyXG4gIEh0dHBFcnJvclJlc3BvbnNlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIHN3aXRjaE1hcCwgbWFwLCBmaWx0ZXIsIHRha2UsIGZpbmFsaXplIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aC9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7XHJcbiAgTG9hZGluZ1NlcnZpY2UsXHJcbiAgVG9hc3RyVXRpbHNTZXJ2aWNlLFxyXG4gIFN0b3JhZ2VTZXJ2aWNlLFxyXG4gIEJyZWFkY3J1bWJTZXJ2aWNlXHJcbn0gZnJvbSAnLi4vY3VzdG9tL2luZGV4JztcclxuXHJcbmltcG9ydCB7XHJcbiAgSGVhZGVycyxcclxuICBTdG9yYWdlLFxyXG4gIFN0b3JhZ2VUeXBlXHJcbn0gZnJvbSAnLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgQ29yZUh0dHBSZXNwb25zZSwgU2VydmljZVJlc3VsdCB9IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUmVxdWVzdEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuXHJcbiAgcHJpdmF0ZSBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZTtcclxuICBwcml2YXRlIGJyZWFkY3J1bWJTZXJ2aWNlOiBCcmVhZGNydW1iU2VydmljZTtcclxuXHJcbiAgaXNSZWZyZXNoaW5nVG9rZW4gPSBmYWxzZTtcclxuICB0b2tlblN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KG51bGwpO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yLFxyXG4gICAgcHJpdmF0ZSBsb2FkaW5nU2VydmljZTogTG9hZGluZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIHRvYXN0cjogVG9hc3RyVXRpbHNTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBTZW50RXZlbnQgfFxyXG4gICAgSHR0cEhlYWRlclJlc3BvbnNlIHxcclxuICAgIEh0dHBQcm9ncmVzc0V2ZW50IHxcclxuICAgIEh0dHBSZXNwb25zZTxhbnk+IHxcclxuICAgIEh0dHBVc2VyRXZlbnQ8YW55PlxyXG4gICAgfCBhbnk+IHtcclxuXHJcbiAgICB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KEF1dGhlbnRpY2F0aW9uU2VydmljZSk7XHJcbiAgICB0aGlzLmxvYWRpbmdTZXJ2aWNlID0gdGhpcy5pbmplY3Rvci5nZXQoTG9hZGluZ1NlcnZpY2UpO1xyXG5cclxuICAgIC8vIGl0IGlzIGZvciBkaXNhYmxpbmcgZ2VuZXJhbCBsb2FkaW5nXHJcbiAgICBsZXQgZGlzYWJsZUdlbmVyYWxMb2FkaW5nOiBib29sZWFuO1xyXG4gICAgLy8gaXQgaXMgZm9yIHNlc3Npb24tZXhwaXJlZCB0byBhdm9pZCBuYXZpdGFpb25cclxuICAgIGxldCBkaXNhYmxlTmF2aWdhdGlvbjogYm9vbGVhbjtcclxuXHJcbiAgICAvLyByZXNpbSBnw7ZuZGVybWUgZGUgY29udGVudCB0eXBlIGVrbGVtaW4gZGl5ZVxyXG4gICAgbGV0IGZpbGVPcGVyYXRpb246IGJvb2xlYW47XHJcblxyXG4gICAgLy8gYnJlYWRjcnVtYmEgY3VzdG9tIGxhYmVsIGfDtm5kZXJtZVxyXG4gICAgbGV0IGlzQnJlYWRjcnVtYkN1c3RvbUxhYmVsOiBib29sZWFuO1xyXG4gICAgbGV0IGJyZWFkY3J1bWJDdXN0b21MYWJlbDogc3RyaW5nIHwgc3RyaW5nW107XHJcblxyXG4gICAgLy8gY29udHJvbHMgdGhlIGhlYWRlciBpZiBpdCBleGlzdFxyXG4gICAgaWYgKHJlcXVlc3QuaGVhZGVycy5oYXMoSGVhZGVycy5EaXNhYmxlTG9hZGluZykpIHtcclxuICAgICAgZGlzYWJsZUdlbmVyYWxMb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoeyBoZWFkZXJzOiByZXF1ZXN0LmhlYWRlcnMuZGVsZXRlKEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcpIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNvbnRyb2xzIHRoZSBoZWFkZXIgaWYgaXQgZXhpc3RcclxuICAgIGlmIChyZXF1ZXN0LmhlYWRlcnMuaGFzKEhlYWRlcnMuRGlzYWJsZU5hdmlnYXRpb24pKSB7XHJcbiAgICAgIGRpc2FibGVOYXZpZ2F0aW9uID0gdHJ1ZTtcclxuICAgICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoeyBoZWFkZXJzOiByZXF1ZXN0LmhlYWRlcnMuZGVsZXRlKEhlYWRlcnMuRGlzYWJsZU5hdmlnYXRpb24pIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNvbnRyb2xzIHRoZSBoZWFkZXIgaWYgaXQgZXhpc3RcclxuICAgIGlmIChyZXF1ZXN0LmhlYWRlcnMuaGFzKEhlYWRlcnMuRmlsZU9wZXJhdGlvbikpIHtcclxuICAgICAgZmlsZU9wZXJhdGlvbiA9IHRydWU7XHJcbiAgICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHsgaGVhZGVyczogcmVxdWVzdC5oZWFkZXJzLmRlbGV0ZShIZWFkZXJzLkZpbGVPcGVyYXRpb24pIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGRpc2FibGluZyBsb2FkaW5nIHdoZW4gZ2V0dGluZyBlbiB0clxyXG4gICAgaWYgKHJlcXVlc3QudXJsLmluZGV4T2YoJ2Fzc2V0cy9pMThuJykgIT09IC0xKSB7XHJcbiAgICAgIGRpc2FibGVHZW5lcmFsTG9hZGluZyA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gY29udHJvbHMgdGhlIGhlYWRlciBpZiBpdCBleGlzdFxyXG4gICAgaWYgKHJlcXVlc3QuaGVhZGVycy5oYXMoSGVhZGVycy5CcmVhZGNydW1iQ3VzdG9tTGFiZWwpKSB7XHJcbiAgICAgIHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChCcmVhZGNydW1iU2VydmljZSk7XHJcblxyXG4gICAgICBpc0JyZWFkY3J1bWJDdXN0b21MYWJlbCA9IHRydWU7XHJcbiAgICAgIGJyZWFkY3J1bWJDdXN0b21MYWJlbCA9IHJlcXVlc3QuaGVhZGVycy5nZXRBbGwoSGVhZGVycy5CcmVhZGNydW1iQ3VzdG9tTGFiZWwpO1xyXG4gICAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7IGhlYWRlcnM6IHJlcXVlc3QuaGVhZGVycy5kZWxldGUoSGVhZGVycy5CcmVhZGNydW1iQ3VzdG9tTGFiZWwpIH0pO1xyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgLy8gaWYgaXQgaXMgbm90IHNldCB0cnVlIHRoZW4gc2VuZCByZXF1ZXN0IHVybCB0byBsb2FkaW5nIHNlcnZpY2VcclxuICAgIGlmICghZGlzYWJsZUdlbmVyYWxMb2FkaW5nKSB7XHJcbiAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UuaW5zZXJ0TG9hZGluZ1JlcXVlc3QocmVxdWVzdC51cmwpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghcmVxdWVzdC5oZWFkZXJzLmhhcyhIZWFkZXJzLkNvbnRlbnRUeXBlKSAmJiAhZmlsZU9wZXJhdGlvbikge1xyXG4gICAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7IGhlYWRlcnM6IHJlcXVlc3QuaGVhZGVycy5zZXQoSGVhZGVycy5Db250ZW50VHlwZSwgJ2FwcGxpY2F0aW9uL2pzb24nKSB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7XHJcbiAgICAgIGhlYWRlcnM6IHJlcXVlc3QuaGVhZGVycy5zZXQoJ0FjY2VwdCcsICdhcHBsaWNhdGlvbi9qc29uJylcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IHJlZkRhdGUgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldFN0b3JlZChTdG9yYWdlLlN5c3RlbVJlZkRhdGUsIFN0b3JhZ2VUeXBlLlNlc3Npb25TdG9yYWdlKTtcclxuICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcclxuICAgICAgaGVhZGVyczogcmVxdWVzdC5oZWFkZXJzLnNldChIZWFkZXJzLlJlZmVyZW5jZURhdGVIZWFkZXIsIHJlZkRhdGUgfHwgbmV3IERhdGUoKS50b0lTT1N0cmluZygpKVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHRoaXMuYWRkVG9rZW5Ub1JlcXVlc3QocmVxdWVzdCwgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuZ2V0QXV0aFRva2VuKCkpKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBtYXAoKGV2ZW50OiBIdHRwRXZlbnQ8YW55PikgPT4ge1xyXG4gICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlc3BvbnNlID0gZXZlbnQgYXMgSHR0cFJlc3BvbnNlPFNlcnZpY2VSZXN1bHQ8YW55Pj47XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5ib2R5KSB7XHJcbiAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkuaGFzT3duUHJvcGVydHkoJ0lzU3VjY2VzcycpICYmICFyZXNwb25zZS5ib2R5LklzU3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGVycm9yTWVzc2FnZTogYW55ID0gJzAwMDE6IEFuIEVycm9yIE9jY3VycmVkJztcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LkVycm9yTWVzc2FnZSBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZSA9IHJlc3BvbnNlLmJvZHkuRXJyb3JNZXNzYWdlIGFzIHN0cmluZ1tdO1xyXG4gICAgICAgICAgICAgICAgICBlcnJvck1lc3NhZ2UuZm9yRWFjaCgobWVzc2FnZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9hc3RyLmVycm9yKG1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZSA9IHJlc3BvbnNlLmJvZHkuRXJyb3JNZXNzYWdlIGFzIHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgdGhpcy50b2FzdHIuZXJyb3IocmVzcG9uc2UuYm9keS5FcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3JNZXNzYWdlKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKGlzQnJlYWRjcnVtYkN1c3RvbUxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmhhbmRkbGVSZXNwb25zZUZvckJyZWFkY3J1bWIocmVzcG9uc2UuYm9keSwgYnJlYWRjcnVtYkN1c3RvbUxhYmVsKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBldmVudDtcclxuICAgICAgICB9KSxcclxuICAgICAgICBjYXRjaEVycm9yKChlcnI6IGFueSkgPT4ge1xyXG4gICAgICAgICAgaWYgKCFkaXNhYmxlR2VuZXJhbExvYWRpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5yZW1vdmVMb2FkaW5nUmVxdWVzdChyZXF1ZXN0LnVybCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgaWYgKGVyciBpbnN0YW5jZW9mIEh0dHBFcnJvclJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoKGVyciBhcyBIdHRwRXJyb3JSZXNwb25zZSkuc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgY2FzZSAwOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zaG93RXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgIGNhc2UgNDAxOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuaGFuZGxlNDAxRXJyb3IocmVxdWVzdCwgbmV4dCwgZXJyKTtcclxuICAgICAgICAgICAgICBjYXNlIDQwMDpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmhhbmRsZTQwMEVycm9yKGVyciwgZGlzYWJsZU5hdmlnYXRpb24pO1xyXG4gICAgICAgICAgICAgIGNhc2UgNTAwOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zaG93RXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnIpO1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGZpbmFsaXplKCgpID0+IHtcclxuICAgICAgICAgIGlmICghZGlzYWJsZUdlbmVyYWxMb2FkaW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ1NlcnZpY2UucmVtb3ZlTG9hZGluZ1JlcXVlc3QocmVxdWVzdC51cmwpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRkbGVSZXNwb25zZUZvckJyZWFkY3J1bWIocmVzcG9uc2U6IFNlcnZpY2VSZXN1bHQ8YW55PiwgYnJlYWRjcnVtYkN1c3RvbUxhYmVsOiBzdHJpbmcgfCBzdHJpbmdbXSk6IHZvaWQge1xyXG4gICAgaWYgKHJlc3BvbnNlLklzU3VjY2VzcyAmJiByZXNwb25zZS5SZXN1bHQgJiYgdGhpcy5icmVhZGNydW1iU2VydmljZSkge1xyXG5cclxuICAgICAgLy8gY2FtZSBmcm9tIGhlYWRlcnNcclxuICAgICAgbGV0IGJyZWFkY3J1bWJMYWJlbHM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gICAgICAvLyByZXNwb3NuZSBkYXRhXHJcbiAgICAgIGxldCBkYXRhOiBhbnk7XHJcblxyXG4gICAgICBsZXQgZ2VuZXJhdGVkQ3VzdG9tTGFiZWwgPSAnJztcclxuXHJcbiAgICAgIC8vIGNoZWNraCBhcnJheSBvciBzdHJpbmcgYW5kIGNvbnZlcnQgaW50byBhcnJheVxyXG4gICAgICBpZiAoYnJlYWRjcnVtYkN1c3RvbUxhYmVsKSB7XHJcbiAgICAgICAgaWYgKGJyZWFkY3J1bWJDdXN0b21MYWJlbCBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICBpZiAoYnJlYWRjcnVtYkN1c3RvbUxhYmVsLmluZGV4T2YoSGVhZGVycy5CcmVhZGNydW1iQ3VzdG9tTGFiZWwpID09PSAtMSkge1xyXG4gICAgICAgICAgICBicmVhZGNydW1iTGFiZWxzID0gYnJlYWRjcnVtYkN1c3RvbUxhYmVsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoYnJlYWRjcnVtYkN1c3RvbUxhYmVsICE9PSBIZWFkZXJzLkJyZWFkY3J1bWJDdXN0b21MYWJlbCkge1xyXG4gICAgICAgICAgYnJlYWRjcnVtYkxhYmVscy5wdXNoKGJyZWFkY3J1bWJDdXN0b21MYWJlbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBjaGVjayByZXN1bHQgaXMgYXJyYXksIGFuZCBjb252ZXJ0IGludG8gc2luZ2xlIGRhdGEgb2JqZWN0XHJcbiAgICAgIGlmIChyZXNwb25zZS5SZXN1bHQgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgIGlmIChyZXNwb25zZS5SZXN1bHQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgZGF0YSA9IHJlc3BvbnNlLlJlc3VsdFswXTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZGF0YSA9IHJlc3BvbnNlLlJlc3VsdDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICBpZiAoYnJlYWRjcnVtYkxhYmVscyAmJiBicmVhZGNydW1iTGFiZWxzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIC8vIHVzZSBoZWFkZXIgdmFsdWVcclxuICAgICAgICAgIGJyZWFkY3J1bWJMYWJlbHMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgaWYgKGRhdGFbZWxlbWVudF0pIHtcclxuICAgICAgICAgICAgICBnZW5lcmF0ZWRDdXN0b21MYWJlbCArPSBkYXRhW2VsZW1lbnRdICsgJyAnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKGRhdGEuaGFzT3duUHJvcGVydHkoJ0RlZmluaXRpb24nKSAmJiBkYXRhLkRlZmluaXRpb24pIHtcclxuICAgICAgICAgIC8vIHVzZSBkZWZhdWx0IGxhYmVsXHJcbiAgICAgICAgICBnZW5lcmF0ZWRDdXN0b21MYWJlbCA9IGRhdGEuRGVmaW5pdGlvbjtcclxuICAgICAgICB9IGVsc2UgaWYgKGRhdGEuaGFzT3duUHJvcGVydHkoJ0Rlc2NyaXB0aW9uJykgJiYgZGF0YS5EZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgLy8gdXNlIGRlZmF1bHQgbGFiZWxcclxuICAgICAgICAgIGdlbmVyYXRlZEN1c3RvbUxhYmVsID0gZGF0YS5EZXNjcmlwdGlvbjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IHNwbGl0ZWRVcmw6IHN0cmluZ1tdID0gdGhpcy5yb3V0ZXIudXJsLnNwbGl0KCcvJyk7XHJcblxyXG4gICAgICBpZiAoc3BsaXRlZFVybCAmJiBnZW5lcmF0ZWRDdXN0b21MYWJlbCAhPT0gJycpIHtcclxuICAgICAgICB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmFkZEN1c3RvbUxhYmVsKHNwbGl0ZWRVcmxbc3BsaXRlZFVybC5sZW5ndGggLSAxXSwgdGhpcy5yb3V0ZXIudXJsLCBnZW5lcmF0ZWRDdXN0b21MYWJlbCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgYWRkVG9rZW5Ub1JlcXVlc3QocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgdG9rZW46IHN0cmluZyk6IEh0dHBSZXF1ZXN0PGFueT4ge1xyXG4gICAgLy8gaXQgaXMgZm9yIG5vdCBhZGRpbmcgdG9rZW4gdG8gaGVhZGVyIHdoZW4gbG9naW4gb3IgcmVmcmVzaCB0b2tlblxyXG4gICAgaWYgKHRva2VuICYmIHJlcXVlc3QudXJsLmxhc3RJbmRleE9mKCdUb2tlbicpID09PSAtMSkge1xyXG4gICAgICByZXR1cm4gcmVxdWVzdC5jbG9uZSh7IHNldEhlYWRlcnM6IHsgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3Rva2VufWAgfSB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiByZXF1ZXN0O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaGFuZGxlNDAwRXJyb3IoZXJyb3IsIGRpc2FibGVOYXZpZ2F0aW9uPzogYm9vbGVhbikge1xyXG4gICAgdGhpcy5zaG93RXJyb3IoZXJyb3IpO1xyXG4gICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UubG9nb3V0KGRpc2FibGVOYXZpZ2F0aW9uKTtcclxuICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlNDAxRXJyb3IocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIsIGVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgY29uc3QgYXV0aFRva2VuID0gdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuZ2V0QXV0aFRva2VuKCk7XHJcbiAgICAvLyBUT2tlbiB2YXIgbcSxIGtvbnRyb2zDvCB5b2tzYSBsb2dpbiB5YXDEsXlvcmR1clxyXG4gICAgaWYgKGF1dGhUb2tlbikge1xyXG4gICAgICBpZiAoIXRoaXMuaXNSZWZyZXNoaW5nVG9rZW4pIHtcclxuICAgICAgICB0aGlzLmlzUmVmcmVzaGluZ1Rva2VuID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgLy8gUmVzZXQgaGVyZSBzbyB0aGF0IHRoZSBmb2xsb3dpbmcgcmVxdWVzdHMgd2FpdCB1bnRpbCB0aGUgdG9rZW5cclxuICAgICAgICAvLyBjb21lcyBiYWNrIGZyb20gdGhlIHJlZnJlc2hUb2tlbiBjYWxsLlxyXG4gICAgICAgIHRoaXMudG9rZW5TdWJqZWN0Lm5leHQobnVsbCk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5yZWZyZXNoVG9rZW4oKVxyXG4gICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgIHN3aXRjaE1hcCgocmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuaGFuZGRsZVRva2VuUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG9rZW4gPSB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5nZXRBdXRoVG9rZW4oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9rZW5TdWJqZWN0Lm5leHQodG9rZW4pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHRoaXMuYWRkVG9rZW5Ub1JlcXVlc3QocmVxdWVzdCwgdG9rZW4pKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLnNob3dFcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgIHRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLmxvZ291dCgpO1xyXG4gICAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycik7XHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICBmaW5hbGl6ZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5pc1JlZnJlc2hpbmdUb2tlbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmlzUmVmcmVzaGluZ1Rva2VuID0gZmFsc2U7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnRva2VuU3ViamVjdFxyXG4gICAgICAgICAgLnBpcGUoZmlsdGVyKHRva2VuID0+IHRva2VuICE9IG51bGwpLFxyXG4gICAgICAgICAgICB0YWtlKDEpLFxyXG4gICAgICAgICAgICBzd2l0Y2hNYXAodG9rZW4gPT4ge1xyXG4gICAgICAgICAgICAgIGlmICh0b2tlbikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHRoaXMuYWRkVG9rZW5Ub1JlcXVlc3QocmVxdWVzdCwgdG9rZW4pKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UubG9nb3V0KCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2hvd0Vycm9yKGVycm9yKTtcclxuICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UubG9nb3V0KCk7XHJcbiAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNob3dFcnJvcihlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpOiB2b2lkIHtcclxuICAgIGlmIChlcnJvciAmJiBlcnJvci5lcnJvciAmJiBlcnJvci5lcnJvci5FcnJvck1lc3NhZ2UpIHtcclxuICAgICAgdGhpcy50b2FzdHIuZXJyb3IoZXJyb3IuZXJyb3IuRXJyb3JNZXNzYWdlKTtcclxuICAgIH0gZWxzZSBpZiAoZXJyb3IgJiYgZXJyb3IubWVzc2FnZSkge1xyXG4gICAgICB0aGlzLnRvYXN0ci5lcnJvcihlcnJvci5tZXNzYWdlKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19