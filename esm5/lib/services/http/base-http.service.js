/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { HttpHeaders } from '@angular/common/http';
import { Headers } from '../../enums/headers.enum';
var BaseHttpService = /** @class */ (function () {
    function BaseHttpService(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    BaseHttpService.prototype.httpGet = /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    function (requestUrl, headers) {
        if (requestUrl === void 0) { requestUrl = ''; }
        return this.httpClient.get(requestUrl, {
            headers: headers,
            observe: 'response'
        });
    };
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    BaseHttpService.prototype.httpGetFile = /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    function (requestUrl, headers) {
        if (requestUrl === void 0) { requestUrl = ''; }
        return this.httpClient.get(requestUrl, {
            headers: headers,
            observe: 'response',
            responseType: 'blob'
        });
    };
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    BaseHttpService.prototype.httpPost = /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    function (requestUrl, data, headers) {
        if (requestUrl === void 0) { requestUrl = ''; }
        /** @type {?} */
        var fileOperation;
        if (headers) {
            fileOperation = headers.get(Headers.FileOperation);
        }
        return this.httpClient.post(requestUrl, fileOperation ? data : JSON.stringify(data), {
            headers: headers
        });
    };
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    BaseHttpService.prototype.httpPatch = /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    function (requestUrl, data, headers) {
        if (requestUrl === void 0) { requestUrl = ''; }
        /** @type {?} */
        var fileOperation;
        if (headers) {
            fileOperation = headers.get(Headers.FileOperation);
        }
        return this.httpClient.patch(requestUrl, fileOperation ? data : JSON.stringify(data), {
            headers: headers
        });
    };
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    BaseHttpService.prototype.httpDelete = /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    function (requestUrl, headers) {
        if (requestUrl === void 0) { requestUrl = ''; }
        return this.httpClient.delete(requestUrl, {
            headers: headers
        });
    };
    /**
     * @protected
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseHttpService.prototype.getHttpHeader = /**
     * @protected
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    function (filterQuery, headerParameters) {
        /** @type {?} */
        var httpHeaders = new HttpHeaders();
        if (filterQuery) {
            httpHeaders = httpHeaders.append('Charset', 'UTF-8');
            /** @type {?} */
            var newFilterQuery = JSON.parse(JSON.stringify(filterQuery));
            if (newFilterQuery && newFilterQuery.FilterGroups && newFilterQuery.FilterGroups.length > 0) {
                newFilterQuery.FilterGroups.forEach(function (filterGroup) {
                    if (filterGroup.Filters && filterGroup.Filters.length > 0) {
                        filterGroup.Filters.forEach(function (filter) {
                            if (filter.Value) {
                                filter.Value = encodeURIComponent(filter.Value);
                            }
                            if (filter.OtherValue) {
                                filter.OtherValue = encodeURIComponent(filter.OtherValue);
                            }
                        });
                    }
                });
            }
            httpHeaders = httpHeaders.append(Headers.PagingHeader, JSON.stringify(newFilterQuery));
        }
        if (headerParameters) {
            headerParameters.forEach(function (headerParameter) {
                if (headerParameter) {
                    httpHeaders = httpHeaders.append(headerParameter.header, headerParameter.value);
                }
            });
        }
        return httpHeaders;
    };
    return BaseHttpService;
}());
export { BaseHttpService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BaseHttpService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1odHRwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvaHR0cC9iYXNlLWh0dHAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBYyxNQUFNLHNCQUFzQixDQUFDO0FBRy9ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUtuRDtJQUVFLHlCQUFzQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzVDLENBQUM7Ozs7Ozs7SUFFRCxpQ0FBTzs7Ozs7O0lBQVAsVUFBVyxVQUF1QixFQUFFLE9BQXFCO1FBQTlDLDJCQUFBLEVBQUEsZUFBdUI7UUFDaEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBSSxVQUFVLEVBQUU7WUFDeEMsT0FBTyxFQUFFLE9BQU87WUFDaEIsT0FBTyxFQUFFLFVBQVU7U0FDcEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELHFDQUFXOzs7Ozs7SUFBWCxVQUFlLFVBQXVCLEVBQUUsT0FBcUI7UUFBOUMsMkJBQUEsRUFBQSxlQUF1QjtRQUNwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRTtZQUNyQyxPQUFPLEVBQUUsT0FBTztZQUNoQixPQUFPLEVBQUUsVUFBVTtZQUNuQixZQUFZLEVBQUUsTUFBTTtTQUNyQixDQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7OztJQUVELGtDQUFROzs7Ozs7O0lBQVIsVUFBWSxVQUF1QixFQUFFLElBQVMsRUFBRSxPQUFxQjtRQUF6RCwyQkFBQSxFQUFBLGVBQXVCOztZQUM3QixhQUFxQjtRQUN6QixJQUFJLE9BQU8sRUFBRTtZQUNYLGFBQWEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNwRDtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUksVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3RGLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7O0lBRUQsbUNBQVM7Ozs7Ozs7SUFBVCxVQUFhLFVBQXVCLEVBQUUsSUFBUyxFQUFFLE9BQXFCO1FBQXpELDJCQUFBLEVBQUEsZUFBdUI7O1lBQzlCLGFBQXFCO1FBQ3pCLElBQUksT0FBTyxFQUFFO1lBQ1gsYUFBYSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBSSxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdkYsT0FBTyxFQUFFLE9BQU87U0FDakIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELG9DQUFVOzs7Ozs7SUFBVixVQUFjLFVBQXVCLEVBQUUsT0FBcUI7UUFBOUMsMkJBQUEsRUFBQSxlQUF1QjtRQUNuQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFJLFVBQVUsRUFBRTtZQUMzQyxPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7O0lBRVMsdUNBQWE7Ozs7OztJQUF2QixVQUF3QixXQUErQixFQUFFLGdCQUFvQzs7WUFDdkYsV0FBVyxHQUFHLElBQUksV0FBVyxFQUFFO1FBRW5DLElBQUksV0FBVyxFQUFFO1lBQ2YsV0FBVyxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDOztnQkFFL0MsY0FBYyxHQUFzQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFakYsSUFBSSxjQUFjLElBQUksY0FBYyxDQUFDLFlBQVksSUFBSSxjQUFjLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzNGLGNBQWMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUMsV0FBd0I7b0JBQzNELElBQUksV0FBVyxDQUFDLE9BQU8sSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3pELFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBYzs0QkFDekMsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO2dDQUNoQixNQUFNLENBQUMsS0FBSyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDakQ7NEJBRUQsSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFO2dDQUNyQixNQUFNLENBQUMsVUFBVSxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQzs2QkFDM0Q7d0JBRUgsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7Z0JBRUgsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQ3hGO1FBRUQsSUFBSSxnQkFBZ0IsRUFBRTtZQUNwQixnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxlQUFnQztnQkFDeEQsSUFBSSxlQUFlLEVBQUU7b0JBQ25CLFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqRjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLFdBQVcsQ0FBQztJQUNyQixDQUFDO0lBQ0gsc0JBQUM7QUFBRCxDQUFDLEFBdEZELElBc0ZDOzs7Ozs7O0lBcEZhLHFDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBIZWFkZXJzLCBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBHZW5lcmljRXhwcmVzc2lvbiB9IGZyb20gJy4uLy4uL21vZGVscy9nZW5lcmljLWV4cHJlc3Npb24ubW9kZWwnO1xyXG5cclxuaW1wb3J0IHsgSGVhZGVycyB9IGZyb20gJy4uLy4uL2VudW1zL2hlYWRlcnMuZW51bSc7XHJcbmltcG9ydCB7IEhlYWRlclBhcmFtZXRlciB9IGZyb20gJy4uLy4uL21vZGVscy9odHRwL2hlYWRlci1wYXJhbWV0ZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBGaWx0ZXJHcm91cCwgRmlsdGVyIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgQmFzZUh0dHBTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHtcclxuICB9XHJcblxyXG4gIGh0dHBHZXQ8VD4ocmVxdWVzdFVybDogc3RyaW5nID0gJycsIGhlYWRlcnM/OiBIdHRwSGVhZGVycykge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8VD4ocmVxdWVzdFVybCwge1xyXG4gICAgICBoZWFkZXJzOiBoZWFkZXJzLFxyXG4gICAgICBvYnNlcnZlOiAncmVzcG9uc2UnXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGh0dHBHZXRGaWxlPFQ+KHJlcXVlc3RVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpIHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHJlcXVlc3RVcmwsIHtcclxuICAgICAgaGVhZGVyczogaGVhZGVycyxcclxuICAgICAgb2JzZXJ2ZTogJ3Jlc3BvbnNlJyxcclxuICAgICAgcmVzcG9uc2VUeXBlOiAnYmxvYidcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaHR0cFBvc3Q8VD4ocmVxdWVzdFVybDogc3RyaW5nID0gJycsIGRhdGE6IGFueSwgaGVhZGVycz86IEh0dHBIZWFkZXJzKSB7XHJcbiAgICBsZXQgZmlsZU9wZXJhdGlvbjogc3RyaW5nO1xyXG4gICAgaWYgKGhlYWRlcnMpIHtcclxuICAgICAgZmlsZU9wZXJhdGlvbiA9IGhlYWRlcnMuZ2V0KEhlYWRlcnMuRmlsZU9wZXJhdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PFQ+KHJlcXVlc3RVcmwsIGZpbGVPcGVyYXRpb24gPyBkYXRhIDogSlNPTi5zdHJpbmdpZnkoZGF0YSksIHtcclxuICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBodHRwUGF0Y2g8VD4ocmVxdWVzdFVybDogc3RyaW5nID0gJycsIGRhdGE6IGFueSwgaGVhZGVycz86IEh0dHBIZWFkZXJzKSB7XHJcbiAgICBsZXQgZmlsZU9wZXJhdGlvbjogc3RyaW5nO1xyXG4gICAgaWYgKGhlYWRlcnMpIHtcclxuICAgICAgZmlsZU9wZXJhdGlvbiA9IGhlYWRlcnMuZ2V0KEhlYWRlcnMuRmlsZU9wZXJhdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wYXRjaDxUPihyZXF1ZXN0VXJsLCBmaWxlT3BlcmF0aW9uID8gZGF0YSA6IEpTT04uc3RyaW5naWZ5KGRhdGEpLCB7XHJcbiAgICAgIGhlYWRlcnM6IGhlYWRlcnNcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaHR0cERlbGV0ZTxUPihyZXF1ZXN0VXJsOiBzdHJpbmcgPSAnJywgaGVhZGVycz86IEh0dHBIZWFkZXJzKSB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmRlbGV0ZTxUPihyZXF1ZXN0VXJsLCB7XHJcbiAgICAgIGhlYWRlcnM6IGhlYWRlcnNcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGdldEh0dHBIZWFkZXIoZmlsdGVyUXVlcnk/OiBHZW5lcmljRXhwcmVzc2lvbiwgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogSHR0cEhlYWRlcnMge1xyXG4gICAgbGV0IGh0dHBIZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XHJcblxyXG4gICAgaWYgKGZpbHRlclF1ZXJ5KSB7XHJcbiAgICAgIGh0dHBIZWFkZXJzID0gaHR0cEhlYWRlcnMuYXBwZW5kKCdDaGFyc2V0JywgJ1VURi04Jyk7XHJcblxyXG4gICAgICBjb25zdCBuZXdGaWx0ZXJRdWVyeTogR2VuZXJpY0V4cHJlc3Npb24gPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGZpbHRlclF1ZXJ5KSk7XHJcblxyXG4gICAgICBpZiAobmV3RmlsdGVyUXVlcnkgJiYgbmV3RmlsdGVyUXVlcnkuRmlsdGVyR3JvdXBzICYmIG5ld0ZpbHRlclF1ZXJ5LkZpbHRlckdyb3Vwcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgbmV3RmlsdGVyUXVlcnkuRmlsdGVyR3JvdXBzLmZvckVhY2goKGZpbHRlckdyb3VwOiBGaWx0ZXJHcm91cCkgPT4ge1xyXG4gICAgICAgICAgaWYgKGZpbHRlckdyb3VwLkZpbHRlcnMgJiYgZmlsdGVyR3JvdXAuRmlsdGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGZpbHRlckdyb3VwLkZpbHRlcnMuZm9yRWFjaCgoZmlsdGVyOiBGaWx0ZXIpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoZmlsdGVyLlZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBmaWx0ZXIuVmFsdWUgPSBlbmNvZGVVUklDb21wb25lbnQoZmlsdGVyLlZhbHVlKTtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGlmIChmaWx0ZXIuT3RoZXJWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgZmlsdGVyLk90aGVyVmFsdWUgPSBlbmNvZGVVUklDb21wb25lbnQoZmlsdGVyLk90aGVyVmFsdWUpO1xyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaHR0cEhlYWRlcnMgPSBodHRwSGVhZGVycy5hcHBlbmQoSGVhZGVycy5QYWdpbmdIZWFkZXIsIEpTT04uc3RyaW5naWZ5KG5ld0ZpbHRlclF1ZXJ5KSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGhlYWRlclBhcmFtZXRlcnMpIHtcclxuICAgICAgaGVhZGVyUGFyYW1ldGVycy5mb3JFYWNoKChoZWFkZXJQYXJhbWV0ZXI6IEhlYWRlclBhcmFtZXRlcikgPT4ge1xyXG4gICAgICAgIGlmIChoZWFkZXJQYXJhbWV0ZXIpIHtcclxuICAgICAgICAgIGh0dHBIZWFkZXJzID0gaHR0cEhlYWRlcnMuYXBwZW5kKGhlYWRlclBhcmFtZXRlci5oZWFkZXIsIGhlYWRlclBhcmFtZXRlci52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBodHRwSGVhZGVycztcclxuICB9XHJcbn1cclxuIl19