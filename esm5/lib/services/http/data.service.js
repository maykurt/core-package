/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AppConfig } from '../custom/app.config';
import { BaseHttpService } from './base-http.service';
import { Headers } from '../../enums/headers.enum';
var DataService = /** @class */ (function (_super) {
    tslib_1.__extends(DataService, _super);
    function DataService(httpClient) {
        var _this = _super.call(this, httpClient) || this;
        _this.httpClient = httpClient;
        return _this;
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.create = /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (resource, endPointUrl, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpPost(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.update = /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (resource, endPointUrl, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpPatch(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.delete = /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (Id, endPointUrl, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpDelete(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.getList = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.getListBy = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, filterQuery, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.get = /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (Id, endPointUrl, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    DataService.prototype.getBy = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, filterQuery, headerParameters) {
        var _this = this;
        if (endPointUrl === void 0) { endPointUrl = ''; }
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    DataService.prototype.wrapHttpResponse = /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    function (response) {
        /** @type {?} */
        var pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        var responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        var coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    };
    DataService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    DataService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return DataService;
}(BaseHttpService));
export { DataService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2h0dHAvZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXJDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFVdEQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRW5EO0lBQ2lDLHVDQUFlO0lBRTlDLHFCQUFzQixVQUFzQjtRQUE1QyxZQUNFLGtCQUFNLFVBQVUsQ0FBQyxTQUNsQjtRQUZxQixnQkFBVSxHQUFWLFVBQVUsQ0FBWTs7SUFFNUMsQ0FBQzs7Ozs7Ozs7SUFFRCw0QkFBTTs7Ozs7OztJQUFOLFVBQVUsUUFBUSxFQUFFLFdBQXdCLEVBQUUsZ0JBQW9DO1FBQWxGLGlCQUtDO1FBTG1CLDRCQUFBLEVBQUEsZ0JBQXdCO1FBQzFDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3ZILElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFhO1lBQ3RCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELDRCQUFNOzs7Ozs7O0lBQU4sVUFBVSxRQUFRLEVBQUUsV0FBd0IsRUFBRSxnQkFBb0M7UUFBbEYsaUJBS0M7UUFMbUIsNEJBQUEsRUFBQSxnQkFBd0I7UUFDMUMsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxXQUFXLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDeEgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQsNEJBQU07Ozs7Ozs7SUFBTixVQUFVLEVBQUUsRUFBRSxXQUF3QixFQUFFLGdCQUFvQztRQUE1RSxpQkFLQztRQUxhLDRCQUFBLEVBQUEsZ0JBQXdCO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxHQUFHLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUMxSCxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtZQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7OztJQUVELDZCQUFPOzs7Ozs7SUFBUCxVQUFXLFdBQXdCLEVBQUUsZ0JBQW9DO1FBQXpFLGlCQUtDO1FBTFUsNEJBQUEsRUFBQSxnQkFBd0I7UUFDakMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUM1RyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtZQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7Ozs7SUFFRCwrQkFBUzs7Ozs7OztJQUFULFVBQWEsV0FBd0IsRUFBRSxXQUE4QixFQUFFLGdCQUFvQztRQUEzRyxpQkFLQztRQUxZLDRCQUFBLEVBQUEsZ0JBQXdCO1FBQ25DLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDbkgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQseUJBQUc7Ozs7Ozs7SUFBSCxVQUFPLEVBQUUsRUFBRSxXQUF3QixFQUFFLGdCQUFvQztRQUF6RSxpQkFLQztRQUxVLDRCQUFBLEVBQUEsZ0JBQXdCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxHQUFHLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUN2SCxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtZQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7Ozs7SUFFRCwyQkFBSzs7Ozs7OztJQUFMLFVBQVMsV0FBd0IsRUFBRSxXQUE4QixFQUFFLGdCQUFvQztRQUF2RyxpQkFLQztRQUxRLDRCQUFBLEVBQUEsZ0JBQXdCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDbkgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFHTyxzQ0FBZ0I7Ozs7OztJQUF4QixVQUE0QixRQUFhOztZQUNuQyxZQUFZO1FBRWhCLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFBRTtZQUMxRSxZQUFZLEdBQUcsbUJBQWMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFBLENBQUM7U0FDN0Y7O1lBRUssWUFBWSxHQUFHLG1CQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLEVBQUE7O1lBQzVELGdCQUFnQixHQUF3QjtZQUM1QyxhQUFhLEVBQUUsWUFBWTtTQUM1QjtRQUVELElBQUksWUFBWSxFQUFFO1lBQ2hCLGdCQUFnQixDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7U0FDOUM7UUFFRCxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7O2dCQTFFRixVQUFVOzs7O2dCQWxCRixVQUFVOztJQTZGbkIsa0JBQUM7Q0FBQSxBQTNFRCxDQUNpQyxlQUFlLEdBMEUvQztTQTFFWSxXQUFXOzs7Ozs7SUFFVixpQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBBcHBDb25maWcgfSBmcm9tICcuLi9jdXN0b20vYXBwLmNvbmZpZyc7XHJcbmltcG9ydCB7IEJhc2VIdHRwU2VydmljZSB9IGZyb20gJy4vYmFzZS1odHRwLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHtcclxuICBIZWFkZXJQYXJhbWV0ZXIsXHJcbiAgU2VydmljZVJlc3VsdCxcclxuICBQYWdpbmdSZXN1bHQsXHJcbiAgQ29yZUh0dHBSZXNwb25zZSxcclxuICBHZW5lcmljRXhwcmVzc2lvblxyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBIZWFkZXJzIH0gZnJvbSAnLi4vLi4vZW51bXMvaGVhZGVycy5lbnVtJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIERhdGFTZXJ2aWNlIGV4dGVuZHMgQmFzZUh0dHBTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHtcclxuICAgIHN1cGVyKGh0dHBDbGllbnQpO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlPFQ+KHJlc291cmNlLCBlbmRQb2ludFVybDogc3RyaW5nID0gJycsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cFBvc3Q8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCwgcmVzb3VyY2UsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZTxUPihyZXNvdXJjZSwgZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBQYXRjaDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIGVuZFBvaW50VXJsLCByZXNvdXJjZSwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlPFQ+KElkLCBlbmRQb2ludFVybDogc3RyaW5nID0gJycsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cERlbGV0ZTxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIGVuZFBvaW50VXJsICsgJy8nICsgSWQsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGdldExpc3Q8VD4oZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBHZXQ8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdEJ5PFQ+KGVuZFBvaW50VXJsOiBzdHJpbmcgPSAnJywgZmlsdGVyUXVlcnk6IEdlbmVyaWNFeHByZXNzaW9uLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBHZXQ8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCwgdGhpcy5nZXRIdHRwSGVhZGVyKGZpbHRlclF1ZXJ5LCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGdldDxUPihJZCwgZW5kUG9pbnRVcmw6IHN0cmluZyA9ICcnLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIHJldHVybiB0aGlzLmh0dHBHZXQ8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyBlbmRQb2ludFVybCArICcvJyArIElkLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBnZXRCeTxUPihlbmRQb2ludFVybDogc3RyaW5nID0gJycsIGZpbHRlclF1ZXJ5OiBHZW5lcmljRXhwcmVzc2lvbiwgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihmaWx0ZXJRdWVyeSwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHJpdmF0ZSB3cmFwSHR0cFJlc3BvbnNlPFQ+KHJlc3BvbnNlOiBhbnkpIHtcclxuICAgIGxldCBwYWdpbmdSZXN1bHQ7XHJcblxyXG4gICAgaWYgKHJlc3BvbnNlLmhlYWRlcnMgJiYgcmVzcG9uc2UuaGVhZGVycy5nZXQoSGVhZGVycy5QYWdpbmdSZXNwb25zZUhlYWRlcikpIHtcclxuICAgICAgcGFnaW5nUmVzdWx0ID0gPFBhZ2luZ1Jlc3VsdD5KU09OLnBhcnNlKHJlc3BvbnNlLmhlYWRlcnMuZ2V0KEhlYWRlcnMuUGFnaW5nUmVzcG9uc2VIZWFkZXIpKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCByZXNwb25zZUJvZHkgPSA8U2VydmljZVJlc3VsdDxUPj4ocmVzcG9uc2UuYm9keSB8fCByZXNwb25zZSk7XHJcbiAgICBjb25zdCBjb3JlSHR0cFJlc3BvbnNlOiBDb3JlSHR0cFJlc3BvbnNlPFQ+ID0ge1xyXG4gICAgICBzZXJ2aWNlUmVzdWx0OiByZXNwb25zZUJvZHlcclxuICAgIH07XHJcblxyXG4gICAgaWYgKHBhZ2luZ1Jlc3VsdCkge1xyXG4gICAgICBjb3JlSHR0cFJlc3BvbnNlLnBhZ2luZ1Jlc3VsdCA9IHBhZ2luZ1Jlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gY29yZUh0dHBSZXNwb25zZTtcclxuICB9XHJcbn1cclxuIl19