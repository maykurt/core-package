/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Headers } from '../../enums/headers.enum';
import { BaseHttpService } from './base-http.service';
import { AppConfig } from '../custom/app.config';
var BaseService = /** @class */ (function (_super) {
    tslib_1.__extends(BaseService, _super);
    function BaseService(httpClient) {
        var _this = _super.call(this, httpClient) || this;
        _this.httpClient = httpClient;
        _this.endpoint = '';
        _this.moduleUrl = '';
        return _this;
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.create = /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (resource, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPost(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.update = /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (resource, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPatch(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.delete = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.deleteById = /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (id, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.getList = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.getListBy = /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (filterQuery, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint + '/GetListBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.get = /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, null, headerParameters);
    };
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.getById = /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (id, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, null, headerParameters);
    };
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.getBy = /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (filterQuery, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint + '/GetBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.bulkOperation = /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    function (resource, endPointUrl, headerParameters) {
        var _this = this;
        endPointUrl = endPointUrl || this.endpoint + '/BulkOperation';
        return this.update(resource, endPointUrl, headerParameters)
            .pipe(map(function (response) {
            return _this.wrapHttpResponse(response);
        }));
    };
    /**
     * @private
     * @template T
     * @param {?} url
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.callHttpGet = /**
     * @private
     * @template T
     * @param {?} url
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    function (url, filterQuery, headerParameters) {
        var _this = this;
        /** @type {?} */
        var isFileOPeration = this.isFileOPeration(headerParameters);
        if (isFileOPeration) {
            return this.httpGetFile(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map(function (response) {
                return _this.wrapHttpResponse(response);
            }));
        }
        else {
            return this.httpGet(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map(function (response) {
                return _this.wrapHttpResponse(response);
            }));
        }
    };
    /**
     * @private
     * @param {?=} headerParameters
     * @return {?}
     */
    BaseService.prototype.isFileOPeration = /**
     * @private
     * @param {?=} headerParameters
     * @return {?}
     */
    function (headerParameters) {
        if (headerParameters) {
            /** @type {?} */
            var fileOPeration = headerParameters.find(function (x) { return x && x.header === Headers.FileOperation; });
            return fileOPeration ? true : false;
        }
        else {
            return false;
        }
    };
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    BaseService.prototype.wrapHttpResponse = /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    function (response) {
        /** @type {?} */
        var pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        var responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        var coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    };
    BaseService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    BaseService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return BaseService;
}(BaseHttpService));
export { BaseService };
if (false) {
    /** @type {?} */
    BaseService.prototype.endpoint;
    /** @type {?} */
    BaseService.prototype.moduleUrl;
    /**
     * @type {?}
     * @protected
     */
    BaseService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2h0dHAvYmFzZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXJDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVNuRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBSWpEO0lBQ2lDLHVDQUFlO0lBSTlDLHFCQUFzQixVQUFzQjtRQUE1QyxZQUNFLGtCQUFNLFVBQVUsQ0FBQyxTQUNsQjtRQUZxQixnQkFBVSxHQUFWLFVBQVUsQ0FBWTtRQUg1QyxjQUFRLEdBQVcsRUFBRSxDQUFDO1FBQ3RCLGVBQVMsR0FBVyxFQUFFLENBQUM7O0lBSXZCLENBQUM7Ozs7Ozs7O0lBRUQsNEJBQU07Ozs7Ozs7SUFBTixVQUFVLFFBQVEsRUFBRSxXQUFvQixFQUFFLGdCQUFvQztRQUE5RSxpQkFNQztRQUxDLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMzQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3hJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFhO1lBQ3RCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELDRCQUFNOzs7Ozs7O0lBQU4sVUFBVSxRQUFRLEVBQUUsV0FBb0IsRUFBRSxnQkFBb0M7UUFBOUUsaUJBTUM7UUFMQyxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUN6SSxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtZQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7OztJQUVELDRCQUFNOzs7Ozs7SUFBTixVQUFVLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQXBFLGlCQU1DO1FBTEMsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQzthQUNoSSxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtZQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7Ozs7SUFFRCxnQ0FBVTs7Ozs7OztJQUFWLFVBQWMsRUFBTyxFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQWpGLGlCQU1DO1FBTEMsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQzNJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFhO1lBQ3RCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7O0lBRUQsNkJBQU87Ozs7OztJQUFQLFVBQVcsV0FBb0IsRUFBRSxnQkFBb0M7UUFBckUsaUJBTUM7UUFMQyxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQzdILElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFhO1lBQ3RCLE9BQU8sS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7OztJQUVELCtCQUFTOzs7Ozs7O0lBQVQsVUFBYSxXQUE4QixFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQXZHLGlCQU1DO1FBTEMsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQztRQUMxRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDcEksSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFFRCx5QkFBRzs7Ozs7O0lBQUgsVUFBTyxXQUFvQixFQUFFLGdCQUFvQztRQUMvRCxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUNuSCxDQUFDOzs7Ozs7OztJQUVELDZCQUFPOzs7Ozs7O0lBQVAsVUFBVyxFQUFFLEVBQUUsV0FBb0IsRUFBRSxnQkFBb0M7UUFDdkUsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBSSxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUM5SCxDQUFDOzs7Ozs7OztJQUVELDJCQUFLOzs7Ozs7O0lBQUwsVUFBUyxXQUE4QixFQUFFLFdBQW9CLEVBQUUsZ0JBQW9DO1FBQW5HLGlCQU1DO1FBTEMsV0FBVyxHQUFHLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN0RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7YUFDcEksSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7O0lBRUQsbUNBQWE7Ozs7Ozs7SUFBYixVQUFpQixRQUFhLEVBQUUsV0FBb0IsRUFBRSxnQkFBb0M7UUFBMUYsaUJBTUM7UUFMQyxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7UUFDOUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFJLFFBQVEsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLENBQUM7YUFDM0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7OztJQUVPLGlDQUFXOzs7Ozs7OztJQUFuQixVQUF1QixHQUFXLEVBQUUsV0FBK0IsRUFBRSxnQkFBb0M7UUFBekcsaUJBY0M7O1lBYk8sZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUM7UUFFOUQsSUFBSSxlQUFlLEVBQUU7WUFDbkIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFJLEdBQUcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO2lCQUMvRSxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBYTtnQkFDdEIsT0FBTyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNQO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7aUJBQzNFLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFhO2dCQUN0QixPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ1A7SUFDSCxDQUFDOzs7Ozs7SUFFTyxxQ0FBZTs7Ozs7SUFBdkIsVUFBd0IsZ0JBQW9DO1FBQzFELElBQUksZ0JBQWdCLEVBQUU7O2dCQUNkLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxPQUFPLENBQUMsYUFBYSxFQUF2QyxDQUF1QyxDQUFDO1lBQ3pGLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUVyQzthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7Ozs7Ozs7SUFFTyxzQ0FBZ0I7Ozs7OztJQUF4QixVQUE0QixRQUFhOztZQUNuQyxZQUFZO1FBRWhCLElBQUksUUFBUSxDQUFDLE9BQU8sSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFBRTtZQUMxRSxZQUFZLEdBQUcsbUJBQWMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFBLENBQUM7U0FDN0Y7O1lBRUssWUFBWSxHQUFHLG1CQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLEVBQUE7O1lBQzVELGdCQUFnQixHQUF3QjtZQUM1QyxhQUFhLEVBQUUsWUFBWTtTQUM1QjtRQUVELElBQUksWUFBWSxFQUFFO1lBQ2hCLGdCQUFnQixDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7U0FDOUM7UUFFRCxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7O2dCQTlIRixVQUFVOzs7O2dCQXBCRixVQUFVOztJQW1KbkIsa0JBQUM7Q0FBQSxBQS9IRCxDQUNpQyxlQUFlLEdBOEgvQztTQTlIWSxXQUFXOzs7SUFDdEIsK0JBQXNCOztJQUN0QixnQ0FBdUI7Ozs7O0lBRVgsaUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcclxuXHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEhlYWRlcnMgfSBmcm9tICcuLi8uLi9lbnVtcy9oZWFkZXJzLmVudW0nO1xyXG5cclxuaW1wb3J0IHtcclxuICBIZWFkZXJQYXJhbWV0ZXIsXHJcbiAgR2VuZXJpY0V4cHJlc3Npb24sXHJcbiAgQ29yZUh0dHBSZXNwb25zZSxcclxuICBTZXJ2aWNlUmVzdWx0LFxyXG4gIFBhZ2luZ1Jlc3VsdFxyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcbmltcG9ydCB7IEJhc2VIdHRwU2VydmljZSB9IGZyb20gJy4vYmFzZS1odHRwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBDb25maWcgfSBmcm9tICcuLi9jdXN0b20vYXBwLmNvbmZpZyc7XHJcblxyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEJhc2VTZXJ2aWNlIGV4dGVuZHMgQmFzZUh0dHBTZXJ2aWNlIHtcclxuICBlbmRwb2ludDogc3RyaW5nID0gJyc7XHJcbiAgbW9kdWxlVXJsOiBzdHJpbmcgPSAnJztcclxuXHJcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHtcclxuICAgIHN1cGVyKGh0dHBDbGllbnQpO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlPFQ+KHJlc291cmNlLCBlbmRQb2ludFVybD86IHN0cmluZywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogT2JzZXJ2YWJsZTxDb3JlSHR0cFJlc3BvbnNlPFQ+PiB7XHJcbiAgICBlbmRQb2ludFVybCA9IGVuZFBvaW50VXJsIHx8IHRoaXMuZW5kcG9pbnQ7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwUG9zdDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHJlc291cmNlLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGU8VD4ocmVzb3VyY2UsIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBPYnNlcnZhYmxlPENvcmVIdHRwUmVzcG9uc2U8VD4+IHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludDtcclxuICAgIHJldHVybiB0aGlzLmh0dHBQYXRjaDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHJlc291cmNlLCB0aGlzLmdldEh0dHBIZWFkZXIobnVsbCwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBkZWxldGU8VD4oZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50O1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cERlbGV0ZTxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZUJ5SWQ8VD4oaWQ6IGFueSwgZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSk6IE9ic2VydmFibGU8Q29yZUh0dHBSZXNwb25zZTxUPj4ge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50O1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cERlbGV0ZTxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwgKyAnLycgKyBpZCwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdDxUPihlbmRQb2ludFVybD86IHN0cmluZywgaGVhZGVyUGFyYW1ldGVycz86IEhlYWRlclBhcmFtZXRlcltdKTogT2JzZXJ2YWJsZTxDb3JlSHR0cFJlc3BvbnNlPFQ+PiB7XHJcbiAgICBlbmRQb2ludFVybCA9IGVuZFBvaW50VXJsIHx8IHRoaXMuZW5kcG9pbnQ7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgdGhpcy5tb2R1bGVVcmwgKyBlbmRQb2ludFVybCwgdGhpcy5nZXRIdHRwSGVhZGVyKG51bGwsIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdEJ5PFQ+KGZpbHRlclF1ZXJ5OiBHZW5lcmljRXhwcmVzc2lvbiwgZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50ICsgJy9HZXRMaXN0QnknO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cEdldDxUPihBcHBDb25maWcuc2V0dGluZ3MuZW52LmFwaVVybCArIHRoaXMubW9kdWxlVXJsICsgZW5kUG9pbnRVcmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihmaWx0ZXJRdWVyeSwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBnZXQ8VD4oZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50O1xyXG4gICAgcmV0dXJuIHRoaXMuY2FsbEh0dHBHZXQ8VD4oQXBwQ29uZmlnLnNldHRpbmdzLmVudi5hcGlVcmwgKyB0aGlzLm1vZHVsZVVybCArIGVuZFBvaW50VXJsLCBudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKTtcclxuICB9XHJcblxyXG4gIGdldEJ5SWQ8VD4oaWQsIGVuZFBvaW50VXJsPzogc3RyaW5nLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGVuZFBvaW50VXJsID0gZW5kUG9pbnRVcmwgfHwgdGhpcy5lbmRwb2ludDtcclxuICAgIHJldHVybiB0aGlzLmNhbGxIdHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgdGhpcy5tb2R1bGVVcmwgKyBlbmRQb2ludFVybCArICcvJyArIGlkLCBudWxsLCBoZWFkZXJQYXJhbWV0ZXJzKTtcclxuICB9XHJcblxyXG4gIGdldEJ5PFQ+KGZpbHRlclF1ZXJ5OiBHZW5lcmljRXhwcmVzc2lvbiwgZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50ICsgJy9HZXRCeSc7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwR2V0PFQ+KEFwcENvbmZpZy5zZXR0aW5ncy5lbnYuYXBpVXJsICsgdGhpcy5tb2R1bGVVcmwgKyBlbmRQb2ludFVybCwgdGhpcy5nZXRIdHRwSGVhZGVyKGZpbHRlclF1ZXJ5LCBoZWFkZXJQYXJhbWV0ZXJzKSlcclxuICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgIH0pKTtcclxuICB9XHJcblxyXG4gIGJ1bGtPcGVyYXRpb248VD4ocmVzb3VyY2U6IGFueSwgZW5kUG9pbnRVcmw/OiBzdHJpbmcsIGhlYWRlclBhcmFtZXRlcnM/OiBIZWFkZXJQYXJhbWV0ZXJbXSkge1xyXG4gICAgZW5kUG9pbnRVcmwgPSBlbmRQb2ludFVybCB8fCB0aGlzLmVuZHBvaW50ICsgJy9CdWxrT3BlcmF0aW9uJztcclxuICAgIHJldHVybiB0aGlzLnVwZGF0ZTxUPihyZXNvdXJjZSwgZW5kUG9pbnRVcmwsIGhlYWRlclBhcmFtZXRlcnMpXHJcbiAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBIdHRwUmVzcG9uc2UocmVzcG9uc2UpO1xyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNhbGxIdHRwR2V0PFQ+KHVybDogc3RyaW5nLCBmaWx0ZXJRdWVyeT86IEdlbmVyaWNFeHByZXNzaW9uLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGNvbnN0IGlzRmlsZU9QZXJhdGlvbiA9IHRoaXMuaXNGaWxlT1BlcmF0aW9uKGhlYWRlclBhcmFtZXRlcnMpO1xyXG5cclxuICAgIGlmIChpc0ZpbGVPUGVyYXRpb24pIHtcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cEdldEZpbGU8VD4odXJsLCB0aGlzLmdldEh0dHBIZWFkZXIoZmlsdGVyUXVlcnksIGhlYWRlclBhcmFtZXRlcnMpKVxyXG4gICAgICAgIC5waXBlKG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMud3JhcEh0dHBSZXNwb25zZShyZXNwb25zZSk7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cEdldDxUPih1cmwsIHRoaXMuZ2V0SHR0cEhlYWRlcihmaWx0ZXJRdWVyeSwgaGVhZGVyUGFyYW1ldGVycykpXHJcbiAgICAgICAgLnBpcGUobWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy53cmFwSHR0cFJlc3BvbnNlKHJlc3BvbnNlKTtcclxuICAgICAgICB9KSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGlzRmlsZU9QZXJhdGlvbihoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pOiBib29sZWFuIHtcclxuICAgIGlmIChoZWFkZXJQYXJhbWV0ZXJzKSB7XHJcbiAgICAgIGNvbnN0IGZpbGVPUGVyYXRpb24gPSBoZWFkZXJQYXJhbWV0ZXJzLmZpbmQoeCA9PiB4ICYmIHguaGVhZGVyID09PSBIZWFkZXJzLkZpbGVPcGVyYXRpb24pO1xyXG4gICAgICByZXR1cm4gZmlsZU9QZXJhdGlvbiA/IHRydWUgOiBmYWxzZTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHdyYXBIdHRwUmVzcG9uc2U8VD4ocmVzcG9uc2U6IGFueSkge1xyXG4gICAgbGV0IHBhZ2luZ1Jlc3VsdDtcclxuXHJcbiAgICBpZiAocmVzcG9uc2UuaGVhZGVycyAmJiByZXNwb25zZS5oZWFkZXJzLmdldChIZWFkZXJzLlBhZ2luZ1Jlc3BvbnNlSGVhZGVyKSkge1xyXG4gICAgICBwYWdpbmdSZXN1bHQgPSA8UGFnaW5nUmVzdWx0PkpTT04ucGFyc2UocmVzcG9uc2UuaGVhZGVycy5nZXQoSGVhZGVycy5QYWdpbmdSZXNwb25zZUhlYWRlcikpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IHJlc3BvbnNlQm9keSA9IDxTZXJ2aWNlUmVzdWx0PFQ+PihyZXNwb25zZS5ib2R5IHx8IHJlc3BvbnNlKTtcclxuICAgIGNvbnN0IGNvcmVIdHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8VD4gPSB7XHJcbiAgICAgIHNlcnZpY2VSZXN1bHQ6IHJlc3BvbnNlQm9keVxyXG4gICAgfTtcclxuXHJcbiAgICBpZiAocGFnaW5nUmVzdWx0KSB7XHJcbiAgICAgIGNvcmVIdHRwUmVzcG9uc2UucGFnaW5nUmVzdWx0ID0gcGFnaW5nUmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBjb3JlSHR0cFJlc3BvbnNlO1xyXG4gIH1cclxufVxyXG4iXX0=