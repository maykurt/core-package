/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var AppConfig = /** @class */ (function () {
    function AppConfig(injector) {
        this.injector = injector;
    }
    /**
     * @return {?}
     */
    AppConfig.prototype.load = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            /** @type {?} */
            var http = _this.injector.get(HttpClient);
            http.get('/assets/config/config.json')
                .toPromise()
                .then(function (response) {
                AppConfig.settings = (/** @type {?} */ (response));
                resolve();
            }).catch(function (response) {
                console.log(response);
                reject('Could not load file /assets/config.json');
            });
        });
    };
    AppConfig.settings = (/** @type {?} */ ({}));
    AppConfig.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AppConfig.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    return AppConfig;
}());
export { AppConfig };
if (false) {
    /** @type {?} */
    AppConfig.settings;
    /**
     * @type {?}
     * @private
     */
    AppConfig.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9jdXN0b20vYXBwLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBR2xEO0lBR0UsbUJBQW9CLFFBQWtCO1FBQWxCLGFBQVEsR0FBUixRQUFRLENBQVU7SUFBSSxDQUFDOzs7O0lBRTNDLHdCQUFJOzs7SUFBSjtRQUFBLGlCQWNDO1FBYkMsT0FBTyxJQUFJLE9BQU8sQ0FBTyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDakMsSUFBSSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQztZQUMxQyxJQUFJLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDO2lCQUNuQyxTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLFVBQUMsUUFBb0I7Z0JBQ3pCLFNBQVMsQ0FBQyxRQUFRLEdBQUcsbUJBQVksUUFBUSxFQUFBLENBQUM7Z0JBQzFDLE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsUUFBYTtnQkFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxDQUFDLHlDQUF5QyxDQUFDLENBQUM7WUFDcEQsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFqQmEsa0JBQVEsR0FBZSxtQkFBWSxFQUFFLEVBQUEsQ0FBQzs7Z0JBRnJELFVBQVU7Ozs7Z0JBSlUsUUFBUTs7SUF3QjdCLGdCQUFDO0NBQUEsQUFwQkQsSUFvQkM7U0FuQlksU0FBUzs7O0lBQ3BCLG1CQUFvRDs7Ozs7SUFDeEMsNkJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSUFwcENvbmZpZyB9IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBDb25maWcge1xyXG4gIHB1YmxpYyBzdGF0aWMgc2V0dGluZ3M6IElBcHBDb25maWcgPSA8SUFwcENvbmZpZz57fTtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcikgeyB9XHJcblxyXG4gIGxvYWQoKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8dm9pZD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBjb25zdCBodHRwID0gdGhpcy5pbmplY3Rvci5nZXQoSHR0cENsaWVudCk7XHJcbiAgICAgIGh0dHAuZ2V0KCcvYXNzZXRzL2NvbmZpZy9jb25maWcuanNvbicpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBJQXBwQ29uZmlnKSA9PiB7XHJcbiAgICAgICAgICBBcHBDb25maWcuc2V0dGluZ3MgPSA8SUFwcENvbmZpZz5yZXNwb25zZTtcclxuICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICB9KS5jYXRjaCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgcmVqZWN0KCdDb3VsZCBub3QgbG9hZCBmaWxlIC9hc3NldHMvY29uZmlnLmpzb24nKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICB9XHJcbn1cclxuIl19