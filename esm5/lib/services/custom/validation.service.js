/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ValidationService = /** @class */ (function () {
    function ValidationService() {
    }
    /**
     * @param {?} control
     * @return {?}
     */
    ValidationService.tcValidate = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        /** @type {?} */
        var value = control.value;
        // event.target.value;
        /** @type {?} */
        var isZero = false;
        value = String(value);
        // Sadece rakamlardan oluşup oluşmadığını ve 11 haneli olup olmadığını kontrol ediyoruz.
        /** @type {?} */
        var isEleven = /^[0-9]{11}$/.test(value);
        // İlk rakamın 0 ile başlama kontrolü
        /** @type {?} */
        var firstValue = value.substr(0, 1);
        if (firstValue === '0') {
            isZero = true;
        }
        else {
            isZero = false;
        }
        /** @type {?} */
        var totalX = 0;
        for (var i = 0; i < 10; i++) {
            totalX += Number(value.substr(i, 1));
        }
        // İlk 10 hanesinin toplamınınn 11. haneyi verme kontrolü
        /** @type {?} */
        var isRuleX = String(totalX % 10) === value.substr(10, 1);
        // tek ve çift hanelerin toplamlarının atanacağı degişkenleri tanımlıyoruz
        /** @type {?} */
        var totalY1 = 0;
        /** @type {?} */
        var totalY2 = 0;
        // tek hanelerdeki sayıları toplar
        for (var j = 0; j < 10; j += 2) {
            totalY1 += Number(value.substr(j, 1));
        }
        // çift hanelerdeki sayıları toplar
        for (var k = 1; k < 9; k += 2) {
            totalY2 += Number(value.substr(k, 1));
        }
        /** @type {?} */
        var isRuleY = String(((totalY1 * 7) - totalY2) % 10) === value.substr(9, 1);
        /** @type {?} */
        var deger = isEleven && !isZero && isRuleX && isRuleY;
        // this.isTrue = deger === true ? true : false;
        if (!(deger === true ? true : false)) {
            return { 'identificationNumberIsNotValid': true };
        }
        else {
            return null;
        }
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ValidationService.spaceControl = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        /** @type {?} */
        var value = control.value;
        value = String(value);
        /** @type {?} */
        var usingSpace = false;
        if (value === '' || value.indexOf(' ') > -1) {
            usingSpace = true;
        }
        if (usingSpace) {
            return { 'usingSpaceValid': true };
        }
        else {
            return null;
        }
    };
    return ValidationService;
}());
export { ValidationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS92YWxpZGF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBO0lBQUE7SUE4REEsQ0FBQzs7Ozs7SUE1RFEsNEJBQVU7Ozs7SUFBakIsVUFBa0IsT0FBd0I7O1lBQ3BDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSzs7O1lBQ3JCLE1BQU0sR0FBRyxLQUFLO1FBQ2xCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7OztZQUVoQixRQUFRLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7OztZQUdwQyxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JDLElBQUksVUFBVSxLQUFLLEdBQUcsRUFBRTtZQUN0QixNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNMLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDaEI7O1lBRUcsTUFBTSxHQUFHLENBQUM7UUFDZCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzNCLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0Qzs7O1lBRUssT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDOzs7WUFFdkQsT0FBTyxHQUFHLENBQUM7O1lBQ1gsT0FBTyxHQUFHLENBQUM7UUFFZixrQ0FBa0M7UUFDbEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzlCLE9BQU8sSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN2QztRQUVELG1DQUFtQztRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDN0IsT0FBTyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3ZDOztZQUVLLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7O1lBQ3ZFLEtBQUssR0FBRyxRQUFRLElBQUksQ0FBQyxNQUFNLElBQUksT0FBTyxJQUFJLE9BQU87UUFDdkQsK0NBQStDO1FBRS9DLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDcEMsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLElBQUksRUFBRSxDQUFDO1NBQ25EO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7SUFFTSw4QkFBWTs7OztJQUFuQixVQUFvQixPQUF3Qjs7WUFDdEMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLO1FBQ3pCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7O1lBQ2xCLFVBQVUsR0FBRyxLQUFLO1FBRXRCLElBQUksS0FBSyxLQUFLLEVBQUUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQzNDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDbkI7UUFDRCxJQUFJLFVBQVUsRUFBRTtZQUNkLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUNwQzthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFDSCx3QkFBQztBQUFELENBQUMsQUE5REQsSUE4REMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBWYWxpZGF0aW9uRXJyb3JzLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgVmFsaWRhdGlvblNlcnZpY2Uge1xyXG5cclxuICBzdGF0aWMgdGNWYWxpZGF0ZShjb250cm9sOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHwgbnVsbCB7XHJcbiAgICBsZXQgdmFsdWUgPSBjb250cm9sLnZhbHVlOyAvLyBldmVudC50YXJnZXQudmFsdWU7XHJcbiAgICBsZXQgaXNaZXJvID0gZmFsc2U7XHJcbiAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICAvLyBTYWRlY2UgcmFrYW1sYXJkYW4gb2x1xZ91cCBvbHXFn21hZMSxxJ/EsW7EsSB2ZSAxMSBoYW5lbGkgb2x1cCBvbG1hZMSxxJ/EsW7EsSBrb250cm9sIGVkaXlvcnV6LlxyXG4gICAgY29uc3QgaXNFbGV2ZW4gPSAvXlswLTldezExfSQvLnRlc3QodmFsdWUpO1xyXG5cclxuICAgIC8vIMSwbGsgcmFrYW3EsW4gMCBpbGUgYmHFn2xhbWEga29udHJvbMO8XHJcbiAgICBjb25zdCBmaXJzdFZhbHVlID0gdmFsdWUuc3Vic3RyKDAsIDEpO1xyXG4gICAgaWYgKGZpcnN0VmFsdWUgPT09ICcwJykge1xyXG4gICAgICBpc1plcm8gPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaXNaZXJvID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHRvdGFsWCA9IDA7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IDEwOyBpKyspIHtcclxuICAgICAgdG90YWxYICs9IE51bWJlcih2YWx1ZS5zdWJzdHIoaSwgMSkpO1xyXG4gICAgfVxyXG4gICAgLy8gxLBsayAxMCBoYW5lc2luaW4gdG9wbGFtxLFuxLFubiAxMS4gaGFuZXlpIHZlcm1lIGtvbnRyb2zDvFxyXG4gICAgY29uc3QgaXNSdWxlWCA9IFN0cmluZyh0b3RhbFggJSAxMCkgPT09IHZhbHVlLnN1YnN0cigxMCwgMSk7XHJcbiAgICAvLyB0ZWsgdmUgw6dpZnQgaGFuZWxlcmluIHRvcGxhbWxhcsSxbsSxbiBhdGFuYWNhxJ/EsSBkZWdpxZ9rZW5sZXJpIHRhbsSxbWzEsXlvcnV6XHJcbiAgICBsZXQgdG90YWxZMSA9IDA7XHJcbiAgICBsZXQgdG90YWxZMiA9IDA7XHJcblxyXG4gICAgLy8gdGVrIGhhbmVsZXJkZWtpIHNhecSxbGFyxLEgdG9wbGFyXHJcbiAgICBmb3IgKGxldCBqID0gMDsgaiA8IDEwOyBqICs9IDIpIHtcclxuICAgICAgdG90YWxZMSArPSBOdW1iZXIodmFsdWUuc3Vic3RyKGosIDEpKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyDDp2lmdCBoYW5lbGVyZGVraSBzYXnEsWxhcsSxIHRvcGxhclxyXG4gICAgZm9yIChsZXQgayA9IDE7IGsgPCA5OyBrICs9IDIpIHtcclxuICAgICAgdG90YWxZMiArPSBOdW1iZXIodmFsdWUuc3Vic3RyKGssIDEpKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBpc1J1bGVZID0gU3RyaW5nKCgodG90YWxZMSAqIDcpIC0gdG90YWxZMikgJSAxMCkgPT09IHZhbHVlLnN1YnN0cig5LCAxKTtcclxuICAgIGNvbnN0IGRlZ2VyID0gaXNFbGV2ZW4gJiYgIWlzWmVybyAmJiBpc1J1bGVYICYmIGlzUnVsZVk7XHJcbiAgICAvLyB0aGlzLmlzVHJ1ZSA9IGRlZ2VyID09PSB0cnVlID8gdHJ1ZSA6IGZhbHNlO1xyXG5cclxuICAgIGlmICghKGRlZ2VyID09PSB0cnVlID8gdHJ1ZSA6IGZhbHNlKSkge1xyXG4gICAgICByZXR1cm4geyAnaWRlbnRpZmljYXRpb25OdW1iZXJJc05vdFZhbGlkJzogdHJ1ZSB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgc3BhY2VDb250cm9sKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IFZhbGlkYXRpb25FcnJvcnMgfCBudWxsIHtcclxuICAgIGxldCB2YWx1ZSA9IGNvbnRyb2wudmFsdWU7XHJcbiAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICBsZXQgdXNpbmdTcGFjZSA9IGZhbHNlO1xyXG5cclxuICAgIGlmICh2YWx1ZSA9PT0gJycgfHwgdmFsdWUuaW5kZXhPZignICcpID4gLTEpIHtcclxuICAgICAgdXNpbmdTcGFjZSA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBpZiAodXNpbmdTcGFjZSkge1xyXG4gICAgICByZXR1cm4geyAndXNpbmdTcGFjZVZhbGlkJzogdHJ1ZSB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==