/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
var PreventUnsavedChangesService = /** @class */ (function () {
    function PreventUnsavedChangesService() {
        this.status = new Subject();
    }
    /**
     * @return {?}
     */
    PreventUnsavedChangesService.prototype.getIsChanged = /**
     * @return {?}
     */
    function () {
        return this.isChanged;
    };
    /**
     * @param {?} isChanged
     * @return {?}
     */
    PreventUnsavedChangesService.prototype.setIsChanged = /**
     * @param {?} isChanged
     * @return {?}
     */
    function (isChanged) {
        this.isChanged = isChanged;
    };
    /**
     * @return {?}
     */
    PreventUnsavedChangesService.prototype.getStatus = /**
     * @return {?}
     */
    function () {
        return this.status.asObservable();
    };
    /**
     * @param {?} status
     * @return {?}
     */
    PreventUnsavedChangesService.prototype.setStatus = /**
     * @param {?} status
     * @return {?}
     */
    function (status) {
        this.status.next(status);
    };
    PreventUnsavedChangesService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PreventUnsavedChangesService.ctorParameters = function () { return []; };
    return PreventUnsavedChangesService;
}());
export { PreventUnsavedChangesService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesService.prototype.isChanged;
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesService.prototype.status;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmVudC11bnNhdmVkLWNoYW5nZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9jdXN0b20vcHJldmVudC11bnNhdmVkLWNoYW5nZXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBRTNDO0lBTUU7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELG1EQUFZOzs7SUFBWjtRQUNFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDOzs7OztJQUVELG1EQUFZOzs7O0lBQVosVUFBYSxTQUFrQjtRQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztJQUM3QixDQUFDOzs7O0lBRUQsZ0RBQVM7OztJQUFUO1FBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BDLENBQUM7Ozs7O0lBRUQsZ0RBQVM7Ozs7SUFBVCxVQUFVLE1BQWM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Z0JBeEJGLFVBQVU7Ozs7SUF5QlgsbUNBQUM7Q0FBQSxBQXpCRCxJQXlCQztTQXhCWSw0QkFBNEI7Ozs7OztJQUN2QyxpREFBMkI7Ozs7O0lBRTNCLDhDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZSB7XHJcbiAgcHJpdmF0ZSBpc0NoYW5nZWQ6IGJvb2xlYW47XHJcbiAgLy8gJ3dhaXQsIGZpbmlzaGVkJ1xyXG4gIHByaXZhdGUgc3RhdHVzOiBTdWJqZWN0PHN0cmluZz47XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5zdGF0dXMgPSBuZXcgU3ViamVjdCgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0SXNDaGFuZ2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaXNDaGFuZ2VkO1xyXG4gIH1cclxuXHJcbiAgc2V0SXNDaGFuZ2VkKGlzQ2hhbmdlZDogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgdGhpcy5pc0NoYW5nZWQgPSBpc0NoYW5nZWQ7XHJcbiAgfVxyXG5cclxuICBnZXRTdGF0dXMoKTogT2JzZXJ2YWJsZTxzdHJpbmc+IHtcclxuICAgIHJldHVybiB0aGlzLnN0YXR1cy5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIHNldFN0YXR1cyhzdGF0dXM6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5zdGF0dXMubmV4dChzdGF0dXMpO1xyXG4gIH1cclxufVxyXG4iXX0=