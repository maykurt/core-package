/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as i0 from "@angular/core";
var CoreModalService = /** @class */ (function () {
    function CoreModalService() {
        this.isModalDisplayed = new BehaviorSubject(false);
    }
    /**
     * @return {?}
     */
    CoreModalService.prototype.getModalDisplayStatus = /**
     * @return {?}
     */
    function () {
        return this.isModalDisplayed.asObservable();
    };
    /**
     * @param {?} status
     * @return {?}
     */
    CoreModalService.prototype.setModalDisplayStatus = /**
     * @param {?} status
     * @return {?}
     */
    function (status) {
        this.isModalDisplayed.next(status || false);
    };
    CoreModalService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CoreModalService.ctorParameters = function () { return []; };
    /** @nocollapse */ CoreModalService.ngInjectableDef = i0.defineInjectable({ factory: function CoreModalService_Factory() { return new CoreModalService(); }, token: CoreModalService, providedIn: "root" });
    return CoreModalService;
}());
export { CoreModalService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CoreModalService.prototype.isModalDisplayed;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1tb2RhbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9jb3JlLW1vZGFsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDOztBQUd2RDtJQU1JO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7SUFFRCxnREFBcUI7OztJQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsZ0RBQXFCOzs7O0lBQXJCLFVBQXNCLE1BQWU7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Z0JBaEJKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7OzJCQU5EO0NBdUJDLEFBbkJELElBbUJDO1NBaEJZLGdCQUFnQjs7Ozs7O0lBQ3pCLDRDQUFtRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcy9CZWhhdmlvclN1YmplY3QnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZU1vZGFsU2VydmljZSB7XHJcbiAgICBwcml2YXRlIGlzTW9kYWxEaXNwbGF5ZWQ6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLmlzTW9kYWxEaXNwbGF5ZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNb2RhbERpc3BsYXlTdGF0dXMoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNNb2RhbERpc3BsYXllZC5hc09ic2VydmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRNb2RhbERpc3BsYXlTdGF0dXMoc3RhdHVzOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5pc01vZGFsRGlzcGxheWVkLm5leHQoc3RhdHVzIHx8IGZhbHNlKTtcclxuICAgIH1cclxuXHJcblxyXG59XHJcbiJdfQ==