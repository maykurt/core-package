/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { ConfirmDialogOperationType, AlertType } from '../../enums/index';
import { TranslateHelperService } from './translate.helper.service';
var SweetAlertService = /** @class */ (function () {
    function SweetAlertService(translate) {
        this.translate = translate;
        this._swal = Swal;
        // this.translate.setInitialLanguage('tr');
    }
    // openConfirmPopup() {
    //   const swalTitle = 'ConfirmDialogAreYouSure';
    //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
    //   const swalConfirmBtnText = 'ConfirmButtonText';
    //   const swalCancelBtnText = 'CancelModalButton';
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
    //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
    //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
    //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
    //   const swalCancelBtnText = 'CancelModalButton';
    //   const swalType = type || AlertType.Warning;
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
    // }
    // openConfirmPopup() {
    //   const swalTitle = 'ConfirmDialogAreYouSure';
    //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
    //   const swalConfirmBtnText = 'ConfirmButtonText';
    //   const swalCancelBtnText = 'CancelModalButton';
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
    //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
    //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
    //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
    //   const swalCancelBtnText = 'CancelModalButton';
    //   const swalType = type || AlertType.Warning;
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
    // }
    /**
     * @param {?=} title
     * @param {?=} text
     * @param {?=} type
     * @param {?=} confirmButtonText
     * @param {?=} showCancelButton
     * @return {?}
     */
    SweetAlertService.prototype.preventUnsavedChangedPopup = 
    // openConfirmPopup() {
    //   const swalTitle = 'ConfirmDialogAreYouSure';
    //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
    //   const swalConfirmBtnText = 'ConfirmButtonText';
    //   const swalCancelBtnText = 'CancelModalButton';
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
    //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
    //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
    //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
    //   const swalCancelBtnText = 'CancelModalButton';
    //   const swalType = type || AlertType.Warning;
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
    // }
    /**
     * @param {?=} title
     * @param {?=} text
     * @param {?=} type
     * @param {?=} confirmButtonText
     * @param {?=} showCancelButton
     * @return {?}
     */
    function (title, text, type, confirmButtonText, showCancelButton) {
        if (showCancelButton === void 0) { showCancelButton = true; }
        /** @type {?} */
        var swalTitle = title ? title : 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        var swalText = text ? text : 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        var swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        var swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning, showCancelButton);
    };
    /**
     * @param {?} confirmDialogOperationType
     * @param {?=} confirmDialogSettings
     * @return {?}
     */
    SweetAlertService.prototype.manageRequestOperationWithConfirm = /**
     * @param {?} confirmDialogOperationType
     * @param {?=} confirmDialogSettings
     * @return {?}
     */
    function (confirmDialogOperationType, confirmDialogSettings) {
        switch (confirmDialogOperationType) {
            case ConfirmDialogOperationType.Add:
                return this.getAddOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Update:
                return this.getUpdateOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Delete:
                return this.getDeleteOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Undo:
                return this.getUndoOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Cancel:
                return this.getCancelOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Refresh:
                return this.getRefreshOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Process:
                return this.getProcessOperationConfirmPopupWithConfirm(confirmDialogSettings);
            default:
                break;
        }
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getProcessOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogProcessCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogProcessInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogProcessOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogProcessCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getAddOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getUpdateOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getDeleteOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogDeleteRecordCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogDeleteRecordInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogDeleteRecordOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogDeleteRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getUndoOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogNotAbleToRevertRecordCapiton';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogNotAbleToRevertRecordInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogNotAbleToRevertRecordOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogNotAbleToRevertRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getCancelOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    };
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.getRefreshOperationConfirmPopupWithConfirm = /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    function (settings) {
        if (settings === void 0) { settings = {}; }
        /** @type {?} */
        var swalTitle = settings.Title || 'ConfirmDialogRefreshCaption';
        /** @type {?} */
        var swalText = settings.Text || 'ConfirmDialogRefreshInfo';
        /** @type {?} */
        var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogRefreshsOk';
        /** @type {?} */
        var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogRefreshCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    };
    /**
     * @param {?} result
     * @param {?=} settings
     * @return {?}
     */
    SweetAlertService.prototype.confirmOperationPopup = /**
     * @param {?} result
     * @param {?=} settings
     * @return {?}
     */
    function (result, settings) {
        if (settings === void 0) { settings = {}; }
        if (result.value) {
            /** @type {?} */
            var swalTitle = settings.title || 'Deleted!';
            /** @type {?} */
            var swalText = settings.text || 'Your file has been deleted.';
            /** @type {?} */
            var swalType = settings.type || AlertType.Success;
            return this.openCompletedConfirmDialog(swalTitle, swalText, swalType);
        }
        else if (result.dismiss === this._swal.DismissReason.cancel) {
            return this.openCompletedConfirmDialog('Cancelled', 'OperationCanceled', AlertType.Error);
        }
    };
    // loadingSweetAlert() {
    //   swal.showLoading();
    // }
    //
    // cancelLoadingSweetAlert() {
    //   swal.hideLoading();
    // }
    // cancelEditFormConfirmModal() {
    //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
    //   const swalText = 'ConfirmDialogLoseChangesInfo';
    //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
    //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // loadingSweetAlert() {
    //   swal.showLoading();
    // }
    //
    // cancelLoadingSweetAlert() {
    //   swal.hideLoading();
    // }
    // cancelEditFormConfirmModal() {
    //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
    //   const swalText = 'ConfirmDialogLoseChangesInfo';
    //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
    //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?=} swalType
     * @return {?}
     */
    SweetAlertService.prototype.openCompletedConfirmDialog = 
    // loadingSweetAlert() {
    //   swal.showLoading();
    // }
    //
    // cancelLoadingSweetAlert() {
    //   swal.hideLoading();
    // }
    // cancelEditFormConfirmModal() {
    //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
    //   const swalText = 'ConfirmDialogLoseChangesInfo';
    //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
    //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?=} swalType
     * @return {?}
     */
    function (swalTitle, swalText, swalType) {
        if (swalType === void 0) { swalType = AlertType.Info; }
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalType = swalType || AlertType.Info;
        return this.translate.get([swalTitle, swalText])
            .pipe(flatMap(function (translatedResult) { return from(Swal.fire(translatedResult[swalTitle], translatedResult[swalText], swalType)); }, function (translatedResult, result) {
            /** @type {?} */
            var alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    };
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?} swalConfirmBtnText
     * @param {?} swalCancelBtnText
     * @param {?} swalType
     * @param {?=} showCancelButton
     * @return {?}
     */
    SweetAlertService.prototype.openConfirmDialog = /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?} swalConfirmBtnText
     * @param {?} swalCancelBtnText
     * @param {?} swalType
     * @param {?=} showCancelButton
     * @return {?}
     */
    function (swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton) {
        if (showCancelButton === void 0) { showCancelButton = true; }
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalConfirmBtnText = swalConfirmBtnText || '';
        swalCancelBtnText = swalCancelBtnText || '';
        return this.translate.get([swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText])
            .pipe(flatMap(function (translatedResult) { return from(Swal.fire({
            title: translatedResult[swalTitle],
            text: translatedResult[swalText],
            type: swalType ? swalType : AlertType.Warning,
            confirmButtonText: translatedResult[swalConfirmBtnText],
            showCancelButton: showCancelButton,
            cancelButtonText: translatedResult[swalCancelBtnText],
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-info mr-20',
            cancelButtonClass: 'btn btn-danger',
            customClass: 'sweet-confirm-dialog',
        })); }, function (translatedResult, result) {
            /** @type {?} */
            var alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    };
    SweetAlertService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    SweetAlertService.ctorParameters = function () { return [
        { type: TranslateHelperService }
    ]; };
    return SweetAlertService;
}());
export { SweetAlertService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype._swal;
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype.swalOptions;
    /**
     * @type {?}
     * @private
     */
    SweetAlertService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dlZXQuYWxlcnQuY29uZmlybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9zd2VldC5hbGVydC5jb25maXJtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsT0FBTyxJQUFJLE1BQU0sYUFBYSxDQUFDO0FBRS9CLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUIsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pDLE9BQU8sRUFBc0IsMEJBQTBCLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFOUYsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFcEU7SUFNRSwyQkFBb0IsU0FBaUM7UUFBakMsY0FBUyxHQUFULFNBQVMsQ0FBd0I7UUFIN0MsVUFBSyxHQUFRLElBQUksQ0FBQztRQUl4QiwyQ0FBMkM7SUFDN0MsQ0FBQztJQUVELHVCQUF1QjtJQUN2QixpREFBaUQ7SUFDakQsMkRBQTJEO0lBQzNELG9EQUFvRDtJQUNwRCxtREFBbUQ7SUFDbkQsRUFBRTtJQUNGLGtIQUFrSDtJQUNsSCxJQUFJO0lBRUosNkhBQTZIO0lBQzdILGlFQUFpRTtJQUNqRSwrREFBK0Q7SUFDL0QsZ0dBQWdHO0lBQ2hHLG1EQUFtRDtJQUNuRCxnREFBZ0Q7SUFDaEQsRUFBRTtJQUNGLDJIQUEySDtJQUMzSCxJQUFJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUVKLHNEQUEwQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFBMUIsVUFBMkIsS0FBYyxFQUFFLElBQWEsRUFBRSxJQUFnQixFQUFFLGlCQUEwQixFQUFFLGdCQUF1QjtRQUF2QixpQ0FBQSxFQUFBLHVCQUF1Qjs7WUFDdkgsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7O1lBQzdELFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsOEJBQThCOztZQUN2RCxrQkFBa0IsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLDRCQUE0Qjs7WUFDekYsaUJBQWlCLEdBQUcsZ0NBQWdDO1FBQzFELE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pJLENBQUM7Ozs7OztJQUVELDZEQUFpQzs7Ozs7SUFBakMsVUFBa0MsMEJBQXNELEVBQUUscUJBQTZDO1FBQ3JJLFFBQVEsMEJBQTBCLEVBQUU7WUFDbEMsS0FBSywwQkFBMEIsQ0FBQyxHQUFHO2dCQUNqQyxPQUFPLElBQUksQ0FBQyxzQ0FBc0MsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQzVFLEtBQUssMEJBQTBCLENBQUMsTUFBTTtnQkFDcEMsT0FBTyxJQUFJLENBQUMseUNBQXlDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvRSxLQUFLLDBCQUEwQixDQUFDLE1BQU07Z0JBQ3BDLE9BQU8sSUFBSSxDQUFDLHlDQUF5QyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsS0FBSywwQkFBMEIsQ0FBQyxJQUFJO2dCQUNsQyxPQUFPLElBQUksQ0FBQyx1Q0FBdUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLEtBQUssMEJBQTBCLENBQUMsTUFBTTtnQkFDcEMsT0FBTyxJQUFJLENBQUMseUNBQXlDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvRSxLQUFLLDBCQUEwQixDQUFDLE9BQU87Z0JBQ3JDLE9BQU8sSUFBSSxDQUFDLDBDQUEwQyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDaEYsS0FBSywwQkFBMEIsQ0FBQyxPQUFPO2dCQUNyQyxPQUFPLElBQUksQ0FBQywwQ0FBMEMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hGO2dCQUNFLE1BQU07U0FDVDtJQUNILENBQUM7Ozs7OztJQUVPLHNFQUEwQzs7Ozs7SUFBbEQsVUFBbUQsUUFBb0M7UUFBcEMseUJBQUEsRUFBQSxhQUFvQzs7WUFDL0UsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksNkJBQTZCOztZQUMzRCxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksSUFBSSwwQkFBMEI7O1lBQ3RELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLElBQUksd0JBQXdCOztZQUN4RSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsYUFBYSxJQUFJLDRCQUE0QjtRQUNoRixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1RyxDQUFDOzs7Ozs7SUFFTyxrRUFBc0M7Ozs7O0lBQTlDLFVBQStDLFFBQW9DO1FBQXBDLHlCQUFBLEVBQUEsYUFBb0M7O1lBQzNFLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxJQUFJLGdDQUFnQzs7WUFDOUQsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLElBQUksNkJBQTZCOztZQUN6RCxrQkFBa0IsR0FBRyxRQUFRLENBQUMsY0FBYyxJQUFJLDJCQUEyQjs7WUFDM0UsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLGFBQWEsSUFBSSwrQkFBK0I7UUFFbkYsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUcsQ0FBQzs7Ozs7O0lBRU8scUVBQXlDOzs7OztJQUFqRCxVQUFrRCxRQUFvQztRQUFwQyx5QkFBQSxFQUFBLGFBQW9DOztZQUM5RSxTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssSUFBSSxnQ0FBZ0M7O1lBQzlELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDZCQUE2Qjs7WUFDekQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLGNBQWMsSUFBSSwyQkFBMkI7O1lBQzNFLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLElBQUksK0JBQStCO1FBRW5GLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVHLENBQUM7Ozs7OztJQUVPLHFFQUF5Qzs7Ozs7SUFBakQsVUFBa0QsUUFBb0M7UUFBcEMseUJBQUEsRUFBQSxhQUFvQzs7WUFDOUUsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksa0NBQWtDOztZQUNoRSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksSUFBSSwrQkFBK0I7O1lBQzNELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLElBQUksNkJBQTZCOztZQUM3RSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsYUFBYSxJQUFJLGlDQUFpQztRQUNyRixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRyxDQUFDOzs7Ozs7SUFFTyxtRUFBdUM7Ozs7O0lBQS9DLFVBQWdELFFBQW9DO1FBQXBDLHlCQUFBLEVBQUEsYUFBb0M7O1lBQzVFLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxJQUFJLDJDQUEyQzs7WUFDekUsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLElBQUksd0NBQXdDOztZQUNwRSxrQkFBa0IsR0FBRyxRQUFRLENBQUMsY0FBYyxJQUFJLHNDQUFzQzs7WUFDdEYsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLGFBQWEsSUFBSSwwQ0FBMEM7UUFDOUYsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0csQ0FBQzs7Ozs7O0lBRU8scUVBQXlDOzs7OztJQUFqRCxVQUFrRCxRQUFvQztRQUFwQyx5QkFBQSxFQUFBLGFBQW9DOztZQUM5RSxTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssSUFBSSxpQ0FBaUM7O1lBQy9ELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDhCQUE4Qjs7WUFDMUQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLGNBQWMsSUFBSSw0QkFBNEI7O1lBQzVFLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxhQUFhLElBQUksZ0NBQWdDO1FBQ3BGLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9HLENBQUM7Ozs7OztJQUVPLHNFQUEwQzs7Ozs7SUFBbEQsVUFBbUQsUUFBb0M7UUFBcEMseUJBQUEsRUFBQSxhQUFvQzs7WUFDL0UsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksNkJBQTZCOztZQUMzRCxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksSUFBSSwwQkFBMEI7O1lBQ3RELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLElBQUkseUJBQXlCOztZQUN6RSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsYUFBYSxJQUFJLDRCQUE0QjtRQUNoRixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRyxDQUFDOzs7Ozs7SUFFRCxpREFBcUI7Ozs7O0lBQXJCLFVBQXNCLE1BQU0sRUFBRSxRQUFrQjtRQUFsQix5QkFBQSxFQUFBLGFBQWtCO1FBQzlDLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTs7Z0JBQ1YsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksVUFBVTs7Z0JBQ3hDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLDZCQUE2Qjs7Z0JBQ3pELFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxPQUFPO1lBRW5ELE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FFdkU7YUFBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQzdELE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0Y7SUFDSCxDQUFDO0lBRUQsd0JBQXdCO0lBQ3hCLHdCQUF3QjtJQUN4QixJQUFJO0lBQ0osRUFBRTtJQUNGLDhCQUE4QjtJQUM5Qix3QkFBd0I7SUFDeEIsSUFBSTtJQUVKLGlDQUFpQztJQUNqQyx5REFBeUQ7SUFDekQscURBQXFEO0lBQ3JELDZEQUE2RDtJQUM3RCxnRUFBZ0U7SUFDaEUsa0hBQWtIO0lBQ2xILElBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFFSSxzREFBMEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFBbEMsVUFBbUMsU0FBaUIsRUFBRSxRQUFnQixFQUFFLFFBQW9DO1FBQXBDLHlCQUFBLEVBQUEsV0FBc0IsU0FBUyxDQUFDLElBQUk7UUFDMUcsU0FBUyxHQUFHLFNBQVMsSUFBSSxFQUFFLENBQUM7UUFDNUIsUUFBUSxHQUFHLFFBQVEsSUFBSSxFQUFFLENBQUM7UUFDMUIsUUFBUSxHQUFHLFFBQVEsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLGdCQUFnQixJQUFJLE9BQUEsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLEVBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUMsRUFBbEYsQ0FBa0YsRUFDbEgsVUFBQyxnQkFBZ0IsRUFBRSxNQUF3Qjs7Z0JBQ25DLFdBQVcsR0FBZ0IsRUFBRTtZQUNuQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hCLFdBQVcsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzthQUNsQztZQUNELElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsV0FBVyxDQUFDLE9BQU8sR0FBRyxtQkFBb0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBQSxDQUFDO2FBQ3JFO1lBQ0QsT0FBTyxXQUFXLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7Ozs7O0lBRU8sNkNBQWlCOzs7Ozs7Ozs7O0lBQXpCLFVBQ0UsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsa0JBQTBCLEVBQzFCLGlCQUF5QixFQUN6QixRQUFtQixFQUNuQixnQkFBZ0M7UUFBaEMsaUNBQUEsRUFBQSx1QkFBZ0M7UUFDaEMsU0FBUyxHQUFHLFNBQVMsSUFBSSxFQUFFLENBQUM7UUFDNUIsUUFBUSxHQUFHLFFBQVEsSUFBSSxFQUFFLENBQUM7UUFDMUIsa0JBQWtCLEdBQUcsa0JBQWtCLElBQUksRUFBRSxDQUFDO1FBQzlDLGlCQUFpQixHQUFHLGlCQUFpQixJQUFJLEVBQUUsQ0FBQztRQUU1QyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2FBQ3BGLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxnQkFBZ0IsSUFBSSxPQUFBLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQy9DLEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7WUFDbEMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUNoQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPO1lBQzdDLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO1lBQ3ZELGdCQUFnQixFQUFFLGdCQUFnQjtZQUNsQyxnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQztZQUNyRCxjQUFjLEVBQUUsS0FBSztZQUNyQixrQkFBa0IsRUFBRSxvQkFBb0I7WUFDeEMsaUJBQWlCLEVBQUUsZ0JBQWdCO1lBQ25DLFdBQVcsRUFBRSxzQkFBc0I7U0FDcEMsQ0FBQyxDQUFDLEVBWCtCLENBVy9CLEVBQUUsVUFBQyxnQkFBZ0IsRUFBRSxNQUF3Qjs7Z0JBQ3hDLFdBQVcsR0FBZ0IsRUFBRTtZQUNuQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hCLFdBQVcsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzthQUNsQztZQUNELElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsV0FBVyxDQUFDLE9BQU8sR0FBRyxtQkFBb0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBQSxDQUFDO2FBQ3JFO1lBQ0QsT0FBTyxXQUFXLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7O2dCQXJNRixVQUFVOzs7O2dCQUZGLHNCQUFzQjs7SUF3TS9CLHdCQUFDO0NBQUEsQUF0TUQsSUFzTUM7U0FyTVksaUJBQWlCOzs7Ozs7SUFFNUIsa0NBQTBCOzs7OztJQUMxQix3Q0FBeUI7Ozs7O0lBRWIsc0NBQXlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWxlcnRSZXN1bHQgfSBmcm9tICcuLi8uLi9tb2RlbHMvYWxlcnQtcmVzdWx0Lm1vZGVsJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTd2VldEFsZXJ0UmVzdWx0IH0gZnJvbSAnc3dlZXRhbGVydDInO1xyXG5cclxuaW1wb3J0IFN3YWwgZnJvbSAnc3dlZXRhbGVydDInO1xyXG5cclxuaW1wb3J0IHsgZnJvbSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBmbGF0TWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBBbGVydERpc21pc3NSZWFzb24sIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLCBBbGVydFR5cGUgfSBmcm9tICcuLi8uLi9lbnVtcy9pbmRleCc7XHJcbmltcG9ydCB7IENvbmZpcm1EaWFsb2dTZXR0aW5ncyB9IGZyb20gJy4uLy4uL21vZGVscy9jb25maXJtLWRpYWxvZy1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZUhlbHBlclNlcnZpY2UgfSBmcm9tICcuL3RyYW5zbGF0ZS5oZWxwZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTd2VldEFsZXJ0U2VydmljZSB7XHJcblxyXG4gIHByaXZhdGUgX3N3YWw6IGFueSA9IFN3YWw7XHJcbiAgcHJpdmF0ZSBzd2FsT3B0aW9uczogYW55O1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlSGVscGVyU2VydmljZSkge1xyXG4gICAgLy8gdGhpcy50cmFuc2xhdGUuc2V0SW5pdGlhbExhbmd1YWdlKCd0cicpO1xyXG4gIH1cclxuXHJcbiAgLy8gb3BlbkNvbmZpcm1Qb3B1cCgpIHtcclxuICAvLyAgIGNvbnN0IHN3YWxUaXRsZSA9ICdDb25maXJtRGlhbG9nQXJlWW91U3VyZSc7XHJcbiAgLy8gICBjb25zdCBzd2FsVGV4dCA9ICdDb25maXJtRGlhbG9nTm90QWJsZVRvUmV2ZXJ0UmVjb3JkJztcclxuICAvLyAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9ICdDb25maXJtQnV0dG9uVGV4dCc7XHJcbiAgLy8gICBjb25zdCBzd2FsQ2FuY2VsQnRuVGV4dCA9ICdDYW5jZWxNb2RhbEJ1dHRvbic7XHJcbiAgLy9cclxuICAvLyAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nKTtcclxuICAvLyB9XHJcblxyXG4gIC8vIG9wZW5EaXJ0eUZvcm1Qb3B1cCh0aXRsZT86IHN0cmluZywgdGV4dD86IHN0cmluZywgdHlwZT86IEFsZXJ0VHlwZSwgY29uZmlybUJ1dHRvblRleHQ/OiBzdHJpbmcsIHNob3dDYW5jZWxCdXR0b24gPSB0cnVlKSB7XHJcbiAgLy8gICBjb25zdCBzd2FsVGl0bGUgPSB0aXRsZSA/IHRpdGxlIDogJ0NvbmZpcm1EaWFsb2dBcmVZb3VTdXJlJztcclxuICAvLyAgIGNvbnN0IHN3YWxUZXh0ID0gdGV4dCA/IHRleHQgOiAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzJztcclxuICAvLyAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IGNvbmZpcm1CdXR0b25UZXh0ID8gY29uZmlybUJ1dHRvblRleHQgOiAnQ29uZmlybUJ1dHRvbkNvbnRpbnVlJztcclxuICAvLyAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gJ0NhbmNlbE1vZGFsQnV0dG9uJztcclxuICAvLyAgIGNvbnN0IHN3YWxUeXBlID0gdHlwZSB8fCBBbGVydFR5cGUuV2FybmluZztcclxuICAvL1xyXG4gIC8vICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgc3dhbFR5cGUsIHNob3dDYW5jZWxCdXR0b24pO1xyXG4gIC8vIH1cclxuXHJcbiAgcHJldmVudFVuc2F2ZWRDaGFuZ2VkUG9wdXAodGl0bGU/OiBzdHJpbmcsIHRleHQ/OiBzdHJpbmcsIHR5cGU/OiBBbGVydFR5cGUsIGNvbmZpcm1CdXR0b25UZXh0Pzogc3RyaW5nLCBzaG93Q2FuY2VsQnV0dG9uID0gdHJ1ZSkge1xyXG4gICAgY29uc3Qgc3dhbFRpdGxlID0gdGl0bGUgPyB0aXRsZSA6ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gdGV4dCA/IHRleHQgOiAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzSW5mbyc7XHJcbiAgICBjb25zdCBzd2FsQ29uZmlybUJ0blRleHQgPSBjb25maXJtQnV0dG9uVGV4dCA/IGNvbmZpcm1CdXR0b25UZXh0IDogJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc09rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc0NhbmNlbCc7XHJcbiAgICByZXR1cm4gdGhpcy5vcGVuQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0LCBBbGVydFR5cGUuV2FybmluZywgc2hvd0NhbmNlbEJ1dHRvbik7XHJcbiAgfVxyXG5cclxuICBtYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ09wZXJhdGlvblR5cGU6IENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLCBjb25maXJtRGlhbG9nU2V0dGluZ3M/OiBDb25maXJtRGlhbG9nU2V0dGluZ3MpIHtcclxuICAgIHN3aXRjaCAoY29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUpIHtcclxuICAgICAgY2FzZSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5BZGQ6XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0QWRkT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ1NldHRpbmdzKTtcclxuICAgICAgY2FzZSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5VcGRhdGU6XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VXBkYXRlT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ1NldHRpbmdzKTtcclxuICAgICAgY2FzZSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5EZWxldGU6XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RGVsZXRlT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ1NldHRpbmdzKTtcclxuICAgICAgY2FzZSBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5VbmRvOlxyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFVuZG9PcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLkNhbmNlbDpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRDYW5jZWxPcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShjb25maXJtRGlhbG9nU2V0dGluZ3MpO1xyXG4gICAgICBjYXNlIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlJlZnJlc2g6XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UmVmcmVzaE9wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKGNvbmZpcm1EaWFsb2dTZXR0aW5ncyk7XHJcbiAgICAgIGNhc2UgQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuUHJvY2VzczpcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRQcm9jZXNzT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oY29uZmlybURpYWxvZ1NldHRpbmdzKTtcclxuICAgICAgZGVmYXVsdDpcclxuICAgICAgICBicmVhaztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0UHJvY2Vzc09wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKHNldHRpbmdzOiBDb25maXJtRGlhbG9nU2V0dGluZ3MgPSB7fSkge1xyXG4gICAgY29uc3Qgc3dhbFRpdGxlID0gc2V0dGluZ3MuVGl0bGUgfHwgJ0NvbmZpcm1EaWFsb2dQcm9jZXNzQ2FwdGlvbic7XHJcbiAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLlRleHQgfHwgJ0NvbmZpcm1EaWFsb2dQcm9jZXNzSW5mbyc7XHJcbiAgICBjb25zdCBzd2FsQ29uZmlybUJ0blRleHQgPSBzZXR0aW5ncy5Db25maXJtQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1Byb2Nlc3NPayc7XHJcbiAgICBjb25zdCBzd2FsQ2FuY2VsQnRuVGV4dCA9IHNldHRpbmdzLkNhbmNlbEJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dQcm9jZXNzQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5JbmZvKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0QWRkT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZE9rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gc2V0dGluZ3MuQ2FuY2VsQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRDYW5jZWwnO1xyXG5cclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5JbmZvKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0VXBkYXRlT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRDYXB0aW9uJztcclxuICAgIGNvbnN0IHN3YWxUZXh0ID0gc2V0dGluZ3MuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nU2F2ZVJlY29yZE9rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gc2V0dGluZ3MuQ2FuY2VsQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1NhdmVSZWNvcmRDYW5jZWwnO1xyXG5cclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5JbmZvKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0RGVsZXRlT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ0RlbGV0ZVJlY29yZENhcHRpb24nO1xyXG4gICAgY29uc3Qgc3dhbFRleHQgPSBzZXR0aW5ncy5UZXh0IHx8ICdDb25maXJtRGlhbG9nRGVsZXRlUmVjb3JkSW5mbyc7XHJcbiAgICBjb25zdCBzd2FsQ29uZmlybUJ0blRleHQgPSBzZXR0aW5ncy5Db25maXJtQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ0RlbGV0ZVJlY29yZE9rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gc2V0dGluZ3MuQ2FuY2VsQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ0RlbGV0ZVJlY29yZENhbmNlbCc7XHJcbiAgICByZXR1cm4gdGhpcy5vcGVuQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0LCBBbGVydFR5cGUuV2FybmluZyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldFVuZG9PcGVyYXRpb25Db25maXJtUG9wdXBXaXRoQ29uZmlybShzZXR0aW5nczogQ29uZmlybURpYWxvZ1NldHRpbmdzID0ge30pIHtcclxuICAgIGNvbnN0IHN3YWxUaXRsZSA9IHNldHRpbmdzLlRpdGxlIHx8ICdDb25maXJtRGlhbG9nTm90QWJsZVRvUmV2ZXJ0UmVjb3JkQ2FwaXRvbic7XHJcbiAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLlRleHQgfHwgJ0NvbmZpcm1EaWFsb2dOb3RBYmxlVG9SZXZlcnRSZWNvcmRJbmZvJztcclxuICAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9IHNldHRpbmdzLkNvbmZpcm1CdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nTm90QWJsZVRvUmV2ZXJ0UmVjb3JkT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nTm90QWJsZVRvUmV2ZXJ0UmVjb3JkQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0Q2FuY2VsT3BlcmF0aW9uQ29uZmlybVBvcHVwV2l0aENvbmZpcm0oc2V0dGluZ3M6IENvbmZpcm1EaWFsb2dTZXR0aW5ncyA9IHt9KSB7XHJcbiAgICBjb25zdCBzd2FsVGl0bGUgPSBzZXR0aW5ncy5UaXRsZSB8fCAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzQ2FwdGlvbic7XHJcbiAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLlRleHQgfHwgJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc0luZm8nO1xyXG4gICAgY29uc3Qgc3dhbENvbmZpcm1CdG5UZXh0ID0gc2V0dGluZ3MuQ29uZmlybUJ0blRleHQgfHwgJ0NvbmZpcm1EaWFsb2dMb3NlQ2hhbmdlc09rJztcclxuICAgIGNvbnN0IHN3YWxDYW5jZWxCdG5UZXh0ID0gc2V0dGluZ3MuQ2FuY2VsQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzQ2FuY2VsJztcclxuICAgIHJldHVybiB0aGlzLm9wZW5Db25maXJtRGlhbG9nKHN3YWxUaXRsZSwgc3dhbFRleHQsIHN3YWxDb25maXJtQnRuVGV4dCwgc3dhbENhbmNlbEJ0blRleHQsIEFsZXJ0VHlwZS5XYXJuaW5nKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0UmVmcmVzaE9wZXJhdGlvbkNvbmZpcm1Qb3B1cFdpdGhDb25maXJtKHNldHRpbmdzOiBDb25maXJtRGlhbG9nU2V0dGluZ3MgPSB7fSkge1xyXG4gICAgY29uc3Qgc3dhbFRpdGxlID0gc2V0dGluZ3MuVGl0bGUgfHwgJ0NvbmZpcm1EaWFsb2dSZWZyZXNoQ2FwdGlvbic7XHJcbiAgICBjb25zdCBzd2FsVGV4dCA9IHNldHRpbmdzLlRleHQgfHwgJ0NvbmZpcm1EaWFsb2dSZWZyZXNoSW5mbyc7XHJcbiAgICBjb25zdCBzd2FsQ29uZmlybUJ0blRleHQgPSBzZXR0aW5ncy5Db25maXJtQnRuVGV4dCB8fCAnQ29uZmlybURpYWxvZ1JlZnJlc2hzT2snO1xyXG4gICAgY29uc3Qgc3dhbENhbmNlbEJ0blRleHQgPSBzZXR0aW5ncy5DYW5jZWxCdG5UZXh0IHx8ICdDb25maXJtRGlhbG9nUmVmcmVzaENhbmNlbCc7XHJcbiAgICByZXR1cm4gdGhpcy5vcGVuQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0LCBBbGVydFR5cGUuV2FybmluZyk7XHJcbiAgfVxyXG5cclxuICBjb25maXJtT3BlcmF0aW9uUG9wdXAocmVzdWx0LCBzZXR0aW5nczogYW55ID0ge30pIHtcclxuICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgY29uc3Qgc3dhbFRpdGxlID0gc2V0dGluZ3MudGl0bGUgfHwgJ0RlbGV0ZWQhJztcclxuICAgICAgY29uc3Qgc3dhbFRleHQgPSBzZXR0aW5ncy50ZXh0IHx8ICdZb3VyIGZpbGUgaGFzIGJlZW4gZGVsZXRlZC4nO1xyXG4gICAgICBjb25zdCBzd2FsVHlwZSA9IHNldHRpbmdzLnR5cGUgfHwgQWxlcnRUeXBlLlN1Y2Nlc3M7XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5vcGVuQ29tcGxldGVkQ29uZmlybURpYWxvZyhzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsVHlwZSk7XHJcblxyXG4gICAgfSBlbHNlIGlmIChyZXN1bHQuZGlzbWlzcyA9PT0gdGhpcy5fc3dhbC5EaXNtaXNzUmVhc29uLmNhbmNlbCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5vcGVuQ29tcGxldGVkQ29uZmlybURpYWxvZygnQ2FuY2VsbGVkJywgJ09wZXJhdGlvbkNhbmNlbGVkJywgQWxlcnRUeXBlLkVycm9yKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIGxvYWRpbmdTd2VldEFsZXJ0KCkge1xyXG4gIC8vICAgc3dhbC5zaG93TG9hZGluZygpO1xyXG4gIC8vIH1cclxuICAvL1xyXG4gIC8vIGNhbmNlbExvYWRpbmdTd2VldEFsZXJ0KCkge1xyXG4gIC8vICAgc3dhbC5oaWRlTG9hZGluZygpO1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gY2FuY2VsRWRpdEZvcm1Db25maXJtTW9kYWwoKSB7XHJcbiAgLy8gICBjb25zdCBzd2FsVGl0bGUgPSAnQ29uZmlybURpYWxvZ0xvc2VDaGFuZ2VzQ2FwdGlvbic7XHJcbiAgLy8gICBjb25zdCBzd2FsVGV4dCA9ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNJbmZvJztcclxuICAvLyAgIGNvbnN0IHN3YWxDb25maXJtQnRuVGV4dCA9ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNPayc7XHJcbiAgLy8gICBjb25zdCBzd2FsQ2FuY2VsQnRuVGV4dCA9ICdDb25maXJtRGlhbG9nTG9zZUNoYW5nZXNDYW5jZWwnO1xyXG4gIC8vICAgcmV0dXJuIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coc3dhbFRpdGxlLCBzd2FsVGV4dCwgc3dhbENvbmZpcm1CdG5UZXh0LCBzd2FsQ2FuY2VsQnRuVGV4dCwgQWxlcnRUeXBlLldhcm5pbmcpO1xyXG4gIC8vIH1cclxuXHJcbiAgcHJpdmF0ZSBvcGVuQ29tcGxldGVkQ29uZmlybURpYWxvZyhzd2FsVGl0bGU6IHN0cmluZywgc3dhbFRleHQ6IHN0cmluZywgc3dhbFR5cGU6IEFsZXJ0VHlwZSA9IEFsZXJ0VHlwZS5JbmZvKSB7XHJcbiAgICBzd2FsVGl0bGUgPSBzd2FsVGl0bGUgfHwgJyc7XHJcbiAgICBzd2FsVGV4dCA9IHN3YWxUZXh0IHx8ICcnO1xyXG4gICAgc3dhbFR5cGUgPSBzd2FsVHlwZSB8fCBBbGVydFR5cGUuSW5mbztcclxuICAgIHJldHVybiB0aGlzLnRyYW5zbGF0ZS5nZXQoW3N3YWxUaXRsZSwgc3dhbFRleHRdKVxyXG4gICAgICAucGlwZShmbGF0TWFwKHRyYW5zbGF0ZWRSZXN1bHQgPT4gZnJvbShTd2FsLmZpcmUodHJhbnNsYXRlZFJlc3VsdFtzd2FsVGl0bGVdLCB0cmFuc2xhdGVkUmVzdWx0W3N3YWxUZXh0XSwgc3dhbFR5cGUpKSxcclxuICAgICAgICAodHJhbnNsYXRlZFJlc3VsdCwgcmVzdWx0OiBTd2VldEFsZXJ0UmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBhbGVydFJlc3VsdDogQWxlcnRSZXN1bHQgPSB7fTtcclxuICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgYWxlcnRSZXN1bHQudmFsdWUgPSByZXN1bHQudmFsdWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAocmVzdWx0LmRpc21pc3MpIHtcclxuICAgICAgICAgICAgYWxlcnRSZXN1bHQuZGlzbWlzcyA9IDxBbGVydERpc21pc3NSZWFzb24+cmVzdWx0LmRpc21pc3MudG9TdHJpbmcoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBhbGVydFJlc3VsdDtcclxuICAgICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIG9wZW5Db25maXJtRGlhbG9nKFxyXG4gICAgc3dhbFRpdGxlOiBzdHJpbmcsXHJcbiAgICBzd2FsVGV4dDogc3RyaW5nLFxyXG4gICAgc3dhbENvbmZpcm1CdG5UZXh0OiBzdHJpbmcsXHJcbiAgICBzd2FsQ2FuY2VsQnRuVGV4dDogc3RyaW5nLFxyXG4gICAgc3dhbFR5cGU6IEFsZXJ0VHlwZSxcclxuICAgIHNob3dDYW5jZWxCdXR0b246IGJvb2xlYW4gPSB0cnVlKSB7XHJcbiAgICBzd2FsVGl0bGUgPSBzd2FsVGl0bGUgfHwgJyc7XHJcbiAgICBzd2FsVGV4dCA9IHN3YWxUZXh0IHx8ICcnO1xyXG4gICAgc3dhbENvbmZpcm1CdG5UZXh0ID0gc3dhbENvbmZpcm1CdG5UZXh0IHx8ICcnO1xyXG4gICAgc3dhbENhbmNlbEJ0blRleHQgPSBzd2FsQ2FuY2VsQnRuVGV4dCB8fCAnJztcclxuXHJcbiAgICByZXR1cm4gdGhpcy50cmFuc2xhdGUuZ2V0KFtzd2FsVGl0bGUsIHN3YWxUZXh0LCBzd2FsQ29uZmlybUJ0blRleHQsIHN3YWxDYW5jZWxCdG5UZXh0XSlcclxuICAgICAgLnBpcGUoZmxhdE1hcCh0cmFuc2xhdGVkUmVzdWx0ID0+IGZyb20oU3dhbC5maXJlKHtcclxuICAgICAgICB0aXRsZTogdHJhbnNsYXRlZFJlc3VsdFtzd2FsVGl0bGVdLFxyXG4gICAgICAgIHRleHQ6IHRyYW5zbGF0ZWRSZXN1bHRbc3dhbFRleHRdLFxyXG4gICAgICAgIHR5cGU6IHN3YWxUeXBlID8gc3dhbFR5cGUgOiBBbGVydFR5cGUuV2FybmluZyxcclxuICAgICAgICBjb25maXJtQnV0dG9uVGV4dDogdHJhbnNsYXRlZFJlc3VsdFtzd2FsQ29uZmlybUJ0blRleHRdLFxyXG4gICAgICAgIHNob3dDYW5jZWxCdXR0b246IHNob3dDYW5jZWxCdXR0b24sXHJcbiAgICAgICAgY2FuY2VsQnV0dG9uVGV4dDogdHJhbnNsYXRlZFJlc3VsdFtzd2FsQ2FuY2VsQnRuVGV4dF0sXHJcbiAgICAgICAgYnV0dG9uc1N0eWxpbmc6IGZhbHNlLFxyXG4gICAgICAgIGNvbmZpcm1CdXR0b25DbGFzczogJ2J0biBidG4taW5mbyBtci0yMCcsXHJcbiAgICAgICAgY2FuY2VsQnV0dG9uQ2xhc3M6ICdidG4gYnRuLWRhbmdlcicsXHJcbiAgICAgICAgY3VzdG9tQ2xhc3M6ICdzd2VldC1jb25maXJtLWRpYWxvZycsXHJcbiAgICAgIH0pKSwgKHRyYW5zbGF0ZWRSZXN1bHQsIHJlc3VsdDogU3dlZXRBbGVydFJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGFsZXJ0UmVzdWx0OiBBbGVydFJlc3VsdCA9IHt9O1xyXG4gICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgIGFsZXJ0UmVzdWx0LnZhbHVlID0gcmVzdWx0LnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAocmVzdWx0LmRpc21pc3MpIHtcclxuICAgICAgICAgIGFsZXJ0UmVzdWx0LmRpc21pc3MgPSA8QWxlcnREaXNtaXNzUmVhc29uPnJlc3VsdC5kaXNtaXNzLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhbGVydFJlc3VsdDtcclxuICAgICAgfSkpO1xyXG4gIH1cclxufVxyXG4iXX0=