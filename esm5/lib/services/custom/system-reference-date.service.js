/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StorageService } from './storage.service';
import { Storage } from '../../enums/storage.enum';
import { StorageType } from '../../enums/storage-type.enum';
var SystemReferenceDateService = /** @class */ (function () {
    function SystemReferenceDateService(storageService) {
        this.storageService = storageService;
        this.systemDate = '';
        this.dateChange = new BehaviorSubject('');
    }
    /**
     * @param {?} newDate
     * @return {?}
     */
    SystemReferenceDateService.prototype.setSelectedReferenceDate = /**
     * @param {?} newDate
     * @return {?}
     */
    function (newDate) {
        this.systemDate = newDate;
        this.storageService.store(Storage.SystemRefDate, newDate, StorageType.SessionStorage);
        this.dateChange.next(newDate);
    };
    /**
     * @return {?}
     */
    SystemReferenceDateService.prototype.checkIsNullAndChangeCurrentDateWithStorage = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var systemDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (systemDate && systemDate !== this.systemDate) {
            this.systemDate = systemDate;
            this.dateChange.next(systemDate);
        }
        else if (!systemDate) {
            return true;
        }
        return false;
    };
    /**
     * @return {?}
     */
    SystemReferenceDateService.prototype.getSelectedReferenceDate = /**
     * @return {?}
     */
    function () {
        return this.dateChange.asObservable();
    };
    /**
     * @return {?}
     */
    SystemReferenceDateService.prototype.getSystemDate = /**
     * @return {?}
     */
    function () {
        return this.systemDate;
    };
    /**
     * @return {?}
     */
    SystemReferenceDateService.prototype.getCurrentDateAsISO = /**
     * @return {?}
     */
    function () {
        return this.convertDateToISO(new Date);
    };
    /**
     * @param {?} date
     * @return {?}
     */
    SystemReferenceDateService.prototype.convertDateToISO = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        /** @type {?} */
        var newDate = new Date(date);
        /** @type {?} */
        var year = newDate.getFullYear();
        /** @type {?} */
        var month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        var day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    };
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    SystemReferenceDateService.prototype.convertDateToDateUTC = /**
     * @param {?} dateAsString
     * @return {?}
     */
    function (dateAsString) {
        /** @type {?} */
        var newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    };
    SystemReferenceDateService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    SystemReferenceDateService.ctorParameters = function () { return [
        { type: StorageService }
    ]; };
    return SystemReferenceDateService;
}());
export { SystemReferenceDateService };
if (false) {
    /** @type {?} */
    SystemReferenceDateService.prototype.dateChange;
    /** @type {?} */
    SystemReferenceDateService.prototype.systemDate;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXJlZmVyZW5jZS1kYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY3VzdG9tL3N5c3RlbS1yZWZlcmVuY2UtZGF0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUV2RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUc1RDtJQU1FLG9DQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDaEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBUyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7OztJQUVELDZEQUF3Qjs7OztJQUF4QixVQUF5QixPQUFlO1FBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO1FBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsT0FBTyxFQUFFLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN0RixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsK0VBQTBDOzs7SUFBMUM7O1lBQ1EsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLGNBQWMsQ0FBQztRQUNuRyxJQUFJLFVBQVUsSUFBSSxVQUFVLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDdEIsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELDZEQUF3Qjs7O0lBQXhCO1FBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFRCxrREFBYTs7O0lBQWI7UUFDRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELHdEQUFtQjs7O0lBQW5CO1FBQ0UsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVELHFEQUFnQjs7OztJQUFoQixVQUFpQixJQUFVOztZQUNuQixPQUFPLEdBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDOztZQUNsQyxJQUFJLEdBQUcsT0FBTyxDQUFDLFdBQVcsRUFBRTs7WUFDMUIsS0FBSyxHQUFHLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7O1lBQ3ZDLEdBQUcsR0FBRyxFQUFFLEdBQUcsT0FBTyxDQUFDLE9BQU8sRUFBRTtRQUU5QixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLEtBQUssR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNsQixHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztTQUNqQjtRQUVELE9BQU8sQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUVELHlEQUFvQjs7OztJQUFwQixVQUFxQixZQUFvQjs7WUFDakMsT0FBTyxHQUFTLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM1QyxPQUFPLElBQUksSUFBSSxDQUNiLE9BQU8sQ0FBQyxjQUFjLEVBQUUsRUFDeEIsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUNyQixPQUFPLENBQUMsVUFBVSxFQUFFLEVBQ3BCLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFDckIsT0FBTyxDQUFDLGFBQWEsRUFBRSxFQUN2QixPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUM3QixDQUFDOztnQkFqRUYsVUFBVTs7OztnQkFMRixjQUFjOztJQXVFdkIsaUNBQUM7Q0FBQSxBQWxFRCxJQWtFQztTQWpFWSwwQkFBMEI7OztJQUVyQyxnREFBMkM7O0lBQzNDLGdEQUEwQjs7Ozs7SUFFZCxvREFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMvQmVoYXZpb3JTdWJqZWN0JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdG9yYWdlIH0gZnJvbSAnLi4vLi4vZW51bXMvc3RvcmFnZS5lbnVtJztcclxuaW1wb3J0IHsgU3RvcmFnZVR5cGUgfSBmcm9tICcuLi8uLi9lbnVtcy9zdG9yYWdlLXR5cGUuZW51bSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgU3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2Uge1xyXG5cclxuICBwdWJsaWMgZGF0ZUNoYW5nZTogQmVoYXZpb3JTdWJqZWN0PHN0cmluZz47XHJcbiAgcHVibGljIHN5c3RlbURhdGU6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgIHRoaXMuc3lzdGVtRGF0ZSA9ICcnO1xyXG4gICAgdGhpcy5kYXRlQ2hhbmdlID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KCcnKTtcclxuICB9XHJcblxyXG4gIHNldFNlbGVjdGVkUmVmZXJlbmNlRGF0ZShuZXdEYXRlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuc3lzdGVtRGF0ZSA9IG5ld0RhdGU7XHJcbiAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnN0b3JlKFN0b3JhZ2UuU3lzdGVtUmVmRGF0ZSwgbmV3RGF0ZSwgU3RvcmFnZVR5cGUuU2Vzc2lvblN0b3JhZ2UpO1xyXG4gICAgdGhpcy5kYXRlQ2hhbmdlLm5leHQobmV3RGF0ZSk7XHJcbiAgfVxyXG5cclxuICBjaGVja0lzTnVsbEFuZENoYW5nZUN1cnJlbnREYXRlV2l0aFN0b3JhZ2UoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBzeXN0ZW1EYXRlID0gdGhpcy5zdG9yYWdlU2VydmljZS5nZXRTdG9yZWQoU3RvcmFnZS5TeXN0ZW1SZWZEYXRlLCBTdG9yYWdlVHlwZS5TZXNzaW9uU3RvcmFnZSk7XHJcbiAgICBpZiAoc3lzdGVtRGF0ZSAmJiBzeXN0ZW1EYXRlICE9PSB0aGlzLnN5c3RlbURhdGUpIHtcclxuICAgICAgdGhpcy5zeXN0ZW1EYXRlID0gc3lzdGVtRGF0ZTtcclxuICAgICAgdGhpcy5kYXRlQ2hhbmdlLm5leHQoc3lzdGVtRGF0ZSk7XHJcbiAgICB9IGVsc2UgaWYgKCFzeXN0ZW1EYXRlKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2VsZWN0ZWRSZWZlcmVuY2VEYXRlKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5kYXRlQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0U3lzdGVtRGF0ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLnN5c3RlbURhdGU7XHJcbiAgfVxyXG5cclxuICBnZXRDdXJyZW50RGF0ZUFzSVNPKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5jb252ZXJ0RGF0ZVRvSVNPKG5ldyBEYXRlKTtcclxuICB9XHJcblxyXG4gIGNvbnZlcnREYXRlVG9JU08oZGF0ZTogRGF0ZSk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBuZXdEYXRlOiBEYXRlID0gbmV3IERhdGUoZGF0ZSksXHJcbiAgICAgIHllYXIgPSBuZXdEYXRlLmdldEZ1bGxZZWFyKCk7XHJcbiAgICBsZXQgbW9udGggPSAnJyArIChuZXdEYXRlLmdldE1vbnRoKCkgKyAxKSxcclxuICAgICAgZGF5ID0gJycgKyBuZXdEYXRlLmdldERhdGUoKTtcclxuXHJcbiAgICBpZiAobW9udGgubGVuZ3RoIDwgMikge1xyXG4gICAgICBtb250aCA9ICcwJyArIG1vbnRoO1xyXG4gICAgfVxyXG4gICAgaWYgKGRheS5sZW5ndGggPCAyKSB7XHJcbiAgICAgIGRheSA9ICcwJyArIGRheTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gW3llYXIsIG1vbnRoLCBkYXldLmpvaW4oJy0nKSArICdUMDA6MDA6MDAuMDAwWic7XHJcbiAgfVxyXG5cclxuICBjb252ZXJ0RGF0ZVRvRGF0ZVVUQyhkYXRlQXNTdHJpbmc6IHN0cmluZyk6IERhdGUge1xyXG4gICAgY29uc3QgbmV3RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKGRhdGVBc1N0cmluZyk7XHJcbiAgICByZXR1cm4gbmV3IERhdGUoXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDRnVsbFllYXIoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENNb250aCgpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ0RhdGUoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENIb3VycygpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ01pbnV0ZXMoKSxcclxuICAgICAgbmV3RGF0ZS5nZXRVVENTZWNvbmRzKCkpO1xyXG4gIH1cclxufVxyXG4iXX0=