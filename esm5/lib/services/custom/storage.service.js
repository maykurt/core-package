/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/share';
import { StorageType } from '../../enums/storage-type.enum';
var StorageService = /** @class */ (function () {
    function StorageService() {
        this.onSubject = new Subject();
        this.changes = this.onSubject.asObservable().share();
        this.start();
    }
    /**
     * @return {?}
     */
    StorageService.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.stop();
    };
    /**
     * @param {?=} storageType
     * @return {?}
     */
    StorageService.prototype.getStorage = /**
     * @param {?=} storageType
     * @return {?}
     */
    function (storageType) {
        /** @type {?} */
        var storage = storageType || StorageType.LocalStorage;
        /** @type {?} */
        var s = [];
        for (var i = 0; i < localStorage.length; i++) {
            s.push({
                key: storage === StorageType.LocalStorage ? localStorage.key(i) : sessionStorage.key(i),
                value: storage === StorageType.LocalStorage ?
                    JSON.parse(localStorage.getItem(localStorage.key(i))) : JSON.parse(localStorage.getItem(sessionStorage.key(i)))
            });
        }
        return s;
    };
    /**
     * @param {?} key
     * @param {?} data
     * @param {?=} storageType
     * @return {?}
     */
    StorageService.prototype.store = /**
     * @param {?} key
     * @param {?} data
     * @param {?=} storageType
     * @return {?}
     */
    function (key, data, storageType) {
        /** @type {?} */
        var storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.setItem(key, JSON.stringify(data));
        }
        else {
            sessionStorage.setItem(key, JSON.stringify(data));
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: data });
    };
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    StorageService.prototype.getStored = /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    function (key, storageType) {
        /** @type {?} */
        var storage = storageType || StorageType.LocalStorage;
        try {
            if (storage === StorageType.LocalStorage) {
                return JSON.parse(localStorage.getItem(key));
            }
            else {
                return JSON.parse(sessionStorage.getItem(key));
            }
        }
        catch (e) {
            if (storage === StorageType.LocalStorage) {
                return localStorage.getItem(key);
            }
            else {
                return sessionStorage.getItem(key);
            }
        }
    };
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    StorageService.prototype.clear = /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    function (key, storageType) {
        /** @type {?} */
        var storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.removeItem(key);
        }
        else {
            sessionStorage.removeItem(key);
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: null });
    };
    /**
     * @private
     * @return {?}
     */
    StorageService.prototype.start = /**
     * @private
     * @return {?}
     */
    function () {
        window.addEventListener('storage', this.storageEventListener.bind(this));
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    StorageService.prototype.storageEventListener = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.storageArea == localStorage) {
            /** @type {?} */
            var v = void 0;
            try {
                v = JSON.parse(event.newValue);
            }
            catch (e) {
                v = event.newValue;
            }
            this.onSubject.next({ key: event.key, value: v });
        }
    };
    /**
     * @private
     * @return {?}
     */
    StorageService.prototype.stop = /**
     * @private
     * @return {?}
     */
    function () {
        window.removeEventListener('storage', this.storageEventListener.bind(this));
        this.onSubject.complete();
    };
    StorageService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    StorageService.ctorParameters = function () { return []; };
    return StorageService;
}());
export { StorageService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.onSubject;
    /** @type {?} */
    StorageService.prototype.changes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9zdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN2QyxPQUFPLHlCQUF5QixDQUFDO0FBRWpDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUU1RDtJQUtFO1FBSFEsY0FBUyxHQUFHLElBQUksT0FBTyxFQUErQixDQUFDO1FBQ3hELFlBQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBR3JELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7Ozs7SUFFRCxvQ0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDOzs7OztJQUVNLG1DQUFVOzs7O0lBQWpCLFVBQWtCLFdBQXlCOztZQUNyQyxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTs7WUFDOUQsQ0FBQyxHQUFHLEVBQUU7UUFDVixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNMLEdBQUcsRUFBRSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLEtBQUssRUFBRSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUMzQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEgsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7Ozs7Ozs7SUFFTSw4QkFBSzs7Ozs7O0lBQVosVUFBYSxHQUFXLEVBQUUsSUFBUyxFQUFFLFdBQXlCOztZQUN4RCxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTtRQUNsRSxJQUFJLE9BQU8sS0FBSyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQ3hDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0wsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ25EO1FBRUQseUVBQXlFO1FBQ3pFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7SUFFTSxrQ0FBUzs7Ozs7SUFBaEIsVUFBaUIsR0FBVyxFQUFFLFdBQXlCOztZQUNqRCxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTtRQUNsRSxJQUFJO1lBQ0YsSUFBSSxPQUFPLEtBQUssV0FBVyxDQUFDLFlBQVksRUFBRTtnQkFDeEMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUM5QztpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2hEO1NBQ0Y7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLElBQUksT0FBTyxLQUFLLFdBQVcsQ0FBQyxZQUFZLEVBQUU7Z0JBQ3hDLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEM7U0FDRjtJQUNILENBQUM7Ozs7OztJQUVNLDhCQUFLOzs7OztJQUFaLFVBQWEsR0FBRyxFQUFFLFdBQXlCOztZQUNyQyxPQUFPLEdBQWdCLFdBQVcsSUFBSSxXQUFXLENBQUMsWUFBWTtRQUNsRSxJQUFJLE9BQU8sS0FBSyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQ3hDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDOUI7YUFBTTtZQUNMLGNBQWMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7UUFFRCx5RUFBeUU7UUFDekUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7O0lBR08sOEJBQUs7Ozs7SUFBYjtRQUNFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7Ozs7OztJQUVPLDZDQUFvQjs7Ozs7SUFBNUIsVUFBNkIsS0FBbUI7UUFDOUMsSUFBSSxLQUFLLENBQUMsV0FBVyxJQUFJLFlBQVksRUFBRTs7Z0JBQ2pDLENBQUMsU0FBQTtZQUNMLElBQUk7Z0JBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQUU7WUFDdkMsT0FBTyxDQUFDLEVBQUU7Z0JBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7YUFBRTtZQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ25EO0lBQ0gsQ0FBQzs7Ozs7SUFFTyw2QkFBSTs7OztJQUFaO1FBQ0UsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM1QixDQUFDOztnQkFwRkYsVUFBVTs7OztJQXFGWCxxQkFBQztDQUFBLEFBckZELElBcUZDO1NBcEZZLGNBQWM7Ozs7OztJQUN6QixtQ0FBK0Q7O0lBQy9ELGlDQUF1RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcy9TdWJqZWN0JztcclxuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9zaGFyZSc7XHJcblxyXG5pbXBvcnQgeyBTdG9yYWdlVHlwZSB9IGZyb20gJy4uLy4uL2VudW1zL3N0b3JhZ2UtdHlwZS5lbnVtJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN0b3JhZ2VTZXJ2aWNlIGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuICBwcml2YXRlIG9uU3ViamVjdCA9IG5ldyBTdWJqZWN0PHsga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkgfT4oKTtcclxuICBwdWJsaWMgY2hhbmdlcyA9IHRoaXMub25TdWJqZWN0LmFzT2JzZXJ2YWJsZSgpLnNoYXJlKCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5zdGFydCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnN0b3AoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTdG9yYWdlKHN0b3JhZ2VUeXBlPzogU3RvcmFnZVR5cGUpIHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIGxldCBzID0gW107XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxvY2FsU3RvcmFnZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICBzLnB1c2goe1xyXG4gICAgICAgIGtleTogc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlID8gbG9jYWxTdG9yYWdlLmtleShpKSA6IHNlc3Npb25TdG9yYWdlLmtleShpKSxcclxuICAgICAgICB2YWx1ZTogc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlID9cclxuICAgICAgICAgIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0obG9jYWxTdG9yYWdlLmtleShpKSkpIDogSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShzZXNzaW9uU3RvcmFnZS5rZXkoaSkpKVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBzO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0b3JlKGtleTogc3RyaW5nLCBkYXRhOiBhbnksIHN0b3JhZ2VUeXBlPzogU3RvcmFnZVR5cGUpOiB2b2lkIHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oa2V5LCBKU09OLnN0cmluZ2lmeShkYXRhKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKGtleSwgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHRoZSBsb2NhbCBhcHBsaWNhdGlvbiBkb2Vzbid0IHNlZW0gdG8gY2F0Y2ggY2hhbmdlcyB0byBsb2NhbFN0b3JhZ2UuLi5cclxuICAgIHRoaXMub25TdWJqZWN0Lm5leHQoeyBrZXk6IGtleSwgdmFsdWU6IGRhdGEgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0U3RvcmVkKGtleTogc3RyaW5nLCBzdG9yYWdlVHlwZT86IFN0b3JhZ2VUeXBlKTogYW55IHtcclxuICAgIGxldCBzdG9yYWdlOiBTdG9yYWdlVHlwZSA9IHN0b3JhZ2VUeXBlIHx8IFN0b3JhZ2VUeXBlLkxvY2FsU3RvcmFnZTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSkpO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIGlmIChzdG9yYWdlID09PSBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xlYXIoa2V5LCBzdG9yYWdlVHlwZT86IFN0b3JhZ2VUeXBlKSB7XHJcbiAgICBsZXQgc3RvcmFnZTogU3RvcmFnZVR5cGUgPSBzdG9yYWdlVHlwZSB8fCBTdG9yYWdlVHlwZS5Mb2NhbFN0b3JhZ2U7XHJcbiAgICBpZiAoc3RvcmFnZSA9PT0gU3RvcmFnZVR5cGUuTG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdGhlIGxvY2FsIGFwcGxpY2F0aW9uIGRvZXNuJ3Qgc2VlbSB0byBjYXRjaCBjaGFuZ2VzIHRvIGxvY2FsU3RvcmFnZS4uLlxyXG4gICAgdGhpcy5vblN1YmplY3QubmV4dCh7IGtleToga2V5LCB2YWx1ZTogbnVsbCB9KTtcclxuICB9XHJcblxyXG5cclxuICBwcml2YXRlIHN0YXJ0KCk6IHZvaWQge1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3N0b3JhZ2UnLCB0aGlzLnN0b3JhZ2VFdmVudExpc3RlbmVyLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdG9yYWdlRXZlbnRMaXN0ZW5lcihldmVudDogU3RvcmFnZUV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQuc3RvcmFnZUFyZWEgPT0gbG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgIGxldCB2O1xyXG4gICAgICB0cnkgeyB2ID0gSlNPTi5wYXJzZShldmVudC5uZXdWYWx1ZSk7IH1cclxuICAgICAgY2F0Y2ggKGUpIHsgdiA9IGV2ZW50Lm5ld1ZhbHVlOyB9XHJcbiAgICAgIHRoaXMub25TdWJqZWN0Lm5leHQoeyBrZXk6IGV2ZW50LmtleSwgdmFsdWU6IHYgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHN0b3AoKTogdm9pZCB7XHJcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc3RvcmFnZScsIHRoaXMuc3RvcmFnZUV2ZW50TGlzdGVuZXIuYmluZCh0aGlzKSk7XHJcbiAgICB0aGlzLm9uU3ViamVjdC5jb21wbGV0ZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=