/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateHelperService } from './translate.helper.service';
// declare let toastr: any;
var ToastrUtilsService = /** @class */ (function () {
    function ToastrUtilsService(toastr, translate) {
        this.toastr = toastr;
        this.translate = translate;
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    ToastrUtilsService.prototype.success = /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    function (message, title) {
        this.translateValue(message, 'success', title);
    };
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    ToastrUtilsService.prototype.info = /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    function (message, title) {
        this.translateValue(message, 'info', title);
    };
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    ToastrUtilsService.prototype.warning = /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    function (message, title) {
        this.translateValue(message, 'warning', title);
    };
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    ToastrUtilsService.prototype.error = /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    function (message, title) {
        this.translateValue(message, 'error', title);
    };
    /**
     * @param {?} message
     * @param {?} messageType
     * @param {?=} title
     * @return {?}
     */
    ToastrUtilsService.prototype.translateValue = /**
     * @param {?} message
     * @param {?} messageType
     * @param {?=} title
     * @return {?}
     */
    function (message, messageType, title) {
        var _this = this;
        title = title || '';
        message = message || '';
        this.translate.get([message, title]).subscribe(function (translatedResult) {
            if (messageType === 'success') {
                _this.toastr.success(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'info') {
                _this.toastr.info(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'warning') {
                _this.toastr.warning(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'error') {
                _this.toastr.error(translatedResult[message] || message, translatedResult[title] || title);
            }
        }, function (error) {
        });
    };
    ToastrUtilsService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ToastrUtilsService.ctorParameters = function () { return [
        { type: ToastrService },
        { type: TranslateHelperService }
    ]; };
    return ToastrUtilsService;
}());
export { ToastrUtilsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ToastrUtilsService.prototype.toastr;
    /**
     * @type {?}
     * @private
     */
    ToastrUtilsService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3RyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvY3VzdG9tL3RvYXN0ci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDM0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7O0FBSXBFO0lBRUUsNEJBQW9CLE1BQXFCLEVBQVUsU0FBaUM7UUFBaEUsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQXdCO0lBQ3BGLENBQUM7Ozs7OztJQUVELG9DQUFPOzs7OztJQUFQLFVBQVEsT0FBZSxFQUFFLEtBQWM7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7OztJQUVELGlDQUFJOzs7OztJQUFKLFVBQUssT0FBZSxFQUFFLEtBQWM7UUFDbEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7OztJQUVELG9DQUFPOzs7OztJQUFQLFVBQVEsT0FBZSxFQUFFLEtBQWM7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7OztJQUVELGtDQUFLOzs7OztJQUFMLFVBQU0sT0FBZSxFQUFFLEtBQWM7UUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7Ozs7SUFFRCwyQ0FBYzs7Ozs7O0lBQWQsVUFBZSxPQUFlLEVBQUUsV0FBbUIsRUFBRSxLQUFjO1FBQW5FLGlCQWVDO1FBZEMsS0FBSyxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDcEIsT0FBTyxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxnQkFBZ0I7WUFDOUQsSUFBSSxXQUFXLEtBQUssU0FBUyxFQUFFO2dCQUM3QixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUM7YUFDN0Y7aUJBQU0sSUFBSSxXQUFXLEtBQUssTUFBTSxFQUFFO2dCQUNqQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUM7YUFDMUY7aUJBQU0sSUFBSSxXQUFXLEtBQUssU0FBUyxFQUFFO2dCQUNwQyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUM7YUFDN0Y7aUJBQU0sSUFBSSxXQUFXLEtBQUssT0FBTyxFQUFFO2dCQUNsQyxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUM7YUFDM0Y7UUFDSCxDQUFDLEVBQUUsVUFBQyxLQUFLO1FBQ1QsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkFwQ0YsVUFBVTs7OztnQkFMRixhQUFhO2dCQUNiLHNCQUFzQjs7SUF5Qy9CLHlCQUFDO0NBQUEsQUFyQ0QsSUFxQ0M7U0FwQ1ksa0JBQWtCOzs7Ozs7SUFDakIsb0NBQTZCOzs7OztJQUFFLHVDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVG9hc3RyU2VydmljZSB9IGZyb20gJ25neC10b2FzdHInO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi90cmFuc2xhdGUuaGVscGVyLnNlcnZpY2UnO1xyXG5cclxuLy8gZGVjbGFyZSBsZXQgdG9hc3RyOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBUb2FzdHJVdGlsc1NlcnZpY2Uge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdG9hc3RyOiBUb2FzdHJTZXJ2aWNlLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlSGVscGVyU2VydmljZSkge1xyXG4gIH1cclxuXHJcbiAgc3VjY2VzcyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICdzdWNjZXNzJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgaW5mbyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICdpbmZvJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgd2FybmluZyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVZhbHVlKG1lc3NhZ2UsICd3YXJuaW5nJywgdGl0bGUpO1xyXG4gIH1cclxuXHJcbiAgZXJyb3IobWVzc2FnZTogc3RyaW5nLCB0aXRsZT86IHN0cmluZykge1xyXG4gICAgdGhpcy50cmFuc2xhdGVWYWx1ZShtZXNzYWdlLCAnZXJyb3InLCB0aXRsZSk7XHJcbiAgfVxyXG5cclxuICB0cmFuc2xhdGVWYWx1ZShtZXNzYWdlOiBzdHJpbmcsIG1lc3NhZ2VUeXBlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nKSB7XHJcbiAgICB0aXRsZSA9IHRpdGxlIHx8ICcnO1xyXG4gICAgbWVzc2FnZSA9IG1lc3NhZ2UgfHwgJyc7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoW21lc3NhZ2UsIHRpdGxlXSkuc3Vic2NyaWJlKCh0cmFuc2xhdGVkUmVzdWx0KSA9PiB7XHJcbiAgICAgIGlmIChtZXNzYWdlVHlwZSA9PT0gJ3N1Y2Nlc3MnKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIuc3VjY2Vzcyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ2luZm8nKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIuaW5mbyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ3dhcm5pbmcnKSB7XHJcbiAgICAgICAgdGhpcy50b2FzdHIud2FybmluZyh0cmFuc2xhdGVkUmVzdWx0W21lc3NhZ2VdIHx8IG1lc3NhZ2UsIHRyYW5zbGF0ZWRSZXN1bHRbdGl0bGVdIHx8IHRpdGxlKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXNzYWdlVHlwZSA9PT0gJ2Vycm9yJykge1xyXG4gICAgICAgIHRoaXMudG9hc3RyLmVycm9yKHRyYW5zbGF0ZWRSZXN1bHRbbWVzc2FnZV0gfHwgbWVzc2FnZSwgdHJhbnNsYXRlZFJlc3VsdFt0aXRsZV0gfHwgdGl0bGUpO1xyXG4gICAgICB9XHJcbiAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=