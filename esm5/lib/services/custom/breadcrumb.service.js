/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var BreadcrumbService = /** @class */ (function () {
    function BreadcrumbService() {
        this.breadcrumbsWithCustomLabels = [];
    }
    /**
     * @param {?} label
     * @param {?} url
     * @param {?} customLabel
     * @return {?}
     */
    BreadcrumbService.prototype.addCustomLabel = /**
     * @param {?} label
     * @param {?} url
     * @param {?} customLabel
     * @return {?}
     */
    function (label, url, customLabel) {
        /** @type {?} */
        var indexOfBreadcrumbItem = this.breadcrumbsWithCustomLabels.findIndex(function (x) { return x.label === label && x.url === url; });
        if (indexOfBreadcrumbItem !== -1) {
            this.breadcrumbsWithCustomLabels[indexOfBreadcrumbItem]['customLabel'] = customLabel;
        }
        else {
            /** @type {?} */
            var breadcrumb = {
                label: label,
                url: url,
                customLabel: customLabel
            };
            this.breadcrumbsWithCustomLabels.push(breadcrumb);
        }
        // update the current list after send
        if (this.breadcrumbs) {
            /** @type {?} */
            var currentBreadcrumbItem = this.breadcrumbs.find(function (x) { return x.label === label && x.url === url; });
            if (currentBreadcrumbItem) {
                currentBreadcrumbItem['customLabel'] = customLabel;
            }
        }
    };
    BreadcrumbService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BreadcrumbService.ctorParameters = function () { return []; };
    /** @nocollapse */ BreadcrumbService.ngInjectableDef = i0.defineInjectable({ factory: function BreadcrumbService_Factory() { return new BreadcrumbService(); }, token: BreadcrumbService, providedIn: "root" });
    return BreadcrumbService;
}());
export { BreadcrumbService };
if (false) {
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbs;
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbsWithCustomLabels;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N1c3RvbS9icmVhZGNydW1iLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRTNDO0lBUUU7UUFDRSxJQUFJLENBQUMsMkJBQTJCLEdBQUcsRUFBRSxDQUFDO0lBQ3hDLENBQUM7Ozs7Ozs7SUFFRCwwQ0FBYzs7Ozs7O0lBQWQsVUFBZSxLQUFhLEVBQUUsR0FBVyxFQUFFLFdBQW1COztZQUN0RCxxQkFBcUIsR0FBVyxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHLEVBQWxDLENBQWtDLENBQUM7UUFDekgsSUFBSSxxQkFBcUIsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsMkJBQTJCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxhQUFhLENBQUMsR0FBRyxXQUFXLENBQUM7U0FDdEY7YUFBTTs7Z0JBQ0MsVUFBVSxHQUFRO2dCQUN0QixLQUFLLE9BQUE7Z0JBQ0wsR0FBRyxLQUFBO2dCQUNILFdBQVcsYUFBQTthQUNaO1lBQ0QsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNuRDtRQUNELHFDQUFxQztRQUNyQyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7O2dCQUNkLHFCQUFxQixHQUFRLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHLEVBQWxDLENBQWtDLENBQUM7WUFDakcsSUFBSSxxQkFBcUIsRUFBRTtnQkFDekIscUJBQXFCLENBQUMsYUFBYSxDQUFDLEdBQUcsV0FBVyxDQUFDO2FBQ3BEO1NBQ0Y7SUFDSCxDQUFDOztnQkEvQkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7NEJBSkQ7Q0FrQ0MsQUFoQ0QsSUFnQ0M7U0E3QlksaUJBQWlCOzs7SUFDNUIsd0NBQTBCOztJQUUxQix3REFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iU2VydmljZSB7XHJcbiAgcHVibGljIGJyZWFkY3J1bWJzOiBhbnlbXTtcclxuXHJcbiAgcHVibGljIGJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVsczogYW55W107XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5icmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbHMgPSBbXTtcclxuICB9XHJcblxyXG4gIGFkZEN1c3RvbUxhYmVsKGxhYmVsOiBzdHJpbmcsIHVybDogc3RyaW5nLCBjdXN0b21MYWJlbDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICBjb25zdCBpbmRleE9mQnJlYWRjcnVtYkl0ZW06IG51bWJlciA9IHRoaXMuYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWxzLmZpbmRJbmRleCh4ID0+IHgubGFiZWwgPT09IGxhYmVsICYmIHgudXJsID09PSB1cmwpO1xyXG4gICAgaWYgKGluZGV4T2ZCcmVhZGNydW1iSXRlbSAhPT0gLTEpIHtcclxuICAgICAgdGhpcy5icmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbHNbaW5kZXhPZkJyZWFkY3J1bWJJdGVtXVsnY3VzdG9tTGFiZWwnXSA9IGN1c3RvbUxhYmVsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc3QgYnJlYWRjcnVtYjogYW55ID0ge1xyXG4gICAgICAgIGxhYmVsLFxyXG4gICAgICAgIHVybCxcclxuICAgICAgICBjdXN0b21MYWJlbFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmJyZWFkY3J1bWJzV2l0aEN1c3RvbUxhYmVscy5wdXNoKGJyZWFkY3J1bWIpO1xyXG4gICAgfVxyXG4gICAgLy8gdXBkYXRlIHRoZSBjdXJyZW50IGxpc3QgYWZ0ZXIgc2VuZFxyXG4gICAgaWYgKHRoaXMuYnJlYWRjcnVtYnMpIHtcclxuICAgICAgY29uc3QgY3VycmVudEJyZWFkY3J1bWJJdGVtOiBhbnkgPSB0aGlzLmJyZWFkY3J1bWJzLmZpbmQoeCA9PiB4LmxhYmVsID09PSBsYWJlbCAmJiB4LnVybCA9PT0gdXJsKTtcclxuICAgICAgaWYgKGN1cnJlbnRCcmVhZGNydW1iSXRlbSkge1xyXG4gICAgICAgIGN1cnJlbnRCcmVhZGNydW1iSXRlbVsnY3VzdG9tTGFiZWwnXSA9IGN1c3RvbUxhYmVsO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==