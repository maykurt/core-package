/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { map, concatMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { SweetAlertService } from '../custom/sweet.alert.confirm.service';
import { PreventUnsavedChangesService } from '../custom/prevent-unsaved-changes.service';
import * as i0 from "@angular/core";
import * as i1 from "../custom/sweet.alert.confirm.service";
import * as i2 from "../custom/prevent-unsaved-changes.service";
var PreventUnsavedChangesGuard = /** @class */ (function () {
    function PreventUnsavedChangesGuard(sweetAlert, preventUnsavedChangesService) {
        this.sweetAlert = sweetAlert;
        this.preventUnsavedChangesService = preventUnsavedChangesService;
    }
    /**
     * @return {?}
     */
    PreventUnsavedChangesGuard.prototype.canDeactivate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        try {
            setTimeout(function () {
                _this.preventUnsavedChangesService.setStatus('waiting');
            }, 100);
            return this.preventUnsavedChangesService.getStatus()
                .pipe(concatMap(function (result) {
                if (result === 'finished') {
                    /** @type {?} */
                    var isChanged = _this.preventUnsavedChangesService.getIsChanged();
                    if (isChanged) {
                        return _this.sweetAlert.preventUnsavedChangedPopup()
                            .pipe(map(function (alertResult) {
                            // console.log('canDeactivate in pipe', alertResult);
                            if (alertResult.value) {
                                _this.preventUnsavedChangesService.setIsChanged(false);
                                return true;
                            }
                            else {
                                return false;
                            }
                        }));
                    }
                    else {
                        _this.preventUnsavedChangesService.setIsChanged(false);
                        return of(true);
                    }
                }
                // return result;
            }), catchError(function (error) {
                console.error(error);
                return of(true);
            }));
        }
        catch (err) {
            console.error(err);
            return true;
        }
    };
    PreventUnsavedChangesGuard.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PreventUnsavedChangesGuard.ctorParameters = function () { return [
        { type: SweetAlertService },
        { type: PreventUnsavedChangesService }
    ]; };
    /** @nocollapse */ PreventUnsavedChangesGuard.ngInjectableDef = i0.defineInjectable({ factory: function PreventUnsavedChangesGuard_Factory() { return new PreventUnsavedChangesGuard(i0.inject(i1.SweetAlertService), i0.inject(i2.PreventUnsavedChangesService)); }, token: PreventUnsavedChangesGuard, providedIn: "root" });
    return PreventUnsavedChangesGuard;
}());
export { PreventUnsavedChangesGuard };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesGuard.prototype.sweetAlert;
    /**
     * @type {?}
     * @private
     */
    PreventUnsavedChangesGuard.prototype.preventUnsavedChangesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmVudC11bnNhdmVkLWNoYW5nZXMuZ3VhcmQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvZ3VhcmRzL3ByZXZlbnQtdW5zYXZlZC1jaGFuZ2VzLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzVELE9BQU8sRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFHMUIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDMUUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sMkNBQTJDLENBQUM7Ozs7QUFHekY7SUFLRSxvQ0FBb0IsVUFBNkIsRUFBVSw0QkFBMEQ7UUFBakcsZUFBVSxHQUFWLFVBQVUsQ0FBbUI7UUFBVSxpQ0FBNEIsR0FBNUIsNEJBQTRCLENBQThCO0lBQ3JILENBQUM7Ozs7SUFFRCxrREFBYTs7O0lBQWI7UUFBQSxpQkFxQ0M7UUFwQ0MsSUFBSTtZQUNGLFVBQVUsQ0FBQztnQkFDVCxLQUFJLENBQUMsNEJBQTRCLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUVSLE9BQU8sSUFBSSxDQUFDLDRCQUE0QixDQUFDLFNBQVMsRUFBRTtpQkFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7Z0JBQzFCLElBQUksTUFBTSxLQUFLLFVBQVUsRUFBRTs7d0JBQ25CLFNBQVMsR0FBWSxLQUFJLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFO29CQUMzRSxJQUFJLFNBQVMsRUFBRTt3QkFDYixPQUFPLEtBQUksQ0FBQyxVQUFVLENBQUMsMEJBQTBCLEVBQUU7NkJBQ2hELElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxXQUF3Qjs0QkFDakMscURBQXFEOzRCQUNyRCxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUU7Z0NBQ3JCLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQ3RELE9BQU8sSUFBSSxDQUFDOzZCQUNiO2lDQUFNO2dDQUNMLE9BQU8sS0FBSyxDQUFDOzZCQUNkO3dCQUNILENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ1A7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLDRCQUE0QixDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdEQsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2pCO2lCQUNGO2dCQUNELGlCQUFpQjtZQUNuQixDQUFDLENBQUMsRUFDQSxVQUFVLENBQUMsVUFBQyxLQUFVO2dCQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixDQUFDLENBQUMsQ0FDSCxDQUFDO1NBQ0w7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7O2dCQTdDRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQU5RLGlCQUFpQjtnQkFDakIsNEJBQTRCOzs7cUNBUHJDO0NBd0RDLEFBOUNELElBOENDO1NBM0NZLDBCQUEwQjs7Ozs7O0lBRXpCLGdEQUFxQzs7Ozs7SUFBRSxrRUFBa0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhbkRlYWN0aXZhdGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBtYXAsIGNvbmNhdE1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgb2YgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IFVuc2F2ZWRDaGFuZ2VzIH0gZnJvbSAnLi4vLi4vbW9kZWxzL3Vuc2F2ZWQtY2hhbmdlcy5tb2RlbCc7XHJcbmltcG9ydCB7IFN3ZWV0QWxlcnRTZXJ2aWNlIH0gZnJvbSAnLi4vY3VzdG9tL3N3ZWV0LmFsZXJ0LmNvbmZpcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IFByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2UgfSBmcm9tICcuLi9jdXN0b20vcHJldmVudC11bnNhdmVkLWNoYW5nZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZXJ0UmVzdWx0IH0gZnJvbSAnLi4vLi4vbW9kZWxzL2FsZXJ0LXJlc3VsdC5tb2RlbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQcmV2ZW50VW5zYXZlZENoYW5nZXNHdWFyZCBpbXBsZW1lbnRzIENhbkRlYWN0aXZhdGU8VW5zYXZlZENoYW5nZXM+IHtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzd2VldEFsZXJ0OiBTd2VldEFsZXJ0U2VydmljZSwgcHJpdmF0ZSBwcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlOiBQcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBjYW5EZWFjdGl2YXRlKCk6IGFueSB7XHJcbiAgICB0cnkge1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICB0aGlzLnByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2Uuc2V0U3RhdHVzKCd3YWl0aW5nJyk7XHJcbiAgICAgIH0sIDEwMCk7XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLmdldFN0YXR1cygpXHJcbiAgICAgICAgLnBpcGUoY29uY2F0TWFwKChyZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3VsdCA9PT0gJ2ZpbmlzaGVkJykge1xyXG4gICAgICAgICAgICBjb25zdCBpc0NoYW5nZWQ6IGJvb2xlYW4gPSB0aGlzLnByZXZlbnRVbnNhdmVkQ2hhbmdlc1NlcnZpY2UuZ2V0SXNDaGFuZ2VkKCk7XHJcbiAgICAgICAgICAgIGlmIChpc0NoYW5nZWQpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zd2VldEFsZXJ0LnByZXZlbnRVbnNhdmVkQ2hhbmdlZFBvcHVwKClcclxuICAgICAgICAgICAgICAgIC5waXBlKG1hcCgoYWxlcnRSZXN1bHQ6IEFsZXJ0UmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdjYW5EZWFjdGl2YXRlIGluIHBpcGUnLCBhbGVydFJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChhbGVydFJlc3VsdC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5zZXRJc0NoYW5nZWQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5zZXRJc0NoYW5nZWQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgIHJldHVybiBvZih0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLy8gcmV0dXJuIHJlc3VsdDtcclxuICAgICAgICB9KSxcclxuICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yOiBhbnkpOiBhbnkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgcmV0dXJuIG9mKHRydWUpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==