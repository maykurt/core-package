/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var FormTextInputComponent = /** @class */ (function () {
    function FormTextInputComponent() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
        this.numberOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    FormTextInputComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    FormTextInputComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormTextInputComponent.prototype, "inputValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this._value = val;
            this.onChange(val);
            this.onTouched();
            this.changed.emit(this._value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    FormTextInputComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormTextInputComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this._value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormTextInputComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FormTextInputComponent.prototype.onKeypress = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.disableSpace) {
            if (event && event.code === 'Space') {
                return false;
            }
        }
        if (this.numberOnly) {
            /** @type {?} */
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        return true;
    };
    FormTextInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-text-input',
                    template: "\n  <div style=\"width:100%;\">\n    <input type=\"text\"\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           name=\"inputValue\"\n           autocomplete=\"off\"\n           [(ngModel)]=\"inputValue\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [attr.maxlength]=\"maxLength\"\n           placeholder=\"{{placeholder|translate}}\"\n           [hidden]=\"hidden\"\n           (keypress)=\"onKeypress($event)\"\n           >\n    <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n  ",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormTextInputComponent; }),
                            multi: true
                        }
                    ],
                    styles: [""]
                }] }
    ];
    FormTextInputComponent.propDecorators = {
        placeholder: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        hidden: [{ type: Input }],
        clearable: [{ type: Input }],
        disableSpace: [{ type: Input }],
        maxLength: [{ type: Input }],
        changed: [{ type: Output }],
        numberOnly: [{ type: Input }]
    };
    return FormTextInputComponent;
}());
export { FormTextInputComponent };
if (false) {
    /** @type {?} */
    FormTextInputComponent.prototype.placeholder;
    /** @type {?} */
    FormTextInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormTextInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormTextInputComponent.prototype.hidden;
    /** @type {?} */
    FormTextInputComponent.prototype.clearable;
    /** @type {?} */
    FormTextInputComponent.prototype.disableSpace;
    /** @type {?} */
    FormTextInputComponent.prototype.maxLength;
    /** @type {?} */
    FormTextInputComponent.prototype.changed;
    /** @type {?} */
    FormTextInputComponent.prototype.numberOnly;
    /** @type {?} */
    FormTextInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS10ZXh0LWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtdGV4dC9mb3JtLXRleHQtaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQXdCLE1BQU0sZ0JBQWdCLENBQUM7QUFFekU7SUFBQTtRQStCVyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFHakIsWUFBTyxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pELGVBQVUsR0FBRyxLQUFLLENBQUM7SUFpRDlCLENBQUM7Ozs7O0lBNUNDLHlDQUFROzs7O0lBQVIsVUFBUyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCwwQ0FBUzs7O0lBQVQ7SUFDQSxDQUFDO0lBRUQsc0JBQUksOENBQVU7Ozs7UUFBZDtZQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDOzs7OztRQUVELFVBQWUsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNqQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDakMsQ0FBQzs7O09BUEE7Ozs7O0lBU0QsaURBQWdCOzs7O0lBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCwyQ0FBVTs7OztJQUFWLFVBQVcsS0FBSztRQUNkLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsa0RBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFRCwyQ0FBVTs7OztJQUFWLFVBQVcsS0FBVTtRQUNuQixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxPQUFPLEVBQUU7Z0JBQ25DLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTs7Z0JBQ2IsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTztZQUM1RCxJQUFJLFFBQVEsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRTtnQkFDckQsT0FBTyxLQUFLLENBQUM7YUFDZDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOztnQkF0RkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx3QkFBd0I7b0JBQ2xDLFFBQVEsRUFBRSwyckJBaUJUO29CQUVELFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSxzQkFBc0IsRUFBdEIsQ0FBc0IsQ0FBQzs0QkFDckQsS0FBSyxFQUFFLElBQUk7eUJBQ1o7cUJBQUM7O2lCQUNMOzs7OEJBR0UsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7eUJBQ0wsS0FBSzs0QkFDTCxLQUFLOytCQUNMLEtBQUs7NEJBQ0wsS0FBSzswQkFDTCxNQUFNOzZCQUNOLEtBQUs7O0lBaURSLDZCQUFDO0NBQUEsQUF2RkQsSUF1RkM7U0EzRFksc0JBQXNCOzs7SUFFakMsNkNBQThCOztJQUM5Qiw0Q0FBNEI7O0lBQzVCLDRDQUE0Qjs7SUFDNUIsd0NBQXdCOztJQUN4QiwyQ0FBMkI7O0lBQzNCLDhDQUFnQzs7SUFDaEMsMkNBQTRCOztJQUM1Qix5Q0FBMEQ7O0lBQzFELDRDQUE0Qjs7SUFHNUIsd0NBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiwgQ29udHJvbFZhbHVlQWNjZXNzb3IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1mb3JtLXRleHQtaW5wdXQnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPGRpdiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICA8aW5wdXQgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sIGZvcm0tY29udHJvbC1zbSBmaXhlZC1oZWlnaHQtaW5wdXQgZm9ybS1pbnB1dFwiXHJcbiAgICAgICAgICAgbmFtZT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgIGF1dG9jb21wbGV0ZT1cIm9mZlwiXHJcbiAgICAgICAgICAgWyhuZ01vZGVsKV09XCJpbnB1dFZhbHVlXCJcclxuICAgICAgICAgICBbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICAgICAgICAgW3JlYWRvbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAgICAgIFtjbGFzcy5wci0yNV09XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCJcclxuICAgICAgICAgICBbYXR0ci5tYXhsZW5ndGhdPVwibWF4TGVuZ3RoXCJcclxuICAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7cGxhY2Vob2xkZXJ8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgICAgIFtoaWRkZW5dPVwiaGlkZGVuXCJcclxuICAgICAgICAgICAoa2V5cHJlc3MpPVwib25LZXlwcmVzcygkZXZlbnQpXCJcclxuICAgICAgICAgICA+XHJcbiAgICA8Y29yZS1pY29uIGljb249XCJ0aW1lc1wiICpuZ0lmPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiIGNsYXNzPVwic2VhcmNoY2xlYXJcIiAoY2xpY2spPVwiaW5wdXRWYWx1ZSA9IG51bGxcIj48L2NvcmUtaWNvbj5cclxuICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL2Zvcm0tdGV4dC1pbnB1dC5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybVRleHRJbnB1dENvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybVRleHRJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuXHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBoaWRkZW4gPSBmYWxzZTtcclxuICBASW5wdXQoKSBjbGVhcmFibGUgPSBmYWxzZTtcclxuICBASW5wdXQoKSBkaXNhYmxlU3BhY2U/OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIG1heExlbmd0aD86IG51bWJlcjtcclxuICBAT3V0cHV0KCkgY2hhbmdlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQElucHV0KCkgbnVtYmVyT25seSA9IGZhbHNlO1xyXG5cclxuXHJcbiAgX3ZhbHVlOiBzdHJpbmc7XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgaW5wdXRWYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBpbnB1dFZhbHVlKHZhbCkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWw7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gICAgdGhpcy5jaGFuZ2VkLmVtaXQodGhpcy5fdmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xyXG4gICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuKSB7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCA9IGZuO1xyXG4gIH1cclxuXHJcbiAgb25LZXlwcmVzcyhldmVudDogYW55KTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5kaXNhYmxlU3BhY2UpIHtcclxuICAgICAgaWYgKGV2ZW50ICYmIGV2ZW50LmNvZGUgPT09ICdTcGFjZScpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5udW1iZXJPbmx5KSB7XHJcbiAgICAgIGNvbnN0IGNoYXJDb2RlID0gKGV2ZW50LndoaWNoKSA/IGV2ZW50LndoaWNoIDogZXZlbnQua2V5Q29kZTtcclxuICAgICAgaWYgKGNoYXJDb2RlID4gMzEgJiYgKGNoYXJDb2RlIDwgNDggfHwgY2hhckNvZGUgPiA1NykpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxufVxyXG4iXX0=