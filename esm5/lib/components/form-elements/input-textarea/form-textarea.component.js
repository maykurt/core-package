/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var FormTextAreaComponent = /** @class */ (function () {
    function FormTextAreaComponent() {
        this.isReadOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    FormTextAreaComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    FormTextAreaComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormTextAreaComponent.prototype, "inputValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this._value = val;
            this.onChange(val);
            this.onTouched();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    FormTextAreaComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormTextAreaComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this._value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormTextAreaComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FormTextAreaComponent.prototype.onKeyDown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    };
    FormTextAreaComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-textarea',
                    template: "\n    <textarea name=\"inputValue\" class=\"form-textarea\"\n              id=\"inputValue\" style=\"min-width: 100%;\"\n              rows=\"5\"\n              [attr.maxlength]=\"maxLength\"\n              [class.readonly-input] = \"isReadOnly\"\n              [readonly]=\"isReadOnly\"\n              [(ngModel)]=\"inputValue\"\n              (keydown)=\"onKeyDown($event)\"\n              placeholder=\"{{placeholder|translate}}\"\n              ></textarea>\n  ",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormTextAreaComponent; }),
                            multi: true
                        }
                    ],
                    styles: [".readonly-input{background-color:#e9ecef;cursor:default}"]
                }] }
    ];
    FormTextAreaComponent.propDecorators = {
        placeholder: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        maxLength: [{ type: Input }]
    };
    return FormTextAreaComponent;
}());
export { FormTextAreaComponent };
if (false) {
    /** @type {?} */
    FormTextAreaComponent.prototype.placeholder;
    /** @type {?} */
    FormTextAreaComponent.prototype.isReadOnly;
    /** @type {?} */
    FormTextAreaComponent.prototype.maxLength;
    /**
     * @type {?}
     * @private
     */
    FormTextAreaComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS10ZXh0YXJlYS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLWVsZW1lbnRzL2lucHV0LXRleHRhcmVhL2Zvcm0tdGV4dGFyZWEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBRXpFO0lBQUE7UUF3QlcsZUFBVSxHQUFHLEtBQUssQ0FBQztJQXVDOUIsQ0FBQzs7Ozs7SUFqQ0Msd0NBQVE7Ozs7SUFBUixVQUFTLEdBQVE7SUFDakIsQ0FBQzs7OztJQUVELHlDQUFTOzs7SUFBVDtJQUNBLENBQUM7SUFFRCxzQkFBSSw2Q0FBVTs7OztRQUFkO1lBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JCLENBQUM7Ozs7O1FBRUQsVUFBZSxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25CLENBQUM7OztPQU5BOzs7OztJQVFELGdEQUFnQjs7OztJQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsMENBQVU7Ozs7SUFBVixVQUFXLEtBQUs7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGlEQUFpQjs7OztJQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQseUNBQVM7Ozs7SUFBVCxVQUFVLEtBQVU7UUFDbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7O2dCQTlERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtvQkFDaEMsUUFBUSxFQUFFLG1kQVdUO29CQUVELFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSxxQkFBcUIsRUFBckIsQ0FBcUIsQ0FBQzs0QkFDcEQsS0FBSyxFQUFFLElBQUk7eUJBQ1o7cUJBQUM7O2lCQUNMOzs7OEJBRUUsS0FBSzs2QkFDTCxLQUFLOzRCQUVMLEtBQUs7O0lBcUNSLDRCQUFDO0NBQUEsQUEvREQsSUErREM7U0F6Q1kscUJBQXFCOzs7SUFDaEMsNENBQXFCOztJQUNyQiwyQ0FBNEI7O0lBRTVCLDBDQUE0Qjs7Ozs7SUFFNUIsdUNBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWZvcm0tdGV4dGFyZWEnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8dGV4dGFyZWEgbmFtZT1cImlucHV0VmFsdWVcIiBjbGFzcz1cImZvcm0tdGV4dGFyZWFcIlxyXG4gICAgICAgICAgICAgIGlkPVwiaW5wdXRWYWx1ZVwiIHN0eWxlPVwibWluLXdpZHRoOiAxMDAlO1wiXHJcbiAgICAgICAgICAgICAgcm93cz1cIjVcIlxyXG4gICAgICAgICAgICAgIFthdHRyLm1heGxlbmd0aF09XCJtYXhMZW5ndGhcIlxyXG4gICAgICAgICAgICAgIFtjbGFzcy5yZWFkb25seS1pbnB1dF0gPSBcImlzUmVhZE9ubHlcIlxyXG4gICAgICAgICAgICAgIFtyZWFkb25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgICAgIChrZXlkb3duKT1cIm9uS2V5RG93bigkZXZlbnQpXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7cGxhY2Vob2xkZXJ8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgICAgICAgID48L3RleHRhcmVhPlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS10ZXh0YXJlYS5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybVRleHRBcmVhQ29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtVGV4dEFyZWFDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG5cclxuICBASW5wdXQoKSBtYXhMZW5ndGg/OiBudW1iZXI7XHJcblxyXG4gIHByaXZhdGUgX3ZhbHVlO1xyXG5cclxuICBvbkNoYW5nZSh2YWw6IGFueSkge1xyXG4gIH1cclxuXHJcbiAgb25Ub3VjaGVkKCkge1xyXG4gIH1cclxuXHJcbiAgZ2V0IGlucHV0VmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgfVxyXG5cclxuICBzZXQgaW5wdXRWYWx1ZSh2YWwpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIG9uS2V5RG93bihldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5pc1JlYWRPbmx5KSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==