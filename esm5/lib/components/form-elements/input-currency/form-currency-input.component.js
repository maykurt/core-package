/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var FormCurrencyInputComponent = /** @class */ (function () {
    function FormCurrencyInputComponent() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.changed = new EventEmitter();
        this.clearable = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormCurrencyInputComponent.prototype, "inputValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this._value = val;
            this.onChange(val);
            this.onTouched();
            this.changed.emit(this._value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this._value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FormCurrencyInputComponent.prototype.onKeyDown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    };
    FormCurrencyInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-currency-input',
                    template: "\n  <div style=\"width:100%;\">\n    <input currencyMask\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           autocomplete=\"off\"\n           name=\"inputValue\"\n           [(ngModel)]=\"inputValue\"\n           (keydown)=\"onKeyDown($event)\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           placeholder=\"{{placeholder|translate}}\"\n           [class.readonly-input] = \"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [hidden]=\"hidden\">\n     <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n\n  ",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormCurrencyInputComponent; }),
                            multi: true
                        }
                    ],
                    styles: [".readonly-input:focus{outline:0;box-shadow:none}"]
                }] }
    ];
    FormCurrencyInputComponent.propDecorators = {
        placeholder: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        hidden: [{ type: Input }],
        changed: [{ type: Output }],
        clearable: [{ type: Input }]
    };
    return FormCurrencyInputComponent;
}());
export { FormCurrencyInputComponent };
if (false) {
    /** @type {?} */
    FormCurrencyInputComponent.prototype.placeholder;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.hidden;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.changed;
    /** @type {?} */
    FormCurrencyInputComponent.prototype.clearable;
    /** @type {?} */
    FormCurrencyInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1jdXJyZW5jeS1pbnB1dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLWVsZW1lbnRzL2lucHV0LWN1cnJlbmN5L2Zvcm0tY3VycmVuY3ktaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQXdCLE1BQU0sZ0JBQWdCLENBQUM7QUFFekU7SUFBQTtRQStCVyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNkLFlBQU8sR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVqRCxjQUFTLEdBQVksS0FBSyxDQUFDO0lBc0N0QyxDQUFDOzs7OztJQWxDQyw2Q0FBUTs7OztJQUFSLFVBQVMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQsOENBQVM7OztJQUFUO0lBQ0EsQ0FBQztJQUVELHNCQUFJLGtEQUFVOzs7O1FBQWQ7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckIsQ0FBQzs7Ozs7UUFFRCxVQUFlLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDakIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLENBQUM7OztPQVBBOzs7OztJQVNELHFEQUFnQjs7OztJQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsK0NBQVU7Ozs7SUFBVixVQUFXLEtBQUs7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELHNEQUFpQjs7OztJQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsOENBQVM7Ozs7SUFBVCxVQUFVLEtBQVU7UUFDbEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7O2dCQXpFRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtvQkFDdEMsUUFBUSxFQUFFLHVyQkFpQlQ7b0JBRUQsU0FBUyxFQUFFO3dCQUNUOzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLDBCQUEwQixFQUExQixDQUEwQixDQUFDOzRCQUN6RCxLQUFLLEVBQUUsSUFBSTt5QkFDWjtxQkFBQzs7aUJBQ0w7Ozs4QkFHRSxLQUFLOzZCQUNMLEtBQUs7NkJBQ0wsS0FBSzt5QkFDTCxLQUFLOzBCQUNMLE1BQU07NEJBRU4sS0FBSzs7SUFzQ1IsaUNBQUM7Q0FBQSxBQTFFRCxJQTBFQztTQTdDWSwwQkFBMEI7OztJQUNyQyxpREFBOEI7O0lBQzlCLGdEQUE0Qjs7SUFDNUIsZ0RBQTRCOztJQUM1Qiw0Q0FBd0I7O0lBQ3hCLDZDQUEwRDs7SUFFMUQsK0NBQW9DOztJQUVwQyw0Q0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgZm9yd2FyZFJlZiwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWZvcm0tY3VycmVuY3ktaW5wdXQnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPGRpdiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICA8aW5wdXQgY3VycmVuY3lNYXNrXHJcbiAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZm9ybS1jb250cm9sLXNtIGZpeGVkLWhlaWdodC1pbnB1dCBmb3JtLWlucHV0XCJcclxuICAgICAgICAgICBhdXRvY29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgIG5hbWU9XCJpbnB1dFZhbHVlXCJcclxuICAgICAgICAgICBbKG5nTW9kZWwpXT1cImlucHV0VmFsdWVcIlxyXG4gICAgICAgICAgIChrZXlkb3duKT1cIm9uS2V5RG93bigkZXZlbnQpXCJcclxuICAgICAgICAgICBbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICAgICAgICAgW3JlYWRvbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAgICAgIHBsYWNlaG9sZGVyPVwie3twbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXHJcbiAgICAgICAgICAgW2NsYXNzLnJlYWRvbmx5LWlucHV0XSA9IFwiaXNSZWFkT25seVwiXHJcbiAgICAgICAgICAgW2NsYXNzLnByLTI1XT1cImNsZWFyYWJsZSAmJiBfdmFsdWVcIlxyXG4gICAgICAgICAgIFtoaWRkZW5dPVwiaGlkZGVuXCI+XHJcbiAgICAgPGNvcmUtaWNvbiBpY29uPVwidGltZXNcIiAqbmdJZj1cImNsZWFyYWJsZSAmJiBfdmFsdWVcIiBjbGFzcz1cInNlYXJjaGNsZWFyXCIgKGNsaWNrKT1cImlucHV0VmFsdWUgPSBudWxsXCI+PC9jb3JlLWljb24+XHJcbiAgPC9kaXY+XHJcblxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS1jdXJyZW5jeS1pbnB1dC5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybUN1cnJlbmN5SW5wdXRDb21wb25lbnQpLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtQ3VycmVuY3lJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuICBASW5wdXQoKSBwbGFjZWhvbGRlcj86IHN0cmluZztcclxuICBASW5wdXQoKSBpc0Rpc2FibGVkID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGhpZGRlbiA9IGZhbHNlO1xyXG4gIEBPdXRwdXQoKSBjaGFuZ2VkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgQElucHV0KCkgY2xlYXJhYmxlOiBCb29sZWFuID0gZmFsc2U7XHJcblxyXG4gIF92YWx1ZTogYW55O1xyXG5cclxuICBvbkNoYW5nZSh2YWw6IGFueSkge1xyXG4gIH1cclxuXHJcbiAgb25Ub3VjaGVkKCkge1xyXG4gIH1cclxuXHJcbiAgZ2V0IGlucHV0VmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgfVxyXG5cclxuICBzZXQgaW5wdXRWYWx1ZSh2YWwpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuICAgIHRoaXMuY2hhbmdlZC5lbWl0KHRoaXMuX3ZhbHVlKTtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWUpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIG9uS2V5RG93bihldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5pc1JlYWRPbmx5KSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==