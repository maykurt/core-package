/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MaskType } from '../../../enums/mask-type.enum';
import emailMask from 'text-mask-addons/dist/emailMask';
var FormMaskInputComponent = /** @class */ (function () {
    function FormMaskInputComponent() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    FormMaskInputComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.maskType) {
            if (this.maskType === MaskType.Phone) {
                this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
                this.placeholder = '(__) ___-____';
            }
            else if (this.maskType === MaskType.Mail) {
                this.mask = emailMask;
                // this.placeholder = 'john@smith.com';
                this.placeholder = '';
            }
            else if (this.maskType === MaskType.PostCode) {
                this.mask = [/\d/, /\d/, /\d/, /\d/, /\d/];
                // this.placeholder = '38000';
                this.placeholder = '';
            }
        }
        if (changes.placeholder && changes.placeholder.currentValue) {
            this.placeholder = changes.placeholder.currentValue;
        }
    };
    /**
     * @param {?} val
     * @return {?}
     */
    FormMaskInputComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    FormMaskInputComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormMaskInputComponent.prototype, "inputValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            if (val) {
                if (this.maskType === MaskType.Phone) {
                    val = val.replace(/\D+/g, '');
                }
            }
            this._value = val;
            this.onChange(val);
            this.onTouched();
            this.changed.emit(this._value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    FormMaskInputComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormMaskInputComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this._value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormMaskInputComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    FormMaskInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-mask-input',
                    template: "\n\n  <div style=\"width:100%;\">\n    <input type=\"text\"\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           name=\"inputValue\"\n           autocomplete=\"off\"\n           type=\"text\"\n           [textMask]=\"{mask: mask}\"\n           [(ngModel)]=\"inputValue\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [hidden]=\"hidden\"\n           [placeholder]=\"placeholder\"\n           >\n    <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormMaskInputComponent; }),
                            multi: true
                        }
                    ]
                }] }
    ];
    /** @nocollapse */
    FormMaskInputComponent.ctorParameters = function () { return []; };
    FormMaskInputComponent.propDecorators = {
        maskType: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        hidden: [{ type: Input }],
        placeholder: [{ type: Input }],
        clearable: [{ type: Input }],
        changed: [{ type: Output }]
    };
    return FormMaskInputComponent;
}());
export { FormMaskInputComponent };
if (false) {
    /** @type {?} */
    FormMaskInputComponent.prototype.maskType;
    /** @type {?} */
    FormMaskInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormMaskInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormMaskInputComponent.prototype.hidden;
    /** @type {?} */
    FormMaskInputComponent.prototype.placeholder;
    /** @type {?} */
    FormMaskInputComponent.prototype.clearable;
    /** @type {?} */
    FormMaskInputComponent.prototype.changed;
    /** @type {?} */
    FormMaskInputComponent.prototype._value;
    /** @type {?} */
    FormMaskInputComponent.prototype.mask;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1tYXNrLWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtbWFzay9mb3JtLW1hc2staW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQW9DLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckgsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUV6RCxPQUFPLFNBQVMsTUFBTSxpQ0FBaUMsQ0FBQztBQUV4RDtJQTJDRTtRQWJTLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBRWYsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMxQixZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQVM1QyxDQUFDOzs7OztJQUVELDRDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDcEIsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxLQUFLLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ2hHLElBQUksQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDO2FBQ3BDO2lCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsSUFBSSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztnQkFDdEIsdUNBQXVDO2dCQUN2QyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzthQUN2QjtpQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLFFBQVEsRUFBRTtnQkFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDM0MsOEJBQThCO2dCQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzthQUN2QjtTQUNGO1FBRUQsSUFBSSxPQUFPLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQzNELElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7U0FDckQ7SUFDSCxDQUFDOzs7OztJQUVELHlDQUFROzs7O0lBQVIsVUFBUyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCwwQ0FBUzs7O0lBQVQ7SUFDQSxDQUFDO0lBRUQsc0JBQUksOENBQVU7Ozs7UUFBZDtZQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDOzs7OztRQUVELFVBQWUsR0FBRztZQUNoQixJQUFJLEdBQUcsRUFBRTtnQkFDUCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLEtBQUssRUFBRTtvQkFDcEMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2lCQUMvQjthQUNGO1lBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDakIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLENBQUM7OztPQWJBOzs7OztJQWVELGlEQUFnQjs7OztJQUFoQixVQUFpQixFQUFFO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsMkNBQVU7Ozs7SUFBVixVQUFXLEtBQUs7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELGtEQUFpQjs7OztJQUFqQixVQUFrQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7O2dCQXBHRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsUUFBUSxFQUFFLHdwQkFrQlg7b0JBQ0MsU0FBUyxFQUFFO3dCQUNUOzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLHNCQUFzQixFQUF0QixDQUFzQixDQUFDOzRCQUNyRCxLQUFLLEVBQUUsSUFBSTt5QkFDWjtxQkFBQztpQkFDTDs7Ozs7MkJBRUUsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7eUJBQ0wsS0FBSzs4QkFDTCxLQUFLOzRCQUNMLEtBQUs7MEJBQ0wsTUFBTTs7SUFtRVQsNkJBQUM7Q0FBQSxBQXRHRCxJQXNHQztTQTFFWSxzQkFBc0I7OztJQUNqQywwQ0FBNEI7O0lBQzVCLDRDQUE0Qjs7SUFDNUIsNENBQTRCOztJQUM1Qix3Q0FBd0I7O0lBQ3hCLDZDQUE4Qjs7SUFDOUIsMkNBQW9DOztJQUNwQyx5Q0FBNEM7O0lBRzVDLHdDQUFlOztJQUNmLHNDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBmb3J3YXJkUmVmLCBJbnB1dCwgT25Jbml0LCBTaW1wbGVDaGFuZ2VzLCBPbkNoYW5nZXMsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWFza1R5cGUgfSBmcm9tICcuLi8uLi8uLi9lbnVtcy9tYXNrLXR5cGUuZW51bSc7XHJcblxyXG5pbXBvcnQgZW1haWxNYXNrIGZyb20gJ3RleHQtbWFzay1hZGRvbnMvZGlzdC9lbWFpbE1hc2snO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtZm9ybS1tYXNrLWlucHV0JyxcclxuICB0ZW1wbGF0ZTogYFxyXG5cclxuICA8ZGl2IHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2wgZm9ybS1jb250cm9sLXNtIGZpeGVkLWhlaWdodC1pbnB1dCBmb3JtLWlucHV0XCJcclxuICAgICAgICAgICBuYW1lPVwiaW5wdXRWYWx1ZVwiXHJcbiAgICAgICAgICAgYXV0b2NvbXBsZXRlPVwib2ZmXCJcclxuICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgW3RleHRNYXNrXT1cInttYXNrOiBtYXNrfVwiXHJcbiAgICAgICAgICAgWyhuZ01vZGVsKV09XCJpbnB1dFZhbHVlXCJcclxuICAgICAgICAgICBbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICAgICAgICAgW3JlYWRvbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAgICAgIFtjbGFzcy5wci0yNV09XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCJcclxuICAgICAgICAgICBbaGlkZGVuXT1cImhpZGRlblwiXHJcbiAgICAgICAgICAgW3BsYWNlaG9sZGVyXT1cInBsYWNlaG9sZGVyXCJcclxuICAgICAgICAgICA+XHJcbiAgICA8Y29yZS1pY29uIGljb249XCJ0aW1lc1wiICpuZ0lmPVwiY2xlYXJhYmxlICYmIF92YWx1ZVwiIGNsYXNzPVwic2VhcmNoY2xlYXJcIiAoY2xpY2spPVwiaW5wdXRWYWx1ZSA9IG51bGxcIj48L2NvcmUtaWNvbj5cclxuICA8L2Rpdj5cclxuYCxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZvcm1NYXNrSW5wdXRDb21wb25lbnQpLFxyXG4gICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgfV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1NYXNrSW5wdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuICBASW5wdXQoKSBtYXNrVHlwZTogTWFza1R5cGU7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBoaWRkZW4gPSBmYWxzZTtcclxuICBASW5wdXQoKSBwbGFjZWhvbGRlcj86IHN0cmluZztcclxuICBASW5wdXQoKSBjbGVhcmFibGU6IEJvb2xlYW4gPSBmYWxzZTtcclxuICBAT3V0cHV0KCkgY2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuXHJcbiAgX3ZhbHVlOiBzdHJpbmc7XHJcbiAgbWFzazogYW55O1xyXG5cclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMubWFza1R5cGUpIHtcclxuICAgICAgaWYgKHRoaXMubWFza1R5cGUgPT09IE1hc2tUeXBlLlBob25lKSB7XHJcbiAgICAgICAgdGhpcy5tYXNrID0gWycoJywgL1sxLTldLywgL1xcZC8sIC9cXGQvLCAnKScsICcgJywgL1xcZC8sIC9cXGQvLCAvXFxkLywgJy0nLCAvXFxkLywgL1xcZC8sIC9cXGQvLCAvXFxkL107XHJcbiAgICAgICAgdGhpcy5wbGFjZWhvbGRlciA9ICcoX18pIF9fXy1fX19fJztcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLm1hc2tUeXBlID09PSBNYXNrVHlwZS5NYWlsKSB7XHJcbiAgICAgICAgdGhpcy5tYXNrID0gZW1haWxNYXNrO1xyXG4gICAgICAgIC8vIHRoaXMucGxhY2Vob2xkZXIgPSAnam9obkBzbWl0aC5jb20nO1xyXG4gICAgICAgIHRoaXMucGxhY2Vob2xkZXIgPSAnJztcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLm1hc2tUeXBlID09PSBNYXNrVHlwZS5Qb3N0Q29kZSkge1xyXG4gICAgICAgIHRoaXMubWFzayA9IFsvXFxkLywgL1xcZC8sIC9cXGQvLCAvXFxkLywgL1xcZC9dO1xyXG4gICAgICAgIC8vIHRoaXMucGxhY2Vob2xkZXIgPSAnMzgwMDAnO1xyXG4gICAgICAgIHRoaXMucGxhY2Vob2xkZXIgPSAnJztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjaGFuZ2VzLnBsYWNlaG9sZGVyICYmIGNoYW5nZXMucGxhY2Vob2xkZXIuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgIHRoaXMucGxhY2Vob2xkZXIgPSBjaGFuZ2VzLnBsYWNlaG9sZGVyLmN1cnJlbnRWYWx1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgaW5wdXRWYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICB9XHJcblxyXG4gIHNldCBpbnB1dFZhbHVlKHZhbCkge1xyXG4gICAgaWYgKHZhbCkge1xyXG4gICAgICBpZiAodGhpcy5tYXNrVHlwZSA9PT0gTWFza1R5cGUuUGhvbmUpIHtcclxuICAgICAgICB2YWwgPSB2YWwucmVwbGFjZSgvXFxEKy9nLCAnJyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbDtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsKTtcclxuICAgIHRoaXMub25Ub3VjaGVkKCk7XHJcbiAgICB0aGlzLmNoYW5nZWQuZW1pdCh0aGlzLl92YWx1ZSk7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=