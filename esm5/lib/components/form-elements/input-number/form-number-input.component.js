/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Input, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
var FormNumberInputComponent = /** @class */ (function () {
    function FormNumberInputComponent() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    FormNumberInputComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.maxLength && this.maxLength) {
            /** @type {?} */
            var value = '';
            for (var i = 0; i < this.maxLength; i++) {
                value += '1';
            }
            this.maxValue = parseInt(value, 10) * 9;
        }
    };
    /**
     * @param {?} val
     * @return {?}
     */
    FormNumberInputComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    FormNumberInputComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormNumberInputComponent.prototype, "inputValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this._value = val;
            this.onChange(val);
            this.onTouched();
            this.changed.emit(this._value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    FormNumberInputComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormNumberInputComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this._value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormNumberInputComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FormNumberInputComponent.prototype.onKeyUp = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.maxValue && this.maxLength && this.inputValue && this.inputValue > this.maxValue) {
            this.inputValue = parseInt(this.inputValue.toString().slice(0, this.maxLength), 10);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FormNumberInputComponent.prototype.numberOnly = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event && event.code === 'KeyE') {
            return false;
        }
        return true;
    };
    FormNumberInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-number-input',
                    template: "\n    <div style=\"width:100%;\">\n      <input type=\"number\"\n             class=\"form-control form-control-sm fixed-height-input form-input\"\n             name=\"inputValue\"\n             autocomplete=\"off\"\n             [(ngModel)]=\"inputValue\"\n             [disabled]=\"isDisabled\"\n             [readonly]=\"isReadOnly\"\n             [class.pr-25]=\"clearable && _value\"\n             (keyup)=\"onKeyUp($event)\"\n             (keypress)=\"numberOnly($event)\"\n              placeholder=\"{{placeholder|translate}}\"\n             [hidden]=\"hidden\">\n      <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n    </div>\n  ",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormNumberInputComponent; }),
                            multi: true
                        }
                    ],
                    styles: ["input::-webkit-inner-spin-button,input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}"]
                }] }
    ];
    FormNumberInputComponent.propDecorators = {
        placeholder: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        hidden: [{ type: Input }],
        clearable: [{ type: Input }],
        changed: [{ type: Output }],
        maxLength: [{ type: Input }]
    };
    return FormNumberInputComponent;
}());
export { FormNumberInputComponent };
if (false) {
    /** @type {?} */
    FormNumberInputComponent.prototype.placeholder;
    /** @type {?} */
    FormNumberInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormNumberInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormNumberInputComponent.prototype.hidden;
    /** @type {?} */
    FormNumberInputComponent.prototype.clearable;
    /** @type {?} */
    FormNumberInputComponent.prototype.changed;
    /** @type {?} */
    FormNumberInputComponent.prototype.maxLength;
    /**
     * @type {?}
     * @private
     */
    FormNumberInputComponent.prototype.maxValue;
    /** @type {?} */
    FormNumberInputComponent.prototype._value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1udW1iZXItaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZm9ybS1lbGVtZW50cy9pbnB1dC1udW1iZXIvZm9ybS1udW1iZXItaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDN0csT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGdCQUFnQixDQUFDO0FBRXpFO0lBQUE7UUE4QlcsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzFCLFlBQU8sR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQTRENUQsQ0FBQzs7Ozs7SUFwREMsOENBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFOztnQkFDbkMsS0FBSyxHQUFHLEVBQUU7WUFDZCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdkMsS0FBSyxJQUFJLEdBQUcsQ0FBQzthQUNkO1lBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7Ozs7O0lBRUQsMkNBQVE7Ozs7SUFBUixVQUFTLEdBQVE7SUFDakIsQ0FBQzs7OztJQUVELDRDQUFTOzs7SUFBVDtJQUNBLENBQUM7SUFFRCxzQkFBSSxnREFBVTs7OztRQUFkO1lBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JCLENBQUM7Ozs7O1FBRUQsVUFBZSxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqQyxDQUFDOzs7T0FQQTs7Ozs7SUFTRCxtREFBZ0I7Ozs7SUFBaEIsVUFBaUIsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELDZDQUFVOzs7O0lBQVYsVUFBVyxLQUFhO1FBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsb0RBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFHRCwwQ0FBTzs7OztJQUFQLFVBQVEsS0FBVTtRQUNoQixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUN6RixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3JGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCw2Q0FBVTs7OztJQUFWLFVBQVcsS0FBVTtRQUNuQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtZQUNsQyxPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOztnQkE3RkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLFFBQVEsRUFBRSw0c0JBZ0JUO29CQUVELFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixXQUFXLEVBQUUsVUFBVSxDQUFDLGNBQU0sT0FBQSx3QkFBd0IsRUFBeEIsQ0FBd0IsQ0FBQzs0QkFDdkQsS0FBSyxFQUFFLElBQUk7eUJBQ1o7cUJBQUM7O2lCQUNMOzs7OEJBR0UsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7eUJBQ0wsS0FBSzs0QkFDTCxLQUFLOzBCQUNMLE1BQU07NEJBRU4sS0FBSzs7SUEwRFIsK0JBQUM7Q0FBQSxBQTlGRCxJQThGQztTQW5FWSx3QkFBd0I7OztJQUVuQywrQ0FBOEI7O0lBQzlCLDhDQUE0Qjs7SUFDNUIsOENBQTRCOztJQUM1QiwwQ0FBd0I7O0lBQ3hCLDZDQUFvQzs7SUFDcEMsMkNBQTBEOztJQUUxRCw2Q0FBNEI7Ozs7O0lBRTVCLDRDQUF5Qjs7SUFFekIsMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIGZvcndhcmRSZWYsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgU2ltcGxlQ2hhbmdlcywgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5HX1ZBTFVFX0FDQ0VTU09SLCBDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWZvcm0tbnVtYmVyLWlucHV0JyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgIDxpbnB1dCB0eXBlPVwibnVtYmVyXCJcclxuICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sIGZvcm0tY29udHJvbC1zbSBmaXhlZC1oZWlnaHQtaW5wdXQgZm9ybS1pbnB1dFwiXHJcbiAgICAgICAgICAgICBuYW1lPVwiaW5wdXRWYWx1ZVwiXHJcbiAgICAgICAgICAgICBhdXRvY29tcGxldGU9XCJvZmZcIlxyXG4gICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJpbnB1dFZhbHVlXCJcclxuICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJpc0Rpc2FibGVkXCJcclxuICAgICAgICAgICAgIFtyZWFkb25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAgICAgIFtjbGFzcy5wci0yNV09XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCJcclxuICAgICAgICAgICAgIChrZXl1cCk9XCJvbktleVVwKCRldmVudClcIlxyXG4gICAgICAgICAgICAgKGtleXByZXNzKT1cIm51bWJlck9ubHkoJGV2ZW50KVwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7e3BsYWNlaG9sZGVyfHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAgICAgIFtoaWRkZW5dPVwiaGlkZGVuXCI+XHJcbiAgICAgIDxjb3JlLWljb24gaWNvbj1cInRpbWVzXCIgKm5nSWY9XCJjbGVhcmFibGUgJiYgX3ZhbHVlXCIgY2xhc3M9XCJzZWFyY2hjbGVhclwiIChjbGljayk9XCJpbnB1dFZhbHVlID0gbnVsbFwiPjwvY29yZS1pY29uPlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9mb3JtLW51bWJlci1pbnB1dC5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybU51bWJlcklucHV0Q29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtTnVtYmVySW5wdXRDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICBASW5wdXQoKSBoaWRkZW4gPSBmYWxzZTtcclxuICBASW5wdXQoKSBjbGVhcmFibGU6IEJvb2xlYW4gPSBmYWxzZTtcclxuICBAT3V0cHV0KCkgY2hhbmdlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIEBJbnB1dCgpIG1heExlbmd0aD86IG51bWJlcjtcclxuXHJcbiAgcHJpdmF0ZSBtYXhWYWx1ZTogbnVtYmVyO1xyXG5cclxuICBfdmFsdWU6IG51bWJlcjtcclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMubWF4TGVuZ3RoICYmIHRoaXMubWF4TGVuZ3RoKSB7XHJcbiAgICAgIGxldCB2YWx1ZSA9ICcnO1xyXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubWF4TGVuZ3RoOyBpKyspIHtcclxuICAgICAgICB2YWx1ZSArPSAnMSc7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5tYXhWYWx1ZSA9IHBhcnNlSW50KHZhbHVlLCAxMCkgKiA5O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2UodmFsOiBhbnkpIHtcclxuICB9XHJcblxyXG4gIG9uVG91Y2hlZCgpIHtcclxuICB9XHJcblxyXG4gIGdldCBpbnB1dFZhbHVlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IGlucHV0VmFsdWUodmFsKSB7XHJcbiAgICB0aGlzLl92YWx1ZSA9IHZhbDtcclxuICAgIHRoaXMub25DaGFuZ2UodmFsKTtcclxuICAgIHRoaXMub25Ub3VjaGVkKCk7XHJcbiAgICB0aGlzLmNoYW5nZWQuZW1pdCh0aGlzLl92YWx1ZSk7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuKSB7XHJcbiAgICB0aGlzLm9uQ2hhbmdlID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlOiBudW1iZXIpIHtcclxuICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG5cclxuICBvbktleVVwKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLm1heFZhbHVlICYmIHRoaXMubWF4TGVuZ3RoICYmIHRoaXMuaW5wdXRWYWx1ZSAmJiB0aGlzLmlucHV0VmFsdWUgPiB0aGlzLm1heFZhbHVlKSB7XHJcbiAgICAgIHRoaXMuaW5wdXRWYWx1ZSA9IHBhcnNlSW50KHRoaXMuaW5wdXRWYWx1ZS50b1N0cmluZygpLnNsaWNlKDAsIHRoaXMubWF4TGVuZ3RoKSwgMTApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbnVtYmVyT25seShldmVudDogYW55KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoZXZlbnQgJiYgZXZlbnQuY29kZSA9PT0gJ0tleUUnKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxufVxyXG4iXX0=