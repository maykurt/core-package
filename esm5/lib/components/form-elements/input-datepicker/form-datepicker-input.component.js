/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import moment from 'moment';
import * as _ from 'lodash';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateSettings } from '../../../models/date-settings.model';
import { DateSelectionMode } from '../../../enums/date-selection-mode.enum';
var FormDatepickerInputComponent = /** @class */ (function () {
    function FormDatepickerInputComponent(fb, translate) {
        var _this = this;
        this.fb = fb;
        this.translate = translate;
        this.isReadOnly = false;
        this.dateChanges = new EventEmitter();
        this._selectionMode = DateSelectionMode.Single;
        this.onChange = function () { };
        this.onTouched = function () { };
        this.view = 'date';
        this.yearDataSource = [];
        this.defaultSettings = new DateSettings();
        this.formGroup = this.fb.group({
            Year: null
        });
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange.subscribe(function (event) {
            if (event) {
                _this.locale = event.lang === 'tr' ? _this.localeTr : _this.localEn;
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.selectionMode && this.selectionMode) {
            this._selectionMode = this.selectionMode;
        }
        if (changes.settings && this.settings) {
            this.defaultSettings = _.assign({}, this.defaultSettings, this.settings);
            if (this.settings.selectionMode && !this.selectionMode) {
                this._selectionMode = this.settings.selectionMode;
            }
            if (this.settings.showYear) {
                this.defaultSettings.dateFormat = 'yy';
                this.initiateYearDataSource();
            }
            if (this.settings.showTimeOnly) {
                this.defaultSettings.disableUTC = true;
                this.defaultSettings.hideCalendarButton = true;
                this.defaultSettings.showButtonBar = false;
            }
            if (this.settings.showTime) {
                this.defaultSettings.disableUTC = true;
            }
            if (this.settings.showMonthPicker) {
                this.view = 'month';
            }
            if (this.settings.minDate) {
                this.minDate = this.convertDateToDateUTC(this.settings.minDate);
            }
            if (this.settings.maxDate) {
                this.maxDate = this.convertDateToDateUTC(this.settings.maxDate);
            }
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value) {
            if (value.indexOf('T') !== -1 && !this.defaultSettings.disableUTC && !this.defaultSettings.showTimeOnly && !this.defaultSettings.showTime) {
                // can be done via moment
                /** @type {?} */
                var splittedValue = value.split('T');
                splittedValue[1] = '00:00:00.000Z';
                value = splittedValue.join('T');
            }
            if (this.selectedDateString !== value) {
                // due to UTC, without being influenced timezone changes, it creates date according to value
                // in order to avoid reflecting timezone changes to UI
                // console.log('value ', value);
                // console.log('moment(value).toDate() ', moment(value).toDate());
                // console.log('this.convertDateToDateUTC(value) ', this.convertDateToDateUTC(value));
                if (!this.usedMometToDate && (this.defaultSettings.showTimeOnly || this.defaultSettings.showTime)) {
                    // first initiation needs to be done this way
                    this._calendarDateValue = moment(value).toDate();
                    this.usedMometToDate = true;
                }
                else {
                    // after the initiation needs to be used this way
                    this._calendarDateValue = this.convertDateToDateUTC(value);
                }
                this.formGroup.setValue({
                    Year: moment(value).format('YYYY')
                });
                this.selectedDateString = value;
            }
        }
        else {
            this._calendarDateValue = null;
            this.selectedDateString = null;
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    Object.defineProperty(FormDatepickerInputComponent.prototype, "selectedDate", {
        get: /**
         * @return {?}
         */
        function () {
            return this.selectedDateString;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.selectedDateString = value;
            // console.log('selectedDate', this.selectedDateString);
            this.onChange(this.selectedDateString);
            this.dateChanges.emit(this.selectedDateString);
            this.onTouched();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormDatepickerInputComponent.prototype, "calendarDateValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._calendarDateValue;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._calendarDateValue = value;
            this.onChangeDate();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.onChangeDate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this._selectionMode === DateSelectionMode.Range && this._calendarDateValue) {
            /** @type {?} */
            var dateRangeList = (/** @type {?} */ ((/** @type {?} */ (this._calendarDateValue))));
            /** @type {?} */
            var newSelectedDateList_1 = [];
            dateRangeList.forEach(function (date) {
                if (date) {
                    newSelectedDateList_1.push(_this.convertDateToISO(date));
                }
            });
            if (newSelectedDateList_1.length === 2) {
                this.selectedDate = newSelectedDateList_1;
            }
        }
        else {
            if (this.selectedDateString && !this._calendarDateValue) {
                this.selectedDate = null;
            }
            else if (this._calendarDateValue) {
                if (this.defaultSettings.showYear) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                }
                if (this.defaultSettings.showTimeOnly) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                    this._calendarDateValue.setFullYear(this.defaultSettings.defaultYear);
                }
                if (!this.defaultSettings.enableSeconds && this._calendarDateValue.setSeconds) {
                    this._calendarDateValue.setSeconds(0);
                }
                /** @type {?} */
                var calendarDateValueISO = this.convertDateToISO(this._calendarDateValue);
                if (this.defaultSettings.disableUTC) {
                    calendarDateValueISO = moment(this._calendarDateValue).format('YYYY-MM-DDTHH:mm:ss');
                }
                if (calendarDateValueISO !== this.selectedDateString) {
                    this.selectedDate = calendarDateValueISO;
                }
            }
        }
    };
    /**
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.initiateYearDataSource = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var dataSource = [];
        for (var i = ((new Date()).getFullYear() + 30); i > 1922; i--) {
            dataSource.push({
                Value: i.toString(),
                Label: i.toString()
            });
        }
        dataSource.push({ Value: '2900', Label: '2900' });
        this.yearDataSource = dataSource;
    };
    /**
     * @param {?} selectedData
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.onYearValueChanged = /**
     * @param {?} selectedData
     * @return {?}
     */
    function (selectedData) {
        if (selectedData) {
            /** @type {?} */
            var month = (this.defaultSettings.defaultMonth + 1).toString();
            if (month.length < 2) {
                month = '0' + month;
            }
            /** @type {?} */
            var day = (this.defaultSettings.defaultDay).toString();
            if (day.length < 2) {
                day = '0' + day;
            }
            this.selectedDate = selectedData['Value'] + '-' + month + '-' + day + 'T00:00:00.000Z';
        }
        else {
            this.selectedDate = null;
        }
    };
    /**
     * @param {?} date
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.convertDateToISO = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        /** @type {?} */
        var newDate = new Date(date);
        /** @type {?} */
        var year = newDate.getFullYear();
        /** @type {?} */
        var month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        var day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    };
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.convertDateToDateUTC = /**
     * @param {?} dateAsString
     * @return {?}
     */
    function (dateAsString) {
        /** @type {?} */
        var newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    };
    /**
     * @return {?}
     */
    FormDatepickerInputComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.translateSubscription.unsubscribe();
    };
    FormDatepickerInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-form-datepicker-input',
                    template: "<div class=\"dp-container form-datepicker-input\" [class.disable-selection]=\"isReadOnly\"\r\n     [class.hidden-btn]=\"defaultSettings.hideCalendarButton\">\r\n  <p-calendar class=\"calendar-fit\" *ngIf=\"!defaultSettings.showYear\" [(ngModel)]=\"calendarDateValue\"\r\n              [dateFormat]=\"defaultSettings.dateFormat\" [timeOnly]=\"defaultSettings.showTimeOnly\"\r\n              [disabled]=\"isDisabled\" [showTime]=\"defaultSettings.showTime\"\r\n              [showIcon]=\"!defaultSettings.hideCalendarButton\" [showButtonBar]=\"defaultSettings.showButtonBar\"\r\n              [minDate]=\"minDate\" [maxDate]=\"maxDate\" [locale]=\"locale\" [view]=\"view\" [selectionMode]=\"_selectionMode\"\r\n              [style]=\"{'width':'100%', 'height': '30px', 'top': '-2px'}\" monthNavigator=\"true\" yearNavigator=\"true\"\r\n              yearRange=\"1930:2030\" placeholder=\"{{placeholder|translate}}\" appendTo=\"body\"></p-calendar>\r\n  <!--\r\n  <div [hidden]=\"hideCalendarButton\" class=\"dp-btn-container\">\r\n    <button class=\"btn btn-success dp-btn\" type=\"button\" (click)=\"dpStart.show(); $event.stopPropagation();\">\r\n      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n    </button>\r\n  </div> -->\r\n</div>\r\n\r\n<div *ngIf=\"formGroup && defaultSettings.showYear\" [formGroup]=\"formGroup\" class=\"form-datepicker-input\"\r\n     [class.disable-selection]=\"isReadOnly\">\r\n  <layout-static-selector formControlName=\"Year\" [dataSource]=\"yearDataSource\" [valueField]=\"'Value'\"\r\n                          [labelField]=\"'Label'\" [isDisabled]=\"isDisabled\" [placeholder]=\"placeholder\"\r\n                          (changed)=\"onYearValueChanged($event)\"></layout-static-selector>\r\n</div>",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return FormDatepickerInputComponent; }),
                            multi: true
                        }
                    ],
                    styles: [".dp-btn-container,.dp-container{display:flex}.dp-input{height:26px!important;border-radius:3px 0 0 3px}.dp-btn{border-radius:0 3px 3px 0;border:none;height:26px}.bs-timepicker-field{height:30px!important;width:50px!important}:host /deep/ .ui-calendar .ui-inputtext{border:1px solid #ced4da;height:30px!important;font-size:inherit;width:calc(100% - 33px);border-right:1px solid #ced4da;padding-left:10px}:host /deep/ .ui-calendar .ui-calendar-button{height:30px;top:2px;background-color:#3594e8;border-color:#3594e8}:host /deep/ .ui-calendar .ui-inputtext:disabled{background-color:#ebebe4}:host /deep/ .ui-calendar .ui-state-disabled,:host /deep/ .ui-calendar .ui-widget:disabled{opacity:1}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext{width:calc(100%)}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext:hover{border-right:1px solid #000!important}::ng-deep .ui-datepicker table td{padding:.2em!important;font-size:12px}::ng-deep .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}::ng-deep .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}.calendar-fit{width:calc(100%)}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-calendar .ui-inputtext{background-color:#e9ecef}"]
                }] }
    ];
    /** @nocollapse */
    FormDatepickerInputComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    FormDatepickerInputComponent.propDecorators = {
        settings: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        dateChanges: [{ type: Output }],
        placeholder: [{ type: Input }],
        selectionMode: [{ type: Input }]
    };
    return FormDatepickerInputComponent;
}());
export { FormDatepickerInputComponent };
if (false) {
    /** @type {?} */
    FormDatepickerInputComponent.prototype._calendarDateValue;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.selectedDateString;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.usedMometToDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.settings;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.isDisabled;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.isReadOnly;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.dateChanges;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.placeholder;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.selectionMode;
    /** @type {?} */
    FormDatepickerInputComponent.prototype._selectionMode;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.defaultSettings;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.view;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.yearDataSource;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.formGroup;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.minDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.maxDate;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.locale;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.localeTr;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.localEn;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.translateSubscription;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.onChange;
    /** @type {?} */
    FormDatepickerInputComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    FormDatepickerInputComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1kYXRlcGlja2VyLWlucHV0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0tZWxlbWVudHMvaW5wdXQtZGF0ZXBpY2tlci9mb3JtLWRhdGVwaWNrZXItaW5wdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsVUFBVSxFQUFpQixZQUFZLEVBQUUsTUFBTSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hJLE9BQU8sRUFBRSxpQkFBaUIsRUFBd0IsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFhLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR3ZELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNuRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUU1RTtJQTBDRSxzQ0FBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXhFLGlCQXFDQztRQXJDbUIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBekIvRCxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGdCQUFXLEdBQXVCLElBQUksWUFBWSxFQUFFLENBQUM7UUFJL0QsbUJBQWMsR0FBc0IsaUJBQWlCLENBQUMsTUFBTSxDQUFDO1FBZ0I3RCxhQUFRLEdBQVEsY0FBUSxDQUFDLENBQUM7UUFDMUIsY0FBUyxHQUFRLGNBQVEsQ0FBQyxDQUFDO1FBSXpCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ25CLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLElBQUksRUFBRSxJQUFJO1NBQ1gsQ0FBQyxDQUFDO1FBRUgsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxPQUFPLEdBQUc7WUFDYixjQUFjLEVBQUUsQ0FBQztZQUNqQixRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7WUFDeEYsYUFBYSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDO1lBQ2hFLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQztZQUN2RCxVQUFVLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQztZQUN0SSxlQUFlLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNyRyxLQUFLLEVBQUUsT0FBTztZQUNkLEtBQUssRUFBRSxPQUFPO1NBQ2YsQ0FBQztRQUNGLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ2QsY0FBYyxFQUFFLENBQUM7WUFDakIsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDO1lBQ3JGLGFBQWEsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNoRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7WUFDdkQsVUFBVSxFQUFFLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUM7WUFDM0gsZUFBZSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDckcsS0FBSyxFQUFFLE9BQU87WUFDZCxLQUFLLEVBQUUsU0FBUztTQUNqQixDQUFDO1FBQ0YsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM1QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBVTtZQUM1RSxJQUFJLEtBQUssRUFBRTtnQkFDVCxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDO2FBQ2xFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGtEQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUMvQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7U0FDMUM7UUFDRCxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNyQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRXpFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN0RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDO2FBQ25EO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMvQjtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7Z0JBQy9DLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzthQUM1QztZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUN4QztZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDO2FBQ3JCO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNqRTtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDakU7U0FDRjtJQUNILENBQUM7Ozs7O0lBRUQsdURBQWdCOzs7O0lBQWhCLFVBQWlCLEVBQUU7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxpREFBVTs7OztJQUFWLFVBQVcsS0FBYTtRQUN0QixJQUFJLEtBQUssRUFBRTtZQUVULElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTs7O29CQUVuSSxhQUFhLEdBQWEsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBQ2hELGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxlQUFlLENBQUM7Z0JBQ25DLEtBQUssR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssS0FBSyxFQUFFO2dCQUNyQyw0RkFBNEY7Z0JBQzVGLHNEQUFzRDtnQkFDdEQsZ0NBQWdDO2dCQUNoQyxrRUFBa0U7Z0JBQ2xFLHNGQUFzRjtnQkFDdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNqRyw2Q0FBNkM7b0JBQzdDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ2pELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO2lCQUM3QjtxQkFBTTtvQkFDTCxpREFBaUQ7b0JBQ2pELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVEO2dCQUVELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO29CQUN0QixJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7aUJBQ25DLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2FBQ2pDO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDL0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUNoQztJQUNILENBQUM7Ozs7O0lBRUQsd0RBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELHNCQUFJLHNEQUFZOzs7O1FBQWhCO1lBQ0UsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDakMsQ0FBQzs7Ozs7UUFFRCxVQUFpQixLQUF3QjtZQUN2QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLHdEQUF3RDtZQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDOzs7T0FSQTtJQVlELHNCQUFJLDJEQUFpQjs7OztRQUFyQjtZQUNFLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO1FBQ2pDLENBQUM7Ozs7O1FBRUQsVUFBc0IsS0FBVztZQUMvQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN0QixDQUFDOzs7T0FMQTs7OztJQU9ELG1EQUFZOzs7SUFBWjtRQUFBLGlCQTBDQztRQXpDQyxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssaUJBQWlCLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs7Z0JBQ3hFLGFBQWEsR0FBRyxtQkFBQSxtQkFBQSxJQUFJLENBQUMsa0JBQWtCLEVBQVcsRUFBUzs7Z0JBQzNELHFCQUFtQixHQUFhLEVBQUU7WUFDeEMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQVU7Z0JBQy9CLElBQUksSUFBSSxFQUFFO29CQUNSLHFCQUFtQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDdkQ7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUkscUJBQW1CLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLFlBQVksR0FBRyxxQkFBbUIsQ0FBQzthQUN6QztTQUNGO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDdkQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7YUFDMUI7aUJBQU0sSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ2xDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDcEUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNsRTtnQkFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO29CQUNyQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3BFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDakUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUN2RTtnQkFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRTtvQkFDN0UsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdkM7O29CQUVHLG9CQUFvQixHQUFXLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7Z0JBQ2pGLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUU7b0JBQ25DLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztpQkFDdEY7Z0JBRUQsSUFBSSxvQkFBb0IsS0FBSyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQW9CLENBQUM7aUJBQzFDO2FBQ0Y7U0FDRjtJQUVILENBQUM7Ozs7SUFFRCw2REFBc0I7OztJQUF0Qjs7WUFDUSxVQUFVLEdBQWUsRUFBRTtRQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3RCxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUNkLEtBQUssRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFO2dCQUNuQixLQUFLLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRTthQUNwQixDQUFDLENBQUM7U0FDSjtRQUNELFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsVUFBVSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQseURBQWtCOzs7O0lBQWxCLFVBQW1CLFlBQWlCO1FBQ2xDLElBQUksWUFBWSxFQUFFOztnQkFDWixLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7WUFDOUQsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDcEIsS0FBSyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7YUFDckI7O2dCQUVHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxFQUFFO1lBQ3RELElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xCLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO2FBQ2pCO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLGdCQUFnQixDQUFDO1NBQ3hGO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUMxQjtJQUVILENBQUM7Ozs7O0lBRUQsdURBQWdCOzs7O0lBQWhCLFVBQWlCLElBQVU7O1lBQ25CLE9BQU8sR0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7O1lBQ2xDLElBQUksR0FBRyxPQUFPLENBQUMsV0FBVyxFQUFFOztZQUMxQixLQUFLLEdBQUcsRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQzs7WUFDdkMsR0FBRyxHQUFHLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFO1FBRTlCLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEIsS0FBSyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7U0FDckI7UUFDRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO1NBQ2pCO1FBQ0QsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLGdCQUFnQixDQUFDO0lBQ3pELENBQUM7Ozs7O0lBRUQsMkRBQW9COzs7O0lBQXBCLFVBQXFCLFlBQW9COztZQUNqQyxPQUFPLEdBQVMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzVDLE9BQU8sSUFBSSxJQUFJLENBQ2IsT0FBTyxDQUFDLGNBQWMsRUFBRSxFQUN4QixPQUFPLENBQUMsV0FBVyxFQUFFLEVBQ3JCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFDcEIsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUNyQixPQUFPLENBQUMsYUFBYSxFQUFFLEVBQ3ZCLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxrREFBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDM0MsQ0FBQzs7Z0JBblNGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsOEJBQThCO29CQUN4QyxndURBQXFEO29CQUVyRCxTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsNEJBQTRCLEVBQTVCLENBQTRCLENBQUM7NEJBQzNELEtBQUssRUFBRSxJQUFJO3lCQUNaO3FCQUFDOztpQkFDTDs7OztnQkFqQm1CLFdBQVc7Z0JBQ3RCLGdCQUFnQjs7OzJCQXFCdEIsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7OEJBQ0wsTUFBTTs4QkFDTixLQUFLO2dDQUNMLEtBQUs7O0lBZ1JSLG1DQUFDO0NBQUEsQUFwU0QsSUFvU0M7U0F6UlksNEJBQTRCOzs7SUFDdkMsMERBQXlCOztJQUN6QiwwREFBc0M7Ozs7O0lBQ3RDLHVEQUFpQzs7SUFDakMsZ0RBQWlDOztJQUNqQyxrREFBOEI7O0lBQzlCLGtEQUE0Qjs7SUFDNUIsbURBQStEOztJQUMvRCxtREFBOEI7O0lBQzlCLHFEQUEyQzs7SUFFM0Msc0RBQTZEOztJQUU3RCx1REFBOEI7O0lBQzlCLDRDQUFhOztJQUViLHNEQUEyQjs7SUFDM0IsaURBQXFCOztJQUVyQiwrQ0FBYzs7SUFDZCwrQ0FBYzs7SUFFZCw4Q0FBWTs7SUFDWixnREFBYzs7SUFDZCwrQ0FBYTs7SUFDYiw2REFBb0M7O0lBRXBDLGdEQUEwQjs7SUFDMUIsaURBQTJCOzs7OztJQUdmLDBDQUF1Qjs7Ozs7SUFBRSxpREFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25DaGFuZ2VzLCBJbnB1dCwgZm9yd2FyZFJlZiwgU2ltcGxlQ2hhbmdlcywgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiwgQ29udHJvbFZhbHVlQWNjZXNzb3IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgRGF0ZVNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vbW9kZWxzL2RhdGUtc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRlU2VsZWN0aW9uTW9kZSB9IGZyb20gJy4uLy4uLy4uL2VudW1zL2RhdGUtc2VsZWN0aW9uLW1vZGUuZW51bSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1mb3JtLWRhdGVwaWNrZXItaW5wdXQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb3JtLWRhdGVwaWNrZXItaW5wdXQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2Zvcm0tZGF0ZXBpY2tlci1pbnB1dC5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRm9ybURhdGVwaWNrZXJJbnB1dENvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybURhdGVwaWNrZXJJbnB1dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcywgT25EZXN0cm95LCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcbiAgX2NhbGVuZGFyRGF0ZVZhbHVlOiBEYXRlO1xyXG4gIHNlbGVjdGVkRGF0ZVN0cmluZzogc3RyaW5nIHwgc3RyaW5nW107XHJcbiAgcHJpdmF0ZSB1c2VkTW9tZXRUb0RhdGU6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgc2V0dGluZ3M/OiBEYXRlU2V0dGluZ3M7XHJcbiAgQElucHV0KCkgaXNEaXNhYmxlZD86IGJvb2xlYW47XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG4gIEBPdXRwdXQoKSBkYXRlQ2hhbmdlcz86IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHNlbGVjdGlvbk1vZGU/OiBEYXRlU2VsZWN0aW9uTW9kZTtcclxuXHJcbiAgX3NlbGVjdGlvbk1vZGU6IERhdGVTZWxlY3Rpb25Nb2RlID0gRGF0ZVNlbGVjdGlvbk1vZGUuU2luZ2xlO1xyXG5cclxuICBkZWZhdWx0U2V0dGluZ3M6IERhdGVTZXR0aW5ncztcclxuICB2aWV3OiBzdHJpbmc7XHJcblxyXG4gIHllYXJEYXRhU291cmNlOiBBcnJheTxhbnk+O1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG5cclxuICBtaW5EYXRlOiBEYXRlO1xyXG4gIG1heERhdGU6IERhdGU7XHJcblxyXG4gIGxvY2FsZTogYW55O1xyXG4gIGxvY2FsZVRyOiBhbnk7XHJcbiAgbG9jYWxFbjogYW55O1xyXG4gIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBvbkNoYW5nZTogYW55ID0gKCkgPT4geyB9O1xyXG4gIG9uVG91Y2hlZDogYW55ID0gKCkgPT4geyB9O1xyXG5cclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLnZpZXcgPSAnZGF0ZSc7XHJcbiAgICB0aGlzLnllYXJEYXRhU291cmNlID0gW107XHJcbiAgICB0aGlzLmRlZmF1bHRTZXR0aW5ncyA9IG5ldyBEYXRlU2V0dGluZ3MoKTtcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgIFllYXI6IG51bGxcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIHVudGlsIHRoZSBiZXR0ZXIgc29sdXRpb24gZm9yIHRyYW5zbGF0ZS5cclxuICAgIHRoaXMubG9jYWxFbiA9IHtcclxuICAgICAgZmlyc3REYXlPZldlZWs6IDEsXHJcbiAgICAgIGRheU5hbWVzOiBbJ1N1bmRheScsICdNb25kYXknLCAnVHVlc2RheScsICdXZWRuZXNkYXknLCAnVGh1cnNkYXknLCAnRnJpZGF5JywgJ1NhdHVyZGF5J10sXHJcbiAgICAgIGRheU5hbWVzU2hvcnQ6IFsnU3VuJywgJ01vbicsICdUdWUnLCAnV2VkJywgJ1RodScsICdGcmknLCAnU2F0J10sXHJcbiAgICAgIGRheU5hbWVzTWluOiBbJ1N1JywgJ01vJywgJ1R1JywgJ1dlJywgJ1RoJywgJ0ZyJywgJ1NhJ10sXHJcbiAgICAgIG1vbnRoTmFtZXM6IFsnSmFudWFyeScsICdGZWJydWFyeScsICdNYXJjaCcsICdBcHJpbCcsICdNYXknLCAnSnVuZScsICdKdWx5JywgJ0F1Z3VzdCcsICdTZXB0ZW1iZXInLCAnT2N0b2JlcicsICdOb3ZlbWJlcicsICdEZWNlbWJlciddLFxyXG4gICAgICBtb250aE5hbWVzU2hvcnQ6IFsnSmFuJywgJ0ZlYicsICdNYXInLCAnQXByJywgJ01heScsICdKdW4nLCAnSnVsJywgJ0F1ZycsICdTZXAnLCAnT2N0JywgJ05vdicsICdEZWMnXSxcclxuICAgICAgdG9kYXk6ICdUb2RheScsXHJcbiAgICAgIGNsZWFyOiAnQ2xlYXInXHJcbiAgICB9O1xyXG4gICAgLy8gdW50aWwgdGhlIGJldHRlciBzb2x1dGlvbiBmb3IgdHJhbnNsYXRlLlxyXG4gICAgdGhpcy5sb2NhbGVUciA9IHtcclxuICAgICAgZmlyc3REYXlPZldlZWs6IDEsXHJcbiAgICAgIGRheU5hbWVzOiBbJ1BhemFyJywgJ1BhemFydGVzaScsICdTYWzEsScsICfDh2FyxZ9hbWJhJywgJ1BlcsWfZW1iZScsICdDdW1hJywgJ0N1bWFydGVzaSddLFxyXG4gICAgICBkYXlOYW1lc1Nob3J0OiBbJ1BheicsICdQdHMnLCAnU2FsJywgJ8OHYXInLCAnUGVyJywgJ0N1bScsICdDdHMnXSxcclxuICAgICAgZGF5TmFtZXNNaW46IFsnUHonLCAnUHQnLCAnU2wnLCAnw4dyJywgJ1ByJywgJ0NtJywgJ0N0J10sXHJcbiAgICAgIG1vbnRoTmFtZXM6IFsnT2NhaycsICfFnnViYXQnLCAnTWFydCcsICdOaXNhbicsICdNYXnEsXMnLCAnSGF6aXJhbicsICdUZW1tdXonLCAnQcSfdXN0b3MnLCAnRXlsw7xsJywgJ0VraW0nLCAnS2FzxLFtJywgJ0FyYWzEsWsnXSxcclxuICAgICAgbW9udGhOYW1lc1Nob3J0OiBbJ09jYScsICfFnnViJywgJ01hcicsICdOaXMnLCAnTWF5JywgJ0hheicsICdUZW0nLCAnQcSfdScsICdFeWwnLCAnRWtpJywgJ0thcycsICdBcmEnXSxcclxuICAgICAgdG9kYXk6ICdCdWfDvG4nLFxyXG4gICAgICBjbGVhcjogJ1RlbWl6bGUnXHJcbiAgICB9O1xyXG4gICAgLy8gdW50aWwgdGhlIGJldHRlciBzb2x1dGlvbiBmb3IgdHJhbnNsYXRlLlxyXG4gICAgdGhpcy5sb2NhbGUgPSB0aGlzLmxvY2FsZVRyO1xyXG4gICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24gPSB0aGlzLnRyYW5zbGF0ZS5vbkxhbmdDaGFuZ2Uuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgIHRoaXMubG9jYWxlID0gZXZlbnQubGFuZyA9PT0gJ3RyJyA/IHRoaXMubG9jYWxlVHIgOiB0aGlzLmxvY2FsRW47XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMuc2VsZWN0aW9uTW9kZSAmJiB0aGlzLnNlbGVjdGlvbk1vZGUpIHtcclxuICAgICAgdGhpcy5fc2VsZWN0aW9uTW9kZSA9IHRoaXMuc2VsZWN0aW9uTW9kZTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzLnNldHRpbmdzICYmIHRoaXMuc2V0dGluZ3MpIHtcclxuICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MgPSBfLmFzc2lnbih7fSwgdGhpcy5kZWZhdWx0U2V0dGluZ3MsIHRoaXMuc2V0dGluZ3MpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3Muc2VsZWN0aW9uTW9kZSAmJiAhdGhpcy5zZWxlY3Rpb25Nb2RlKSB7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0aW9uTW9kZSA9IHRoaXMuc2V0dGluZ3Muc2VsZWN0aW9uTW9kZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3Muc2hvd1llYXIpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRTZXR0aW5ncy5kYXRlRm9ybWF0ID0gJ3l5JztcclxuICAgICAgICB0aGlzLmluaXRpYXRlWWVhckRhdGFTb3VyY2UoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3Muc2hvd1RpbWVPbmx5KSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MuZGlzYWJsZVVUQyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MuaGlkZUNhbGVuZGFyQnV0dG9uID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmRlZmF1bHRTZXR0aW5ncy5zaG93QnV0dG9uQmFyID0gZmFsc2U7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNob3dUaW1lKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0U2V0dGluZ3MuZGlzYWJsZVVUQyA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNob3dNb250aFBpY2tlcikge1xyXG4gICAgICAgIHRoaXMudmlldyA9ICdtb250aCc7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLm1pbkRhdGUpIHtcclxuICAgICAgICB0aGlzLm1pbkRhdGUgPSB0aGlzLmNvbnZlcnREYXRlVG9EYXRlVVRDKHRoaXMuc2V0dGluZ3MubWluRGF0ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLm1heERhdGUpIHtcclxuICAgICAgICB0aGlzLm1heERhdGUgPSB0aGlzLmNvbnZlcnREYXRlVG9EYXRlVVRDKHRoaXMuc2V0dGluZ3MubWF4RGF0ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25DaGFuZ2UoZm4pIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcblxyXG4gICAgICBpZiAodmFsdWUuaW5kZXhPZignVCcpICE9PSAtMSAmJiAhdGhpcy5kZWZhdWx0U2V0dGluZ3MuZGlzYWJsZVVUQyAmJiAhdGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd1RpbWVPbmx5ICYmICF0aGlzLmRlZmF1bHRTZXR0aW5ncy5zaG93VGltZSkge1xyXG4gICAgICAgIC8vIGNhbiBiZSBkb25lIHZpYSBtb21lbnRcclxuICAgICAgICBjb25zdCBzcGxpdHRlZFZhbHVlOiBzdHJpbmdbXSA9IHZhbHVlLnNwbGl0KCdUJyk7XHJcbiAgICAgICAgc3BsaXR0ZWRWYWx1ZVsxXSA9ICcwMDowMDowMC4wMDBaJztcclxuICAgICAgICB2YWx1ZSA9IHNwbGl0dGVkVmFsdWUuam9pbignVCcpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZERhdGVTdHJpbmcgIT09IHZhbHVlKSB7XHJcbiAgICAgICAgLy8gZHVlIHRvIFVUQywgd2l0aG91dCBiZWluZyBpbmZsdWVuY2VkIHRpbWV6b25lIGNoYW5nZXMsIGl0IGNyZWF0ZXMgZGF0ZSBhY2NvcmRpbmcgdG8gdmFsdWVcclxuICAgICAgICAvLyBpbiBvcmRlciB0byBhdm9pZCByZWZsZWN0aW5nIHRpbWV6b25lIGNoYW5nZXMgdG8gVUlcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygndmFsdWUgJywgdmFsdWUpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdtb21lbnQodmFsdWUpLnRvRGF0ZSgpICcsIG1vbWVudCh2YWx1ZSkudG9EYXRlKCkpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCd0aGlzLmNvbnZlcnREYXRlVG9EYXRlVVRDKHZhbHVlKSAnLCB0aGlzLmNvbnZlcnREYXRlVG9EYXRlVVRDKHZhbHVlKSk7XHJcbiAgICAgICAgaWYgKCF0aGlzLnVzZWRNb21ldFRvRGF0ZSAmJiAodGhpcy5kZWZhdWx0U2V0dGluZ3Muc2hvd1RpbWVPbmx5IHx8IHRoaXMuZGVmYXVsdFNldHRpbmdzLnNob3dUaW1lKSkge1xyXG4gICAgICAgICAgLy8gZmlyc3QgaW5pdGlhdGlvbiBuZWVkcyB0byBiZSBkb25lIHRoaXMgd2F5XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IG1vbWVudCh2YWx1ZSkudG9EYXRlKCk7XHJcbiAgICAgICAgICB0aGlzLnVzZWRNb21ldFRvRGF0ZSA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIGFmdGVyIHRoZSBpbml0aWF0aW9uIG5lZWRzIHRvIGJlIHVzZWQgdGhpcyB3YXlcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlID0gdGhpcy5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyh2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmZvcm1Hcm91cC5zZXRWYWx1ZSh7XHJcbiAgICAgICAgICBZZWFyOiBtb21lbnQodmFsdWUpLmZvcm1hdCgnWVlZWScpXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERhdGVTdHJpbmcgPSB2YWx1ZTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSBudWxsO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0ZVN0cmluZyA9IG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uVG91Y2hlZChmbikge1xyXG4gICAgdGhpcy5vblRvdWNoZWQgPSBmbjtcclxuICB9XHJcblxyXG4gIGdldCBzZWxlY3RlZERhdGUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RlZERhdGVTdHJpbmc7XHJcbiAgfVxyXG5cclxuICBzZXQgc2VsZWN0ZWREYXRlKHZhbHVlOiBzdHJpbmcgfCBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy5zZWxlY3RlZERhdGVTdHJpbmcgPSB2YWx1ZTtcclxuICAgIC8vIGNvbnNvbGUubG9nKCdzZWxlY3RlZERhdGUnLCB0aGlzLnNlbGVjdGVkRGF0ZVN0cmluZyk7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nKTtcclxuICAgIHRoaXMuZGF0ZUNoYW5nZXMuZW1pdCh0aGlzLnNlbGVjdGVkRGF0ZVN0cmluZyk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gIH1cclxuXHJcblxyXG5cclxuICBnZXQgY2FsZW5kYXJEYXRlVmFsdWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWU7XHJcbiAgfVxyXG5cclxuICBzZXQgY2FsZW5kYXJEYXRlVmFsdWUodmFsdWU6IERhdGUpIHtcclxuICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlID0gdmFsdWU7XHJcbiAgICB0aGlzLm9uQ2hhbmdlRGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2VEYXRlKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuX3NlbGVjdGlvbk1vZGUgPT09IERhdGVTZWxlY3Rpb25Nb2RlLlJhbmdlICYmIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IGRhdGVSYW5nZUxpc3QgPSB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSBhcyB1bmtub3duIGFzIGFueVtdO1xyXG4gICAgICBjb25zdCBuZXdTZWxlY3RlZERhdGVMaXN0OiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgICBkYXRlUmFuZ2VMaXN0LmZvckVhY2goKGRhdGU6IERhdGUpID0+IHtcclxuICAgICAgICBpZiAoZGF0ZSkge1xyXG4gICAgICAgICAgbmV3U2VsZWN0ZWREYXRlTGlzdC5wdXNoKHRoaXMuY29udmVydERhdGVUb0lTTyhkYXRlKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGlmIChuZXdTZWxlY3RlZERhdGVMaXN0Lmxlbmd0aCA9PT0gMikge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gbmV3U2VsZWN0ZWREYXRlTGlzdDtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWREYXRlU3RyaW5nICYmICF0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gbnVsbDtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmRlZmF1bHRTZXR0aW5ncy5zaG93WWVhcikge1xyXG4gICAgICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUuc2V0TW9udGgodGhpcy5kZWZhdWx0U2V0dGluZ3MuZGVmYXVsdE1vbnRoKTtcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldERhdGUodGhpcy5kZWZhdWx0U2V0dGluZ3MuZGVmYXVsdERheSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRlZmF1bHRTZXR0aW5ncy5zaG93VGltZU9ubHkpIHtcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldE1vbnRoKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHRNb250aCk7XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZS5zZXREYXRlKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHREYXkpO1xyXG4gICAgICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUuc2V0RnVsbFllYXIodGhpcy5kZWZhdWx0U2V0dGluZ3MuZGVmYXVsdFllYXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmRlZmF1bHRTZXR0aW5ncy5lbmFibGVTZWNvbmRzICYmIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldFNlY29uZHMpIHtcclxuICAgICAgICAgIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlLnNldFNlY29uZHMoMCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY2FsZW5kYXJEYXRlVmFsdWVJU086IHN0cmluZyA9IHRoaXMuY29udmVydERhdGVUb0lTTyh0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRpc2FibGVVVEMpIHtcclxuICAgICAgICAgIGNhbGVuZGFyRGF0ZVZhbHVlSVNPID0gbW9tZW50KHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKS5mb3JtYXQoJ1lZWVktTU0tRERUSEg6bW06c3MnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjYWxlbmRhckRhdGVWYWx1ZUlTTyAhPT0gdGhpcy5zZWxlY3RlZERhdGVTdHJpbmcpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gY2FsZW5kYXJEYXRlVmFsdWVJU087XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgaW5pdGlhdGVZZWFyRGF0YVNvdXJjZSgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGRhdGFTb3VyY2U6IEFycmF5PGFueT4gPSBbXTtcclxuICAgIGZvciAobGV0IGkgPSAoKG5ldyBEYXRlKCkpLmdldEZ1bGxZZWFyKCkgKyAzMCk7IGkgPiAxOTIyOyBpLS0pIHtcclxuICAgICAgZGF0YVNvdXJjZS5wdXNoKHtcclxuICAgICAgICBWYWx1ZTogaS50b1N0cmluZygpLFxyXG4gICAgICAgIExhYmVsOiBpLnRvU3RyaW5nKClcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBkYXRhU291cmNlLnB1c2goeyBWYWx1ZTogJzI5MDAnLCBMYWJlbDogJzI5MDAnIH0pO1xyXG4gICAgdGhpcy55ZWFyRGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgfVxyXG5cclxuICBvblllYXJWYWx1ZUNoYW5nZWQoc2VsZWN0ZWREYXRhOiBhbnkpIHtcclxuICAgIGlmIChzZWxlY3RlZERhdGEpIHtcclxuICAgICAgbGV0IG1vbnRoID0gKHRoaXMuZGVmYXVsdFNldHRpbmdzLmRlZmF1bHRNb250aCArIDEpLnRvU3RyaW5nKCk7XHJcbiAgICAgIGlmIChtb250aC5sZW5ndGggPCAyKSB7XHJcbiAgICAgICAgbW9udGggPSAnMCcgKyBtb250aDtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IGRheSA9ICh0aGlzLmRlZmF1bHRTZXR0aW5ncy5kZWZhdWx0RGF5KS50b1N0cmluZygpO1xyXG4gICAgICBpZiAoZGF5Lmxlbmd0aCA8IDIpIHtcclxuICAgICAgICBkYXkgPSAnMCcgKyBkYXk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc2VsZWN0ZWREYXRlID0gc2VsZWN0ZWREYXRhWydWYWx1ZSddICsgJy0nICsgbW9udGggKyAnLScgKyBkYXkgKyAnVDAwOjAwOjAwLjAwMFonO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGUgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIGNvbnZlcnREYXRlVG9JU08oZGF0ZTogRGF0ZSk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBuZXdEYXRlOiBEYXRlID0gbmV3IERhdGUoZGF0ZSksXHJcbiAgICAgIHllYXIgPSBuZXdEYXRlLmdldEZ1bGxZZWFyKCk7XHJcbiAgICBsZXQgbW9udGggPSAnJyArIChuZXdEYXRlLmdldE1vbnRoKCkgKyAxKSxcclxuICAgICAgZGF5ID0gJycgKyBuZXdEYXRlLmdldERhdGUoKTtcclxuXHJcbiAgICBpZiAobW9udGgubGVuZ3RoIDwgMikge1xyXG4gICAgICBtb250aCA9ICcwJyArIG1vbnRoO1xyXG4gICAgfVxyXG4gICAgaWYgKGRheS5sZW5ndGggPCAyKSB7XHJcbiAgICAgIGRheSA9ICcwJyArIGRheTtcclxuICAgIH1cclxuICAgIHJldHVybiBbeWVhciwgbW9udGgsIGRheV0uam9pbignLScpICsgJ1QwMDowMDowMC4wMDBaJztcclxuICB9XHJcblxyXG4gIGNvbnZlcnREYXRlVG9EYXRlVVRDKGRhdGVBc1N0cmluZzogc3RyaW5nKTogRGF0ZSB7XHJcbiAgICBjb25zdCBuZXdEYXRlOiBEYXRlID0gbmV3IERhdGUoZGF0ZUFzU3RyaW5nKTtcclxuICAgIHJldHVybiBuZXcgRGF0ZShcclxuICAgICAgbmV3RGF0ZS5nZXRVVENGdWxsWWVhcigpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ01vbnRoKCksXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDRGF0ZSgpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ0hvdXJzKCksXHJcbiAgICAgIG5ld0RhdGUuZ2V0VVRDTWludXRlcygpLFxyXG4gICAgICBuZXdEYXRlLmdldFVUQ1NlY29uZHMoKSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==