/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { CoreModalService } from '../../services/custom/core-modal.service';
var CoreModalComponent = /** @class */ (function () {
    function CoreModalComponent(coreModalService) {
        this.coreModalService = coreModalService;
        this.lockBackground = true;
        this.closeOnEscape = true;
        this.maximizable = false;
        this.draggable = true;
        this.closable = true;
        this.showHeader = true;
        this.width = 50;
        this.modalWidth = (window.screen.width) * this.width / 100;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    CoreModalComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.displayData) {
            if (this.displayData && this.displayData.display) {
                this._display = true;
            }
            else {
                this._display = false;
            }
        }
        if (changes.width) {
            if (this.width) {
                this.modalWidth = (window.screen.width) * this.width / 100;
            }
            else {
                this.modalWidth = null;
            }
        }
    };
    /**
     * @return {?}
     */
    CoreModalComponent.prototype.openModal = /**
     * @return {?}
     */
    function () {
        this.displayData = {
            display: true
        };
        this._display = true;
    };
    /**
     * @return {?}
     */
    CoreModalComponent.prototype.closeModal = /**
     * @return {?}
     */
    function () {
        this.displayData = {
            display: false
        };
        this._display = false;
    };
    /**
     * @return {?}
     */
    CoreModalComponent.prototype.onDialogShow = /**
     * @return {?}
     */
    function () {
        this.coreModalService.setModalDisplayStatus(true);
    };
    /**
     * @return {?}
     */
    CoreModalComponent.prototype.onDialogHide = /**
     * @return {?}
     */
    function () {
        this.coreModalService.setModalDisplayStatus(false);
    };
    CoreModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-modal',
                    template: "<p-dialog appendTo=\"body\" focusOnShow=\"false\" [(visible)]=\"_display\" [(modal)]=\"lockBackground\"\r\n  [(maximizable)]=\"maximizable\" [(draggable)]=\"draggable\" [(closable)]=\"closable\" [(showHeader)]=\"showHeader\"\r\n  [(closeOnEscape)]=\"closeOnEscape\" [autoZIndex]=\"false\" [blockScroll]=\"true\" [baseZIndex]=\"1001\" [width]=\"modalWidth\"\r\n  (onShow)=\"onDialogShow()\" (onHide)=\"onDialogHide()\">\r\n  <p-header *ngIf=\"modalTitle\">\r\n    {{modalTitle | translate}}\r\n  </p-header>\r\n  <div>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</p-dialog>",
                    styles: [".active-item:hover{z-index:2;color:#fff;background-color:#007bff;border-color:#007bff;cursor:pointer}::ng-deep .ui-dialog .ui-dialog-titlebar{background:#2d5f8b!important;color:#fff!important;font-weight:700!important;font-size:18px!important;padding:10px 15px!important;cursor:pointer!important;border:none!important}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:not(:hover){color:#fff!important;margin-top:2px}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:hover{margin-top:2px}::ng-deep .ui-dialog .ui-dialog-content{padding:30px!important}::ng-deep .ui-dialog{z-index:1001}::ng-deep .ui-widget-overlay{z-index:1000}:host /deep/ .ng-dropdown-panel.ng-select-bottom{position:fixed!important}"]
                }] }
    ];
    /** @nocollapse */
    CoreModalComponent.ctorParameters = function () { return [
        { type: CoreModalService }
    ]; };
    CoreModalComponent.propDecorators = {
        modalTitle: [{ type: Input }],
        lockBackground: [{ type: Input }],
        closeOnEscape: [{ type: Input }],
        maximizable: [{ type: Input }],
        draggable: [{ type: Input }],
        closable: [{ type: Input }],
        showHeader: [{ type: Input }],
        displayData: [{ type: Input }],
        width: [{ type: Input }]
    };
    return CoreModalComponent;
}());
export { CoreModalComponent };
if (false) {
    /** @type {?} */
    CoreModalComponent.prototype.modalTitle;
    /** @type {?} */
    CoreModalComponent.prototype.lockBackground;
    /** @type {?} */
    CoreModalComponent.prototype.closeOnEscape;
    /** @type {?} */
    CoreModalComponent.prototype.maximizable;
    /** @type {?} */
    CoreModalComponent.prototype.draggable;
    /** @type {?} */
    CoreModalComponent.prototype.closable;
    /** @type {?} */
    CoreModalComponent.prototype.showHeader;
    /** @type {?} */
    CoreModalComponent.prototype.displayData;
    /** @type {?} */
    CoreModalComponent.prototype.width;
    /** @type {?} */
    CoreModalComponent.prototype.modalWidth;
    /** @type {?} */
    CoreModalComponent.prototype._display;
    /**
     * @type {?}
     * @private
     */
    CoreModalComponent.prototype.coreModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1tb2RhbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLW1vZGFsL2NvcmUtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUU1RTtJQW9CRSw0QkFBb0IsZ0JBQWtDO1FBQWxDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFaN0MsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFFbEIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUViLGVBQVUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7SUFLN0QsQ0FBQzs7Ozs7SUFFRCx3Q0FBVzs7OztJQUFYLFVBQVksT0FBTztRQUNqQixJQUFJLE9BQU8sQ0FBQyxXQUFXLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO2dCQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtTQUNGO1FBRUQsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2pCLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQzthQUM1RDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUN4QjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELHNDQUFTOzs7SUFBVDtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsT0FBTyxFQUFFLElBQUk7U0FDZCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELHVDQUFVOzs7SUFBVjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7OztJQUdELHlDQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQseUNBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JELENBQUM7O2dCQS9ERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLDZrQkFBMEM7O2lCQUUzQzs7OztnQkFOUSxnQkFBZ0I7Ozs2QkFTdEIsS0FBSztpQ0FDTCxLQUFLO2dDQUNMLEtBQUs7OEJBQ0wsS0FBSzs0QkFDTCxLQUFLOzJCQUNMLEtBQUs7NkJBQ0wsS0FBSzs4QkFDTCxLQUFLO3dCQUNMLEtBQUs7O0lBaURSLHlCQUFDO0NBQUEsQUFoRUQsSUFnRUM7U0EzRFksa0JBQWtCOzs7SUFFN0Isd0NBQTZCOztJQUM3Qiw0Q0FBK0I7O0lBQy9CLDJDQUE4Qjs7SUFDOUIseUNBQTZCOztJQUM3Qix1Q0FBMEI7O0lBQzFCLHNDQUF5Qjs7SUFDekIsd0NBQTJCOztJQUMzQix5Q0FBMkI7O0lBQzNCLG1DQUFvQjs7SUFFcEIsd0NBQTZEOztJQUM3RCxzQ0FBeUI7Ozs7O0lBRWIsOENBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvcmVNb2RhbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jdXN0b20vY29yZS1tb2RhbC5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnY29yZS1tb2RhbCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvcmUtbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvcmUtbW9kYWwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZU1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgbW9kYWxUaXRsZT86IHN0cmluZztcclxuICBASW5wdXQoKSBsb2NrQmFja2dyb3VuZCA9IHRydWU7XHJcbiAgQElucHV0KCkgY2xvc2VPbkVzY2FwZSA9IHRydWU7XHJcbiAgQElucHV0KCkgbWF4aW1pemFibGUgPSBmYWxzZTtcclxuICBASW5wdXQoKSBkcmFnZ2FibGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGNsb3NhYmxlID0gdHJ1ZTtcclxuICBASW5wdXQoKSBzaG93SGVhZGVyID0gdHJ1ZTtcclxuICBASW5wdXQoKSBkaXNwbGF5RGF0YT86IGFueTtcclxuICBASW5wdXQoKSB3aWR0aCA9IDUwO1xyXG5cclxuICBwdWJsaWMgbW9kYWxXaWR0aCA9ICh3aW5kb3cuc2NyZWVuLndpZHRoKSAqIHRoaXMud2lkdGggLyAxMDA7XHJcbiAgcHVibGljIF9kaXNwbGF5OiBib29sZWFuO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvcmVNb2RhbFNlcnZpY2U6IENvcmVNb2RhbFNlcnZpY2UpIHtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzKTogdm9pZCB7XHJcbiAgICBpZiAoY2hhbmdlcy5kaXNwbGF5RGF0YSkge1xyXG4gICAgICBpZiAodGhpcy5kaXNwbGF5RGF0YSAmJiB0aGlzLmRpc3BsYXlEYXRhLmRpc3BsYXkpIHtcclxuICAgICAgICB0aGlzLl9kaXNwbGF5ID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9kaXNwbGF5ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAoY2hhbmdlcy53aWR0aCkge1xyXG4gICAgICBpZiAodGhpcy53aWR0aCkge1xyXG4gICAgICAgIHRoaXMubW9kYWxXaWR0aCA9ICh3aW5kb3cuc2NyZWVuLndpZHRoKSAqIHRoaXMud2lkdGggLyAxMDA7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5tb2RhbFdpZHRoID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb3Blbk1vZGFsKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHtcclxuICAgICAgZGlzcGxheTogdHJ1ZVxyXG4gICAgfTtcclxuICAgIHRoaXMuX2Rpc3BsYXkgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgY2xvc2VNb2RhbCgpIHtcclxuICAgIHRoaXMuZGlzcGxheURhdGEgPSB7XHJcbiAgICAgIGRpc3BsYXk6IGZhbHNlXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fZGlzcGxheSA9IGZhbHNlO1xyXG4gIH1cclxuXHJcblxyXG4gIG9uRGlhbG9nU2hvdygpOiB2b2lkIHtcclxuICAgIHRoaXMuY29yZU1vZGFsU2VydmljZS5zZXRNb2RhbERpc3BsYXlTdGF0dXModHJ1ZSk7XHJcbiAgfVxyXG5cclxuICBvbkRpYWxvZ0hpZGUoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNvcmVNb2RhbFNlcnZpY2Uuc2V0TW9kYWxEaXNwbGF5U3RhdHVzKGZhbHNlKTtcclxuICB9XHJcbn1cclxuIl19