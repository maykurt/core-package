/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter, forwardRef, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
var StaticSelectorComponent = /** @class */ (function () {
    function StaticSelectorComponent() {
        this.valueField = 'Id';
        this.labelField = 'Definition';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    StaticSelectorComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        if (changes.dataSource && this.selector && this.model) {
            if (this.dataSource && this.dataSource.length > 500) {
                this.virtualScroll = true;
            }
            // this.dataSource = [...this.dataSource, changes.dataSource];
            /** @type {?} */
            var index = _.findIndex(this.dataSource, ['Id', this.Model]);
            if (index === -1) {
                this.model = null;
            }
        }
        if (changes.dataSource && this.dataSource) {
            this.dataSource = _.orderBy(this.dataSource, [
                'OrderNo',
                function (z) { return z[_this._labelField] && (typeof z[_this._labelField] === 'string') ? z[_this._labelField].toLowerCase() : z[_this._labelField]; },
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                this._labelField
            ], ['asc']);
            this.findDisplayValue();
        }
        if (changes.labelField && this.labelField) {
            if (changes.labelField.currentValue instanceof Array) {
                this._labelField = 'CombinedFields';
            }
            else {
                this._labelField = (/** @type {?} */ (this.labelField));
            }
        }
        if (changes.dataSource || changes.labelField || changes.seperator) {
            if (this.dataSource && this.dataSource.length > 0 && this._labelField === 'CombinedFields') {
                this.dataSource.map(function (item) {
                    /** @type {?} */
                    var combinedValue = '';
                    ((/** @type {?} */ (_this.labelField))).forEach(function (field) {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + _this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -_this.seperator.length) : null;
                    return item;
                });
                this.findDisplayValue();
            }
        }
    };
    /**
     * @param {?} val
     * @return {?}
     */
    StaticSelectorComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    StaticSelectorComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(StaticSelectorComponent.prototype, "Model", {
        get: /**
         * @return {?}
         */
        function () {
            return this.model;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            var _this = this;
            this.model = val;
            this.onChange(val);
            this.onTouched();
            if (val) {
                if (this.dataSource) {
                    /** @type {?} */
                    var data = this.dataSource.find(function (x) { return x[_this.valueField] === _this.model; });
                    if (data) {
                        this.value = data[this._labelField];
                        this.changed.emit(data);
                    }
                }
                else {
                    /** @type {?} */
                    var emitData = {};
                    emitData[this.valueField] = this.model;
                    this.changed.emit(emitData);
                }
            }
            else {
                this.changed.emit(null);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    StaticSelectorComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    StaticSelectorComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    StaticSelectorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.model = value;
        this.findDisplayValue();
    };
    /**
     * @return {?}
     */
    StaticSelectorComponent.prototype.findDisplayValue = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // if (this.isReadOnly)
        if (this.dataSource && this.dataSource.length > 0 && (this.model || this.model === 0)) {
            /** @type {?} */
            var data = this.dataSource.find(function (x) { return x[_this.valueField] === _this.model; });
            if (data) {
                this.value = data[this._labelField];
                if (!this.isFirstValueSend) {
                    this.firstValue.emit(data);
                }
            }
        }
    };
    StaticSelectorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-static-selector',
                    template: "\n\t<ng-select *ngIf=\"!isReadOnly\"\n\t\t#selector\n      \tclass=\"custom-ng-select form-selector\"\n\t\t[items]=\"dataSource\"\n\t\t[searchable]=\"searchable\"\n\t\t[clearable]=\"clearable\"\n      \t[(ngModel)]=\"Model\"\n\t\t[bindValue]=\"valueField\"\n\t\t[bindLabel]=\"_labelField\"\n\t\t[disabled]=\"isDisabled\"\n    appendTo= \"body\"\n    [virtualScroll]=\"virtualScroll\"\n    placeholder=\"{{placeholder|translate}}\"\n\t\tngDefaultControl\n\t>\n\t</ng-select>\n\t<layout-form-text-input class=\"readonly-text-input\" *ngIf=\"isReadOnly\" [isReadOnly]=\"isReadOnly\" [isDisabled]=\"isDisabled\" [(ngModel)]=\"value\"></layout-form-text-input>\n  \t",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return StaticSelectorComponent; }),
                            multi: true
                        }
                    ],
                    styles: [".custom-ng-select{height:30px!important;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
                }] }
    ];
    StaticSelectorComponent.propDecorators = {
        dataSource: [{ type: Input }],
        valueField: [{ type: Input }],
        labelField: [{ type: Input }],
        seperator: [{ type: Input }],
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        searchable: [{ type: Input }],
        clearable: [{ type: Input }],
        changed: [{ type: Output }],
        placeholder: [{ type: Input }],
        selector: [{ type: ViewChild, args: ['selector',] }],
        firstValue: [{ type: Output }]
    };
    return StaticSelectorComponent;
}());
export { StaticSelectorComponent };
if (false) {
    /** @type {?} */
    StaticSelectorComponent.prototype.dataSource;
    /** @type {?} */
    StaticSelectorComponent.prototype.valueField;
    /** @type {?} */
    StaticSelectorComponent.prototype.labelField;
    /** @type {?} */
    StaticSelectorComponent.prototype._labelField;
    /** @type {?} */
    StaticSelectorComponent.prototype.seperator;
    /** @type {?} */
    StaticSelectorComponent.prototype.isDisabled;
    /** @type {?} */
    StaticSelectorComponent.prototype.isReadOnly;
    /** @type {?} */
    StaticSelectorComponent.prototype.searchable;
    /** @type {?} */
    StaticSelectorComponent.prototype.clearable;
    /** @type {?} */
    StaticSelectorComponent.prototype.changed;
    /** @type {?} */
    StaticSelectorComponent.prototype.placeholder;
    /** @type {?} */
    StaticSelectorComponent.prototype.selector;
    /** @type {?} */
    StaticSelectorComponent.prototype.virtualScroll;
    /** @type {?} */
    StaticSelectorComponent.prototype.value;
    /** @type {?} */
    StaticSelectorComponent.prototype.label;
    /** @type {?} */
    StaticSelectorComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    StaticSelectorComponent.prototype.model;
    /** @type {?} */
    StaticSelectorComponent.prototype.firstValue;
    /**
     * @type {?}
     * @private
     */
    StaticSelectorComponent.prototype.isFirstValueSend;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljLXNlbGVjdG9yLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N0YXRpYy1zZWxlY3Rvci9zdGF0aWMtc2VsZWN0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQ3hILE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QjtJQUFBO1FBaUNXLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsZUFBVSxHQUFzQixZQUFZLENBQUM7UUFDdEQsZ0JBQVcsR0FBRyxZQUFZLENBQUM7UUFDbEIsY0FBUyxHQUFHLEdBQUcsQ0FBQztRQUNoQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDOztRQWNsQyxlQUFVLEdBQXVCLElBQUksWUFBWSxFQUFPLENBQUM7SUFpSHJFLENBQUM7Ozs7O0lBN0dDLDZDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkFpREM7UUFoREMsSUFBSSxPQUFPLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUVyRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFO2dCQUNuRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzthQUMzQjs7O2dCQUdLLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQzthQUNuQjtTQUNGO1FBRUQsSUFBSSxPQUFPLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDekMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQ3pDO2dCQUNFLFNBQVM7Z0JBQ1QsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxFQUExSCxDQUEwSDtnQkFDL0gsbUhBQW1IO2dCQUNuSCxJQUFJLENBQUMsV0FBVzthQUNqQixFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3pCO1FBRUQsSUFBSSxPQUFPLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDekMsSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksWUFBWSxLQUFLLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxXQUFXLEdBQUcsZ0JBQWdCLENBQUM7YUFDckM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxtQkFBUSxJQUFJLENBQUMsVUFBVSxFQUFBLENBQUM7YUFDNUM7U0FDRjtRQUVELElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxPQUFPLENBQUMsVUFBVSxJQUFJLE9BQU8sQ0FBQyxTQUFTLEVBQUU7WUFDakUsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLGdCQUFnQixFQUFFO2dCQUMxRixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQVM7O3dCQUN4QixhQUFhLEdBQUcsRUFBRTtvQkFDdEIsQ0FBQyxtQkFBVSxLQUFJLENBQUMsVUFBVSxFQUFBLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFhO3dCQUNoRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTs0QkFDZixhQUFhLEdBQUcsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO3lCQUM5RDtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFFSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUMvRixPQUFPLElBQUksQ0FBQztnQkFDZCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUN6QjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCwwQ0FBUTs7OztJQUFSLFVBQVMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQsMkNBQVM7OztJQUFUO0lBQ0EsQ0FBQztJQUVELHNCQUFJLDBDQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDcEIsQ0FBQzs7Ozs7UUFFRCxVQUFVLEdBQUc7WUFBYixpQkFxQkM7WUFwQkMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7WUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFHakIsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOzt3QkFDYixJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUksQ0FBQyxLQUFLLEVBQWpDLENBQWlDLENBQUM7b0JBQ3pFLElBQUksSUFBSSxFQUFFO3dCQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3pCO2lCQUNGO3FCQUFNOzt3QkFDQyxRQUFRLEdBQVEsRUFBRTtvQkFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO29CQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDN0I7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN6QjtRQUNILENBQUM7OztPQXZCQTs7Ozs7SUF5QkQsa0RBQWdCOzs7O0lBQWhCLFVBQWlCLEVBQU87UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxtREFBaUI7Ozs7SUFBakIsVUFBa0IsRUFBTztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELDRDQUFVOzs7O0lBQVYsVUFBVyxLQUFVO1FBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxrREFBZ0I7OztJQUFoQjtRQUFBLGlCQVdDO1FBVkMsdUJBQXVCO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7O2dCQUMvRSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUksQ0FBQyxLQUFLLEVBQWpDLENBQWlDLENBQUM7WUFDekUsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO29CQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDNUI7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7Z0JBdktGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxRQUFRLEVBQUUsdXBCQWtCUjtvQkFFRixTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsdUJBQXVCLEVBQXZCLENBQXVCLENBQUM7NEJBQ3RELEtBQUssRUFBRSxJQUFJO3lCQUNaO3FCQUNGOztpQkFDRjs7OzZCQUdFLEtBQUs7NkJBQ0wsS0FBSzs2QkFDTCxLQUFLOzRCQUVMLEtBQUs7NkJBQ0wsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7NEJBQ0wsS0FBSzswQkFDTCxNQUFNOzhCQUNOLEtBQUs7MkJBRUwsU0FBUyxTQUFDLFVBQVU7NkJBV3BCLE1BQU07O0lBaUhULDhCQUFDO0NBQUEsQUF4S0QsSUF3S0M7U0ExSVksdUJBQXVCOzs7SUFFbEMsNkNBQTJCOztJQUMzQiw2Q0FBMkI7O0lBQzNCLDZDQUFzRDs7SUFDdEQsOENBQTJCOztJQUMzQiw0Q0FBeUI7O0lBQ3pCLDZDQUE0Qjs7SUFDNUIsNkNBQTRCOztJQUM1Qiw2Q0FBMkI7O0lBQzNCLDRDQUEwQjs7SUFDMUIsMENBQTRDOztJQUM1Qyw4Q0FBOEI7O0lBRTlCLDJDQUFtRDs7SUFFbkQsZ0RBQThCOztJQUU5Qix3Q0FBcUI7O0lBQ3JCLHdDQUFxQjs7SUFDckIsMkNBQXVCOzs7OztJQUV2Qix3Q0FBYzs7SUFHZCw2Q0FBbUU7Ozs7O0lBQ25FLG1EQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIFZpZXdDaGlsZCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE5nU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnQG5nLXNlbGVjdC9uZy1zZWxlY3QnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1zdGF0aWMtc2VsZWN0b3InLFxyXG4gIHRlbXBsYXRlOiBgXHJcblx0PG5nLXNlbGVjdCAqbmdJZj1cIiFpc1JlYWRPbmx5XCJcclxuXHRcdCNzZWxlY3RvclxyXG4gICAgICBcdGNsYXNzPVwiY3VzdG9tLW5nLXNlbGVjdCBmb3JtLXNlbGVjdG9yXCJcclxuXHRcdFtpdGVtc109XCJkYXRhU291cmNlXCJcclxuXHRcdFtzZWFyY2hhYmxlXT1cInNlYXJjaGFibGVcIlxyXG5cdFx0W2NsZWFyYWJsZV09XCJjbGVhcmFibGVcIlxyXG4gICAgICBcdFsobmdNb2RlbCldPVwiTW9kZWxcIlxyXG5cdFx0W2JpbmRWYWx1ZV09XCJ2YWx1ZUZpZWxkXCJcclxuXHRcdFtiaW5kTGFiZWxdPVwiX2xhYmVsRmllbGRcIlxyXG5cdFx0W2Rpc2FibGVkXT1cImlzRGlzYWJsZWRcIlxyXG4gICAgYXBwZW5kVG89IFwiYm9keVwiXHJcbiAgICBbdmlydHVhbFNjcm9sbF09XCJ2aXJ0dWFsU2Nyb2xsXCJcclxuICAgIHBsYWNlaG9sZGVyPVwie3twbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXHJcblx0XHRuZ0RlZmF1bHRDb250cm9sXHJcblx0PlxyXG5cdDwvbmctc2VsZWN0PlxyXG5cdDxsYXlvdXQtZm9ybS10ZXh0LWlucHV0IGNsYXNzPVwicmVhZG9ubHktdGV4dC1pbnB1dFwiICpuZ0lmPVwiaXNSZWFkT25seVwiIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIiBbaXNEaXNhYmxlZF09XCJpc0Rpc2FibGVkXCIgWyhuZ01vZGVsKV09XCJ2YWx1ZVwiPjwvbGF5b3V0LWZvcm0tdGV4dC1pbnB1dD5cclxuICBcdGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc3RhdGljLXNlbGVjdG9yLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxyXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBTdGF0aWNTZWxlY3RvckNvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3RhdGljU2VsZWN0b3JDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgZGF0YVNvdXJjZTogYW55W107XHJcbiAgQElucHV0KCkgdmFsdWVGaWVsZCA9ICdJZCc7XHJcbiAgQElucHV0KCkgbGFiZWxGaWVsZDogc3RyaW5nIHwgc3RyaW5nW10gPSAnRGVmaW5pdGlvbic7XHJcbiAgX2xhYmVsRmllbGQgPSAnRGVmaW5pdGlvbic7XHJcbiAgQElucHV0KCkgc2VwZXJhdG9yID0gJyAnO1xyXG4gIEBJbnB1dCgpIGlzRGlzYWJsZWQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBpc1JlYWRPbmx5ID0gZmFsc2U7XHJcbiAgQElucHV0KCkgc2VhcmNoYWJsZSA9IHRydWU7XHJcbiAgQElucHV0KCkgY2xlYXJhYmxlID0gdHJ1ZTtcclxuICBAT3V0cHV0KCkgY2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xyXG5cclxuICBAVmlld0NoaWxkKCdzZWxlY3RvcicpIHNlbGVjdG9yOiBOZ1NlbGVjdENvbXBvbmVudDtcclxuXHJcbiAgcHVibGljIHZpcnR1YWxTY3JvbGw6IGJvb2xlYW47XHJcblxyXG4gIHB1YmxpYyB2YWx1ZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBsYWJlbDogc3RyaW5nO1xyXG4gIHB1YmxpYyBsaXN0RGF0YTogYW55W107XHJcblxyXG4gIHByaXZhdGUgbW9kZWw7XHJcblxyXG4gIC8vIGl0IGlzIGZvciBnZXR0aW5nIHRoZSBmaXJzdCBzZWxlY3RlZCB2YWx1ZVxyXG4gIEBPdXRwdXQoKSBmaXJzdFZhbHVlPzogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBwcml2YXRlIGlzRmlyc3RWYWx1ZVNlbmQ6IGJvb2xlYW47XHJcblxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICBpZiAoY2hhbmdlcy5kYXRhU291cmNlICYmIHRoaXMuc2VsZWN0b3IgJiYgdGhpcy5tb2RlbCkge1xyXG5cclxuICAgICAgaWYgKHRoaXMuZGF0YVNvdXJjZSAmJiB0aGlzLmRhdGFTb3VyY2UubGVuZ3RoID4gNTAwKSB7XHJcbiAgICAgICAgdGhpcy52aXJ0dWFsU2Nyb2xsID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICAvLyB0aGlzLmRhdGFTb3VyY2UgPSBbLi4udGhpcy5kYXRhU291cmNlLCBjaGFuZ2VzLmRhdGFTb3VyY2VdO1xyXG5cclxuICAgICAgY29uc3QgaW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmRhdGFTb3VyY2UsIFsnSWQnLCB0aGlzLk1vZGVsXSk7XHJcbiAgICAgIGlmIChpbmRleCA9PT0gLTEpIHtcclxuICAgICAgICB0aGlzLm1vZGVsID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjaGFuZ2VzLmRhdGFTb3VyY2UgJiYgdGhpcy5kYXRhU291cmNlKSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IF8ub3JkZXJCeSh0aGlzLmRhdGFTb3VyY2UsXHJcbiAgICAgICAgW1xyXG4gICAgICAgICAgJ09yZGVyTm8nLFxyXG4gICAgICAgICAgeiA9PiB6W3RoaXMuX2xhYmVsRmllbGRdICYmICh0eXBlb2Ygelt0aGlzLl9sYWJlbEZpZWxkXSA9PT0gJ3N0cmluZycpID8gelt0aGlzLl9sYWJlbEZpZWxkXS50b0xvd2VyQ2FzZSgpIDogelt0aGlzLl9sYWJlbEZpZWxkXSxcclxuICAgICAgICAgIC8vIHkgPT4geVsnRGVmaW5pdGlvbiddICYmICh0eXBlb2YgeVsnRGVmaW5pdGlvbiddID09PSAnc3RyaW5nJykgPyB5WydEZWZpbml0aW9uJ10udG9Mb3dlckNhc2UoKSA6IHlbJ0RlZmluaXRpb24nXSxcclxuICAgICAgICAgIHRoaXMuX2xhYmVsRmllbGRcclxuICAgICAgICBdLCBbJ2FzYyddKTtcclxuICAgICAgdGhpcy5maW5kRGlzcGxheVZhbHVlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNoYW5nZXMubGFiZWxGaWVsZCAmJiB0aGlzLmxhYmVsRmllbGQpIHtcclxuICAgICAgaWYgKGNoYW5nZXMubGFiZWxGaWVsZC5jdXJyZW50VmFsdWUgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgIHRoaXMuX2xhYmVsRmllbGQgPSAnQ29tYmluZWRGaWVsZHMnO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX2xhYmVsRmllbGQgPSA8c3RyaW5nPnRoaXMubGFiZWxGaWVsZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjaGFuZ2VzLmRhdGFTb3VyY2UgfHwgY2hhbmdlcy5sYWJlbEZpZWxkIHx8IGNoYW5nZXMuc2VwZXJhdG9yKSB7XHJcbiAgICAgIGlmICh0aGlzLmRhdGFTb3VyY2UgJiYgdGhpcy5kYXRhU291cmNlLmxlbmd0aCA+IDAgJiYgdGhpcy5fbGFiZWxGaWVsZCA9PT0gJ0NvbWJpbmVkRmllbGRzJykge1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZS5tYXAoKGl0ZW06IGFueSkgPT4ge1xyXG4gICAgICAgICAgbGV0IGNvbWJpbmVkVmFsdWUgPSAnJztcclxuICAgICAgICAgICg8c3RyaW5nW10+dGhpcy5sYWJlbEZpZWxkKS5mb3JFYWNoKChmaWVsZDogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChpdGVtW2ZpZWxkXSkge1xyXG4gICAgICAgICAgICAgIGNvbWJpbmVkVmFsdWUgPSBjb21iaW5lZFZhbHVlICsgaXRlbVtmaWVsZF0gKyB0aGlzLnNlcGVyYXRvcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgaXRlbVsnQ29tYmluZWRGaWVsZHMnXSA9IGNvbWJpbmVkVmFsdWUgPyBjb21iaW5lZFZhbHVlLnNsaWNlKDAsIC10aGlzLnNlcGVyYXRvci5sZW5ndGgpIDogbnVsbDtcclxuICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZmluZERpc3BsYXlWYWx1ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZSh2YWw6IGFueSkge1xyXG4gIH1cclxuXHJcbiAgb25Ub3VjaGVkKCkge1xyXG4gIH1cclxuXHJcbiAgZ2V0IE1vZGVsKCkge1xyXG4gICAgcmV0dXJuIHRoaXMubW9kZWw7XHJcbiAgfVxyXG5cclxuICBzZXQgTW9kZWwodmFsKSB7XHJcbiAgICB0aGlzLm1vZGVsID0gdmFsO1xyXG4gICAgdGhpcy5vbkNoYW5nZSh2YWwpO1xyXG4gICAgdGhpcy5vblRvdWNoZWQoKTtcclxuXHJcblxyXG4gICAgaWYgKHZhbCkge1xyXG4gICAgICBpZiAodGhpcy5kYXRhU291cmNlKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGF0YVNvdXJjZS5maW5kKHggPT4geFt0aGlzLnZhbHVlRmllbGRdID09PSB0aGlzLm1vZGVsKTtcclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgdGhpcy52YWx1ZSA9IGRhdGFbdGhpcy5fbGFiZWxGaWVsZF07XHJcbiAgICAgICAgICB0aGlzLmNoYW5nZWQuZW1pdChkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgZW1pdERhdGE6IGFueSA9IHt9O1xyXG4gICAgICAgIGVtaXREYXRhW3RoaXMudmFsdWVGaWVsZF0gPSB0aGlzLm1vZGVsO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlZC5lbWl0KGVtaXREYXRhKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGFuZ2VkLmVtaXQobnVsbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICB3cml0ZVZhbHVlKHZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMubW9kZWwgPSB2YWx1ZTtcclxuICAgIHRoaXMuZmluZERpc3BsYXlWYWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgZmluZERpc3BsYXlWYWx1ZSgpOiB2b2lkIHtcclxuICAgIC8vIGlmICh0aGlzLmlzUmVhZE9ubHkpXHJcbiAgICBpZiAodGhpcy5kYXRhU291cmNlICYmIHRoaXMuZGF0YVNvdXJjZS5sZW5ndGggPiAwICYmICh0aGlzLm1vZGVsIHx8IHRoaXMubW9kZWwgPT09IDApKSB7XHJcbiAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRhdGFTb3VyY2UuZmluZCh4ID0+IHhbdGhpcy52YWx1ZUZpZWxkXSA9PT0gdGhpcy5tb2RlbCk7XHJcbiAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IGRhdGFbdGhpcy5fbGFiZWxGaWVsZF07XHJcbiAgICAgICAgaWYgKCF0aGlzLmlzRmlyc3RWYWx1ZVNlbmQpIHtcclxuICAgICAgICAgIHRoaXMuZmlyc3RWYWx1ZS5lbWl0KGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=