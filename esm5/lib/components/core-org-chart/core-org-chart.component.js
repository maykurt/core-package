/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output, Input, NgZone } from '@angular/core';
// @Inject(DOCUMENT) private document: Document
var CoreOrgChartComponent = /** @class */ (function () {
    function CoreOrgChartComponent(ngZone) {
        this.ngZone = ngZone;
        this.clickedExport = new EventEmitter();
    }
    /**
     * @param {?} simpleChanges
     * @return {?}
     */
    CoreOrgChartComponent.prototype.ngOnChanges = /**
     * @param {?} simpleChanges
     * @return {?}
     */
    function (simpleChanges) {
        if (simpleChanges.export && this.export) {
            this.onExport();
        }
    };
    /**
     * @return {?}
     */
    CoreOrgChartComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.datasource = {
            name: 'A',
            title: 'Yönetim Kurulu Başkanı',
            className: 'org-node-blue',
            gender: 'M',
            office: 'Ankara',
            imgUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAvCAYAAABOmTCPAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAEmJJREFUeJztnQtUU1e6gK3WKgVR3oS8ICQQEgLhaZAY5SUvBQGFKmqtiIooUh+oKFqtU2d0zbTT1lerrk7vmtVqOzNt1207Tqe3qzO3M7Vzrd65szq9vY7kvPMgBAStWvXcvU8IQnJyEsAAsfxr7RUXyb/3f/79nX+//nOcNGlCJmRCHgipLTxEyNSfEgnpv2cruET1KZmqvdC5tknnbVuun3rd31hU/htkRoQZi074FyaWsxe+jKCy87c56mMiVQIWLf8O/Kbdpa5AhhLSpHYiRZvlqG9pPRCPy1P+jvGlqCt9NCoWxWVJ7abyJWXe9gdVvDQfF8gITBjn8npwsRzDoxVfYGJFlLft8SkhYhP/jIYIaSw8mrWggZE0Lk6gO5atXu5tW3qOnQg05pVcQZ4IorEoKY3xYtlLmJgmM3RHHPWR8Og0lBfjWg8WcE24UE4TiowCR31ry/5kXKK8C+t3pY+GiGgAFG0sKVvlbX8YcxZVMf0QwX1NaKSkE42IEXvbHp8S4JQLIALaQGIpaIiAxmNVdEdtXYW3bek5fjLAmF/6V2RaMM1lE+xsMnPe8476SGSsGkTUOy71YAGQ4KIEmlBm5jjqW3fuV+GxiT0MTK78EQqgj06gjaXly7ztD0NeWRkDdaTE9fVAm3ixGChCb9vjU4LwJJ9wOi24H+xKb9vykMD+YQLsCZlEZuo+w4KFLkGaAHsCbJ8Uo27Bn9BZggmwJ02A/UgJlamzLR4nwPZlsNEJsB2ESM76AoW7AOMA7N4TJwMB2N95BHaG7iVH/ZGC3dmyLxWATeMegG0qLVvjbX9QmvwaBmoOsHEb2LeRSEmMt+3xKSHU2QBs0bgAuxtEbEN+yZv6aUH/jfKllwCk7CVcfIWcnbvJUX+kYHft3h8HwP5PJFx82VXbSJjoMhot/5oqqSz1tj+My1bngfauoBHRX7u0JyoW2Cr9BOXLeN62x6cEl6d+yTn0jiLYUIj8Ur9r04Nn6gVSfz0/lr1EiGYYsoumOuqOFOwbew9OxqQq/2vhogCXbYcLA5DohACsqGaKt31hffkXk/XBUYH6cLFLe65FSWaggjg/MlrxmLft8SnBpaqvmHkcF9jSJLqjspYVbGJuwePEvAV+hDbXj8wueHy07R8oIwV7Qh4IpcmdbK7bwDM1bk0xrducbV6/Kd3U0urxqGAsr5lmfbY1xlD9dKa5EeivbUw1Vq8K96bNgwSXJbkHO0ZJW5av6QfbUFIZb6zbsI7KLngVT9Z8hKdoLuJJmReJJM2HBl3hcUNpdZOxqlZjrm+YNhRbbr569gmwYFrS7jdrCyqUNqKCWPYSKd6szyrSOOqPFGxz66EQTJZUj0aINrlqG0TsTWiMvNFYUhXnyTUZ122aQWbqVIZFNSuMRVVtVMa8X1KFC08bK6rP4uqs49T8wkNUbvEqQ/lSpzmyde/hGCRUsBmJELv2BV+yGUTs1YhIMcNRv+vgIb5l+y5tx47dGtby7HadZc++tO4LFwL67a1aLuh4um4dmZL9OzBSk5gw/g4mkN0Dnz9gsYkkqc1701zX4HRq26+/ZEUaVVy5n0jK/DNYi1gxYdxdTAj0BXF3QIBEyPmFZ81bd+R64rsRCSqQ/RfG41hxz+LTwKgfeo68OK/zhZ8qiaycX+PihOvozEgaDQLfhYL5eYjQdtQMFqHwb8zfeRKazM77jMwpWmNpevYJT2zpOX4CLh6/9WxXRPeio/7Id0X2pcDFo2e7ImV1XNdCli0XE3Ny28is3C/x6IS7cOcJDYpi/IXO5NFoQAT4N/BXsJBGZvFoImXONx0NzaGD6phTUOPhrsgtlGXxSGpy2tBw0B4vhrUgAWGg3SxL1yuvhnbUN042Ln6qDZMmIkhgJBPQ+o/zYfsRElv/QtuDBLShuOLgwLaMzS0CQ27pafD7u4x+qBDoRD/QhZ99KRoIqJssXnzGsn1PgKPND03QyJhLnCDA3AqR/IaxsOxjXKKkEL9QALLI9h2Ej61EgRIOHOcfwegTaXO+ImtWuR36H5XtPqK4fCMmS9ZjoAMZgOGuE4e/UNgeX9Zh1BUP2rIb6T62IX1eG7xxsLBo1oL4h9OkOktv3b6rFE/M+JABclbUA1vZ2oP2ArCRJ8Npy/rG3bCd7qNHs1FxwjUU1IeGRQ/ggE0/juFHP3UmvDl+8/35cx4FvSELypNwg81nPu8j8E6F0ZvLaBYnQEigA5FZgu+tO1pXc9nyKIBtqd/4cyQwgolsGExS8sBfEGww7GPmhdWDMvRGDLYmtxXlmGYyRSTvxWJV3f2JcJ70rTCeRgLB9cUoblp3tu4kU7Pa0SfDuG+IgUUQB/wootunBNLWHS173EI6HAFz7Ctcc+z+AjvJE5hZC7jLgSOQgMj7vadPVruyxdfBtjbvaNBPC2Eimsc3vx1sSSJmLl06umDDoAXqhpD134Se9mlf0IJTUGZU4A+RCQg3GB1QUXzX9b37ZJ4T66EQiRn/QD29U0dS7Hd5eEx3z7HjKjZbfBnsnpdf5ZGJ6RQyPYTptKH4ZszAfhhlJAEP+AmO5pY161qGTq4bIRRp/zMqYPfBrQfQGhcs/OyHP/7eaR/Yl8E2121cq/cLGVpHM/nUA6Yivgj2SAozXxfSZNb8PwyPXg4ZVbAZKMGiEixSrAcOVTna4stgG6trTyDTPIjWsDPhzsgMMA+HOyVgOEb8w+AC3QzAHrRP/MiDDQu4Nlyi/HZ49HIIEaf+BsI2ZLCddkI814XDD5Vb+ImjLb4KNpGR/TguV3+AhbkJEHxb+2DBdoeau+B9SpPzAjU37wyePPsyIVZgFm3hoC07nwPbfu1DmW+DUQvc1O3DJ9iF4NKkfzL7jJ4aDp0MFxtw4QAXSXDhACNQqNjji4G7K3hcUk/nrlb1QFt8FWxMkTIVi1X+O7NvG+VmBwREafPKZ3YMbNeyfsss85aWLP1zh4MG/n1UwYaPmMFtSeZMQkh7zITdBnhTQz24mIR1QEag3e6Cng3sa8Nhl1NwmYdgwyG0b4Mej0tuJ9LnHrfUb9xqqlm5G1dnvYtLVT0IBD3CffRn6gE3RMe6DYMSmXwVbAaigoVn4JSC2afluvZgAW1tafEogWpUwIZ+Br+BfQLm+RYiWfNXIjH9G2gnVzrzIH3oEzilyNS9ZVq/eaX5mfV1aKzyU2b/3h3cYwp2X6SGHQfmkr80b93plDNg2bRV0x4i+htzYsVxkmm/GLjBbyyrOjuwDl8Gu2P9pia3i0cYHMBoReUV/an35DG3SVSjAjbc7gM2k6rM8x2NzQnMtezZP920uq4VE8qYE1N3QQqMVHese/YPemuAZc8+f+PCqj8g00PHKdh98yUIG4jQr3DVdf3s6TAqO+8qnENzOhM6ZAaPptLnft65paW/g3tOnPJZsHt/e04OAOlmrp1rAcmT2La4Vq05465vvL+PLaMRsIglNPP/YWna7nQCaF5e+y7iF8Z9s4KRF6wv2i07WwOd/Hn4aBU82GOmruMPbHDx4OJwVcbVO19+EeSuPutPD9cgwYK7zIKUq87ASJpM116xNG/vr9OXwYbS2dj0QvtjMx4cK7uK2iAKwlM3y8bmw1y+HI2IjQbzaTJF+z5b+6blzzQxbHAlycF+lqmuGgoKZzrqm5c/rcOi5beYvJFRBztG+Z07x+mnh9CmiprXPKmv98yZIEKRhrsbwrAQMKeLT/6OKCgS9Oueet2nwb715edPUEVlH+unzOQ+noYHE2BKon98Jt21/9B2V74cFbBBNCWUsy+wtY/Hp61nroEjYjM3sUx1zVReEeLUH0Hi+UD/BucuidfAFidc5TIcfgc7k9TltXpSX2fbwamkOusSyjX8wKgVEE4Tadnt5m27+re4ek++5tNgQ+l6+XgokTr3PPJkKM1A5WpaAo+UYTJRYOT9ntPHWBeTowI2CECESsMKNljgb+T0pRuw0fDYHLDeuslZh9fAFiX8ixNsUJCACLjQc0oTZZOuX70RQKg1V5ltQC6wYWZZWrbeumOXxK7b8wiAbRfjqrW7QVS+z2yJcsEdGEXjArmx9+Rpp7TTsQYbkygaUOZ3w4vYaKRsDMGWKP+P03FwPgxAIzQ5l25dueg2xfD7U6fyQH230TDufW2UWXSk/K+ppJxv132UwIZiKCpbhYvj76Eh3KMXPLE0Lq0976Q/xmBTGu065neRvgh2gvrvnNl9cH4Ej8Gnh9Ldx17a7K6+juLy/9DDLR43215wNU6mzf26s3mXv13X1xePrP6oW/dvcI3CBSeM6mDqdrv76ItzBuqONdiGovJ622kph+3jFWwyc+6HTEThPAq25TeAC7zZuXtfPVs9XceOhxpKK34NgXWbBgv3sacH06bSqncH1tFz7MQjB7b5mboyNFzMvUsEzwjANZuqV54cqDvWYHc0PbvGdjDH4Y/xCra5uPIIkxTvLnnHvtgBhUjN/oTUFjR0bN6abVpVX0DOyTtAJGX+sz8yCdwn1iMBkbS1oal5oC2+DLb54pXJbP4FgaMB+O6eu0MwJn8mc97l3heO9D8uNdZgU4uXrkT50vuc233jFezOnburmTxprsbtxZ6ZNosHFjzgRohW3MCjFbdtz7LBOjxLVoe/xaWq611HX1QMtMWXTx57P74wuWPDljYiXfscVV5d2HnwULZhyYq1WIyCsNXHvUDvexuA2bywMtFe51iDjYjkNeA3dzn1xyvYN869G4GJlRSEdUgJ8jC/AHayPR/AUz0YncAc3FhWdc7RFl8GG4q58qkL7ZMDmCNq5rf294x7EjDASIjHJd+gKqv7X0g/1mCTReWV8EFuzvXBeAUbimVD40/0kwOHBOewSl8HgqlKb+9rp5Ic7fB5sJfWvqeHL62HYA8lOw76hYnYqm5qUWW6vb7ROaARujygAaPOIkyccJvrTGJcg3378wsRWFzyNf2UWUN+rGkoncc82Dsjku7e27aFzY5HAuypQcPyDXz6n8zQXun6+Ut+9vpGA2xkZhRNZeV8zNYfZMnifEwYd9Mnd0XsAiJoFXxsCx7GuFv8DbkwTyWLmSmIoaLmlCsbfrRgAzjgMXzXrtZBp7ujAjYINIa84o/Y+gNXpukACzc43zsz3sGG0rW3dTvchmNSTx9W5O6bfsAXpVDzik93NG1z+Rq0HyXYIIjA6Quhyrh069z5QYlEowU2Nb+QFezOfc9p8FhFD8Zx2OYTYEOxbNm6AQkT9zJvcxriopANIuYRe77sprF29T53bf+owO5LLkJgzow689tbH33g9AqCUQEbjNBULnvEtu7ck4LFKLvR8EcAbCjGhi1ZRELqV3CIRMEcbNB7JzjSMZlPmLgOn76wv+YsXfvHzpa9c9y3+jDAlowp2KbF1Rc8WoQz26Zi5i1RhCLt4u0P3pOy+cPrYIM+Zd4aUPkUa9oqKs9QgpHbig0zuw+NkBZ48tAJLpSbXDHx0KVjxVp/Y1HlZjxp9mVMGH8X9Q+zPVkdLLQ9NcE8ORHT97os2zv74LDGPDomSbQSyZrPDEtqa4bSZs8rxwIN8wra9ZOetO2X9z1/51RAG4Q6y+mhB/3MqEwkhO9ar++pcLgYwqXJJY761m17UsFiyfZ+Ohf6zDVGSWjDgmKnE9iOlWve0T8WwCR4wYDg8uaEIxlP0mMsWnzMWLbUZY67QVu4jBnxgrivCb4LDwniS5z0U7U/Q6C+K12gp5/kR5sqqv/C1r6pfqMCF8Uz00iXbQcCX4nivjeWLAxz1Ed48flolLQXBJt74PMOa+FJ7oGIjbjygdfEsnn7FNPSlYVUec1REqyecblaj0kSb+NiOQM1/D8gAcjXcdXsK+TcgreoqmXbTPWb0obTVvcbb/oZl654HhPL3yFS5rwN4GUviRnnDMWVTjcNkqqLBt+/5VIPFlXmOTJd9zaVU5roqG9+/oiYnJ3zBqzflT6uTD9HZuje7qhdo3XU72o7oCSyC/aQugXHSbXmsqtjdObxsNwi1ig5UIzL1mZAe4mk2a6vR605h6vnnMBTtKGO+tSi6goc6rvSTda8DcB9x7x2I+sLa3rfez+cml/0Oi5Pce2PxMzzVFbOy50rnnZ626telBiE8KW5CF9SjPBj81kLL6YQi1V4NKJ7VQz5pSJCmaGm8hdqjA3NWtCJmcDxSkNFbfBY2zaexHLwJ/XMCzwdh3GY324D2ymbb0ImZNyL5fCRjcxawRXYOYW/G2sbJ2RChiyWfQebEban1h+A/duxtnFCJmTIYmk70ML8n/D2XQ17gQtHsOCi5i1gPcaekMHy/wuA7v+MZ1yPAAAAAElFTkSuQmCC',
            children: []
        };
        /** @type {?} */
        var nodeTemplate = function (data) {
            /** @type {?} */
            var office = "";
            /** @type {?} */
            var title = "<div class=\"title\">" + data.name + "</div>";
            /** @type {?} */
            var content = "<div class=\"content\" title=\"" + data.title + "\"> " + data.title + " </div>";
            if (data.gender) {
                if (data.gender === 'F') {
                    content = "\n          <div class=\"content\">\n          <img src=\"./assets/images/user-female.png\" class=\"org-node-pic\"> " + data.title + "\n          </div>";
                }
                else {
                    content = "\n          <div class=\"content\">\n          <img src=\"./assets/images/user.png\" class=\"org-node-pic\"> " + data.title + "\n          </div>";
                }
            }
            if (data.imgUrl) {
                content = "\n        <div class=\"content\">\n        <img src=\"" + data.imgUrl + "\" class=\"org-node-pic\"> " + data.title + "\n        </div>";
            }
            if (data.office) {
                office = "<span class=\"office\">" + data.office + "</span>";
            }
            /** @type {?} */
            var template = '';
            template += office;
            template += title;
            template += content;
            return template;
        };
        // dragable false çünkü ekrandaki order bozuluyor sapıtıyor
        this.orgChart = $('.chart-container').orgchart({
            data: this.datasource,
            nodeTemplate: nodeTemplate,
            verticalLevel: 5,
            visibleLevel: 4,
            nodeContent: 'title',
            exportButton: false,
            // exportFilename: 'MyOrgChart',
            // exportFileextension: 'png',
            draggable: false,
            pan: false,
            zoom: false,
        });
    };
    /**
     * @param {?} orgChart
     * @param {?} datasource
     * @return {?}
     */
    CoreOrgChartComponent.prototype.refreshOrgChart = /**
     * @param {?} orgChart
     * @param {?} datasource
     * @return {?}
     */
    function (orgChart, datasource) {
        // $('.chart-container').empty();
        orgChart.init({ data: JSON.parse(datasource) });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreOrgChartComponent.prototype.fileChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var fileList = event.target.files;
        if (fileList.length > 0) {
            /** @type {?} */
            var file = fileList[0];
            /** @type {?} */
            var formData = new FormData();
            formData.append('uploadFile', file, file.name);
            if (file) {
                this.readFile(file, this.refreshOrgChart, this.orgChart);
            }
        }
    };
    /**
     * @param {?} file
     * @param {?} refreshOrgChart
     * @param {?} orgChart
     * @return {?}
     */
    CoreOrgChartComponent.prototype.readFile = /**
     * @param {?} file
     * @param {?} refreshOrgChart
     * @param {?} orgChart
     * @return {?}
     */
    function (file, refreshOrgChart, orgChart) {
        var _this = this;
        /** @type {?} */
        var reader = new FileReader();
        reader.readAsText(file, 'UTF-8');
        reader.onload = function (evt) {
            /** @type {?} */
            var datasource = ((/** @type {?} */ ((evt.target)))).result;
            _this.datasource = datasource;
            refreshOrgChart(orgChart, datasource);
        };
        reader.onerror = function (evt) {
            console.log('error reading file');
        };
    };
    /**
     * @param {?} zoomType
     * @return {?}
     */
    CoreOrgChartComponent.prototype.onClickZoom = /**
     * @param {?} zoomType
     * @return {?}
     */
    function (zoomType) {
        console.log(zoomType);
        console.log(this.orgChart);
        /** @type {?} */
        var currentZoom = parseFloat($('.orgchart').css('zoom'));
        this.orgChart.setChartScale(this.orgChart.$chart, zoomType === '+' ? currentZoom += 0.2 : currentZoom -= 0.2);
    };
    /**
     * @return {?}
     */
    CoreOrgChartComponent.prototype.onClickExport = /**
     * @return {?}
     */
    function () {
        this.clickedExport.emit({ data: true });
    };
    /**
     * @return {?}
     */
    CoreOrgChartComponent.prototype.onExport = /**
     * @return {?}
     */
    function () {
        this.orgChart.export('organization-schema', 'png');
    };
    /**
     * @return {?}
     */
    CoreOrgChartComponent.prototype.onClickResetAll = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.ngZone.run(function () {
            $('.orgchart').css('transform', ''); // remove the tansform settings
            _this.orgChart.init({ data: _this.datasource });
        });
    };
    CoreOrgChartComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-org-chart',
                    template: "<input id=\"file-upload\" type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".json\">\r\n\r\n\r\n\r\n<div class='chart-container'>\r\n</div>\r\n\r\n<div class=\"fixed-operation-bar\">\r\n\r\n  <!-- <label for=\"file-upload\" class=\"btn btn-primary   fl-l\">\r\n    <core-icon class=\"main-icon\" icon=\"upload\"></core-icon>\r\n    {{ 'FileUpload' | translate}}\r\n  </label> -->\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickResetAll()\" class=\"btn btn-danger ml-10 fl-r\" title=\"{{'Reset'| translate}}\">\r\n    <core-icon icon=\"times\"></core-icon> {{'ResetAll' | translate}}\r\n  </button> -->\r\n\r\n  <button type=\"button\" (click)=\"onClickExport()\" class=\"btn btn-primary ml-10 fl-r\" title=\"{{'Export'| translate}}\">\r\n    <core-icon icon=\"download\"></core-icon> {{'Export' | translate}}\r\n  </button>\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickZoom('-')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomOut'| translate}}\">\r\n    <core-icon icon=\"search-minus\"></core-icon> {{'ZoomOut' | translate}}\r\n  </button>\r\n\r\n  <button type=\"button\" (click)=\"onClickZoom('+')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomIn'| translate}}\">\r\n    <core-icon icon=\"search-plus\"></core-icon> {{'ZoomIn' | translate}}\r\n  </button> -->\r\n</div>",
                    styles: [".chart-container.canvasContainer{position:initial}:host /deep/ .orgchart{background:#fff}input[type=file]{display:none}.btn{cursor:pointer}:host /deep/ .chart-container.canvasContainer{position:initial}:host /deep/ .orgchart .node .content{height:auto;padding:5px}:host /deep/ .orgchart .node .content .org-node-pic{width:40px}:host /deep/ .orgchart .node{width:auto}:host /deep/ .orgchart .node .office{font-weight:600}:host /deep/ .orgchart .node .title{padding:1px 5px}:host /deep/ .orgchart .node .title .symbol{margin-top:2px;margin-left:0}:host /deep/ .orgchart .node .title .fa-group:before,:host /deep/ .orgchart .node .title .fa-users:before{padding-right:4px}.chart-container{zoom:1}:host /deep/ .org-node-blue .title{background-color:#007bff}:host /deep/ .org-node-blue .content{border-color:#007bff}:host /deep/ .org-node-blue-light .title{background-color:#e9f5ff}:host /deep/ .org-node-blue-light .content{border-color:#e9f5ff}:host /deep/ .org-node-blue-dark .title{background-color:#166dba}:host /deep/ .org-node-blue-dark .content{border-color:#166dba}:host /deep/ .org-node-blue-extra-dark .title{background-color:#15233d}:host /deep/ .org-node-blue-extra-dark .content{border-color:#15233d}:host /deep/ .org-node-purple .title{background-color:#8622cd}:host /deep/ .org-node-purple .content{border-color:#8622cd}:host /deep/ .org-node-pink .title{background-color:#e83e8c}:host /deep/ .org-node-pink .content{border-color:#e83e8c}:host /deep/ .org-node-red .title{background-color:#e30a3a}:host /deep/ .org-node-red .content{border-color:#e30a3a}:host /deep/ .org-node-orange .title{background-color:#fd7e14}:host /deep/ .org-node-orange .content{border-color:#fd7e14}:host /deep/ .org-node-orange-light .title{background-color:#ffb822}:host /deep/ .org-node-orange-light .content{border-color:#ffb822}:host /deep/ .org-node-yellow .title{background-color:#ffc107}:host /deep/ .org-node-yellow .content{border-color:#ffc107}:host /deep/ .org-node-yellow-light .title{background-color:#fff589}:host /deep/ .org-node-yellow-light .content{border-color:#fff589}:host /deep/ .org-node-green .title{background-color:#28a745}:host /deep/ .org-node-green .content{border-color:#28a745}:host /deep/ .org-node-indigo .title{background-color:#6610f2}:host /deep/ .org-node-indigo .content{border-color:#6610f2}:host /deep/ .org-node-teal .title{background-color:#20c997}:host /deep/ .org-node-teal .content{border-color:#20c997}:host /deep/ .org-node-cyan .title{background-color:#17a2b8}:host /deep/ .org-node-cyan .content{border-color:#17a2b8}:host /deep/ .org-node-gray-dark .title{background-color:#343a40}:host /deep/ .org-node-gray-dark .content{border-color:#343a40}"]
                }] }
    ];
    /** @nocollapse */
    CoreOrgChartComponent.ctorParameters = function () { return [
        { type: NgZone }
    ]; };
    CoreOrgChartComponent.propDecorators = {
        export: [{ type: Input }],
        clickedExport: [{ type: Output }]
    };
    return CoreOrgChartComponent;
}());
export { CoreOrgChartComponent };
if (false) {
    /** @type {?} */
    CoreOrgChartComponent.prototype.export;
    /** @type {?} */
    CoreOrgChartComponent.prototype.clickedExport;
    /** @type {?} */
    CoreOrgChartComponent.prototype.datasource;
    /** @type {?} */
    CoreOrgChartComponent.prototype.orgChart;
    /**
     * @type {?}
     * @private
     */
    CoreOrgChartComponent.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1vcmctY2hhcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1vcmctY2hhcnQvY29yZS1vcmctY2hhcnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBTWpIO0lBWUUsK0JBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBTHhCLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQU9sRCxDQUFDOzs7OztJQUVELDJDQUFXOzs7O0lBQVgsVUFBWSxhQUE0QjtRQUN0QyxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN2QyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDOzs7O0lBRUQsd0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLFVBQVUsR0FBRztZQUNoQixJQUFJLEVBQUUsR0FBRztZQUNULEtBQUssRUFBRSx3QkFBd0I7WUFDL0IsU0FBUyxFQUFFLGVBQWU7WUFDMUIsTUFBTSxFQUFFLEdBQUc7WUFDWCxNQUFNLEVBQUUsUUFBUTtZQUNoQixNQUFNLEVBQUUsb3hNQUFveE07WUFDNXhNLFFBQVEsRUFBRSxFQUNUO1NBQ0YsQ0FBQzs7WUFHSSxZQUFZLEdBQUcsVUFBQyxJQUFJOztnQkFDcEIsTUFBTSxHQUFHLEVBQUU7O2dCQUNULEtBQUssR0FBRywwQkFBc0IsSUFBSSxDQUFDLElBQUksV0FBUTs7Z0JBQ2pELE9BQU8sR0FBRyxvQ0FBK0IsSUFBSSxDQUFDLEtBQUssWUFBTSxJQUFJLENBQUMsS0FBSyxZQUFTO1lBRWhGLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO29CQUN2QixPQUFPLEdBQUcseUhBRXlELElBQUksQ0FBQyxLQUFLLHVCQUN0RSxDQUFDO2lCQUNUO3FCQUFNO29CQUNMLE9BQU8sR0FBRyxrSEFFa0QsSUFBSSxDQUFDLEtBQUssdUJBQy9ELENBQUM7aUJBQ1Q7YUFDRjtZQUVELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixPQUFPLEdBQUcsMkRBRUUsSUFBSSxDQUFDLE1BQU0sbUNBQTJCLElBQUksQ0FBQyxLQUFLLHFCQUNyRCxDQUFDO2FBQ1Q7WUFHRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsTUFBTSxHQUFHLDRCQUF3QixJQUFJLENBQUMsTUFBTSxZQUFTLENBQUM7YUFDdkQ7O2dCQUVHLFFBQVEsR0FBRyxFQUFFO1lBQ2pCLFFBQVEsSUFBSSxNQUFNLENBQUM7WUFDbkIsUUFBUSxJQUFJLEtBQUssQ0FBQztZQUNsQixRQUFRLElBQUksT0FBTyxDQUFDO1lBRXBCLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUM7UUFHRCwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDN0MsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3JCLFlBQVksY0FBQTtZQUNaLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFlBQVksRUFBRSxDQUFDO1lBQ2YsV0FBVyxFQUFFLE9BQU87WUFDcEIsWUFBWSxFQUFFLEtBQUs7OztZQUduQixTQUFTLEVBQUUsS0FBSztZQUNoQixHQUFHLEVBQUUsS0FBSztZQUNWLElBQUksRUFBRSxLQUFLO1NBRVosQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsK0NBQWU7Ozs7O0lBQWYsVUFBZ0IsUUFBYSxFQUFFLFVBQWU7UUFDNUMsaUNBQWlDO1FBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbEQsQ0FBQzs7Ozs7SUFFRCwwQ0FBVTs7OztJQUFWLFVBQVcsS0FBSzs7WUFDUixRQUFRLEdBQWEsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLO1FBQzdDLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O2dCQUNqQixJQUFJLEdBQVMsUUFBUSxDQUFDLENBQUMsQ0FBQzs7Z0JBQ3hCLFFBQVEsR0FBYSxJQUFJLFFBQVEsRUFBRTtZQUN6QyxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9DLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzFEO1NBRUY7SUFDSCxDQUFDOzs7Ozs7O0lBRUQsd0NBQVE7Ozs7OztJQUFSLFVBQVMsSUFBUyxFQUFFLGVBQW9CLEVBQUUsUUFBYTtRQUF2RCxpQkFZQzs7WUFYTyxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUU7UUFDL0IsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakMsTUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFDLEdBQUc7O2dCQUVaLFVBQVUsR0FBRyxDQUFDLG1CQUFBLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFjLENBQUMsQ0FBQyxNQUFNO1lBQ3RELEtBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1lBQzdCLGVBQWUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBQ0YsTUFBTSxDQUFDLE9BQU8sR0FBRyxVQUFDLEdBQUc7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsMkNBQVc7Ozs7SUFBWCxVQUFZLFFBQWdCO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7O1lBQ3ZCLFdBQVcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxRQUFRLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxXQUFXLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxXQUFXLElBQUksR0FBRyxDQUFDLENBQUM7SUFDaEgsQ0FBQzs7OztJQUVELDZDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7OztJQUVELHdDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7SUFFRCwrQ0FBZTs7O0lBQWY7UUFBQSxpQkFLQztRQUpDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ2QsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQywrQkFBK0I7WUFDcEUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkFoSkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLHMwQ0FBOEM7O2lCQUUvQzs7OztnQkFWa0YsTUFBTTs7O3lCQVl0RixLQUFLO2dDQUNMLE1BQU07O0lBMElULDRCQUFDO0NBQUEsQUFqSkQsSUFpSkM7U0E1SVkscUJBQXFCOzs7SUFDaEMsdUNBQXNCOztJQUN0Qiw4Q0FBa0Q7O0lBRWxELDJDQUF1Qjs7SUFDdkIseUNBQXFCOzs7OztJQUVULHVDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcywgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5kZWNsYXJlIHZhciAkOiBhbnk7XHJcblxyXG4vLyBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBEb2N1bWVudFxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdjb3JlLW9yZy1jaGFydCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvcmUtb3JnLWNoYXJ0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb3JlLW9yZy1jaGFydC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlT3JnQ2hhcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgQElucHV0KCkgZXhwb3J0PzogYW55O1xyXG4gIEBPdXRwdXQoKSBjbGlja2VkRXhwb3J0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIHB1YmxpYyBkYXRhc291cmNlOiBhbnk7XHJcbiAgcHVibGljIG9yZ0NoYXJ0OiBhbnk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbmdab25lOiBOZ1pvbmUpIHtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhzaW1wbGVDaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBpZiAoc2ltcGxlQ2hhbmdlcy5leHBvcnQgJiYgdGhpcy5leHBvcnQpIHtcclxuICAgICAgdGhpcy5vbkV4cG9ydCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmRhdGFzb3VyY2UgPSB7XHJcbiAgICAgIG5hbWU6ICdBJyxcclxuICAgICAgdGl0bGU6ICdZw7ZuZXRpbSBLdXJ1bHUgQmHFn2thbsSxJyxcclxuICAgICAgY2xhc3NOYW1lOiAnb3JnLW5vZGUtYmx1ZScsXHJcbiAgICAgIGdlbmRlcjogJ00nLFxyXG4gICAgICBvZmZpY2U6ICdBbmthcmEnLFxyXG4gICAgICBpbWdVcmw6ICdkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUxZQUFBQXZDQVlBQUFCT21UQ1BBQUFBQVhOU1IwSUIyY2tzZndBQUFBbHdTRmx6QUFBTEV3QUFDeE1CQUpxY0dBQUFFbUpKUkVGVWVKenRuUXRVVTFlNmdLM1dLZ1ZSM29TOElDUVFFZ0xoYVpBWTVTVXZCUUdGS21xdGlJb29VaCtvS0ZxdFUyZDB6YlRUMWxlcnJrN3ZtdFZxT3pOdDEyMDdUcWUzcXpPM003VnpyZDY1c3pxOXZZN2t2UE1nQkFTdFd2WGN2VThJUW5KeUVzQUFzZnhyN1JVWHliLzNmLzc5blgrLy9uT2NOR2xDSm1SQ0hnaXBMVHhFeU5TZkVnbnB2MmNydUVUMUtabXF2ZEM1dGtubmJWdXVuM3JkMzFoVS9odGtSb1FaaTA3NEZ5YVdzeGUraktDeTg3YzU2bU1pVlFJV0xmOE8vS2JkcGE1QWhoTFNwSFlpUlp2bHFHOXBQUkNQeTFQK2p2R2xxQ3Q5TkNvV3hXVko3YWJ5SldYZTlnZFZ2RFFmRjhnSVRCam44bnB3c1J6RG94VmZZR0pGbExmdDhTa2hZaFAvaklZSWFTdzhtcldnZ1pFMExrNmdPNWF0WHU1dFczcU9uUWcwNXBWY1FaNElvckVvS1kzeFl0bExtSmdtTTNSSEhQV1I4T2cwbEJmaldnOFdjRTI0VUU0VGlvd0NSMzFyeS81a1hLSzhDK3QzcFkrR2lHZ0FGRzBzS1Z2bGJYOFljeFpWTWYwUXdYMU5hS1NrRTQySUVYdmJIcDhTNEpRTElBTGFRR0lwYUlpQXhtTlZkRWR0WFlXM2JlazVmakxBbUYvNlYyUmFNTTFsRSt4c01uUGU4NDc2U0dTc0drVFVPeTcxWUFHUTRLSUVtbEJtNWpqcVczZnVWK0d4aVQwTVRLNzhFUXFnajA2Z2phWGx5N3p0RDBOZVdSa0RkYVRFOWZWQW0zaXhHQ2hDYjl2alU0THdKSjl3T2kyNEgreEtiOXZ5a01EK1lRTHNDWmxFWnVvK3c0S0ZMa0dhQUhzQ2JKOFVvMjdCbjlCWmdnbXdKMDJBL1VnSmxhbXpMUjRud1BabHNORUpzQjJFU003NkFvVzdBT01BN040VEp3TUIyTjk1QkhhRzdpVkgvWkdDM2RteUx4V0FUZU1lZ0cwcUxWdmpiWDlRbXZ3YUJtb09zSEViMkxlUlNFbU10KzN4S1NIVTJRQnMwYmdBdXh0RWJFTit5WnY2YVVIL2pmS2xsd0NrN0NWY2ZJV2NuYnZKVVgra1lIZnQzaDhId1A1UEpGeDgyVlhiU0pqb01ob3QvNW9xcVN6MXRqK015MWJuZ2ZhdW9CSFJYN3UwSnlvVzJDcjlCT1hMZU42Mng2Y0VsNmQreVRuMGppTFlVSWo4VXI5cjA0Tm42Z1ZTZnowL2xyMUVpR1lZc291bU91cU9GT3diZXc5T3hxUXEvMnZob2dDWGJZY0xBNURvaEFDc3FHYUt0MzFoZmZrWGsvWEJVWUg2Y0xGTGU2NUZTV2FnZ2pnL01scnhtTGZ0OFNuQnBhcXZtSGtjRjlqU0pMcWpzcFlWYkdKdXdlUEV2QVYraERiWGo4d3VlSHkwN1I4b0l3VjdRaDRJcGNtZGJLN2J3RE0xYmsweHJkdWNiVjYvS2QzVTB1cnhxR0FzcjVsbWZiWTF4bEQ5ZEthNUVlaXZiVXcxVnE4Szk2Yk5nd1NYSmJrSE8wWkpXNWF2NlFmYlVGSVpiNnpic0k3S0xuZ1ZUOVo4aEtkb0x1SkptUmVKSk0ySEJsM2hjVU5wZFpPeHFsWmpybStZTmhSYmJyNTY5Z213WUZyUzdqZHJDeXFVTnFLQ1dQWVNLZDZzenlyU09PcVBGR3h6NjZFUVRKWlVqMGFJTnJscUcwVHNUV2lNdk5GWVVoWG55VFVaMTIyYVFXYnFWSVpGTlN1TVJWVnRWTWE4WDFLRkMwOGJLNnJQNHVxczQ5VDh3a05VYnZFcVEvbFNwem15ZGUvaEdDUlVzQm1KRUx2MkJWK3lHVVRzMVloSU1jTlJ2K3ZnSWI1bCt5NXR4NDdkR3RieTdIYWRaYysrdE80TEZ3TDY3YTFhTHVoNHVtNGRtWkw5T3pCU2s1Z3cvZzRta04wRG56OWdzWWtrcWMxNzAxelg0SFJxMjYrL1pFVWFWVnk1bjBqSy9ETllpMWd4WWR4ZFRBajBCWEYzUUlCRXlQbUZaODFiZCtSNjRyc1JDU3FRL1JmRzQxaHh6K0xUd0tnZmVvNjhPSy96aFo4cWlheWNYK1BpaE92b3pFZ2FEUUxmaFlMNWVZalFkdFFNRnFId2I4emZlUkthek03N2pNd3BXbU5wZXZZSlQyenBPWDRDTGg2LzlXeFhSUGVpby83SWQwWDJwY0RGbzJlN0ltVjFYTmRDbGkwWEUzTnkyOGlzM0MveDZJUzdjT2NKRFlwaS9JWE81TkZvUUFUNE4vQlhzSkJHWnZGb0ltWE9OeDBOemFHRDZwaFRVT1BocnNndGxHWHhTR3B5MnRCdzBCNHZoclVnQVdHZzNTeEwxeXV2aG5iVU4wNDJMbjZxRFpNbUlraGdKQlBRK28vellmc1JFbHYvUXR1REJMU2h1T0xnd0xhTXpTMENRMjdwYWZEN3U0eCtxQkRvUkQvUWhaOTlLUm9JcUpzc1huekdzbjFQZ0tQTkQwM1F5SmhMbkNEQTNBcVIvSWF4c094alhLS2tFTDlRQUxMSTloMkVqNjFFZ1JJT0hPY2Z3ZWdUYVhPK0ltdFd1UjM2SDVYdFBxSzRmQ01tUzlaam9BTVpnT0d1RTRlL1VOZ2VYOVpoMUJVUDJySWI2VDYySVgxZUc3eHhzTEJvMW9MNGg5T2tPa3R2M2I2ckZFL00rSkFCY2xiVUExdloyb1AyQXJDUko4TnB5L3JHM2JDZDdxTkhzMUZ4d2pVVTFJZUdSUS9nZ0UwL2p1RkhQM1VtdkRsKzgvMzVjeDRGdlNFTHlwTndnODFuUHU4ajhFNkYwWnZMYUJZblFFaWdBNUZaZ3UrdE8xcFhjOW55S0lCdHFkLzRjeVF3Z29sc0dFeFM4c0JmRUd3dzdHUG1oZFdETXZSR0RMWW10eFhsbUdZeVJTVHZ4V0pWM2YySmNKNzByVENlUmdMQjljVW9ibHAzdHU0a1U3UGEwU2ZEdUcrSWdVVVFCL3dvb3R1bkJOTFdIUzE3M0VJNkhBRno3Q3RjYyt6K0FqdkpFNWhaQzdqTGdTT1FnTWo3dmFkUFZydXl4ZGZCdGpidmFOQlBDMkVpbXNjM3Z4MXNTU0ptTGwwNnVtRERvQVhxaHBEMTM0U2U5bWxmMElKVFVHWlU0QStSQ1FnM0dCMVFVWHpYOWIzN1pKNFQ2NkVRaVJuL1FEMjlVMGRTN0hkNWVFeDN6N0hqS2paYmZCbnNucGRmNVpHSjZSUXlQWVRwdEtINFpzekFmaGhsSkFFUCtBbU81cFkxNjFxR1RxNGJJUlJwL3pNcVlQZkJyUWZRR2hjcy9PeUhQLzdlYVIvWWw4RTIxMjFjcS9jTEdWcEhNL25VQTZZaXZnajJTQW96WHhmU1pOYjhQd3lQWGc0WlZiQVpLTUdpRWl4U3JBY09WVG5hNHN0Z0c2dHJUeURUUElqV3NEUGh6c2dNTUErSE95VmdPRWI4dytBQzNRekFIclJQL01pRERRdTRObHlpL0haNDlISUlFYWYrQnNJMlpMQ2Rka0k4MTRYREQ1VmIrSW1qTGI0S05wR1IvVGd1VjMrQWhia0pFSHhiKzJEQmRvZWF1K0I5U3BQekFqVTM3d3llUFBzeUlWWmdGbTNob0MwN253UGJmdTFEbVcrRFVRdmMxTzNESjlpRjROS2tmekw3ako0YURwME1GeHR3NFFBWFNYRGhBQ05RcU5qamk0RzdLM2hjVWsvbnJsYjFRRnQ4Rld4TWtUSVZpMVgrTzdOdkcrVm1Cd1JFYWZQS1ozWU1iTmV5ZnNzczg1YVdMUDF6aDRNRy9uMVV3WWFQbU1GdFNlWk1Ra2g3eklUZEJuaFRRejI0bUlSMVFFYWczZTZDbmczc2E4TmhsMU53bVlkZ3d5RzBiNE1lajB0dUo5TG5IcmZVYjl4cXFsbTVHMWRudll0TFZUMElCRDNDZmZSbjZnRTNSTWU2RFlNU21Yd1ZiQWFpZ29WbjRKU0MyYWZsdXZaZ0FXMXRhZkVvZ1dwVXdJWitCcitCZlFMbStSWWlXZk5YSWpIOUcyZ25WenJ6SUgzb0V6aWx5TlM5WlZxL2VhWDVtZlYxYUt6eVUyYi8zaDNjWXdwMlg2U0dIUWZta3I4MGI5M3BsRE5nMmJSVjB4NGkraHR6WXNWeGttbS9HTGpCYnl5ck9qdXdEbDhHdTJQOXBpYTNpMGNZSE1Cb1JlVVYvYW4zNURHM1NWU2pBamJjN2dNMms2ck04eDJOelFuTXRlelpQOTIwdXE0VkU4cVlFMU4zUVFxTVZIZXNlL1lQZW11QVpjOCtmK1BDcWo4ZzAwUEhLZGg5OHlVSUc0alFyM0RWZGYzczZUQXFPKzhxbkVOek9oTTZaQWFQcHRMbmZ0NjVwYVcvZzN0T25QSlpzSHQvZTA0T0FPbG1ycDFyQWNtVDJMYTRWcTA1NDY1dnZMK1BMYU1Sc0lnbE5QUC9ZV25hN25RQ2FGNWUreTdpRjhaOXM0S1JGNnd2MmkwN1d3T2QvSG40YUJVODJHT21ydU1QYkhEeDRPSndWY2JWTzE5K0VlU3VQdXRQRDljZ3dZSzd6SUtVcTg3QVNKcE0xMTZ4TkcvdnI5T1h3WWJTMmRqMFF2dGpNeDRjSzd1SzJpQUt3bE0zeThibXcxeStISTJJalFiemFUSkYrejViKzZibHp6UXhiSEFseWNGK2xxbXVHZ29LWnpycW01Yy9yY09pNWJlWXZKRlJCenRHK1owN3grbW5oOUNtaXByWFBLbXY5OHlaSUVLUmhyc2J3ckFRTUtlTFQvNk9LQ2dTOU91ZWV0Mm53YjcxNWVkUFVFVmxIK3Vuek9RK25vWUhFMkJLb245OEp0MjEvOUIyVjc0Y0ZiQkJOQ1dVc3krd3RZL0hwNjFucm9FallqTTNzVXgxelZSZUVlTFVIMEhpK1VEL0J1Y3VpZGZBRmlkYzVUSWNmZ2M3azlUbHRYcFNYMmZid2Fta091c1N5alg4d0tnVkVFNFRhZG50NW0yNytyZTRlaysrNXROZ1ErbDYrWGdva1RyM1BQSmtLTTFBNVdwYUFvK1VZVEpSWU9UOW50UEhXQmVUb3dJMkNFQ0VTc01LTmxqZ2IrVDBwUnV3MGZEWUhMRGV1c2xaaDlmQUZpWDhpeE5zVUpDQUNMalFjMG9UWlpPdVg3MFJRS2cxVjVsdFFDNndZV1paV3JiZXVtT1h4SzdiOHdpQWJSZmpxclc3UVZTK3oyeUpjc0VkR0VYakFybXg5K1JwcDdUVHNRWWJreWdhVU9aM3c0dllhS1JzRE1HV0tQK1AwM0Z3UGd4QUl6UTVsMjVkdWVnMnhmRDdVNmZ5UUgyMzBURHVmVzJVV1hTay9LK3BwSnh2MTMyVXdJWmlLQ3BiaFl2ajc2RWgzS01YUExFMExxMDk3NlEveG1CVEd1MDY1bmVSdmdoMmd2cnZuTmw5Y0g0RWo4R25oOUxkeDE3YTdLNitqdUx5LzlERExSNDMyMTV3TlU2bXpmMjZzM21YdjEzWDF4ZVByUDZvVy9kdmNJM0NCU2VNNm1EcWRydjc2SXR6QnVxT05kaUdvdko2MjJrcGgrM2pGV3d5Yys2SFRFVGhQQXEyNVRlQUM3elp1WHRmUFZzOVhjZU9oeHBLSzM0TmdYV2JCZ3Yzc2FjSDA2YlNxbmNIMXRGejdNUWpCN2I1bWJveU5Gek12VXNFendqQU5adXFWNTRjcUR2V1lIYzBQYnZHZGpESDRZL3hDcmE1dVBJSWt4VHZMbm5IdnRnQmhVak4vb1RVRmpSMGJONmFiVnBWWDBET3lUdEFKR1grc3o4eUNkd24xaU1Ca2JTMW9hbDVvQzIrRExiNTRwWEpiUDRGZ2FNQitPNmV1ME13Sm44bWM5N2wzaGVPOUQ4dU5kWmdVNHVYcmtUNTB2dWMyMzNqRmV6T25idXJtVHhwcnNidHhaNlpOb3NIRmp6Z1JvaFczTUNqRmJkdHo3TEJPanhMVm9lL3hhV3E2MTFIWDFRTXRNV1hUeDU3UDc0d3VXUERsallpWGZzY1ZWNWQySG53VUxaaHlZcTFXSXlDc05YSHZVRHZleHVBMmJ5d010RmU1MWlEallqa05lQTNkem4xeHl2WU44NjlHNEdKbFJTRWRVZ0o4akMvQUhheVBSL0FVejBZbmNBYzNGaFdkYzdSRmw4R0c0cTU4cWtMN1pNRG1DTnE1cmYyOTR4N0VqREFTSWpISmQrZ0txdjdYMGcvMW1DVFJlV1Y4RUZ1enZYQmVBVWJpbVZENDAvMGt3T0hCT2V3U2w4SGdxbEtiKzlycDVJYzdmQjVzSmZXdnFlSEw2MkhZQThsT3c3NmhZbllxbTVxVVdXNnZiN1JPYUFSdWp5Z0FhUE9Ja3ljY0p2clRHSmNnMzM3OHdzUldGenlOZjJVV1VOK3JHa29uY2M4MkRzamt1N2UyN2FGelk1SEF1eXBRY1B5RFh6Nm44elFYdW42K1V0Kzl2cEdBMnhrWmhSTlplVjh6TllmWk1uaWZFd1lkOU1uZDBYc0FpSm9GWHhzQ3g3R3VGdjhEYmt3VHlXTG1TbUlvYUxtbENzYmZyUmdBempnTVh6WHJ0WkJwN3VqQWpZSU5JYTg0by9ZK2dOWHB1a0FDemM0M3pzejNzR0cwclczZFR2Y2htTlNUeDlXNU82YmZzQVhwVkR6aWs5M05HMXorUnEwSHlYWUlJakE2UXVoeXJoMDY5ejVRWWxFb3dVMk5iK1FGZXpPZmM5cDhGaEZEOFp4Mk9ZVFlFT3hiTm02QVFrVDl6SnZjeHJpb3BBTkl1WVJlNzdzcHJGMjlUNTNiZitvd081TExrSmd6b3c2ODl0YkgzM2c5QXFDVVFFYmpOQlVMbnZFdHU3Y2s0TEZLTHZSOEVjQWJDakdoaTFaUkVMcVYzQ0lSTUVjYk5CN0p6alNNWmxQbUxnT243Nnd2K1lzWGZ2SHpwYTljOXkzK2pEQWxvd3AyS2JGMVJjOFdvUXoyNlppNWkxUmhDTHQ0dTBQM3BPeStjUHJZSU0rWmQ0YVVQa1VhOW9xS3M5UWdwSGJpZzB6dXcrTmtCWjQ4dEFKTHBTYlhESHgwS1ZqeFZwL1kxSGxaanhwOW1WTUdIOFg5USt6UFZrZExMUTlOY0U4T1JIVDk3b3MyenY3NExER1BEb21TYlFTeVpyUERFdHFhNGJTWnM4cnh3SU44d3JhOVpPZXRPMlg5ejEvNTFSQUc0UTZ5K21oQi8zTXFFd2toTzlhcisrcGNMZ1l3cVhKSlk3NjFtMTdVc0ZpeWZaK09oZjZ6RFZHU1dqRGdtS25FOWlPbFd2ZTBUOFd3Q1I0d1lEZzh1YUVJeGxQMG1Nc1duek1XTGJVWlk2N1FWdTRqQm54Z3JpdkNiNExEd25pUzV6MFU3VS9RNkMrSzEyZ3A1L2tSNXNxcXYvQzFyNnBmcU1DRjhVejAwaVhiUWNDWDRuaXZqZVdMQXh6MUVkNDhmbG9sTFFYQkp0NzRQTU9hK0ZKN29HSWpianlnZGZFc25uN0ZOUFNsWVZVZWMxUkVxeWVjYmxhajBrU2IrTmlPUU0xL0Q4Z0FjalhjZFhzSytUY2dyZW9xbVhiVFBXYjBvYlRWdmNiYi9vWmw2NTRIaFBMM3lGUzVyd040R1V2aVJubkRNV1ZUamNOa3FxTEJ0Ky81VklQRmxYbU9USmQ5emFWVTVyb3FHOSsvb2lZbkozekJxemZsVDZ1VEQ5SFp1amU3cWhkbzNYVTcybzdvQ1N5Qy9hUXVnWEhTYlhtc3F0amRPYnhzTndpMWlnNVVJekwxbVpBZTRtazJhNnZSNjA1aDZ2bm5NQlR0S0dPK3RTaTZnb2M2cnZTVGRhOERjQjl4N3gySStzTGEzcmZleitjbWwvME9pNVBjZTJQeE16elZGYk95NTBybm5aNjI2dGVsQmlFOEtXNUNGOVNqUEJqODFrTEw2WVFpMVY0TktKN1ZRejVwU0pDbWFHbThoZHFqQTNOV3RDSm1jRHhTa05GYmZCWTJ6YWV4SEx3Si9YTUN6d2RoM0dZMzI0RDJ5bWJiMEltWk55TDVmQ1JqY3hhd1JYWU9ZVy9HMnNiSjJSQ2hpeVdmUWViRWJhbjFoK0EvZHV4dG5GQ0ptVElZbWs3ME1MOG4vRDJYUTE3Z1F0SHNPQ2k1aTFnUGNhZWtNSHkvd3VBN3YrTVoxeVBBQUFBQUVsRlRrU3VRbUNDJyxcclxuICAgICAgY2hpbGRyZW46IFtcclxuICAgICAgXVxyXG4gICAgfTtcclxuXHJcblxyXG4gICAgY29uc3Qgbm9kZVRlbXBsYXRlID0gKGRhdGEpID0+IHtcclxuICAgICAgbGV0IG9mZmljZSA9IGBgO1xyXG4gICAgICBjb25zdCB0aXRsZSA9IGA8ZGl2IGNsYXNzPVwidGl0bGVcIj4ke2RhdGEubmFtZX08L2Rpdj5gO1xyXG4gICAgICBsZXQgY29udGVudCA9IGA8ZGl2IGNsYXNzPVwiY29udGVudFwiIHRpdGxlPVwiJHtkYXRhLnRpdGxlfVwiPiAke2RhdGEudGl0bGV9IDwvZGl2PmA7XHJcblxyXG4gICAgICBpZiAoZGF0YS5nZW5kZXIpIHtcclxuICAgICAgICBpZiAoZGF0YS5nZW5kZXIgPT09ICdGJykge1xyXG4gICAgICAgICAgY29udGVudCA9IGBcclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi4vYXNzZXRzL2ltYWdlcy91c2VyLWZlbWFsZS5wbmdcIiBjbGFzcz1cIm9yZy1ub2RlLXBpY1wiPiAke2RhdGEudGl0bGV9XHJcbiAgICAgICAgICA8L2Rpdj5gO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb250ZW50ID0gYFxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiLi9hc3NldHMvaW1hZ2VzL3VzZXIucG5nXCIgY2xhc3M9XCJvcmctbm9kZS1waWNcIj4gJHtkYXRhLnRpdGxlfVxyXG4gICAgICAgICAgPC9kaXY+YDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChkYXRhLmltZ1VybCkge1xyXG4gICAgICAgIGNvbnRlbnQgPSBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgICAgICA8aW1nIHNyYz1cIiR7ZGF0YS5pbWdVcmx9XCIgY2xhc3M9XCJvcmctbm9kZS1waWNcIj4gJHtkYXRhLnRpdGxlfVxyXG4gICAgICAgIDwvZGl2PmA7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICBpZiAoZGF0YS5vZmZpY2UpIHtcclxuICAgICAgICBvZmZpY2UgPSBgPHNwYW4gY2xhc3M9XCJvZmZpY2VcIj4ke2RhdGEub2ZmaWNlfTwvc3Bhbj5gO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgdGVtcGxhdGUgPSAnJztcclxuICAgICAgdGVtcGxhdGUgKz0gb2ZmaWNlO1xyXG4gICAgICB0ZW1wbGF0ZSArPSB0aXRsZTtcclxuICAgICAgdGVtcGxhdGUgKz0gY29udGVudDtcclxuXHJcbiAgICAgIHJldHVybiB0ZW1wbGF0ZTtcclxuICAgIH07XHJcblxyXG5cclxuICAgIC8vIGRyYWdhYmxlIGZhbHNlIMOnw7xua8O8IGVrcmFuZGFraSBvcmRlciBib3p1bHV5b3Igc2FwxLF0xLF5b3JcclxuICAgIHRoaXMub3JnQ2hhcnQgPSAkKCcuY2hhcnQtY29udGFpbmVyJykub3JnY2hhcnQoe1xyXG4gICAgICBkYXRhOiB0aGlzLmRhdGFzb3VyY2UsXHJcbiAgICAgIG5vZGVUZW1wbGF0ZSxcclxuICAgICAgdmVydGljYWxMZXZlbDogNSxcclxuICAgICAgdmlzaWJsZUxldmVsOiA0LFxyXG4gICAgICBub2RlQ29udGVudDogJ3RpdGxlJyxcclxuICAgICAgZXhwb3J0QnV0dG9uOiBmYWxzZSxcclxuICAgICAgLy8gZXhwb3J0RmlsZW5hbWU6ICdNeU9yZ0NoYXJ0JyxcclxuICAgICAgLy8gZXhwb3J0RmlsZWV4dGVuc2lvbjogJ3BuZycsXHJcbiAgICAgIGRyYWdnYWJsZTogZmFsc2UsXHJcbiAgICAgIHBhbjogZmFsc2UsXHJcbiAgICAgIHpvb206IGZhbHNlLFxyXG4gICAgICAvLyBkaXJlY3Rpb246ICdsMnInXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hPcmdDaGFydChvcmdDaGFydDogYW55LCBkYXRhc291cmNlOiBhbnkpOiB2b2lkIHtcclxuICAgIC8vICQoJy5jaGFydC1jb250YWluZXInKS5lbXB0eSgpO1xyXG4gICAgb3JnQ2hhcnQuaW5pdCh7IGRhdGE6IEpTT04ucGFyc2UoZGF0YXNvdXJjZSkgfSk7XHJcbiAgfVxyXG5cclxuICBmaWxlQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICBjb25zdCBmaWxlTGlzdDogRmlsZUxpc3QgPSBldmVudC50YXJnZXQuZmlsZXM7XHJcbiAgICBpZiAoZmlsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICBjb25zdCBmaWxlOiBGaWxlID0gZmlsZUxpc3RbMF07XHJcbiAgICAgIGNvbnN0IGZvcm1EYXRhOiBGb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICBmb3JtRGF0YS5hcHBlbmQoJ3VwbG9hZEZpbGUnLCBmaWxlLCBmaWxlLm5hbWUpO1xyXG4gICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgIHRoaXMucmVhZEZpbGUoZmlsZSwgdGhpcy5yZWZyZXNoT3JnQ2hhcnQsIHRoaXMub3JnQ2hhcnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVhZEZpbGUoZmlsZTogYW55LCByZWZyZXNoT3JnQ2hhcnQ6IGFueSwgb3JnQ2hhcnQ6IGFueSk6IHZvaWQge1xyXG4gICAgY29uc3QgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgIHJlYWRlci5yZWFkQXNUZXh0KGZpbGUsICdVVEYtOCcpO1xyXG4gICAgcmVhZGVyLm9ubG9hZCA9IChldnQpID0+IHtcclxuXHJcbiAgICAgIGNvbnN0IGRhdGFzb3VyY2UgPSAoKGV2dC50YXJnZXQpIGFzIEZpbGVSZWFkZXIpLnJlc3VsdDtcclxuICAgICAgdGhpcy5kYXRhc291cmNlID0gZGF0YXNvdXJjZTtcclxuICAgICAgcmVmcmVzaE9yZ0NoYXJ0KG9yZ0NoYXJ0LCBkYXRhc291cmNlKTtcclxuICAgIH07XHJcbiAgICByZWFkZXIub25lcnJvciA9IChldnQpID0+IHtcclxuICAgICAgY29uc29sZS5sb2coJ2Vycm9yIHJlYWRpbmcgZmlsZScpO1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIG9uQ2xpY2tab29tKHpvb21UeXBlOiBzdHJpbmcpIHtcclxuICAgIGNvbnNvbGUubG9nKHpvb21UeXBlKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMub3JnQ2hhcnQpO1xyXG4gICAgbGV0IGN1cnJlbnRab29tID0gcGFyc2VGbG9hdCgkKCcub3JnY2hhcnQnKS5jc3MoJ3pvb20nKSk7XHJcbiAgICB0aGlzLm9yZ0NoYXJ0LnNldENoYXJ0U2NhbGUodGhpcy5vcmdDaGFydC4kY2hhcnQsIHpvb21UeXBlID09PSAnKycgPyBjdXJyZW50Wm9vbSArPSAwLjIgOiBjdXJyZW50Wm9vbSAtPSAwLjIpO1xyXG4gIH1cclxuXHJcbiAgb25DbGlja0V4cG9ydCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xpY2tlZEV4cG9ydC5lbWl0KHsgZGF0YTogdHJ1ZSB9KTtcclxuICB9XHJcblxyXG4gIG9uRXhwb3J0KCkge1xyXG4gICAgdGhpcy5vcmdDaGFydC5leHBvcnQoJ29yZ2FuaXphdGlvbi1zY2hlbWEnLCAncG5nJyk7XHJcbiAgfVxyXG5cclxuICBvbkNsaWNrUmVzZXRBbGwoKTogdm9pZCB7XHJcbiAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xyXG4gICAgICAkKCcub3JnY2hhcnQnKS5jc3MoJ3RyYW5zZm9ybScsICcnKTsgLy8gcmVtb3ZlIHRoZSB0YW5zZm9ybSBzZXR0aW5nc1xyXG4gICAgICB0aGlzLm9yZ0NoYXJ0LmluaXQoeyBkYXRhOiB0aGlzLmRhdGFzb3VyY2UgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19