/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormWizardComponent } from '../form-wizard/form-wizard.component';
var FormWizardStepComponent = /** @class */ (function () {
    function FormWizardStepComponent(parent) {
        this.parent = parent;
        this.title = '';
        this.isValid = true;
        // this.step = 0;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    FormWizardStepComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.isValid) {
            console.log(this.isValid);
            this.parent.isValid = this.isValid;
        }
    };
    /**
     * @return {?}
     */
    FormWizardStepComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.step = this.parent.addStep(this.title);
        this.isCurrent = this.step === this.parent.step;
        this.stepChangeSubcriotion = this.parent.stepChange.subscribe(function (step) {
            _this.isCurrent = _this.step === step;
        });
    };
    /**
     * @return {?}
     */
    FormWizardStepComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.stepChangeSubcriotion) {
            this.stepChangeSubcriotion.unsubscribe();
        }
    };
    FormWizardStepComponent.decorators = [
        { type: Component, args: [{
                    selector: 'wizard-step',
                    host: {
                        '[style.display]': 'isCurrent ? "flex" : "none"',
                    },
                    template: "\n    <ng-content></ng-content>\n  "
                }] }
    ];
    /** @nocollapse */
    FormWizardStepComponent.ctorParameters = function () { return [
        { type: FormWizardComponent }
    ]; };
    FormWizardStepComponent.propDecorators = {
        title: [{ type: Input }],
        isValid: [{ type: Input }]
    };
    return FormWizardStepComponent;
}());
export { FormWizardStepComponent };
if (false) {
    /** @type {?} */
    FormWizardStepComponent.prototype.isCurrent;
    /** @type {?} */
    FormWizardStepComponent.prototype.step;
    /** @type {?} */
    FormWizardStepComponent.prototype.stepChangeSubcriotion;
    /** @type {?} */
    FormWizardStepComponent.prototype.title;
    /** @type {?} */
    FormWizardStepComponent.prototype.isValid;
    /**
     * @type {?}
     * @private
     */
    FormWizardStepComponent.prototype.parent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS13aXphcmQtc3RlcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy93aXphcmQvZm9ybS13aXphcmQtc3RlcC9mb3JtLXdpemFyZC1zdGVwLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQStDLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBRzNFO0lBa0JFLGlDQUFvQixNQUEyQjtRQUEzQixXQUFNLEdBQU4sTUFBTSxDQUFxQjtRQUh0QyxVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFlBQU8sR0FBWSxJQUFJLENBQUM7UUFHL0IsaUJBQWlCO0lBQ25CLENBQUM7Ozs7O0lBRUQsNkNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQzs7OztJQUlELDBDQUFROzs7SUFBUjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBRWhELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQ2hFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsNkNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7Z0JBNUNGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsSUFBSSxFQUFFO3dCQUNKLGlCQUFpQixFQUFFLDZCQUE2QjtxQkFDakQ7b0JBQ0QsUUFBUSxFQUFFLHFDQUVUO2lCQUNGOzs7O2dCQVhRLG1CQUFtQjs7O3dCQWtCekIsS0FBSzswQkFDTCxLQUFLOztJQTZCUiw4QkFBQztDQUFBLEFBN0NELElBNkNDO1NBcENZLHVCQUF1Qjs7O0lBQ2xDLDRDQUFtQjs7SUFDbkIsdUNBQWE7O0lBQ2Isd0RBQW9DOztJQUdwQyx3Q0FBNEI7O0lBQzVCLDBDQUFpQzs7Ozs7SUFFckIseUNBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtV2l6YXJkQ29tcG9uZW50IH0gZnJvbSAnLi4vZm9ybS13aXphcmQvZm9ybS13aXphcmQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3dpemFyZC1zdGVwJyxcclxuICBob3N0OiB7XHJcbiAgICAnW3N0eWxlLmRpc3BsYXldJzogJ2lzQ3VycmVudCA/IFwiZmxleFwiIDogXCJub25lXCInLFxyXG4gIH0sXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICBgLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybVdpemFyZFN0ZXBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcclxuICBpc0N1cnJlbnQ6IGJvb2xlYW47XHJcbiAgc3RlcDogbnVtYmVyO1xyXG4gIHN0ZXBDaGFuZ2VTdWJjcmlvdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHJcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZyA9ICcnO1xyXG4gIEBJbnB1dCgpIGlzVmFsaWQ6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhcmVudDogRm9ybVdpemFyZENvbXBvbmVudCkge1xyXG4gICAgLy8gdGhpcy5zdGVwID0gMDtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmIChjaGFuZ2VzLmlzVmFsaWQpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5pc1ZhbGlkKTtcclxuICAgICAgdGhpcy5wYXJlbnQuaXNWYWxpZCA9IHRoaXMuaXNWYWxpZDtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnN0ZXAgPSB0aGlzLnBhcmVudC5hZGRTdGVwKHRoaXMudGl0bGUpO1xyXG4gICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnN0ZXAgPT09IHRoaXMucGFyZW50LnN0ZXA7XHJcblxyXG4gICAgdGhpcy5zdGVwQ2hhbmdlU3ViY3Jpb3Rpb24gPSB0aGlzLnBhcmVudC5zdGVwQ2hhbmdlLnN1YnNjcmliZShzdGVwID0+IHtcclxuICAgICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnN0ZXAgPT09IHN0ZXA7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMuc3RlcENoYW5nZVN1YmNyaW90aW9uKSB7XHJcbiAgICAgIHRoaXMuc3RlcENoYW5nZVN1YmNyaW90aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==