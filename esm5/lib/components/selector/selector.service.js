/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { DataService } from '../../services/http/data.service';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "../../services/http/data.service";
var SelectorService = /** @class */ (function () {
    function SelectorService(dataService) {
        this.dataService = dataService;
        this.listStateChanged = new Subject();
        this.counter = 0;
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    SelectorService.prototype.setLoadUrlSetting = /**
     * @param {?} urlOptions
     * @return {?}
     */
    function (urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    };
    /**
     * @return {?}
     */
    SelectorService.prototype.getLoadUrlSetting = /**
     * @return {?}
     */
    function () {
        return (/** @type {?} */ ({
            moduleUrl: this.moduleUrl,
            endPointUrl: this.endPointUrl
        }));
    };
    /**
     * @param {?} headerParameters
     * @return {?}
     */
    SelectorService.prototype.setHeaders = /**
     * @param {?} headerParameters
     * @return {?}
     */
    function (headerParameters) {
        this.headerParameters = headerParameters;
    };
    /**
     * @return {?}
     */
    SelectorService.prototype.getHeaders = /**
     * @return {?}
     */
    function () {
        return this.headerParameters;
    };
    /**
     * @param {?} expression
     * @return {?}
     */
    SelectorService.prototype.loadDataByFilterAndPageSetting = /**
     * @param {?} expression
     * @return {?}
     */
    function (expression) {
        var _this = this;
        /** @type {?} */
        var reqUrl = "" + this.moduleUrl + this.endPointUrl;
        this.dataService.getListBy(reqUrl, expression, this.headerParameters)
            .subscribe(function (httpResponse) {
            _this.listStateChanged.next(httpResponse.serviceResult.Result.slice());
        });
    };
    SelectorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SelectorService.ctorParameters = function () { return [
        { type: DataService }
    ]; };
    /** @nocollapse */ SelectorService.ngInjectableDef = i0.defineInjectable({ factory: function SelectorService_Factory() { return new SelectorService(i0.inject(i1.DataService)); }, token: SelectorService, providedIn: "root" });
    return SelectorService;
}());
export { SelectorService };
if (false) {
    /** @type {?} */
    SelectorService.prototype.listStateChanged;
    /** @type {?} */
    SelectorService.prototype.counter;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.moduleUrl;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.endPointUrl;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.headerParameters;
    /**
     * @type {?}
     * @private
     */
    SelectorService.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3NlbGVjdG9yL3NlbGVjdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7OztBQVEvQjtJQVlFLHlCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVJyQyxxQkFBZ0IsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO1FBQ3hDLFlBQU8sR0FBRyxDQUFDLENBQUM7SUFPNkIsQ0FBQzs7Ozs7SUFFakQsMkNBQWlCOzs7O0lBQWpCLFVBQWtCLFVBQXNCO1FBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELDJDQUFpQjs7O0lBQWpCO1FBQ0UsT0FBTyxtQkFBWTtZQUNqQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7WUFDekIsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQzlCLEVBQUEsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsb0NBQVU7Ozs7SUFBVixVQUFXLGdCQUFtQztRQUM1QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELG9DQUFVOzs7SUFBVjtRQUNFLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsd0RBQThCOzs7O0lBQTlCLFVBQStCLFVBQTZCO1FBQTVELGlCQU1DOztZQUxPLE1BQU0sR0FBRyxLQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQWE7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDbEUsU0FBUyxDQUFDLFVBQUMsWUFBcUM7WUFDL0MsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBeENGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBWFEsV0FBVzs7OzBCQURwQjtDQW1EQyxBQXpDRCxJQXlDQztTQXRDWSxlQUFlOzs7SUFDMUIsMkNBQStDOztJQUMvQyxrQ0FBbUI7Ozs7O0lBRW5CLG9DQUF1Qjs7Ozs7SUFDdkIsc0NBQXlCOzs7OztJQUV6QiwyQ0FBNEM7Ozs7O0lBRWhDLHNDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9odHRwL2RhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHtcclxuICBIZWFkZXJQYXJhbWV0ZXIsXHJcbiAgQ29yZUh0dHBSZXNwb25zZSxcclxuICBHZW5lcmljRXhwcmVzc2lvbixcclxuICBVcmxPcHRpb25zXHJcbn0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdG9yU2VydmljZSB7XHJcbiAgcHVibGljIGxpc3RTdGF0ZUNoYW5nZWQgPSBuZXcgU3ViamVjdDxhbnlbXT4oKTtcclxuICBwdWJsaWMgY291bnRlciA9IDA7XHJcblxyXG4gIHByaXZhdGUgbW9kdWxlVXJsOiBhbnk7XHJcbiAgcHJpdmF0ZSBlbmRQb2ludFVybDogYW55O1xyXG5cclxuICBwcml2YXRlIGhlYWRlclBhcmFtZXRlcnM6IEhlYWRlclBhcmFtZXRlcltdO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRhdGFTZXJ2aWNlOiBEYXRhU2VydmljZSkgeyB9XHJcblxyXG4gIHNldExvYWRVcmxTZXR0aW5nKHVybE9wdGlvbnM6IFVybE9wdGlvbnMpIHtcclxuICAgIHRoaXMubW9kdWxlVXJsID0gdXJsT3B0aW9ucy5tb2R1bGVVcmw7XHJcbiAgICB0aGlzLmVuZFBvaW50VXJsID0gdXJsT3B0aW9ucy5lbmRQb2ludFVybDtcclxuICB9XHJcblxyXG4gIGdldExvYWRVcmxTZXR0aW5nKCk6IFVybE9wdGlvbnMge1xyXG4gICAgcmV0dXJuIDxVcmxPcHRpb25zPntcclxuICAgICAgbW9kdWxlVXJsOiB0aGlzLm1vZHVsZVVybCxcclxuICAgICAgZW5kUG9pbnRVcmw6IHRoaXMuZW5kUG9pbnRVcmxcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBzZXRIZWFkZXJzKGhlYWRlclBhcmFtZXRlcnM6IEhlYWRlclBhcmFtZXRlcltdKSB7XHJcbiAgICB0aGlzLmhlYWRlclBhcmFtZXRlcnMgPSBoZWFkZXJQYXJhbWV0ZXJzO1xyXG4gIH1cclxuXHJcbiAgZ2V0SGVhZGVycygpOiBIZWFkZXJQYXJhbWV0ZXJbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5oZWFkZXJQYXJhbWV0ZXJzO1xyXG4gIH1cclxuXHJcbiAgbG9hZERhdGFCeUZpbHRlckFuZFBhZ2VTZXR0aW5nKGV4cHJlc3Npb246IEdlbmVyaWNFeHByZXNzaW9uKSB7XHJcbiAgICBjb25zdCByZXFVcmwgPSBgJHt0aGlzLm1vZHVsZVVybH0ke3RoaXMuZW5kUG9pbnRVcmx9YDtcclxuICAgIHRoaXMuZGF0YVNlcnZpY2UuZ2V0TGlzdEJ5KHJlcVVybCwgZXhwcmVzc2lvbiwgdGhpcy5oZWFkZXJQYXJhbWV0ZXJzKVxyXG4gICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55W10+KSA9PiB7XHJcbiAgICAgICAgdGhpcy5saXN0U3RhdGVDaGFuZ2VkLm5leHQoaHR0cFJlc3BvbnNlLnNlcnZpY2VSZXN1bHQuUmVzdWx0LnNsaWNlKCkpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbn1cclxuIl19