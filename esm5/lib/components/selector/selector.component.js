/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectorService } from './selector.service';
import * as _ from 'lodash';
var SelectorComponent = /** @class */ (function () {
    function SelectorComponent(selectorService) {
        var _this = this;
        this.selectorService = selectorService;
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
        this.value = 'Id';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.selectorService.listStateChanged
            .subscribe(function (result) {
            if (result && result.length > 0 && _this._labelField === 'CombinedFields') {
                result.map(function (item) {
                    /** @type {?} */
                    var combinedValue = '';
                    ((/** @type {?} */ (_this.labelField))).forEach(function (field) {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + _this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -_this.seperator.length) : null;
                    return item;
                });
            }
            _this.listData = _.orderBy(result, [
                'OrderNo',
                function (z) { return z[_this._labelField] && (typeof z[_this._labelField] === 'string') ? z[_this._labelField].toLowerCase() : z[_this._labelField]; },
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                _this._labelField
            ], ['asc']);
            _this.findReadonlyValue();
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    SelectorComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.requestOptions && this.requestOptions) {
            this.urlOptions = this.requestOptions.UrlOptions;
            this.customFilter = this.requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(this.requestOptions.HeaderParameters || []);
            this.load();
        }
    };
    /**
     * @param {?} requestOptions
     * @param {?=} value
     * @param {?=} labelField
     * @param {?=} seperator
     * @return {?}
     */
    SelectorComponent.prototype.setRequestOptions = /**
     * @param {?} requestOptions
     * @param {?=} value
     * @param {?=} labelField
     * @param {?=} seperator
     * @return {?}
     */
    function (requestOptions, value, labelField, seperator) {
        if (value === void 0) { value = null; }
        if (labelField === void 0) { labelField = null; }
        if (seperator === void 0) { seperator = ' '; }
        if (value) {
            this.value = value;
            this.model = value;
        }
        if (labelField) {
            this.labelField = labelField;
        }
        if (seperator) {
            this.seperator = seperator;
        }
        if (this.labelField instanceof Array) {
            this._labelField = 'CombinedFields';
        }
        else if (this.labelField) {
            this._labelField = (/** @type {?} */ (this.labelField));
        }
        if (requestOptions) {
            this.urlOptions = requestOptions.UrlOptions;
            this.customFilter = requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(requestOptions.HeaderParameters || []);
        }
    };
    /**
     * @return {?}
     */
    SelectorComponent.prototype.load = /**
     * @return {?}
     */
    function () {
        this.getDataSource();
    };
    /**
     * @return {?}
     */
    SelectorComponent.prototype.reload = /**
     * @return {?}
     */
    function () {
        this.getDataSource();
    };
    /**
     * @return {?}
     */
    SelectorComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.listData = [];
    };
    /**
     * @param {?} val
     * @return {?}
     */
    SelectorComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    SelectorComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(SelectorComponent.prototype, "Model", {
        get: /**
         * @return {?}
         */
        function () {
            return this.model;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            var _this = this;
            this.model = val;
            this.onChange(val);
            this.onTouched();
            if (val) {
                if (this.listData) {
                    /** @type {?} */
                    var data = this.listData.find(function (x) { return x[_this.value] === _this.model; });
                    if (data) {
                        this.changed.emit(data);
                        this.readOnlyValue = data[this._labelField];
                    }
                }
                else {
                    /** @type {?} */
                    var emitData = {};
                    emitData[this.value] = this.model;
                    this.readOnlyValue = this.model;
                    this.changed.emit(emitData);
                }
            }
            else {
                this.changed.emit(null);
                this.readOnlyValue = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    SelectorComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    SelectorComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    SelectorComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
    };
    /**
     * @param {?} value
     * @return {?}
     */
    SelectorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.model = value;
        this.findReadonlyValue();
    };
    /**
     * @return {?}
     */
    SelectorComponent.prototype.findReadonlyValue = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.readOnlyValue = null;
        if (this.listData) {
            if (this.model || this.model === 0) {
                /** @type {?} */
                var data = this.listData.find(function (x) { return x[_this.value] === _this.model; });
                if (data) {
                    this.readOnlyValue = data[this._labelField];
                    if (!this.isFirstValueSend) {
                        this.firstValue.emit(data);
                        this.isFirstValueSend = true;
                    }
                }
            }
        }
    };
    /**
     * @private
     * @return {?}
     */
    SelectorComponent.prototype.getDataSource = /**
     * @private
     * @return {?}
     */
    function () {
        this.selectorService.loadDataByFilterAndPageSetting(this.customFilter);
    };
    SelectorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-selector',
                    template: "\n    <ng-select *ngIf=\"!isReadOnly\"\n      class=\"custom-ng-select form-selector\"\n      [items]=\"listData\"\n      [searchable]=\"searchable\"\n\t\t\t[clearable]=\"clearable\"\n      [bindValue]=\"value\"\n      [bindLabel]=\"_labelField\"\n      [(ngModel)]=\"Model\"\n      [disabled]=\"isDisabled\"\n    >\n    </ng-select>\n    <layout-form-text-input class=\"readonly-text-input\" *ngIf=\"isReadOnly\"\n      [isReadOnly]=\"isReadOnly\"\n      [isDisabled]=\"isDisabled\"\n      [(ngModel)]=\"readOnlyValue\"></layout-form-text-input>\n  ",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return SelectorComponent; }),
                            multi: true
                        },
                        SelectorService
                    ],
                    styles: [".custom-ng-select{height:30px!important;min-width:100px;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
                }] }
    ];
    /** @nocollapse */
    SelectorComponent.ctorParameters = function () { return [
        { type: SelectorService }
    ]; };
    SelectorComponent.propDecorators = {
        isDisabled: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        searchable: [{ type: Input }],
        clearable: [{ type: Input }],
        requestOptions: [{ type: Input }],
        changed: [{ type: Output }],
        firstValue: [{ type: Output }]
    };
    return SelectorComponent;
}());
export { SelectorComponent };
if (false) {
    /** @type {?} */
    SelectorComponent.prototype.isDisabled;
    /** @type {?} */
    SelectorComponent.prototype.isReadOnly;
    /** @type {?} */
    SelectorComponent.prototype.searchable;
    /** @type {?} */
    SelectorComponent.prototype.clearable;
    /** @type {?} */
    SelectorComponent.prototype.requestOptions;
    /** @type {?} */
    SelectorComponent.prototype.changed;
    /** @type {?} */
    SelectorComponent.prototype.firstValue;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.isFirstValueSend;
    /** @type {?} */
    SelectorComponent.prototype.listData;
    /** @type {?} */
    SelectorComponent.prototype.value;
    /** @type {?} */
    SelectorComponent.prototype.labelField;
    /** @type {?} */
    SelectorComponent.prototype._labelField;
    /** @type {?} */
    SelectorComponent.prototype.seperator;
    /** @type {?} */
    SelectorComponent.prototype.readOnlyValue;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.model;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.customFilter;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.urlOptions;
    /**
     * @type {?}
     * @private
     */
    SelectorComponent.prototype.selectorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc2VsZWN0b3Ivc2VsZWN0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDN0csT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBR3pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQVFyRCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QjtJQXdERSwyQkFBb0IsZUFBZ0M7UUFBcEQsaUJBeUJDO1FBekJtQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUF6QjNDLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFFaEIsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7O1FBR2xDLGVBQVUsR0FBdUIsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUk1RCxVQUFLLEdBQUcsSUFBSSxDQUFDO1FBRWIsZ0JBQVcsR0FBRyxZQUFZLENBQUM7UUFFbEMsY0FBUyxHQUFHLEdBQUcsQ0FBQztRQVVkLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCO2FBQ2xDLFNBQVMsQ0FBQyxVQUFDLE1BQWE7WUFDdkIsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSSxDQUFDLFdBQVcsS0FBSyxnQkFBZ0IsRUFBRTtnQkFDeEUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQVM7O3dCQUNmLGFBQWEsR0FBRyxFQUFFO29CQUN0QixDQUFDLG1CQUFVLEtBQUksQ0FBQyxVQUFVLEVBQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQWE7d0JBQ2hELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFOzRCQUNmLGFBQWEsR0FBRyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7eUJBQzlEO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUVILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQy9GLE9BQU8sSUFBSSxDQUFDO2dCQUNkLENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUM5QjtnQkFDRSxTQUFTO2dCQUNULFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBMUgsQ0FBMEg7Z0JBQy9ILG1IQUFtSDtnQkFDbkgsS0FBSSxDQUFDLFdBQVc7YUFDakIsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDZCxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsdUNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDakQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUNyRCxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFRCw2Q0FBaUI7Ozs7Ozs7SUFBakIsVUFBa0IsY0FBOEIsRUFBRSxLQUFvQixFQUFFLFVBQXNCLEVBQUUsU0FBdUI7UUFBckUsc0JBQUEsRUFBQSxZQUFvQjtRQUFFLDJCQUFBLEVBQUEsaUJBQXNCO1FBQUUsMEJBQUEsRUFBQSxlQUF1QjtRQUNySCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ3BCO1FBRUQsSUFBSSxVQUFVLEVBQUU7WUFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztTQUM5QjtRQUVELElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7U0FDNUI7UUFHRCxJQUFJLElBQUksQ0FBQyxVQUFVLFlBQVksS0FBSyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsZ0JBQWdCLENBQUM7U0FDckM7YUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxtQkFBUSxJQUFJLENBQUMsVUFBVSxFQUFBLENBQUM7U0FDNUM7UUFFRCxJQUFJLGNBQWMsRUFBRTtZQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDNUMsSUFBSSxDQUFDLFlBQVksR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUN4RTtJQUNILENBQUM7Ozs7SUFFRCxnQ0FBSTs7O0lBQUo7UUFDRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELGtDQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7O0lBRUQsaUNBQUs7OztJQUFMO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxvQ0FBUTs7OztJQUFSLFVBQVMsR0FBUTtJQUNqQixDQUFDOzs7O0lBRUQscUNBQVM7OztJQUFUO0lBQ0EsQ0FBQztJQUVELHNCQUFJLG9DQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDcEIsQ0FBQzs7Ozs7UUFFRCxVQUFVLEdBQUc7WUFBYixpQkFzQkM7WUFyQkMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7WUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFakIsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFOzt3QkFDWCxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUksQ0FBQyxLQUFLLEVBQTVCLENBQTRCLENBQUM7b0JBQ2xFLElBQUksSUFBSSxFQUFFO3dCQUNSLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQzdDO2lCQUNGO3FCQUFNOzt3QkFDQyxRQUFRLEdBQVEsRUFBRTtvQkFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO29CQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUM3QjthQUNGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzthQUMzQjtRQUNILENBQUM7OztPQXhCQTs7Ozs7SUEwQkQsNENBQWdCOzs7O0lBQWhCLFVBQWlCLEVBQU87UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCw2Q0FBaUI7Ozs7SUFBakIsVUFBa0IsRUFBTztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELDRDQUFnQjs7OztJQUFoQixVQUFpQixVQUFtQjtJQUNwQyxDQUFDOzs7OztJQUVELHNDQUFVOzs7O0lBQVYsVUFBVyxLQUFVO1FBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCw2Q0FBaUI7OztJQUFqQjtRQUFBLGlCQWNDO1FBYkMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsRUFBRTs7b0JBQzVCLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSSxDQUFDLEtBQUssRUFBNUIsQ0FBNEIsQ0FBQztnQkFDbEUsSUFBSSxJQUFJLEVBQUU7b0JBQ1IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO3dCQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztxQkFDOUI7aUJBQ0Y7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFTyx5Q0FBYTs7OztJQUFyQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3pFLENBQUM7O2dCQTFNRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsUUFBUSxFQUFFLHdpQkFnQlQ7b0JBRUQsU0FBUyxFQUFFO3dCQUNUOzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGlCQUFpQixFQUFqQixDQUFpQixDQUFDOzRCQUNoRCxLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRCxlQUFlO3FCQUNoQjs7aUJBQ0Y7Ozs7Z0JBdENRLGVBQWU7Ozs2QkF5Q3JCLEtBQUs7NkJBQ0wsS0FBSzs2QkFDTCxLQUFLOzRCQUNMLEtBQUs7aUNBQ0wsS0FBSzswQkFDTCxNQUFNOzZCQUdOLE1BQU07O0lBb0tULHdCQUFDO0NBQUEsQUEzTUQsSUEyTUM7U0E5S1ksaUJBQWlCOzs7SUFFNUIsdUNBQTRCOztJQUM1Qix1Q0FBNEI7O0lBQzVCLHVDQUEyQjs7SUFDM0Isc0NBQTBCOztJQUMxQiwyQ0FBeUM7O0lBQ3pDLG9DQUE0Qzs7SUFHNUMsdUNBQW1FOzs7OztJQUNuRSw2Q0FBa0M7O0lBRWxDLHFDQUF1Qjs7SUFDdkIsa0NBQW9COztJQUNwQix1Q0FBcUM7O0lBQ3JDLHdDQUFrQzs7SUFFbEMsc0NBQWdCOztJQUVoQiwwQ0FBNkI7Ozs7O0lBRTdCLGtDQUFjOzs7OztJQUNkLHlDQUF3Qzs7Ozs7SUFDeEMsdUNBQStCOzs7OztJQUduQiw0Q0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBmb3J3YXJkUmVmLCBJbnB1dCwgU2ltcGxlQ2hhbmdlcywgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcblxyXG5pbXBvcnQgeyBTZWxlY3RvclNlcnZpY2UgfSBmcm9tICcuL3NlbGVjdG9yLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHtcclxuICBSZXF1ZXN0T3B0aW9ucyxcclxuICBVcmxPcHRpb25zLFxyXG4gIEdlbmVyaWNFeHByZXNzaW9uXHJcbn0gZnJvbSAnLi4vLi4vbW9kZWxzL2luZGV4JztcclxuXHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LXNlbGVjdG9yJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPG5nLXNlbGVjdCAqbmdJZj1cIiFpc1JlYWRPbmx5XCJcclxuICAgICAgY2xhc3M9XCJjdXN0b20tbmctc2VsZWN0IGZvcm0tc2VsZWN0b3JcIlxyXG4gICAgICBbaXRlbXNdPVwibGlzdERhdGFcIlxyXG4gICAgICBbc2VhcmNoYWJsZV09XCJzZWFyY2hhYmxlXCJcclxuXHRcdFx0W2NsZWFyYWJsZV09XCJjbGVhcmFibGVcIlxyXG4gICAgICBbYmluZFZhbHVlXT1cInZhbHVlXCJcclxuICAgICAgW2JpbmRMYWJlbF09XCJfbGFiZWxGaWVsZFwiXHJcbiAgICAgIFsobmdNb2RlbCldPVwiTW9kZWxcIlxyXG4gICAgICBbZGlzYWJsZWRdPVwiaXNEaXNhYmxlZFwiXHJcbiAgICA+XHJcbiAgICA8L25nLXNlbGVjdD5cclxuICAgIDxsYXlvdXQtZm9ybS10ZXh0LWlucHV0IGNsYXNzPVwicmVhZG9ubHktdGV4dC1pbnB1dFwiICpuZ0lmPVwiaXNSZWFkT25seVwiXHJcbiAgICAgIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICBbaXNEaXNhYmxlZF09XCJpc0Rpc2FibGVkXCJcclxuICAgICAgWyhuZ01vZGVsKV09XCJyZWFkT25seVZhbHVlXCI+PC9sYXlvdXQtZm9ybS10ZXh0LWlucHV0PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0b3IuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IFNlbGVjdG9yQ29tcG9uZW50KSxcclxuICAgICAgbXVsdGk6IHRydWVcclxuICAgIH0sXHJcbiAgICBTZWxlY3RvclNlcnZpY2VcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RvckNvbXBvbmVudCBpbXBsZW1lbnRzIENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBPbkNoYW5nZXMge1xyXG5cclxuICBASW5wdXQoKSBpc0Rpc2FibGVkID0gZmFsc2U7XHJcbiAgQElucHV0KCkgaXNSZWFkT25seSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHNlYXJjaGFibGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGNsZWFyYWJsZSA9IHRydWU7XHJcbiAgQElucHV0KCkgcmVxdWVzdE9wdGlvbnM/OiBSZXF1ZXN0T3B0aW9ucztcclxuICBAT3V0cHV0KCkgY2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAvLyBpdCBpcyBmb3IgZ2V0dGluZyB0aGUgZmlyc3Qgc2VsZWN0ZWQgdmFsdWVcclxuICBAT3V0cHV0KCkgZmlyc3RWYWx1ZT86IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgcHJpdmF0ZSBpc0ZpcnN0VmFsdWVTZW5kOiBib29sZWFuO1xyXG5cclxuICBwdWJsaWMgbGlzdERhdGE6IGFueVtdO1xyXG4gIHB1YmxpYyB2YWx1ZSA9ICdJZCc7XHJcbiAgcHVibGljIGxhYmVsRmllbGQ6IHN0cmluZyB8IHN0cmluZ1tdO1xyXG4gIHB1YmxpYyBfbGFiZWxGaWVsZCA9ICdEZWZpbml0aW9uJztcclxuXHJcbiAgc2VwZXJhdG9yID0gJyAnO1xyXG5cclxuICBwdWJsaWMgcmVhZE9ubHlWYWx1ZTogc3RyaW5nO1xyXG5cclxuICBwcml2YXRlIG1vZGVsO1xyXG4gIHByaXZhdGUgY3VzdG9tRmlsdGVyOiBHZW5lcmljRXhwcmVzc2lvbjtcclxuICBwcml2YXRlIHVybE9wdGlvbnM6IFVybE9wdGlvbnM7XHJcblxyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlbGVjdG9yU2VydmljZTogU2VsZWN0b3JTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLnNlbGVjdG9yU2VydmljZS5saXN0U3RhdGVDaGFuZ2VkXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55W10pID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5sZW5ndGggPiAwICYmIHRoaXMuX2xhYmVsRmllbGQgPT09ICdDb21iaW5lZEZpZWxkcycpIHtcclxuICAgICAgICAgIHJlc3VsdC5tYXAoKGl0ZW06IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgY29tYmluZWRWYWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICAoPHN0cmluZ1tdPnRoaXMubGFiZWxGaWVsZCkuZm9yRWFjaCgoZmllbGQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChpdGVtW2ZpZWxkXSkge1xyXG4gICAgICAgICAgICAgICAgY29tYmluZWRWYWx1ZSA9IGNvbWJpbmVkVmFsdWUgKyBpdGVtW2ZpZWxkXSArIHRoaXMuc2VwZXJhdG9yO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBpdGVtWydDb21iaW5lZEZpZWxkcyddID0gY29tYmluZWRWYWx1ZSA/IGNvbWJpbmVkVmFsdWUuc2xpY2UoMCwgLXRoaXMuc2VwZXJhdG9yLmxlbmd0aCkgOiBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3REYXRhID0gXy5vcmRlckJ5KHJlc3VsdCxcclxuICAgICAgICAgIFtcclxuICAgICAgICAgICAgJ09yZGVyTm8nLFxyXG4gICAgICAgICAgICB6ID0+IHpbdGhpcy5fbGFiZWxGaWVsZF0gJiYgKHR5cGVvZiB6W3RoaXMuX2xhYmVsRmllbGRdID09PSAnc3RyaW5nJykgPyB6W3RoaXMuX2xhYmVsRmllbGRdLnRvTG93ZXJDYXNlKCkgOiB6W3RoaXMuX2xhYmVsRmllbGRdLFxyXG4gICAgICAgICAgICAvLyB5ID0+IHlbJ0RlZmluaXRpb24nXSAmJiAodHlwZW9mIHlbJ0RlZmluaXRpb24nXSA9PT0gJ3N0cmluZycpID8geVsnRGVmaW5pdGlvbiddLnRvTG93ZXJDYXNlKCkgOiB5WydEZWZpbml0aW9uJ10sXHJcbiAgICAgICAgICAgIHRoaXMuX2xhYmVsRmllbGRcclxuICAgICAgICAgIF0sIFsnYXNjJ10pO1xyXG4gICAgICAgIHRoaXMuZmluZFJlYWRvbmx5VmFsdWUoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICBpZiAoY2hhbmdlcy5yZXF1ZXN0T3B0aW9ucyAmJiB0aGlzLnJlcXVlc3RPcHRpb25zKSB7XHJcbiAgICAgIHRoaXMudXJsT3B0aW9ucyA9IHRoaXMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucztcclxuICAgICAgdGhpcy5jdXN0b21GaWx0ZXIgPSB0aGlzLnJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlcjtcclxuICAgICAgdGhpcy5zZWxlY3RvclNlcnZpY2Uuc2V0TG9hZFVybFNldHRpbmcodGhpcy51cmxPcHRpb25zKTtcclxuICAgICAgdGhpcy5zZWxlY3RvclNlcnZpY2Uuc2V0SGVhZGVycyh0aGlzLnJlcXVlc3RPcHRpb25zLkhlYWRlclBhcmFtZXRlcnMgfHwgW10pO1xyXG4gICAgICB0aGlzLmxvYWQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldFJlcXVlc3RPcHRpb25zKHJlcXVlc3RPcHRpb25zOiBSZXF1ZXN0T3B0aW9ucywgdmFsdWU6IHN0cmluZyA9IG51bGwsIGxhYmVsRmllbGQ6IGFueSA9IG51bGwsIHNlcGVyYXRvcjogc3RyaW5nID0gJyAnKSB7XHJcbiAgICBpZiAodmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICB0aGlzLm1vZGVsID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGxhYmVsRmllbGQpIHtcclxuICAgICAgdGhpcy5sYWJlbEZpZWxkID0gbGFiZWxGaWVsZDtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoc2VwZXJhdG9yKSB7XHJcbiAgICAgIHRoaXMuc2VwZXJhdG9yID0gc2VwZXJhdG9yO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBpZiAodGhpcy5sYWJlbEZpZWxkIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgdGhpcy5fbGFiZWxGaWVsZCA9ICdDb21iaW5lZEZpZWxkcyc7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMubGFiZWxGaWVsZCkge1xyXG4gICAgICB0aGlzLl9sYWJlbEZpZWxkID0gPHN0cmluZz50aGlzLmxhYmVsRmllbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHJlcXVlc3RPcHRpb25zKSB7XHJcbiAgICAgIHRoaXMudXJsT3B0aW9ucyA9IHJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnM7XHJcbiAgICAgIHRoaXMuY3VzdG9tRmlsdGVyID0gcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyO1xyXG4gICAgICB0aGlzLnNlbGVjdG9yU2VydmljZS5zZXRMb2FkVXJsU2V0dGluZyh0aGlzLnVybE9wdGlvbnMpO1xyXG4gICAgICB0aGlzLnNlbGVjdG9yU2VydmljZS5zZXRIZWFkZXJzKHJlcXVlc3RPcHRpb25zLkhlYWRlclBhcmFtZXRlcnMgfHwgW10pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZCgpIHtcclxuICAgIHRoaXMuZ2V0RGF0YVNvdXJjZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVsb2FkKCkge1xyXG4gICAgdGhpcy5nZXREYXRhU291cmNlKCk7XHJcbiAgfVxyXG5cclxuICByZXNldCgpIHtcclxuICAgIHRoaXMubGlzdERhdGEgPSBbXTtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlKHZhbDogYW55KSB7XHJcbiAgfVxyXG5cclxuICBvblRvdWNoZWQoKSB7XHJcbiAgfVxyXG5cclxuICBnZXQgTW9kZWwoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5tb2RlbDtcclxuICB9XHJcblxyXG4gIHNldCBNb2RlbCh2YWwpIHtcclxuICAgIHRoaXMubW9kZWwgPSB2YWw7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG5cclxuICAgIGlmICh2YWwpIHtcclxuICAgICAgaWYgKHRoaXMubGlzdERhdGEpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5saXN0RGF0YS5maW5kKHggPT4geFt0aGlzLnZhbHVlXSA9PT0gdGhpcy5tb2RlbCk7XHJcbiAgICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICAgIHRoaXMuY2hhbmdlZC5lbWl0KGRhdGEpO1xyXG4gICAgICAgICAgdGhpcy5yZWFkT25seVZhbHVlID0gZGF0YVt0aGlzLl9sYWJlbEZpZWxkXTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgZW1pdERhdGE6IGFueSA9IHt9O1xyXG4gICAgICAgIGVtaXREYXRhW3RoaXMudmFsdWVdID0gdGhpcy5tb2RlbDtcclxuICAgICAgICB0aGlzLnJlYWRPbmx5VmFsdWUgPSB0aGlzLm1vZGVsO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlZC5lbWl0KGVtaXREYXRhKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGFuZ2VkLmVtaXQobnVsbCk7XHJcbiAgICAgIHRoaXMucmVhZE9ubHlWYWx1ZSA9IG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMub25DaGFuZ2UgPSBmbjtcclxuICB9XHJcblxyXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKGZuOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICBzZXREaXNhYmxlZFN0YXRlKGlzRGlzYWJsZWQ6IGJvb2xlYW4pOiB2b2lkIHtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5tb2RlbCA9IHZhbHVlO1xyXG4gICAgdGhpcy5maW5kUmVhZG9ubHlWYWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgZmluZFJlYWRvbmx5VmFsdWUoKTogdm9pZCB7XHJcbiAgICB0aGlzLnJlYWRPbmx5VmFsdWUgPSBudWxsO1xyXG4gICAgaWYgKHRoaXMubGlzdERhdGEpIHtcclxuICAgICAgaWYgKHRoaXMubW9kZWwgfHwgdGhpcy5tb2RlbCA9PT0gMCkge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmxpc3REYXRhLmZpbmQoeCA9PiB4W3RoaXMudmFsdWVdID09PSB0aGlzLm1vZGVsKTtcclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgdGhpcy5yZWFkT25seVZhbHVlID0gZGF0YVt0aGlzLl9sYWJlbEZpZWxkXTtcclxuICAgICAgICAgIGlmICghdGhpcy5pc0ZpcnN0VmFsdWVTZW5kKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyc3RWYWx1ZS5lbWl0KGRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmlzRmlyc3RWYWx1ZVNlbmQgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXREYXRhU291cmNlKCkge1xyXG4gICAgdGhpcy5zZWxlY3RvclNlcnZpY2UubG9hZERhdGFCeUZpbHRlckFuZFBhZ2VTZXR0aW5nKHRoaXMuY3VzdG9tRmlsdGVyKTtcclxuICB9XHJcbn1cclxuIl19