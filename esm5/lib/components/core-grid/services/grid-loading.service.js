/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { LoadingService } from '../../../services/custom/loading.service';
var GridLoadingService = /** @class */ (function (_super) {
    tslib_1.__extends(GridLoadingService, _super);
    function GridLoadingService() {
        return _super.call(this) || this;
    }
    GridLoadingService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    GridLoadingService.ctorParameters = function () { return []; };
    return GridLoadingService;
}(LoadingService));
export { GridLoadingService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1sb2FkaW5nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvc2VydmljZXMvZ3JpZC1sb2FkaW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUxRTtJQUN3Qyw4Q0FBYztJQUNsRDtlQUNJLGlCQUFPO0lBQ1gsQ0FBQzs7Z0JBSkosVUFBVTs7OztJQUtYLHlCQUFDO0NBQUEsQUFMRCxDQUN3QyxjQUFjLEdBSXJEO1NBSlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBMb2FkaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2N1c3RvbS9sb2FkaW5nLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgR3JpZExvYWRpbmdTZXJ2aWNlIGV4dGVuZHMgTG9hZGluZ1NlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgIH1cclxufVxyXG4iXX0=