/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
// Enums
import { OperationType, CoreGridCellType, SortType, CoreGridColumnType, ConfirmDialogOperationType, ToastrMessages, Headers, Comparison, Connector } from '../../../enums/index';
// Services
import { DataService, ToastrUtilsService, SweetAlertService } from '../../../services/index';
// components
import { CommandCellComponent } from '../cell/command/command-cell.component';
import { TextFilterCellComponent } from '../cell/filter/text-filter-cell/text-filter-cell.component';
import { SelectFilterCellComponent } from '../cell/filter/select-filter-cell/select-filter-cell.component';
import { DateFilterCellComponent } from '../cell/filter/date-filter-cell/date-filter-cell.component';
import { NumericFilterCellComponent } from '../cell/filter/numeric-filter-cell/numeric-filter-cell.component';
import { BooleanFilterCellComponent } from '../cell/filter/boolean-filter-cell/boolean-filter-cell.component';
import { CurrencyFilterCellComponent } from '../cell/filter/currency-filter-cell/currency-filter-cell.component';
import { LovFilterCellComponent } from '../cell/filter/lov-filter-cell/lov-filter-cell.component';
import { MaskFilterCellComponent } from '../cell/filter/mask-filter-cell/mask-filter-cell.component';
import { SelectCellComponent } from '../cell/read-only/select-cell/select-cell.component';
import { LabelCellComponent } from '../cell/read-only/label-cell/label-cell.component';
import { DateCellComponent } from '../cell/read-only/date-cell/date-cell.component';
import { BooleanCellComponent } from '../cell/read-only/boolean-cell/boolean-cell.component';
import { CurrencyCellComponent } from '../cell/read-only/currency-cell/currency-cell.component';
import { MaskCellComponent } from '../cell/read-only/mask-cell/mask-cell.component';
import { NumericCellComponent } from '../cell/read-only/numeric-cell/numeric-cell.component';
import { TextEditCellComponent } from '../cell/edit/text-edit-cell/text-edit-cell.component';
import { SelectEditCellComponent } from '../cell/edit/select-edit-cell/select-edit-cell.component';
import { NumericEditCellComponent } from '../cell/edit/numeric-edit-cell/numeric-edit-cell.component';
import { LovEditCellComponent } from '../cell/edit/lov-edit-cell/lov-edit-cell.component';
import { DateEditCellComponent } from '../cell/edit/date-edit-cell/date-edit-cell.component';
import { BooleanEditCellComponent } from '../cell/edit/boolean-edit-cell/boolean-edit-cell.component';
import { MaskEditCellComponent } from '../cell/edit/mask-edit-cell/mask-edit-cell.component';
import { CurrencyEditCellComponent } from '../cell/edit/currency-edit-cell/currency-edit-cell.component';
import { ColumnHeaderComponent } from '../column-header/column-header.component';
import { NoRowsOverlayComponent } from '../overlay/no-rows/no-rows-overlay.component';
import { LoadingOverlayComponent } from '../overlay/loading/loading-overlay.component';
import { GridLoadingService } from './grid-loading.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../services/http/data.service";
import * as i2 from "../../../services/custom/toastr.service";
import * as i3 from "./grid-loading.service";
import * as i4 from "../../../services/custom/sweet.alert.confirm.service";
import * as i5 from "@angular/router";
var CoreGridService = /** @class */ (function () {
    function CoreGridService(dataService, toastr, loadingService, sweetAlertService, ngZone, router, route) {
        var _this = this;
        this.dataService = dataService;
        this.toastr = toastr;
        this.loadingService = loadingService;
        this.sweetAlertService = sweetAlertService;
        this.ngZone = ngZone;
        this.router = router;
        this.route = route;
        this.pagingResult = {};
        this.data = [];
        this.originalRowData = [];
        this.deletedData = [];
        this.filter = [];
        this.filterGroupList = [];
        this.gridForm = new FormGroup({});
        this.loadOnInit = true;
        this.isDataChanged = false;
        this.isLoad = false;
        this.columnDefs = [];
        this.allGridColumns = [];
        this.recordEventsSubject = new Subject();
        this.retrievedDataSourceSubject = new Subject();
        this.loadingDisplayStatusSubscription = this.loadingService.getLoadingBarDisplayStatus().subscribe(function (data) {
            _this.loadingDisplayStatus = data;
            _this.loadingOverlay();
        });
    }
    /**
     * @param {?} options
     * @param {?} loadOnInit
     * @param {?=} loadColumnDataSourceOnInit
     * @return {?}
     */
    CoreGridService.prototype.init = /**
     * @param {?} options
     * @param {?} loadOnInit
     * @param {?=} loadColumnDataSourceOnInit
     * @return {?}
     */
    function (options, loadOnInit, loadColumnDataSourceOnInit) {
        this.loadOnInit = loadOnInit;
        this.loadColumnDataSourceOnInit = loadColumnDataSourceOnInit;
        this.gridOptions = options;
        if (this.gridApi) {
            this.pagingResult = {
                CurrentPage: 1,
                PageSize: options.requestOptions.CustomFilter.PageSize
            };
            this.reloadColumnDefs();
            this.setLoadUrlSetting();
            if (this.loadOnInit === true) {
                this.isLoad = true;
                this.refreshGridData();
            }
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.setLoadUrlSetting = /**
     * @return {?}
     */
    function () {
        this.urlOptions = {
            moduleUrl: this.gridOptions.requestOptions.UrlOptions.moduleUrl,
            endPointUrl: this.gridOptions.requestOptions.UrlOptions.endPointUrl
        };
    };
    /**
     * @private
     * @return {?}
     */
    CoreGridService.prototype.createColumns = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.gridOptions && this.gridOptions.enableCheckboxSelection) {
            /** @type {?} */
            var selectColumn = {
                headerName: 'Select',
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: true,
                checkboxSelection: true,
                minWidth: 40,
                maxWidth: 40,
                cellStyle: { 'padding-top': '5px' },
                showRowGroup: false,
                enableRowGroup: false,
                enablePivot: false,
                suppressFilter: true,
                menuTabs: []
            };
            this.columnDefs.push(selectColumn);
        }
        this.gridOptions.columns.forEach(function (item) {
            /** @type {?} */
            var cellRenderer;
            /** @type {?} */
            var columEditable = _this.gridOptions.editable;
            if (item.editable !== undefined) {
                columEditable = item.editable;
            }
            if (columEditable) {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreTextEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreLOVEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyEditCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
            }
            else {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.TextSelect) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
            }
            if (item.visible !== false) {
                item.visible = true;
            }
            else {
                item.visible = false;
            }
            /** @type {?} */
            var headerName = item.field;
            if (_this.gridOptions
                && _this.gridOptions.requestOptions
                && _this.gridOptions.requestOptions.UrlOptions
                && _this.gridOptions.requestOptions.UrlOptions.endPointUrl) {
                /** @type {?} */
                var endPointUrl = _this.gridOptions.requestOptions.UrlOptions.endPointUrl;
                if (endPointUrl.indexOf('/') !== -1) {
                    headerName = endPointUrl.split('/')[0] + item.field;
                }
                else {
                    headerName = endPointUrl + item.field;
                }
            }
            /** @type {?} */
            var column = {
                field: item.field,
                headerName: item.headerName || headerName,
                cellRenderer: cellRenderer,
                hide: !item.visible,
                suppressSizeToFit: null,
                filter: _this.gridOptions.enableFilter || false,
                sortable: _this.gridOptions.enableSorting || false,
                resizable: true,
                cellEditorParams: {
                    dataSource: null,
                    settings: null,
                    data: null,
                    customDisplayFunction: item.customDisplayFunction || null,
                    customEditableFunction: item.customEditableFunction || null,
                    customFunctionForRowValueChanges: item.customFunctionForRowValueChanges || null
                },
                floatingFilterComponentParams: {
                    hideFilter: item.hideFilter || false,
                    serviceAccess: _this,
                    field: item.field,
                    customFilterFunction: item.customFilterFunction || null
                },
                customFieldForFilter: item.customFieldForFilter,
                customFieldForSort: item.customFieldForSort,
                getDataSourceAfterDataLoaded: item.getDataSourceAfterDataLoaded,
                columnType: item.columnType
            };
            if (item.field === _this.gridOptions.keyField) {
                column.suppressSizeToFit = true;
            }
            if (item.columnType === CoreGridColumnType.Date && (_this.gridOptions.editable || column['editable'])) {
                column['minWidth'] = 135;
            }
            if (item.minWidth) {
                column['minWidth'] = item.minWidth;
            }
            if (item.maxWidth) {
                column['maxWidth'] = item.maxWidth;
            }
            if (item.dataSource) {
                column.cellEditorParams.dataSource = item.dataSource;
            }
            if (item.settings) {
                column.cellEditorParams.settings = item.settings;
            }
            if ((item.columnType === CoreGridColumnType.Select || cellRenderer === CoreGridCellType.CoreSelectCell)
                && item.settings) {
                column.cellEditorParams.data = new Subject();
                if (!item.getDataSourceAfterDataLoaded) {
                    _this.getColumnDataSource(column.cellEditorParams.data, column);
                }
            }
            /** @type {?} */
            var index = _.findIndex(_this.columnDefs, ['field', item.field]);
            if (index < 0) {
                _this.columnDefs.push(column);
            }
            else {
                _this.columnDefs[index] = column;
            }
        });
        this.setFilterComponents();
        this.addCommandColumn();
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.reloadColumnDefs = /**
     * @return {?}
     */
    function () {
        this.createColumns();
        this.reloadGridColumnDefs();
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getColumnState = /**
     * @return {?}
     */
    function () {
        console.log(this.columnApi.getColumnState());
    };
    /**
     * @param {?} data
     * @param {?} column
     * @param {?=} extraFilters
     * @return {?}
     */
    CoreGridService.prototype.getColumnDataSource = /**
     * @param {?} data
     * @param {?} column
     * @param {?=} extraFilters
     * @return {?}
     */
    function (data, column, extraFilters) {
        var _this = this;
        if (this.loadOnInit || this.loadColumnDataSourceOnInit) {
            /** @type {?} */
            var settings = (/** @type {?} */ (column.cellEditorParams.settings));
            /** @type {?} */
            var requestOptions = void 0;
            if (settings) {
                if (column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.TextSelect) {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).requestOptions));
                }
                else {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).options.requestOptions));
                }
            }
            if (requestOptions) {
                /** @type {?} */
                var reqUrl_1 = requestOptions.UrlOptions.moduleUrl + requestOptions.UrlOptions.endPointUrl;
                /** @type {?} */
                var disableGeneralLoading = false;
                if (!this.gridOptions.disableSelfLoading) {
                    disableGeneralLoading = true;
                    this.loadingService.insertLoadingRequest(reqUrl_1);
                }
                /** @type {?} */
                var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                if (extraFilters && extraFilters.length > 0) {
                    if (requestOptions.CustomFilter && requestOptions.CustomFilter.FilterGroups) {
                        if (requestOptions.CustomFilter.FilterGroups.length === 0) {
                            requestOptions.CustomFilter.FilterGroups = [{
                                    Filters: []
                                }];
                        }
                        else if (requestOptions.CustomFilter.FilterGroups.length > 0) {
                            requestOptions.CustomFilter.FilterGroups.unshift({
                                Filters: []
                            });
                        }
                    }
                    else if (!requestOptions.CustomFilter) {
                        requestOptions.CustomFilter = {
                            FilterGroups: [{
                                    Filters: []
                                }]
                        };
                    }
                    else if (requestOptions.CustomFilter && !requestOptions.CustomFilter.FilterGroups) {
                        requestOptions.CustomFilter.FilterGroups = [{
                                Filters: []
                            }];
                    }
                    if (requestOptions.CustomFilter &&
                        requestOptions.CustomFilter.FilterGroups &&
                        requestOptions.CustomFilter.FilterGroups.length > 0) {
                        requestOptions.CustomFilter.FilterGroups[0].Filters = extraFilters;
                    }
                }
                this.dataService.getListBy(reqUrl_1, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                    .pipe(finalize(function () {
                    if (!_this.gridOptions.disableSelfLoading) {
                        _this.loadingService.removeLoadingRequest(reqUrl_1);
                    }
                }))
                    .subscribe(function (httpResponse) {
                    /** @type {?} */
                    var lovData = httpResponse.serviceResult.Result;
                    /** @type {?} */
                    var dataSource = lovData.map(function (x) { return Object.assign({}, x); });
                    /** @type {?} */
                    var colIndex = _.findIndex(_this.columnDefs, ['field', column.field]);
                    column.cellEditorParams.dataSource = dataSource;
                    _this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                    data.next({ data: true });
                });
            }
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getComponents = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var components = {};
        // Edit Cell
        components[CoreGridCellType.CoreBooleanEditCell] = BooleanEditCellComponent;
        components[CoreGridCellType.CoreDateEditCell] = DateEditCellComponent;
        components[CoreGridCellType.CoreLOVEditCell] = LovEditCellComponent;
        components[CoreGridCellType.CoreNumericEditCell] = NumericEditCellComponent;
        components[CoreGridCellType.CoreSelectEditCell] = SelectEditCellComponent;
        components[CoreGridCellType.CoreTextEditCell] = TextEditCellComponent;
        components[CoreGridCellType.CoreMaskEditCell] = MaskEditCellComponent;
        components[CoreGridCellType.CoreCurrencyEditCell] = CurrencyEditCellComponent;
        // Read-Only Cell
        components[CoreGridCellType.CoreBooleanCell] = BooleanCellComponent;
        components[CoreGridCellType.CoreLabelCell] = LabelCellComponent;
        components[CoreGridCellType.CoreDateCell] = DateCellComponent;
        components[CoreGridCellType.CoreSelectCell] = SelectCellComponent;
        components[CoreGridCellType.CoreCurrencyCell] = CurrencyCellComponent;
        components[CoreGridCellType.CoreMaskCell] = MaskCellComponent;
        components[CoreGridCellType.CoreNumericCell] = NumericCellComponent;
        // Filter Cell
        components[CoreGridCellType.CoreDateFilterCell] = DateFilterCellComponent;
        components[CoreGridCellType.CoreSelectFilterCell] = SelectFilterCellComponent;
        components[CoreGridCellType.CoreTextFilterCell] = TextFilterCellComponent;
        components[CoreGridCellType.CoreNumericFilterCell] = NumericFilterCellComponent;
        components[CoreGridCellType.CoreBooleanFilterCell] = BooleanFilterCellComponent;
        components[CoreGridCellType.CoreCurrencyFilterCell] = CurrencyFilterCellComponent;
        components[CoreGridCellType.CoreLOVFilterCell] = LovFilterCellComponent;
        components[CoreGridCellType.CoreMaskFilterCell] = MaskFilterCellComponent;
        // Command Cell
        components[CoreGridCellType.CoreCommandCell] = CommandCellComponent;
        components['agColumnHeader'] = ColumnHeaderComponent;
        components['customNoRowsOverlay'] = NoRowsOverlayComponent;
        components['customLoadingOverlay'] = LoadingOverlayComponent;
        return components;
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.setFilterComponents = /**
     * @return {?}
     */
    function () {
        this.columnDefs.forEach(function (item) {
            if (item.cellRenderer === CoreGridCellType.CoreBooleanEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLOVEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreLOVFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreTextEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreBooleanCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLabelCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell
                && item.columnType === CoreGridColumnType.TextSelect) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
        });
    };
    /**
     * @private
     * @param {?} genericExpression
     * @return {?}
     */
    CoreGridService.prototype.loadDataByFilterAndPageSetting = /**
     * @private
     * @param {?} genericExpression
     * @return {?}
     */
    function (genericExpression) {
        var _this = this;
        /** @type {?} */
        var reqUrl = "" + this.urlOptions.moduleUrl + this.urlOptions.endPointUrl;
        /** @type {?} */
        var headerParams = [];
        if (this.gridOptions && this.gridOptions.requestOptions && this.gridOptions.requestOptions.HeaderParameters) {
            headerParams = this.gridOptions.requestOptions.HeaderParameters;
        }
        if (!this.gridOptions.disableSelfLoading) {
            this.loadingService.insertLoadingRequest(reqUrl);
            /** @type {?} */
            var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            headerParams.push(disableLoadingHeader);
        }
        return this.dataService.getListBy(reqUrl, genericExpression, headerParams)
            .pipe(finalize(function () {
            if (!_this.gridOptions.disableSelfLoading) {
                _this.loadingService.removeLoadingRequest(reqUrl);
            }
        }));
    };
    /**
     * @private
     * @return {?}
     */
    CoreGridService.prototype.loadGridViewDataByOptionsUrlAndFilters = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.gridOptions) {
            /** @type {?} */
            var searchFilter_1 = _.cloneDeep(this.gridOptions.requestOptions.CustomFilter);
            if (this.pagingResult) {
                searchFilter_1.PageNumber = this.pagingResult.CurrentPage;
                searchFilter_1.PageSize = this.pagingResult.PageSize;
            }
            searchFilter_1.FilterGroups = searchFilter_1.FilterGroups || [];
            this.filter.forEach(function (element) {
                /** @type {?} */
                var name;
                /** @type {?} */
                var foundColumn = _this.gridOptions.columns.find(function (x) { return x.field === element.key; });
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (element.key.indexOf('Id') > 0 &&
                        (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = element.key.substr(0, element.key.indexOf('Id')) +
                            '.' + element.key.substr(element.key.indexOf('Id'), element.key.length);
                    }
                    else {
                        name = element.key;
                    }
                }
                /** @type {?} */
                var group = {
                    Filters: [{
                            PropertyName: name,
                            Value: element.value['filter'],
                            Comparison: element.value['type'],
                            OtherValue: element.value['other'] || null
                        }]
                };
                searchFilter_1.FilterGroups.push(group);
            });
            // it is for CoreGridColumnType.TextSelect
            this.filterGroupList.forEach(function (filterGroupItem) {
                searchFilter_1.FilterGroups.unshift(filterGroupItem.value);
            });
            // if (searchFilter.FilterGroups.length > 0) {
            //   searchFilter.FilterGroups[searchFilter.FilterGroups.length - 1]['Connector'] = Connector.Or;
            // }
            this.loadDataByFilterAndPageSetting(searchFilter_1)
                .subscribe(function (httpResponse) {
                _this.pagingResult = httpResponse.pagingResult;
                _this.data = httpResponse.serviceResult.Result;
                if (_this.data) {
                    _this.originalRowData = _this.data.map(function (item) { return Object.assign({}, item); });
                }
                _this.retrievedDataSourceSubject.next(_this.data);
                if (!_this.data || (_this.data && _this.data.length === 0)) {
                    _this.loadingOverlay();
                }
                _this.loadDataSourceAfterGetGridData();
            });
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.loadDataSourceAfterGetGridData = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var needToLoadDataSourceColumns = this.columnDefs.filter(function (x) { return x.getDataSourceAfterDataLoaded === true; });
        needToLoadDataSourceColumns.forEach(function (column) {
            /** @type {?} */
            var columnValues = _this.data.map(function (x) { return x[column.field]; });
            /** @type {?} */
            var newFilters = [];
            columnValues.forEach(function (columnValue) {
                /** @type {?} */
                var newFilter = {
                    PropertyName: column.cellEditorParams.settings.valueField,
                    Value: columnValue,
                    Comparison: Comparison.EqualTo,
                    Connector: Connector.Or
                };
                /** @type {?} */
                var name = column.cellEditorParams.settings.valueField;
                // can be changable
                if (name) {
                    if (column.cellEditorParams.settings.valueField.indexOf('Id') !== -1 &&
                        (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = name.substr(0, name.indexOf('Id')) +
                            '.' + name.substr(name.indexOf('Id'), name.length);
                    }
                }
                newFilter.PropertyName = name;
                newFilters.push(newFilter);
            });
            if (newFilters.length > 0) {
                newFilters[newFilters.length - 1].Connector = Connector.And;
            }
            if (newFilters.length > 0) {
                _this.getColumnDataSource(column.cellEditorParams.data, column, newFilters);
            }
        });
        // getColumnDataSource 
    };
    /**
     * @private
     * @return {?}
     */
    CoreGridService.prototype.addCommandColumn = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var showRowDeleteButton = true;
        /** @type {?} */
        var showRowEditButton = true;
        /** @type {?} */
        var buttonCounts = 1;
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showRowDeleteButton !== undefined) {
                showRowDeleteButton = this.gridOptions.buttonSettings.showRowDeleteButton;
                if (showRowDeleteButton) {
                    buttonCounts++;
                }
            }
            if (this.gridOptions.buttonSettings.showEditButton !== undefined) {
                showRowEditButton = this.gridOptions.buttonSettings.showEditButton;
                if (showRowEditButton) {
                    buttonCounts++;
                }
            }
        }
        // it is for delete and revert for inline grid
        // if (this.gridOptions.editable && buttonCounts === 1) {
        //   buttonCounts = 2;
        // }
        if (showRowDeleteButton || showRowEditButton) {
            /** @type {?} */
            var index = _.findIndex(this.columnDefs, ['field', 'CommandColumn']);
            if (index < 0) {
                /** @type {?} */
                var column = {
                    headerName: '',
                    field: 'CommandColumn',
                    editable: false,
                    cellRenderer: CoreGridCellType.CoreCommandCell,
                    filter: false,
                    sortable: false,
                    minWidth: buttonCounts * 50,
                    maxWidth: buttonCounts * 50
                };
                this.columnDefs.push(column);
            }
            // this.columnDefs.push();
        }
    };
    /**
     * @private
     * @return {?}
     */
    CoreGridService.prototype.createFormControls = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var columns = this.columnApi.getAllColumns();
        /** @type {?} */
        var gridFormGroup = ((/** @type {?} */ (this.gridForm)));
        // clear out old form group controls if switching between data sources
        /** @type {?} */
        var controlNames = Object.keys(gridFormGroup.controls);
        controlNames.forEach(function (controlName) {
            gridFormGroup.removeControl(controlName);
        });
        this.gridApi.forEachNode(function (rowNode) {
            /** @type {?} */
            var formArray = new FormArray([]);
            columns.filter(function (column) { return column.getColDef().field !== 'CommandColumn'; })
                .forEach(function (column) {
                /** @type {?} */
                var key = _this.createKey(_this.columnApi, column);
                // the cells will use this same createKey method
                /** @type {?} */
                var col = _.find(_this.gridOptions.columns, ['field', column.getColDef().field]);
                /** @type {?} */
                var validation = [];
                if (col) {
                    if (col.validation) {
                        if (col.validation.required) {
                            validation.push(Validators.required);
                        }
                        if (col.validation.email) {
                            validation.push(Validators.email);
                        }
                        if (col.validation.requiredTrue) {
                            validation.push(Validators.requiredTrue);
                        }
                        if (col.validation.minValue) {
                            validation.push(Validators.min(col.validation.minValue));
                        }
                        if (col.validation.maxValue) {
                            validation.push(Validators.max(col.validation.maxValue));
                        }
                        if (col.validation.minLength) {
                            validation.push(Validators.minLength(col.validation.minLength));
                        }
                        if (col.validation.maxLength) {
                            validation.push(Validators.maxLength(col.validation.maxLength));
                        }
                        if (col.validation.regex) {
                            validation.push(Validators.pattern(col.validation.regex));
                        }
                    }
                }
                formArray.setControl((/** @type {?} */ (key)), new FormControl('', { validators: validation }));
            });
            gridFormGroup.addControl((/** @type {?} */ (rowNode.id)), formArray);
        });
    };
    /**
     * @private
     * @return {?}
     */
    CoreGridService.prototype.reloadGridColumnDefs = /**
     * @private
     * @return {?}
     */
    function () {
        // this.columnDefs = columnDefs;
        if (this.gridApi) {
            this.gridApi.setColumnDefs(this.columnDefs);
            this.columnApi.resetColumnState();
        }
        this.refreshFormControls();
        if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    };
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    CoreGridService.prototype.setColumnDataSource = /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    function (columnName, dataSource) {
        /** @type {?} */
        var index = _.findIndex(this.gridOptions.columns, ['field', columnName]);
        this.gridOptions.columns[index].dataSource = dataSource;
        if (this.gridApi) {
            this.gridApi.refreshCells({ columns: [columnName], force: true });
        }
    };
    /**
     * @param {?} column
     * @return {?}
     */
    CoreGridService.prototype.setColumnLovOptions = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        var _this = this;
        if ((column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.Lov) && column.settings) {
            /** @type {?} */
            var settings_1 = (/** @type {?} */ (column.settings));
            /** @type {?} */
            var requestOptions = void 0;
            if (column.columnType === CoreGridColumnType.Select) {
                requestOptions = ((/** @type {?} */ (settings_1))).requestOptions;
            }
            else {
                requestOptions = ((/** @type {?} */ (settings_1))).options.requestOptions;
            }
            /** @type {?} */
            var reqUrl_2 = requestOptions.UrlOptions.moduleUrl + '/'
                + requestOptions.UrlOptions.endPointUrl;
            /** @type {?} */
            var disableGeneralLoading = false;
            if (!this.gridOptions.disableSelfLoading) {
                disableGeneralLoading = true;
                this.loadingService.insertLoadingRequest(reqUrl_2);
            }
            /** @type {?} */
            var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            this.dataService.getListBy(reqUrl_2, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                .pipe(finalize(function () {
                if (!_this.gridOptions.disableSelfLoading) {
                    _this.loadingService.removeLoadingRequest(reqUrl_2);
                }
            }))
                .subscribe(function (httpResponse) {
                /** @type {?} */
                var lovData = httpResponse.serviceResult.Result;
                /** @type {?} */
                var dataSource = lovData.map(function (x) { return Object.assign({}, x); });
                /** @type {?} */
                var colIndex = _.findIndex(_this.columnDefs, ['field', column.field]);
                column.dataSource = dataSource;
                _this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                _this.columnDefs[colIndex].cellEditorParams.settings = settings_1;
                if (_this.gridApi) {
                    _this.gridApi.refreshCells({ columns: [column.field], force: true });
                }
            });
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    CoreGridService.prototype.onGridReady = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        this.refreshFormControls();
        if (this.loadingDisplayStatus) {
            this.gridApi.showLoadingOverlay();
        }
        if (this.isLoad === false) {
            this.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
        }
        this.gridApi.sizeColumnsToFit();
    };
    /**
     * @param {?} params
     * @return {?}
     */
    CoreGridService.prototype.onColumnEverythingChanged = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.allGridColumns = params.columnApi.getAllColumns();
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.onGridSizeChanged = /**
     * @return {?}
     */
    function () {
        if (this.gridApi) {
            if (this.gridOptions && !this.gridOptions.disableSizeToFit && this.gridApi) {
                this.gridApi.sizeColumnsToFit();
            }
            this.gridApi.refreshCells({ force: true });
        }
    };
    /**
     * @private
     * @param {?} columnApi
     * @param {?} column
     * @return {?}
     */
    CoreGridService.prototype.createKey = /**
     * @private
     * @param {?} columnApi
     * @param {?} column
     * @return {?}
     */
    function (columnApi, column) {
        return columnApi.getAllColumns().indexOf(column);
    };
    /**
     * @param {?} self
     * @return {?}
     */
    CoreGridService.prototype.getContext = /**
     * @param {?} self
     * @return {?}
     */
    function (self) {
        return {
            componentParent: self,
            formGroup: this.gridForm,
            createKey: this.createKey
        };
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.refreshFormControls = /**
     * @return {?}
     */
    function () {
        if (this.gridApi) {
            this.createFormControls();
            this.gridApi.refreshCells({ force: true });
        }
    };
    /**
     * @param {?=} isCheckBulkData
     * @return {?}
     */
    CoreGridService.prototype.refreshGridData = /**
     * @param {?=} isCheckBulkData
     * @return {?}
     */
    function (isCheckBulkData) {
        var _this = this;
        /** @type {?} */
        var bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0 && isCheckBulkData) {
            this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Refresh)
                .subscribe(function (res) {
                if (res && res.value) {
                    _this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
                    _this.refreshGrid();
                    _this.toastr.success(ToastrMessages.GridRefreshed);
                }
            });
        }
        else {
            this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
            this.refreshGrid();
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.refreshGrid = /**
     * @return {?}
     */
    function () {
        this.deletedData = [];
        this.isDataChanged = false;
        this.loadGridViewDataByOptionsUrlAndFilters();
        if (this.isLoad === false) {
            this.init(this.gridOptions, true);
        }
    };
    /**
     * @param {?} item
     * @return {?}
     */
    CoreGridService.prototype.onCellValueChanged = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.isDataChanged = true;
        if (item.data.OperationType !== OperationType.Created) {
            item.data.OperationType = OperationType.Updated;
        }
    };
    /**
     * @param {?} sortModel
     * @return {?}
     */
    CoreGridService.prototype.onSortChanged = /**
     * @param {?} sortModel
     * @return {?}
     */
    function (sortModel) {
        if (sortModel) {
            // const sortModel = _.head(this.gridApi.getSortModel());
            this.currentSortedColId = sortModel.colId;
            /** @type {?} */
            var columnDef = (/** @type {?} */ (this.gridApi.getColumnDef(sortModel.colId)));
            /** @type {?} */
            var name_1 = columnDef.customFieldForSort;
            if (!name_1) {
                if (columnDef.field.indexOf('Id') > 0 &&
                    (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                    name_1 = columnDef.field.substr(0, columnDef.field.indexOf('Id'));
                }
                else {
                    name_1 = columnDef.field;
                }
            }
            /** @type {?} */
            var sort = [{
                    PropertyName: name_1,
                    Type: sortModel.sort === 'asc' ? SortType.Asc : SortType.Desc
                }];
            this.gridOptions.requestOptions.CustomFilter.Sort = sort;
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridService.prototype.onFilterChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        // this.filter = Object.entries(this.gridApi.getFilterModel()).map(([key, value]) => ({ key, value }));
        this.loadGridViewDataByOptionsUrlAndFilters();
    };
    /**
     * @param {?} field
     * @param {?} model
     * @return {?}
     */
    CoreGridService.prototype.applyChangesToFilter = /**
     * @param {?} field
     * @param {?} model
     * @return {?}
     */
    function (field, model) {
        this.addToListForFilter(this.filter, model.filter || model.filter === 0 ? model : null, field);
    };
    /**
     * @param {?} listArray
     * @param {?} value
     * @param {?} key
     * @return {?}
     */
    CoreGridService.prototype.addToListForFilter = /**
     * @param {?} listArray
     * @param {?} value
     * @param {?} key
     * @return {?}
     */
    function (listArray, value, key) {
        /** @type {?} */
        var foundDataIndex = listArray.findIndex(function (x) { return x.key === key; });
        if (value) {
            /** @type {?} */
            var dataToSend = {
                key: key,
                value: value
            };
            if (foundDataIndex !== -1) {
                listArray[foundDataIndex] = dataToSend;
            }
            else {
                listArray.push(dataToSend);
            }
        }
        else {
            if (foundDataIndex !== -1) {
                listArray.splice(foundDataIndex, 1);
            }
        }
    };
    // called from text filter for CoreGridColumnType.TextSelect
    // called from text filter for CoreGridColumnType.TextSelect
    /**
     * @param {?} field
     * @param {?} settings
     * @param {?} model
     * @return {?}
     */
    CoreGridService.prototype.textSelectFindValuesOnFilter = 
    // called from text filter for CoreGridColumnType.TextSelect
    /**
     * @param {?} field
     * @param {?} settings
     * @param {?} model
     * @return {?}
     */
    function (field, settings, model) {
        var _this = this;
        /** @type {?} */
        var foundColumn = this.gridOptions.columns.find(function (x) { return x.field === field; });
        if (model.filter) {
            /** @type {?} */
            var filter = {
                PageSize: 100,
                FilterGroups: [
                    {
                        Filters: [
                            {
                                PropertyName: (/** @type {?} */ (settings.labelField)),
                                Value: model.filter,
                                Comparison: model.type,
                                Connector: Connector.And
                            },
                        ]
                    }
                ]
            };
            this.dataService.getListBy(settings.requestOptions.UrlOptions.moduleUrl + settings.requestOptions.UrlOptions.endPointUrl, filter)
                .subscribe(function (httpResponse) {
                /** @type {?} */
                var result = httpResponse.serviceResult.Result || [];
                /** @type {?} */
                var idList = result.map(function (x) { return x[settings.valueField]; }).filter(_this.onlyUnique);
                /** @type {?} */
                var newFilters = [];
                /** @type {?} */
                var name = '';
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (field.indexOf('Id') > 0 &&
                        (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = field.substr(0, field.indexOf('Id')) +
                            '.' + field.substr(field.indexOf('Id'), field.length);
                    }
                    else {
                        name = field;
                    }
                }
                idList.forEach(function (id) {
                    newFilters.push({
                        PropertyName: name,
                        Value: id,
                        Comparison: Comparison.EqualTo,
                        Connector: Connector.Or
                    });
                });
                if (newFilters.length > 0) {
                    newFilters[newFilters.length - 1].Connector = Connector.And;
                }
                /** @type {?} */
                var filterGroup = {
                    Connector: Connector.And,
                    Filters: newFilters
                };
                if (newFilters.length > 0) {
                    _this.addToListForFilter(_this.filterGroupList, filterGroup, field);
                    _this.loadGridViewDataByOptionsUrlAndFilters();
                }
                else {
                    // gridi boşalmak lazım
                    _this.data = [];
                    _this.pagingResult.TotalCount = 0;
                }
            });
        }
        else {
            this.addToListForFilter(this.filterGroupList, null, field);
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    };
    /**
     * @param {?} value
     * @param {?} index
     * @param {?} self
     * @return {?}
     */
    CoreGridService.prototype.onlyUnique = /**
     * @param {?} value
     * @param {?} index
     * @param {?} self
     * @return {?}
     */
    function (value, index, self) {
        return self.indexOf(value) === index;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridService.prototype.onPageChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.pagingResult.CurrentPage = event.page;
        this.pagingResult.PageSize = event.itemsPerPage;
        this.loadGridViewDataByOptionsUrlAndFilters();
    };
    /**
     * @param {?} pageSizeEvent
     * @return {?}
     */
    CoreGridService.prototype.onChangeItemsPerPage = /**
     * @param {?} pageSizeEvent
     * @return {?}
     */
    function (pageSizeEvent) {
        if (pageSizeEvent.Id) {
            this.pagingResult.PageSize = pageSizeEvent.Id;
        }
        if (this.pagingResult.TotalCount > 0) {
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getBulkOperationData = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var resultData = [];
        if (this.gridOptions && this.gridOptions.editable) {
            this.validateForm();
            this.deletedData
                .forEach(function (obj) {
                resultData.push(obj);
            });
            this.data.forEach(function (obj) {
                if (obj.OperationType !== OperationType.None) {
                    resultData.push(obj);
                }
            });
        }
        return resultData;
    };
    /**
     * @param {?=} formGroup
     * @return {?}
     */
    CoreGridService.prototype.validateForm = /**
     * @param {?=} formGroup
     * @return {?}
     */
    function (formGroup) {
        var _this = this;
        if (formGroup === void 0) { formGroup = this.gridForm; }
        Object.keys(formGroup.controls).forEach(function (field) {
            /** @type {?} */
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.validateForm(control);
            }
            else if (control instanceof FormArray) {
                Object.keys(control.controls).forEach(function (c) {
                    /** @type {?} */
                    var ctrl = control.get(c);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
        });
        return formGroup.valid;
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.revertData = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe(function (res) {
            if (res && res.value) {
                _this.isDataChanged = false;
                _this.deletedData = [];
                _this.data = _this.originalRowData.map(function (item) { return Object.assign({}, item); });
                _this.gridApi.setRowData(_this.data);
                _this.toastr.success(ToastrMessages.RecordReverted);
                _this.recordEventsSubject.next({ type: 'RevertedGrid', record: _this.data });
            }
        });
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.addRow = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.gridOptions.editable) {
            /** @type {?} */
            var addedObject_1 = { Id: -1, OperationType: OperationType.Created };
            this.gridOptions.columns.filter(function (x) { return x.field !== 'Id'; }).forEach(function (element) {
                addedObject_1[element.field] = element.defaultValue;
            });
            this.data.unshift(addedObject_1);
            this.isDataChanged = true;
            // this.gridApi.updateRowData({
            //   add: [addedObject],
            //   addIndex: 0
            // });
            this.gridApi.setRowData(this.data);
            this.refreshFormControls();
            this.recordEventsSubject.next({ type: 'Add', record: addedObject_1 });
        }
        else {
            this.ngZone.run(function () {
                if (_this.gridOptions.addUrl) {
                    _this.router.navigate([_this.gridOptions.addUrl]);
                }
                else {
                    _this.router.navigate(['new'], { relativeTo: _this.route });
                }
            });
        }
    };
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    CoreGridService.prototype.editRecord = /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    function (params, rowId, key) {
        var _this = this;
        if (this.gridOptions.editUrlBlank) {
            if (this.gridOptions.editUrl) {
                window.open(window.location.origin + '/' + this.gridOptions.editUrl + '/' + params.data[this.gridOptions.keyField]);
            }
            else {
                window.open(this.router.url + '/' + 'edit/' + params.data[this.gridOptions.keyField]);
            }
        }
        else {
            this.ngZone.run(function () {
                if (_this.gridOptions.editUrl) {
                    _this.router.navigate([_this.gridOptions.editUrl, params.data[_this.gridOptions.keyField]]);
                }
                else {
                    _this.router.navigate(['edit', params.data[_this.gridOptions.keyField]], { relativeTo: _this.route });
                }
                // this.handleBreadcrumbLabel(params.data);
            });
        }
    };
    // handleBreadcrumbLabel(rowData: any): void {
    //   if (this.gridOptions &&
    //     this.gridOptions.breadcrumbLabel &&
    //     rowData &&
    //     this.gridOptions.breadcrumbLabel &&
    //     this.gridOptions.keyField &&
    //     rowData[this.gridOptions.keyField]) {
    //     let editUrl: string;
    //     const keyFieldValue = rowData[this.gridOptions.keyField];
    //     if (this.gridOptions.editUrl) {
    //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
    //     } else {
    //       editUrl = this.router.url + '/edit/' + keyFieldValue;
    //     }
    //     let customLabel = '';
    //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
    //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
    //       if (foundColumn && foundColumn.colDef) {
    //         if (foundColumn.colDef.cellEditorParams &&
    //           foundColumn.colDef.cellEditorParams.dataSource &&
    //           foundColumn.colDef.cellEditorParams.settings) {
    //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
    //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
    //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
    //           customLabel += foundData[labelField] + ' ';
    //         } else {
    //           customLabel += rowData[columnField] + ' ';
    //         }
    //       }
    //     });
    //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
    //   }
    // }
    // handleBreadcrumbLabel(rowData: any): void {
    //   if (this.gridOptions &&
    //     this.gridOptions.breadcrumbLabel &&
    //     rowData &&
    //     this.gridOptions.breadcrumbLabel &&
    //     this.gridOptions.keyField &&
    //     rowData[this.gridOptions.keyField]) {
    //     let editUrl: string;
    //     const keyFieldValue = rowData[this.gridOptions.keyField];
    //     if (this.gridOptions.editUrl) {
    //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
    //     } else {
    //       editUrl = this.router.url + '/edit/' + keyFieldValue;
    //     }
    //     let customLabel = '';
    //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
    //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
    //       if (foundColumn && foundColumn.colDef) {
    //         if (foundColumn.colDef.cellEditorParams &&
    //           foundColumn.colDef.cellEditorParams.dataSource &&
    //           foundColumn.colDef.cellEditorParams.settings) {
    //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
    //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
    //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
    //           customLabel += foundData[labelField] + ' ';
    //         } else {
    //           customLabel += rowData[columnField] + ' ';
    //         }
    //       }
    //     });
    //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
    //   }
    // }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    CoreGridService.prototype.undoRecord = 
    // handleBreadcrumbLabel(rowData: any): void {
    //   if (this.gridOptions &&
    //     this.gridOptions.breadcrumbLabel &&
    //     rowData &&
    //     this.gridOptions.breadcrumbLabel &&
    //     this.gridOptions.keyField &&
    //     rowData[this.gridOptions.keyField]) {
    //     let editUrl: string;
    //     const keyFieldValue = rowData[this.gridOptions.keyField];
    //     if (this.gridOptions.editUrl) {
    //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
    //     } else {
    //       editUrl = this.router.url + '/edit/' + keyFieldValue;
    //     }
    //     let customLabel = '';
    //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
    //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
    //       if (foundColumn && foundColumn.colDef) {
    //         if (foundColumn.colDef.cellEditorParams &&
    //           foundColumn.colDef.cellEditorParams.dataSource &&
    //           foundColumn.colDef.cellEditorParams.settings) {
    //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
    //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
    //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
    //           customLabel += foundData[labelField] + ' ';
    //         } else {
    //           customLabel += rowData[columnField] + ' ';
    //         }
    //       }
    //     });
    //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
    //   }
    // }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    function (params, rowId, key) {
        var _this = this;
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe(function (result) {
            if (result.value) {
                /** @type {?} */
                var revertedObject_1 = _this.data[rowId];
                /** @type {?} */
                var orginalObject = _this.originalRowData
                    .find(function (x) { return revertedObject_1 && x[_this.gridOptions.keyField] === revertedObject_1[_this.gridOptions.keyField]; });
                _this.data[rowId] = orginalObject ? JSON.parse(JSON.stringify(orginalObject)) : null;
                params.node.data = _this.data[rowId];
                _this.gridApi.redrawRows({ rowNodes: [params.node] });
                _this.gridApi.refreshCells({ rowNodes: [params.node], force: true });
                params.context.formGroup.controls[rowId].markAsPristine();
            }
        });
    };
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    CoreGridService.prototype.deleteRecord = /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    function (params, rowId, key) {
        var _this = this;
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe(function (result) {
            if (result.value) {
                /** @type {?} */
                var deleteUrl = _this.gridOptions.deleteEndPointUrl ? _this.gridOptions.deleteEndPointUrl : _this.urlOptions.endPointUrl;
                /** @type {?} */
                var reqUrl = "" + _this.urlOptions.moduleUrl + deleteUrl;
                _this.dataService.delete(_this.data[rowId][_this.gridOptions.keyField], reqUrl)
                    .subscribe(function (res) {
                    _this.recordEventsSubject.next({ type: 'Delete', record: _this.data[rowId] });
                    // const indexOfData = this.data.indexOf(params.data);
                    if (_this.pagingResult.TotalCount > 10) {
                        _this.refreshAndRevertChanges();
                    }
                    else {
                        _this.data.splice(rowId, 1);
                        if (_this.pagingResult.TotalCount && _this.pagingResult.TotalCount > 0) {
                            _this.pagingResult.TotalCount--;
                        }
                        _this.gridApi.setRowData(_this.data);
                    }
                    _this.toastr.success(ToastrMessages.RecordDeleted);
                });
            }
            else {
                _this.sweetAlertService.confirmOperationPopup(result);
            }
        });
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.callUndoConfirmAlert = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0) {
            return this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo);
        }
        else {
            return of({ value: true });
        }
    };
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    CoreGridService.prototype.removeRow = /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    function (params, rowId, key) {
        var _this = this;
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe(function (res) {
            if (res && res.value) {
                // this.data[rowId] = Object.assign({}, this.originalRowData[rowId]);
                /** @type {?} */
                var deletedObject_1 = _this.data[rowId];
                _this.data.splice(rowId, 1);
                _this.recordEventsSubject.next({ type: 'Delete', record: deletedObject_1 });
                if (params.data.OperationType !== OperationType.Created) {
                    /** @type {?} */
                    var orginalDeletedObject = _this.originalRowData
                        .find(function (x) { return x[_this.gridOptions.keyField] === deletedObject_1[_this.gridOptions.keyField]; });
                    if (orginalDeletedObject) {
                        orginalDeletedObject['OperationType'] = OperationType.Deleted;
                        _this.deletedData.push(orginalDeletedObject);
                    }
                    else {
                        deletedObject_1['OperationType'] = OperationType.Deleted;
                        _this.deletedData.push(deletedObject_1);
                    }
                }
                _this.isDataChanged = true;
                _this.gridApi.setRowData(_this.data);
                // if (!this.gridOptions.editable) {
                //   this.toastr.success(ToastrMessages.RecordDeleted);
                // }
            }
        });
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.refreshAndRevertChanges = /**
     * @return {?}
     */
    function () {
        this.deletedData = [];
        this.isDataChanged = false;
        this.refreshGridData();
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.isChanged = /**
     * @return {?}
     */
    function () {
        return this.isDataChanged;
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getTotalCount = /**
     * @return {?}
     */
    function () {
        return this.pagingResult.TotalCount - this.deletedData.length;
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getNoRowsOverlayComponent = /**
     * @return {?}
     */
    function () {
        return 'customNoRowsOverlay';
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getLoadingOverlayComponent = /**
     * @return {?}
     */
    function () {
        return 'customLoadingOverlay';
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.getSelectedRows = /**
     * @return {?}
     */
    function () {
        return this.gridApi.getSelectedRows();
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.loadingOverlay = /**
     * @return {?}
     */
    function () {
        if (this.gridApi) {
            if (this.loadingDisplayStatus) {
                this.gridApi.showLoadingOverlay();
            }
            else {
                this.gridApi.hideOverlay();
                if (!this.data || (this.data && this.data.length === 0)) {
                    this.gridApi.showNoRowsOverlay();
                }
            }
        }
    };
    /**
     * @return {?}
     */
    CoreGridService.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.loadingDisplayStatusSubscription) {
            this.loadingDisplayStatusSubscription.unsubscribe();
        }
    };
    CoreGridService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CoreGridService.ctorParameters = function () { return [
        { type: DataService },
        { type: ToastrUtilsService },
        { type: GridLoadingService },
        { type: SweetAlertService },
        { type: NgZone },
        { type: Router },
        { type: ActivatedRoute }
    ]; };
    /** @nocollapse */ CoreGridService.ngInjectableDef = i0.defineInjectable({ factory: function CoreGridService_Factory() { return new CoreGridService(i0.inject(i1.DataService), i0.inject(i2.ToastrUtilsService), i0.inject(i3.GridLoadingService), i0.inject(i4.SweetAlertService), i0.inject(i0.NgZone), i0.inject(i5.Router), i0.inject(i5.ActivatedRoute)); }, token: CoreGridService, providedIn: "root" });
    return CoreGridService;
}());
export { CoreGridService };
if (false) {
    /** @type {?} */
    CoreGridService.prototype.gridOptions;
    /** @type {?} */
    CoreGridService.prototype.pagingResult;
    /** @type {?} */
    CoreGridService.prototype.data;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.gridApi;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.columnApi;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.urlOptions;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.originalRowData;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.deletedData;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.filter;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.filterGroupList;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.gridForm;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadOnInit;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadColumnDataSourceOnInit;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.isDataChanged;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.isLoad;
    /** @type {?} */
    CoreGridService.prototype.columnDefs;
    /** @type {?} */
    CoreGridService.prototype.allGridColumns;
    /** @type {?} */
    CoreGridService.prototype.currentSortedColId;
    /** @type {?} */
    CoreGridService.prototype.loadingDisplayStatusSubscription;
    /** @type {?} */
    CoreGridService.prototype.loadingDisplayStatus;
    /** @type {?} */
    CoreGridService.prototype.recordEventsSubject;
    /** @type {?} */
    CoreGridService.prototype.retrievedDataSourceSubject;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.dataService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.toastr;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.loadingService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.sweetAlertService;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.ngZone;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    CoreGridService.prototype.route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvc2VydmljZXMvY29yZS1ncmlkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWEsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBZSxNQUFNLGdCQUFnQixDQUFDO0FBRTVGLE9BQU8sRUFBZ0IsT0FBTyxFQUFjLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7O0FBSzVCLE9BQU8sRUFDTCxhQUFhLEVBQ2IsZ0JBQWdCLEVBQ2hCLFFBQVEsRUFDUixrQkFBa0IsRUFDbEIsMEJBQTBCLEVBQzFCLGNBQWMsRUFDZCxPQUFPLEVBQ1AsVUFBVSxFQUNWLFNBQVMsRUFDVixNQUFNLHNCQUFzQixDQUFDOztBQXNCOUIsT0FBTyxFQUNMLFdBQVcsRUFDWCxrQkFBa0IsRUFDbEIsaUJBQWlCLEVBQ2xCLE1BQU0seUJBQXlCLENBQUM7O0FBR2pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTlFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdFQUFnRSxDQUFDO0FBQzNHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3JHLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBQzlHLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBQzlHLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLG9FQUFvRSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBRXJHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQzdGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBRTdGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ25HLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDREQUE0RCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDhEQUE4RCxDQUFDO0FBR3pHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOzs7Ozs7O0FBRzVEO0lBZ0NFLHlCQUNVLFdBQXdCLEVBQ3hCLE1BQTBCLEVBQzFCLGNBQWtDLEVBQ2xDLGlCQUFvQyxFQUNwQyxNQUFjLEVBQ2QsTUFBYyxFQUVkLEtBQXFCO1FBUi9CLGlCQWFDO1FBWlMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBb0I7UUFDMUIsbUJBQWMsR0FBZCxjQUFjLENBQW9CO1FBQ2xDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFdBQU0sR0FBTixNQUFNLENBQVE7UUFFZCxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQWxDeEIsaUJBQVksR0FBaUIsRUFBRSxDQUFDO1FBQ2hDLFNBQUksR0FBUSxFQUFFLENBQUM7UUFLZCxvQkFBZSxHQUFRLEVBQUUsQ0FBQztRQUMxQixnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUN0QixXQUFNLEdBQVEsRUFBRSxDQUFDO1FBQ2pCLG9CQUFlLEdBQVUsRUFBRSxDQUFDO1FBQzVCLGFBQVEsR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM3QixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBRWxCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDaEIsZUFBVSxHQUFVLEVBQUUsQ0FBQztRQUN2QixtQkFBYyxHQUFVLEVBQUUsQ0FBQztRQU1sQyx3QkFBbUIsR0FBaUIsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQUN2RCwrQkFBMEIsR0FBbUIsSUFBSSxPQUFPLEVBQVMsQ0FBQztRQVloRSxJQUFJLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDdEcsS0FBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztZQUNqQyxLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7O0lBRUQsOEJBQUk7Ozs7OztJQUFKLFVBQUssT0FBb0IsRUFBRSxVQUFtQixFQUFFLDBCQUFvQztRQUNsRixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsMEJBQTBCLEdBQUcsMEJBQTBCLENBQUM7UUFDN0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUM7UUFDM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUc7Z0JBQ2xCLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFFBQVEsRUFBRSxPQUFPLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxRQUFRO2FBQ3ZELENBQUM7WUFFRixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUV6QixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO2dCQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRU0sMkNBQWlCOzs7SUFBeEI7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsU0FBUztZQUMvRCxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVc7U0FDcEUsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRU8sdUNBQWE7Ozs7SUFBckI7UUFBQSxpQkFzS0M7UUFyS0MsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEVBQUU7O2dCQUMxRCxZQUFZLEdBQUc7Z0JBQ25CLFVBQVUsRUFBRSxRQUFRO2dCQUNwQix1QkFBdUIsRUFBRSxJQUFJO2dCQUM3QixtQ0FBbUMsRUFBRSxJQUFJO2dCQUN6QyxpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsRUFBRTtnQkFDWixTQUFTLEVBQUUsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFO2dCQUNuQyxZQUFZLEVBQUUsS0FBSztnQkFDbkIsY0FBYyxFQUFFLEtBQUs7Z0JBQ3JCLFdBQVcsRUFBRSxLQUFLO2dCQUNsQixjQUFjLEVBQUUsSUFBSTtnQkFDcEIsUUFBUSxFQUFFLEVBQUU7YUFDYjtZQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3BDO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs7Z0JBQy9CLFlBQThCOztnQkFDOUIsYUFBYSxHQUFZLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN0RCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO2dCQUMvQixhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUMvQjtZQUVELElBQUksYUFBYSxFQUFFO2dCQUNqQixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsS0FBSyxFQUFFO29CQUNoRCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsYUFBYSxDQUFDO2lCQUMvQztxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsSUFBSSxFQUFFO29CQUN0RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUM7aUJBQ2xEO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUU7b0JBQ3RELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDbEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtvQkFDeEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2lCQUNwRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsT0FBTyxFQUFFO29CQUN6RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUM7aUJBQ3JEO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7b0JBQ3pELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQztpQkFDckQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEdBQUcsRUFBRTtvQkFDckQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDO2lCQUNsRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsUUFBUSxFQUFFO29CQUMxRCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUM7aUJBQ3REO3FCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUU7b0JBQzlELFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUM7aUJBQ2pEO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEtBQUssRUFBRTtvQkFDaEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQztpQkFDL0M7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQztpQkFDL0M7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQztpQkFDOUM7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtvQkFDeEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztpQkFDaEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtvQkFDekQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtvQkFDekQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztpQkFDakQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLEdBQUcsRUFBRTtvQkFDckQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztpQkFDaEQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksRUFBRTtvQkFDdEQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLFlBQVksQ0FBQztpQkFDOUM7cUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLFFBQVEsRUFBRTtvQkFDMUQsWUFBWSxHQUFHLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDO2lCQUNsRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsWUFBWSxFQUFFO29CQUM5RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsZUFBZSxDQUFDO2lCQUNqRDtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsVUFBVSxFQUFFO29CQUM1RCxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsY0FBYyxDQUFDO2lCQUNoRDthQUNGO1lBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7O2dCQUVHLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSztZQUMzQixJQUFJLEtBQUksQ0FBQyxXQUFXO21CQUNmLEtBQUksQ0FBQyxXQUFXLENBQUMsY0FBYzttQkFDL0IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVTttQkFDMUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRTs7b0JBQ3JELFdBQVcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVztnQkFDMUUsSUFBSSxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUNuQyxVQUFVLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNyRDtxQkFBTTtvQkFDTCxVQUFVLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7aUJBQ3ZDO2FBQ0Y7O2dCQUVLLE1BQU0sR0FBRztnQkFDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxJQUFJLFVBQVU7Z0JBQ3pDLFlBQVksY0FBQTtnQkFDWixJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTztnQkFDbkIsaUJBQWlCLEVBQUUsSUFBSTtnQkFDdkIsTUFBTSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxJQUFJLEtBQUs7Z0JBQzlDLFFBQVEsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsSUFBSSxLQUFLO2dCQUNqRCxTQUFTLEVBQUUsSUFBSTtnQkFDZixnQkFBZ0IsRUFBRTtvQkFDaEIsVUFBVSxFQUFFLElBQUk7b0JBQ2hCLFFBQVEsRUFBRSxJQUFJO29CQUNkLElBQUksRUFBRSxJQUFJO29CQUNWLHFCQUFxQixFQUFFLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJO29CQUN6RCxzQkFBc0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSTtvQkFDM0QsZ0NBQWdDLEVBQUUsSUFBSSxDQUFDLGdDQUFnQyxJQUFJLElBQUk7aUJBQ2hGO2dCQUNELDZCQUE2QixFQUFFO29CQUM3QixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLO29CQUNwQyxhQUFhLEVBQUUsS0FBSTtvQkFDbkIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO29CQUNqQixvQkFBb0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSTtpQkFDeEQ7Z0JBQ0Qsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLG9CQUFvQjtnQkFDL0Msa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtnQkFDM0MsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLDRCQUE0QjtnQkFDL0QsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO2FBQzVCO1lBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFO2dCQUM1QyxNQUFNLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFO2dCQUNwRyxNQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQzFCO1lBR0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNqQixNQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNwQztZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDcEM7WUFHRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ25CLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUN0RDtZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQ2xEO1lBRUQsSUFDRSxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsTUFBTSxJQUFJLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7bUJBQ2hHLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2xCLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztnQkFFbEQsSUFBSSxDQUFDLElBQUksQ0FBQyw0QkFBNEIsRUFBRTtvQkFDdEMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7aUJBQ2hFO2FBQ0Y7O2dCQUVLLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pFLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtnQkFDYixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLE1BQU0sQ0FBQzthQUNqQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELDBDQUFnQjs7O0lBQWhCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7Ozs7SUFFRCx3Q0FBYzs7O0lBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7Ozs7O0lBRUQsNkNBQW1COzs7Ozs7SUFBbkIsVUFBb0IsSUFBa0IsRUFBRSxNQUFXLEVBQUUsWUFBdUI7UUFBNUUsaUJBd0VDO1FBdkVDLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7O2dCQUNoRCxRQUFRLEdBQUcsbUJBQUEsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBa0M7O2dCQUMvRSxjQUFjLFNBQWdCO1lBQ2xDLElBQUksUUFBUSxFQUFFO2dCQUNaLElBQUksTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUU7b0JBQzFHLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxtQkFBQSxRQUFRLEVBQW9CLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2lCQUM1RjtxQkFBTTtvQkFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsbUJBQUEsUUFBUSxFQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztpQkFDL0Y7YUFDRjtZQUVELElBQUksY0FBYyxFQUFFOztvQkFDWixRQUFNLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXOztvQkFFdEYscUJBQXFCLEdBQUcsS0FBSztnQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7b0JBQ3hDLHFCQUFxQixHQUFHLElBQUksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFNLENBQUMsQ0FBQztpQkFDbEQ7O29CQUNLLG9CQUFvQixHQUFvQixFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFO2dCQUUvRyxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDM0MsSUFBSSxjQUFjLENBQUMsWUFBWSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFO3dCQUMzRSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7NEJBQ3pELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxHQUFHLENBQUM7b0NBQzFDLE9BQU8sRUFBRSxFQUFFO2lDQUNaLENBQUMsQ0FBQzt5QkFFSjs2QkFBTSxJQUFJLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQzlELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQztnQ0FDL0MsT0FBTyxFQUFFLEVBQUU7NkJBQ1osQ0FBQyxDQUFDO3lCQUVKO3FCQUNGO3lCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFO3dCQUN2QyxjQUFjLENBQUMsWUFBWSxHQUFHOzRCQUM1QixZQUFZLEVBQUUsQ0FBQztvQ0FDYixPQUFPLEVBQUUsRUFBRTtpQ0FDWixDQUFDO3lCQUNILENBQUM7cUJBRUg7eUJBQU0sSUFBSSxjQUFjLENBQUMsWUFBWSxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUU7d0JBQ25GLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxHQUFHLENBQUM7Z0NBQzFDLE9BQU8sRUFBRSxFQUFFOzZCQUNaLENBQUMsQ0FBQztxQkFDSjtvQkFFRCxJQUFJLGNBQWMsQ0FBQyxZQUFZO3dCQUM3QixjQUFjLENBQUMsWUFBWSxDQUFDLFlBQVk7d0JBQ3hDLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3JELGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUM7cUJBQ3BFO2lCQUNGO2dCQUVELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFFBQU0sRUFBRSxjQUFjLENBQUMsWUFBWSxFQUFFLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztxQkFDbkgsSUFBSSxDQUNILFFBQVEsQ0FBQztvQkFDUCxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRTt3QkFDeEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFNLENBQUMsQ0FBQztxQkFDbEQ7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0osU0FBUyxDQUFDLFVBQUMsWUFBbUM7O3dCQUN2QyxPQUFPLEdBQVUsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNOzt3QkFDbEQsVUFBVSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQzs7d0JBQ25ELFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0RSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztvQkFDaEQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO29CQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQzVCLENBQUMsQ0FBQyxDQUFDO2FBQ047U0FDRjtJQUNILENBQUM7Ozs7SUFFRCx1Q0FBYTs7O0lBQWI7O1lBQ1EsVUFBVSxHQUFHLEVBQUU7UUFDckIsWUFBWTtRQUNaLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLHdCQUF3QixDQUFDO1FBQzVFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBQ3RFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsR0FBRyxvQkFBb0IsQ0FBQztRQUNwRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsR0FBRyx3QkFBd0IsQ0FBQztRQUM1RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsR0FBRyx1QkFBdUIsQ0FBQztRQUMxRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsR0FBRyx5QkFBeUIsQ0FBQztRQUM5RSxpQkFBaUI7UUFDakIsVUFBVSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxHQUFHLG9CQUFvQixDQUFDO1FBQ3BFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsR0FBRyxrQkFBa0IsQ0FBQztRQUNoRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEdBQUcsaUJBQWlCLENBQUM7UUFDOUQsVUFBVSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxHQUFHLG1CQUFtQixDQUFDO1FBQ2xFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBQ3RFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsR0FBRyxpQkFBaUIsQ0FBQztRQUM5RCxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLEdBQUcsb0JBQW9CLENBQUM7UUFDcEUsY0FBYztRQUNkLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLHlCQUF5QixDQUFDO1FBQzlFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO1FBQ2hGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO1FBQ2hGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLDJCQUEyQixDQUFDO1FBQ2xGLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLHNCQUFzQixDQUFDO1FBQ3hFLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLHVCQUF1QixDQUFDO1FBQzFFLGVBQWU7UUFDZixVQUFVLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLEdBQUcsb0JBQW9CLENBQUM7UUFFcEUsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEdBQUcscUJBQXFCLENBQUM7UUFDckQsVUFBVSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsc0JBQXNCLENBQUM7UUFDM0QsVUFBVSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsdUJBQXVCLENBQUM7UUFFN0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQzs7OztJQUVELDZDQUFtQjs7O0lBQW5CO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBRTFCLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDOUQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDO2FBQ3ZFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDbEUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2FBQ3BFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUU7Z0JBQzlELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQzthQUNwRTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsZUFBZSxFQUFFO2dCQUNqRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUM7YUFDbkU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFO2dCQUNyRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMscUJBQXFCLENBQUM7YUFDdkU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFO2dCQUNwRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUM7YUFDdEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO2dCQUNsRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7YUFDcEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGVBQWUsRUFBRTtnQkFDakUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDO2FBQ3ZFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUU7Z0JBQy9ELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQzthQUNwRTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2xFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQzthQUNwRTtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssZ0JBQWdCLENBQUMsWUFBWSxFQUFFO2dCQUM5RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7YUFDcEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLG9CQUFvQixFQUFFO2dCQUN0RSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsc0JBQXNCLENBQUM7YUFDeEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO2dCQUNsRSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsc0JBQXNCLENBQUM7YUFDeEU7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLGdCQUFnQixDQUFDLGVBQWUsRUFBRTtnQkFDakUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDO2FBQ3ZFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxjQUFjO21CQUMzRCxJQUFJLENBQUMsVUFBVSxLQUFLLGtCQUFrQixDQUFDLFVBQVUsRUFBRTtnQkFDdEQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO2FBQ3BFO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQzthQUN0RTtRQUVILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sd0RBQThCOzs7OztJQUF0QyxVQUF1QyxpQkFBb0M7UUFBM0UsaUJBc0JDOztZQXJCTyxNQUFNLEdBQUcsS0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQWE7O1lBRXZFLFlBQVksR0FBc0IsRUFBRTtRQUN4QyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7WUFDM0csWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDO1NBQ2pFO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Z0JBRTNDLG9CQUFvQixHQUFvQixFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsY0FBYyxFQUFFO1lBQy9HLFlBQVksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztTQUN6QztRQUVELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLGlCQUFpQixFQUFFLFlBQVksQ0FBQzthQUN2RSxJQUFJLENBQ0gsUUFBUSxDQUFDO1lBQ1AsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3hDLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDbEQ7UUFDSCxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFFTyxnRUFBc0M7Ozs7SUFBOUM7UUFBQSxpQkErREM7UUE5REMsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOztnQkFDZCxjQUFZLEdBQXNCLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ2pHLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDckIsY0FBWSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQztnQkFDeEQsY0FBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQzthQUNwRDtZQUVELGNBQVksQ0FBQyxZQUFZLEdBQUcsY0FBWSxDQUFDLFlBQVksSUFBSSxFQUFFLENBQUM7WUFDNUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPOztvQkFDckIsSUFBWTs7b0JBQ1YsV0FBVyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxDQUFDLEdBQUcsRUFBdkIsQ0FBdUIsQ0FBQztnQkFDL0UsSUFBSSxXQUFXLEVBQUU7b0JBQ2YsSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDekM7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDVCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQy9CLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLEtBQUssSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLEVBQUU7d0JBQzVILElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3JELEdBQUcsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUMzRTt5QkFBTTt3QkFDTCxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQztxQkFDcEI7aUJBQ0Y7O29CQUVLLEtBQUssR0FBZ0I7b0JBQ3pCLE9BQU8sRUFBRSxDQUFDOzRCQUNSLFlBQVksRUFBRSxJQUFJOzRCQUNsQixLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7NEJBQzlCLFVBQVUsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzs0QkFDakMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSTt5QkFDM0MsQ0FBQztpQkFDSDtnQkFFRCxjQUFZLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUVILDBDQUEwQztZQUMxQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFDLGVBQW9CO2dCQUNoRCxjQUFZLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0QsQ0FBQyxDQUFDLENBQUM7WUFFSCw4Q0FBOEM7WUFDOUMsaUdBQWlHO1lBQ2pHLElBQUk7WUFFSixJQUFJLENBQUMsOEJBQThCLENBQUMsY0FBWSxDQUFDO2lCQUM5QyxTQUFTLENBQUMsVUFBQyxZQUFtQztnQkFDN0MsS0FBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO2dCQUM5QyxLQUFJLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO2dCQUM5QyxJQUFJLEtBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ2IsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUF2QixDQUF1QixDQUFDLENBQUM7aUJBQ3ZFO2dCQUNELEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVoRCxJQUFJLENBQUMsS0FBSSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxJQUFJLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZELEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDdkI7Z0JBRUQsS0FBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7WUFFeEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7Ozs7SUFFRCx3REFBOEI7OztJQUE5QjtRQUFBLGlCQXNDQzs7WUFyQ08sMkJBQTJCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsNEJBQTRCLEtBQUssSUFBSSxFQUF2QyxDQUF1QyxDQUFDO1FBQ3hHLDJCQUEyQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07O2dCQUNsQyxZQUFZLEdBQVUsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFmLENBQWUsQ0FBQzs7Z0JBQ3pELFVBQVUsR0FBYSxFQUFFO1lBRS9CLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxXQUFnQjs7b0JBQzlCLFNBQVMsR0FBVztvQkFDeEIsWUFBWSxFQUFFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsVUFBVTtvQkFDekQsS0FBSyxFQUFFLFdBQVc7b0JBQ2xCLFVBQVUsRUFBRSxVQUFVLENBQUMsT0FBTztvQkFDOUIsU0FBUyxFQUFFLFNBQVMsQ0FBQyxFQUFFO2lCQUN4Qjs7b0JBRUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsVUFBVTtnQkFFdEQsbUJBQW1CO2dCQUNuQixJQUFJLElBQUksRUFBRTtvQkFDUixJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ2xFLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLEtBQUssSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLEVBQUU7d0JBQzVILElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUN2QyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDdEQ7aUJBQ0Y7Z0JBRUQsU0FBUyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBRTlCLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN6QixVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQzthQUM3RDtZQUNELElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQzthQUM1RTtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsdUJBQXVCO0lBQ3pCLENBQUM7Ozs7O0lBRU8sMENBQWdCOzs7O0lBQXhCOztZQUNNLG1CQUFtQixHQUFHLElBQUk7O1lBQzFCLGlCQUFpQixHQUFHLElBQUk7O1lBQ3hCLFlBQVksR0FBRyxDQUFDO1FBRXBCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUN2RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLG1CQUFtQixLQUFLLFNBQVMsRUFBRTtnQkFDckUsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUM7Z0JBQzFFLElBQUksbUJBQW1CLEVBQUU7b0JBQ3ZCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1lBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO2dCQUNoRSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7Z0JBQ25FLElBQUksaUJBQWlCLEVBQUU7b0JBQ3JCLFlBQVksRUFBRSxDQUFDO2lCQUNoQjthQUNGO1NBQ0Y7UUFFRCw4Q0FBOEM7UUFDOUMseURBQXlEO1FBQ3pELHNCQUFzQjtRQUN0QixJQUFJO1FBRUosSUFBSSxtQkFBbUIsSUFBSSxpQkFBaUIsRUFBRTs7Z0JBQ3RDLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdEUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFOztvQkFDUCxNQUFNLEdBQUc7b0JBQ2IsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsS0FBSyxFQUFFLGVBQWU7b0JBQ3RCLFFBQVEsRUFBRSxLQUFLO29CQUNmLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlO29CQUM5QyxNQUFNLEVBQUUsS0FBSztvQkFDYixRQUFRLEVBQUUsS0FBSztvQkFDZixRQUFRLEVBQUUsWUFBWSxHQUFHLEVBQUU7b0JBQzNCLFFBQVEsRUFBRSxZQUFZLEdBQUcsRUFBRTtpQkFDNUI7Z0JBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUI7WUFDRCwwQkFBMEI7U0FDM0I7SUFDSCxDQUFDOzs7OztJQUVPLDRDQUFrQjs7OztJQUExQjtRQUFBLGlCQW1EQzs7WUFsRE8sT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFOztZQUV4QyxhQUFhLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLENBQUMsUUFBUSxFQUFhLENBQUM7OztZQUU1QyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQ3hELFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxXQUFXO1lBQy9CLGFBQWEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxVQUFDLE9BQWdCOztnQkFFbEMsU0FBUyxHQUFjLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQztZQUU5QyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUMsTUFBYyxJQUFLLE9BQUEsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssS0FBSyxlQUFlLEVBQTVDLENBQTRDLENBQUM7aUJBQzdFLE9BQU8sQ0FBQyxVQUFDLE1BQWM7O29CQUNoQixHQUFHLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQzs7O29CQUM1QyxHQUFHLEdBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7O29CQUN2RixVQUFVLEdBQWtCLEVBQUU7Z0JBQ3BDLElBQUksR0FBRyxFQUFFO29CQUNQLElBQUksR0FBRyxDQUFDLFVBQVUsRUFBRTt3QkFDbEIsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRTs0QkFDM0IsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ3RDO3dCQUNELElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUU7NEJBQ3hCLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUNuQzt3QkFDRCxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFOzRCQUMvQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQzt5QkFDMUM7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRTs0QkFDM0IsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt5QkFDMUQ7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRTs0QkFDM0IsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt5QkFDMUQ7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTs0QkFDNUIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt5QkFDakU7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTs0QkFDNUIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt5QkFDakU7d0JBQ0QsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRTs0QkFDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt5QkFDM0Q7cUJBQ0Y7aUJBQ0Y7Z0JBQ0QsU0FBUyxDQUFDLFVBQVUsQ0FBQyxtQkFBQSxHQUFHLEVBQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLENBQUMsQ0FBQyxDQUFDO1lBQ0wsYUFBYSxDQUFDLFVBQVUsQ0FBQyxtQkFBQSxPQUFPLENBQUMsRUFBRSxFQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDekQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVPLDhDQUFvQjs7OztJQUE1QjtRQUNFLGdDQUFnQztRQUNoQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUNuQztRQUNELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7Ozs7SUFFRCw2Q0FBbUI7Ozs7O0lBQW5CLFVBQW9CLFVBQWtCLEVBQUUsVUFBZTs7WUFDL0MsS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUN4RCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUNuRTtJQUNILENBQUM7Ozs7O0lBRUQsNkNBQW1COzs7O0lBQW5CLFVBQW9CLE1BQWtCO1FBQXRDLGlCQXFDQztRQXBDQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsS0FBSyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFOztnQkFDbEgsVUFBUSxHQUFHLG1CQUFBLE1BQU0sQ0FBQyxRQUFRLEVBQWtDOztnQkFDOUQsY0FBYyxTQUFnQjtZQUNsQyxJQUFJLE1BQU0sQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsTUFBTSxFQUFFO2dCQUNuRCxjQUFjLEdBQUcsQ0FBQyxtQkFBQSxVQUFRLEVBQW9CLENBQUMsQ0FBQyxjQUFjLENBQUM7YUFDaEU7aUJBQU07Z0JBQ0wsY0FBYyxHQUFHLENBQUMsbUJBQUEsVUFBUSxFQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDO2FBQ25FOztnQkFDSyxRQUFNLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsR0FBRztrQkFDcEQsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXOztnQkFFckMscUJBQXFCLEdBQUcsS0FBSztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLFFBQU0sQ0FBQyxDQUFDO2FBQ2xEOztnQkFDSyxvQkFBb0IsR0FBb0IsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLGNBQWMsRUFBRTtZQUMvRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFNLEVBQUUsY0FBYyxDQUFDLFlBQVksRUFBRSxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ25ILElBQUksQ0FDSCxRQUFRLENBQUM7Z0JBQ1AsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7b0JBQ3hDLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsUUFBTSxDQUFDLENBQUM7aUJBQ2xEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0osU0FBUyxDQUFDLFVBQUMsWUFBbUM7O29CQUN2QyxPQUFPLEdBQVUsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNOztvQkFDbEQsVUFBVSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQzs7b0JBQ25ELFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0RSxNQUFNLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDL0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUNuRSxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxVQUFRLENBQUM7Z0JBQy9ELElBQUksS0FBSSxDQUFDLE9BQU8sRUFBRTtvQkFDaEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3JFO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7Ozs7O0lBRUQscUNBQVc7Ozs7SUFBWCxVQUFZLE1BQU07UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUVsQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUMzQixJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDbkM7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1NBQy9FO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsbURBQXlCOzs7O0lBQXpCLFVBQTBCLE1BQU07UUFDOUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCwyQ0FBaUI7OztJQUFqQjtRQUNFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQzFFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUNqQztZQUNELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7U0FDNUM7SUFDSCxDQUFDOzs7Ozs7O0lBRU8sbUNBQVM7Ozs7OztJQUFqQixVQUFrQixTQUFvQixFQUFFLE1BQWM7UUFDcEQsT0FBTyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25ELENBQUM7Ozs7O0lBRUQsb0NBQVU7Ozs7SUFBVixVQUFXLElBQVM7UUFDbEIsT0FBTztZQUNMLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN4QixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDMUIsQ0FBQztJQUNKLENBQUM7Ozs7SUFFRCw2Q0FBbUI7OztJQUFuQjtRQUNFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCx5Q0FBZTs7OztJQUFmLFVBQWdCLGVBQXlCO1FBQXpDLGlCQWVDOztZQWRPLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7UUFDNUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksZUFBZSxFQUFFO1lBQ3RELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUM7aUJBQ3pGLFNBQVMsQ0FBQyxVQUFDLEdBQWdCO2dCQUMxQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsS0FBSyxFQUFFO29CQUNwQixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDdkUsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNuQixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7aUJBQ25EO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDdkUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDO1FBQzlDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ25DO0lBQ0gsQ0FBQzs7Ozs7SUFFRCw0Q0FBa0I7Ozs7SUFBbEIsVUFBbUIsSUFBUztRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxLQUFLLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQztTQUNqRDtJQUNILENBQUM7Ozs7O0lBRUQsdUNBQWE7Ozs7SUFBYixVQUFjLFNBQWM7UUFDMUIsSUFBSSxTQUFTLEVBQUU7WUFDYix5REFBeUQ7WUFFekQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7O2dCQUVwQyxTQUFTLEdBQUcsbUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFjOztnQkFDdEUsTUFBSSxHQUFXLFNBQVMsQ0FBQyxrQkFBa0I7WUFFL0MsSUFBSSxDQUFDLE1BQUksRUFBRTtnQkFDVCxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7b0JBQ25DLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLEVBQUU7b0JBQzVILE1BQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDakU7cUJBQU07b0JBQ0wsTUFBSSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7aUJBQ3hCO2FBQ0Y7O2dCQUVLLElBQUksR0FBRyxDQUFDO29CQUNaLFlBQVksRUFBRSxNQUFJO29CQUNsQixJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJO2lCQUM5RCxDQUFDO1lBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFFekQsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7U0FDL0M7SUFDSCxDQUFDOzs7OztJQUVELHlDQUFlOzs7O0lBQWYsVUFBZ0IsS0FBVTtRQUN4Qix1R0FBdUc7UUFDdkcsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7SUFDaEQsQ0FBQzs7Ozs7O0lBRUQsOENBQW9COzs7OztJQUFwQixVQUFxQixLQUFhLEVBQUUsS0FBVTtRQUM1QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDakMsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQ2pELEtBQUssQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7Ozs7OztJQUVELDRDQUFrQjs7Ozs7O0lBQWxCLFVBQW1CLFNBQWdCLEVBQUUsS0FBVSxFQUFFLEdBQVE7O1lBQ2pELGNBQWMsR0FBVyxTQUFTLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHLEVBQWIsQ0FBYSxDQUFDO1FBQ3RFLElBQUksS0FBSyxFQUFFOztnQkFDSCxVQUFVLEdBQVE7Z0JBQ3RCLEdBQUcsS0FBQTtnQkFDSCxLQUFLLE9BQUE7YUFDTjtZQUNELElBQUksY0FBYyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUN6QixTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsVUFBVSxDQUFDO2FBQ3hDO2lCQUFNO2dCQUNMLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDNUI7U0FDRjthQUFNO1lBQ0wsSUFBSSxjQUFjLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQ3pCLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3JDO1NBQ0Y7SUFDSCxDQUFDO0lBR0QsNERBQTREOzs7Ozs7OztJQUM1RCxzREFBNEI7Ozs7Ozs7O0lBQTVCLFVBQTZCLEtBQWEsRUFBRSxRQUEwQixFQUFFLEtBQVU7UUFBbEYsaUJBMEVDOztZQXpFTyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLEVBQWpCLENBQWlCLENBQUM7UUFFekUsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFOztnQkFDVixNQUFNLEdBQXNCO2dCQUNoQyxRQUFRLEVBQUUsR0FBRztnQkFDYixZQUFZLEVBQUU7b0JBQ1o7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLFlBQVksRUFBRSxtQkFBQSxRQUFRLENBQUMsVUFBVSxFQUFVO2dDQUMzQyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU07Z0NBQ25CLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSTtnQ0FDdEIsU0FBUyxFQUFFLFNBQVMsQ0FBQyxHQUFHOzZCQUN6Qjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFDdEgsTUFBTSxDQUFDO2lCQUNOLFNBQVMsQ0FBQyxVQUFDLFlBQXFDOztvQkFDekMsTUFBTSxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLEVBQUU7O29CQUNoRCxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQXRCLENBQXNCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQzs7b0JBQ3hFLFVBQVUsR0FBYSxFQUFFOztvQkFFM0IsSUFBSSxHQUFHLEVBQUU7Z0JBQ2IsSUFBSSxXQUFXLEVBQUU7b0JBQ2YsSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDekM7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDVCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzt3QkFDekIsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssS0FBSyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsRUFBRTt3QkFDNUgsSUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3pDLEdBQUcsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN6RDt5QkFBTTt3QkFDTCxJQUFJLEdBQUcsS0FBSyxDQUFDO3FCQUNkO2lCQUNGO2dCQUVELE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxFQUFVO29CQUN4QixVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixLQUFLLEVBQUUsRUFBRTt3QkFDVCxVQUFVLEVBQUUsVUFBVSxDQUFDLE9BQU87d0JBQzlCLFNBQVMsRUFBRSxTQUFTLENBQUMsRUFBRTtxQkFDeEIsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3pCLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDO2lCQUM3RDs7b0JBR0ssV0FBVyxHQUFnQjtvQkFDL0IsU0FBUyxFQUFFLFNBQVMsQ0FBQyxHQUFHO29CQUN4QixPQUFPLEVBQUUsVUFBVTtpQkFDcEI7Z0JBRUQsSUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDekIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxlQUFlLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNsRSxLQUFJLENBQUMsc0NBQXNDLEVBQUUsQ0FBQztpQkFDL0M7cUJBQU07b0JBQ0wsdUJBQXVCO29CQUN2QixLQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7aUJBQ2xDO1lBRUgsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDO1NBQy9DO0lBQ0gsQ0FBQzs7Ozs7OztJQUVELG9DQUFVOzs7Ozs7SUFBVixVQUFXLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSTtRQUMzQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRUQsdUNBQWE7Ozs7SUFBYixVQUFjLEtBQVU7UUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDO1FBQ2hELElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsOENBQW9COzs7O0lBQXBCLFVBQXFCLGFBQWtCO1FBQ3JDLElBQUksYUFBYSxDQUFDLEVBQUUsRUFBRTtZQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUMsRUFBRSxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsR0FBRyxDQUFDLEVBQUU7WUFDcEMsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLENBQUM7U0FDL0M7SUFDSCxDQUFDOzs7O0lBRUQsOENBQW9COzs7SUFBcEI7O1lBQ1EsVUFBVSxHQUFHLEVBQUU7UUFDckIsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFO1lBQ2pELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUVwQixJQUFJLENBQUMsV0FBVztpQkFDYixPQUFPLENBQUMsVUFBQyxHQUFHO2dCQUNYLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7WUFFTCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7Z0JBQ25CLElBQUksR0FBRyxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsSUFBSSxFQUFFO29CQUM1QyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN0QjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDOzs7OztJQUVELHNDQUFZOzs7O0lBQVosVUFBYSxTQUFvQztRQUFqRCxpQkFlQztRQWZZLDBCQUFBLEVBQUEsWUFBdUIsSUFBSSxDQUFDLFFBQVE7UUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSzs7Z0JBQ3JDLE9BQU8sR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUNwQyxJQUFJLE9BQU8sWUFBWSxXQUFXLEVBQUU7Z0JBQ2xDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUMzQztpQkFBTSxJQUFJLE9BQU8sWUFBWSxTQUFTLEVBQUU7Z0JBQ3ZDLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDNUI7aUJBQU0sSUFBSSxPQUFPLFlBQVksU0FBUyxFQUFFO2dCQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDOzt3QkFDL0IsSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sU0FBUyxDQUFDLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsb0NBQVU7OztJQUFWO1FBQUEsaUJBWUM7UUFYQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDO2FBQ3RGLFNBQVMsQ0FBQyxVQUFDLEdBQWdCO1lBQzFCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7Z0JBQ3BCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixLQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUF2QixDQUF1QixDQUFDLENBQUM7Z0JBQ3RFLEtBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNuRCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsS0FBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7YUFDNUU7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxnQ0FBTTs7O0lBQU47UUFBQSxpQkE4QkM7UUE3QkMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTs7Z0JBQ3ZCLGFBQVcsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxhQUFhLEVBQUUsYUFBYSxDQUFDLE9BQU8sRUFBRTtZQUNwRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87Z0JBQ3BFLGFBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUNwRCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQVcsQ0FBQyxDQUFDO1lBRS9CLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBRTFCLCtCQUErQjtZQUMvQix3QkFBd0I7WUFDeEIsZ0JBQWdCO1lBQ2hCLE1BQU07WUFFTixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbkMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFFM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGFBQVcsRUFBRSxDQUFDLENBQUM7U0FDckU7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNkLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7b0JBQzNCLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUNqRDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2lCQUMzRDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7O0lBRUQsb0NBQVU7Ozs7OztJQUFWLFVBQVcsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBQTdCLGlCQWtCQztRQWpCQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQ2pDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7Z0JBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUNySDtpQkFBTTtnQkFDTCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDdkY7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ2QsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtvQkFDNUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxRjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztpQkFDcEc7Z0JBRUQsMkNBQTJDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQsOENBQThDO0lBQzlDLDRCQUE0QjtJQUM1QiwwQ0FBMEM7SUFDMUMsaUJBQWlCO0lBQ2pCLDBDQUEwQztJQUMxQyxtQ0FBbUM7SUFDbkMsNENBQTRDO0lBRTVDLDJCQUEyQjtJQUMzQixnRUFBZ0U7SUFFaEUsc0NBQXNDO0lBQ3RDLGtFQUFrRTtJQUNsRSxlQUFlO0lBQ2YsOERBQThEO0lBQzlELFFBQVE7SUFHUiw0QkFBNEI7SUFFNUIsMEVBQTBFO0lBQzFFLG9GQUFvRjtJQUVwRixpREFBaUQ7SUFFakQscURBQXFEO0lBQ3JELDhEQUE4RDtJQUM5RCw0REFBNEQ7SUFDNUQsd0ZBQXdGO0lBQ3hGLHdGQUF3RjtJQUV4RixnSUFBZ0k7SUFDaEksd0RBQXdEO0lBRXhELG1CQUFtQjtJQUNuQix1REFBdUQ7SUFDdkQsWUFBWTtJQUNaLFVBQVU7SUFDVixVQUFVO0lBRVYsNkZBQTZGO0lBQzdGLE1BQU07SUFDTixJQUFJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBRUosb0NBQVU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFBVixVQUFXLE1BQU0sRUFBRSxLQUFLLEVBQUUsR0FBRztRQUE3QixpQkFlQztRQWRDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUM7YUFDdEYsU0FBUyxDQUFDLFVBQUMsTUFBVztZQUNyQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7O29CQUNWLGdCQUFjLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7O29CQUNqQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGVBQWU7cUJBQ3ZDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLGdCQUFjLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssZ0JBQWMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUE1RixDQUE0RixDQUFDO2dCQUUxRyxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDcEYsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNyRCxLQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDcEUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQzNEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7O0lBR0Qsc0NBQVk7Ozs7OztJQUFaLFVBQWEsTUFBTSxFQUFFLEtBQUssRUFBRSxHQUFHO1FBQS9CLGlCQTZCQztRQTVCQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUNBQWlDLENBQUMsMEJBQTBCLENBQUMsTUFBTSxDQUFDO2FBQ3hGLFNBQVMsQ0FBQyxVQUFDLE1BQVc7WUFDckIsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFOztvQkFDVixTQUFTLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXOztvQkFDakgsTUFBTSxHQUFHLEtBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsU0FBVztnQkFDekQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUFFLE1BQU0sQ0FBQztxQkFDekUsU0FBUyxDQUFDLFVBQUMsR0FBUTtvQkFDbEIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUU1RSxzREFBc0Q7b0JBRXRELElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsRUFBRSxFQUFFO3dCQUNyQyxLQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztxQkFDaEM7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUMzQixJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBRTs0QkFDcEUsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsQ0FBQzt5QkFDaEM7d0JBQ0QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNwQztvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBRXBELENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3REO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsOENBQW9COzs7SUFBcEI7O1lBQ1EsUUFBUSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtRQUM1QyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsRzthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7Ozs7SUFFRCxtQ0FBUzs7Ozs7O0lBQVQsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUc7UUFBNUIsaUJBK0JDO1FBOUJDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLENBQUM7YUFDeEYsU0FBUyxDQUFDLFVBQUMsR0FBZ0I7WUFDMUIsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRTs7O29CQUVkLGVBQWEsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDdEMsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUUzQixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsZUFBYSxFQUFFLENBQUMsQ0FBQztnQkFFekUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsT0FBTyxFQUFFOzt3QkFDakQsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGVBQWU7eUJBQzlDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLGVBQWEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUF6RSxDQUF5RSxDQUFDO29CQUV2RixJQUFJLG9CQUFvQixFQUFFO3dCQUN4QixvQkFBb0IsQ0FBQyxlQUFlLENBQUMsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDO3dCQUM5RCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO3FCQUM3Qzt5QkFBTTt3QkFDTCxlQUFhLENBQUMsZUFBZSxDQUFDLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQzt3QkFDdkQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBYSxDQUFDLENBQUM7cUJBQ3RDO2lCQUNGO2dCQUVELEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2dCQUMxQixLQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5DLG9DQUFvQztnQkFDcEMsdURBQXVEO2dCQUN2RCxJQUFJO2FBQ0w7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxpREFBdUI7OztJQUF2QjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsbUNBQVM7OztJQUFUO1FBQ0UsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCx1Q0FBYTs7O0lBQWI7UUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO0lBQ2hFLENBQUM7Ozs7SUFFRCxtREFBeUI7OztJQUF6QjtRQUNFLE9BQU8scUJBQXFCLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELG9EQUEwQjs7O0lBQTFCO1FBQ0UsT0FBTyxzQkFBc0IsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQseUNBQWU7OztJQUFmO1FBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFRCx3Q0FBYzs7O0lBQWQ7UUFDRSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUNuQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztpQkFDbEM7YUFDRjtTQUNGO0lBRUgsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLGdDQUFnQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyRDtJQUNILENBQUM7O2dCQXRyQ0YsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkEzQ0MsV0FBVztnQkFDWCxrQkFBa0I7Z0JBcUNYLGtCQUFrQjtnQkFwQ3pCLGlCQUFpQjtnQkEvQ2EsTUFBTTtnQkFDN0IsTUFBTTtnQkFBRSxjQUFjOzs7MEJBRC9CO0NBNndDQyxBQXZyQ0QsSUF1ckNDO1NBbnJDWSxlQUFlOzs7SUFDMUIsc0NBQWdDOztJQUNoQyx1Q0FBdUM7O0lBQ3ZDLCtCQUFzQjs7Ozs7SUFFdEIsa0NBQXlCOzs7OztJQUN6QixvQ0FBNkI7Ozs7O0lBQzdCLHFDQUErQjs7Ozs7SUFDL0IsMENBQWtDOzs7OztJQUNsQyxzQ0FBOEI7Ozs7O0lBQzlCLGlDQUF5Qjs7Ozs7SUFDekIsMENBQW9DOzs7OztJQUNwQyxtQ0FBcUM7Ozs7O0lBQ3JDLHFDQUEwQjs7Ozs7SUFDMUIscURBQTRDOzs7OztJQUM1Qyx3Q0FBOEI7Ozs7O0lBQzlCLGlDQUF1Qjs7SUFDdkIscUNBQThCOztJQUM5Qix5Q0FBa0M7O0lBQ2xDLDZDQUFrQzs7SUFFbEMsMkRBQStDOztJQUMvQywrQ0FBOEI7O0lBRTlCLDhDQUF1RDs7SUFDdkQscURBQWtFOzs7OztJQUloRSxzQ0FBZ0M7Ozs7O0lBQ2hDLGlDQUFrQzs7Ozs7SUFDbEMseUNBQTBDOzs7OztJQUMxQyw0Q0FBNEM7Ozs7O0lBQzVDLGlDQUFzQjs7Ozs7SUFDdEIsaUNBQXNCOzs7OztJQUV0QixnQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBPbkRlc3Ryb3ksIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQXJyYXksIEZvcm1Db250cm9sLCBWYWxpZGF0b3JzLCBWYWxpZGF0b3JGbiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiwgU3ViamVjdCwgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZmluYWxpemUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgeyBHcmlkQXBpLCBDb2x1bW5BcGksIENvbHVtbiwgUm93Tm9kZSB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuXHJcbi8vIEVudW1zXHJcbmltcG9ydCB7XHJcbiAgT3BlcmF0aW9uVHlwZSxcclxuICBDb3JlR3JpZENlbGxUeXBlLFxyXG4gIFNvcnRUeXBlLFxyXG4gIENvcmVHcmlkQ29sdW1uVHlwZSxcclxuICBDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZSxcclxuICBUb2FzdHJNZXNzYWdlcyxcclxuICBIZWFkZXJzLFxyXG4gIENvbXBhcmlzb24sXHJcbiAgQ29ubmVjdG9yXHJcbn0gZnJvbSAnLi4vLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuXHJcbi8vIE1vZGVscyBcclxuaW1wb3J0IHtcclxuICBHcmlkT3B0aW9ucyxcclxuICBQYWdpbmdSZXN1bHQsXHJcbiAgVXJsT3B0aW9ucyxcclxuICBHZW5lcmljRXhwcmVzc2lvbixcclxuICBTb3J0LFxyXG4gIEdyaWRDb2x1bW4sXHJcbiAgU2VsZWN0b3JTZXR0aW5ncyxcclxuICBMb3ZTZXR0aW5ncyxcclxuICBSZXF1ZXN0T3B0aW9ucyxcclxuICBGaWx0ZXJHcm91cCxcclxuICBBbGVydFJlc3VsdCxcclxuICBDb3JlSHR0cFJlc3BvbnNlLFxyXG4gIEhlYWRlclBhcmFtZXRlcixcclxuICBGaWx0ZXJcclxufSBmcm9tICcuLi8uLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5cclxuLy8gU2VydmljZXNcclxuaW1wb3J0IHtcclxuICBEYXRhU2VydmljZSxcclxuICBUb2FzdHJVdGlsc1NlcnZpY2UsXHJcbiAgU3dlZXRBbGVydFNlcnZpY2VcclxufSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9pbmRleCc7XHJcblxyXG4vLyBjb21wb25lbnRzXHJcbmltcG9ydCB7IENvbW1hbmRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9jb21tYW5kL2NvbW1hbmQtY2VsbC5jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IHsgVGV4dEZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci90ZXh0LWZpbHRlci1jZWxsL3RleHQtZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2VsZWN0RmlsdGVyQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZmlsdGVyL3NlbGVjdC1maWx0ZXItY2VsbC9zZWxlY3QtZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGF0ZUZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9kYXRlLWZpbHRlci1jZWxsL2RhdGUtZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTnVtZXJpY0ZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9udW1lcmljLWZpbHRlci1jZWxsL251bWVyaWMtZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQm9vbGVhbkZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9ib29sZWFuLWZpbHRlci1jZWxsL2Jvb2xlYW4tZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ3VycmVuY3lGaWx0ZXJDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9maWx0ZXIvY3VycmVuY3ktZmlsdGVyLWNlbGwvY3VycmVuY3ktZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG92RmlsdGVyQ2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZmlsdGVyL2xvdi1maWx0ZXItY2VsbC9sb3YtZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWFza0ZpbHRlckNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2ZpbHRlci9tYXNrLWZpbHRlci1jZWxsL21hc2stZmlsdGVyLWNlbGwuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IFNlbGVjdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL3JlYWQtb25seS9zZWxlY3QtY2VsbC9zZWxlY3QtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJlbENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL3JlYWQtb25seS9sYWJlbC1jZWxsL2xhYmVsLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGF0ZUNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL3JlYWQtb25seS9kYXRlLWNlbGwvZGF0ZS1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEJvb2xlYW5DZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvYm9vbGVhbi1jZWxsL2Jvb2xlYW4tY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDdXJyZW5jeUNlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL3JlYWQtb25seS9jdXJyZW5jeS1jZWxsL2N1cnJlbmN5LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWFza0NlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL3JlYWQtb25seS9tYXNrLWNlbGwvbWFzay1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE51bWVyaWNDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9yZWFkLW9ubHkvbnVtZXJpYy1jZWxsL251bWVyaWMtY2VsbC5jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IHsgVGV4dEVkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L3RleHQtZWRpdC1jZWxsL3RleHQtZWRpdC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlbGVjdEVkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L3NlbGVjdC1lZGl0LWNlbGwvc2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOdW1lcmljRWRpdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2VkaXQvbnVtZXJpYy1lZGl0LWNlbGwvbnVtZXJpYy1lZGl0LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG92RWRpdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2VkaXQvbG92LWVkaXQtY2VsbC9sb3YtZWRpdC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVFZGl0Q2VsbENvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwvZWRpdC9kYXRlLWVkaXQtY2VsbC9kYXRlLWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBCb29sZWFuRWRpdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2VkaXQvYm9vbGVhbi1lZGl0LWNlbGwvYm9vbGVhbi1lZGl0LWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWFza0VkaXRDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC9lZGl0L21hc2stZWRpdC1jZWxsL21hc2stZWRpdC1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEN1cnJlbmN5RWRpdENlbGxDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsL2VkaXQvY3VycmVuY3ktZWRpdC1jZWxsL2N1cnJlbmN5LWVkaXQtY2VsbC5jb21wb25lbnQnO1xyXG5cclxuXHJcbmltcG9ydCB7IENvbHVtbkhlYWRlckNvbXBvbmVudCB9IGZyb20gJy4uL2NvbHVtbi1oZWFkZXIvY29sdW1uLWhlYWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOb1Jvd3NPdmVybGF5Q29tcG9uZW50IH0gZnJvbSAnLi4vb3ZlcmxheS9uby1yb3dzL25vLXJvd3Mtb3ZlcmxheS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2FkaW5nT3ZlcmxheUNvbXBvbmVudCB9IGZyb20gJy4uL292ZXJsYXkvbG9hZGluZy9sb2FkaW5nLW92ZXJsYXkuY29tcG9uZW50JztcclxuaW1wb3J0IHsgR3JpZExvYWRpbmdTZXJ2aWNlIH0gZnJvbSAnLi9ncmlkLWxvYWRpbmcuc2VydmljZSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENvcmVHcmlkU2VydmljZSBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XHJcbiAgcHVibGljIGdyaWRPcHRpb25zOiBHcmlkT3B0aW9ucztcclxuICBwdWJsaWMgcGFnaW5nUmVzdWx0OiBQYWdpbmdSZXN1bHQgPSB7fTtcclxuICBwdWJsaWMgZGF0YTogYW55ID0gW107XHJcblxyXG4gIHByaXZhdGUgZ3JpZEFwaTogR3JpZEFwaTtcclxuICBwcml2YXRlIGNvbHVtbkFwaTogQ29sdW1uQXBpO1xyXG4gIHByaXZhdGUgdXJsT3B0aW9uczogVXJsT3B0aW9ucztcclxuICBwcml2YXRlIG9yaWdpbmFsUm93RGF0YTogYW55ID0gW107XHJcbiAgcHJpdmF0ZSBkZWxldGVkRGF0YTogYW55ID0gW107XHJcbiAgcHJpdmF0ZSBmaWx0ZXI6IGFueSA9IFtdO1xyXG4gIHByaXZhdGUgZmlsdGVyR3JvdXBMaXN0OiBhbnlbXSA9IFtdO1xyXG4gIHByaXZhdGUgZ3JpZEZvcm0gPSBuZXcgRm9ybUdyb3VwKHt9KTtcclxuICBwcml2YXRlIGxvYWRPbkluaXQgPSB0cnVlO1xyXG4gIHByaXZhdGUgbG9hZENvbHVtbkRhdGFTb3VyY2VPbkluaXQ6IGJvb2xlYW47XHJcbiAgcHJpdmF0ZSBpc0RhdGFDaGFuZ2VkID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSBpc0xvYWQgPSBmYWxzZTtcclxuICBwdWJsaWMgY29sdW1uRGVmczogYW55W10gPSBbXTtcclxuICBwdWJsaWMgYWxsR3JpZENvbHVtbnM6IGFueVtdID0gW107XHJcbiAgcHVibGljIGN1cnJlbnRTb3J0ZWRDb2xJZDogc3RyaW5nO1xyXG5cclxuICBsb2FkaW5nRGlzcGxheVN0YXR1c1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIGxvYWRpbmdEaXNwbGF5U3RhdHVzOiBib29sZWFuO1xyXG5cclxuICByZWNvcmRFdmVudHNTdWJqZWN0OiBTdWJqZWN0PGFueT4gPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcbiAgcmV0cmlldmVkRGF0YVNvdXJjZVN1YmplY3Q6IFN1YmplY3Q8YW55W10+ID0gbmV3IFN1YmplY3Q8YW55W10+KCk7XHJcblxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZGF0YVNlcnZpY2U6IERhdGFTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSB0b2FzdHI6IFRvYXN0clV0aWxzU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGluZ1NlcnZpY2U6IEdyaWRMb2FkaW5nU2VydmljZSxcclxuICAgIHByaXZhdGUgc3dlZXRBbGVydFNlcnZpY2U6IFN3ZWV0QWxlcnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAvLyBwcml2YXRlIGJyZWFkY3J1bWJTZXJ2aWNlOiBCcmVhZGNydW1iU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgICB0aGlzLmxvYWRpbmdEaXNwbGF5U3RhdHVzU3Vic2NyaXB0aW9uID0gdGhpcy5sb2FkaW5nU2VydmljZS5nZXRMb2FkaW5nQmFyRGlzcGxheVN0YXR1cygpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRpbmdEaXNwbGF5U3RhdHVzID0gZGF0YTtcclxuICAgICAgdGhpcy5sb2FkaW5nT3ZlcmxheSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBpbml0KG9wdGlvbnM6IEdyaWRPcHRpb25zLCBsb2FkT25Jbml0OiBib29sZWFuLCBsb2FkQ29sdW1uRGF0YVNvdXJjZU9uSW5pdD86IGJvb2xlYW4pIHtcclxuICAgIHRoaXMubG9hZE9uSW5pdCA9IGxvYWRPbkluaXQ7XHJcbiAgICB0aGlzLmxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0ID0gbG9hZENvbHVtbkRhdGFTb3VyY2VPbkluaXQ7XHJcbiAgICB0aGlzLmdyaWRPcHRpb25zID0gb3B0aW9ucztcclxuICAgIGlmICh0aGlzLmdyaWRBcGkpIHtcclxuICAgICAgdGhpcy5wYWdpbmdSZXN1bHQgPSB7XHJcbiAgICAgICAgQ3VycmVudFBhZ2U6IDEsXHJcbiAgICAgICAgUGFnZVNpemU6IG9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLlBhZ2VTaXplXHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLnJlbG9hZENvbHVtbkRlZnMoKTtcclxuICAgICAgdGhpcy5zZXRMb2FkVXJsU2V0dGluZygpO1xyXG5cclxuICAgICAgaWYgKHRoaXMubG9hZE9uSW5pdCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgIHRoaXMuaXNMb2FkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnJlZnJlc2hHcmlkRGF0YSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0TG9hZFVybFNldHRpbmcoKSB7XHJcbiAgICB0aGlzLnVybE9wdGlvbnMgPSB7XHJcbiAgICAgIG1vZHVsZVVybDogdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLm1vZHVsZVVybCxcclxuICAgICAgZW5kUG9pbnRVcmw6IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5lbmRQb2ludFVybFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY3JlYXRlQ29sdW1ucygpIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMuZW5hYmxlQ2hlY2tib3hTZWxlY3Rpb24pIHtcclxuICAgICAgY29uc3Qgc2VsZWN0Q29sdW1uID0ge1xyXG4gICAgICAgIGhlYWRlck5hbWU6ICdTZWxlY3QnLFxyXG4gICAgICAgIGhlYWRlckNoZWNrYm94U2VsZWN0aW9uOiB0cnVlLFxyXG4gICAgICAgIGhlYWRlckNoZWNrYm94U2VsZWN0aW9uRmlsdGVyZWRPbmx5OiB0cnVlLFxyXG4gICAgICAgIGNoZWNrYm94U2VsZWN0aW9uOiB0cnVlLFxyXG4gICAgICAgIG1pbldpZHRoOiA0MCxcclxuICAgICAgICBtYXhXaWR0aDogNDAsXHJcbiAgICAgICAgY2VsbFN0eWxlOiB7ICdwYWRkaW5nLXRvcCc6ICc1cHgnIH0sXHJcbiAgICAgICAgc2hvd1Jvd0dyb3VwOiBmYWxzZSxcclxuICAgICAgICBlbmFibGVSb3dHcm91cDogZmFsc2UsXHJcbiAgICAgICAgZW5hYmxlUGl2b3Q6IGZhbHNlLFxyXG4gICAgICAgIHN1cHByZXNzRmlsdGVyOiB0cnVlLFxyXG4gICAgICAgIG1lbnVUYWJzOiBbXVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmNvbHVtbkRlZnMucHVzaChzZWxlY3RDb2x1bW4pO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZ3JpZE9wdGlvbnMuY29sdW1ucy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBsZXQgY2VsbFJlbmRlcmVyOiBDb3JlR3JpZENlbGxUeXBlO1xyXG4gICAgICBsZXQgY29sdW1FZGl0YWJsZTogYm9vbGVhbiA9IHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGU7XHJcbiAgICAgIGlmIChpdGVtLmVkaXRhYmxlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICBjb2x1bUVkaXRhYmxlID0gaXRlbS5lZGl0YWJsZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNvbHVtRWRpdGFibGUpIHtcclxuICAgICAgICBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTGFiZWwpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUxhYmVsQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlRleHQpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVRleHRFZGl0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkRhdGUpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVFZGl0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlNlbGVjdCkge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0RWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5OdW1lcmljKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljRWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5Cb29sZWFuKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVCb29sZWFuRWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5Mb3YpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUxPVkVkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTWFzaykge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0VkaXRDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuQ3VycmVuY3kpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5RWRpdENlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5OdW1lcmljTGFiZWwpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNDZWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTGFiZWwpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUxhYmVsQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlRleHQpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUxhYmVsQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkRhdGUpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuU2VsZWN0KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTnVtZXJpYykge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTnVtZXJpY0NlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5Cb29sZWFuKSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVCb29sZWFuQ2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLkxvdikge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLk1hc2spIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU1hc2tDZWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuQ3VycmVuY3kpIHtcclxuICAgICAgICAgIGNlbGxSZW5kZXJlciA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5Q2VsbDtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLk51bWVyaWNMYWJlbCkge1xyXG4gICAgICAgICAgY2VsbFJlbmRlcmVyID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTnVtZXJpY0NlbGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0U2VsZWN0KSB7XHJcbiAgICAgICAgICBjZWxsUmVuZGVyZXIgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RDZWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGl0ZW0udmlzaWJsZSAhPT0gZmFsc2UpIHtcclxuICAgICAgICBpdGVtLnZpc2libGUgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGl0ZW0udmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgaGVhZGVyTmFtZSA9IGl0ZW0uZmllbGQ7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zXHJcbiAgICAgICAgJiYgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9uc1xyXG4gICAgICAgICYmIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9uc1xyXG4gICAgICAgICYmIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5lbmRQb2ludFVybCkge1xyXG4gICAgICAgIGNvbnN0IGVuZFBvaW50VXJsID0gdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLmVuZFBvaW50VXJsO1xyXG4gICAgICAgIGlmIChlbmRQb2ludFVybC5pbmRleE9mKCcvJykgIT09IC0xKSB7XHJcbiAgICAgICAgICBoZWFkZXJOYW1lID0gZW5kUG9pbnRVcmwuc3BsaXQoJy8nKVswXSArIGl0ZW0uZmllbGQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGhlYWRlck5hbWUgPSBlbmRQb2ludFVybCArIGl0ZW0uZmllbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBjb2x1bW4gPSB7XHJcbiAgICAgICAgZmllbGQ6IGl0ZW0uZmllbGQsXHJcbiAgICAgICAgaGVhZGVyTmFtZTogaXRlbS5oZWFkZXJOYW1lIHx8IGhlYWRlck5hbWUsXHJcbiAgICAgICAgY2VsbFJlbmRlcmVyLFxyXG4gICAgICAgIGhpZGU6ICFpdGVtLnZpc2libGUsXHJcbiAgICAgICAgc3VwcHJlc3NTaXplVG9GaXQ6IG51bGwsXHJcbiAgICAgICAgZmlsdGVyOiB0aGlzLmdyaWRPcHRpb25zLmVuYWJsZUZpbHRlciB8fCBmYWxzZSxcclxuICAgICAgICBzb3J0YWJsZTogdGhpcy5ncmlkT3B0aW9ucy5lbmFibGVTb3J0aW5nIHx8IGZhbHNlLFxyXG4gICAgICAgIHJlc2l6YWJsZTogdHJ1ZSxcclxuICAgICAgICBjZWxsRWRpdG9yUGFyYW1zOiB7XHJcbiAgICAgICAgICBkYXRhU291cmNlOiBudWxsLFxyXG4gICAgICAgICAgc2V0dGluZ3M6IG51bGwsXHJcbiAgICAgICAgICBkYXRhOiBudWxsLFxyXG4gICAgICAgICAgY3VzdG9tRGlzcGxheUZ1bmN0aW9uOiBpdGVtLmN1c3RvbURpc3BsYXlGdW5jdGlvbiB8fCBudWxsLFxyXG4gICAgICAgICAgY3VzdG9tRWRpdGFibGVGdW5jdGlvbjogaXRlbS5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uIHx8IG51bGwsXHJcbiAgICAgICAgICBjdXN0b21GdW5jdGlvbkZvclJvd1ZhbHVlQ2hhbmdlczogaXRlbS5jdXN0b21GdW5jdGlvbkZvclJvd1ZhbHVlQ2hhbmdlcyB8fCBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmbG9hdGluZ0ZpbHRlckNvbXBvbmVudFBhcmFtczoge1xyXG4gICAgICAgICAgaGlkZUZpbHRlcjogaXRlbS5oaWRlRmlsdGVyIHx8IGZhbHNlLFxyXG4gICAgICAgICAgc2VydmljZUFjY2VzczogdGhpcyxcclxuICAgICAgICAgIGZpZWxkOiBpdGVtLmZpZWxkLFxyXG4gICAgICAgICAgY3VzdG9tRmlsdGVyRnVuY3Rpb246IGl0ZW0uY3VzdG9tRmlsdGVyRnVuY3Rpb24gfHwgbnVsbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3VzdG9tRmllbGRGb3JGaWx0ZXI6IGl0ZW0uY3VzdG9tRmllbGRGb3JGaWx0ZXIsXHJcbiAgICAgICAgY3VzdG9tRmllbGRGb3JTb3J0OiBpdGVtLmN1c3RvbUZpZWxkRm9yU29ydCxcclxuICAgICAgICBnZXREYXRhU291cmNlQWZ0ZXJEYXRhTG9hZGVkOiBpdGVtLmdldERhdGFTb3VyY2VBZnRlckRhdGFMb2FkZWQsXHJcbiAgICAgICAgY29sdW1uVHlwZTogaXRlbS5jb2x1bW5UeXBlXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoaXRlbS5maWVsZCA9PT0gdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZCkge1xyXG4gICAgICAgIGNvbHVtbi5zdXBwcmVzc1NpemVUb0ZpdCA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5EYXRlICYmICh0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlIHx8IGNvbHVtblsnZWRpdGFibGUnXSkpIHtcclxuICAgICAgICBjb2x1bW5bJ21pbldpZHRoJ10gPSAxMzU7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICBpZiAoaXRlbS5taW5XaWR0aCkge1xyXG4gICAgICAgIGNvbHVtblsnbWluV2lkdGgnXSA9IGl0ZW0ubWluV2lkdGg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLm1heFdpZHRoKSB7XHJcbiAgICAgICAgY29sdW1uWydtYXhXaWR0aCddID0gaXRlbS5tYXhXaWR0aDtcclxuICAgICAgfVxyXG5cclxuXHJcbiAgICAgIGlmIChpdGVtLmRhdGFTb3VyY2UpIHtcclxuICAgICAgICBjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gaXRlbS5kYXRhU291cmNlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChpdGVtLnNldHRpbmdzKSB7XHJcbiAgICAgICAgY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MgPSBpdGVtLnNldHRpbmdzO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoXHJcbiAgICAgICAgKGl0ZW0uY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlNlbGVjdCB8fCBjZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdENlbGwpXHJcbiAgICAgICAgJiYgaXRlbS5zZXR0aW5ncykge1xyXG4gICAgICAgIGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLmRhdGEgPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG4gICAgICAgIGlmICghaXRlbS5nZXREYXRhU291cmNlQWZ0ZXJEYXRhTG9hZGVkKSB7XHJcbiAgICAgICAgICB0aGlzLmdldENvbHVtbkRhdGFTb3VyY2UoY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuZGF0YSwgY29sdW1uKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IGluZGV4ID0gXy5maW5kSW5kZXgodGhpcy5jb2x1bW5EZWZzLCBbJ2ZpZWxkJywgaXRlbS5maWVsZF0pO1xyXG4gICAgICBpZiAoaW5kZXggPCAwKSB7XHJcbiAgICAgICAgdGhpcy5jb2x1bW5EZWZzLnB1c2goY29sdW1uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmNvbHVtbkRlZnNbaW5kZXhdID0gY29sdW1uO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuc2V0RmlsdGVyQ29tcG9uZW50cygpO1xyXG4gICAgdGhpcy5hZGRDb21tYW5kQ29sdW1uKCk7XHJcbiAgfVxyXG5cclxuICByZWxvYWRDb2x1bW5EZWZzKCkge1xyXG4gICAgdGhpcy5jcmVhdGVDb2x1bW5zKCk7XHJcbiAgICB0aGlzLnJlbG9hZEdyaWRDb2x1bW5EZWZzKCk7XHJcbiAgfVxyXG5cclxuICBnZXRDb2x1bW5TdGF0ZSgpOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY29sdW1uQXBpLmdldENvbHVtblN0YXRlKCkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q29sdW1uRGF0YVNvdXJjZShkYXRhOiBTdWJqZWN0PGFueT4sIGNvbHVtbjogYW55LCBleHRyYUZpbHRlcnM/OiBGaWx0ZXJbXSkge1xyXG4gICAgaWYgKHRoaXMubG9hZE9uSW5pdCB8fCB0aGlzLmxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0KSB7XHJcbiAgICAgIGNvbnN0IHNldHRpbmdzID0gY29sdW1uLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MgYXMgU2VsZWN0b3JTZXR0aW5ncyB8IExvdlNldHRpbmdzO1xyXG4gICAgICBsZXQgcmVxdWVzdE9wdGlvbnM6IFJlcXVlc3RPcHRpb25zO1xyXG4gICAgICBpZiAoc2V0dGluZ3MpIHtcclxuICAgICAgICBpZiAoY29sdW1uLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5TZWxlY3QgfHwgY29sdW1uLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0U2VsZWN0KSB7XHJcbiAgICAgICAgICByZXF1ZXN0T3B0aW9ucyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoKHNldHRpbmdzIGFzIFNlbGVjdG9yU2V0dGluZ3MpLnJlcXVlc3RPcHRpb25zKSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJlcXVlc3RPcHRpb25zID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSgoc2V0dGluZ3MgYXMgTG92U2V0dGluZ3MpLm9wdGlvbnMucmVxdWVzdE9wdGlvbnMpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChyZXF1ZXN0T3B0aW9ucykge1xyXG4gICAgICAgIGNvbnN0IHJlcVVybCA9IHJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMubW9kdWxlVXJsICsgcmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5lbmRQb2ludFVybDtcclxuXHJcbiAgICAgICAgbGV0IGRpc2FibGVHZW5lcmFsTG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmICghdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2VsZkxvYWRpbmcpIHtcclxuICAgICAgICAgIGRpc2FibGVHZW5lcmFsTG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLmxvYWRpbmdTZXJ2aWNlLmluc2VydExvYWRpbmdSZXF1ZXN0KHJlcVVybCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGRpc2FibGVMb2FkaW5nSGVhZGVyOiBIZWFkZXJQYXJhbWV0ZXIgPSB7IGhlYWRlcjogSGVhZGVycy5EaXNhYmxlTG9hZGluZywgdmFsdWU6IEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcgfTtcclxuXHJcbiAgICAgICAgaWYgKGV4dHJhRmlsdGVycyAmJiBleHRyYUZpbHRlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgaWYgKHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlciAmJiByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzKSB7XHJcbiAgICAgICAgICAgIGlmIChyZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMgPSBbe1xyXG4gICAgICAgICAgICAgICAgRmlsdGVyczogW11cclxuICAgICAgICAgICAgICB9XTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcy51bnNoaWZ0KHtcclxuICAgICAgICAgICAgICAgIEZpbHRlcnM6IFtdXHJcbiAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKCFyZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIpIHtcclxuICAgICAgICAgICAgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyID0ge1xyXG4gICAgICAgICAgICAgIEZpbHRlckdyb3VwczogW3tcclxuICAgICAgICAgICAgICAgIEZpbHRlcnM6IFtdXHJcbiAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlciAmJiAhcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcykge1xyXG4gICAgICAgICAgICByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzID0gW3tcclxuICAgICAgICAgICAgICBGaWx0ZXJzOiBbXVxyXG4gICAgICAgICAgICB9XTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAocmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyICYmXHJcbiAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMgJiZcclxuICAgICAgICAgICAgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLkZpbHRlckdyb3Vwcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHNbMF0uRmlsdGVycyA9IGV4dHJhRmlsdGVycztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZGF0YVNlcnZpY2UuZ2V0TGlzdEJ5KHJlcVVybCwgcmVxdWVzdE9wdGlvbnMuQ3VzdG9tRmlsdGVyLCBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPyBbZGlzYWJsZUxvYWRpbmdIZWFkZXJdIDogbnVsbClcclxuICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICBmaW5hbGl6ZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKCF0aGlzLmdyaWRPcHRpb25zLmRpc2FibGVTZWxmTG9hZGluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5yZW1vdmVMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBsb3ZEYXRhOiBhbnlbXSA9IGh0dHBSZXNwb25zZS5zZXJ2aWNlUmVzdWx0LlJlc3VsdDtcclxuICAgICAgICAgICAgY29uc3QgZGF0YVNvdXJjZSA9IGxvdkRhdGEubWFwKHggPT4gT2JqZWN0LmFzc2lnbih7fSwgeCkpO1xyXG4gICAgICAgICAgICBjb25zdCBjb2xJbmRleCA9IF8uZmluZEluZGV4KHRoaXMuY29sdW1uRGVmcywgWydmaWVsZCcsIGNvbHVtbi5maWVsZF0pO1xyXG4gICAgICAgICAgICBjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgICAgICAgdGhpcy5jb2x1bW5EZWZzW2NvbEluZGV4XS5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICAgICAgICBkYXRhLm5leHQoeyBkYXRhOiB0cnVlIH0pO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldENvbXBvbmVudHMoKSB7XHJcbiAgICBjb25zdCBjb21wb25lbnRzID0ge307XHJcbiAgICAvLyBFZGl0IENlbGxcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkVkaXRDZWxsXSA9IEJvb2xlYW5FZGl0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlRGF0ZUVkaXRDZWxsXSA9IERhdGVFZGl0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTE9WRWRpdENlbGxdID0gTG92RWRpdENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNFZGl0Q2VsbF0gPSBOdW1lcmljRWRpdENlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdEVkaXRDZWxsXSA9IFNlbGVjdEVkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RWRpdENlbGxdID0gVGV4dEVkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrRWRpdENlbGxdID0gTWFza0VkaXRDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUVkaXRDZWxsXSA9IEN1cnJlbmN5RWRpdENlbGxDb21wb25lbnQ7XHJcbiAgICAvLyBSZWFkLU9ubHkgQ2VsbFxyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVCb29sZWFuQ2VsbF0gPSBCb29sZWFuQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTGFiZWxDZWxsXSA9IExhYmVsQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlRGF0ZUNlbGxdID0gRGF0ZUNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdENlbGxdID0gU2VsZWN0Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lDZWxsXSA9IEN1cnJlbmN5Q2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0NlbGxdID0gTWFza0NlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNDZWxsXSA9IE51bWVyaWNDZWxsQ29tcG9uZW50O1xyXG4gICAgLy8gRmlsdGVyIENlbGxcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlRGF0ZUZpbHRlckNlbGxdID0gRGF0ZUZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdEZpbHRlckNlbGxdID0gU2VsZWN0RmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlVGV4dEZpbHRlckNlbGxdID0gVGV4dEZpbHRlckNlbGxDb21wb25lbnQ7XHJcbiAgICBjb21wb25lbnRzW0NvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNGaWx0ZXJDZWxsXSA9IE51bWVyaWNGaWx0ZXJDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVCb29sZWFuRmlsdGVyQ2VsbF0gPSBCb29sZWFuRmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lGaWx0ZXJDZWxsXSA9IEN1cnJlbmN5RmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbQ29yZUdyaWRDZWxsVHlwZS5Db3JlTE9WRmlsdGVyQ2VsbF0gPSBMb3ZGaWx0ZXJDZWxsQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrRmlsdGVyQ2VsbF0gPSBNYXNrRmlsdGVyQ2VsbENvbXBvbmVudDtcclxuICAgIC8vIENvbW1hbmQgQ2VsbFxyXG4gICAgY29tcG9uZW50c1tDb3JlR3JpZENlbGxUeXBlLkNvcmVDb21tYW5kQ2VsbF0gPSBDb21tYW5kQ2VsbENvbXBvbmVudDtcclxuXHJcbiAgICBjb21wb25lbnRzWydhZ0NvbHVtbkhlYWRlciddID0gQ29sdW1uSGVhZGVyQ29tcG9uZW50O1xyXG4gICAgY29tcG9uZW50c1snY3VzdG9tTm9Sb3dzT3ZlcmxheSddID0gTm9Sb3dzT3ZlcmxheUNvbXBvbmVudDtcclxuICAgIGNvbXBvbmVudHNbJ2N1c3RvbUxvYWRpbmdPdmVybGF5J10gPSBMb2FkaW5nT3ZlcmxheUNvbXBvbmVudDtcclxuXHJcbiAgICByZXR1cm4gY29tcG9uZW50cztcclxuICB9XHJcblxyXG4gIHNldEZpbHRlckNvbXBvbmVudHMoKSB7XHJcbiAgICB0aGlzLmNvbHVtbkRlZnMuZm9yRWFjaChpdGVtID0+IHtcclxuXHJcbiAgICAgIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkVkaXRDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUJvb2xlYW5GaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVEYXRlRWRpdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlRGF0ZUZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZURhdGVGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMT1ZFZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVMT1ZGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVOdW1lcmljRWRpdENlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTnVtZXJpY0ZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdEVkaXRDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVNlbGVjdEZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZVRleHRFZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkNlbGwpIHtcclxuICAgICAgICBpdGVtLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50ID0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQm9vbGVhbkZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUxhYmVsQ2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlTWFza0VkaXRDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU1hc2tGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrQ2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVNYXNrRmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ3VycmVuY3lFZGl0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZUN1cnJlbmN5Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVDdXJyZW5jeUZpbHRlckNlbGw7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5jZWxsUmVuZGVyZXIgPT09IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNDZWxsKSB7XHJcbiAgICAgICAgaXRlbS5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudCA9IENvcmVHcmlkQ2VsbFR5cGUuQ29yZU51bWVyaWNGaWx0ZXJDZWxsO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uY2VsbFJlbmRlcmVyID09PSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RDZWxsXHJcbiAgICAgICAgJiYgaXRlbS5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuVGV4dFNlbGVjdCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVUZXh0RmlsdGVyQ2VsbDtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmNlbGxSZW5kZXJlciA9PT0gQ29yZUdyaWRDZWxsVHlwZS5Db3JlU2VsZWN0Q2VsbCkge1xyXG4gICAgICAgIGl0ZW0uZmxvYXRpbmdGaWx0ZXJDb21wb25lbnQgPSBDb3JlR3JpZENlbGxUeXBlLkNvcmVTZWxlY3RGaWx0ZXJDZWxsO1xyXG4gICAgICB9XHJcblxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGxvYWREYXRhQnlGaWx0ZXJBbmRQYWdlU2V0dGluZyhnZW5lcmljRXhwcmVzc2lvbjogR2VuZXJpY0V4cHJlc3Npb24pIHtcclxuICAgIGNvbnN0IHJlcVVybCA9IGAke3RoaXMudXJsT3B0aW9ucy5tb2R1bGVVcmx9JHt0aGlzLnVybE9wdGlvbnMuZW5kUG9pbnRVcmx9YDtcclxuXHJcbiAgICBsZXQgaGVhZGVyUGFyYW1zOiBIZWFkZXJQYXJhbWV0ZXJbXSA9IFtdO1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkhlYWRlclBhcmFtZXRlcnMpIHtcclxuICAgICAgaGVhZGVyUGFyYW1zID0gdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5IZWFkZXJQYXJhbWV0ZXJzO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghdGhpcy5ncmlkT3B0aW9ucy5kaXNhYmxlU2VsZkxvYWRpbmcpIHtcclxuICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5pbnNlcnRMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG5cclxuICAgICAgY29uc3QgZGlzYWJsZUxvYWRpbmdIZWFkZXI6IEhlYWRlclBhcmFtZXRlciA9IHsgaGVhZGVyOiBIZWFkZXJzLkRpc2FibGVMb2FkaW5nLCB2YWx1ZTogSGVhZGVycy5EaXNhYmxlTG9hZGluZyB9O1xyXG4gICAgICBoZWFkZXJQYXJhbXMucHVzaChkaXNhYmxlTG9hZGluZ0hlYWRlcik7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuZGF0YVNlcnZpY2UuZ2V0TGlzdEJ5KHJlcVVybCwgZ2VuZXJpY0V4cHJlc3Npb24sIGhlYWRlclBhcmFtcylcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmluYWxpemUoKCkgPT4ge1xyXG4gICAgICAgICAgaWYgKCF0aGlzLmdyaWRPcHRpb25zLmRpc2FibGVTZWxmTG9hZGluZykge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRpbmdTZXJ2aWNlLnJlbW92ZUxvYWRpbmdSZXF1ZXN0KHJlcVVybCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSkpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBsb2FkR3JpZFZpZXdEYXRhQnlPcHRpb25zVXJsQW5kRmlsdGVycygpIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zKSB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaEZpbHRlcjogR2VuZXJpY0V4cHJlc3Npb24gPSBfLmNsb25lRGVlcCh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlcik7XHJcbiAgICAgIGlmICh0aGlzLnBhZ2luZ1Jlc3VsdCkge1xyXG4gICAgICAgIHNlYXJjaEZpbHRlci5QYWdlTnVtYmVyID0gdGhpcy5wYWdpbmdSZXN1bHQuQ3VycmVudFBhZ2U7XHJcbiAgICAgICAgc2VhcmNoRmlsdGVyLlBhZ2VTaXplID0gdGhpcy5wYWdpbmdSZXN1bHQuUGFnZVNpemU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHNlYXJjaEZpbHRlci5GaWx0ZXJHcm91cHMgPSBzZWFyY2hGaWx0ZXIuRmlsdGVyR3JvdXBzIHx8IFtdO1xyXG4gICAgICB0aGlzLmZpbHRlci5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgIGxldCBuYW1lOiBzdHJpbmc7XHJcbiAgICAgICAgY29uc3QgZm91bmRDb2x1bW4gPSB0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMuZmluZCh4ID0+IHguZmllbGQgPT09IGVsZW1lbnQua2V5KTtcclxuICAgICAgICBpZiAoZm91bmRDb2x1bW4pIHtcclxuICAgICAgICAgIG5hbWUgPSBmb3VuZENvbHVtbi5jdXN0b21GaWVsZEZvckZpbHRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFuYW1lKSB7XHJcbiAgICAgICAgICBpZiAoZWxlbWVudC5rZXkuaW5kZXhPZignSWQnKSA+IDAgJiZcclxuICAgICAgICAgICAgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IGZhbHNlIHx8IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgICAgbmFtZSA9IGVsZW1lbnQua2V5LnN1YnN0cigwLCBlbGVtZW50LmtleS5pbmRleE9mKCdJZCcpKSArXHJcbiAgICAgICAgICAgICAgJy4nICsgZWxlbWVudC5rZXkuc3Vic3RyKGVsZW1lbnQua2V5LmluZGV4T2YoJ0lkJyksIGVsZW1lbnQua2V5Lmxlbmd0aCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBuYW1lID0gZWxlbWVudC5rZXk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBncm91cDogRmlsdGVyR3JvdXAgPSB7XHJcbiAgICAgICAgICBGaWx0ZXJzOiBbe1xyXG4gICAgICAgICAgICBQcm9wZXJ0eU5hbWU6IG5hbWUsXHJcbiAgICAgICAgICAgIFZhbHVlOiBlbGVtZW50LnZhbHVlWydmaWx0ZXInXSxcclxuICAgICAgICAgICAgQ29tcGFyaXNvbjogZWxlbWVudC52YWx1ZVsndHlwZSddLFxyXG4gICAgICAgICAgICBPdGhlclZhbHVlOiBlbGVtZW50LnZhbHVlWydvdGhlciddIHx8IG51bGxcclxuICAgICAgICAgIH1dXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgc2VhcmNoRmlsdGVyLkZpbHRlckdyb3Vwcy5wdXNoKGdyb3VwKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAvLyBpdCBpcyBmb3IgQ29yZUdyaWRDb2x1bW5UeXBlLlRleHRTZWxlY3RcclxuICAgICAgdGhpcy5maWx0ZXJHcm91cExpc3QuZm9yRWFjaCgoZmlsdGVyR3JvdXBJdGVtOiBhbnkpID0+IHtcclxuICAgICAgICBzZWFyY2hGaWx0ZXIuRmlsdGVyR3JvdXBzLnVuc2hpZnQoZmlsdGVyR3JvdXBJdGVtLnZhbHVlKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAvLyBpZiAoc2VhcmNoRmlsdGVyLkZpbHRlckdyb3Vwcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIC8vICAgc2VhcmNoRmlsdGVyLkZpbHRlckdyb3Vwc1tzZWFyY2hGaWx0ZXIuRmlsdGVyR3JvdXBzLmxlbmd0aCAtIDFdWydDb25uZWN0b3InXSA9IENvbm5lY3Rvci5PcjtcclxuICAgICAgLy8gfVxyXG5cclxuICAgICAgdGhpcy5sb2FkRGF0YUJ5RmlsdGVyQW5kUGFnZVNldHRpbmcoc2VhcmNoRmlsdGVyKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKGh0dHBSZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnBhZ2luZ1Jlc3VsdCA9IGh0dHBSZXNwb25zZS5wYWdpbmdSZXN1bHQ7XHJcbiAgICAgICAgICB0aGlzLmRhdGEgPSBodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQ7XHJcbiAgICAgICAgICBpZiAodGhpcy5kYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3JpZ2luYWxSb3dEYXRhID0gdGhpcy5kYXRhLm1hcChpdGVtID0+IE9iamVjdC5hc3NpZ24oe30sIGl0ZW0pKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZVN1YmplY3QubmV4dCh0aGlzLmRhdGEpO1xyXG5cclxuICAgICAgICAgIGlmICghdGhpcy5kYXRhIHx8ICh0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLmxlbmd0aCA9PT0gMCkpIHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nT3ZlcmxheSgpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMubG9hZERhdGFTb3VyY2VBZnRlckdldEdyaWREYXRhKCk7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZERhdGFTb3VyY2VBZnRlckdldEdyaWREYXRhKCk6IHZvaWQge1xyXG4gICAgY29uc3QgbmVlZFRvTG9hZERhdGFTb3VyY2VDb2x1bW5zID0gdGhpcy5jb2x1bW5EZWZzLmZpbHRlcih4ID0+IHguZ2V0RGF0YVNvdXJjZUFmdGVyRGF0YUxvYWRlZCA9PT0gdHJ1ZSk7XHJcbiAgICBuZWVkVG9Mb2FkRGF0YVNvdXJjZUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICBjb25zdCBjb2x1bW5WYWx1ZXM6IGFueVtdID0gdGhpcy5kYXRhLm1hcCh4ID0+IHhbY29sdW1uLmZpZWxkXSk7XHJcbiAgICAgIGNvbnN0IG5ld0ZpbHRlcnM6IEZpbHRlcltdID0gW107XHJcblxyXG4gICAgICBjb2x1bW5WYWx1ZXMuZm9yRWFjaCgoY29sdW1uVmFsdWU6IGFueSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IG5ld0ZpbHRlcjogRmlsdGVyID0ge1xyXG4gICAgICAgICAgUHJvcGVydHlOYW1lOiBjb2x1bW4uY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy52YWx1ZUZpZWxkLFxyXG4gICAgICAgICAgVmFsdWU6IGNvbHVtblZhbHVlLFxyXG4gICAgICAgICAgQ29tcGFyaXNvbjogQ29tcGFyaXNvbi5FcXVhbFRvLFxyXG4gICAgICAgICAgQ29ubmVjdG9yOiBDb25uZWN0b3IuT3JcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBsZXQgbmFtZSA9IGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLnZhbHVlRmllbGQ7XHJcblxyXG4gICAgICAgIC8vIGNhbiBiZSBjaGFuZ2FibGVcclxuICAgICAgICBpZiAobmFtZSkge1xyXG4gICAgICAgICAgaWYgKGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLnZhbHVlRmllbGQuaW5kZXhPZignSWQnKSAhPT0gLTEgJiZcclxuICAgICAgICAgICAgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IGZhbHNlIHx8IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgICAgbmFtZSA9IG5hbWUuc3Vic3RyKDAsIG5hbWUuaW5kZXhPZignSWQnKSkgK1xyXG4gICAgICAgICAgICAgICcuJyArIG5hbWUuc3Vic3RyKG5hbWUuaW5kZXhPZignSWQnKSwgbmFtZS5sZW5ndGgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbmV3RmlsdGVyLlByb3BlcnR5TmFtZSA9IG5hbWU7XHJcblxyXG4gICAgICAgIG5ld0ZpbHRlcnMucHVzaChuZXdGaWx0ZXIpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGlmIChuZXdGaWx0ZXJzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICBuZXdGaWx0ZXJzW25ld0ZpbHRlcnMubGVuZ3RoIC0gMV0uQ29ubmVjdG9yID0gQ29ubmVjdG9yLkFuZDtcclxuICAgICAgfVxyXG4gICAgICBpZiAobmV3RmlsdGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5nZXRDb2x1bW5EYXRhU291cmNlKGNvbHVtbi5jZWxsRWRpdG9yUGFyYW1zLmRhdGEsIGNvbHVtbiwgbmV3RmlsdGVycyk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgLy8gZ2V0Q29sdW1uRGF0YVNvdXJjZSBcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYWRkQ29tbWFuZENvbHVtbigpIHtcclxuICAgIGxldCBzaG93Um93RGVsZXRlQnV0dG9uID0gdHJ1ZTtcclxuICAgIGxldCBzaG93Um93RWRpdEJ1dHRvbiA9IHRydWU7XHJcbiAgICBsZXQgYnV0dG9uQ291bnRzID0gMTtcclxuXHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzKSB7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSb3dEZWxldGVCdXR0b24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHNob3dSb3dEZWxldGVCdXR0b24gPSB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSb3dEZWxldGVCdXR0b247XHJcbiAgICAgICAgaWYgKHNob3dSb3dEZWxldGVCdXR0b24pIHtcclxuICAgICAgICAgIGJ1dHRvbkNvdW50cysrO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93RWRpdEJ1dHRvbiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgc2hvd1Jvd0VkaXRCdXR0b24gPSB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dFZGl0QnV0dG9uO1xyXG4gICAgICAgIGlmIChzaG93Um93RWRpdEJ1dHRvbikge1xyXG4gICAgICAgICAgYnV0dG9uQ291bnRzKys7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gaXQgaXMgZm9yIGRlbGV0ZSBhbmQgcmV2ZXJ0IGZvciBpbmxpbmUgZ3JpZFxyXG4gICAgLy8gaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGUgJiYgYnV0dG9uQ291bnRzID09PSAxKSB7XHJcbiAgICAvLyAgIGJ1dHRvbkNvdW50cyA9IDI7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgaWYgKHNob3dSb3dEZWxldGVCdXR0b24gfHwgc2hvd1Jvd0VkaXRCdXR0b24pIHtcclxuICAgICAgY29uc3QgaW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmNvbHVtbkRlZnMsIFsnZmllbGQnLCAnQ29tbWFuZENvbHVtbiddKTtcclxuICAgICAgaWYgKGluZGV4IDwgMCkge1xyXG4gICAgICAgIGNvbnN0IGNvbHVtbiA9IHtcclxuICAgICAgICAgIGhlYWRlck5hbWU6ICcnLFxyXG4gICAgICAgICAgZmllbGQ6ICdDb21tYW5kQ29sdW1uJyxcclxuICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcclxuICAgICAgICAgIGNlbGxSZW5kZXJlcjogQ29yZUdyaWRDZWxsVHlwZS5Db3JlQ29tbWFuZENlbGwsXHJcbiAgICAgICAgICBmaWx0ZXI6IGZhbHNlLFxyXG4gICAgICAgICAgc29ydGFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgbWluV2lkdGg6IGJ1dHRvbkNvdW50cyAqIDUwLFxyXG4gICAgICAgICAgbWF4V2lkdGg6IGJ1dHRvbkNvdW50cyAqIDUwXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmNvbHVtbkRlZnMucHVzaChjb2x1bW4pO1xyXG4gICAgICB9XHJcbiAgICAgIC8vIHRoaXMuY29sdW1uRGVmcy5wdXNoKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZUZvcm1Db250cm9scygpIHtcclxuICAgIGNvbnN0IGNvbHVtbnMgPSB0aGlzLmNvbHVtbkFwaS5nZXRBbGxDb2x1bW5zKCk7XHJcblxyXG4gICAgY29uc3QgZ3JpZEZvcm1Hcm91cCA9ICh0aGlzLmdyaWRGb3JtIGFzIEZvcm1Hcm91cCk7XHJcbiAgICAvLyBjbGVhciBvdXQgb2xkIGZvcm0gZ3JvdXAgY29udHJvbHMgaWYgc3dpdGNoaW5nIGJldHdlZW4gZGF0YSBzb3VyY2VzXHJcbiAgICBjb25zdCBjb250cm9sTmFtZXMgPSBPYmplY3Qua2V5cyhncmlkRm9ybUdyb3VwLmNvbnRyb2xzKTtcclxuICAgIGNvbnRyb2xOYW1lcy5mb3JFYWNoKChjb250cm9sTmFtZSkgPT4ge1xyXG4gICAgICBncmlkRm9ybUdyb3VwLnJlbW92ZUNvbnRyb2woY29udHJvbE5hbWUpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5ncmlkQXBpLmZvckVhY2hOb2RlKChyb3dOb2RlOiBSb3dOb2RlKSA9PiB7XHJcblxyXG4gICAgICBjb25zdCBmb3JtQXJyYXk6IEZvcm1BcnJheSA9IG5ldyBGb3JtQXJyYXkoW10pO1xyXG5cclxuICAgICAgY29sdW1ucy5maWx0ZXIoKGNvbHVtbjogQ29sdW1uKSA9PiBjb2x1bW4uZ2V0Q29sRGVmKCkuZmllbGQgIT09ICdDb21tYW5kQ29sdW1uJylcclxuICAgICAgICAuZm9yRWFjaCgoY29sdW1uOiBDb2x1bW4pID0+IHtcclxuICAgICAgICAgIGNvbnN0IGtleSA9IHRoaXMuY3JlYXRlS2V5KHRoaXMuY29sdW1uQXBpLCBjb2x1bW4pOyAvLyB0aGUgY2VsbHMgd2lsbCB1c2UgdGhpcyBzYW1lIGNyZWF0ZUtleSBtZXRob2RcclxuICAgICAgICAgIGNvbnN0IGNvbDogR3JpZENvbHVtbiA9IF8uZmluZCh0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMsIFsnZmllbGQnLCBjb2x1bW4uZ2V0Q29sRGVmKCkuZmllbGRdKTtcclxuICAgICAgICAgIGNvbnN0IHZhbGlkYXRpb246IFZhbGlkYXRvckZuW10gPSBbXTtcclxuICAgICAgICAgIGlmIChjb2wpIHtcclxuICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5yZXF1aXJlZCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlmIChjb2wudmFsaWRhdGlvbi5lbWFpbCkge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbi5wdXNoKFZhbGlkYXRvcnMuZW1haWwpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24ucmVxdWlyZWRUcnVlKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5yZXF1aXJlZFRydWUpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24ubWluVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRpb24ucHVzaChWYWxpZGF0b3JzLm1pbihjb2wudmFsaWRhdGlvbi5taW5WYWx1ZSkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24ubWF4VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRpb24ucHVzaChWYWxpZGF0b3JzLm1heChjb2wudmFsaWRhdGlvbi5tYXhWYWx1ZSkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoY29sLnZhbGlkYXRpb24ubWluTGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5taW5MZW5ndGgoY29sLnZhbGlkYXRpb24ubWluTGVuZ3RoKSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlmIChjb2wudmFsaWRhdGlvbi5tYXhMZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRpb24ucHVzaChWYWxpZGF0b3JzLm1heExlbmd0aChjb2wudmFsaWRhdGlvbi5tYXhMZW5ndGgpKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGNvbC52YWxpZGF0aW9uLnJlZ2V4KSB7XHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uLnB1c2goVmFsaWRhdG9ycy5wYXR0ZXJuKGNvbC52YWxpZGF0aW9uLnJlZ2V4KSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBmb3JtQXJyYXkuc2V0Q29udHJvbChrZXkgYXMgYW55LCBuZXcgRm9ybUNvbnRyb2woJycsIHsgdmFsaWRhdG9yczogdmFsaWRhdGlvbiB9KSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIGdyaWRGb3JtR3JvdXAuYWRkQ29udHJvbChyb3dOb2RlLmlkIGFzIGFueSwgZm9ybUFycmF5KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZWxvYWRHcmlkQ29sdW1uRGVmcygpOiB2b2lkIHtcclxuICAgIC8vIHRoaXMuY29sdW1uRGVmcyA9IGNvbHVtbkRlZnM7XHJcbiAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgIHRoaXMuZ3JpZEFwaS5zZXRDb2x1bW5EZWZzKHRoaXMuY29sdW1uRGVmcyk7XHJcbiAgICAgIHRoaXMuY29sdW1uQXBpLnJlc2V0Q29sdW1uU3RhdGUoKTtcclxuICAgIH1cclxuICAgIHRoaXMucmVmcmVzaEZvcm1Db250cm9scygpO1xyXG4gICAgaWYgKHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICB0aGlzLmdyaWRBcGkuc2l6ZUNvbHVtbnNUb0ZpdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2V0Q29sdW1uRGF0YVNvdXJjZShjb2x1bW5OYW1lOiBzdHJpbmcsIGRhdGFTb3VyY2U6IGFueSkge1xyXG4gICAgY29uc3QgaW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMsIFsnZmllbGQnLCBjb2x1bW5OYW1lXSk7XHJcbiAgICB0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnNbaW5kZXhdLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgaWYgKHRoaXMuZ3JpZEFwaSkge1xyXG4gICAgICB0aGlzLmdyaWRBcGkucmVmcmVzaENlbGxzKHsgY29sdW1uczogW2NvbHVtbk5hbWVdLCBmb3JjZTogdHJ1ZSB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldENvbHVtbkxvdk9wdGlvbnMoY29sdW1uOiBHcmlkQ29sdW1uKSB7XHJcbiAgICBpZiAoKGNvbHVtbi5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuU2VsZWN0IHx8IGNvbHVtbi5jb2x1bW5UeXBlID09PSBDb3JlR3JpZENvbHVtblR5cGUuTG92KSAmJiBjb2x1bW4uc2V0dGluZ3MpIHtcclxuICAgICAgY29uc3Qgc2V0dGluZ3MgPSBjb2x1bW4uc2V0dGluZ3MgYXMgU2VsZWN0b3JTZXR0aW5ncyB8IExvdlNldHRpbmdzO1xyXG4gICAgICBsZXQgcmVxdWVzdE9wdGlvbnM6IFJlcXVlc3RPcHRpb25zO1xyXG4gICAgICBpZiAoY29sdW1uLmNvbHVtblR5cGUgPT09IENvcmVHcmlkQ29sdW1uVHlwZS5TZWxlY3QpIHtcclxuICAgICAgICByZXF1ZXN0T3B0aW9ucyA9IChzZXR0aW5ncyBhcyBTZWxlY3RvclNldHRpbmdzKS5yZXF1ZXN0T3B0aW9ucztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXF1ZXN0T3B0aW9ucyA9IChzZXR0aW5ncyBhcyBMb3ZTZXR0aW5ncykub3B0aW9ucy5yZXF1ZXN0T3B0aW9ucztcclxuICAgICAgfVxyXG4gICAgICBjb25zdCByZXFVcmwgPSByZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLm1vZHVsZVVybCArICcvJ1xyXG4gICAgICAgICsgcmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucy5lbmRQb2ludFVybDtcclxuXHJcbiAgICAgIGxldCBkaXNhYmxlR2VuZXJhbExvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgaWYgKCF0aGlzLmdyaWRPcHRpb25zLmRpc2FibGVTZWxmTG9hZGluZykge1xyXG4gICAgICAgIGRpc2FibGVHZW5lcmFsTG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5pbnNlcnRMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnN0IGRpc2FibGVMb2FkaW5nSGVhZGVyOiBIZWFkZXJQYXJhbWV0ZXIgPSB7IGhlYWRlcjogSGVhZGVycy5EaXNhYmxlTG9hZGluZywgdmFsdWU6IEhlYWRlcnMuRGlzYWJsZUxvYWRpbmcgfTtcclxuICAgICAgdGhpcy5kYXRhU2VydmljZS5nZXRMaXN0QnkocmVxVXJsLCByZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIsIGRpc2FibGVHZW5lcmFsTG9hZGluZyA/IFtkaXNhYmxlTG9hZGluZ0hlYWRlcl0gOiBudWxsKVxyXG4gICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgZmluYWxpemUoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuZ3JpZE9wdGlvbnMuZGlzYWJsZVNlbGZMb2FkaW5nKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nU2VydmljZS5yZW1vdmVMb2FkaW5nUmVxdWVzdChyZXFVcmwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KSlcclxuICAgICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgICAgY29uc3QgbG92RGF0YTogYW55W10gPSBodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQ7XHJcbiAgICAgICAgICBjb25zdCBkYXRhU291cmNlID0gbG92RGF0YS5tYXAoeCA9PiBPYmplY3QuYXNzaWduKHt9LCB4KSk7XHJcbiAgICAgICAgICBjb25zdCBjb2xJbmRleCA9IF8uZmluZEluZGV4KHRoaXMuY29sdW1uRGVmcywgWydmaWVsZCcsIGNvbHVtbi5maWVsZF0pO1xyXG4gICAgICAgICAgY29sdW1uLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5EZWZzW2NvbEluZGV4XS5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5EZWZzW2NvbEluZGV4XS5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzID0gc2V0dGluZ3M7XHJcbiAgICAgICAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ3JpZEFwaS5yZWZyZXNoQ2VsbHMoeyBjb2x1bW5zOiBbY29sdW1uLmZpZWxkXSwgZm9yY2U6IHRydWUgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkdyaWRSZWFkeShwYXJhbXMpIHtcclxuICAgIHRoaXMuZ3JpZEFwaSA9IHBhcmFtcy5hcGk7XHJcbiAgICB0aGlzLmNvbHVtbkFwaSA9IHBhcmFtcy5jb2x1bW5BcGk7XHJcblxyXG4gICAgdGhpcy5yZWZyZXNoRm9ybUNvbnRyb2xzKCk7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nRGlzcGxheVN0YXR1cykge1xyXG4gICAgICB0aGlzLmdyaWRBcGkuc2hvd0xvYWRpbmdPdmVybGF5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuaXNMb2FkID09PSBmYWxzZSkge1xyXG4gICAgICB0aGlzLmluaXQodGhpcy5ncmlkT3B0aW9ucywgdGhpcy5sb2FkT25Jbml0LCB0aGlzLmxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0KTtcclxuICAgIH1cclxuICAgIHRoaXMuZ3JpZEFwaS5zaXplQ29sdW1uc1RvRml0KCk7XHJcbiAgfVxyXG5cclxuICBvbkNvbHVtbkV2ZXJ5dGhpbmdDaGFuZ2VkKHBhcmFtcykge1xyXG4gICAgdGhpcy5hbGxHcmlkQ29sdW1ucyA9IHBhcmFtcy5jb2x1bW5BcGkuZ2V0QWxsQ29sdW1ucygpO1xyXG4gIH1cclxuXHJcbiAgb25HcmlkU2l6ZUNoYW5nZWQoKSB7XHJcbiAgICBpZiAodGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zICYmICF0aGlzLmdyaWRPcHRpb25zLmRpc2FibGVTaXplVG9GaXQgJiYgdGhpcy5ncmlkQXBpKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkQXBpLnNpemVDb2x1bW5zVG9GaXQoKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmdyaWRBcGkucmVmcmVzaENlbGxzKHsgZm9yY2U6IHRydWUgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZUtleShjb2x1bW5BcGk6IENvbHVtbkFwaSwgY29sdW1uOiBDb2x1bW4pOiBhbnkge1xyXG4gICAgcmV0dXJuIGNvbHVtbkFwaS5nZXRBbGxDb2x1bW5zKCkuaW5kZXhPZihjb2x1bW4pO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q29udGV4dChzZWxmOiBhbnkpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGNvbXBvbmVudFBhcmVudDogc2VsZixcclxuICAgICAgZm9ybUdyb3VwOiB0aGlzLmdyaWRGb3JtLFxyXG4gICAgICBjcmVhdGVLZXk6IHRoaXMuY3JlYXRlS2V5XHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcmVmcmVzaEZvcm1Db250cm9scygpIHtcclxuICAgIGlmICh0aGlzLmdyaWRBcGkpIHtcclxuICAgICAgdGhpcy5jcmVhdGVGb3JtQ29udHJvbHMoKTtcclxuICAgICAgdGhpcy5ncmlkQXBpLnJlZnJlc2hDZWxscyh7IGZvcmNlOiB0cnVlIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaEdyaWREYXRhKGlzQ2hlY2tCdWxrRGF0YT86IGJvb2xlYW4pIHtcclxuICAgIGNvbnN0IGJ1bGtEYXRhID0gdGhpcy5nZXRCdWxrT3BlcmF0aW9uRGF0YSgpO1xyXG4gICAgaWYgKGJ1bGtEYXRhICYmIGJ1bGtEYXRhLmxlbmd0aCA+IDAgJiYgaXNDaGVja0J1bGtEYXRhKSB7XHJcbiAgICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlJlZnJlc2gpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzOiBBbGVydFJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWNvcmRFdmVudHNTdWJqZWN0Lm5leHQoeyB0eXBlOiAnUmVmcmVzaGVkR3JpZCcsIHJlY29yZDogbnVsbCB9KTtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoR3JpZCgpO1xyXG4gICAgICAgICAgICB0aGlzLnRvYXN0ci5zdWNjZXNzKFRvYXN0ck1lc3NhZ2VzLkdyaWRSZWZyZXNoZWQpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yZWNvcmRFdmVudHNTdWJqZWN0Lm5leHQoeyB0eXBlOiAnUmVmcmVzaGVkR3JpZCcsIHJlY29yZDogbnVsbCB9KTtcclxuICAgICAgdGhpcy5yZWZyZXNoR3JpZCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaEdyaWQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRlbGV0ZWREYXRhID0gW107XHJcbiAgICB0aGlzLmlzRGF0YUNoYW5nZWQgPSBmYWxzZTtcclxuICAgIHRoaXMubG9hZEdyaWRWaWV3RGF0YUJ5T3B0aW9uc1VybEFuZEZpbHRlcnMoKTtcclxuICAgIGlmICh0aGlzLmlzTG9hZCA9PT0gZmFsc2UpIHtcclxuICAgICAgdGhpcy5pbml0KHRoaXMuZ3JpZE9wdGlvbnMsIHRydWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25DZWxsVmFsdWVDaGFuZ2VkKGl0ZW06IGFueSkge1xyXG4gICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gdHJ1ZTtcclxuICAgIGlmIChpdGVtLmRhdGEuT3BlcmF0aW9uVHlwZSAhPT0gT3BlcmF0aW9uVHlwZS5DcmVhdGVkKSB7XHJcbiAgICAgIGl0ZW0uZGF0YS5PcGVyYXRpb25UeXBlID0gT3BlcmF0aW9uVHlwZS5VcGRhdGVkO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25Tb3J0Q2hhbmdlZChzb3J0TW9kZWw6IGFueSkge1xyXG4gICAgaWYgKHNvcnRNb2RlbCkge1xyXG4gICAgICAvLyBjb25zdCBzb3J0TW9kZWwgPSBfLmhlYWQodGhpcy5ncmlkQXBpLmdldFNvcnRNb2RlbCgpKTtcclxuXHJcbiAgICAgIHRoaXMuY3VycmVudFNvcnRlZENvbElkID0gc29ydE1vZGVsLmNvbElkO1xyXG5cclxuICAgICAgY29uc3QgY29sdW1uRGVmID0gdGhpcy5ncmlkQXBpLmdldENvbHVtbkRlZihzb3J0TW9kZWwuY29sSWQpIGFzIEdyaWRDb2x1bW47XHJcbiAgICAgIGxldCBuYW1lOiBzdHJpbmcgPSBjb2x1bW5EZWYuY3VzdG9tRmllbGRGb3JTb3J0O1xyXG5cclxuICAgICAgaWYgKCFuYW1lKSB7XHJcbiAgICAgICAgaWYgKGNvbHVtbkRlZi5maWVsZC5pbmRleE9mKCdJZCcpID4gMCAmJlxyXG4gICAgICAgICAgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IGZhbHNlIHx8IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgIG5hbWUgPSBjb2x1bW5EZWYuZmllbGQuc3Vic3RyKDAsIGNvbHVtbkRlZi5maWVsZC5pbmRleE9mKCdJZCcpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbmFtZSA9IGNvbHVtbkRlZi5maWVsZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IHNvcnQgPSBbe1xyXG4gICAgICAgIFByb3BlcnR5TmFtZTogbmFtZSxcclxuICAgICAgICBUeXBlOiBzb3J0TW9kZWwuc29ydCA9PT0gJ2FzYycgPyBTb3J0VHlwZS5Bc2MgOiBTb3J0VHlwZS5EZXNjXHJcbiAgICAgIH1dO1xyXG5cclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuU29ydCA9IHNvcnQ7XHJcblxyXG4gICAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkZpbHRlckNoYW5nZWQoZXZlbnQ6IGFueSkge1xyXG4gICAgLy8gdGhpcy5maWx0ZXIgPSBPYmplY3QuZW50cmllcyh0aGlzLmdyaWRBcGkuZ2V0RmlsdGVyTW9kZWwoKSkubWFwKChba2V5LCB2YWx1ZV0pID0+ICh7IGtleSwgdmFsdWUgfSkpO1xyXG4gICAgdGhpcy5sb2FkR3JpZFZpZXdEYXRhQnlPcHRpb25zVXJsQW5kRmlsdGVycygpO1xyXG4gIH1cclxuXHJcbiAgYXBwbHlDaGFuZ2VzVG9GaWx0ZXIoZmllbGQ6IHN0cmluZywgbW9kZWw6IGFueSkge1xyXG4gICAgdGhpcy5hZGRUb0xpc3RGb3JGaWx0ZXIodGhpcy5maWx0ZXIsXHJcbiAgICAgIG1vZGVsLmZpbHRlciB8fCBtb2RlbC5maWx0ZXIgPT09IDAgPyBtb2RlbCA6IG51bGwsXHJcbiAgICAgIGZpZWxkKTtcclxuICB9XHJcblxyXG4gIGFkZFRvTGlzdEZvckZpbHRlcihsaXN0QXJyYXk6IGFueVtdLCB2YWx1ZTogYW55LCBrZXk6IGFueSk6IHZvaWQge1xyXG4gICAgY29uc3QgZm91bmREYXRhSW5kZXg6IG51bWJlciA9IGxpc3RBcnJheS5maW5kSW5kZXgoeCA9PiB4LmtleSA9PT0ga2V5KTtcclxuICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICBjb25zdCBkYXRhVG9TZW5kOiBhbnkgPSB7XHJcbiAgICAgICAga2V5LFxyXG4gICAgICAgIHZhbHVlXHJcbiAgICAgIH07XHJcbiAgICAgIGlmIChmb3VuZERhdGFJbmRleCAhPT0gLTEpIHtcclxuICAgICAgICBsaXN0QXJyYXlbZm91bmREYXRhSW5kZXhdID0gZGF0YVRvU2VuZDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBsaXN0QXJyYXkucHVzaChkYXRhVG9TZW5kKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKGZvdW5kRGF0YUluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgIGxpc3RBcnJheS5zcGxpY2UoZm91bmREYXRhSW5kZXgsIDEpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8gY2FsbGVkIGZyb20gdGV4dCBmaWx0ZXIgZm9yIENvcmVHcmlkQ29sdW1uVHlwZS5UZXh0U2VsZWN0XHJcbiAgdGV4dFNlbGVjdEZpbmRWYWx1ZXNPbkZpbHRlcihmaWVsZDogc3RyaW5nLCBzZXR0aW5nczogU2VsZWN0b3JTZXR0aW5ncywgbW9kZWw6IGFueSk6IHZvaWQge1xyXG4gICAgY29uc3QgZm91bmRDb2x1bW4gPSB0aGlzLmdyaWRPcHRpb25zLmNvbHVtbnMuZmluZCh4ID0+IHguZmllbGQgPT09IGZpZWxkKTtcclxuXHJcbiAgICBpZiAobW9kZWwuZmlsdGVyKSB7XHJcbiAgICAgIGNvbnN0IGZpbHRlcjogR2VuZXJpY0V4cHJlc3Npb24gPSB7XHJcbiAgICAgICAgUGFnZVNpemU6IDEwMCxcclxuICAgICAgICBGaWx0ZXJHcm91cHM6IFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgRmlsdGVyczogW1xyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFByb3BlcnR5TmFtZTogc2V0dGluZ3MubGFiZWxGaWVsZCBhcyBzdHJpbmcsXHJcbiAgICAgICAgICAgICAgICBWYWx1ZTogbW9kZWwuZmlsdGVyLFxyXG4gICAgICAgICAgICAgICAgQ29tcGFyaXNvbjogbW9kZWwudHlwZSxcclxuICAgICAgICAgICAgICAgIENvbm5lY3RvcjogQ29ubmVjdG9yLkFuZFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLmRhdGFTZXJ2aWNlLmdldExpc3RCeShzZXR0aW5ncy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zLm1vZHVsZVVybCArIHNldHRpbmdzLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmwsXHJcbiAgICAgICAgZmlsdGVyKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKGh0dHBSZXNwb25zZTogQ29yZUh0dHBSZXNwb25zZTxhbnlbXT4pID0+IHtcclxuICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGh0dHBSZXNwb25zZS5zZXJ2aWNlUmVzdWx0LlJlc3VsdCB8fCBbXTtcclxuICAgICAgICAgIGNvbnN0IGlkTGlzdCA9IHJlc3VsdC5tYXAoeCA9PiB4W3NldHRpbmdzLnZhbHVlRmllbGRdKS5maWx0ZXIodGhpcy5vbmx5VW5pcXVlKTtcclxuICAgICAgICAgIGNvbnN0IG5ld0ZpbHRlcnM6IEZpbHRlcltdID0gW107XHJcblxyXG4gICAgICAgICAgbGV0IG5hbWUgPSAnJztcclxuICAgICAgICAgIGlmIChmb3VuZENvbHVtbikge1xyXG4gICAgICAgICAgICBuYW1lID0gZm91bmRDb2x1bW4uY3VzdG9tRmllbGRGb3JGaWx0ZXI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoIW5hbWUpIHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkLmluZGV4T2YoJ0lkJykgPiAwICYmXHJcbiAgICAgICAgICAgICAgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IGZhbHNlIHx8IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgICAgICBuYW1lID0gZmllbGQuc3Vic3RyKDAsIGZpZWxkLmluZGV4T2YoJ0lkJykpICtcclxuICAgICAgICAgICAgICAgICcuJyArIGZpZWxkLnN1YnN0cihmaWVsZC5pbmRleE9mKCdJZCcpLCBmaWVsZC5sZW5ndGgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIG5hbWUgPSBmaWVsZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlkTGlzdC5mb3JFYWNoKChpZDogbnVtYmVyKSA9PiB7XHJcbiAgICAgICAgICAgIG5ld0ZpbHRlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgUHJvcGVydHlOYW1lOiBuYW1lLFxyXG4gICAgICAgICAgICAgIFZhbHVlOiBpZCxcclxuICAgICAgICAgICAgICBDb21wYXJpc29uOiBDb21wYXJpc29uLkVxdWFsVG8sXHJcbiAgICAgICAgICAgICAgQ29ubmVjdG9yOiBDb25uZWN0b3IuT3JcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICBpZiAobmV3RmlsdGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIG5ld0ZpbHRlcnNbbmV3RmlsdGVycy5sZW5ndGggLSAxXS5Db25uZWN0b3IgPSBDb25uZWN0b3IuQW5kO1xyXG4gICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICBjb25zdCBmaWx0ZXJHcm91cDogRmlsdGVyR3JvdXAgPSB7XHJcbiAgICAgICAgICAgIENvbm5lY3RvcjogQ29ubmVjdG9yLkFuZCxcclxuICAgICAgICAgICAgRmlsdGVyczogbmV3RmlsdGVyc1xyXG4gICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICBpZiAobmV3RmlsdGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkVG9MaXN0Rm9yRmlsdGVyKHRoaXMuZmlsdGVyR3JvdXBMaXN0LCBmaWx0ZXJHcm91cCwgZmllbGQpO1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBncmlkaSBib8WfYWxtYWsgbGF6xLFtXHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50ID0gMDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFkZFRvTGlzdEZvckZpbHRlcih0aGlzLmZpbHRlckdyb3VwTGlzdCwgbnVsbCwgZmllbGQpO1xyXG4gICAgICB0aGlzLmxvYWRHcmlkVmlld0RhdGFCeU9wdGlvbnNVcmxBbmRGaWx0ZXJzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbmx5VW5pcXVlKHZhbHVlLCBpbmRleCwgc2VsZikge1xyXG4gICAgcmV0dXJuIHNlbGYuaW5kZXhPZih2YWx1ZSkgPT09IGluZGV4O1xyXG4gIH1cclxuXHJcbiAgb25QYWdlQ2hhbmdlZChldmVudDogYW55KSB7XHJcbiAgICB0aGlzLnBhZ2luZ1Jlc3VsdC5DdXJyZW50UGFnZSA9IGV2ZW50LnBhZ2U7XHJcbiAgICB0aGlzLnBhZ2luZ1Jlc3VsdC5QYWdlU2l6ZSA9IGV2ZW50Lml0ZW1zUGVyUGFnZTtcclxuICAgIHRoaXMubG9hZEdyaWRWaWV3RGF0YUJ5T3B0aW9uc1VybEFuZEZpbHRlcnMoKTtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlSXRlbXNQZXJQYWdlKHBhZ2VTaXplRXZlbnQ6IGFueSkge1xyXG4gICAgaWYgKHBhZ2VTaXplRXZlbnQuSWQpIHtcclxuICAgICAgdGhpcy5wYWdpbmdSZXN1bHQuUGFnZVNpemUgPSBwYWdlU2l6ZUV2ZW50LklkO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucGFnaW5nUmVzdWx0LlRvdGFsQ291bnQgPiAwKSB7XHJcbiAgICAgIHRoaXMubG9hZEdyaWRWaWV3RGF0YUJ5T3B0aW9uc1VybEFuZEZpbHRlcnMoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldEJ1bGtPcGVyYXRpb25EYXRhKCk6IGFueSB7XHJcbiAgICBjb25zdCByZXN1bHREYXRhID0gW107XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlKSB7XHJcbiAgICAgIHRoaXMudmFsaWRhdGVGb3JtKCk7XHJcblxyXG4gICAgICB0aGlzLmRlbGV0ZWREYXRhXHJcbiAgICAgICAgLmZvckVhY2goKG9iaikgPT4ge1xyXG4gICAgICAgICAgcmVzdWx0RGF0YS5wdXNoKG9iaik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICB0aGlzLmRhdGEuZm9yRWFjaChvYmogPT4ge1xyXG4gICAgICAgIGlmIChvYmouT3BlcmF0aW9uVHlwZSAhPT0gT3BlcmF0aW9uVHlwZS5Ob25lKSB7XHJcbiAgICAgICAgICByZXN1bHREYXRhLnB1c2gob2JqKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlc3VsdERhdGE7XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZvcm0oZm9ybUdyb3VwOiBGb3JtR3JvdXAgPSB0aGlzLmdyaWRGb3JtKTogYm9vbGVhbiB7XHJcbiAgICBPYmplY3Qua2V5cyhmb3JtR3JvdXAuY29udHJvbHMpLmZvckVhY2goZmllbGQgPT4ge1xyXG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybUdyb3VwLmdldChmaWVsZCk7XHJcbiAgICAgIGlmIChjb250cm9sIGluc3RhbmNlb2YgRm9ybUNvbnRyb2wpIHtcclxuICAgICAgICBjb250cm9sLm1hcmtBc1RvdWNoZWQoeyBvbmx5U2VsZjogdHJ1ZSB9KTtcclxuICAgICAgfSBlbHNlIGlmIChjb250cm9sIGluc3RhbmNlb2YgRm9ybUdyb3VwKSB7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0ZUZvcm0oY29udHJvbCk7XHJcbiAgICAgIH0gZWxzZSBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1BcnJheSkge1xyXG4gICAgICAgIE9iamVjdC5rZXlzKGNvbnRyb2wuY29udHJvbHMpLmZvckVhY2goYyA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjdHJsID0gY29udHJvbC5nZXQoYyk7XHJcbiAgICAgICAgICBjdHJsLm1hcmtBc1RvdWNoZWQoeyBvbmx5U2VsZjogdHJ1ZSB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gZm9ybUdyb3VwLnZhbGlkO1xyXG4gIH1cclxuXHJcbiAgcmV2ZXJ0RGF0YSgpIHtcclxuICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVuZG8pXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICBpZiAocmVzICYmIHJlcy52YWx1ZSkge1xyXG4gICAgICAgICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLmRlbGV0ZWREYXRhID0gW107XHJcbiAgICAgICAgICB0aGlzLmRhdGEgPSB0aGlzLm9yaWdpbmFsUm93RGF0YS5tYXAoaXRlbSA9PiBPYmplY3QuYXNzaWduKHt9LCBpdGVtKSk7XHJcbiAgICAgICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLmRhdGEpO1xyXG4gICAgICAgICAgdGhpcy50b2FzdHIuc3VjY2VzcyhUb2FzdHJNZXNzYWdlcy5SZWNvcmRSZXZlcnRlZCk7XHJcbiAgICAgICAgICB0aGlzLnJlY29yZEV2ZW50c1N1YmplY3QubmV4dCh7IHR5cGU6ICdSZXZlcnRlZEdyaWQnLCByZWNvcmQ6IHRoaXMuZGF0YSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWRkUm93KCkge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGUpIHtcclxuICAgICAgY29uc3QgYWRkZWRPYmplY3QgPSB7IElkOiAtMSwgT3BlcmF0aW9uVHlwZTogT3BlcmF0aW9uVHlwZS5DcmVhdGVkIH07XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMuY29sdW1ucy5maWx0ZXIoeCA9PiB4LmZpZWxkICE9PSAnSWQnKS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgIGFkZGVkT2JqZWN0W2VsZW1lbnQuZmllbGRdID0gZWxlbWVudC5kZWZhdWx0VmFsdWU7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgdGhpcy5kYXRhLnVuc2hpZnQoYWRkZWRPYmplY3QpO1xyXG5cclxuICAgICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gdHJ1ZTtcclxuXHJcbiAgICAgIC8vIHRoaXMuZ3JpZEFwaS51cGRhdGVSb3dEYXRhKHtcclxuICAgICAgLy8gICBhZGQ6IFthZGRlZE9iamVjdF0sXHJcbiAgICAgIC8vICAgYWRkSW5kZXg6IDBcclxuICAgICAgLy8gfSk7XHJcblxyXG4gICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLmRhdGEpO1xyXG5cclxuICAgICAgdGhpcy5yZWZyZXNoRm9ybUNvbnRyb2xzKCk7XHJcblxyXG4gICAgICB0aGlzLnJlY29yZEV2ZW50c1N1YmplY3QubmV4dCh7IHR5cGU6ICdBZGQnLCByZWNvcmQ6IGFkZGVkT2JqZWN0IH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5hZGRVcmwpIHtcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLmdyaWRPcHRpb25zLmFkZFVybF0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ25ldyddLCB7IHJlbGF0aXZlVG86IHRoaXMucm91dGUgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGVkaXRSZWNvcmQocGFyYW1zLCByb3dJZCwga2V5KSB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5lZGl0VXJsQmxhbmspIHtcclxuICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybCkge1xyXG4gICAgICAgIHdpbmRvdy5vcGVuKHdpbmRvdy5sb2NhdGlvbi5vcmlnaW4gKyAnLycgKyB0aGlzLmdyaWRPcHRpb25zLmVkaXRVcmwgKyAnLycgKyBwYXJhbXMuZGF0YVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgd2luZG93Lm9wZW4odGhpcy5yb3V0ZXIudXJsICsgJy8nICsgJ2VkaXQvJyArIHBhcmFtcy5kYXRhW3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5lZGl0VXJsKSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5ncmlkT3B0aW9ucy5lZGl0VXJsLCBwYXJhbXMuZGF0YVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXV0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2VkaXQnLCBwYXJhbXMuZGF0YVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXV0sIHsgcmVsYXRpdmVUbzogdGhpcy5yb3V0ZSB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoaXMuaGFuZGxlQnJlYWRjcnVtYkxhYmVsKHBhcmFtcy5kYXRhKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBoYW5kbGVCcmVhZGNydW1iTGFiZWwocm93RGF0YTogYW55KTogdm9pZCB7XHJcbiAgLy8gICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJlxyXG4gIC8vICAgICB0aGlzLmdyaWRPcHRpb25zLmJyZWFkY3J1bWJMYWJlbCAmJlxyXG4gIC8vICAgICByb3dEYXRhICYmXHJcbiAgLy8gICAgIHRoaXMuZ3JpZE9wdGlvbnMuYnJlYWRjcnVtYkxhYmVsICYmXHJcbiAgLy8gICAgIHRoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGQgJiZcclxuICAvLyAgICAgcm93RGF0YVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSkge1xyXG5cclxuICAvLyAgICAgbGV0IGVkaXRVcmw6IHN0cmluZztcclxuICAvLyAgICAgY29uc3Qga2V5RmllbGRWYWx1ZSA9IHJvd0RhdGFbdGhpcy5ncmlkT3B0aW9ucy5rZXlGaWVsZF07XHJcblxyXG4gIC8vICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5lZGl0VXJsKSB7XHJcbiAgLy8gICAgICAgZWRpdFVybCA9IHRoaXMuZ3JpZE9wdGlvbnMuZWRpdFVybCArICcvJyArIGtleUZpZWxkVmFsdWU7XHJcbiAgLy8gICAgIH0gZWxzZSB7XHJcbiAgLy8gICAgICAgZWRpdFVybCA9IHRoaXMucm91dGVyLnVybCArICcvZWRpdC8nICsga2V5RmllbGRWYWx1ZTtcclxuICAvLyAgICAgfVxyXG5cclxuXHJcbiAgLy8gICAgIGxldCBjdXN0b21MYWJlbCA9ICcnO1xyXG5cclxuICAvLyAgICAgdGhpcy5ncmlkT3B0aW9ucy5icmVhZGNydW1iTGFiZWwuZm9yRWFjaCgoY29sdW1uRmllbGQ6IHN0cmluZykgPT4ge1xyXG4gIC8vICAgICAgIGNvbnN0IGZvdW5kQ29sdW1uID0gdGhpcy5hbGxHcmlkQ29sdW1ucy5maW5kKHggPT4geC5jb2xJZCA9PT0gY29sdW1uRmllbGQpO1xyXG5cclxuICAvLyAgICAgICBpZiAoZm91bmRDb2x1bW4gJiYgZm91bmRDb2x1bW4uY29sRGVmKSB7XHJcblxyXG4gIC8vICAgICAgICAgaWYgKGZvdW5kQ29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zICYmXHJcbiAgLy8gICAgICAgICAgIGZvdW5kQ29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UgJiZcclxuICAvLyAgICAgICAgICAgZm91bmRDb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAvLyAgICAgICAgICAgY29uc3QgbGFiZWxGaWVsZCA9IGZvdW5kQ29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmxhYmVsRmllbGQ7XHJcbiAgLy8gICAgICAgICAgIGNvbnN0IHZhbHVlRmlsZWQgPSBmb3VuZENvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy52YWx1ZUZpZWxkO1xyXG5cclxuICAvLyAgICAgICAgICAgY29uc3QgZm91bmREYXRhID0gZm91bmRDb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZS5maW5kKHkgPT4geVt2YWx1ZUZpbGVkXSA9PT0gcm93RGF0YVtjb2x1bW5GaWVsZF0pO1xyXG4gIC8vICAgICAgICAgICBjdXN0b21MYWJlbCArPSBmb3VuZERhdGFbbGFiZWxGaWVsZF0gKyAnICc7XHJcblxyXG4gIC8vICAgICAgICAgfSBlbHNlIHtcclxuICAvLyAgICAgICAgICAgY3VzdG9tTGFiZWwgKz0gcm93RGF0YVtjb2x1bW5GaWVsZF0gKyAnICc7XHJcbiAgLy8gICAgICAgICB9XHJcbiAgLy8gICAgICAgfVxyXG4gIC8vICAgICB9KTtcclxuXHJcbiAgLy8gICAgIHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UuYWRkQ3VzdG9tTGFiZWwoa2V5RmllbGRWYWx1ZS50b1N0cmluZygpLCBlZGl0VXJsLCBjdXN0b21MYWJlbCk7XHJcbiAgLy8gICB9XHJcbiAgLy8gfVxyXG5cclxuICB1bmRvUmVjb3JkKHBhcmFtcywgcm93SWQsIGtleSkge1xyXG4gICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuVW5kbylcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICBjb25zdCByZXZlcnRlZE9iamVjdCA9IHRoaXMuZGF0YVtyb3dJZF07XHJcbiAgICAgICAgICBjb25zdCBvcmdpbmFsT2JqZWN0ID0gdGhpcy5vcmlnaW5hbFJvd0RhdGFcclxuICAgICAgICAgICAgLmZpbmQoeCA9PiByZXZlcnRlZE9iamVjdCAmJiB4W3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdID09PSByZXZlcnRlZE9iamVjdFt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSk7XHJcblxyXG4gICAgICAgICAgdGhpcy5kYXRhW3Jvd0lkXSA9IG9yZ2luYWxPYmplY3QgPyBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KG9yZ2luYWxPYmplY3QpKSA6IG51bGw7XHJcbiAgICAgICAgICBwYXJhbXMubm9kZS5kYXRhID0gdGhpcy5kYXRhW3Jvd0lkXTtcclxuICAgICAgICAgIHRoaXMuZ3JpZEFwaS5yZWRyYXdSb3dzKHsgcm93Tm9kZXM6IFtwYXJhbXMubm9kZV0gfSk7XHJcbiAgICAgICAgICB0aGlzLmdyaWRBcGkucmVmcmVzaENlbGxzKHsgcm93Tm9kZXM6IFtwYXJhbXMubm9kZV0sIGZvcmNlOiB0cnVlIH0pO1xyXG4gICAgICAgICAgcGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3Jvd0lkXS5tYXJrQXNQcmlzdGluZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgZGVsZXRlUmVjb3JkKHBhcmFtcywgcm93SWQsIGtleSkge1xyXG4gICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuRGVsZXRlKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgIGNvbnN0IGRlbGV0ZVVybCA9IHRoaXMuZ3JpZE9wdGlvbnMuZGVsZXRlRW5kUG9pbnRVcmwgPyB0aGlzLmdyaWRPcHRpb25zLmRlbGV0ZUVuZFBvaW50VXJsIDogdGhpcy51cmxPcHRpb25zLmVuZFBvaW50VXJsO1xyXG4gICAgICAgICAgY29uc3QgcmVxVXJsID0gYCR7dGhpcy51cmxPcHRpb25zLm1vZHVsZVVybH0ke2RlbGV0ZVVybH1gO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU2VydmljZS5kZWxldGUodGhpcy5kYXRhW3Jvd0lkXVt0aGlzLmdyaWRPcHRpb25zLmtleUZpZWxkXSwgcmVxVXJsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChyZXM6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMucmVjb3JkRXZlbnRzU3ViamVjdC5uZXh0KHsgdHlwZTogJ0RlbGV0ZScsIHJlY29yZDogdGhpcy5kYXRhW3Jvd0lkXSB9KTtcclxuXHJcbiAgICAgICAgICAgICAgLy8gY29uc3QgaW5kZXhPZkRhdGEgPSB0aGlzLmRhdGEuaW5kZXhPZihwYXJhbXMuZGF0YSk7XHJcblxyXG4gICAgICAgICAgICAgIGlmICh0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50ID4gMTApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaEFuZFJldmVydENoYW5nZXMoKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhLnNwbGljZShyb3dJZCwgMSk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wYWdpbmdSZXN1bHQuVG90YWxDb3VudCAmJiB0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50LS07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLmRhdGEpO1xyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgdGhpcy50b2FzdHIuc3VjY2VzcyhUb2FzdHJNZXNzYWdlcy5SZWNvcmREZWxldGVkKTtcclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnN3ZWV0QWxlcnRTZXJ2aWNlLmNvbmZpcm1PcGVyYXRpb25Qb3B1cChyZXN1bHQpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYWxsVW5kb0NvbmZpcm1BbGVydCgpOiBPYnNlcnZhYmxlPEFsZXJ0UmVzdWx0PiB7XHJcbiAgICBjb25zdCBidWxrRGF0YSA9IHRoaXMuZ2V0QnVsa09wZXJhdGlvbkRhdGEoKTtcclxuICAgIGlmIChidWxrRGF0YSAmJiBidWxrRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnN3ZWV0QWxlcnRTZXJ2aWNlLm1hbmFnZVJlcXVlc3RPcGVyYXRpb25XaXRoQ29uZmlybShDb25maXJtRGlhbG9nT3BlcmF0aW9uVHlwZS5VbmRvKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBvZih7IHZhbHVlOiB0cnVlIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlUm93KHBhcmFtcywgcm93SWQsIGtleSkge1xyXG4gICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuRGVsZXRlKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXM6IEFsZXJ0UmVzdWx0KSA9PiB7XHJcbiAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgIC8vIHRoaXMuZGF0YVtyb3dJZF0gPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLm9yaWdpbmFsUm93RGF0YVtyb3dJZF0pO1xyXG4gICAgICAgICAgY29uc3QgZGVsZXRlZE9iamVjdCA9IHRoaXMuZGF0YVtyb3dJZF07XHJcbiAgICAgICAgICB0aGlzLmRhdGEuc3BsaWNlKHJvd0lkLCAxKTtcclxuXHJcbiAgICAgICAgICB0aGlzLnJlY29yZEV2ZW50c1N1YmplY3QubmV4dCh7IHR5cGU6ICdEZWxldGUnLCByZWNvcmQ6IGRlbGV0ZWRPYmplY3QgfSk7XHJcblxyXG4gICAgICAgICAgaWYgKHBhcmFtcy5kYXRhLk9wZXJhdGlvblR5cGUgIT09IE9wZXJhdGlvblR5cGUuQ3JlYXRlZCkge1xyXG4gICAgICAgICAgICBjb25zdCBvcmdpbmFsRGVsZXRlZE9iamVjdCA9IHRoaXMub3JpZ2luYWxSb3dEYXRhXHJcbiAgICAgICAgICAgICAgLmZpbmQoeCA9PiB4W3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdID09PSBkZWxldGVkT2JqZWN0W3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChvcmdpbmFsRGVsZXRlZE9iamVjdCkge1xyXG4gICAgICAgICAgICAgIG9yZ2luYWxEZWxldGVkT2JqZWN0WydPcGVyYXRpb25UeXBlJ10gPSBPcGVyYXRpb25UeXBlLkRlbGV0ZWQ7XHJcbiAgICAgICAgICAgICAgdGhpcy5kZWxldGVkRGF0YS5wdXNoKG9yZ2luYWxEZWxldGVkT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBkZWxldGVkT2JqZWN0WydPcGVyYXRpb25UeXBlJ10gPSBPcGVyYXRpb25UeXBlLkRlbGV0ZWQ7XHJcbiAgICAgICAgICAgICAgdGhpcy5kZWxldGVkRGF0YS5wdXNoKGRlbGV0ZWRPYmplY3QpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gdHJ1ZTtcclxuICAgICAgICAgIHRoaXMuZ3JpZEFwaS5zZXRSb3dEYXRhKHRoaXMuZGF0YSk7XHJcblxyXG4gICAgICAgICAgLy8gaWYgKCF0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlKSB7XHJcbiAgICAgICAgICAvLyAgIHRoaXMudG9hc3RyLnN1Y2Nlc3MoVG9hc3RyTWVzc2FnZXMuUmVjb3JkRGVsZXRlZCk7XHJcbiAgICAgICAgICAvLyB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hBbmRSZXZlcnRDaGFuZ2VzKCkge1xyXG4gICAgdGhpcy5kZWxldGVkRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5pc0RhdGFDaGFuZ2VkID0gZmFsc2U7XHJcbiAgICB0aGlzLnJlZnJlc2hHcmlkRGF0YSgpO1xyXG4gIH1cclxuXHJcbiAgaXNDaGFuZ2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaXNEYXRhQ2hhbmdlZDtcclxuICB9XHJcblxyXG4gIGdldFRvdGFsQ291bnQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLnBhZ2luZ1Jlc3VsdC5Ub3RhbENvdW50IC0gdGhpcy5kZWxldGVkRGF0YS5sZW5ndGg7XHJcbiAgfVxyXG5cclxuICBnZXROb1Jvd3NPdmVybGF5Q29tcG9uZW50KCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gJ2N1c3RvbU5vUm93c092ZXJsYXknO1xyXG4gIH1cclxuXHJcbiAgZ2V0TG9hZGluZ092ZXJsYXlDb21wb25lbnQoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiAnY3VzdG9tTG9hZGluZ092ZXJsYXknO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2VsZWN0ZWRSb3dzKCk6IGFueVtdIHtcclxuICAgIHJldHVybiB0aGlzLmdyaWRBcGkuZ2V0U2VsZWN0ZWRSb3dzKCk7XHJcbiAgfVxyXG5cclxuICBsb2FkaW5nT3ZlcmxheSgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmdyaWRBcGkpIHtcclxuICAgICAgaWYgKHRoaXMubG9hZGluZ0Rpc3BsYXlTdGF0dXMpIHtcclxuICAgICAgICB0aGlzLmdyaWRBcGkuc2hvd0xvYWRpbmdPdmVybGF5KCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ncmlkQXBpLmhpZGVPdmVybGF5KCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLmRhdGEgfHwgKHRoaXMuZGF0YSAmJiB0aGlzLmRhdGEubGVuZ3RoID09PSAwKSkge1xyXG4gICAgICAgICAgdGhpcy5ncmlkQXBpLnNob3dOb1Jvd3NPdmVybGF5KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nRGlzcGxheVN0YXR1c1N1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmxvYWRpbmdEaXNwbGF5U3RhdHVzU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==