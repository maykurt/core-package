/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef } from '@angular/core';
import { SweetAlertService } from '../../../services/custom/sweet.alert.confirm.service';
import { ConfirmDialogOperationType } from '../../../enums/confirm-dialog-operation-type.enum';
/**
 * @record
 */
function MyParams() { }
if (false) {
    /** @type {?} */
    MyParams.prototype.menuIcon;
}
var ColumnHeaderComponent = /** @class */ (function () {
    function ColumnHeaderComponent(elementRef, sweetAlertService) {
        this.sweetAlertService = sweetAlertService;
        this.elementRef = elementRef;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    ColumnHeaderComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.coreGridService = this.params.context.componentParent;
        this.coldId = this.params.column.getColId();
        this.params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        this.onSortChanged();
    };
    /**
     * @return {?}
     */
    ColumnHeaderComponent.prototype.onMenuClick = /**
     * @return {?}
     */
    function () {
        this.params.showColumnMenu(this.querySelector('.customHeaderMenuButton'));
    };
    /**
     * @param {?} order
     * @param {?} event
     * @return {?}
     */
    ColumnHeaderComponent.prototype.onSortRequested = /**
     * @param {?} order
     * @param {?} event
     * @return {?}
     */
    function (order, event) {
        this.params.setSort(order, event.shiftKey);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ColumnHeaderComponent.prototype.onClickHeader = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        if (this.params.enableSorting) {
            /** @type {?} */
            var coreGridService = this.params.context.componentParent;
            /** @type {?} */
            var bulkData = coreGridService.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Cancel)
                    .subscribe(function (res) {
                    if (res && res.value) {
                        _this.setSort(event);
                    }
                });
            }
            else {
                this.setSort(event);
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ColumnHeaderComponent.prototype.setSort = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.sorted === '') {
            this.sorted = 'desc';
        }
        else if (this.sorted === 'desc') {
            this.sorted = 'asc';
        }
        else if (this.sorted === 'asc') {
            this.sorted = 'desc';
        }
        /** @type {?} */
        var sortModel = {
            colId: this.coldId,
            sort: this.sorted
        };
        this.coreGridService.onSortChanged(sortModel);
        // this.params.setSort(this.sorted, event.shiftKey);
    };
    /**
     * @return {?}
     */
    ColumnHeaderComponent.prototype.onSortChanged = /**
     * @return {?}
     */
    function () {
        if (this.params.column.isSortAscending()) {
            this.sorted = 'asc';
        }
        else if (this.params.column.isSortDescending()) {
            this.sorted = 'desc';
        }
        else {
            this.sorted = '';
        }
    };
    /**
     * @return {?}
     */
    ColumnHeaderComponent.prototype.isColumnSorted = /**
     * @return {?}
     */
    function () {
        return this.coreGridService.currentSortedColId === this.coldId;
    };
    /**
     * @private
     * @param {?} selector
     * @return {?}
     */
    ColumnHeaderComponent.prototype.querySelector = /**
     * @private
     * @param {?} selector
     * @return {?}
     */
    function (selector) {
        return (/** @type {?} */ (this.elementRef.nativeElement.querySelector('.customHeaderMenuButton', selector)));
    };
    ColumnHeaderComponent.decorators = [
        { type: Component, args: [{
                    template: "<!-- <div  (click) = \"onClickHeader($event)\" style= \"height: 100%; width: 100%;\">\r\n    <div class=\"cutomLabelContainer\">\r\n        <div class=\"customHeaderLabel\">\r\n            {{params.displayName | translate}}\r\n        </div>\r\n\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'desc'\" class=\"{{'customSortDownLabel'+ (this.sorted === 'desc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('desc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-down\"></i>\r\n        </div>\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'asc'\"  class=\"{{'customSortUpLabel'+ (this.sorted === 'asc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('asc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-up\"></i>\r\n        </div>\r\n    </div>\r\n\r\n    <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->\r\n\r\n<div class=\"ag-cell-label-container\" role=\"presentation\">\r\n    <div class=\"ag-header-cell-label\" role=\"presentation\" (click)=\"onClickHeader($event); $event.stopPropagation();\">\r\n        <span class=\"ag-header-cell-text\" role=\"columnheader\">{{params.displayName | translate}}</span>\r\n        <!-- <span ref=\"eFilter\" class=\"ag-header-icon ag-filter-icon\"></span> -->\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'asc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-ascending-icon sort-icons\"></span>\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'desc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-descending-icon sort-icons\"></span>\r\n        <!-- <span ref=\"eSortNone\" class=\"ag-header-icon ag-sort-none-icon\" ></span> -->\r\n    </div>\r\n    <!-- <span [hidden]=\"!params.enableMenu\"  ref=\"eMenu\" class=\"ag-header-icon ag-header-cell-menu-button\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <span class=\"ag-icon ag-icon-menu\"></span>\r\n    </span> -->\r\n    <!-- <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->",
                    styles: [".customHeaderMenuButton{position:absolute;right:15px;top:0}.customSortDownLabel{float:left;margin-left:7px;margin-top:1px}.customSortUpLabel{float:left;margin-left:7px}.customSortRemoveLabel{float:left;font-size:11px;margin-left:3px}.cutomLabelContainer{width:calc(100% - 30px);height:100%;overflow:hidden}.customHeaderLabel{margin-left:5px;float:left}.active{color:#6495ed}.sort-icons{margin-top:8px!important}"]
                }] }
    ];
    /** @nocollapse */
    ColumnHeaderComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: SweetAlertService }
    ]; };
    return ColumnHeaderComponent;
}());
export { ColumnHeaderComponent };
if (false) {
    /** @type {?} */
    ColumnHeaderComponent.prototype.params;
    /** @type {?} */
    ColumnHeaderComponent.prototype.sorted;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.coreGridService;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.coldId;
    /**
     * @type {?}
     * @private
     */
    ColumnHeaderComponent.prototype.sweetAlertService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWhlYWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY29sdW1uLWhlYWRlci9jb2x1bW4taGVhZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLdEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDekYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sbURBQW1ELENBQUM7Ozs7QUFFL0YsdUJBRUM7OztJQURDLDRCQUFpQjs7QUFHbkI7SUFXRSwrQkFBWSxVQUFzQixFQUFVLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQzlFLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsc0NBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUM7UUFDM0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUU1QyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELDJDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO0lBQzVFLENBQUM7Ozs7OztJQUVELCtDQUFlOzs7OztJQUFmLFVBQWdCLEtBQUssRUFBRSxLQUFLO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFRCw2Q0FBYTs7OztJQUFiLFVBQWMsS0FBSztRQUFuQixpQkFnQkM7UUFmQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFOztnQkFDdkIsZUFBZSxHQUFvQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlOztnQkFDdEUsUUFBUSxHQUFHLGVBQWUsQ0FBQyxvQkFBb0IsRUFBRTtZQUV2RCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlDQUFpQyxDQUFDLDBCQUEwQixDQUFDLE1BQU0sQ0FBQztxQkFDeEYsU0FBUyxDQUFDLFVBQUMsR0FBUTtvQkFDbEIsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRTt3QkFDcEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDckI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3JCO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELHVDQUFPOzs7O0lBQVAsVUFBUSxLQUFVO1FBRWhCLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7WUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdEI7YUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO2FBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztTQUN0Qjs7WUFFSyxTQUFTLEdBQUc7WUFDaEIsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTTtTQUNsQjtRQUdELElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlDLG9EQUFvRDtJQUN0RCxDQUFDOzs7O0lBRUQsNkNBQWE7OztJQUFiO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNyQjthQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtZQUNoRCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztTQUN0QjthQUFNO1lBQ0wsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7U0FDbEI7SUFDSCxDQUFDOzs7O0lBRUQsOENBQWM7OztJQUFkO1FBQ0UsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixLQUFLLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDakUsQ0FBQzs7Ozs7O0lBRU8sNkNBQWE7Ozs7O0lBQXJCLFVBQXNCLFFBQWdCO1FBQ3BDLE9BQU8sbUJBQUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUNoRCx5QkFBeUIsRUFBRSxRQUFRLENBQUMsRUFBZSxDQUFDO0lBQ3hELENBQUM7O2dCQXhGRixTQUFTLFNBQUM7b0JBQ1QsMjFFQUEyQzs7aUJBRTVDOzs7O2dCQWZtQixVQUFVO2dCQUtyQixpQkFBaUI7O0lBZ0cxQiw0QkFBQztDQUFBLEFBekZELElBeUZDO1NBckZZLHFCQUFxQjs7O0lBQ2hDLHVDQUF3Qjs7SUFDeEIsdUNBQXNCOzs7OztJQUN0QiwyQ0FBK0I7Ozs7O0lBQy9CLGdEQUF5Qzs7Ozs7SUFDekMsdUNBQXVCOzs7OztJQUVhLGtEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJSGVhZGVyUGFyYW1zIH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHknO1xyXG5pbXBvcnQgeyBJSGVhZGVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXIvbWFpbic7XHJcbmltcG9ydCB7IENvcmVHcmlkU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2NvcmUtZ3JpZC5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IFN3ZWV0QWxlcnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvY3VzdG9tL3N3ZWV0LmFsZXJ0LmNvbmZpcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlIH0gZnJvbSAnLi4vLi4vLi4vZW51bXMvY29uZmlybS1kaWFsb2ctb3BlcmF0aW9uLXR5cGUuZW51bSc7XHJcblxyXG5pbnRlcmZhY2UgTXlQYXJhbXMgZXh0ZW5kcyBJSGVhZGVyUGFyYW1zIHtcclxuICBtZW51SWNvbjogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICB0ZW1wbGF0ZVVybDogJ2NvbHVtbi1oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydjb2x1bW4taGVhZGVyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbHVtbkhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIElIZWFkZXJBbmd1bGFyQ29tcCB7XHJcbiAgcHVibGljIHBhcmFtczogTXlQYXJhbXM7XHJcbiAgcHVibGljIHNvcnRlZDogc3RyaW5nO1xyXG4gIHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZjtcclxuICBwcml2YXRlIGNvcmVHcmlkU2VydmljZTogQ29yZUdyaWRTZXJ2aWNlO1xyXG4gIHByaXZhdGUgY29sZElkOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsIHByaXZhdGUgc3dlZXRBbGVydFNlcnZpY2U6IFN3ZWV0QWxlcnRTZXJ2aWNlLCApIHtcclxuICAgIHRoaXMuZWxlbWVudFJlZiA9IGVsZW1lbnRSZWY7XHJcbiAgfVxyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG5cclxuICAgIHRoaXMuY29yZUdyaWRTZXJ2aWNlID0gdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQ7XHJcbiAgICB0aGlzLmNvbGRJZCA9IHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xJZCgpO1xyXG5cclxuICAgIHRoaXMucGFyYW1zLmNvbHVtbi5hZGRFdmVudExpc3RlbmVyKCdzb3J0Q2hhbmdlZCcsIHRoaXMub25Tb3J0Q2hhbmdlZC5iaW5kKHRoaXMpKTtcclxuICAgIHRoaXMub25Tb3J0Q2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgb25NZW51Q2xpY2soKSB7XHJcbiAgICB0aGlzLnBhcmFtcy5zaG93Q29sdW1uTWVudSh0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5jdXN0b21IZWFkZXJNZW51QnV0dG9uJykpO1xyXG4gIH1cclxuXHJcbiAgb25Tb3J0UmVxdWVzdGVkKG9yZGVyLCBldmVudCkge1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0U29ydChvcmRlciwgZXZlbnQuc2hpZnRLZXkpO1xyXG4gIH1cclxuXHJcbiAgb25DbGlja0hlYWRlcihldmVudCkge1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLmVuYWJsZVNvcnRpbmcpIHtcclxuICAgICAgY29uc3QgY29yZUdyaWRTZXJ2aWNlOiBDb3JlR3JpZFNlcnZpY2UgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNvbXBvbmVudFBhcmVudDtcclxuICAgICAgY29uc3QgYnVsa0RhdGEgPSBjb3JlR3JpZFNlcnZpY2UuZ2V0QnVsa09wZXJhdGlvbkRhdGEoKTtcclxuXHJcbiAgICAgIGlmIChidWxrRGF0YSAmJiBidWxrRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuQ2FuY2VsKVxyXG4gICAgICAgICAgLnN1YnNjcmliZSgocmVzOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgICAgICB0aGlzLnNldFNvcnQoZXZlbnQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNldFNvcnQoZXZlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRTb3J0KGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuXHJcbiAgICBpZiAodGhpcy5zb3J0ZWQgPT09ICcnKSB7XHJcbiAgICAgIHRoaXMuc29ydGVkID0gJ2Rlc2MnO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNvcnRlZCA9PT0gJ2Rlc2MnKSB7XHJcbiAgICAgIHRoaXMuc29ydGVkID0gJ2FzYyc7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuc29ydGVkID09PSAnYXNjJykge1xyXG4gICAgICB0aGlzLnNvcnRlZCA9ICdkZXNjJztcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzb3J0TW9kZWwgPSB7XHJcbiAgICAgIGNvbElkOiB0aGlzLmNvbGRJZCxcclxuICAgICAgc29ydDogdGhpcy5zb3J0ZWRcclxuICAgIH07XHJcblxyXG5cclxuICAgIHRoaXMuY29yZUdyaWRTZXJ2aWNlLm9uU29ydENoYW5nZWQoc29ydE1vZGVsKTtcclxuICAgIC8vIHRoaXMucGFyYW1zLnNldFNvcnQodGhpcy5zb3J0ZWQsIGV2ZW50LnNoaWZ0S2V5KTtcclxuICB9XHJcblxyXG4gIG9uU29ydENoYW5nZWQoKSB7XHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sdW1uLmlzU29ydEFzY2VuZGluZygpKSB7XHJcbiAgICAgIHRoaXMuc29ydGVkID0gJ2FzYyc7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMucGFyYW1zLmNvbHVtbi5pc1NvcnREZXNjZW5kaW5nKCkpIHtcclxuICAgICAgdGhpcy5zb3J0ZWQgPSAnZGVzYyc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNvcnRlZCA9ICcnO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaXNDb2x1bW5Tb3J0ZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jb3JlR3JpZFNlcnZpY2UuY3VycmVudFNvcnRlZENvbElkID09PSB0aGlzLmNvbGRJZDtcclxuICB9XHJcbiBcclxuICBwcml2YXRlIHF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3I6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXHJcbiAgICAgICcuY3VzdG9tSGVhZGVyTWVudUJ1dHRvbicsIHNlbGVjdG9yKSBhcyBIVE1MRWxlbWVudDtcclxuICB9XHJcbn1cclxuIl19