/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var SelectCellComponent = /** @class */ (function () {
    function SelectCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    SelectCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(function (x) {
            _this.getListData();
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    SelectCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params
            && this.params.colDef
            && this.params.colDef.cellEditorParams
            && this.params.colDef.cellEditorParams.settings
            && this.params.colDef.cellEditorParams.settings.sameFieldWith) {
            // this.value = this.params.node.data[this.params.colDef.cellEditorParams.settings.sameFieldWith];
            /** @type {?} */
            var fields = this.params.colDef.cellEditorParams.settings.sameFieldWith.split('.');
            /** @type {?} */
            var data_1 = Object.assign({}, this.params.node.data);
            fields.forEach(function (field) {
                data_1 = data_1[field] || null;
            });
            this.value = data_1;
        }
        // this.params.node.data["PagePanel"]["PageId"]
        // 58
        // this.params.node.data["PagePanel.PageId"]
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
        }
        return true;
    };
    /**
     * @return {?}
     */
    SelectCellComponent.prototype.getListData = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.params.colDef.cellEditorParams.dataSource) {
            this.listData = this.params.colDef.cellEditorParams.dataSource;
            // this.clearSubscription();
            /** @type {?} */
            var data_2 = this.listData.find(function (x) { return x[_this.params.colDef.cellEditorParams.settings.valueField] === _this.value; });
            if (data_2) {
                this.definition = '';
                if (this.params.colDef.cellEditorParams.settings.labelField instanceof Array) {
                    /** @type {?} */
                    var labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.labelField));
                    labels.forEach(function (columnName) {
                        _this.definition = _this.definition + ' ' + data_2[columnName];
                    });
                }
                else if (this.params.colDef.cellEditorParams.settings.labelField) {
                    this.definition = data_2[this.params.colDef.cellEditorParams.settings.labelField];
                }
            }
            if (this.definition) {
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.definition);
                }
            }
        }
    };
    /**
     * @return {?}
     */
    SelectCellComponent.prototype.clearSubscription = /**
     * @return {?}
     */
    function () {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    SelectCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.clearSubscription();
    };
    SelectCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-select-cell',
                    template: "\n  <div *ngIf=\"definition\" class=\"grid-none-editable-select-cell user-select-text text-ellipsis\"  title=\"{{definition}}\">\n    {{ definition }}\n  </div>\n  ",
                    styles: ["\n      .grid-none-editable-select-cell {\n        padding-top: 5px;\n        white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n      }\n    "]
                }] }
    ];
    return SelectCellComponent;
}());
export { SelectCellComponent };
if (false) {
    /** @type {?} */
    SelectCellComponent.prototype.listData;
    /** @type {?} */
    SelectCellComponent.prototype.formGroup;
    /** @type {?} */
    SelectCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.params;
    /** @type {?} */
    SelectCellComponent.prototype.value;
    /** @type {?} */
    SelectCellComponent.prototype.definition;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    SelectCellComponent.prototype.dataSubscription;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvcmVhZC1vbmx5L3NlbGVjdC1jZWxsL3NlbGVjdC1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUtyRDtJQUFBO0lBc0dBLENBQUM7Ozs7O0lBMUVDLG9DQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQWxCLGlCQVNDO1FBUkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDMUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFFakMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBQSxDQUFDO1lBQzFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQscUNBQU87Ozs7SUFBUCxVQUFRLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwRSxJQUFJLElBQUksQ0FBQyxNQUFNO2VBQ1YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2VBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQjtlQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRO2VBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUU7OztnQkFFekQsTUFBTSxHQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7Z0JBQzFGLE1BQUksR0FBUSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDeEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQWE7Z0JBQzNCLE1BQUksR0FBRyxNQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFJLENBQUM7U0FHbkI7UUFDRCwrQ0FBK0M7UUFDL0MsS0FBSztRQUNMLDRDQUE0QztRQUU1QyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtRQUFBLGlCQXNCQztRQXJCQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRTtZQUNsRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQzs7O2dCQUV6RCxNQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUksQ0FBQyxLQUFLLEVBQXpFLENBQXlFLENBQUM7WUFDL0csSUFBSSxNQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsWUFBWSxLQUFLLEVBQUU7O3dCQUN0RSxNQUFNLEdBQWEsbUJBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBQTtvQkFDMUYsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQWtCO3dCQUNoQyxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxHQUFHLE1BQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDN0QsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFO29CQUNsRSxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQ2pGO2FBQ0Y7WUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ25CLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQ3pEO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCwrQ0FBaUI7OztJQUFqQjtRQUNFLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyQztJQUNILENBQUM7Ozs7SUFFRCx5Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDOztnQkFyR0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSw4QkFBOEI7b0JBQ3hDLFFBQVEsRUFBRSxzS0FJVDs2QkFFQyx3S0FPQztpQkFFSjs7SUFxRkQsMEJBQUM7Q0FBQSxBQXRHRCxJQXNHQztTQXBGWSxtQkFBbUI7OztJQUM5Qix1Q0FBcUI7O0lBQ3JCLHdDQUE0Qjs7SUFDNUIsa0NBQVc7Ozs7O0lBQ1gscUNBQW9COztJQUNwQixvQ0FBYTs7SUFDYix5Q0FBa0I7Ozs7O0lBQ2xCLG9DQUFtQjs7Ozs7SUFDbkIsK0NBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUFycmF5IH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1zZWxlY3QtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICA8ZGl2ICpuZ0lmPVwiZGVmaW5pdGlvblwiIGNsYXNzPVwiZ3JpZC1ub25lLWVkaXRhYmxlLXNlbGVjdC1jZWxsIHVzZXItc2VsZWN0LXRleHQgdGV4dC1lbGxpcHNpc1wiICB0aXRsZT1cInt7ZGVmaW5pdGlvbn19XCI+XHJcbiAgICB7eyBkZWZpbml0aW9uIH19XHJcbiAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZXM6IFtcclxuICAgIGBcclxuICAgICAgLmdyaWQtbm9uZS1lZGl0YWJsZS1zZWxlY3QtY2VsbCB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICB9XHJcbiAgICBgXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0Q2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCwgT25EZXN0cm95IHtcclxuICBwdWJsaWMgbGlzdERhdGE6IGFueTtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuICBwdWJsaWMgZGVmaW5pdGlvbjtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcbiAgcHJpdmF0ZSBkYXRhU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgcGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWUgPyB0aGlzLnBhcmFtcy52YWx1ZSA6IG51bGw7XHJcbiAgICB0aGlzLnJvd0lkID0gdGhpcy5wYXJhbXMubm9kZS5pZDtcclxuXHJcbiAgICB0aGlzLmRhdGFTdWJzY3JpcHRpb24gPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhLnN1YnNjcmliZSh4ID0+IHtcclxuICAgICAgdGhpcy5nZXRMaXN0RGF0YSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zXHJcbiAgICAgICYmIHRoaXMucGFyYW1zLmNvbERlZlxyXG4gICAgICAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtc1xyXG4gICAgICAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5nc1xyXG4gICAgICAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5zYW1lRmllbGRXaXRoKSB7XHJcbiAgICAgIC8vIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy5ub2RlLmRhdGFbdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3Muc2FtZUZpZWxkV2l0aF07XHJcbiAgICAgIGNvbnN0IGZpZWxkczogc3RyaW5nW10gPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5zYW1lRmllbGRXaXRoLnNwbGl0KCcuJyk7XHJcbiAgICAgIGxldCBkYXRhOiBhbnkgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnBhcmFtcy5ub2RlLmRhdGEpO1xyXG4gICAgICBmaWVsZHMuZm9yRWFjaCgoZmllbGQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGRhdGEgPSBkYXRhW2ZpZWxkXSB8fCBudWxsO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy52YWx1ZSA9IGRhdGE7XHJcblxyXG5cclxuICAgIH1cclxuICAgIC8vIHRoaXMucGFyYW1zLm5vZGUuZGF0YVtcIlBhZ2VQYW5lbFwiXVtcIlBhZ2VJZFwiXVxyXG4gICAgLy8gNThcclxuICAgIC8vIHRoaXMucGFyYW1zLm5vZGUuZGF0YVtcIlBhZ2VQYW5lbC5QYWdlSWRcIl1cclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgdGhpcy5nZXRMaXN0RGF0YSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdERhdGEoKSB7XHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLmxpc3REYXRhID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZTtcclxuICAgICAgLy8gdGhpcy5jbGVhclN1YnNjcmlwdGlvbigpO1xyXG4gICAgICBjb25zdCBkYXRhID0gdGhpcy5saXN0RGF0YS5maW5kKHggPT4geFt0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy52YWx1ZUZpZWxkXSA9PT0gdGhpcy52YWx1ZSk7XHJcbiAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5kZWZpbml0aW9uID0gJyc7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmxhYmVsRmllbGQgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgY29uc3QgbGFiZWxzOiBzdHJpbmdbXSA9IDxzdHJpbmdbXT50aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5sYWJlbEZpZWxkO1xyXG4gICAgICAgICAgbGFiZWxzLmZvckVhY2goKGNvbHVtbk5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRlZmluaXRpb24gPSB0aGlzLmRlZmluaXRpb24gKyAnICcgKyBkYXRhW2NvbHVtbk5hbWVdO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5sYWJlbEZpZWxkKSB7XHJcbiAgICAgICAgICB0aGlzLmRlZmluaXRpb24gPSBkYXRhW3RoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmxhYmVsRmllbGRdO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5kZWZpbml0aW9uKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybUdyb3VwKSB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLmRlZmluaXRpb24pO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY2xlYXJTdWJzY3JpcHRpb24oKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5kYXRhU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMuZGF0YVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsZWFyU3Vic2NyaXB0aW9uKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==