/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var BooleanCellComponent = /** @class */ (function () {
    function BooleanCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    BooleanCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    BooleanCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        this.formGroup.at(this.key).disable({ onlySelf: true });
        this.formGroup.at(this.key).patchValue(this.value);
        return true;
    };
    /**
     * @return {?}
     */
    BooleanCellComponent.prototype.onValueChanged = /**
     * @return {?}
     */
    function () {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    };
    BooleanCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-boolean-cell',
                    template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" class=\"grid-boolean-container\">\n\t\t<layout-form-checkbox [formControl]=\"formGroup.controls[key]\" isReadOnly=\"true\"></layout-form-checkbox>\n    </div>\n  ",
                    styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
                }] }
    ];
    return BooleanCellComponent;
}());
export { BooleanCellComponent };
if (false) {
    /** @type {?} */
    BooleanCellComponent.prototype.formGroup;
    /** @type {?} */
    BooleanCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    BooleanCellComponent.prototype.params;
    /** @type {?} */
    BooleanCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    BooleanCellComponent.prototype.rowId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9ib29sZWFuLWNlbGwvYm9vbGVhbi1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxQztJQUFBO0lBbUNBLENBQUM7Ozs7O0lBbkJDLHFDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsc0NBQU87Ozs7SUFBUCxVQUFRLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsNkNBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOztnQkFsQ0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSwrQkFBK0I7b0JBQ3pDLFFBQVEsRUFBRSw2TkFJVDs7aUJBRUY7O0lBMkJELDJCQUFDO0NBQUEsQUFuQ0QsSUFtQ0M7U0ExQlksb0JBQW9COzs7SUFDL0IseUNBQTRCOztJQUM1QixtQ0FBVzs7Ozs7SUFDWCxzQ0FBb0I7O0lBQ3BCLHFDQUFhOzs7OztJQUNiLHFDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtYm9vbGVhbi1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiAqbmdJZj1cImZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgY2xhc3M9XCJncmlkLWJvb2xlYW4tY29udGFpbmVyXCI+XHJcblx0XHQ8bGF5b3V0LWZvcm0tY2hlY2tib3ggW2Zvcm1Db250cm9sXT1cImZvcm1Hcm91cC5jb250cm9sc1trZXldXCIgaXNSZWFkT25seT1cInRydWVcIj48L2xheW91dC1mb3JtLWNoZWNrYm94PlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9ib29sZWFuLWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQm9vbGVhbkNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIHZhbHVlO1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZTtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmRpc2FibGUoeyBvbmx5U2VsZjogdHJ1ZSB9KTtcclxuICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBvblZhbHVlQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWU7XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICB9XHJcbn1cclxuIl19