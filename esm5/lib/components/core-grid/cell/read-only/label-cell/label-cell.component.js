/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var LabelCellComponent = /** @class */ (function () {
    function LabelCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    LabelCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this._value = this.value;
        this.rowId = this.params.node.id;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    LabelCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.value = '';
                if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                    /** @type {?} */
                    var labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.fields));
                    labels.forEach(function (columnName) {
                        _this.value = _this.value + ' ' + _this.params.node.data[columnName];
                    });
                }
            }
        }
        this._value = this.value;
        if (this.params.colDef.cellEditorParams.customDisplayFunction) {
            this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    LabelCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-label-cell',
                    template: "\n    <div *ngIf=\"value\" class=\"grid-none-editable-label-cell user-select-text text-ellipsis\" title=\"{{_value}}\">\n      {{ _value }}\n    </div>\n  ",
                    styles: ["\n    .grid-none-editable-label-cell {\n      padding-top: 5px;\n      white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    }\n    "]
                }] }
    ];
    return LabelCellComponent;
}());
export { LabelCellComponent };
if (false) {
    /** @type {?} */
    LabelCellComponent.prototype.formGroup;
    /** @type {?} */
    LabelCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    LabelCellComponent.prototype.params;
    /** @type {?} */
    LabelCellComponent.prototype.value;
    /** @type {?} */
    LabelCellComponent.prototype._value;
    /**
     * @type {?}
     * @private
     */
    LabelCellComponent.prototype.rowId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFiZWwtY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9yZWFkLW9ubHkvbGFiZWwtY2VsbC9sYWJlbC1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxQztJQUFBO0lBOERBLENBQUM7Ozs7O0lBcENDLG1DQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELG9DQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQW5CLGlCQTJCQztRQTFCQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxNQUFNLFlBQVksS0FBSyxFQUFFOzt3QkFDbEUsTUFBTSxHQUFhLG1CQUFVLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUE7b0JBQ3RGLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFrQjt3QkFDaEMsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3BFLENBQUMsQ0FBQyxDQUFDO2lCQUNKO2FBQ0Y7U0FDRjtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUV6QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixFQUFFO1lBQzdELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3JGO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOztnQkE3REYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSw2QkFBNkI7b0JBQ3ZDLFFBQVEsRUFBRSw2SkFJVDs2QkFFQywrSkFPQztpQkFFSjs7SUE2Q0QseUJBQUM7Q0FBQSxBQTlERCxJQThEQztTQTVDWSxrQkFBa0I7OztJQUM3Qix1Q0FBNEI7O0lBQzVCLGlDQUFXOzs7OztJQUNYLG9DQUFvQjs7SUFDcEIsbUNBQWE7O0lBQ2Isb0NBQWM7Ozs7O0lBQ2QsbUNBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1sYWJlbC1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiAqbmdJZj1cInZhbHVlXCIgY2xhc3M9XCJncmlkLW5vbmUtZWRpdGFibGUtbGFiZWwtY2VsbCB1c2VyLXNlbGVjdC10ZXh0IHRleHQtZWxsaXBzaXNcIiB0aXRsZT1cInt7X3ZhbHVlfX1cIj5cclxuICAgICAge3sgX3ZhbHVlIH19XHJcbiAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgLmdyaWQtbm9uZS1lZGl0YWJsZS1sYWJlbC1jZWxsIHtcclxuICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgIH1cclxuICAgIGBcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMYWJlbENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIHZhbHVlO1xyXG4gIHB1YmxpYyBfdmFsdWU7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlO1xyXG4gICAgdGhpcy5fdmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSAnJztcclxuICAgICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgICAgIGNvbnN0IGxhYmVsczogc3RyaW5nW10gPSA8c3RyaW5nW10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzO1xyXG4gICAgICAgICAgbGFiZWxzLmZvckVhY2goKGNvbHVtbk5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy52YWx1ZSArICcgJyArIHRoaXMucGFyYW1zLm5vZGUuZGF0YVtjb2x1bW5OYW1lXTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuX3ZhbHVlID0gdGhpcy52YWx1ZTtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRGlzcGxheUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuX3ZhbHVlID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRGlzcGxheUZ1bmN0aW9uKHRoaXMudmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbn1cclxuIl19