/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var MaskCellComponent = /** @class */ (function () {
    function MaskCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    MaskCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MaskCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.maskType = this.settings.maskType;
            }
            else {
                console.error(this.params.colDef.field + ' settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' settings is not defined');
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    MaskCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-mask-cell',
                    template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\">\n      <layout-form-mask-input\n        class=\"coreGridLabelCellDisable align-middle\"\n        [formControlName]=\"key\"\n        [maskType]=\"maskType\"\n        [isReadOnly]=\"true\">\n      </layout-form-mask-input>\n    </div>\n  ",
                    styles: ["\n      .coreGridLabelCellDisable /deep/ input {\n        background: rgba(0, 0, 0, 0);\n        border: none;\n        width: 100%;\n        white-space: nowrap;\n        overflow: hidden;\n        text-overflow: ellipsis;\n      }\n    "]
                }] }
    ];
    return MaskCellComponent;
}());
export { MaskCellComponent };
if (false) {
    /** @type {?} */
    MaskCellComponent.prototype.formGroup;
    /** @type {?} */
    MaskCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    MaskCellComponent.prototype.params;
    /** @type {?} */
    MaskCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    MaskCellComponent.prototype.rowId;
    /** @type {?} */
    MaskCellComponent.prototype.settings;
    /** @type {?} */
    MaskCellComponent.prototype.maskType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9tYXNrLWNlbGwvbWFzay1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU0xQztJQUFBO0lBOERBLENBQUM7Ozs7O0lBNUJDLGtDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsbUNBQU87Ozs7SUFBUCxVQUFRLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQzthQUN4QztpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRywwQkFBMEIsQ0FBQyxDQUFDO2FBQ3RFO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLDBCQUEwQixDQUFDLENBQUM7U0FDdEU7UUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7O2dCQTdERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtvQkFDdEMsUUFBUSxFQUFFLHdTQVNUOzZCQUVDLGdQQVNDO2lCQUVKOztJQXNDRCx3QkFBQztDQUFBLEFBOURELElBOERDO1NBckNZLGlCQUFpQjs7O0lBQzVCLHNDQUE0Qjs7SUFDNUIsZ0NBQVc7Ozs7O0lBQ1gsbUNBQW9COztJQUNwQixrQ0FBYTs7Ozs7SUFDYixrQ0FBbUI7O0lBQ25CLHFDQUE4Qjs7SUFDOUIscUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgTWFza1R5cGUgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9lbnVtcy9tYXNrLXR5cGUuZW51bSc7XHJcbmltcG9ydCB7IE1hc2tTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL21vZGVscy9tYXNrLXNldHRpbmdzLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1tYXNrLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIj5cclxuICAgICAgPGxheW91dC1mb3JtLW1hc2staW5wdXRcclxuICAgICAgICBjbGFzcz1cImNvcmVHcmlkTGFiZWxDZWxsRGlzYWJsZSBhbGlnbi1taWRkbGVcIlxyXG4gICAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwia2V5XCJcclxuICAgICAgICBbbWFza1R5cGVdPVwibWFza1R5cGVcIlxyXG4gICAgICAgIFtpc1JlYWRPbmx5XT1cInRydWVcIj5cclxuICAgICAgPC9sYXlvdXQtZm9ybS1tYXNrLWlucHV0PlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZXM6IFtcclxuICAgIGBcclxuICAgICAgLmNvcmVHcmlkTGFiZWxDZWxsRGlzYWJsZSAvZGVlcC8gaW5wdXQge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMCk7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgfVxyXG4gICAgYFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE1hc2tDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcbiAgcHVibGljIHNldHRpbmdzOiBNYXNrU2V0dGluZ3M7XHJcbiAgcHVibGljIG1hc2tUeXBlOiBNYXNrVHlwZTtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KSB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZTtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgICAgdGhpcy5tYXNrVHlwZSA9IHRoaXMuc2V0dGluZ3MubWFza1R5cGU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2xEZWYuZmllbGQgKyAnIHNldHRpbmdzIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5wYXJhbXMuY29sRGVmLmZpZWxkICsgJyBzZXR0aW5ncyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcbn1cclxuIl19