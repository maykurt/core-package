/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import moment from 'moment';
var DateCellComponent = /** @class */ (function () {
    function DateCellComponent() {
        this.defaultDateFormat = 'DD.MM.YYYY';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    DateCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        if (this.params && this.params.colDef && this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
        }
        if (this.settings) {
            if (this.settings.showYear) {
                this.defaultDateFormat = 'YYYY';
            }
            if (this.settings.showTimeOnly) {
                this.defaultDateFormat = 'HH:mm';
            }
            if (this.settings.showTime) {
                this.defaultDateFormat = 'DD.MM.YYYY HH:mm';
            }
            if (this.settings.dateFormat) {
                this.defaultDateFormat = this.settings.dateFormat;
            }
        }
        if (this.params.value) {
            this.value = moment(this.params.value).format(this.defaultDateFormat);
        }
        else {
            this.value = null;
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    DateCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.value = this.params.value;
        if (this.defaultDateFormat && this.value) {
            this.value = moment(this.value).format(this.defaultDateFormat);
        }
        return true;
    };
    DateCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-date-cell',
                    template: "\n      <div *ngIf=\"value\" class=\"grid-none-editable-date-cell user-select-text text-ellipsis\">\n        {{ value }}\n      </div>\n  ",
                    styles: ["\n      .grid-none-editable-date-cell {\n        padding-top: 5px;\n      }\n    "]
                }] }
    ];
    return DateCellComponent;
}());
export { DateCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    DateCellComponent.prototype.defaultDateFormat;
    /** @type {?} */
    DateCellComponent.prototype.value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9kYXRlLWNlbGwvZGF0ZS1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxQyxPQUFPLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFHNUI7SUFBQTtRQW1CVSxzQkFBaUIsR0FBRyxZQUFZLENBQUM7SUE2QzNDLENBQUM7Ozs7O0lBMUNDLGtDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUM1RSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7YUFDOUQ7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO2dCQUMxQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRTtnQkFDOUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQzthQUNsQztZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxrQkFBa0IsQ0FBQzthQUM3QztZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQzthQUNuRDtTQUNGO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN2RTthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FDbkI7SUFDSCxDQUFDOzs7OztJQUVELG1DQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDL0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOztnQkEvREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSw0QkFBNEI7b0JBQ3RDLFFBQVEsRUFBRSw0SUFJVDs2QkFFQyxtRkFJQztpQkFFSjs7SUFrREQsd0JBQUM7Q0FBQSxBQWhFRCxJQWdFQztTQWhEWSxpQkFBaUI7Ozs7OztJQUM1QixtQ0FBb0I7Ozs7O0lBQ3BCLHFDQUErQjs7Ozs7SUFDL0IsOENBQXlDOztJQUN6QyxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBEYXRlU2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvZGF0ZS1zZXR0aW5ncy5tb2RlbCc7XHJcblxyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWRhdGUtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgICAgPGRpdiAqbmdJZj1cInZhbHVlXCIgY2xhc3M9XCJncmlkLW5vbmUtZWRpdGFibGUtZGF0ZS1jZWxsIHVzZXItc2VsZWN0LXRleHQgdGV4dC1lbGxpcHNpc1wiPlxyXG4gICAgICAgIHt7IHZhbHVlIH19XHJcbiAgICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVzOiBbXHJcbiAgICBgXHJcbiAgICAgIC5ncmlkLW5vbmUtZWRpdGFibGUtZGF0ZS1jZWxsIHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICB9XHJcbiAgICBgXHJcbiAgXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIERhdGVDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHByaXZhdGUgc2V0dGluZ3M6IERhdGVTZXR0aW5ncztcclxuICBwcml2YXRlIGRlZmF1bHREYXRlRm9ybWF0ID0gJ0RELk1NLllZWVknO1xyXG4gIHZhbHVlOiBhbnk7XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zICYmIHRoaXMucGFyYW1zLmNvbERlZiAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5zZXR0aW5ncykge1xyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5zaG93WWVhcikge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGVGb3JtYXQgPSAnWVlZWSc7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnNob3dUaW1lT25seSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGVGb3JtYXQgPSAnSEg6bW0nO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5zaG93VGltZSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGVGb3JtYXQgPSAnREQuTU0uWVlZWSBISDptbSc7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLmRhdGVGb3JtYXQpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRlRm9ybWF0ID0gdGhpcy5zZXR0aW5ncy5kYXRlRm9ybWF0O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLnZhbHVlKSB7XHJcbiAgICAgIHRoaXMudmFsdWUgPSBtb21lbnQodGhpcy5wYXJhbXMudmFsdWUpLmZvcm1hdCh0aGlzLmRlZmF1bHREYXRlRm9ybWF0KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudmFsdWUgPSBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWU7XHJcbiAgICBpZiAodGhpcy5kZWZhdWx0RGF0ZUZvcm1hdCAmJiB0aGlzLnZhbHVlKSB7XHJcbiAgICAgIHRoaXMudmFsdWUgPSBtb21lbnQodGhpcy52YWx1ZSkuZm9ybWF0KHRoaXMuZGVmYXVsdERhdGVGb3JtYXQpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG59XHJcbiJdfQ==