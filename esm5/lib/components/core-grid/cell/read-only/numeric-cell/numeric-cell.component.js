/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var NumericCellComponent = /** @class */ (function () {
    function NumericCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    NumericCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this._value = this.value;
        this.rowId = this.params.node.id;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    NumericCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        // done for experimental,
        // use case is below
        // {
        // field: 'Department',
        // columnType: CoreGridColumnType.Numeric,
        // settings: {
        //   fields: ['DepartmentId'] //for this reason
        // },
        // headerName: 'DepartmentHistoryDepartmentId'
        // },
        // {
        //   field: 'DepartmentId',
        //   columnType: CoreGridColumnType.Lov,
        //   visible: true,
        //   settings: {
        //     options: this.departmentHistoryLovGridOptions,
        //     valueField: 'DepartmentId',
        //     labelField: ['Definition']
        //   }
        // }
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                    /** @type {?} */
                    var labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.fields));
                    /** @type {?} */
                    var value_1 = '';
                    labels.forEach(function (columnName) {
                        if (_this.params.node.data[columnName]) {
                            value_1 += _this.params.node.data[columnName] + ' ';
                        }
                    });
                    if (value_1 !== '') {
                        this.value = value_1;
                    }
                }
            }
        }
        this._value = this.value;
        if (this.params.colDef.cellEditorParams.customDisplayFunction) {
            this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    NumericCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-numeric-cell',
                    template: "\n  <div *ngIf=\"value || value === 0\" class=\"grid-none-editable-numeric-cell user-select-text text-ellipsis\" title=\"{{_value}}\">\n    {{ _value }}\n  </div>\n  ",
                    styles: ["\n    .grid-none-editable-numeric-cell {\n      padding-top: 5px;\n      white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    }\n    "]
                }] }
    ];
    return NumericCellComponent;
}());
export { NumericCellComponent };
if (false) {
    /** @type {?} */
    NumericCellComponent.prototype.formGroup;
    /** @type {?} */
    NumericCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    NumericCellComponent.prototype.params;
    /** @type {?} */
    NumericCellComponent.prototype.value;
    /** @type {?} */
    NumericCellComponent.prototype._value;
    /**
     * @type {?}
     * @private
     */
    NumericCellComponent.prototype.rowId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtZXJpYy1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL3JlYWQtb25seS9udW1lcmljLWNlbGwvbnVtZXJpYy1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxQztJQUFBO0lBd0ZBLENBQUM7Ozs7O0lBOURDLHFDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELHNDQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQW5CLGlCQXFEQztRQXBEQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBFLHlCQUF5QjtRQUN6QixvQkFBb0I7UUFDcEIsSUFBSTtRQUNKLHVCQUF1QjtRQUN2QiwwQ0FBMEM7UUFDMUMsY0FBYztRQUNkLCtDQUErQztRQUMvQyxLQUFLO1FBQ0wsOENBQThDO1FBQzlDLEtBQUs7UUFDTCxJQUFJO1FBQ0osMkJBQTJCO1FBQzNCLHdDQUF3QztRQUN4QyxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLHFEQUFxRDtRQUNyRCxrQ0FBa0M7UUFDbEMsaUNBQWlDO1FBQ2pDLE1BQU07UUFDTixJQUFJO1FBRUosSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUN2QyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDaEQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxZQUFZLEtBQUssRUFBRTs7d0JBQ2xFLE1BQU0sR0FBYSxtQkFBVSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFBOzt3QkFDbEYsT0FBSyxHQUFXLEVBQUU7b0JBQ3RCLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFrQjt3QkFDaEMsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7NEJBQ3JDLE9BQUssSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDO3lCQUNsRDtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLE9BQUssS0FBSyxFQUFFLEVBQUU7d0JBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBSyxDQUFDO3FCQUNwQjtpQkFDRjthQUNGO1NBQ0Y7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFFekIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFBRTtZQUM3RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNyRjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwRDtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Z0JBdkZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsK0JBQStCO29CQUN6QyxRQUFRLEVBQUUsd0tBSVQ7NkJBRUMsaUtBT0M7aUJBRUo7O0lBdUVELDJCQUFDO0NBQUEsQUF4RkQsSUF3RkM7U0F0RVksb0JBQW9COzs7SUFDL0IseUNBQTRCOztJQUM1QixtQ0FBZ0I7Ozs7O0lBQ2hCLHNDQUFvQjs7SUFDcEIscUNBQWtCOztJQUNsQixzQ0FBbUI7Ozs7O0lBQ25CLHFDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtbnVtZXJpYy1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxkaXYgKm5nSWY9XCJ2YWx1ZSB8fCB2YWx1ZSA9PT0gMFwiIGNsYXNzPVwiZ3JpZC1ub25lLWVkaXRhYmxlLW51bWVyaWMtY2VsbCB1c2VyLXNlbGVjdC10ZXh0IHRleHQtZWxsaXBzaXNcIiB0aXRsZT1cInt7X3ZhbHVlfX1cIj5cclxuICAgIHt7IF92YWx1ZSB9fVxyXG4gIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVzOiBbXHJcbiAgICBgXHJcbiAgICAuZ3JpZC1ub25lLWVkaXRhYmxlLW51bWVyaWMtY2VsbCB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICB9XHJcbiAgICBgXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTnVtZXJpY0NlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5OiBhbnk7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwdWJsaWMgdmFsdWU6IGFueTtcclxuICBwdWJsaWMgX3ZhbHVlOiBhbnk7XHJcbiAgcHJpdmF0ZSByb3dJZDogYW55O1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucGFyYW1zLnZhbHVlO1xyXG4gICAgdGhpcy5fdmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcblxyXG4gICAgLy8gZG9uZSBmb3IgZXhwZXJpbWVudGFsLFxyXG4gICAgLy8gdXNlIGNhc2UgaXMgYmVsb3dcclxuICAgIC8vIHtcclxuICAgIC8vIGZpZWxkOiAnRGVwYXJ0bWVudCcsXHJcbiAgICAvLyBjb2x1bW5UeXBlOiBDb3JlR3JpZENvbHVtblR5cGUuTnVtZXJpYyxcclxuICAgIC8vIHNldHRpbmdzOiB7XHJcbiAgICAvLyAgIGZpZWxkczogWydEZXBhcnRtZW50SWQnXSAvL2ZvciB0aGlzIHJlYXNvblxyXG4gICAgLy8gfSxcclxuICAgIC8vIGhlYWRlck5hbWU6ICdEZXBhcnRtZW50SGlzdG9yeURlcGFydG1lbnRJZCdcclxuICAgIC8vIH0sXHJcbiAgICAvLyB7XHJcbiAgICAvLyAgIGZpZWxkOiAnRGVwYXJ0bWVudElkJyxcclxuICAgIC8vICAgY29sdW1uVHlwZTogQ29yZUdyaWRDb2x1bW5UeXBlLkxvdixcclxuICAgIC8vICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgIC8vICAgc2V0dGluZ3M6IHtcclxuICAgIC8vICAgICBvcHRpb25zOiB0aGlzLmRlcGFydG1lbnRIaXN0b3J5TG92R3JpZE9wdGlvbnMsXHJcbiAgICAvLyAgICAgdmFsdWVGaWVsZDogJ0RlcGFydG1lbnRJZCcsXHJcbiAgICAvLyAgICAgbGFiZWxGaWVsZDogWydEZWZpbml0aW9uJ11cclxuICAgIC8vICAgfVxyXG4gICAgLy8gfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgICAgIGNvbnN0IGxhYmVsczogc3RyaW5nW10gPSA8c3RyaW5nW10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmllbGRzO1xyXG4gICAgICAgICAgbGV0IHZhbHVlOiBTdHJpbmcgPSAnJztcclxuICAgICAgICAgIGxhYmVscy5mb3JFYWNoKChjb2x1bW5OYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLm5vZGUuZGF0YVtjb2x1bW5OYW1lXSkge1xyXG4gICAgICAgICAgICAgIHZhbHVlICs9IHRoaXMucGFyYW1zLm5vZGUuZGF0YVtjb2x1bW5OYW1lXSArICcgJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpZiAodmFsdWUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl92YWx1ZSA9IHRoaXMudmFsdWU7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbURpc3BsYXlGdW5jdGlvbikge1xyXG4gICAgICB0aGlzLl92YWx1ZSA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbURpc3BsYXlGdW5jdGlvbih0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG59XHJcbiJdfQ==