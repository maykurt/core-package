/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
var LovEditCellComponent = /** @class */ (function () {
    function LovEditCellComponent() {
    }
    Object.defineProperty(LovEditCellComponent.prototype, "content", {
        set: /**
         * @param {?} component
         * @return {?}
         */
        function (component) {
            if (component) {
                this.setLovComponent(component);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} params
     * @return {?}
     */
    LovEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        // console.log(params);
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        this.dataSource = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    LovEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        // console.log(params);
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
            else {
                console.error(this.params.colDef.field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + 'lov Setting is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        // console.log('patch-value', this.value);
        if (this.formGroup && this.params.colDef.cellEditorParams.customFunctionForRowValueChanges) {
            this.clearRowChangesSubscription();
            this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
            setTimeout(function () {
            });
            this.rowChangesSubscription = this.formGroup.valueChanges
                .subscribe(function (rowData) {
                _this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(_this);
            });
        }
        return true;
    };
    /**
     * @param {?} component
     * @return {?}
     */
    LovEditCellComponent.prototype.setLovComponent = /**
     * @param {?} component
     * @return {?}
     */
    function (component) {
        this.listOfValue = component;
        if (this.settings) {
            this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField, this.settings.dontInitData);
        }
    };
    /**
     * @return {?}
     */
    LovEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    LovEditCellComponent.prototype.lovValueChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        // console.log('lovValueChanged', this.params);
        if (event && this.listOfValue) {
            ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push({
                Id: this.listOfValue.listOfValuesService.endPointUrl,
                Value: event
            });
            this.dataSource = this.params.colDef.cellEditorParams.dataSource;
            // console.log('dataSource', this.dataSource);
        }
        this.params.setValue(this.value);
    };
    /**
     * @param {?} data
     * @return {?}
     */
    LovEditCellComponent.prototype.onAddToDataSource = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push(data);
        this.dataSource = this.params.colDef.cellEditorParams.dataSource;
    };
    /**
     * @return {?}
     */
    LovEditCellComponent.prototype.clearRowChangesSubscription = /**
     * @return {?}
     */
    function () {
        if (this.rowChangesSubscription) {
            this.rowChangesSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    LovEditCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.clearRowChangesSubscription();
    };
    LovEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-lov-edit-cell',
                    template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-list-of-values\n      [class.not-valid]=\"!isValid()\"\n      #listOfValue\n      [dataSource]=\"dataSource\"\n      [isReadOnly]=\"isReadOnly\"\n      [formControlName]=\"key\"\n      (lovValueChanged)=\"lovValueChanged($event)\"\n      (addToDataSource)=\"onAddToDataSource($event)\"\n      ></layout-list-of-values>\n    </div>\n  ",
                    styles: [".not-valid input{border:1px solid red}:host /deep/ .lov-btn-container{margin-top:3px}"]
                }] }
    ];
    LovEditCellComponent.propDecorators = {
        content: [{ type: ViewChild, args: ['listOfValue',] }]
    };
    return LovEditCellComponent;
}());
export { LovEditCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.listOfValue;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.settings;
    /** @type {?} */
    LovEditCellComponent.prototype.formGroup;
    /** @type {?} */
    LovEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.params;
    /** @type {?} */
    LovEditCellComponent.prototype.isReadOnly;
    /**
     * @type {?}
     * @private
     */
    LovEditCellComponent.prototype.rowChangesSubscription;
    /** @type {?} */
    LovEditCellComponent.prototype.dataSource;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWVkaXQtY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9lZGl0L2xvdi1lZGl0LWNlbGwvbG92LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBU2hFO0lBQUE7SUE0SUEsQ0FBQztJQXhIQyxzQkFBOEIseUNBQU87Ozs7O1FBQXJDLFVBQXNDLFNBQWM7WUFDbEQsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUM7OztPQUFBOzs7OztJQWlCRCxxQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQix1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUVqQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBQSxDQUFDLENBQUM7SUFDNUUsQ0FBQzs7Ozs7SUFFRCxzQ0FBTzs7OztJQUFQLFVBQVEsTUFBVztRQUFuQixpQkFtQ0M7UUFsQ0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsdUJBQXVCO1FBRXZCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2FBQzlEO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLDRCQUE0QixDQUFDLENBQUM7YUFDeEU7U0FDRjthQUFNO1lBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsNEJBQTRCLENBQUMsQ0FBQztTQUN4RTtRQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsMENBQTBDO1FBRTFDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsRUFBRTtZQUMxRixJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUUzRSxVQUFVLENBQUM7WUFDWCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7aUJBQ3RELFNBQVMsQ0FBQyxVQUFDLE9BQVk7Z0JBQ3RCLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGdDQUFnQyxDQUFDLEtBQUksQ0FBQyxDQUFDO1lBQzdFLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFHRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7O0lBRUQsOENBQWU7Ozs7SUFBZixVQUFnQixTQUFnQztRQUM5QyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNwSTtJQUNILENBQUM7Ozs7SUFFRCxzQ0FBTzs7O0lBQVA7UUFDRSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0gsQ0FBQzs7Ozs7SUFFRCw4Q0FBZTs7OztJQUFmLFVBQWdCLEtBQVU7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzRixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFO1lBQ25ELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7U0FDckQ7UUFDRCwrQ0FBK0M7UUFFL0MsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUM3QixDQUFDLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBQSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRCxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXO2dCQUNwRCxLQUFLLEVBQUUsS0FBSzthQUNiLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO1lBQ2pFLDhDQUE4QztTQUMvQztRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELGdEQUFpQjs7OztJQUFqQixVQUFrQixJQUFTO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUU7WUFDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztTQUNyRDtRQUVELENBQUMsbUJBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7SUFDbkUsQ0FBQzs7OztJQUVELDBEQUEyQjs7O0lBQTNCO1FBQ0UsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDL0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzNDO0lBQ0gsQ0FBQzs7OztJQUVELDBDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO0lBQ3JDLENBQUM7O2dCQTNJRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGdDQUFnQztvQkFDMUMsUUFBUSxFQUFFLHVhQWNUOztpQkFFRjs7OzBCQUVFLFNBQVMsU0FBQyxhQUFhOztJQXdIMUIsMkJBQUM7Q0FBQSxBQTVJRCxJQTRJQztTQXpIWSxvQkFBb0I7Ozs7OztJQU8vQiwyQ0FBMkM7Ozs7O0lBQzNDLHdDQUE4Qjs7SUFFOUIseUNBQTRCOztJQUM1QixtQ0FBVzs7Ozs7SUFDWCxxQ0FBYzs7Ozs7SUFDZCxxQ0FBbUI7Ozs7O0lBQ25CLHNDQUFvQjs7SUFFcEIsMENBQTJCOzs7OztJQUUzQixzREFBNkM7O0lBRTdDLDBDQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUFycmF5IH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgTGlzdE9mVmFsdWVzQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vLi4vbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG92U2V0dGluZ3MgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9tb2RlbHMvbG92LXNldHRpbmdzLm1vZGVsJztcclxuXHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWxvdi1lZGl0LWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2XHJcbiAgICAgICpuZ0lmPVwiZm9ybUdyb3VwXCJcclxuICAgICAgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIj5cclxuICAgICAgPGxheW91dC1saXN0LW9mLXZhbHVlc1xyXG4gICAgICBbY2xhc3Mubm90LXZhbGlkXT1cIiFpc1ZhbGlkKClcIlxyXG4gICAgICAjbGlzdE9mVmFsdWVcclxuICAgICAgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiXHJcbiAgICAgIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICBbZm9ybUNvbnRyb2xOYW1lXT1cImtleVwiXHJcbiAgICAgIChsb3ZWYWx1ZUNoYW5nZWQpPVwibG92VmFsdWVDaGFuZ2VkKCRldmVudClcIlxyXG4gICAgICAoYWRkVG9EYXRhU291cmNlKT1cIm9uQWRkVG9EYXRhU291cmNlKCRldmVudClcIlxyXG4gICAgICA+PC9sYXlvdXQtbGlzdC1vZi12YWx1ZXM+XHJcbiAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL2xvdi1lZGl0LWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG92RWRpdENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAsIE9uRGVzdHJveSB7XHJcbiAgQFZpZXdDaGlsZCgnbGlzdE9mVmFsdWUnKSBzZXQgY29udGVudChjb21wb25lbnQ6IGFueSkge1xyXG4gICAgaWYgKGNvbXBvbmVudCkge1xyXG4gICAgICB0aGlzLnNldExvdkNvbXBvbmVudChjb21wb25lbnQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBsaXN0T2ZWYWx1ZTogTGlzdE9mVmFsdWVzQ29tcG9uZW50O1xyXG4gIHByaXZhdGUgc2V0dGluZ3M6IExvdlNldHRpbmdzO1xyXG5cclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwcml2YXRlIHZhbHVlO1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG5cclxuICBwdWJsaWMgaXNSZWFkT25seTogYm9vbGVhbjtcclxuXHJcbiAgcHJpdmF0ZSByb3dDaGFuZ2VzU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHB1YmxpYyBkYXRhU291cmNlOiBhbnlbXTtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhwYXJhbXMpO1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgcGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWUgPyArdGhpcy5wYXJhbXMudmFsdWUgOiBudWxsO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gIXRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24odGhpcy5wYXJhbXMuZGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5kYXRhU291cmNlID0gKDxhbnlbXT50aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgLy8gY29uc29sZS5sb2cocGFyYW1zKTtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5wYXJhbXMuY29sRGVmLmZpZWxkICsgJ2xvdiBTZXR0aW5nIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5wYXJhbXMuY29sRGVmLmZpZWxkICsgJ2xvdiBTZXR0aW5nIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBjb25zb2xlLmxvZygncGF0Y2gtdmFsdWUnLCB0aGlzLnZhbHVlKTtcclxuXHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXAgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRnVuY3Rpb25Gb3JSb3dWYWx1ZUNoYW5nZXMpIHtcclxuICAgICAgdGhpcy5jbGVhclJvd0NoYW5nZXNTdWJzY3JpcHRpb24oKTtcclxuICAgICAgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRnVuY3Rpb25Gb3JSb3dWYWx1ZUNoYW5nZXModGhpcyk7XHJcblxyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMucm93Q2hhbmdlc1N1YnNjcmlwdGlvbiA9IHRoaXMuZm9ybUdyb3VwLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJvd0RhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRnVuY3Rpb25Gb3JSb3dWYWx1ZUNoYW5nZXModGhpcyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgc2V0TG92Q29tcG9uZW50KGNvbXBvbmVudDogTGlzdE9mVmFsdWVzQ29tcG9uZW50KSB7XHJcbiAgICB0aGlzLmxpc3RPZlZhbHVlID0gY29tcG9uZW50O1xyXG4gICAgaWYgKHRoaXMuc2V0dGluZ3MpIHtcclxuICAgICAgdGhpcy5saXN0T2ZWYWx1ZS5zZXRPcHRpb25zKHRoaXMuc2V0dGluZ3Mub3B0aW9ucywgdGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkLCB0aGlzLnNldHRpbmdzLmxhYmVsRmllbGQsIHRoaXMuc2V0dGluZ3MuZG9udEluaXREYXRhKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISgodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmVycm9ycyAmJiAodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmRpcnR5IHx8IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS50b3VjaGVkKSkpO1xyXG4gIH1cclxuXHJcbiAgbG92VmFsdWVDaGFuZ2VkKGV2ZW50OiBhbnkpIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWUgPyArdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlIDogbnVsbDtcclxuICAgIGlmICghdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlID0gW107XHJcbiAgICB9XHJcbiAgICAvLyBjb25zb2xlLmxvZygnbG92VmFsdWVDaGFuZ2VkJywgdGhpcy5wYXJhbXMpO1xyXG5cclxuICAgIGlmIChldmVudCAmJiB0aGlzLmxpc3RPZlZhbHVlKSB7XHJcbiAgICAgICg8YW55W10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSkucHVzaCh7XHJcbiAgICAgICAgSWQ6IHRoaXMubGlzdE9mVmFsdWUubGlzdE9mVmFsdWVzU2VydmljZS5lbmRQb2ludFVybCxcclxuICAgICAgICBWYWx1ZTogZXZlbnRcclxuICAgICAgfSk7XHJcblxyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZygnZGF0YVNvdXJjZScsIHRoaXMuZGF0YVNvdXJjZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICB9XHJcblxyXG4gIG9uQWRkVG9EYXRhU291cmNlKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKCF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKSB7XHJcbiAgICAgIHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UgPSBbXTtcclxuICAgIH1cclxuXHJcbiAgICAoPGFueVtdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpLnB1c2goZGF0YSk7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJSb3dDaGFuZ2VzU3Vic2NyaXB0aW9uKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMucm93Q2hhbmdlc1N1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnJvd0NoYW5nZXNTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbGVhclJvd0NoYW5nZXNTdWJzY3JpcHRpb24oKTtcclxuICB9XHJcbn1cclxuIl19