/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var SelectEditCellComponent = /** @class */ (function () {
    function SelectEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    SelectEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        this.gridService = this.params.context.componentParent;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
            this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
        }
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(function (x) {
            _this.getListData();
        });
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.createListeningSubscription = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.filterByField) {
            // clear if suscription has
            this.clearfilterByFieldSubscription();
            // if formgroup not exist, get formgroup
            if (!this.formGroup) {
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
            }
            if (this.formGroup) {
                // get key in order to track changes
                /** @type {?} */
                var columnKeyToListen_1 = this.getFilterByFieldKey();
                // for initial value fill according to that
                /** @type {?} */
                var formgroupForKeyToListen = this.formGroup.at(columnKeyToListen_1);
                if (formgroupForKeyToListen && (formgroupForKeyToListen.value || formgroupForKeyToListen.value === 0)) {
                    this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                        .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                }
                // listen that cloumn changes at that field
                this.filterByFieldSubscription = this.formGroup.at(columnKeyToListen_1).valueChanges
                    .subscribe(function (data) {
                    if (!data) {
                        // if the data set to null in that column, then set selft to null
                        _this.setValue(null);
                        // clear list because data comes according to this data value
                        _this.listData = [];
                    }
                    else {
                        // clear list in order to assign new ones
                        _this.listData = [];
                        // fill the list according to value at that column
                        _this.listData = ((/** @type {?} */ (_this.params.colDef.cellEditorParams.dataSource)))
                            .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                        // look the current selected id, if it is exist in new listData,
                        /** @type {?} */
                        var foundData = _this.listData.find(function (x) { return x[_this.settings.valueField] === _this.formGroup.at(_this.key).value; });
                        if (!foundData) {
                            // if it is not clear data of select
                            _this.setValue(null);
                            // somehow lisData broken, in order to avoid this, I re assigned that
                            _this.listData = ((/** @type {?} */ (_this.params.colDef.cellEditorParams.dataSource)))
                                .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                        }
                    }
                });
            }
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    SelectEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            else {
                console.error(this.params.colDef.field + ' lov settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' data source or lov settings is not defined');
        }
        return true;
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.getListData = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.params.colDef.cellEditorParams.dataSource) {
            // get filterByField
            if (this.params.colDef.cellEditorParams.settings) {
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            if (!this.filterByField) {
                // if filterByField not exist, then assign orginal data source
                this.listData = this.params.colDef.cellEditorParams.dataSource;
            }
            else {
                // if exist, get the key, then filter according to that
                /** @type {?} */
                var columnKeyToListen_2 = this.getFilterByFieldKey();
                this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                    .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_2).value; });
            }
            this.clearDataSubscription();
            this.createListeningSubscription();
        }
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.getFilterByFieldKey = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // get find Column according to  filterByField
        /** @type {?} */
        var column = this.gridService.allGridColumns.find(function (x) { return x.colDef.field === _this.filterByField; });
        // createKey with that column in order to use for tracking
        /** @type {?} */
        var columnKeyToListen = this.params.context.createKey(this.params.columnApi, column);
        return columnKeyToListen;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    SelectEditCellComponent.prototype.onValueChanged = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    SelectEditCellComponent.prototype.setValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.value = value;
        this.formGroup.at(this.key).patchValue(value);
        this.params.setValue(value);
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.clearDataSubscription = /**
     * @return {?}
     */
    function () {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.clearfilterByFieldSubscription = /**
     * @return {?}
     */
    function () {
        if (this.filterByFieldSubscription) {
            this.filterByFieldSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    SelectEditCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.clearDataSubscription();
        this.clearfilterByFieldSubscription();
    };
    SelectEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-select-edit-cell',
                    template: "\n    <div\n      *ngIf=\"formGroup && this.settings && this.settings.valueField\"\n      [formGroup]=\"formGroup\">\n      <layout-static-selector\n        #selector\n\t\t\t\t[class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        (changed)=\"onValueChanged($event)\"\n        [dataSource]=\"listData\"\n        [isReadOnly]=\"isReadOnly\"\n        [valueField]=\"this.settings.valueField\"\n        [labelField]=\"this.settings.labelField\"\n      >\n      </layout-static-selector>\n    </div>\n  ",
                    styles: [":host /deep/ .custom-ng-select{margin-top:2px}:host /deep/ .not-valid .custom-ng-select .ng-select-container{border:1px solid red}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:1px}"]
                }] }
    ];
    return SelectEditCellComponent;
}());
export { SelectEditCellComponent };
if (false) {
    /** @type {?} */
    SelectEditCellComponent.prototype.formGroup;
    /** @type {?} */
    SelectEditCellComponent.prototype.key;
    /** @type {?} */
    SelectEditCellComponent.prototype.settings;
    /** @type {?} */
    SelectEditCellComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.dataSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.filterByFieldSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.gridService;
    /**
     * @type {?}
     * @private
     */
    SelectEditCellComponent.prototype.filterByField;
    /** @type {?} */
    SelectEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9lZGl0L3NlbGVjdC1lZGl0LWNlbGwvc2VsZWN0LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFPckQ7SUFBQTtJQW1NQSxDQUFDOzs7OztJQTVKQyx3Q0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUFsQixpQkFtQkM7UUFsQkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQztRQUV2RCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7WUFDN0csSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDO1NBQ2pGO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBQSxDQUFDO1lBQzFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCw2REFBMkI7OztJQUEzQjtRQUFBLGlCQWtEQztRQWpEQyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsMkJBQTJCO1lBQzNCLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO1lBRXRDLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNyRTtZQUVELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTs7O29CQUVaLG1CQUFpQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRTs7O29CQUc5Qyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQztnQkFDcEUsSUFBSSx1QkFBdUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUssSUFBSSx1QkFBdUIsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3JHLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQzt5QkFDcEUsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDLEtBQUssRUFBcEUsQ0FBb0UsQ0FBQyxDQUFDO2lCQUN0RjtnQkFHRCwyQ0FBMkM7Z0JBQzNDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDLFlBQVk7cUJBQy9FLFNBQVMsQ0FBQyxVQUFDLElBQVM7b0JBQ25CLElBQUksQ0FBQyxJQUFJLEVBQUU7d0JBQ1QsaUVBQWlFO3dCQUNqRSxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQiw2REFBNkQ7d0JBQzdELEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3FCQUNwQjt5QkFBTTt3QkFDTCx5Q0FBeUM7d0JBQ3pDLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixrREFBa0Q7d0JBQ2xELEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQzs2QkFDcEUsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDLEtBQUssRUFBcEUsQ0FBb0UsQ0FBQyxDQUFDOzs7NEJBRy9FLFNBQVMsR0FBUSxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLEtBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEVBQWpFLENBQWlFLENBQUM7d0JBQ2pILElBQUksQ0FBQyxTQUFTLEVBQUU7NEJBQ2Qsb0NBQW9DOzRCQUNwQyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNwQixxRUFBcUU7NEJBQ3JFLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQztpQ0FDcEUsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDLEtBQUssRUFBcEUsQ0FBb0UsQ0FBQyxDQUFDO3lCQUN0RjtxQkFDRjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNOO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELHlDQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUN2QyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2dCQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7YUFDakY7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsOEJBQThCLENBQUMsQ0FBQzthQUMxRTtTQUNGO2FBQU07WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyw2Q0FBNkMsQ0FBQyxDQUFDO1NBQ3pGO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsNkNBQVc7OztJQUFYO1FBQUEsaUJBb0JDO1FBbkJDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFO1lBQ2xELG9CQUFvQjtZQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDO2FBQ2pGO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3ZCLDhEQUE4RDtnQkFDOUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7YUFDaEU7aUJBQU07OztvQkFFQyxtQkFBaUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUEsQ0FBQztxQkFDcEUsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDLEtBQUssRUFBcEUsQ0FBb0UsQ0FBQyxDQUFDO2FBQ3RGO1lBRUQsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDN0IsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQscURBQW1COzs7SUFBbkI7UUFBQSxpQkFNQzs7O1lBSk8sTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUksQ0FBQyxhQUFhLEVBQXJDLENBQXFDLENBQUM7OztZQUV6RixpQkFBaUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDO1FBQ3RGLE9BQU8saUJBQWlCLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCxnREFBYzs7OztJQUFkLFVBQWUsSUFBUztRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzNGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELDBDQUFROzs7O0lBQVIsVUFBUyxLQUFVO1FBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELHlDQUFPOzs7SUFBUDtRQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOzs7O0lBRUQsdURBQXFCOzs7SUFBckI7UUFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDckM7SUFDSCxDQUFDOzs7O0lBRUQsZ0VBQThCOzs7SUFBOUI7UUFDRSxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDOUM7SUFDSCxDQUFDOzs7O0lBRUQsNkNBQVc7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7SUFDeEMsQ0FBQzs7Z0JBbE1GLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUNBQW1DO29CQUM3QyxRQUFRLEVBQUUsMmdCQWdCVDs7aUJBRUY7O0lBK0tELDhCQUFDO0NBQUEsQUFuTUQsSUFtTUM7U0E5S1ksdUJBQXVCOzs7SUFDbEMsNENBQTRCOztJQUM1QixzQ0FBZ0I7O0lBQ2hCLDJDQUFrQzs7SUFDbEMsMkNBQXVCOzs7OztJQUV2Qix3Q0FBbUI7Ozs7O0lBQ25CLHdDQUFtQjs7Ozs7SUFDbkIseUNBQW9COzs7OztJQUNwQixtREFBdUM7Ozs7O0lBRXZDLDREQUFnRDs7Ozs7SUFFaEQsOENBQXFDOzs7OztJQUNyQyxnREFBOEI7O0lBRTlCLDZDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgU2VsZWN0b3JTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL21vZGVscy9zZWxlY3Rvci1zZXR0aW5ncy5tb2RlbCc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBDb3JlR3JpZFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9jb3JlLWdyaWQuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtc2VsZWN0LWVkaXQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXZcclxuICAgICAgKm5nSWY9XCJmb3JtR3JvdXAgJiYgdGhpcy5zZXR0aW5ncyAmJiB0aGlzLnNldHRpbmdzLnZhbHVlRmllbGRcIlxyXG4gICAgICBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiPlxyXG4gICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvclxyXG4gICAgICAgICNzZWxlY3RvclxyXG5cdFx0XHRcdFtjbGFzcy5ub3QtdmFsaWRdPVwiIWlzVmFsaWQoKVwiXHJcbiAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICAgIChjaGFuZ2VkKT1cIm9uVmFsdWVDaGFuZ2VkKCRldmVudClcIlxyXG4gICAgICAgIFtkYXRhU291cmNlXT1cImxpc3REYXRhXCJcclxuICAgICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICBbdmFsdWVGaWVsZF09XCJ0aGlzLnNldHRpbmdzLnZhbHVlRmllbGRcIlxyXG4gICAgICAgIFtsYWJlbEZpZWxkXT1cInRoaXMuc2V0dGluZ3MubGFiZWxGaWVsZFwiXHJcbiAgICAgID5cclxuICAgICAgPC9sYXlvdXQtc3RhdGljLXNlbGVjdG9yPlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9zZWxlY3QtZWRpdC1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdEVkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wLCBPbkRlc3Ryb3kge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5OiBhbnk7XHJcbiAgcHVibGljIHNldHRpbmdzOiBTZWxlY3RvclNldHRpbmdzO1xyXG4gIHB1YmxpYyBsaXN0RGF0YTogYW55W107XHJcblxyXG4gIHByaXZhdGUgdmFsdWU6IGFueTtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwcml2YXRlIGRhdGFTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgcHJpdmF0ZSBmaWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHByaXZhdGUgZ3JpZFNlcnZpY2U6IENvcmVHcmlkU2VydmljZTtcclxuICBwcml2YXRlIGZpbHRlckJ5RmllbGQ6IHN0cmluZztcclxuXHJcbiAgcHVibGljIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgcGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWUgPyArdGhpcy5wYXJhbXMudmFsdWUgOiBudWxsO1xyXG4gICAgdGhpcy5yb3dJZCA9IHRoaXMucGFyYW1zLm5vZGUuaWQ7XHJcbiAgICB0aGlzLmdyaWRTZXJ2aWNlID0gdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQ7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gIXRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24odGhpcy5wYXJhbXMuZGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZiAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcyAmJiB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgIHRoaXMuZmlsdGVyQnlGaWVsZCA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpbHRlckJ5RmllbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5kYXRhU3Vic2NyaXB0aW9uID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YS5zdWJzY3JpYmUoeCA9PiB7XHJcbiAgICAgIHRoaXMuZ2V0TGlzdERhdGEoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlTGlzdGVuaW5nU3Vic2NyaXB0aW9uKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZmlsdGVyQnlGaWVsZCkge1xyXG4gICAgICAvLyBjbGVhciBpZiBzdXNjcmlwdGlvbiBoYXNcclxuICAgICAgdGhpcy5jbGVhcmZpbHRlckJ5RmllbGRTdWJzY3JpcHRpb24oKTtcclxuXHJcbiAgICAgIC8vIGlmIGZvcm1ncm91cCBub3QgZXhpc3QsIGdldCBmb3JtZ3JvdXBcclxuICAgICAgaWYgKCF0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICAgIC8vIGdldCBrZXkgaW4gb3JkZXIgdG8gdHJhY2sgY2hhbmdlc1xyXG4gICAgICAgIGNvbnN0IGNvbHVtbktleVRvTGlzdGVuID0gdGhpcy5nZXRGaWx0ZXJCeUZpZWxkS2V5KCk7XHJcblxyXG4gICAgICAgIC8vIGZvciBpbml0aWFsIHZhbHVlIGZpbGwgYWNjb3JkaW5nIHRvIHRoYXRcclxuICAgICAgICBjb25zdCBmb3JtZ3JvdXBGb3JLZXlUb0xpc3RlbiA9IHRoaXMuZm9ybUdyb3VwLmF0KGNvbHVtbktleVRvTGlzdGVuKTtcclxuICAgICAgICBpZiAoZm9ybWdyb3VwRm9yS2V5VG9MaXN0ZW4gJiYgKGZvcm1ncm91cEZvcktleVRvTGlzdGVuLnZhbHVlIHx8IGZvcm1ncm91cEZvcktleVRvTGlzdGVuLnZhbHVlID09PSAwKSkge1xyXG4gICAgICAgICAgdGhpcy5saXN0RGF0YSA9ICg8YW55W10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSlcclxuICAgICAgICAgICAgLmZpbHRlcih4ID0+IHhbdGhpcy5maWx0ZXJCeUZpZWxkXSA9PT0gdGhpcy5mb3JtR3JvdXAuYXQoY29sdW1uS2V5VG9MaXN0ZW4pLnZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAvLyBsaXN0ZW4gdGhhdCBjbG91bW4gY2hhbmdlcyBhdCB0aGF0IGZpZWxkXHJcbiAgICAgICAgdGhpcy5maWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uID0gdGhpcy5mb3JtR3JvdXAuYXQoY29sdW1uS2V5VG9MaXN0ZW4pLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAgICAgLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgICAgIC8vIGlmIHRoZSBkYXRhIHNldCB0byBudWxsIGluIHRoYXQgY29sdW1uLCB0aGVuIHNldCBzZWxmdCB0byBudWxsXHJcbiAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICAgICAgICAvLyBjbGVhciBsaXN0IGJlY2F1c2UgZGF0YSBjb21lcyBhY2NvcmRpbmcgdG8gdGhpcyBkYXRhIHZhbHVlXHJcbiAgICAgICAgICAgICAgdGhpcy5saXN0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIC8vIGNsZWFyIGxpc3QgaW4gb3JkZXIgdG8gYXNzaWduIG5ldyBvbmVzXHJcbiAgICAgICAgICAgICAgdGhpcy5saXN0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgICAgIC8vIGZpbGwgdGhlIGxpc3QgYWNjb3JkaW5nIHRvIHZhbHVlIGF0IHRoYXQgY29sdW1uXHJcbiAgICAgICAgICAgICAgdGhpcy5saXN0RGF0YSA9ICg8YW55W10+dGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSlcclxuICAgICAgICAgICAgICAgIC5maWx0ZXIoeCA9PiB4W3RoaXMuZmlsdGVyQnlGaWVsZF0gPT09IHRoaXMuZm9ybUdyb3VwLmF0KGNvbHVtbktleVRvTGlzdGVuKS52YWx1ZSk7XHJcblxyXG4gICAgICAgICAgICAgIC8vIGxvb2sgdGhlIGN1cnJlbnQgc2VsZWN0ZWQgaWQsIGlmIGl0IGlzIGV4aXN0IGluIG5ldyBsaXN0RGF0YSxcclxuICAgICAgICAgICAgICBjb25zdCBmb3VuZERhdGE6IGFueSA9IHRoaXMubGlzdERhdGEuZmluZCh4ID0+IHhbdGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXSA9PT0gdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlKTtcclxuICAgICAgICAgICAgICBpZiAoIWZvdW5kRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgaXQgaXMgbm90IGNsZWFyIGRhdGEgb2Ygc2VsZWN0XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlKG51bGwpO1xyXG4gICAgICAgICAgICAgICAgLy8gc29tZWhvdyBsaXNEYXRhIGJyb2tlbiwgaW4gb3JkZXIgdG8gYXZvaWQgdGhpcywgSSByZSBhc3NpZ25lZCB0aGF0XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxpc3REYXRhID0gKDxhbnlbXT50aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlKVxyXG4gICAgICAgICAgICAgICAgICAuZmlsdGVyKHggPT4geFt0aGlzLmZpbHRlckJ5RmllbGRdID09PSB0aGlzLmZvcm1Hcm91cC5hdChjb2x1bW5LZXlUb0xpc3RlbikudmFsdWUpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpOiBib29sZWFuIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMpIHtcclxuICAgICAgdGhpcy5nZXRMaXN0RGF0YSgpO1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgICAgdGhpcy5maWx0ZXJCeUZpZWxkID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmlsdGVyQnlGaWVsZDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbERlZi5maWVsZCArICcgbG92IHNldHRpbmdzIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5wYXJhbXMuY29sRGVmLmZpZWxkICsgJyBkYXRhIHNvdXJjZSBvciBsb3Ygc2V0dGluZ3MgaXMgbm90IGRlZmluZWQnKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIGdldExpc3REYXRhKCkge1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpIHtcclxuICAgICAgLy8gZ2V0IGZpbHRlckJ5RmllbGRcclxuICAgICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzKSB7XHJcbiAgICAgICAgdGhpcy5maWx0ZXJCeUZpZWxkID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MuZmlsdGVyQnlGaWVsZDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCF0aGlzLmZpbHRlckJ5RmllbGQpIHtcclxuICAgICAgICAvLyBpZiBmaWx0ZXJCeUZpZWxkIG5vdCBleGlzdCwgdGhlbiBhc3NpZ24gb3JnaW5hbCBkYXRhIHNvdXJjZVxyXG4gICAgICAgIHRoaXMubGlzdERhdGEgPSB0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5kYXRhU291cmNlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vIGlmIGV4aXN0LCBnZXQgdGhlIGtleSwgdGhlbiBmaWx0ZXIgYWNjb3JkaW5nIHRvIHRoYXRcclxuICAgICAgICBjb25zdCBjb2x1bW5LZXlUb0xpc3RlbiA9IHRoaXMuZ2V0RmlsdGVyQnlGaWVsZEtleSgpO1xyXG4gICAgICAgIHRoaXMubGlzdERhdGEgPSAoPGFueVtdPnRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2UpXHJcbiAgICAgICAgICAuZmlsdGVyKHggPT4geFt0aGlzLmZpbHRlckJ5RmllbGRdID09PSB0aGlzLmZvcm1Hcm91cC5hdChjb2x1bW5LZXlUb0xpc3RlbikudmFsdWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLmNsZWFyRGF0YVN1YnNjcmlwdGlvbigpO1xyXG4gICAgICB0aGlzLmNyZWF0ZUxpc3RlbmluZ1N1YnNjcmlwdGlvbigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0RmlsdGVyQnlGaWVsZEtleSgpOiBudW1iZXIge1xyXG4gICAgLy8gZ2V0IGZpbmQgQ29sdW1uIGFjY29yZGluZyB0byAgZmlsdGVyQnlGaWVsZFxyXG4gICAgY29uc3QgY29sdW1uID0gdGhpcy5ncmlkU2VydmljZS5hbGxHcmlkQ29sdW1ucy5maW5kKHggPT4geC5jb2xEZWYuZmllbGQgPT09IHRoaXMuZmlsdGVyQnlGaWVsZCk7XHJcbiAgICAvLyBjcmVhdGVLZXkgd2l0aCB0aGF0IGNvbHVtbiBpbiBvcmRlciB0byB1c2UgZm9yIHRyYWNraW5nXHJcbiAgICBjb25zdCBjb2x1bW5LZXlUb0xpc3RlbiA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgY29sdW1uKTtcclxuICAgIHJldHVybiBjb2x1bW5LZXlUb0xpc3RlbjtcclxuICB9XHJcblxyXG4gIG9uVmFsdWVDaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZSA/ICt0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWUgOiBudWxsO1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBzZXRWYWx1ZSh2YWx1ZTogYW55KSB7XHJcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XHJcbiAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh2YWx1ZSk7XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEoKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5lcnJvcnMgJiYgKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5kaXJ0eSB8fCB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudG91Y2hlZCkpKTtcclxuICB9XHJcblxyXG4gIGNsZWFyRGF0YVN1YnNjcmlwdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmRhdGFTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5kYXRhU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjbGVhcmZpbHRlckJ5RmllbGRTdWJzY3JpcHRpb24oKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5maWx0ZXJCeUZpZWxkU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMuZmlsdGVyQnlGaWVsZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsZWFyRGF0YVN1YnNjcmlwdGlvbigpO1xyXG4gICAgdGhpcy5jbGVhcmZpbHRlckJ5RmllbGRTdWJzY3JpcHRpb24oKTtcclxuICB9XHJcbn1cclxuIl19