/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var CurrencyEditCellComponent = /** @class */ (function () {
    function CurrencyEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    CurrencyEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = (this.params.value || this.params.value === 0) ? this.params.value.toString() : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    CurrencyEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    /**
     * @return {?}
     */
    CurrencyEditCellComponent.prototype.onValueChanged = /**
     * @return {?}
     */
    function () {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    };
    /**
     * @return {?}
     */
    CurrencyEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    CurrencyEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-currency-edit-cell',
                    template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-form-currency-input\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (changed)=\"onValueChanged()\">\n      </layout-form-currency-input>\n    </div>\n  ",
                    styles: [""]
                }] }
    ];
    return CurrencyEditCellComponent;
}());
export { CurrencyEditCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CurrencyEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    CurrencyEditCellComponent.prototype.rowId;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.formGroup;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.key;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.value;
    /** @type {?} */
    CurrencyEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3ktZWRpdC1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2VkaXQvY3VycmVuY3ktZWRpdC1jZWxsL2N1cnJlbmN5LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFJMUM7SUFBQTtJQXNEQSxDQUFDOzs7OztJQTdCQywwQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2xHLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBRWpDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLEVBQUU7WUFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakc7SUFDSCxDQUFDOzs7OztJQUVELDJDQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsa0RBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsMkNBQU87OztJQUFQO1FBQ0UsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9ILENBQUM7O2dCQXJERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFDQUFxQztvQkFDL0MsUUFBUSxFQUFFLDRUQVdUOztpQkFFRjs7SUF1Q0QsZ0NBQUM7Q0FBQSxBQXRERCxJQXNEQztTQXRDWSx5QkFBeUI7Ozs7OztJQUNwQywyQ0FBb0I7Ozs7O0lBQ3BCLDBDQUFtQjs7SUFDbkIsOENBQTRCOztJQUM1Qix3Q0FBVzs7SUFDWCwwQ0FBYTs7SUFFYiwrQ0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgRm9ybUFycmF5IH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWN1cnJlbmN5LWVkaXQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXZcclxuICAgICAgKm5nSWY9XCJmb3JtR3JvdXBcIlxyXG4gICAgICBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tY3VycmVuY3ktaW5wdXRcclxuICAgICAgICBbY2xhc3Mubm90LXZhbGlkXT1cIiFpc1ZhbGlkKClcIlxyXG4gICAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwia2V5XCJcclxuICAgICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAoY2hhbmdlZCk9XCJvblZhbHVlQ2hhbmdlZCgpXCI+XHJcbiAgICAgIDwvbGF5b3V0LWZvcm0tY3VycmVuY3ktaW5wdXQ+XHJcbiAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL2N1cnJlbmN5LWVkaXQtY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDdXJyZW5jeUVkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwdWJsaWMgdmFsdWU7XHJcblxyXG4gIHB1YmxpYyBpc1JlYWRPbmx5OiBib29sZWFuO1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgdGhpcy52YWx1ZSA9ICh0aGlzLnBhcmFtcy52YWx1ZSB8fCB0aGlzLnBhcmFtcy52YWx1ZSA9PT0gMCkgPyB0aGlzLnBhcmFtcy52YWx1ZS50b1N0cmluZygpIDogbnVsbDtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcblxyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGlmICh0aGlzLmZvcm1Hcm91cCkge1xyXG4gICAgICB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkucGF0Y2hWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgb25WYWx1ZUNoYW5nZWQoKSB7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnZhbHVlO1xyXG4gICAgdGhpcy5wYXJhbXMuc2V0VmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuICEoKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5lcnJvcnMgJiYgKHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5kaXJ0eSB8fCB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudG91Y2hlZCkpKTtcclxuICB9XHJcbn1cclxuIl19