/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var MaskEditCellComponent = /** @class */ (function () {
    function MaskEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    MaskEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value.toString() : '';
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    MaskEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.maskType = this.settings.maskType;
            }
            else {
                console.error(this.params.colDef.field + ' settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' settings is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    /**
     * @return {?}
     */
    MaskEditCellComponent.prototype.onValueChanged = /**
     * @return {?}
     */
    function () {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    };
    /**
     * @return {?}
     */
    MaskEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    MaskEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-mask-edit-cell',
                    template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\">\n    <layout-form-mask-input\n      [class.not-valid]=\"!isValid()\"\n      [formControlName]=\"key\"\n      [maskType]=\"maskType\"\n      [isReadOnly]=\"isReadOnly\"\n      (changed)=\"onValueChanged()\"\n      [isReadOnly]=\"true\">\n    </layout-form-mask-input>\n    </div>\n  ",
                    styles: [""]
                }] }
    ];
    return MaskEditCellComponent;
}());
export { MaskEditCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaskEditCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    MaskEditCellComponent.prototype.rowId;
    /** @type {?} */
    MaskEditCellComponent.prototype.formGroup;
    /** @type {?} */
    MaskEditCellComponent.prototype.key;
    /** @type {?} */
    MaskEditCellComponent.prototype.value;
    /** @type {?} */
    MaskEditCellComponent.prototype.settings;
    /** @type {?} */
    MaskEditCellComponent.prototype.maskType;
    /** @type {?} */
    MaskEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC9tYXNrLWVkaXQtY2VsbC9tYXNrLWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRMUM7SUFBQTtJQW1FQSxDQUFDOzs7OztJQXhDQyxzQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUVqQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO0lBQ0gsQ0FBQzs7Ozs7SUFFRCx1Q0FBTzs7OztJQUFQLFVBQVEsTUFBVztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUVyQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQzthQUN4QztpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRywwQkFBMEIsQ0FBQyxDQUFDO2FBQ3RFO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLDBCQUEwQixDQUFDLENBQUM7U0FDdEU7UUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BFLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwRDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELDhDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELHVDQUFPOzs7SUFBUDtRQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOztnQkFsRUYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQ0FBaUM7b0JBQzNDLFFBQVEsRUFBRSxzVkFXVDs7aUJBRUY7O0lBb0RELDRCQUFDO0NBQUEsQUFuRUQsSUFtRUM7U0FuRFkscUJBQXFCOzs7Ozs7SUFDaEMsdUNBQW9COzs7OztJQUNwQixzQ0FBbUI7O0lBQ25CLDBDQUE0Qjs7SUFDNUIsb0NBQVc7O0lBQ1gsc0NBQWE7O0lBQ2IseUNBQThCOztJQUM5Qix5Q0FBMEI7O0lBRTFCLDJDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5pbXBvcnQgeyBNYXNrVHlwZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL21hc2stdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgTWFza1NldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL21hc2stc2V0dGluZ3MubW9kZWwnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1tYXNrLWVkaXQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgKm5nSWY9XCJmb3JtR3JvdXBcIiBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiPlxyXG4gICAgPGxheW91dC1mb3JtLW1hc2staW5wdXRcclxuICAgICAgW2NsYXNzLm5vdC12YWxpZF09XCIhaXNWYWxpZCgpXCJcclxuICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICBbbWFza1R5cGVdPVwibWFza1R5cGVcIlxyXG4gICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgKGNoYW5nZWQpPVwib25WYWx1ZUNoYW5nZWQoKVwiXHJcbiAgICAgIFtpc1JlYWRPbmx5XT1cInRydWVcIj5cclxuICAgIDwvbGF5b3V0LWZvcm0tbWFzay1pbnB1dD5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbWFzay1lZGl0LWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFza0VkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwdWJsaWMgdmFsdWU7XHJcbiAgcHVibGljIHNldHRpbmdzOiBNYXNrU2V0dGluZ3M7XHJcbiAgcHVibGljIG1hc2tUeXBlOiBNYXNrVHlwZTtcclxuXHJcbiAgcHVibGljIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmtleSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgcGFyYW1zLmNvbHVtbik7XHJcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wYXJhbXMudmFsdWUgPyB0aGlzLnBhcmFtcy52YWx1ZS50b1N0cmluZygpIDogJyc7XHJcbiAgICB0aGlzLnJvd0lkID0gdGhpcy5wYXJhbXMubm9kZS5pZDtcclxuXHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRWRpdGFibGVGdW5jdGlvbikge1xyXG4gICAgICB0aGlzLmlzUmVhZE9ubHkgPSAhdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuY3VzdG9tRWRpdGFibGVGdW5jdGlvbih0aGlzLnBhcmFtcy5kYXRhKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2gocGFyYW1zOiBhbnkpIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgICAgdGhpcy5tYXNrVHlwZSA9IHRoaXMuc2V0dGluZ3MubWFza1R5cGU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcih0aGlzLnBhcmFtcy5jb2xEZWYuZmllbGQgKyAnIHNldHRpbmdzIGlzIG5vdCBkZWZpbmVkJyk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IodGhpcy5wYXJhbXMuY29sRGVmLmZpZWxkICsgJyBzZXR0aW5ncyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIG9uVmFsdWVDaGFuZ2VkKCkge1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZTtcclxuICAgIHRoaXMucGFyYW1zLnNldFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhKCh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZXJyb3JzICYmICh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZGlydHkgfHwgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnRvdWNoZWQpKSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==