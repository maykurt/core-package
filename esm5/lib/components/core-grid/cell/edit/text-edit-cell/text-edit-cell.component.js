/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var TextEditCellComponent = /** @class */ (function () {
    function TextEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    TextEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value.toString() : '';
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    TextEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    /**
     * @return {?}
     */
    TextEditCellComponent.prototype.onValueChanged = /**
     * @return {?}
     */
    function () {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    };
    /**
     * @return {?}
     */
    TextEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    TextEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-text-edit-cell',
                    template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-form-text-input\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (changed)=\"onValueChanged()\">\n      </layout-form-text-input>\n    </div>\n  ",
                    styles: [""]
                }] }
    ];
    return TextEditCellComponent;
}());
export { TextEditCellComponent };
if (false) {
    /** @type {?} */
    TextEditCellComponent.prototype.formGroup;
    /** @type {?} */
    TextEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    TextEditCellComponent.prototype.params;
    /** @type {?} */
    TextEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    TextEditCellComponent.prototype.rowId;
    /** @type {?} */
    TextEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC90ZXh0LWVkaXQtY2VsbC90ZXh0LWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFJMUM7SUFBQTtJQW1EQSxDQUFDOzs7OztJQTNCQyxzQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO0lBQ0gsQ0FBQzs7Ozs7SUFFRCx1Q0FBTzs7OztJQUFQLFVBQVEsTUFBVztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BFLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwRDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELDhDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELHVDQUFPOzs7SUFBUDtRQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDOztnQkFsREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQ0FBaUM7b0JBQzNDLFFBQVEsRUFBRSxvVEFXVDs7aUJBRUY7O0lBb0NELDRCQUFDO0NBQUEsQUFuREQsSUFtREM7U0FuQ1kscUJBQXFCOzs7SUFDaEMsMENBQTRCOztJQUM1QixvQ0FBVzs7Ozs7SUFDWCx1Q0FBb0I7O0lBQ3BCLHNDQUFhOzs7OztJQUNiLHNDQUFtQjs7SUFDbkIsMkNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC10ZXh0LWVkaXQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXZcclxuICAgICAgKm5nSWY9XCJmb3JtR3JvdXBcIlxyXG4gICAgICBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tdGV4dC1pbnB1dFxyXG4gICAgICAgIFtjbGFzcy5ub3QtdmFsaWRdPVwiIWlzVmFsaWQoKVwiXHJcbiAgICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICAgIFtpc1JlYWRPbmx5XT1cImlzUmVhZE9ubHlcIlxyXG4gICAgICAgIChjaGFuZ2VkKT1cIm9uVmFsdWVDaGFuZ2VkKClcIj5cclxuICAgICAgPC9sYXlvdXQtZm9ybS10ZXh0LWlucHV0PlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi90ZXh0LWVkaXQtY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUZXh0RWRpdENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBwdWJsaWMga2V5O1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHVibGljIHZhbHVlO1xyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwdWJsaWMgaXNSZWFkT25seTogYm9vbGVhbjtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZSA/IHRoaXMucGFyYW1zLnZhbHVlLnRvU3RyaW5nKCkgOiAnJztcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gIXRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLmN1c3RvbUVkaXRhYmxlRnVuY3Rpb24odGhpcy5wYXJhbXMuZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKHBhcmFtczogYW55KSB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5wYXJhbXMuY29udGV4dC5mb3JtR3JvdXAuY29udHJvbHNbdGhpcy5yb3dJZF07XHJcbiAgICBpZiAodGhpcy5mb3JtR3JvdXApIHtcclxuICAgICAgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnBhdGNoVmFsdWUodGhpcy52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIG9uVmFsdWVDaGFuZ2VkKCkge1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZTtcclxuICAgIHRoaXMucGFyYW1zLnNldFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiAhKCh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZXJyb3JzICYmICh0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkuZGlydHkgfHwgdGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLnRvdWNoZWQpKSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==