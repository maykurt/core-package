/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var DateEditCellComponent = /** @class */ (function () {
    function DateEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    DateEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = params.context.createKey(this.params.columnApi, this.params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    DateEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    DateEditCellComponent.prototype.onDateChanges = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        this.value = this.formGroup.at(this.key).value ? this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    };
    /**
     * @return {?}
     */
    DateEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    DateEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-date-edit-cell',
                    template: "\n    <div *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\"\n      class=\"input-group\" style=\"width:100%; height:30px;margin-top:2px;\">\n      <layout-form-datepicker-input style=\"width:100%;\"\n\t\t\t[class.not-valid]=\"!isValid()\"\n      [formControlName]=\"key\"\n      [isReadOnly]=\"isReadOnly\"\n\t\t  [settings]= \"settings\"\n      (dateChanges)= \"onDateChanges($event)\"></layout-form-datepicker-input>\n    </div>\n  ",
                    styles: [":host /deep/ .not-valid .ui-calendar input{border:1px solid red}"]
                }] }
    ];
    return DateEditCellComponent;
}());
export { DateEditCellComponent };
if (false) {
    /** @type {?} */
    DateEditCellComponent.prototype.formGroup;
    /** @type {?} */
    DateEditCellComponent.prototype.settings;
    /** @type {?} */
    DateEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    DateEditCellComponent.prototype.params;
    /** @type {?} */
    DateEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC9kYXRlLWVkaXQtY2VsbC9kYXRlLWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNMUM7SUFBQTtJQTBEQSxDQUFDOzs7OztJQWhDQyxzQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMxRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUVqQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFO1lBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pHO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7WUFDN0csSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7U0FDOUQ7SUFDSCxDQUFDOzs7OztJQUVELHVDQUFPOzs7O0lBQVAsVUFBUSxNQUFXO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELDZDQUFhOzs7O0lBQWIsVUFBYyxJQUFZO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzFGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsdUNBQU87OztJQUFQO1FBQ0UsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9ILENBQUM7O2dCQXpERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlDQUFpQztvQkFDM0MsUUFBUSxFQUFFLHdiQVdUOztpQkFFRjs7SUEyQ0QsNEJBQUM7Q0FBQSxBQTFERCxJQTBEQztTQTFDWSxxQkFBcUI7OztJQUNoQywwQ0FBNEI7O0lBQzVCLHlDQUF1Qjs7SUFDdkIsb0NBQWdCOzs7OztJQUVoQixzQ0FBYzs7Ozs7SUFDZCxzQ0FBbUI7Ozs7O0lBQ25CLHVDQUFvQjs7SUFDcEIsMkNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgRGF0ZVNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL2RhdGUtc2V0dGluZ3MubW9kZWwnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1kYXRlLWVkaXQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgKm5nSWY9XCJmb3JtR3JvdXBcIlxyXG4gICAgICBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiXHJcbiAgICAgIGNsYXNzPVwiaW5wdXQtZ3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7IGhlaWdodDozMHB4O21hcmdpbi10b3A6MnB4O1wiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tZGF0ZXBpY2tlci1pbnB1dCBzdHlsZT1cIndpZHRoOjEwMCU7XCJcclxuXHRcdFx0W2NsYXNzLm5vdC12YWxpZF09XCIhaXNWYWxpZCgpXCJcclxuICAgICAgW2Zvcm1Db250cm9sTmFtZV09XCJrZXlcIlxyXG4gICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuXHRcdCAgW3NldHRpbmdzXT0gXCJzZXR0aW5nc1wiXHJcbiAgICAgIChkYXRlQ2hhbmdlcyk9IFwib25EYXRlQ2hhbmdlcygkZXZlbnQpXCI+PC9sYXlvdXQtZm9ybS1kYXRlcGlja2VyLWlucHV0PlxyXG4gICAgPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9kYXRlLWVkaXQtY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRlRWRpdENlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAge1xyXG4gIHB1YmxpYyBmb3JtR3JvdXA6IEZvcm1BcnJheTtcclxuICBzZXR0aW5nczogRGF0ZVNldHRpbmdzO1xyXG4gIHB1YmxpYyBrZXk6IGFueTtcclxuXHJcbiAgcHJpdmF0ZSB2YWx1ZTtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwdWJsaWMgaXNSZWFkT25seTogYm9vbGVhbjtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gcGFyYW1zLmNvbnRleHQuY3JlYXRlS2V5KHRoaXMucGFyYW1zLmNvbHVtbkFwaSwgdGhpcy5wYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZSA/IHRoaXMucGFyYW1zLnZhbHVlIDogbnVsbDtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMgJiYgdGhpcy5wYXJhbXMuY29sRGVmLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgdGhpcy5zZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgaWYgKHRoaXMuZm9ybUdyb3VwKSB7XHJcbiAgICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBvbkRhdGVDaGFuZ2VzKGRhdGU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZSA/IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS52YWx1ZSA6IG51bGw7XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICB9XHJcblxyXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISgodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmVycm9ycyAmJiAodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmRpcnR5IHx8IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS50b3VjaGVkKSkpO1xyXG4gIH1cclxufVxyXG4iXX0=