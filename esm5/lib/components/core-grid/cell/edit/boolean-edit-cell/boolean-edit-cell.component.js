/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var BooleanEditCellComponent = /** @class */ (function () {
    function BooleanEditCellComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    BooleanEditCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        if (this.params.value === undefined) {
            this.value = false;
            this.params.setValue(this.value);
        }
        else {
            this.value = this.params.value;
        }
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    BooleanEditCellComponent.prototype.refresh = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    };
    /**
     * @return {?}
     */
    BooleanEditCellComponent.prototype.onValueChanged = /**
     * @return {?}
     */
    function () {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    };
    /**
     * @return {?}
     */
    BooleanEditCellComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    };
    BooleanEditCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-boolean-edit-cell',
                    template: "\n    <div  *ngIf=\"formGroup\"  class=\"grid-boolean-container\" [formGroup]=\"formGroup\">\n      <layout-form-checkbox\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (checkChanged)=\"onValueChanged()\">\n      </layout-form-checkbox>\n    </div>\n  ",
                    styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
                }] }
    ];
    return BooleanEditCellComponent;
}());
export { BooleanEditCellComponent };
if (false) {
    /** @type {?} */
    BooleanEditCellComponent.prototype.formGroup;
    /** @type {?} */
    BooleanEditCellComponent.prototype.key;
    /**
     * @type {?}
     * @private
     */
    BooleanEditCellComponent.prototype.params;
    /** @type {?} */
    BooleanEditCellComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    BooleanEditCellComponent.prototype.rowId;
    /** @type {?} */
    BooleanEditCellComponent.prototype.isReadOnly;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1lZGl0LWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29yZS1ncmlkL2NlbGwvZWRpdC9ib29sZWFuLWVkaXQtY2VsbC9ib29sZWFuLWVkaXQtY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFJMUM7SUFBQTtJQXdEQSxDQUFDOzs7OztJQWpDQyx5Q0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDbkMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2xDO2FBQU07WUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFFakMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRTtZQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqRztJQUNILENBQUM7Ozs7O0lBRUQsMENBQU87Ozs7SUFBUCxVQUFRLE1BQVc7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7SUFFRCxpREFBYzs7O0lBQWQ7UUFDRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7SUFFRCwwQ0FBTzs7O0lBQVA7UUFDRSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0gsQ0FBQzs7Z0JBdkRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsb0NBQW9DO29CQUM5QyxRQUFRLEVBQUUsMFVBU1Q7O2lCQUVGOztJQTJDRCwrQkFBQztDQUFBLEFBeERELElBd0RDO1NBMUNZLHdCQUF3Qjs7O0lBQ25DLDZDQUE0Qjs7SUFDNUIsdUNBQVc7Ozs7O0lBQ1gsMENBQW9COztJQUNwQix5Q0FBYTs7Ozs7SUFDYix5Q0FBbUI7O0lBRW5CLDhDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQ2VsbFJlbmRlcmVyQW5ndWxhckNvbXAgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtYm9vbGVhbi1lZGl0LWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2ICAqbmdJZj1cImZvcm1Hcm91cFwiICBjbGFzcz1cImdyaWQtYm9vbGVhbi1jb250YWluZXJcIiBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tY2hlY2tib3hcclxuICAgICAgICBbY2xhc3Mubm90LXZhbGlkXT1cIiFpc1ZhbGlkKClcIlxyXG4gICAgICAgIFtmb3JtQ29udHJvbE5hbWVdPVwia2V5XCJcclxuICAgICAgICBbaXNSZWFkT25seV09XCJpc1JlYWRPbmx5XCJcclxuICAgICAgICAoY2hlY2tDaGFuZ2VkKT1cIm9uVmFsdWVDaGFuZ2VkKClcIj5cclxuICAgICAgPC9sYXlvdXQtZm9ybS1jaGVja2JveD5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vYm9vbGVhbi1lZGl0LWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQm9vbGVhbkVkaXRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcbiAgcHVibGljIGtleTtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuICBwcml2YXRlIHJvd0lkOiBhbnk7XHJcblxyXG4gIHB1YmxpYyBpc1JlYWRPbmx5OiBib29sZWFuO1xyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5rZXkgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmNyZWF0ZUtleSh0aGlzLnBhcmFtcy5jb2x1bW5BcGksIHBhcmFtcy5jb2x1bW4pO1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLnZhbHVlID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy52YWx1ZSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZTtcclxuICAgIH1cclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuaXNSZWFkT25seSA9ICF0aGlzLnBhcmFtcy5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5jdXN0b21FZGl0YWJsZUZ1bmN0aW9uKHRoaXMucGFyYW1zLmRhdGEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaChwYXJhbXM6IGFueSkge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMucGFyYW1zLmNvbnRleHQuZm9ybUdyb3VwLmNvbnRyb2xzW3RoaXMucm93SWRdO1xyXG4gICAgaWYgKHRoaXMuZm9ybUdyb3VwKSB7XHJcbiAgICAgIHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS5wYXRjaFZhbHVlKHRoaXMudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBvblZhbHVlQ2hhbmdlZCgpIHtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLmZvcm1Hcm91cC5hdCh0aGlzLmtleSkudmFsdWU7XHJcbiAgICB0aGlzLnBhcmFtcy5zZXRWYWx1ZSh0aGlzLnZhbHVlKTtcclxuICB9XHJcblxyXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gISgodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmVycm9ycyAmJiAodGhpcy5mb3JtR3JvdXAuYXQodGhpcy5rZXkpLmRpcnR5IHx8IHRoaXMuZm9ybUdyb3VwLmF0KHRoaXMua2V5KS50b3VjaGVkKSkpO1xyXG4gIH1cclxufVxyXG4iXX0=