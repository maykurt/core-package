/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ComparisonList, ComparisonType, DefaultComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var SelectFilterCellComponent = /** @class */ (function () {
    function SelectFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    SelectFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            this.settings = this.params.column.getColDef().cellEditorParams.settings;
            this.getListData();
        }
        this.dataSubscription = this.params.column.getColDef().cellEditorParams.data.subscribe(function (x) {
            _this.getListData();
        });
    };
    /**
     * @return {?}
     */
    SelectFilterCellComponent.prototype.getListData = /**
     * @return {?}
     */
    function () {
        if (this.params.column.getColDef().cellEditorParams.dataSource) {
            this.listData = this.params.column['colDef'].cellEditorParams.dataSource;
            // this.clearSubscription();
        }
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    SelectFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        var _this = this;
        this.currentValue = newValue && newValue[this.settings.valueField] ? newValue[this.settings.valueField] : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe(function (result) {
                if (result.value) {
                    if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                        _this.params.serviceAccess.applyChangesToFilter(_this.settings && ((/** @type {?} */ (_this.settings))).sameFieldWith ?
                            ((/** @type {?} */ (_this.settings))).sameFieldWith : _this.params.field, _this.buildModel());
                    }
                    _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                    _this.prevValue = _this.currentValue;
                }
                else {
                    _this.currentValue = null;
                    _this.formGroup.patchValue({
                        filterValue: _this.prevValue || (_this.prevValue && parseInt(_this.prevValue, 10) === 0) ? _this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    SelectFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    SelectFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            /** @type {?} */
            var newValue = {};
            newValue[this.settings.valueField] = this.currentValue;
            this.valueChanged(newValue);
        }
    };
    /**
     * @return {?}
     */
    SelectFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    };
    /**
     * @return {?}
     */
    SelectFilterCellComponent.prototype.clearSubscription = /**
     * @return {?}
     */
    function () {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    SelectFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    SelectFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.clearSubscription();
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    SelectFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-select-filter-cell',
                    template: "\n    <div class=\"input-group\">\n      <div *ngIf=\"settings\" [formGroup]=\"formGroup\" style=\"width:100%;    padding-top: 4px;\">\n        <layout-static-selector\n          #selector\n          formControlName=\"filterValue\"\n          (changed)=\"valueChanged($event)\"\n          [dataSource]=\"listData\"\n          placeholder=\"{{filterKey|translate}}\"\n          [valueField]=\"this.settings.valueField\"\n          [labelField]=\"this.settings.labelField\"\n        >\n        </layout-static-selector>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"selectFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n\n  ",
                    styles: [":host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container .ng-placeholder{font-weight:400;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#6c7591}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                }] }
    ];
    /** @nocollapse */
    SelectFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return SelectFilterCellComponent;
}());
export { SelectFilterCellComponent };
if (false) {
    /** @type {?} */
    SelectFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    SelectFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    SelectFilterCellComponent.prototype.listData;
    /** @type {?} */
    SelectFilterCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.params;
    /** @type {?} */
    SelectFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    SelectFilterCellComponent.prototype.selectorFormGroup;
    /** @type {?} */
    SelectFilterCellComponent.prototype.formGroup;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.dataSubscription;
    /** @type {?} */
    SelectFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    SelectFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    SelectFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2ZpbHRlci9zZWxlY3QtZmlsdGVyLWNlbGwvc2VsZWN0LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRzlHLE9BQU8sRUFBYSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUV2RCxzQ0FFQzs7O0lBREMsaUNBQTRCOzs7OztBQUc5QiwwQ0FFQzs7O0lBREMscUNBQWM7O0FBR2hCO0lBb0RFLG1DQUFvQixFQUFlLEVBQVUsU0FBMkI7UUFBcEQsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBVnhFLGVBQVUsR0FBUSxFQUFFLENBQUM7UUFNZCxjQUFTLEdBQUcsWUFBWSxDQUFDO1FBSzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDN0IsV0FBVyxFQUFFLElBQUk7U0FDbEIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ3JDLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDLE9BQU87U0FDN0MsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwwQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUFsQixpQkFzQkM7UUFyQkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7YUFDckQsU0FBUyxDQUFDLFVBQUMsS0FBVTtZQUNwQixJQUFJLEtBQUssRUFBRTtnQkFDVCxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMvQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUN6RSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFBLENBQUM7WUFDdEYsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQzs7OztJQUVELCtDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFO1lBQzlELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO1lBQ3pFLDRCQUE0QjtTQUM3QjtJQUNILENBQUM7Ozs7O0lBRUQsZ0RBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFBckIsaUJBMEJDO1FBekJDLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQy9HLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7Z0JBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtvQkFDaEIsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7d0JBQ2pFLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUM1QyxLQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsbUJBQWtCLEtBQUksQ0FBQyxRQUFRLEVBQUEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUNoRSxDQUFDLG1CQUFrQixLQUFJLENBQUMsUUFBUSxFQUFBLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3FCQUM3RjtvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBRWxFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQztpQkFDcEM7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ3pCLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDO3dCQUN4QixXQUFXLEVBQUUsS0FBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUk7cUJBQzlHLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUNwQztJQUVILENBQUM7Ozs7O0lBRUQsd0RBQW9COzs7O0lBQXBCLFVBQXFCLFdBQWlDO1FBQ3BELHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFFRCwyQ0FBTzs7OztJQUFQLFVBQVEsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7O2dCQUNyRCxRQUFRLEdBQUcsRUFBRTtZQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDN0I7SUFDSCxDQUFDOzs7O0lBRUQsOENBQVU7OztJQUFWO1FBQ0UsT0FBTztZQUNMLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSzs7WUFDM0QsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZO1NBQzFCLENBQUM7SUFDSixDQUFDOzs7O0lBRUQscURBQWlCOzs7SUFBakI7UUFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDckM7SUFDSCxDQUFDOzs7O0lBRUQsMERBQXNCOzs7SUFBdEI7UUFBQSxpQkFlQzs7WUFkTyxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsVUFBVSxFQUFaLENBQVksQ0FBQzs7WUFFM0YsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDO1FBRTdFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNOztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNmLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztvQkFDWCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCwrQ0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV6QixJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOztnQkE5S0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxxQ0FBcUM7b0JBQy9DLFFBQVEsRUFBRSxvbUNBMkJUOztpQkFFRjs7OztnQkEzQ21CLFdBQVc7Z0JBRXRCLGdCQUFnQjs7SUF5THpCLGdDQUFDO0NBQUEsQUEvS0QsSUErS0M7U0EvSVkseUJBQXlCOzs7SUFHcEMsaURBQTRCOztJQUM1Qiw4Q0FBeUI7O0lBRXpCLDZDQUFxQjs7SUFDckIsNkNBQWdEOzs7OztJQUVoRCwyQ0FBb0I7O0lBQ3BCLCtDQUFxQjs7SUFDckIsc0RBQTZCOztJQUM3Qiw4Q0FBcUI7Ozs7O0lBQ3JCLHFEQUF1Qzs7SUFFdkMsZ0RBQTJCOztJQUMzQiw4Q0FBZ0M7Ozs7O0lBRWhDLDBEQUE0Qzs7Ozs7SUFFaEMsdUNBQXVCOzs7OztJQUFFLDhDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFnRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgSUZsb2F0aW5nRmlsdGVyLCBJRmxvYXRpbmdGaWx0ZXJQYXJhbXMsIFNlcmlhbGl6ZWRUZXh0RmlsdGVyIH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHknO1xyXG5pbXBvcnQgeyBDb21wYXJpc29uTGlzdCwgQ29tcGFyaXNvblR5cGUsIERlZmF1bHRDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBMb3ZTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL21vZGVscy9sb3Ytc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBTZWxlY3RvclNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL3NlbGVjdG9yLXNldHRpbmdzLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLXNlbGVjdC1maWx0ZXItY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICA8ZGl2ICpuZ0lmPVwic2V0dGluZ3NcIiBbZm9ybUdyb3VwXT1cImZvcm1Hcm91cFwiIHN0eWxlPVwid2lkdGg6MTAwJTsgICAgcGFkZGluZy10b3A6IDRweDtcIj5cclxuICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvclxyXG4gICAgICAgICAgI3NlbGVjdG9yXHJcbiAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJmaWx0ZXJWYWx1ZVwiXHJcbiAgICAgICAgICAoY2hhbmdlZCk9XCJ2YWx1ZUNoYW5nZWQoJGV2ZW50KVwiXHJcbiAgICAgICAgICBbZGF0YVNvdXJjZV09XCJsaXN0RGF0YVwiXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAgIFt2YWx1ZUZpZWxkXT1cInRoaXMuc2V0dGluZ3MudmFsdWVGaWVsZFwiXHJcbiAgICAgICAgICBbbGFiZWxGaWVsZF09XCJ0aGlzLnNldHRpbmdzLmxhYmVsRmllbGRcIlxyXG4gICAgICAgID5cclxuICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPGRpdiBjbGFzcz1cImdyaWQtZmlsdGVyLWljb25cIj5cclxuICAgICAgICA8Y29yZS1pY29uICNhY3R1YWxUYXJnZXQgaWNvbj1cImZpbHRlclwiIChjbGljayk9XCJvcC50b2dnbGUoJGV2ZW50KVwiPjwvY29yZS1pY29uPlxyXG4gICAgICA8L2Rpdj5cclxuXHJcbiAgICAgIDxwLW92ZXJsYXlQYW5lbCAjb3AgYXBwZW5kVG89XCJib2R5XCIgZGlzbWlzc2FibGU9XCJmYWxzZVwiPlxyXG4gICAgICAgIDxkaXYgKm5nSWY9XCJzZWxlY3RvckZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwic2VsZWN0b3JGb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvciBmb3JtQ29udHJvbE5hbWU9XCJzZWxlY3RGaWx0ZXJWYWx1ZVwiIChjaGFuZ2VkKT1cImNoYW5nZWQoJGV2ZW50KVwiIFtkYXRhU291cmNlXT1cImRhdGFTb3VyY2VcIiBbY2xlYXJhYmxlXT1cImZhbHNlXCIgW3NlYXJjaGFibGVdPVwiZmFsc2VcIj5cclxuICAgICAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9wLW92ZXJsYXlQYW5lbD5cclxuICAgIDwvZGl2PlxyXG5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL3NlbGVjdC1maWx0ZXItY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RGaWx0ZXJDZWxsQ29tcG9uZW50XHJcbiAgaW1wbGVtZW50cyBJRmxvYXRpbmdGaWx0ZXI8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2UsIEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgQWdGcmFtZXdvcmtDb21wb25lbnQ8RmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBPbkRlc3Ryb3kge1xyXG5cclxuICBwdWJsaWMgY3VycmVudFZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIHByZXZWYWx1ZTogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgbGlzdERhdGE6IGFueTtcclxuICBwdWJsaWMgc2V0dGluZ3M6IExvdlNldHRpbmdzIHwgU2VsZWN0b3JTZXR0aW5ncztcclxuXHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBkYXRhU291cmNlOiBhbnkgPSBbXTtcclxuICBzZWxlY3RvckZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gIHByaXZhdGUgZGF0YVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwdWJsaWMgcGxhY2Vob2xkZXI6IHN0cmluZztcclxuICBwdWJsaWMgZmlsdGVyS2V5ID0gJ0dyaWRGaWx0ZXInO1xyXG5cclxuICBwcml2YXRlIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcclxuICAgIHRoaXMuZm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgIGZpbHRlclZhbHVlOiBudWxsXHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnNlbGVjdG9yRm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgIHNlbGVjdEZpbHRlclZhbHVlOiBEZWZhdWx0Q29tcGFyaXNvbi5FcXVhbFRvXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcblxyXG4gICAgdGhpcy5wbGFjZWhvbGRlciA9IHBhcmFtcy5jb2x1bW4uY29sRGVmLmhlYWRlck5hbWUgfHwgcGFyYW1zLmNvbHVtbi5jb2xEZWYuZmllbGQ7XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgIHRoaXMuZ2V0TGlzdERhdGEoKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmRhdGFTdWJzY3JpcHRpb24gPSB0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5kYXRhLnN1YnNjcmliZSh4ID0+IHtcclxuICAgICAgdGhpcy5nZXRMaXN0RGF0YSgpO1xyXG4gICAgfSk7XHJcblxyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdERhdGEoKSB7XHJcbiAgICBpZiAodGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLmxpc3REYXRhID0gdGhpcy5wYXJhbXMuY29sdW1uWydjb2xEZWYnXS5jZWxsRWRpdG9yUGFyYW1zLmRhdGFTb3VyY2U7XHJcbiAgICAgIC8vIHRoaXMuY2xlYXJTdWJzY3JpcHRpb24oKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk6IHZvaWQge1xyXG4gICAgdGhpcy5jdXJyZW50VmFsdWUgPSBuZXdWYWx1ZSAmJiBuZXdWYWx1ZVt0aGlzLnNldHRpbmdzLnZhbHVlRmllbGRdID8gbmV3VmFsdWVbdGhpcy5zZXR0aW5ncy52YWx1ZUZpZWxkXSA6IG51bGw7XHJcbiAgICBpZiAodGhpcy5wYXJhbXMuc2VydmljZUFjY2VzcyAmJiB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KSB7XHJcbiAgICAgIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcihcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0dGluZ3MgJiYgKDxTZWxlY3RvclNldHRpbmdzPnRoaXMuc2V0dGluZ3MpLnNhbWVGaWVsZFdpdGggP1xyXG4gICAgICAgICAgICAgICAgICAoPFNlbGVjdG9yU2V0dGluZ3M+dGhpcy5zZXR0aW5ncykuc2FtZUZpZWxkV2l0aCA6IHRoaXMucGFyYW1zLmZpZWxkLCB0aGlzLmJ1aWxkTW9kZWwoKSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5mb3JtR3JvdXAucGF0Y2hWYWx1ZSh7XHJcbiAgICAgICAgICAgICAgZmlsdGVyVmFsdWU6IHRoaXMucHJldlZhbHVlIHx8ICh0aGlzLnByZXZWYWx1ZSAmJiBwYXJzZUludCh0aGlzLnByZXZWYWx1ZSwgMTApID09PSAwKSA/IHRoaXMucHJldlZhbHVlIDogbnVsbFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgb25QYXJlbnRNb2RlbENoYW5nZWQocGFyZW50TW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyKTogdm9pZCB7XHJcbiAgICAvLyBpZiAoIXBhcmVudE1vZGVsKSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gcGFyZW50TW9kZWwuZmlsdGVyO1xyXG4gICAgLy8gfVxyXG4gIH1cclxuXHJcbiAgY2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIChkYXRhLklkIHx8IGRhdGEuSWQgPT09IDApICYmIHRoaXMuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IG5ld1ZhbHVlID0ge307XHJcbiAgICAgIG5ld1ZhbHVlW3RoaXMuc2V0dGluZ3MudmFsdWVGaWVsZF0gPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQobmV3VmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYnVpbGRNb2RlbCgpOiBTZXJpYWxpemVkVGV4dEZpbHRlciB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmaWx0ZXJUeXBlOiAndGV4dCcsXHJcbiAgICAgIHR5cGU6IHRoaXMuc2VsZWN0b3JGb3JtR3JvdXAuZ2V0KCdzZWxlY3RGaWx0ZXJWYWx1ZScpLnZhbHVlLCAvLyBjYW4gYmUgc2VuZCBpbiBoZXJlXHJcbiAgICAgIGZpbHRlcjogdGhpcy5jdXJyZW50VmFsdWVcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBjbGVhclN1YnNjcmlwdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmRhdGFTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5kYXRhU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuRGVmYXVsdCkubWFwKHggPT4geC5EZWZpbml0aW9uKTtcclxuXHJcbiAgICBjb25zdCBkYXRhU291cmNlID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5EZWZhdWx0KTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoa2V5TGlzdClcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xyXG4gICAgICAgIGtleUxpc3QuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdHJhbnNsYXRlZFZhbHVlID0gcmVzdWx0W2tleV07XHJcbiAgICAgICAgICBkYXRhU291cmNlW2NvdW50ZXJdLkRlZmluaXRpb24gPSB0cmFuc2xhdGVkVmFsdWU7XHJcbiAgICAgICAgICBjb3VudGVyKys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xlYXJTdWJzY3JpcHRpb24oKTtcclxuXHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19