/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ComparisonList, ComparisonType, TextComparison } from '../../../../../enums/comparison-type.enum';
import { CoreGridColumnType } from '../../../../../enums/index';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var TextFilterCellComponent = /** @class */ (function () {
    function TextFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            textFilterValue: TextComparison.Contains
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    TextFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        // this.dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(function (model) {
            _this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe(function (result) {
                    if (result.value) {
                        if (params.column.colDef.columnType === CoreGridColumnType.TextSelect) {
                            if (params.serviceAccess) {
                                params.serviceAccess.textSelectFindValuesOnFilter(params.field, params.column.colDef.cellEditorParams.settings, _this.buildModel());
                            }
                        }
                        else {
                            if (params.serviceAccess && params.serviceAccess.filter) {
                                params.serviceAccess.applyChangesToFilter(params.field, _this.buildModel());
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            _this.prevValue = _this.currentValue;
                        }
                    }
                    else {
                        _this.currentValue = _this.prevValue || null;
                    }
                });
            }
        });
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    TextFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        this.modelChanged.next(newValue);
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    TextFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    TextFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && data.Id) {
            this.valueChanged(this.currentValue);
        }
    };
    /**
     * @return {?}
     */
    TextFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var value;
        /** @type {?} */
        var type = this.formGroup.get('textFilterValue').value;
        if (this.currentValue) {
            value = this.currentValue;
        }
        else if (type === TextComparison.IsNullOrWhiteSpace ||
            type === TextComparison.IsNotNullNorWhiteSpace ||
            type === TextComparison.IsNull ||
            type === TextComparison.IsNotNull ||
            type === TextComparison.IsNotEmpty ||
            type === TextComparison.IsEmpty) {
            value = ' ';
        }
        return {
            filterType: 'string',
            type: type.toString(),
            filter: value
        };
    };
    /**
     * @return {?}
     */
    TextFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Text).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    TextFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    TextFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-text-filter-cell',
                    template: "\n    <div class=\"input-group\">\n      <layout-form-text-input style=\"width: 100%;\"\n          [(ngModel)]=\"currentValue\"\n          [clearable] = \"true\"\n          placeholder=\"{{filterKey|translate}}\"\n          (ngModelChange)=\"valueChanged($event)\">\n      </layout-form-text-input>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"textFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n  "
                }] }
    ];
    /** @nocollapse */
    TextFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return TextFilterCellComponent;
}());
export { TextFilterCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    TextFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    TextFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    TextFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    TextFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    TextFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    TextFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    TextFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvdGV4dC1maWx0ZXItY2VsbC90ZXh0LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUc5QyxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUMzRyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVoRSxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQUdoQjtJQXlDRSxpQ0FBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQWJoRSxpQkFBWSxHQUFvQixJQUFJLE9BQU8sRUFBVSxDQUFDO1FBSzlELGVBQVUsR0FBUSxFQUFFLENBQUM7UUFJZCxjQUFTLEdBQUcsWUFBWSxDQUFDO1FBSzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDN0IsZUFBZSxFQUFFLGNBQWMsQ0FBQyxRQUFRO1NBQ3pDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsd0NBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFBbEIsaUJBOENDO1FBN0NDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUVqRixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUU5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2FBQ3JELFNBQVMsQ0FBQyxVQUFDLEtBQVU7WUFDcEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLDZFQUE2RTtRQUU3RSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQ3BELFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FDbkIsQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLO1lBQ2YsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFFMUIsSUFBSSxNQUFNLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3JFLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7cUJBQ3hDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7b0JBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTt3QkFFaEIsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssa0JBQWtCLENBQUMsVUFBVSxFQUFFOzRCQUNyRSxJQUFJLE1BQU0sQ0FBQyxhQUFhLEVBQUU7Z0NBQ3hCLE1BQU0sQ0FBQyxhQUFhLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLEtBQUssRUFDNUQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDOzZCQUN0RTt5QkFFRjs2QkFBTTs0QkFDTCxJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7Z0NBQ3ZELE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQzs2QkFDNUU7NEJBRUQsS0FBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDOzRCQUNsRSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUM7eUJBQ3BDO3FCQUNGO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUM7cUJBQzVDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOENBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxzREFBb0I7Ozs7SUFBcEIsVUFBcUIsV0FBaUM7UUFFcEQscUNBQXFDO1FBR3JDLHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFFRCx5Q0FBTzs7OztJQUFQLFVBQVEsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7O0lBRUQsNENBQVU7OztJQUFWOztZQUNNLEtBQWE7O1lBQ1gsSUFBSSxHQUFXLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsS0FBSztRQUNoRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDM0I7YUFBTSxJQUFJLElBQUksS0FBSyxjQUFjLENBQUMsa0JBQWtCO1lBQ25ELElBQUksS0FBSyxjQUFjLENBQUMsc0JBQXNCO1lBQzlDLElBQUksS0FBSyxjQUFjLENBQUMsTUFBTTtZQUM5QixJQUFJLEtBQUssY0FBYyxDQUFDLFNBQVM7WUFDakMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxVQUFVO1lBQ2xDLElBQUksS0FBSyxjQUFjLENBQUMsT0FBTyxFQUFFO1lBQ2pDLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDYjtRQUVELE9BQU87WUFDTCxVQUFVLEVBQUUsUUFBUTtZQUNwQixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNyQixNQUFNLEVBQUUsS0FBSztTQUNkLENBQUM7SUFDSixDQUFDOzs7O0lBR0Qsd0RBQXNCOzs7SUFBdEI7UUFBQSxpQkFlQzs7WUFkTyxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsVUFBVSxFQUFaLENBQVksQ0FBQzs7WUFFeEYsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1FBRTFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNOztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNmLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztvQkFDWCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCw2Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNqQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0M7UUFFRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOztnQkFwS0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxtQ0FBbUM7b0JBQzdDLFFBQVEsRUFBRSx1MkJBb0JUO2lCQUNGOzs7O2dCQXBDbUIsV0FBVztnQkFHdEIsZ0JBQWdCOztJQStLekIsOEJBQUM7Q0FBQSxBQXJLRCxJQXFLQztTQTdJWSx1QkFBdUI7Ozs7OztJQUdsQyx5Q0FBb0I7Ozs7O0lBQ3BCLCtDQUE4RDs7Ozs7SUFDOUQsMkRBQStDOztJQUUvQywrQ0FBNEI7O0lBQzVCLDRDQUF5Qjs7SUFDekIsNkNBQXFCOztJQUNyQiw0Q0FBcUI7O0lBRXJCLDhDQUEyQjs7SUFDM0IsNENBQWdDOzs7OztJQUVoQyx3REFBNEM7Ozs7O0lBRWhDLHFDQUF1Qjs7Ozs7SUFBRSw0Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQ29tcGFyaXNvbkxpc3QsIENvbXBhcmlzb25UeXBlLCBUZXh0Q29tcGFyaXNvbiB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2NvbXBhcmlzb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgQ29yZUdyaWRDb2x1bW5UeXBlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IFRyYW5zbGF0ZUhlbHBlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zZXJ2aWNlcy9jdXN0b20vdHJhbnNsYXRlLmhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBUZXh0RmlsdGVyQ2hhbmdlIHtcclxuICBtb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmxvYXRpbmdGaWx0ZXJQYXJhbXMgZXh0ZW5kcyBJRmxvYXRpbmdGaWx0ZXJQYXJhbXM8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2U+IHtcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtdGV4dC1maWx0ZXItY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tdGV4dC1pbnB1dCBzdHlsZT1cIndpZHRoOiAxMDAlO1wiXHJcbiAgICAgICAgICBbKG5nTW9kZWwpXT1cImN1cnJlbnRWYWx1ZVwiXHJcbiAgICAgICAgICBbY2xlYXJhYmxlXSA9IFwidHJ1ZVwiXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCI+XHJcbiAgICAgIDwvbGF5b3V0LWZvcm0tdGV4dC1pbnB1dD5cclxuXHJcbiAgICAgIDxkaXYgY2xhc3M9XCJncmlkLWZpbHRlci1pY29uXCI+XHJcbiAgICAgICAgPGNvcmUtaWNvbiAjYWN0dWFsVGFyZ2V0IGljb249XCJmaWx0ZXJcIiAoY2xpY2spPVwib3AudG9nZ2xlKCRldmVudClcIj48L2NvcmUtaWNvbj5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8cC1vdmVybGF5UGFuZWwgI29wIGFwcGVuZFRvPVwiYm9keVwiIGRpc21pc3NhYmxlPVwiZmFsc2VcIj5cclxuICAgICAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvciBmb3JtQ29udHJvbE5hbWU9XCJ0ZXh0RmlsdGVyVmFsdWVcIiAoY2hhbmdlZCk9XCJjaGFuZ2VkKCRldmVudClcIiBbZGF0YVNvdXJjZV09XCJkYXRhU291cmNlXCIgW2NsZWFyYWJsZV09XCJmYWxzZVwiIFtzZWFyY2hhYmxlXT1cImZhbHNlXCI+XHJcbiAgICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvcC1vdmVybGF5UGFuZWw+XHJcbiAgICA8L2Rpdj5cclxuICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUZXh0RmlsdGVyQ2VsbENvbXBvbmVudFxyXG4gIGltcGxlbWVudHMgSUZsb2F0aW5nRmlsdGVyPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlLCBGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIEFnRnJhbWV3b3JrQ29tcG9uZW50PEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgT25EZXN0cm95IHtcclxuXHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZDogU3ViamVjdDxzdHJpbmc+ID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xyXG4gIHByaXZhdGUgbW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgcHJldlZhbHVlOiBzdHJpbmc7XHJcbiAgZGF0YVNvdXJjZTogYW55ID0gW107XHJcbiAgZm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcblxyXG4gIHB1YmxpYyBwbGFjZWhvbGRlcjogc3RyaW5nO1xyXG4gIHB1YmxpYyBmaWx0ZXJLZXkgPSAnR3JpZEZpbHRlcic7XHJcblxyXG4gIHByaXZhdGUgdHJhbnNsYXRlU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgdGV4dEZpbHRlclZhbHVlOiBUZXh0Q29tcGFyaXNvbi5Db250YWluc1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG5cclxuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBwYXJhbXMuY29sdW1uLmNvbERlZi5oZWFkZXJOYW1lIHx8IHBhcmFtcy5jb2x1bW4uY29sRGVmLmZpZWxkO1xyXG5cclxuICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG5cclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gdGhpcy5kYXRhU291cmNlID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5UZXh0KTtcclxuXHJcbiAgICB0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbiA9IHRoaXMubW9kZWxDaGFuZ2VkLnBpcGUoXHJcbiAgICAgIGRlYm91bmNlVGltZSgxMDAwKVxyXG4gICAgKS5zdWJzY3JpYmUobW9kZWwgPT4ge1xyXG4gICAgICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG1vZGVsO1xyXG5cclxuICAgICAgaWYgKHBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KSB7XHJcbiAgICAgICAgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQoKVxyXG4gICAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG5cclxuICAgICAgICAgICAgICBpZiAocGFyYW1zLmNvbHVtbi5jb2xEZWYuY29sdW1uVHlwZSA9PT0gQ29yZUdyaWRDb2x1bW5UeXBlLlRleHRTZWxlY3QpIHtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbXMuc2VydmljZUFjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgICBwYXJhbXMuc2VydmljZUFjY2Vzcy50ZXh0U2VsZWN0RmluZFZhbHVlc09uRmlsdGVyKHBhcmFtcy5maWVsZCxcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMuY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLCB0aGlzLmJ1aWxkTW9kZWwoKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKHBhcmFtcy5maWVsZCwgdGhpcy5idWlsZE1vZGVsKCkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSB0aGlzLnByZXZWYWx1ZSB8fCBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICB2YWx1ZUNoYW5nZWQobmV3VmFsdWUpOiB2b2lkIHtcclxuICAgIHRoaXMubW9kZWxDaGFuZ2VkLm5leHQobmV3VmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgb25QYXJlbnRNb2RlbENoYW5nZWQocGFyZW50TW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyKTogdm9pZCB7XHJcblxyXG4gICAgLy8gKioqKip3aWxsIGJlIGRvbmUgd2hlbiBuZWVkZWQqKioqKlxyXG5cclxuXHJcbiAgICAvLyBpZiAoIXBhcmVudE1vZGVsKSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gcGFyZW50TW9kZWwuZmlsdGVyO1xyXG4gICAgLy8gfVxyXG4gIH1cclxuXHJcbiAgY2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEuSWQpIHtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQodGhpcy5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYnVpbGRNb2RlbCgpOiBTZXJpYWxpemVkVGV4dEZpbHRlciB7XHJcbiAgICBsZXQgdmFsdWU6IHN0cmluZztcclxuICAgIGNvbnN0IHR5cGU6IG51bWJlciA9IHRoaXMuZm9ybUdyb3VwLmdldCgndGV4dEZpbHRlclZhbHVlJykudmFsdWU7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gVGV4dENvbXBhcmlzb24uSXNOdWxsT3JXaGl0ZVNwYWNlIHx8XHJcbiAgICAgIHR5cGUgPT09IFRleHRDb21wYXJpc29uLklzTm90TnVsbE5vcldoaXRlU3BhY2UgfHxcclxuICAgICAgdHlwZSA9PT0gVGV4dENvbXBhcmlzb24uSXNOdWxsIHx8XHJcbiAgICAgIHR5cGUgPT09IFRleHRDb21wYXJpc29uLklzTm90TnVsbCB8fFxyXG4gICAgICB0eXBlID09PSBUZXh0Q29tcGFyaXNvbi5Jc05vdEVtcHR5IHx8XHJcbiAgICAgIHR5cGUgPT09IFRleHRDb21wYXJpc29uLklzRW1wdHkpIHtcclxuICAgICAgdmFsdWUgPSAnICc7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZmlsdGVyVHlwZTogJ3N0cmluZycsXHJcbiAgICAgIHR5cGU6IHR5cGUudG9TdHJpbmcoKSxcclxuICAgICAgZmlsdGVyOiB2YWx1ZVxyXG4gICAgfTtcclxuICB9XHJcblxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuVGV4dCkubWFwKHggPT4geC5EZWZpbml0aW9uKTtcclxuXHJcbiAgICBjb25zdCBkYXRhU291cmNlID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5UZXh0KTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoa2V5TGlzdClcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xyXG4gICAgICAgIGtleUxpc3QuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdHJhbnNsYXRlZFZhbHVlID0gcmVzdWx0W2tleV07XHJcbiAgICAgICAgICBkYXRhU291cmNlW2NvdW50ZXJdLkRlZmluaXRpb24gPSB0cmFuc2xhdGVkVmFsdWU7XHJcbiAgICAgICAgICBjb3VudGVyKys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLm1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=