/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ComparisonList, ComparisonType, NumberComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var MaskFilterCellComponent = /** @class */ (function () {
    function MaskFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    MaskFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
                this.maskType = this.settings.maskType;
                this.maxLength = this.settings.maxLength;
            }
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(function (model) {
            _this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe(function (result) {
                    if (result.value) {
                        if (params.serviceAccess && params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(params.field, _this.buildModel());
                        }
                        _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                        _this.prevValue = _this.currentValue;
                    }
                    else {
                        _this.currentValue = _this.prevValue || '';
                    }
                });
            }
        });
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    MaskFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        this.modelChanged.next(newValue);
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    MaskFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    MaskFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    };
    /**
     * @return {?}
     */
    MaskFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        return {
            filterType: 'text',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    };
    /**
     * @return {?}
     */
    MaskFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    MaskFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    MaskFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-mask-filter-cell',
                    template: "\n  <div class=\"input-group\">\n  <layout-form-mask-input style=\"width: 100%;\"\n    [maskType]=\"maskType\"\n    [(ngModel)]=\"currentValue\"\n    [clearable] = \"true\"\n    placeholder=\"{{filterKey|translate}}\"\n    (ngModelChange)=\"valueChanged($event)\">\n  </layout-form-mask-input>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"numberFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n  "
                }] }
    ];
    /** @nocollapse */
    MaskFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return MaskFilterCellComponent;
}());
export { MaskFilterCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    MaskFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    MaskFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    MaskFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    MaskFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    MaskFilterCellComponent.prototype.settings;
    /** @type {?} */
    MaskFilterCellComponent.prototype.maskType;
    /** @type {?} */
    MaskFilterCellComponent.prototype.maxLength;
    /** @type {?} */
    MaskFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    MaskFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    MaskFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvbWFzay1maWx0ZXItY2VsbC9tYXNrLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUM3QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM3RyxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQUdoQjtJQTZDRSxpQ0FBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQWpCaEUsaUJBQVksR0FBb0IsSUFBSSxPQUFPLEVBQVUsQ0FBQztRQU05RCxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBT2QsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUs5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDLE9BQU87U0FDNUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx3Q0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUFsQixpQkF5Q0M7UUF4Q0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7YUFDckQsU0FBUyxDQUFDLFVBQUMsS0FBVTtZQUNwQixJQUFJLEtBQUssRUFBRTtnQkFDVCxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMvQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDNUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7Z0JBQ3pFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7YUFDMUM7U0FDRjtRQUVELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDcEQsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUNuQixDQUFDLFNBQVMsQ0FBQyxVQUFBLEtBQUs7WUFDZixLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUUxQixJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDckUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtxQkFDeEMsU0FBUyxDQUFDLFVBQUMsTUFBVztvQkFDckIsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO3dCQUNoQixJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7NEJBQ3ZELE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQzt5QkFDNUU7d0JBRUQsS0FBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO3dCQUNsRSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUM7cUJBQ3BDO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7cUJBQzFDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOENBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxzREFBb0I7Ozs7SUFBcEIsVUFBcUIsV0FBaUM7UUFFcEQscUNBQXFDO1FBR3JDLHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFHRCx5Q0FBTzs7OztJQUFQLFVBQVEsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7SUFFRCw0Q0FBVTs7O0lBQVY7UUFDRSxPQUFPO1lBQ0wsVUFBVSxFQUFFLE1BQU07WUFDbEIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSzs7WUFDbkQsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZO1NBQzFCLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsd0RBQXNCOzs7SUFBdEI7UUFBQSxpQkFlQzs7WUFkTyxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsVUFBVSxFQUFaLENBQVksQ0FBQzs7WUFFMUYsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1FBRTVFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNOztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNmLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztvQkFDWCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCw2Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNqQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0M7UUFFRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOztnQkF0SkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxtQ0FBbUM7b0JBQzdDLFFBQVEsRUFBRSxnMUJBcUJUO2lCQUNGOzs7O2dCQXJDbUIsV0FBVztnQkFHdEIsZ0JBQWdCOztJQWlLekIsOEJBQUM7Q0FBQSxBQXZKRCxJQXVKQztTQTlIWSx1QkFBdUI7Ozs7OztJQUVsQyx5Q0FBcUM7Ozs7O0lBQ3JDLCtDQUE4RDs7Ozs7SUFDOUQsMkRBQStDOztJQUUvQywrQ0FBNEI7O0lBQzVCLDRDQUF5Qjs7SUFFekIsNkNBQXFCOztJQUNyQiw0Q0FBcUI7O0lBQ3JCLDJDQUE4Qjs7SUFDOUIsMkNBQTBCOztJQUMxQiw0Q0FBMEI7O0lBRTFCLDhDQUEyQjs7SUFDM0IsNENBQWdDOzs7OztJQUVoQyx3REFBNEM7Ozs7O0lBRWhDLHFDQUF1Qjs7Ozs7SUFBRSw0Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgU3ViamVjdCwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGRlYm91bmNlVGltZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgQ29tcGFyaXNvbkxpc3QsIENvbXBhcmlzb25UeXBlLCBOdW1iZXJDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXNrVHlwZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL21hc2stdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgTWFza1NldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL21hc2stc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFRleHRGaWx0ZXJDaGFuZ2Uge1xyXG4gIG1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcjtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGbG9hdGluZ0ZpbHRlclBhcmFtcyBleHRlbmRzIElGbG9hdGluZ0ZpbHRlclBhcmFtczxTZXJpYWxpemVkVGV4dEZpbHRlciwgVGV4dEZpbHRlckNoYW5nZT4ge1xyXG4gIHZhbHVlOiBzdHJpbmc7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1tYXNrLWZpbHRlci1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gIDxsYXlvdXQtZm9ybS1tYXNrLWlucHV0IHN0eWxlPVwid2lkdGg6IDEwMCU7XCJcclxuICAgIFttYXNrVHlwZV09XCJtYXNrVHlwZVwiXHJcbiAgICBbKG5nTW9kZWwpXT1cImN1cnJlbnRWYWx1ZVwiXHJcbiAgICBbY2xlYXJhYmxlXSA9IFwidHJ1ZVwiXHJcbiAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgIChuZ01vZGVsQ2hhbmdlKT1cInZhbHVlQ2hhbmdlZCgkZXZlbnQpXCI+XHJcbiAgPC9sYXlvdXQtZm9ybS1tYXNrLWlucHV0PlxyXG5cclxuICAgIDxkaXYgY2xhc3M9XCJncmlkLWZpbHRlci1pY29uXCI+XHJcbiAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICA8L2Rpdj5cclxuXHJcbiAgICA8cC1vdmVybGF5UGFuZWwgI29wIGFwcGVuZFRvPVwiYm9keVwiIGRpc21pc3NhYmxlPVwiZmFsc2VcIj5cclxuICAgICAgPGRpdiAqbmdJZj1cImZvcm1Hcm91cFwiIFtmb3JtR3JvdXBdPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgICAgIDxsYXlvdXQtc3RhdGljLXNlbGVjdG9yIGZvcm1Db250cm9sTmFtZT1cIm51bWJlckZpbHRlclZhbHVlXCIgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCIgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFtjbGVhcmFibGVdPVwiZmFsc2VcIiBbc2VhcmNoYWJsZV09XCJmYWxzZVwiPlxyXG4gICAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L3Atb3ZlcmxheVBhbmVsPlxyXG4gIDwvZGl2PlxyXG4gIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIE1hc2tGaWx0ZXJDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUZsb2F0aW5nRmlsdGVyPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLFxyXG5UZXh0RmlsdGVyQ2hhbmdlLCBGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIEFnRnJhbWV3b3JrQ29tcG9uZW50PEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgT25EZXN0cm95IHtcclxuICBwcml2YXRlIHBhcmFtczogRmxvYXRpbmdGaWx0ZXJQYXJhbXM7XHJcbiAgcHJpdmF0ZSBtb2RlbENoYW5nZWQ6IFN1YmplY3Q8c3RyaW5nPiA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwdWJsaWMgY3VycmVudFZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIHByZXZWYWx1ZTogc3RyaW5nO1xyXG5cclxuICBkYXRhU291cmNlOiBhbnkgPSBbXTtcclxuICBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuICBwdWJsaWMgc2V0dGluZ3M6IE1hc2tTZXR0aW5ncztcclxuICBwdWJsaWMgbWFza1R5cGU6IE1hc2tUeXBlO1xyXG4gIHB1YmxpYyBtYXhMZW5ndGg/OiBudW1iZXI7XHJcblxyXG4gIHB1YmxpYyBwbGFjZWhvbGRlcjogc3RyaW5nO1xyXG4gIHB1YmxpYyBmaWx0ZXJLZXkgPSAnR3JpZEZpbHRlcic7XHJcblxyXG4gIHByaXZhdGUgdHJhbnNsYXRlU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgbnVtYmVyRmlsdGVyVmFsdWU6IE51bWJlckNvbXBhcmlzb24uRXF1YWxUb1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5wbGFjZWhvbGRlciA9IHBhcmFtcy5jb2x1bW4uY29sRGVmLmhlYWRlck5hbWUgfHwgcGFyYW1zLmNvbHVtbi5jb2xEZWYuZmllbGQ7XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3MpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0gdGhpcy5wYXJhbXMuY29sdW1uLmdldENvbERlZigpLmNlbGxFZGl0b3JQYXJhbXMuc2V0dGluZ3M7XHJcbiAgICAgICAgdGhpcy5tYXNrVHlwZSA9IHRoaXMuc2V0dGluZ3MubWFza1R5cGU7XHJcbiAgICAgICAgdGhpcy5tYXhMZW5ndGggPSB0aGlzLnNldHRpbmdzLm1heExlbmd0aDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMubW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uID0gdGhpcy5tb2RlbENoYW5nZWQucGlwZShcclxuICAgICAgZGVib3VuY2VUaW1lKDEwMDApXHJcbiAgICApLnN1YnNjcmliZShtb2RlbCA9PiB7XHJcbiAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gbW9kZWw7XHJcblxyXG4gICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQpIHtcclxuICAgICAgICBwYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKChyZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuYXBwbHlDaGFuZ2VzVG9GaWx0ZXIocGFyYW1zLmZpZWxkLCB0aGlzLmJ1aWxkTW9kZWwoKSk7XHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5vbkZsb2F0aW5nRmlsdGVyQ2hhbmdlZCh7IG1vZGVsOiB0aGlzLmJ1aWxkTW9kZWwoKSB9KTtcclxuICAgICAgICAgICAgICB0aGlzLnByZXZWYWx1ZSA9IHRoaXMuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gdGhpcy5wcmV2VmFsdWUgfHwgJyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk6IHZvaWQge1xyXG4gICAgdGhpcy5tb2RlbENoYW5nZWQubmV4dChuZXdWYWx1ZSk7XHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuXHJcbiAgICAvLyAqKioqKndpbGwgYmUgZG9uZSB3aGVuIG5lZWRlZCoqKioqXHJcblxyXG5cclxuICAgIC8vIGlmICghcGFyZW50TW9kZWwpIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBwYXJlbnRNb2RlbC5maWx0ZXI7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuXHJcbiAgY2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEuSWQgJiYgdGhpcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQodGhpcy5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYnVpbGRNb2RlbCgpOiBTZXJpYWxpemVkVGV4dEZpbHRlciB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmaWx0ZXJUeXBlOiAndGV4dCcsXHJcbiAgICAgIHR5cGU6IHRoaXMuZm9ybUdyb3VwLmdldCgnbnVtYmVyRmlsdGVyVmFsdWUnKS52YWx1ZSwgLy8gY2FuIGJlIHNlbmQgaW4gaGVyZVxyXG4gICAgICBmaWx0ZXI6IHRoaXMuY3VycmVudFZhbHVlXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGtleUxpc3QgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLk51bWJlcikubWFwKHggPT4geC5EZWZpbml0aW9uKTtcclxuXHJcbiAgICBjb25zdCBkYXRhU291cmNlID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5OdW1iZXIpO1xyXG5cclxuICAgIHRoaXMudHJhbnNsYXRlLmdldChrZXlMaXN0KVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcclxuICAgICAgICBsZXQgY291bnRlciA9IDA7XHJcbiAgICAgICAga2V5TGlzdC5mb3JFYWNoKGtleSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0cmFuc2xhdGVkVmFsdWUgPSByZXN1bHRba2V5XTtcclxuICAgICAgICAgIGRhdGFTb3VyY2VbY291bnRlcl0uRGVmaW5pdGlvbiA9IHRyYW5zbGF0ZWRWYWx1ZTtcclxuICAgICAgICAgIGNvdW50ZXIrKztcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMubW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMubW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==