/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ComparisonList, ComparisonType, DefaultComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var BooleanFilterCellComponent = /** @class */ (function () {
    function BooleanFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.listData = [];
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            booleanFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.listData.push({ Id: 'true', Definition: 'True' });
        this.listData.push({ Id: 'false', Definition: 'False' });
    };
    Object.defineProperty(BooleanFilterCellComponent.prototype, "model", {
        get: /**
         * @return {?}
         */
        function () {
            return this._model;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this._model = val;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} newValue
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        var _this = this;
        if (newValue) {
            this.currentValue = newValue.Id;
        }
        else {
            this.currentValue = null;
        }
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe(function (result) {
                if (result.value) {
                    if (_this.params.serviceAccess.filter) {
                        _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel());
                    }
                    _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                    _this.prevValue = _this.model;
                }
                else {
                    _this.model = _this.prevValue || null;
                    _this.currentValue = _this.model;
                }
            });
        }
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        if (!parentModel) {
            this.currentValue = null;
        }
        else {
            this.currentValue = parentModel.filter;
        }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && data.Id) {
            this.valueChanged({ Id: this.currentValue });
        }
    };
    /**
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        return {
            filterType: 'text',
            type: this.formGroup.get('booleanFilterValue').value,
            filter: this.currentValue === null || this.currentValue === undefined ? null : this.currentValue.toString()
        };
    };
    /**
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    BooleanFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    BooleanFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-boolean-filter-cell',
                    template: "\n    <div class=\"input-group\">\n      <div style=\"width:100%;\">\n      <ng-select class=\"custom-ng-select\"\n      [items]=\"listData\"\n      [id]=\"key\"\n      bindLabel=\"Definition\"\n      [(ngModel)]=\"model\"\n      placeholder=\"{{filterKey|translate}}\"\n      (change)=\"valueChanged($event)\"\n      bindValue=\"Id\" appendTo=\"body\">\n      </ng-select>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n       <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"booleanFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n  ",
                    styles: [".custom-ng-select{height:30px!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important;padding-top:2px}:host /deep/ .ng-select-container .ng-has-value{padding-top:8px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                }] }
    ];
    /** @nocollapse */
    BooleanFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return BooleanFilterCellComponent;
}());
export { BooleanFilterCellComponent };
if (false) {
    /** @type {?} */
    BooleanFilterCellComponent.prototype.key;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    BooleanFilterCellComponent.prototype.params;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    BooleanFilterCellComponent.prototype._model;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    BooleanFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    BooleanFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    BooleanFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    BooleanFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvYm9vbGVhbi1maWx0ZXItY2VsbC9ib29sZWFuLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzlHLE9BQU8sRUFBYSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUV2RCxzQ0FFQzs7O0lBREMsaUNBQTRCOzs7OztBQUc5QiwwQ0FFQzs7O0lBREMscUNBQWM7O0FBR2hCO0lBa0RFLG9DQUFvQixFQUFlLEVBQVUsU0FBMkI7UUFBcEQsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBZGpFLGFBQVEsR0FBVSxFQUFFLENBQUM7UUFHNUIsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQU1kLGNBQVMsR0FBRyxZQUFZLENBQUM7UUFNOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUM3QixrQkFBa0IsRUFBRSxpQkFBaUIsQ0FBQyxPQUFPO1NBQzlDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsMkNBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFBbEIsaUJBZUM7UUFkQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUVyQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2FBQ3JELFNBQVMsQ0FBQyxVQUFDLEtBQVU7WUFDcEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUVqRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCxzQkFBSSw2Q0FBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3JCLENBQUM7Ozs7O1FBRUQsVUFBVSxHQUFHO1lBQ1gsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDcEIsQ0FBQzs7O09BSkE7Ozs7O0lBTUQsaURBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFBckIsaUJBd0JDO1FBdkJDLElBQUksUUFBUSxFQUFFO1lBQ1osSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsRUFBRSxDQUFDO1NBQ2pDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7Z0JBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtvQkFDaEIsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7d0JBQ3BDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3FCQUN0RjtvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQztpQkFDN0I7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQztvQkFDcEMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDO2lCQUVoQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDOzs7OztJQUVELHlEQUFvQjs7OztJQUFwQixVQUFxQixXQUFpQztRQUNwRCxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQzFCO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7U0FDeEM7SUFDSCxDQUFDOzs7OztJQUVELDRDQUFPOzs7O0lBQVAsVUFBUSxJQUFTO1FBQ2YsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1NBQzlDO0lBQ0gsQ0FBQzs7OztJQUVELCtDQUFVOzs7SUFBVjtRQUNFLE9BQU87WUFDTCxVQUFVLEVBQUUsTUFBTTtZQUNsQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxLQUFLO1lBQ3BELE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtTQUM1RyxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELDJEQUFzQjs7O0lBQXRCO1FBQUEsaUJBZUM7O1lBZE8sT0FBTyxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFVBQVUsRUFBWixDQUFZLENBQUM7O1lBRTFGLFVBQVUsR0FBRyxjQUFjLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUU1RSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7YUFDeEIsU0FBUyxDQUFDLFVBQUMsTUFBTTs7Z0JBQ1osT0FBTyxHQUFHLENBQUM7WUFDZixPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRzs7b0JBQ1gsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO2dCQUNqRCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsZ0RBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7Z0JBdEpGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0NBQXNDO29CQUNoRCxRQUFRLEVBQUUsazhCQXlCVDs7aUJBRUY7Ozs7Z0JBekNtQixXQUFXO2dCQUV0QixnQkFBZ0I7O0lBaUt6QixpQ0FBQztDQUFBLEFBdkpELElBdUpDO1NBekhZLDBCQUEwQjs7O0lBR3JDLHlDQUFXOztJQUNYLGtEQUE0Qjs7SUFDNUIsK0NBQXNCOztJQUN0Qiw4Q0FBNEI7Ozs7O0lBRTVCLDRDQUFvQjs7SUFDcEIsZ0RBQXFCOztJQUNyQiwrQ0FBcUI7O0lBRXJCLDRDQUFZOztJQUVaLGlEQUEyQjs7SUFDM0IsK0NBQWdDOzs7OztJQUVoQywyREFBNEM7Ozs7O0lBR2hDLHdDQUF1Qjs7Ozs7SUFBRSwrQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgQ29tcGFyaXNvbkxpc3QsIENvbXBhcmlzb25UeXBlLCBEZWZhdWx0Q29tcGFyaXNvbiB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2NvbXBhcmlzb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVGV4dEZpbHRlckNoYW5nZSB7XHJcbiAgbW9kZWw6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZsb2F0aW5nRmlsdGVyUGFyYW1zIGV4dGVuZHMgSUZsb2F0aW5nRmlsdGVyUGFyYW1zPFNlcmlhbGl6ZWRUZXh0RmlsdGVyLCBUZXh0RmlsdGVyQ2hhbmdlPiB7XHJcbiAgdmFsdWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWJvb2xlYW4tZmlsdGVyLWNlbGwnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj5cclxuICAgICAgPGRpdiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgIDxuZy1zZWxlY3QgY2xhc3M9XCJjdXN0b20tbmctc2VsZWN0XCJcclxuICAgICAgW2l0ZW1zXT1cImxpc3REYXRhXCJcclxuICAgICAgW2lkXT1cImtleVwiXHJcbiAgICAgIGJpbmRMYWJlbD1cIkRlZmluaXRpb25cIlxyXG4gICAgICBbKG5nTW9kZWwpXT1cIm1vZGVsXCJcclxuICAgICAgcGxhY2Vob2xkZXI9XCJ7e2ZpbHRlcktleXx0cmFuc2xhdGV9fVwiXHJcbiAgICAgIChjaGFuZ2UpPVwidmFsdWVDaGFuZ2VkKCRldmVudClcIlxyXG4gICAgICBiaW5kVmFsdWU9XCJJZFwiIGFwcGVuZFRvPVwiYm9keVwiPlxyXG4gICAgICA8L25nLXNlbGVjdD5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZ3JpZC1maWx0ZXItaWNvblwiPlxyXG4gICAgICAgPGNvcmUtaWNvbiAjYWN0dWFsVGFyZ2V0IGljb249XCJmaWx0ZXJcIiAoY2xpY2spPVwib3AudG9nZ2xlKCRldmVudClcIj48L2NvcmUtaWNvbj5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8cC1vdmVybGF5UGFuZWwgI29wIGFwcGVuZFRvPVwiYm9keVwiIGRpc21pc3NhYmxlPVwiZmFsc2VcIj5cclxuICAgICAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvciBmb3JtQ29udHJvbE5hbWU9XCJib29sZWFuRmlsdGVyVmFsdWVcIiAoY2hhbmdlZCk9XCJjaGFuZ2VkKCRldmVudClcIiBbZGF0YVNvdXJjZV09XCJkYXRhU291cmNlXCIgW2NsZWFyYWJsZV09XCJmYWxzZVwiIFtzZWFyY2hhYmxlXT1cImZhbHNlXCI+XHJcbiAgICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvcC1vdmVybGF5UGFuZWw+XHJcbiAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL2Jvb2xlYW4tZmlsdGVyLWNlbGwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQm9vbGVhbkZpbHRlckNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJRmxvYXRpbmdGaWx0ZXI8U2VyaWFsaXplZFRleHRGaWx0ZXIsXHJcblRleHRGaWx0ZXJDaGFuZ2UsIEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgQWdGcmFtZXdvcmtDb21wb25lbnQ8RmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBPbkRlc3Ryb3kge1xyXG5cclxuICBwdWJsaWMga2V5O1xyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgcHJldlZhbHVlOiBhbnk7XHJcbiAgcHVibGljIGxpc3REYXRhOiBhbnlbXSA9IFtdO1xyXG5cclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIGRhdGFTb3VyY2U6IGFueSA9IFtdO1xyXG4gIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG5cclxuICBfbW9kZWw6IGFueTtcclxuXHJcbiAgcHVibGljIHBsYWNlaG9sZGVyOiBzdHJpbmc7XHJcbiAgcHVibGljIGZpbHRlcktleSA9ICdHcmlkRmlsdGVyJztcclxuXHJcbiAgcHJpdmF0ZSB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgYm9vbGVhbkZpbHRlclZhbHVlOiBEZWZhdWx0Q29tcGFyaXNvbi5FcXVhbFRvXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcblxyXG4gICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBwYXJhbXMuY29sdW1uLmNvbERlZi5oZWFkZXJOYW1lIHx8IHBhcmFtcy5jb2x1bW4uY29sRGVmLmZpZWxkO1xyXG5cclxuICAgIHRoaXMubGlzdERhdGEucHVzaCh7IElkOiAndHJ1ZScsIERlZmluaXRpb246ICdUcnVlJyB9KTtcclxuICAgIHRoaXMubGlzdERhdGEucHVzaCh7IElkOiAnZmFsc2UnLCBEZWZpbml0aW9uOiAnRmFsc2UnIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG1vZGVsKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX21vZGVsO1xyXG4gIH1cclxuXHJcbiAgc2V0IG1vZGVsKHZhbCkge1xyXG4gICAgdGhpcy5fbW9kZWwgPSB2YWw7XHJcbiAgfVxyXG5cclxuICB2YWx1ZUNoYW5nZWQobmV3VmFsdWUpOiB2b2lkIHtcclxuICAgIGlmIChuZXdWYWx1ZSkge1xyXG4gICAgICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG5ld1ZhbHVlLklkO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQpIHtcclxuICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcih0aGlzLnBhcmFtcy5maWVsZCwgdGhpcy5idWlsZE1vZGVsKCkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnBhcmFtcy5vbkZsb2F0aW5nRmlsdGVyQ2hhbmdlZCh7IG1vZGVsOiB0aGlzLmJ1aWxkTW9kZWwoKSB9KTtcclxuICAgICAgICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLm1vZGVsO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5tb2RlbCA9IHRoaXMucHJldlZhbHVlIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gdGhpcy5tb2RlbDtcclxuXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuICAgIGlmICghcGFyZW50TW9kZWwpIHtcclxuICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBwYXJlbnRNb2RlbC5maWx0ZXI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgZGF0YS5JZCkge1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZCh7IElkOiB0aGlzLmN1cnJlbnRWYWx1ZSB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGJ1aWxkTW9kZWwoKTogU2VyaWFsaXplZFRleHRGaWx0ZXIge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZmlsdGVyVHlwZTogJ3RleHQnLFxyXG4gICAgICB0eXBlOiB0aGlzLmZvcm1Hcm91cC5nZXQoJ2Jvb2xlYW5GaWx0ZXJWYWx1ZScpLnZhbHVlLFxyXG4gICAgICBmaWx0ZXI6IHRoaXMuY3VycmVudFZhbHVlID09PSBudWxsIHx8IHRoaXMuY3VycmVudFZhbHVlID09PSB1bmRlZmluZWQgPyBudWxsIDogdGhpcy5jdXJyZW50VmFsdWUudG9TdHJpbmcoKVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTogdm9pZCB7XHJcbiAgICBjb25zdCBrZXlMaXN0ID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5OdW1iZXIpLm1hcCh4ID0+IHguRGVmaW5pdGlvbik7XHJcblxyXG4gICAgY29uc3QgZGF0YVNvdXJjZSA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuTnVtYmVyKTtcclxuXHJcbiAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoa2V5TGlzdClcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xyXG4gICAgICAgIGtleUxpc3QuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdHJhbnNsYXRlZFZhbHVlID0gcmVzdWx0W2tleV07XHJcbiAgICAgICAgICBkYXRhU291cmNlW2NvdW50ZXJdLkRlZmluaXRpb24gPSB0cmFuc2xhdGVkVmFsdWU7XHJcbiAgICAgICAgICBjb3VudGVyKys7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YVNvdXJjZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=