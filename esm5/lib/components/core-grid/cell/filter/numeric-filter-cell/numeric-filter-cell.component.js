/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Subject, from } from 'rxjs';
import { debounceTime, flatMap, distinctUntilChanged } from 'rxjs/operators';
import { ComparisonList, ComparisonType, NumberComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var NumericFilterCellComponent = /** @class */ (function () {
    function NumericFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    NumericFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.field = params.field;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        if (params.column
            && params.column.colDef
            && params.column.colDef.cellEditorParams
            && params.column.colDef.cellEditorParams.settings
            && params.column.colDef.cellEditorParams.settings.fields
            && params.column.colDef.cellEditorParams.settings.fields.length > 0) {
            this.field = _.head(params.column.colDef.cellEditorParams.settings.fields);
        }
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000), distinctUntilChanged(), // https://rxjs-dev.firebaseapp.com/api/operators/distinctUntilChanged
        flatMap(function (filterValueResult) { return from(params.serviceAccess.callUndoConfirmAlert()); }, function (filterValueResult, confirmrResult) {
            return { filterValueResult: filterValueResult, confirmrResult: confirmrResult };
        }))
            .subscribe(function (result) {
            if (result.confirmrResult.value) {
                _this.currentValue = result.filterValueResult;
                if (params.serviceAccess && params.serviceAccess.filter) {
                    params.serviceAccess.applyChangesToFilter(_this.field, _this.buildModel());
                }
                _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                _this.prevValue = _this.currentValue;
            }
            else {
                _this.canceledValue = result.filterValueResult;
                _this.currentValue = _this.prevValue || _this.prevValue === 0 ? _this.prevValue : null;
            }
        });
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    NumericFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        this.modelChanged.next(newValue);
        if (this.canceledValue === newValue) {
            this.onFilter();
        }
    };
    // fastest solution copy paste from top
    // fastest solution copy paste from top
    /**
     * @return {?}
     */
    NumericFilterCellComponent.prototype.onFilter = 
    // fastest solution copy paste from top
    /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe(function (result) {
                if (result.value) {
                    if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                        _this.params.serviceAccess.applyChangesToFilter(_this.field, _this.buildModel());
                    }
                    _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                    _this.prevValue = _this.currentValue;
                }
                else {
                    _this.canceledValue = _this.currentValue;
                    _this.currentValue = _this.prevValue || _this.prevValue === 0 ? _this.prevValue : null;
                }
            });
        }
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    NumericFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NumericFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    };
    /**
     * @return {?}
     */
    NumericFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        return {
            filterType: 'number',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue || (this.currentValue === 0) ?
                (this.params.customFilterFunction ? this.params.customFilterFunction(this.currentValue.toString()) : this.currentValue.toString()) : null
        };
    };
    /**
     * @return {?}
     */
    NumericFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    NumericFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    NumericFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-numeric-filter-cell',
                    template: "\n  <div class=\"input-group\">\n    <layout-form-number-input style=\"width: 100%;\"\n        [(ngModel)]=\"currentValue\"\n        [clearable] = \"true\"\n        placeholder=\"{{filterKey|translate}}\"\n        (ngModelChange)=\"valueChanged($event)\">\n    </layout-form-number-input>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"numberFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n  "
                }] }
    ];
    /** @nocollapse */
    NumericFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return NumericFilterCellComponent;
}());
export { NumericFilterCellComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.params;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.modelChanged;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.modelChangedSubscription;
    /** @type {?} */
    NumericFilterCellComponent.prototype.currentValue;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.prevValue;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.canceledValue;
    /** @type {?} */
    NumericFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    NumericFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    NumericFilterCellComponent.prototype.field;
    /** @type {?} */
    NumericFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    NumericFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    NumericFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtZXJpYy1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvbnVtZXJpYy1maWx0ZXItY2VsbC9udW1lcmljLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsT0FBTyxFQUFnQixJQUFJLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbkQsT0FBTyxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3RSxPQUFPLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzdHLE9BQU8sRUFBYSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUV2RCxzQ0FFQzs7O0lBREMsaUNBQTRCOzs7OztBQUc5QiwwQ0FFQzs7O0lBREMscUNBQWM7O0FBR2hCO0lBNkNFLG9DQUFvQixFQUFlLEVBQVUsU0FBMkI7UUFBcEQsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBbEJoRSxpQkFBWSxHQUFvQixJQUFJLE9BQU8sRUFBVSxDQUFDO1FBTTlELGVBQVUsR0FBUSxFQUFFLENBQUM7UUFRZCxjQUFTLEdBQUcsWUFBWSxDQUFDO1FBSzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDN0IsaUJBQWlCLEVBQUUsZ0JBQWdCLENBQUMsT0FBTztTQUM1QyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELDJDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQWxCLGlCQWtEQztRQWpEQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBRWpGLElBQUksTUFBTSxDQUFDLE1BQU07ZUFDWixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU07ZUFDcEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCO2VBQ3JDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7ZUFDOUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU07ZUFDckQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JFLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDNUU7UUFFRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2FBQ3JELFNBQVMsQ0FBQyxVQUFDLEtBQVU7WUFDcEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM3QztRQUNELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDcEQsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUNsQixvQkFBb0IsRUFBRSxFQUFFLHNFQUFzRTtRQUM5RixPQUFPLENBQUMsVUFBQSxpQkFBaUIsSUFBSSxPQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixFQUFFLENBQUMsRUFBakQsQ0FBaUQsRUFDNUUsVUFBQyxpQkFBaUIsRUFBRSxjQUFtQjtZQUNyQyxPQUFPLEVBQUUsaUJBQWlCLG1CQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFFLENBQUM7UUFDL0MsQ0FBQyxDQUFDLENBQ0w7YUFDRSxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ2YsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRTtnQkFDL0IsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUM7Z0JBRTdDLElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTtvQkFDdkQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO2lCQUMxRTtnQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2xFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQzthQUNwQztpQkFBTTtnQkFDTCxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztnQkFDOUMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsU0FBUyxJQUFJLEtBQUksQ0FBQyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7YUFDcEY7UUFFSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsaURBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakMsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLFFBQVEsRUFBRTtZQUNuQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRUQsdUNBQXVDOzs7OztJQUN2Qyw2Q0FBUTs7Ozs7SUFBUjtRQUFBLGlCQWlCQztRQWhCQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixFQUFFO1lBQy9FLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixFQUFFO2lCQUM3QyxTQUFTLENBQUMsVUFBQyxNQUFXO2dCQUNyQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQ2hCLElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO3dCQUNqRSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3FCQUMvRTtvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQztpQkFDcEM7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDO29CQUN2QyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDcEY7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQzs7Ozs7SUFFRCx5REFBb0I7Ozs7SUFBcEIsVUFBcUIsV0FBaUM7UUFFcEQscUNBQXFDO1FBR3JDLHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFHRCw0Q0FBTzs7OztJQUFQLFVBQVEsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7SUFFRCwrQ0FBVTs7O0lBQVY7UUFDRSxPQUFPO1lBQ0wsVUFBVSxFQUFFLFFBQVE7WUFDcEIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSzs7WUFDbkQsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtTQUM1SSxDQUFDO0lBQ0osQ0FBQzs7OztJQUVELDJEQUFzQjs7O0lBQXRCO1FBQUEsaUJBZUM7O1lBZE8sT0FBTyxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFVBQVUsRUFBWixDQUFZLENBQUM7O1lBRTFGLFVBQVUsR0FBRyxjQUFjLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUU1RSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7YUFDeEIsU0FBUyxDQUFDLFVBQUMsTUFBTTs7Z0JBQ1osT0FBTyxHQUFHLENBQUM7WUFDZixPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRzs7b0JBQ1gsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO2dCQUNqRCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsZ0RBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzdDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7Z0JBdkxGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0NBQXNDO29CQUNoRCxRQUFRLEVBQUUsMjBCQW9CVDtpQkFDRjs7OztnQkFuQ21CLFdBQVc7Z0JBRXRCLGdCQUFnQjs7SUFrTXpCLGlDQUFDO0NBQUEsQUF4TEQsSUF3TEM7U0FoS1ksMEJBQTBCOzs7Ozs7SUFFckMsNENBQW9COzs7OztJQUNwQixrREFBOEQ7Ozs7O0lBQzlELDhEQUErQzs7SUFFL0Msa0RBQTRCOzs7OztJQUM1QiwrQ0FBMEI7Ozs7O0lBQzFCLG1EQUE4Qjs7SUFDOUIsZ0RBQXFCOztJQUNyQiwrQ0FBcUI7O0lBSXJCLDJDQUFjOztJQUVkLGlEQUEyQjs7SUFDM0IsK0NBQWdDOzs7OztJQUVoQywyREFBNEM7Ozs7O0lBRWhDLHdDQUF1Qjs7Ozs7SUFBRSwrQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgU3ViamVjdCwgU3Vic2NyaXB0aW9uLCBmcm9tIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZmxhdE1hcCwgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IENvbXBhcmlzb25MaXN0LCBDb21wYXJpc29uVHlwZSwgTnVtYmVyQ29tcGFyaXNvbiB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2VudW1zL2NvbXBhcmlzb24tdHlwZS5lbnVtJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFRleHRGaWx0ZXJDaGFuZ2Uge1xyXG4gIG1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcjtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGbG9hdGluZ0ZpbHRlclBhcmFtcyBleHRlbmRzIElGbG9hdGluZ0ZpbHRlclBhcmFtczxTZXJpYWxpemVkVGV4dEZpbHRlciwgVGV4dEZpbHRlckNoYW5nZT4ge1xyXG4gIHZhbHVlOiBzdHJpbmc7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWNvcmUtZ3JpZC1udW1lcmljLWZpbHRlci1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgPGxheW91dC1mb3JtLW51bWJlci1pbnB1dCBzdHlsZT1cIndpZHRoOiAxMDAlO1wiXHJcbiAgICAgICAgWyhuZ01vZGVsKV09XCJjdXJyZW50VmFsdWVcIlxyXG4gICAgICAgIFtjbGVhcmFibGVdID0gXCJ0cnVlXCJcclxuICAgICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAobmdNb2RlbENoYW5nZSk9XCJ2YWx1ZUNoYW5nZWQoJGV2ZW50KVwiPlxyXG4gICAgPC9sYXlvdXQtZm9ybS1udW1iZXItaW5wdXQ+XHJcblxyXG4gICAgPGRpdiBjbGFzcz1cImdyaWQtZmlsdGVyLWljb25cIj5cclxuICAgICAgPGNvcmUtaWNvbiAjYWN0dWFsVGFyZ2V0IGljb249XCJmaWx0ZXJcIiAoY2xpY2spPVwib3AudG9nZ2xlKCRldmVudClcIj48L2NvcmUtaWNvbj5cclxuICAgIDwvZGl2PlxyXG5cclxuICAgIDxwLW92ZXJsYXlQYW5lbCAjb3AgYXBwZW5kVG89XCJib2R5XCIgZGlzbWlzc2FibGU9XCJmYWxzZVwiPlxyXG4gICAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOjEwMCU7XCI+XHJcbiAgICAgICAgPGxheW91dC1zdGF0aWMtc2VsZWN0b3IgZm9ybUNvbnRyb2xOYW1lPVwibnVtYmVyRmlsdGVyVmFsdWVcIiAoY2hhbmdlZCk9XCJjaGFuZ2VkKCRldmVudClcIiBbZGF0YVNvdXJjZV09XCJkYXRhU291cmNlXCIgW2NsZWFyYWJsZV09XCJmYWxzZVwiIFtzZWFyY2hhYmxlXT1cImZhbHNlXCI+XHJcbiAgICAgICAgPC9sYXlvdXQtc3RhdGljLXNlbGVjdG9yPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvcC1vdmVybGF5UGFuZWw+XHJcbiAgPC9kaXY+XHJcbiAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTnVtZXJpY0ZpbHRlckNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBJRmxvYXRpbmdGaWx0ZXI8U2VyaWFsaXplZFRleHRGaWx0ZXIsXHJcblRleHRGaWx0ZXJDaGFuZ2UsIEZsb2F0aW5nRmlsdGVyUGFyYW1zPiwgQWdGcmFtZXdvcmtDb21wb25lbnQ8RmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBPbkRlc3Ryb3kge1xyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgcHJpdmF0ZSBtb2RlbENoYW5nZWQ6IFN1YmplY3Q8bnVtYmVyPiA9IG5ldyBTdWJqZWN0PG51bWJlcj4oKTtcclxuICBwcml2YXRlIG1vZGVsQ2hhbmdlZFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBwdWJsaWMgY3VycmVudFZhbHVlOiBudW1iZXI7XHJcbiAgcHJpdmF0ZSBwcmV2VmFsdWU6IG51bWJlcjtcclxuICBwcml2YXRlIGNhbmNlbGVkVmFsdWU6IG51bWJlcjtcclxuICBkYXRhU291cmNlOiBhbnkgPSBbXTtcclxuICBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuICAvLyBmaWVsZFxyXG4gIC8vIHNlcnZpY2VBY2Nlc3NcclxuXHJcbiAgZmllbGQ6IHN0cmluZztcclxuXHJcbiAgcHVibGljIHBsYWNlaG9sZGVyOiBzdHJpbmc7XHJcbiAgcHVibGljIGZpbHRlcktleSA9ICdHcmlkRmlsdGVyJztcclxuXHJcbiAgcHJpdmF0ZSB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBudW1iZXJGaWx0ZXJWYWx1ZTogTnVtYmVyQ29tcGFyaXNvbi5FcXVhbFRvXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICB0aGlzLmZpZWxkID0gcGFyYW1zLmZpZWxkO1xyXG5cclxuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBwYXJhbXMuY29sdW1uLmNvbERlZi5oZWFkZXJOYW1lIHx8IHBhcmFtcy5jb2x1bW4uY29sRGVmLmZpZWxkO1xyXG5cclxuICAgIGlmIChwYXJhbXMuY29sdW1uXHJcbiAgICAgICYmIHBhcmFtcy5jb2x1bW4uY29sRGVmXHJcbiAgICAgICYmIHBhcmFtcy5jb2x1bW4uY29sRGVmLmNlbGxFZGl0b3JQYXJhbXNcclxuICAgICAgJiYgcGFyYW1zLmNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5nc1xyXG4gICAgICAmJiBwYXJhbXMuY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpZWxkc1xyXG4gICAgICAmJiBwYXJhbXMuY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zLnNldHRpbmdzLmZpZWxkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMuZmllbGQgPSBfLmhlYWQocGFyYW1zLmNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncy5maWVsZHMpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24gPSB0aGlzLnRyYW5zbGF0ZS5vbkxhbmdDaGFuZ2VcclxuICAgICAgLnN1YnNjcmliZSgoZXZlbnQ6IGFueSkgPT4ge1xyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgdGhpcy5pbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICBpZiAodGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIHRoaXMubW9kZWxDaGFuZ2VkU3Vic2NyaXB0aW9uID0gdGhpcy5tb2RlbENoYW5nZWQucGlwZShcclxuICAgICAgZGVib3VuY2VUaW1lKDEwMDApLFxyXG4gICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpLCAvLyBodHRwczovL3J4anMtZGV2LmZpcmViYXNlYXBwLmNvbS9hcGkvb3BlcmF0b3JzL2Rpc3RpbmN0VW50aWxDaGFuZ2VkXHJcbiAgICAgIGZsYXRNYXAoZmlsdGVyVmFsdWVSZXN1bHQgPT4gZnJvbShwYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpKSxcclxuICAgICAgICAoZmlsdGVyVmFsdWVSZXN1bHQsIGNvbmZpcm1yUmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIHJldHVybiB7IGZpbHRlclZhbHVlUmVzdWx0LCBjb25maXJtclJlc3VsdCB9O1xyXG4gICAgICAgIH0pXHJcbiAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0LmNvbmZpcm1yUmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHJlc3VsdC5maWx0ZXJWYWx1ZVJlc3VsdDtcclxuXHJcbiAgICAgICAgICBpZiAocGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgcGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgIHBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKHRoaXMuZmllbGQsIHRoaXMuYnVpbGRNb2RlbCgpKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLnBhcmFtcy5vbkZsb2F0aW5nRmlsdGVyQ2hhbmdlZCh7IG1vZGVsOiB0aGlzLmJ1aWxkTW9kZWwoKSB9KTtcclxuICAgICAgICAgIHRoaXMucHJldlZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuY2FuY2VsZWRWYWx1ZSA9IHJlc3VsdC5maWx0ZXJWYWx1ZVJlc3VsdDtcclxuICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gdGhpcy5wcmV2VmFsdWUgfHwgdGhpcy5wcmV2VmFsdWUgPT09IDAgPyB0aGlzLnByZXZWYWx1ZSA6IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICB2YWx1ZUNoYW5nZWQobmV3VmFsdWUpOiB2b2lkIHtcclxuICAgIHRoaXMubW9kZWxDaGFuZ2VkLm5leHQobmV3VmFsdWUpO1xyXG4gICAgaWYgKHRoaXMuY2FuY2VsZWRWYWx1ZSA9PT0gbmV3VmFsdWUpIHtcclxuICAgICAgdGhpcy5vbkZpbHRlcigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gZmFzdGVzdCBzb2x1dGlvbiBjb3B5IHBhc3RlIGZyb20gdG9wXHJcbiAgb25GaWx0ZXIoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5wYXJhbXMuc2VydmljZUFjY2VzcyAmJiB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KSB7XHJcbiAgICAgIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzdWx0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuZmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5hcHBseUNoYW5nZXNUb0ZpbHRlcih0aGlzLmZpZWxkLCB0aGlzLmJ1aWxkTW9kZWwoKSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnByZXZWYWx1ZSA9IHRoaXMuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jYW5jZWxlZFZhbHVlID0gdGhpcy5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFZhbHVlID0gdGhpcy5wcmV2VmFsdWUgfHwgdGhpcy5wcmV2VmFsdWUgPT09IDAgPyB0aGlzLnByZXZWYWx1ZSA6IG51bGw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuXHJcbiAgICAvLyAqKioqKndpbGwgYmUgZG9uZSB3aGVuIG5lZWRlZCoqKioqXHJcblxyXG5cclxuICAgIC8vIGlmICghcGFyZW50TW9kZWwpIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIC8vICAgdGhpcy5jdXJyZW50VmFsdWUgPSBwYXJlbnRNb2RlbC5maWx0ZXI7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuXHJcbiAgY2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEuSWQgJiYgdGhpcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQodGhpcy5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYnVpbGRNb2RlbCgpOiBTZXJpYWxpemVkVGV4dEZpbHRlciB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmaWx0ZXJUeXBlOiAnbnVtYmVyJyxcclxuICAgICAgdHlwZTogdGhpcy5mb3JtR3JvdXAuZ2V0KCdudW1iZXJGaWx0ZXJWYWx1ZScpLnZhbHVlLCAvLyBjYW4gYmUgc2VuZCBpbiBoZXJlXHJcbiAgICAgIGZpbHRlcjogdGhpcy5jdXJyZW50VmFsdWUgfHwgKHRoaXMuY3VycmVudFZhbHVlID09PSAwKSA/XHJcbiAgICAgICAgKHRoaXMucGFyYW1zLmN1c3RvbUZpbHRlckZ1bmN0aW9uID8gdGhpcy5wYXJhbXMuY3VzdG9tRmlsdGVyRnVuY3Rpb24odGhpcy5jdXJyZW50VmFsdWUudG9TdHJpbmcoKSkgOiB0aGlzLmN1cnJlbnRWYWx1ZS50b1N0cmluZygpKSA6IG51bGxcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBpbml0aWF0ZUNvbXBhcmlzb25MaXN0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qga2V5TGlzdCA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuTnVtYmVyKS5tYXAoeCA9PiB4LkRlZmluaXRpb24pO1xyXG5cclxuICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLk51bWJlcik7XHJcblxyXG4gICAgdGhpcy50cmFuc2xhdGUuZ2V0KGtleUxpc3QpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBrZXlMaXN0LmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRWYWx1ZSA9IHJlc3VsdFtrZXldO1xyXG4gICAgICAgICAgZGF0YVNvdXJjZVtjb3VudGVyXS5EZWZpbml0aW9uID0gdHJhbnNsYXRlZFZhbHVlO1xyXG4gICAgICAgICAgY291bnRlcisrO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5tb2RlbENoYW5nZWRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19