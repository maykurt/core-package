/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { DateSettings } from '../../../../../models/date-settings.model';
import { ComparisonList, ComparisonType, DateComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateSelectionMode } from '../../../../../enums/date-selection-mode.enum';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var DateFilterCellComponent = /** @class */ (function () {
    function DateFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.selectionMode = DateSelectionMode.Single;
        this.settings = new DateSettings();
        this.formGroup = this.fb.group({
            dateFilterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectorDateFilterValue: DateComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    DateFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        if (this.params.column) {
            /** @type {?} */
            var column = this.params.column;
            if (column.colDef && column.colDef.cellEditorParams && column.colDef.cellEditorParams.settings) {
                this.settings = Object.assign((/** @type {?} */ ({})), column.colDef.cellEditorParams.settings);
            }
        }
        this.settings.hideCalendarButton = true;
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    DateFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        var _this = this;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe(function (result) {
                if (result.value) {
                    /** @type {?} */
                    var selectedDateComparison = _this.selectorFormGroup.get('selectorDateFilterValue').value;
                    if (selectedDateComparison === DateComparison.Between) {
                        _this.selectionMode = DateSelectionMode.Range;
                    }
                    else {
                        _this.selectionMode = DateSelectionMode.Single;
                    }
                    if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                        _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel(newValue));
                    }
                    _this.params.onFloatingFilterChanged({ model: _this.buildModel(newValue) });
                }
                else {
                    _this.formGroup.patchValue({
                        dateFilterValue: null
                    });
                }
            });
        }
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    DateFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    DateFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && data.Id) {
            this.valueChanged(this.formGroup.get('dateFilterValue').value);
        }
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    DateFilterCellComponent.prototype.buildModel = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        /** @type {?} */
        var model = {
            filterType: 'string',
            type: this.selectorFormGroup.get('selectorDateFilterValue').value,
            filter: null,
            other: null
        };
        if (newValue instanceof Array) {
            model.filter = newValue[0];
            model.other = newValue[1];
        }
        else {
            model.filter = newValue;
        }
        return model;
    };
    /**
     * @return {?}
     */
    DateFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Date).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Date);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    DateFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    DateFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-date-filter-cell',
                    template: "\n\n  <div class=\"input-group\">\n    <div *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\"\n      class=\"input-group\" style=\"width:100%;\">\n      <layout-form-datepicker-input style=\"width:100%;\"\n      formControlName=\"dateFilterValue\"\n      [settings]= \"settings\"\n      [selectionMode]=\"selectionMode\"\n      placeholder=\"{{filterKey|translate}}\"\n      (dateChanges)= \"valueChanged($event)\"></layout-form-datepicker-input>\n    </div>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"selectorDateFilterValue\"\n        (changed)=\"changed($event)\"\n        [dataSource]=\"dataSource\"\n        placeholder=\"{{placeholder |translate}} {{filterKey|translate}}\"\n        [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n\n  ",
                    styles: [":host /deep/ .custom-ng-select{min-width:auto!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ui-calendar .ui-inputtext{margin-top:6px}"]
                }] }
    ];
    /** @nocollapse */
    DateFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    return DateFilterCellComponent;
}());
export { DateFilterCellComponent };
if (false) {
    /** @type {?} */
    DateFilterCellComponent.prototype.settings;
    /** @type {?} */
    DateFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    DateFilterCellComponent.prototype.selectorFormGroup;
    /** @type {?} */
    DateFilterCellComponent.prototype.params;
    /** @type {?} */
    DateFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    DateFilterCellComponent.prototype.placeholder;
    /** @type {?} */
    DateFilterCellComponent.prototype.filterKey;
    /**
     * @type {?}
     * @private
     */
    DateFilterCellComponent.prototype.translateSubscription;
    /** @type {?} */
    DateFilterCellComponent.prototype.selectionMode;
    /**
     * @type {?}
     * @private
     */
    DateFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    DateFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1maWx0ZXItY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvZGF0ZS1maWx0ZXItY2VsbC9kYXRlLWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUdyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDekUsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsY0FBYyxFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFFM0csT0FBTyxFQUFhLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXhELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDOzs7O0FBRWxGLHNDQUVDOzs7SUFEQyxpQ0FBNEI7Ozs7O0FBRzlCLDBDQUVDOzs7SUFEQyxxQ0FBYzs7QUFHaEI7SUFvREUsaUNBQW9CLEVBQWUsRUFBVSxTQUEyQjtRQUFwRCxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFUeEUsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQUdkLGNBQVMsR0FBRyxZQUFZLENBQUM7UUFJekIsa0JBQWEsR0FBc0IsaUJBQWlCLENBQUMsTUFBTSxDQUFDO1FBR2pFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLGVBQWUsRUFBRSxJQUFJO1NBQ3RCLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNyQyx1QkFBdUIsRUFBRSxjQUFjLENBQUMsT0FBTztTQUNoRCxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELHdDQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQWxCLGlCQW9CQztRQW5CQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUVyQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFakYsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUNyRCxTQUFTLENBQUMsVUFBQyxLQUFVO1lBQ3BCLElBQUksS0FBSyxFQUFFO2dCQUNULEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFOztnQkFDaEIsTUFBTSxHQUFRLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTtZQUN0QyxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLG1CQUFjLEVBQUUsRUFBQSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDMUY7U0FDRjtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsOENBQVk7Ozs7SUFBWixVQUFhLFFBQVE7UUFBckIsaUJBMEJDO1FBekJDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7Z0JBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTs7d0JBQ1Ysc0JBQXNCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEtBQUs7b0JBRTFGLElBQUksc0JBQXNCLEtBQUssY0FBYyxDQUFDLE9BQU8sRUFBRTt3QkFDckQsS0FBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxLQUFLLENBQUM7cUJBQzlDO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxhQUFhLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDO3FCQUMvQztvQkFFRCxJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTt3QkFDakUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3FCQUM5RjtvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUUzRTtxQkFBTTtvQkFDTCxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3QkFDeEIsZUFBZSxFQUFFLElBQUk7cUJBQ3RCLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDOzs7OztJQUVELHNEQUFvQjs7OztJQUFwQixVQUFxQixXQUFpQztRQUdwRCxxQ0FBcUM7UUFHckMsc0JBQXNCO1FBQ3RCLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsNENBQTRDO1FBQzVDLElBQUk7SUFDTixDQUFDOzs7OztJQUVELHlDQUFPOzs7O0lBQVAsVUFBUSxJQUFTO1FBQ2YsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEU7SUFDSCxDQUFDOzs7OztJQUVELDRDQUFVOzs7O0lBQVYsVUFBVyxRQUEyQjs7WUFDOUIsS0FBSyxHQUFRO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLElBQUksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUMsS0FBSztZQUNqRSxNQUFNLEVBQUUsSUFBSTtZQUNaLEtBQUssRUFBRSxJQUFJO1NBQ1o7UUFFRCxJQUFJLFFBQVEsWUFBWSxLQUFLLEVBQUU7WUFDN0IsS0FBSyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsS0FBSyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDM0I7YUFBTTtZQUNMLEtBQUssQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO1NBQ3pCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsd0RBQXNCOzs7SUFBdEI7UUFBQSxpQkFlQzs7WUFkTyxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsVUFBVSxFQUFaLENBQVksQ0FBQzs7WUFFeEYsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1FBRTFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNOztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNmLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztvQkFDWCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCw2Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOztnQkExS0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxtQ0FBbUM7b0JBQzdDLFFBQVEsRUFBRSx1bkNBOEJUOztpQkFFRjs7OztnQkEvQ21CLFdBQVc7Z0JBRXRCLGdCQUFnQjs7SUFzTHpCLDhCQUFDO0NBQUEsQUEzS0QsSUEyS0M7U0F2SVksdUJBQXVCOzs7SUFFbEMsMkNBQXVCOztJQUN2Qiw0Q0FBcUI7O0lBQ3JCLG9EQUE2Qjs7SUFFN0IseUNBQVk7O0lBQ1osNkNBQXFCOztJQUVyQiw4Q0FBMkI7O0lBQzNCLDRDQUFnQzs7Ozs7SUFFaEMsd0RBQTRDOztJQUU1QyxnREFBbUU7Ozs7O0lBRXZELHFDQUF1Qjs7Ozs7SUFBRSw0Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBZ0ZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IElGbG9hdGluZ0ZpbHRlciwgSUZsb2F0aW5nRmlsdGVyUGFyYW1zLCBTZXJpYWxpemVkVGV4dEZpbHRlciB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcclxuaW1wb3J0IHsgRGF0ZVNldHRpbmdzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbW9kZWxzL2RhdGUtc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBDb21wYXJpc29uTGlzdCwgQ29tcGFyaXNvblR5cGUsIERhdGVDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5cclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHsgRGF0ZVNlbGVjdGlvbk1vZGUgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9lbnVtcy9kYXRlLXNlbGVjdGlvbi1tb2RlLmVudW0nO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBUZXh0RmlsdGVyQ2hhbmdlIHtcclxuICBtb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmxvYXRpbmdGaWx0ZXJQYXJhbXMgZXh0ZW5kcyBJRmxvYXRpbmdGaWx0ZXJQYXJhbXM8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2U+IHtcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtZGF0ZS1maWx0ZXItY2VsbCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuXHJcbiAgPGRpdiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCJcclxuICAgICAgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIlxyXG4gICAgICBjbGFzcz1cImlucHV0LWdyb3VwXCIgc3R5bGU9XCJ3aWR0aDoxMDAlO1wiPlxyXG4gICAgICA8bGF5b3V0LWZvcm0tZGF0ZXBpY2tlci1pbnB1dCBzdHlsZT1cIndpZHRoOjEwMCU7XCJcclxuICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZGF0ZUZpbHRlclZhbHVlXCJcclxuICAgICAgW3NldHRpbmdzXT0gXCJzZXR0aW5nc1wiXHJcbiAgICAgIFtzZWxlY3Rpb25Nb2RlXT1cInNlbGVjdGlvbk1vZGVcIlxyXG4gICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgKGRhdGVDaGFuZ2VzKT0gXCJ2YWx1ZUNoYW5nZWQoJGV2ZW50KVwiPjwvbGF5b3V0LWZvcm0tZGF0ZXBpY2tlci1pbnB1dD5cclxuICAgIDwvZGl2PlxyXG5cclxuICAgIDxkaXYgY2xhc3M9XCJncmlkLWZpbHRlci1pY29uXCI+XHJcbiAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICA8L2Rpdj5cclxuXHJcbiAgICA8cC1vdmVybGF5UGFuZWwgI29wIGFwcGVuZFRvPVwiYm9keVwiIGRpc21pc3NhYmxlPVwiZmFsc2VcIj5cclxuICAgICAgPGRpdiAqbmdJZj1cInNlbGVjdG9yRm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJzZWxlY3RvckZvcm1Hcm91cFwiIHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgICAgICA8bGF5b3V0LXN0YXRpYy1zZWxlY3RvciBmb3JtQ29udHJvbE5hbWU9XCJzZWxlY3RvckRhdGVGaWx0ZXJWYWx1ZVwiXHJcbiAgICAgICAgKGNoYW5nZWQpPVwiY2hhbmdlZCgkZXZlbnQpXCJcclxuICAgICAgICBbZGF0YVNvdXJjZV09XCJkYXRhU291cmNlXCJcclxuICAgICAgICBwbGFjZWhvbGRlcj1cInt7cGxhY2Vob2xkZXIgfHRyYW5zbGF0ZX19IHt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICBbY2xlYXJhYmxlXT1cImZhbHNlXCIgW3NlYXJjaGFibGVdPVwiZmFsc2VcIj5cclxuICAgICAgICA8L2xheW91dC1zdGF0aWMtc2VsZWN0b3I+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9wLW92ZXJsYXlQYW5lbD5cclxuICA8L2Rpdj5cclxuXHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnLi9kYXRlLWZpbHRlci1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlRmlsdGVyQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIElGbG9hdGluZ0ZpbHRlcjxTZXJpYWxpemVkVGV4dEZpbHRlcixcclxuVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcbiAgc2V0dGluZ3M6IERhdGVTZXR0aW5ncztcclxuICBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuICBzZWxlY3RvckZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG5cclxuICBwYXJhbXM6IGFueTtcclxuICBkYXRhU291cmNlOiBhbnkgPSBbXTtcclxuXHJcbiAgcHVibGljIHBsYWNlaG9sZGVyOiBzdHJpbmc7XHJcbiAgcHVibGljIGZpbHRlcktleSA9ICdHcmlkRmlsdGVyJztcclxuXHJcbiAgcHJpdmF0ZSB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgcHVibGljIHNlbGVjdGlvbk1vZGU6IERhdGVTZWxlY3Rpb25Nb2RlID0gRGF0ZVNlbGVjdGlvbk1vZGUuU2luZ2xlO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcclxuICAgIHRoaXMuc2V0dGluZ3MgPSBuZXcgRGF0ZVNldHRpbmdzKCk7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBkYXRlRmlsdGVyVmFsdWU6IG51bGxcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc2VsZWN0b3JGb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgc2VsZWN0b3JEYXRlRmlsdGVyVmFsdWU6IERhdGVDb21wYXJpc29uLkVxdWFsVG9cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuXHJcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gcGFyYW1zLmNvbHVtbi5jb2xEZWYuaGVhZGVyTmFtZSB8fCBwYXJhbXMuY29sdW1uLmNvbERlZi5maWVsZDtcclxuXHJcbiAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbHVtbikge1xyXG4gICAgICBjb25zdCBjb2x1bW46IGFueSA9IHRoaXMucGFyYW1zLmNvbHVtbjtcclxuICAgICAgaWYgKGNvbHVtbi5jb2xEZWYgJiYgY29sdW1uLmNvbERlZi5jZWxsRWRpdG9yUGFyYW1zICYmIGNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSBPYmplY3QuYXNzaWduKDxEYXRlU2V0dGluZ3M+e30sIGNvbHVtbi5jb2xEZWYuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuc2V0dGluZ3MuaGlkZUNhbGVuZGFyQnV0dG9uID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHZhbHVlQ2hhbmdlZChuZXdWYWx1ZSk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCkge1xyXG4gICAgICB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmNhbGxVbmRvQ29uZmlybUFsZXJ0KClcclxuICAgICAgICAuc3Vic2NyaWJlKChyZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3VsdC52YWx1ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZERhdGVDb21wYXJpc29uID0gdGhpcy5zZWxlY3RvckZvcm1Hcm91cC5nZXQoJ3NlbGVjdG9yRGF0ZUZpbHRlclZhbHVlJykudmFsdWU7XHJcblxyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWREYXRlQ29tcGFyaXNvbiA9PT0gRGF0ZUNvbXBhcmlzb24uQmV0d2Vlbikge1xyXG4gICAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uTW9kZSA9IERhdGVTZWxlY3Rpb25Nb2RlLlJhbmdlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uTW9kZSA9IERhdGVTZWxlY3Rpb25Nb2RlLlNpbmdsZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5maWx0ZXIpIHtcclxuICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKHRoaXMucGFyYW1zLmZpZWxkLCB0aGlzLmJ1aWxkTW9kZWwobmV3VmFsdWUpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wYXJhbXMub25GbG9hdGluZ0ZpbHRlckNoYW5nZWQoeyBtb2RlbDogdGhpcy5idWlsZE1vZGVsKG5ld1ZhbHVlKSB9KTtcclxuXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1Hcm91cC5wYXRjaFZhbHVlKHtcclxuICAgICAgICAgICAgICBkYXRlRmlsdGVyVmFsdWU6IG51bGxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblBhcmVudE1vZGVsQ2hhbmdlZChwYXJlbnRNb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXIpOiB2b2lkIHtcclxuXHJcblxyXG4gICAgLy8gKioqKip3aWxsIGJlIGRvbmUgd2hlbiBuZWVkZWQqKioqKlxyXG5cclxuXHJcbiAgICAvLyBpZiAoIXBhcmVudE1vZGVsKSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gbnVsbDtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgIHRoaXMuY3VycmVudFZhbHVlID0gcGFyZW50TW9kZWwuZmlsdGVyO1xyXG4gICAgLy8gfVxyXG4gIH1cclxuXHJcbiAgY2hhbmdlZChkYXRhOiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEuSWQpIHtcclxuICAgICAgdGhpcy52YWx1ZUNoYW5nZWQodGhpcy5mb3JtR3JvdXAuZ2V0KCdkYXRlRmlsdGVyVmFsdWUnKS52YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKG5ld1ZhbHVlOiBzdHJpbmcgfCBzdHJpbmdbXSk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIGNvbnN0IG1vZGVsOiBhbnkgPSB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICB0eXBlOiB0aGlzLnNlbGVjdG9yRm9ybUdyb3VwLmdldCgnc2VsZWN0b3JEYXRlRmlsdGVyVmFsdWUnKS52YWx1ZSxcclxuICAgICAgZmlsdGVyOiBudWxsLFxyXG4gICAgICBvdGhlcjogbnVsbFxyXG4gICAgfTtcclxuXHJcbiAgICBpZiAobmV3VmFsdWUgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICBtb2RlbC5maWx0ZXIgPSBuZXdWYWx1ZVswXTtcclxuICAgICAgbW9kZWwub3RoZXIgPSBuZXdWYWx1ZVsxXTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG1vZGVsLmZpbHRlciA9IG5ld1ZhbHVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG1vZGVsO1xyXG4gIH1cclxuXHJcbiAgaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGtleUxpc3QgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLkRhdGUpLm1hcCh4ID0+IHguRGVmaW5pdGlvbik7XHJcblxyXG4gICAgY29uc3QgZGF0YVNvdXJjZSA9IENvbXBhcmlzb25MaXN0LmNyZWF0ZUtleUxhYmVsQXJyYXkoQ29tcGFyaXNvblR5cGUuRGF0ZSk7XHJcblxyXG4gICAgdGhpcy50cmFuc2xhdGUuZ2V0KGtleUxpc3QpXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBrZXlMaXN0LmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRWYWx1ZSA9IHJlc3VsdFtrZXldO1xyXG4gICAgICAgICAgZGF0YVNvdXJjZVtjb3VudGVyXS5EZWZpbml0aW9uID0gdHJhbnNsYXRlZFZhbHVlO1xyXG4gICAgICAgICAgY291bnRlcisrO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy50cmFuc2xhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19