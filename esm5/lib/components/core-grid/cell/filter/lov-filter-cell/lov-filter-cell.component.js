/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { ComparisonList, ComparisonType, DefaultComparison } from '../../../../../enums/comparison-type.enum';
import { FormBuilder } from '@angular/forms';
import { ListOfValuesComponent } from '../../../../list-of-values/list-of-values.component';
import { TranslateService } from '@ngx-translate/core';
/**
 * @record
 */
export function TextFilterChange() { }
if (false) {
    /** @type {?} */
    TextFilterChange.prototype.model;
}
/**
 * @record
 */
export function FloatingFilterParams() { }
if (false) {
    /** @type {?} */
    FloatingFilterParams.prototype.value;
}
var LovFilterCellComponent = /** @class */ (function () {
    function LovFilterCellComponent(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    LovFilterCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        this.params = params;
        this.hideFilter = this.params.hideFilter;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
            }
            else {
                console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
        }
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.settings) {
                this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField);
            }
        }
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    LovFilterCellComponent.prototype.valueChanged = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        var _this = this;
        this.currentValue = newValue && newValue.id ? newValue.id : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe(function (result) {
                if (result.value) {
                    if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                        _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel());
                    }
                    _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                    _this.prevValue = _this.currentValue;
                }
                else {
                    _this.currentValue = null;
                    _this.formGroup.patchValue({
                        filterValue: _this.prevValue || (_this.prevValue && parseInt(_this.prevValue, 10) === 0) ? _this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    };
    /**
     * @param {?} parentModel
     * @return {?}
     */
    LovFilterCellComponent.prototype.onParentModelChanged = /**
     * @param {?} parentModel
     * @return {?}
     */
    function (parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    LovFilterCellComponent.prototype.changed = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    };
    /**
     * @return {?}
     */
    LovFilterCellComponent.prototype.buildModel = /**
     * @return {?}
     */
    function () {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    };
    /**
     * @return {?}
     */
    LovFilterCellComponent.prototype.initiateComparisonList = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(function (x) { return x.Definition; });
        /** @type {?} */
        var dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe(function (result) {
            /** @type {?} */
            var counter = 0;
            keyList.forEach(function (key) {
                /** @type {?} */
                var translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            _this.dataSource = dataSource;
        });
    };
    /**
     * @return {?}
     */
    LovFilterCellComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    };
    LovFilterCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-lov-filter-cell',
                    template: "\n    <div [hidden]=\"hideFilter\" class=\"input-group\">\n      <div [formGroup]=\"formGroup\" style=\"width: calc(100%);\">\n        <layout-list-of-values #listOfValue\n        [id]=\"key\"\n        formControlName=\"filterValue\"\n        placeholder=\"{{filterKey|translate}}\"\n        (lovValueChanged)=\"valueChanged($event)\"></layout-list-of-values>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n          <layout-static-selector\n          formControlName=\"selectFilterValue\"\n          (changed)=\"changed($event)\"\n          [dataSource]=\"dataSource\"\n          placeholder=\"{{filterKey|translate}}\"\n          [clearable]=\"false\"\n          [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n\n  ",
                    styles: [":host /deep/ .form-lov .btn-info{background-color:#175169!important;border-color:#175169!important}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                }] }
    ];
    /** @nocollapse */
    LovFilterCellComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    LovFilterCellComponent.propDecorators = {
        listOfValue: [{ type: ViewChild, args: ['listOfValue',] }]
    };
    return LovFilterCellComponent;
}());
export { LovFilterCellComponent };
if (false) {
    /** @type {?} */
    LovFilterCellComponent.prototype.listOfValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.key;
    /** @type {?} */
    LovFilterCellComponent.prototype.currentValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.prevValue;
    /** @type {?} */
    LovFilterCellComponent.prototype.listData;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.settings;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.params;
    /** @type {?} */
    LovFilterCellComponent.prototype.dataSource;
    /** @type {?} */
    LovFilterCellComponent.prototype.selectorFormGroup;
    /** @type {?} */
    LovFilterCellComponent.prototype.formGroup;
    /** @type {?} */
    LovFilterCellComponent.prototype.filterKey;
    /** @type {?} */
    LovFilterCellComponent.prototype.hideFilter;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.translateSubscription;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    LovFilterCellComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG92LWZpbHRlci1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2ZpbHRlci9sb3YtZmlsdGVyLWNlbGwvbG92LWZpbHRlci1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFHaEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RyxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFHNUYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdkQsc0NBRUM7OztJQURDLGlDQUE0Qjs7Ozs7QUFHOUIsMENBRUM7OztJQURDLHFDQUFjOztBQUdoQjtJQXVERSxnQ0FBb0IsRUFBZSxFQUFVLFNBQTJCO1FBQXBELE9BQUUsR0FBRixFQUFFLENBQWE7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQVZ4RSxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBSWQsY0FBUyxHQUFHLFlBQVksQ0FBQztRQU85QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQzdCLFdBQVcsRUFBRSxJQUFJO1NBQ2xCLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNyQyxpQkFBaUIsRUFBRSxpQkFBaUIsQ0FBQyxPQUFPO1NBQzdDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsdUNBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFBbEIsaUJBMkJDO1FBMUJDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFFekMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUNyRCxTQUFTLENBQUMsVUFBQyxLQUFVO1lBQ3BCLElBQUksS0FBSyxFQUFFO2dCQUNULEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixFQUFFO1lBQ25ELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUM1RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzthQUMxRTtpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssR0FBRyw0QkFBNEIsQ0FBQyxDQUFDO2FBQ3BGO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsS0FBSyxHQUFHLDRCQUE0QixDQUFDLENBQUM7U0FDcEY7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixFQUFFO1lBQ25ELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN4RztTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCw2Q0FBWTs7OztJQUFaLFVBQWEsUUFBUTtRQUFyQixpQkF1QkM7UUF0QkMsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLElBQUksUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBRWpFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDL0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7aUJBQzdDLFNBQVMsQ0FBQyxVQUFDLE1BQVc7Z0JBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtvQkFDaEIsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7d0JBQ2pFLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO3FCQUN0RjtvQkFFRCxLQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQztpQkFDcEM7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ3pCLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDO3dCQUN4QixXQUFXLEVBQUUsS0FBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUk7cUJBQzlHLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7O0lBRUQscURBQW9COzs7O0lBQXBCLFVBQXFCLFdBQWlDO1FBQ3BELHNCQUFzQjtRQUN0Qiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLDRDQUE0QztRQUM1QyxJQUFJO0lBQ04sQ0FBQzs7Ozs7SUFFRCx3Q0FBTzs7OztJQUFQLFVBQVEsSUFBUztRQUNmLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7O0lBRUQsMkNBQVU7OztJQUFWO1FBQ0UsT0FBTztZQUNMLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLElBQUksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSzs7WUFDM0QsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZO1NBQzFCLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsdURBQXNCOzs7SUFBdEI7UUFBQSxpQkFlQzs7WUFkTyxPQUFPLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsVUFBVSxFQUFaLENBQVksQ0FBQzs7WUFFM0YsVUFBVSxHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDO1FBRTdFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNOztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNmLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztvQkFDWCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDbkMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFHRCw0Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOztnQkFuS0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxrQ0FBa0M7b0JBQzVDLFFBQVEsRUFBRSxnakNBNEJUOztpQkFFRjs7OztnQkE5Q21CLFdBQVc7Z0JBSXRCLGdCQUFnQjs7OzhCQThDdEIsU0FBUyxTQUFDLGFBQWE7O0lBaUkxQiw2QkFBQztDQUFBLEFBcktELElBcUtDO1NBcElZLHNCQUFzQjs7O0lBR2pDLDZDQUE2RDs7SUFFN0QscUNBQVc7O0lBQ1gsOENBQTRCOztJQUM1QiwyQ0FBeUI7O0lBQ3pCLDBDQUFxQjs7Ozs7SUFDckIsMENBQThCOzs7OztJQUU5Qix3Q0FBb0I7O0lBQ3BCLDRDQUFxQjs7SUFDckIsbURBQTZCOztJQUM3QiwyQ0FBcUI7O0lBRXJCLDJDQUFnQzs7SUFFaEMsNENBQW9COzs7OztJQUVwQix1REFBNEM7Ozs7O0lBRWhDLG9DQUF1Qjs7Ozs7SUFBRSwyQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFnRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcclxuaW1wb3J0IHsgSUZsb2F0aW5nRmlsdGVyLCBJRmxvYXRpbmdGaWx0ZXJQYXJhbXMsIFNlcmlhbGl6ZWRUZXh0RmlsdGVyIH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHknO1xyXG5pbXBvcnQgeyBDb21wYXJpc29uTGlzdCwgQ29tcGFyaXNvblR5cGUsIERlZmF1bHRDb21wYXJpc29uIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBMaXN0T2ZWYWx1ZXNDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi8uLi9saXN0LW9mLXZhbHVlcy9saXN0LW9mLXZhbHVlcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb3ZTZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL21vZGVscy9sb3Ytc2V0dGluZ3MubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBUZXh0RmlsdGVyQ2hhbmdlIHtcclxuICBtb2RlbDogU2VyaWFsaXplZFRleHRGaWx0ZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmxvYXRpbmdGaWx0ZXJQYXJhbXMgZXh0ZW5kcyBJRmxvYXRpbmdGaWx0ZXJQYXJhbXM8U2VyaWFsaXplZFRleHRGaWx0ZXIsIFRleHRGaWx0ZXJDaGFuZ2U+IHtcclxuICB2YWx1ZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2xheW91dC1jb3JlLWdyaWQtbG92LWZpbHRlci1jZWxsJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGRpdiBbaGlkZGVuXT1cImhpZGVGaWx0ZXJcIiBjbGFzcz1cImlucHV0LWdyb3VwXCI+XHJcbiAgICAgIDxkaXYgW2Zvcm1Hcm91cF09XCJmb3JtR3JvdXBcIiBzdHlsZT1cIndpZHRoOiBjYWxjKDEwMCUpO1wiPlxyXG4gICAgICAgIDxsYXlvdXQtbGlzdC1vZi12YWx1ZXMgI2xpc3RPZlZhbHVlXHJcbiAgICAgICAgW2lkXT1cImtleVwiXHJcbiAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZmlsdGVyVmFsdWVcIlxyXG4gICAgICAgIHBsYWNlaG9sZGVyPVwie3tmaWx0ZXJLZXl8dHJhbnNsYXRlfX1cIlxyXG4gICAgICAgIChsb3ZWYWx1ZUNoYW5nZWQpPVwidmFsdWVDaGFuZ2VkKCRldmVudClcIj48L2xheW91dC1saXN0LW9mLXZhbHVlcz5cclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZ3JpZC1maWx0ZXItaWNvblwiPlxyXG4gICAgICAgIDxjb3JlLWljb24gI2FjdHVhbFRhcmdldCBpY29uPVwiZmlsdGVyXCIgKGNsaWNrKT1cIm9wLnRvZ2dsZSgkZXZlbnQpXCI+PC9jb3JlLWljb24+XHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPHAtb3ZlcmxheVBhbmVsICNvcCBhcHBlbmRUbz1cImJvZHlcIiBkaXNtaXNzYWJsZT1cImZhbHNlXCI+XHJcbiAgICAgICAgPGRpdiAqbmdJZj1cInNlbGVjdG9yRm9ybUdyb3VwXCIgW2Zvcm1Hcm91cF09XCJzZWxlY3RvckZvcm1Hcm91cFwiIHN0eWxlPVwid2lkdGg6MTAwJTtcIj5cclxuICAgICAgICAgIDxsYXlvdXQtc3RhdGljLXNlbGVjdG9yXHJcbiAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJzZWxlY3RGaWx0ZXJWYWx1ZVwiXHJcbiAgICAgICAgICAoY2hhbmdlZCk9XCJjaGFuZ2VkKCRldmVudClcIlxyXG4gICAgICAgICAgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcj1cInt7ZmlsdGVyS2V5fHRyYW5zbGF0ZX19XCJcclxuICAgICAgICAgIFtjbGVhcmFibGVdPVwiZmFsc2VcIlxyXG4gICAgICAgICAgW3NlYXJjaGFibGVdPVwiZmFsc2VcIj5cclxuICAgICAgICAgIDwvbGF5b3V0LXN0YXRpYy1zZWxlY3Rvcj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9wLW92ZXJsYXlQYW5lbD5cclxuICAgIDwvZGl2PlxyXG5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL2xvdi1maWx0ZXItY2VsbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb3ZGaWx0ZXJDZWxsQ29tcG9uZW50XHJcbiAgaW1wbGVtZW50cyBJRmxvYXRpbmdGaWx0ZXI8U2VyaWFsaXplZFRleHRGaWx0ZXIsXHJcbiAgVGV4dEZpbHRlckNoYW5nZSwgRmxvYXRpbmdGaWx0ZXJQYXJhbXM+LCBBZ0ZyYW1ld29ya0NvbXBvbmVudDxGbG9hdGluZ0ZpbHRlclBhcmFtcz4sIE9uRGVzdHJveSB7XHJcbiAgQFZpZXdDaGlsZCgnbGlzdE9mVmFsdWUnKSBsaXN0T2ZWYWx1ZTogTGlzdE9mVmFsdWVzQ29tcG9uZW50O1xyXG5cclxuICBwdWJsaWMga2V5O1xyXG4gIHB1YmxpYyBjdXJyZW50VmFsdWU6IHN0cmluZztcclxuICBwdWJsaWMgcHJldlZhbHVlOiBzdHJpbmc7XHJcbiAgcHVibGljIGxpc3REYXRhOiBhbnk7XHJcbiAgcHJpdmF0ZSBzZXR0aW5nczogTG92U2V0dGluZ3M7XHJcblxyXG4gIHByaXZhdGUgcGFyYW1zOiBhbnk7XHJcbiAgZGF0YVNvdXJjZTogYW55ID0gW107XHJcbiAgc2VsZWN0b3JGb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuICBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuXHJcbiAgcHVibGljIGZpbHRlcktleSA9ICdHcmlkRmlsdGVyJztcclxuXHJcbiAgaGlkZUZpbHRlcjogYm9vbGVhbjtcclxuXHJcbiAgcHJpdmF0ZSB0cmFuc2xhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsIHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBmaWx0ZXJWYWx1ZTogbnVsbFxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5zZWxlY3RvckZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICBzZWxlY3RGaWx0ZXJWYWx1ZTogRGVmYXVsdENvbXBhcmlzb24uRXF1YWxUb1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xyXG4gICAgdGhpcy5oaWRlRmlsdGVyID0gdGhpcy5wYXJhbXMuaGlkZUZpbHRlcjtcclxuXHJcbiAgICB0aGlzLmluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTtcclxuICAgIHRoaXMudHJhbnNsYXRlU3Vic2NyaXB0aW9uID0gdGhpcy50cmFuc2xhdGUub25MYW5nQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGlhdGVDb21wYXJpc29uTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgaWYgKHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5jZWxsRWRpdG9yUGFyYW1zKSB7XHJcbiAgICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSB0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcy5zZXR0aW5ncztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5maWVsZCArICdsb3YgU2V0dGluZyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zb2xlLmVycm9yKHRoaXMucGFyYW1zLmNvbHVtbi5nZXRDb2xEZWYoKS5maWVsZCArICdsb3YgU2V0dGluZyBpcyBub3QgZGVmaW5lZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5jb2x1bW4uZ2V0Q29sRGVmKCkuY2VsbEVkaXRvclBhcmFtcykge1xyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncykge1xyXG4gICAgICAgIHRoaXMubGlzdE9mVmFsdWUuc2V0T3B0aW9ucyh0aGlzLnNldHRpbmdzLm9wdGlvbnMsIHRoaXMuc2V0dGluZ3MudmFsdWVGaWVsZCwgdGhpcy5zZXR0aW5ncy5sYWJlbEZpZWxkKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdmFsdWVDaGFuZ2VkKG5ld1ZhbHVlKTogdm9pZCB7XHJcbiAgICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG5ld1ZhbHVlICYmIG5ld1ZhbHVlLmlkID8gbmV3VmFsdWUuaWQgOiBudWxsO1xyXG5cclxuICAgIGlmICh0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzICYmIHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MuY2FsbFVuZG9Db25maXJtQWxlcnQpIHtcclxuICAgICAgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5jYWxsVW5kb0NvbmZpcm1BbGVydCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChyZXN1bHQudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGFyYW1zLnNlcnZpY2VBY2Nlc3MgJiYgdGhpcy5wYXJhbXMuc2VydmljZUFjY2Vzcy5maWx0ZXIpIHtcclxuICAgICAgICAgICAgICB0aGlzLnBhcmFtcy5zZXJ2aWNlQWNjZXNzLmFwcGx5Q2hhbmdlc1RvRmlsdGVyKHRoaXMucGFyYW1zLmZpZWxkLCB0aGlzLmJ1aWxkTW9kZWwoKSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucGFyYW1zLm9uRmxvYXRpbmdGaWx0ZXJDaGFuZ2VkKHsgbW9kZWw6IHRoaXMuYnVpbGRNb2RlbCgpIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnByZXZWYWx1ZSA9IHRoaXMuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VmFsdWUgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1Hcm91cC5wYXRjaFZhbHVlKHtcclxuICAgICAgICAgICAgICBmaWx0ZXJWYWx1ZTogdGhpcy5wcmV2VmFsdWUgfHwgKHRoaXMucHJldlZhbHVlICYmIHBhcnNlSW50KHRoaXMucHJldlZhbHVlLCAxMCkgPT09IDApID8gdGhpcy5wcmV2VmFsdWUgOiBudWxsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5wcmV2VmFsdWUgPSB0aGlzLmN1cnJlbnRWYWx1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uUGFyZW50TW9kZWxDaGFuZ2VkKHBhcmVudE1vZGVsOiBTZXJpYWxpemVkVGV4dEZpbHRlcik6IHZvaWQge1xyXG4gICAgLy8gaWYgKCFwYXJlbnRNb2RlbCkge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICB0aGlzLmN1cnJlbnRWYWx1ZSA9IHBhcmVudE1vZGVsLmZpbHRlcjtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG4gIGNoYW5nZWQoZGF0YTogYW55KSB7XHJcbiAgICBpZiAoZGF0YSAmJiAoZGF0YS5JZCB8fCBkYXRhLklkID09PSAwKSAmJiB0aGlzLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICB0aGlzLnZhbHVlQ2hhbmdlZCh0aGlzLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidWlsZE1vZGVsKCk6IFNlcmlhbGl6ZWRUZXh0RmlsdGVyIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZpbHRlclR5cGU6ICd0ZXh0JyxcclxuICAgICAgdHlwZTogdGhpcy5zZWxlY3RvckZvcm1Hcm91cC5nZXQoJ3NlbGVjdEZpbHRlclZhbHVlJykudmFsdWUsIC8vIGNhbiBiZSBzZW5kIGluIGhlcmVcclxuICAgICAgZmlsdGVyOiB0aGlzLmN1cnJlbnRWYWx1ZVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGluaXRpYXRlQ29tcGFyaXNvbkxpc3QoKTogdm9pZCB7XHJcbiAgICBjb25zdCBrZXlMaXN0ID0gQ29tcGFyaXNvbkxpc3QuY3JlYXRlS2V5TGFiZWxBcnJheShDb21wYXJpc29uVHlwZS5EZWZhdWx0KS5tYXAoeCA9PiB4LkRlZmluaXRpb24pO1xyXG5cclxuICAgIGNvbnN0IGRhdGFTb3VyY2UgPSBDb21wYXJpc29uTGlzdC5jcmVhdGVLZXlMYWJlbEFycmF5KENvbXBhcmlzb25UeXBlLkRlZmF1bHQpO1xyXG5cclxuICAgIHRoaXMudHJhbnNsYXRlLmdldChrZXlMaXN0KVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcclxuICAgICAgICBsZXQgY291bnRlciA9IDA7XHJcbiAgICAgICAga2V5TGlzdC5mb3JFYWNoKGtleSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0cmFuc2xhdGVkVmFsdWUgPSByZXN1bHRba2V5XTtcclxuICAgICAgICAgIGRhdGFTb3VyY2VbY291bnRlcl0uRGVmaW5pdGlvbiA9IHRyYW5zbGF0ZWRWYWx1ZTtcclxuICAgICAgICAgIGNvdW50ZXIrKztcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19