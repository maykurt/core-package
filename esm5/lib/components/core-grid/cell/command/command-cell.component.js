/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { OperationType } from '../../../../enums/operation-type.enum';
var CommandCellComponent = /** @class */ (function () {
    function CommandCellComponent() {
        this.showRowDeleteButton = true;
        this.showRowEditButton = true;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    CommandCellComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
        this.editable = this.params.context.componentParent.gridOptions.editable;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        /** @type {?} */
        var buttonSettings = this.params.context.componentParent.gridOptions.buttonSettings;
        if (buttonSettings) {
            if (buttonSettings.showRowDeleteButton !== undefined) {
                this.showRowDeleteButton = buttonSettings.showRowDeleteButton;
            }
            if (buttonSettings.showEditButton !== undefined) {
                this.showRowEditButton = buttonSettings.showEditButton;
            }
        }
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.refresh = /**
     * @return {?}
     */
    function () {
        return false;
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.showUndoButton = /**
     * @return {?}
     */
    function () {
        return this.params &&
            this.params.data &&
            this.params.data.OperationType !== OperationType.Created &&
            this.editable &&
            (!this.formGroup.pristine || this.params.data.OperationType === OperationType.Updated);
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.removeRow = /**
     * @return {?}
     */
    function () {
        this.params.context.componentParent.removeRow(this.params, this.rowId, this.key);
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.deleteRecord = /**
     * @return {?}
     */
    function () {
        this.params.context.componentParent.deleteRecord(this.params, this.rowId, this.key);
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.editRecord = /**
     * @return {?}
     */
    function () {
        this.params.context.componentParent.editRecord(this.params, this.rowId, this.key);
    };
    /**
     * @return {?}
     */
    CommandCellComponent.prototype.undoRecord = /**
     * @return {?}
     */
    function () {
        this.params.context.componentParent.undoRecord(this.params, this.rowId, this.key);
    };
    CommandCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid-command-cell',
                    template: "\n    <div *ngIf=\"formGroup\" style=\"margin-top: 4px;display: flex;\" >\n      <span  class=\"command-cell-item\">\n      <button *ngIf=\"showUndoButton()\"  type=\"button\" style=\"height: 20px\" (click)=\"undoRecord()\" class=\"pull-right btn-grid-inline btn-grid-edit\">\n      <core-icon icon=\"undo\"></core-icon>\n      </button>\n\n\t\t\t\t<button *ngIf=\"!editable && showRowDeleteButton\"  type=\"button\" style=\"height: 20px\" (click)=\"deleteRecord()\" class=\"pull-right btn-grid-inline btn-grid-delete\">\n          <core-icon icon=\"trash-alt\"></core-icon>\n\t\t\t\t</button>\n\n\t\t\t\t<button *ngIf=\"editable && showRowDeleteButton\"  type=\"button\" style=\"height: 20px\" (click)=\"removeRow()\" class=\"pull-right btn-grid-inline btn-grid-delete\">\n          <core-icon icon=\"trash-alt\"></core-icon>\n\t\t\t\t</button>\n\n\t\t\t\t<button *ngIf=\"!editable && showRowEditButton\"  type=\"button\" style=\"height: 20px\" (click)=\"editRecord()\" class=\"pull-right btn-grid-inline btn-grid-edit\">\n        <core-icon icon=\"edit\"></core-icon>\n        </button>\n      </span>\n    </div>\n  ",
                    styles: [".command-cell-item{margin-left:auto;margin-right:auto}.btn-grid-inline{color:#166dba!important;background:0 0!important;border:none!important;cursor:pointer;font-size:16px}.btn-grid-inline:focus{outline:0}.btn-grid-delete:hover{color:#e30a3a!important}.btn-grid-edit:hover{color:#17a2b8!important}"]
                }] }
    ];
    return CommandCellComponent;
}());
export { CommandCellComponent };
if (false) {
    /** @type {?} */
    CommandCellComponent.prototype.key;
    /** @type {?} */
    CommandCellComponent.prototype.value;
    /** @type {?} */
    CommandCellComponent.prototype.formGroup;
    /**
     * @type {?}
     * @private
     */
    CommandCellComponent.prototype.rowId;
    /**
     * @type {?}
     * @private
     */
    CommandCellComponent.prototype.params;
    /** @type {?} */
    CommandCellComponent.prototype.editable;
    /** @type {?} */
    CommandCellComponent.prototype.showRowDeleteButton;
    /** @type {?} */
    CommandCellComponent.prototype.showRowEditButton;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWFuZC1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2NvbW1hbmQvY29tbWFuZC1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUcxQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFHdEU7SUFBQTtRQWtDUyx3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDM0Isc0JBQWlCLEdBQUcsSUFBSSxDQUFDO0lBK0NsQyxDQUFDOzs7OztJQTdDQyxxQ0FBTTs7OztJQUFOLFVBQU8sTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQ3pFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7O1lBQzlELGNBQWMsR0FBbUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1FBQ3JHLElBQUksY0FBYyxFQUFFO1lBQ2xCLElBQUksY0FBYyxDQUFDLG1CQUFtQixLQUFLLFNBQVMsRUFBRTtnQkFDcEQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQzthQUMvRDtZQUNELElBQUksY0FBYyxDQUFDLGNBQWMsS0FBSyxTQUFTLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDO2FBQ3hEO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQsc0NBQU87OztJQUFQO1FBQ0UsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsNkNBQWM7OztJQUFkO1FBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTTtZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUk7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxLQUFLLGFBQWEsQ0FBQyxPQUFPO1lBQ3hELElBQUksQ0FBQyxRQUFRO1lBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0YsQ0FBQzs7OztJQUVELHdDQUFTOzs7SUFBVDtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuRixDQUFDOzs7O0lBRUQsMkNBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7Ozs7SUFFRCx5Q0FBVTs7O0lBQVY7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDcEYsQ0FBQzs7OztJQUVELHlDQUFVOzs7SUFBVjtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwRixDQUFDOztnQkFqRkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSwrQkFBK0I7b0JBQ3pDLFFBQVEsRUFDTixpbUNBb0JEOztpQkFFRjs7SUF5REQsMkJBQUM7Q0FBQSxBQWxGRCxJQWtGQztTQXhEWSxvQkFBb0I7OztJQUMvQixtQ0FBVzs7SUFDWCxxQ0FBYTs7SUFDYix5Q0FBNEI7Ozs7O0lBRTVCLHFDQUFtQjs7Ozs7SUFDbkIsc0NBQW9COztJQUNwQix3Q0FBZ0I7O0lBQ2hCLG1EQUFrQzs7SUFDbEMsaURBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IElDZWxsUmVuZGVyZXJBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcbmltcG9ydCB7IEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgT3BlcmF0aW9uVHlwZSB9IGZyb20gJy4uLy4uLy4uLy4uL2VudW1zL29wZXJhdGlvbi10eXBlLmVudW0nO1xyXG5pbXBvcnQgeyBCdXR0b25TZXR0aW5ncyB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVscy9idXR0b24tc2V0dGluZ3MubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkLWNvbW1hbmQtY2VsbCcsXHJcbiAgdGVtcGxhdGU6XHJcbiAgICBgXHJcbiAgICA8ZGl2ICpuZ0lmPVwiZm9ybUdyb3VwXCIgc3R5bGU9XCJtYXJnaW4tdG9wOiA0cHg7ZGlzcGxheTogZmxleDtcIiA+XHJcbiAgICAgIDxzcGFuICBjbGFzcz1cImNvbW1hbmQtY2VsbC1pdGVtXCI+XHJcbiAgICAgIDxidXR0b24gKm5nSWY9XCJzaG93VW5kb0J1dHRvbigpXCIgIHR5cGU9XCJidXR0b25cIiBzdHlsZT1cImhlaWdodDogMjBweFwiIChjbGljayk9XCJ1bmRvUmVjb3JkKClcIiBjbGFzcz1cInB1bGwtcmlnaHQgYnRuLWdyaWQtaW5saW5lIGJ0bi1ncmlkLWVkaXRcIj5cclxuICAgICAgPGNvcmUtaWNvbiBpY29uPVwidW5kb1wiPjwvY29yZS1pY29uPlxyXG4gICAgICA8L2J1dHRvbj5cclxuXHJcblx0XHRcdFx0PGJ1dHRvbiAqbmdJZj1cIiFlZGl0YWJsZSAmJiBzaG93Um93RGVsZXRlQnV0dG9uXCIgIHR5cGU9XCJidXR0b25cIiBzdHlsZT1cImhlaWdodDogMjBweFwiIChjbGljayk9XCJkZWxldGVSZWNvcmQoKVwiIGNsYXNzPVwicHVsbC1yaWdodCBidG4tZ3JpZC1pbmxpbmUgYnRuLWdyaWQtZGVsZXRlXCI+XHJcbiAgICAgICAgICA8Y29yZS1pY29uIGljb249XCJ0cmFzaC1hbHRcIj48L2NvcmUtaWNvbj5cclxuXHRcdFx0XHQ8L2J1dHRvbj5cclxuXHJcblx0XHRcdFx0PGJ1dHRvbiAqbmdJZj1cImVkaXRhYmxlICYmIHNob3dSb3dEZWxldGVCdXR0b25cIiAgdHlwZT1cImJ1dHRvblwiIHN0eWxlPVwiaGVpZ2h0OiAyMHB4XCIgKGNsaWNrKT1cInJlbW92ZVJvdygpXCIgY2xhc3M9XCJwdWxsLXJpZ2h0IGJ0bi1ncmlkLWlubGluZSBidG4tZ3JpZC1kZWxldGVcIj5cclxuICAgICAgICAgIDxjb3JlLWljb24gaWNvbj1cInRyYXNoLWFsdFwiPjwvY29yZS1pY29uPlxyXG5cdFx0XHRcdDwvYnV0dG9uPlxyXG5cclxuXHRcdFx0XHQ8YnV0dG9uICpuZ0lmPVwiIWVkaXRhYmxlICYmIHNob3dSb3dFZGl0QnV0dG9uXCIgIHR5cGU9XCJidXR0b25cIiBzdHlsZT1cImhlaWdodDogMjBweFwiIChjbGljayk9XCJlZGl0UmVjb3JkKClcIiBjbGFzcz1cInB1bGwtcmlnaHQgYnRuLWdyaWQtaW5saW5lIGJ0bi1ncmlkLWVkaXRcIj5cclxuICAgICAgICA8Y29yZS1pY29uIGljb249XCJlZGl0XCI+PC9jb3JlLWljb24+XHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICAgIDwvc3Bhbj5cclxuICAgIDwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29tbWFuZC1jZWxsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1hbmRDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgSUNlbGxSZW5kZXJlckFuZ3VsYXJDb21wIHtcclxuICBwdWJsaWMga2V5O1xyXG4gIHB1YmxpYyB2YWx1ZTtcclxuICBwdWJsaWMgZm9ybUdyb3VwOiBGb3JtQXJyYXk7XHJcblxyXG4gIHByaXZhdGUgcm93SWQ6IGFueTtcclxuICBwcml2YXRlIHBhcmFtczogYW55O1xyXG4gIHB1YmxpYyBlZGl0YWJsZTtcclxuICBwdWJsaWMgc2hvd1Jvd0RlbGV0ZUJ1dHRvbiA9IHRydWU7XHJcbiAgcHVibGljIHNob3dSb3dFZGl0QnV0dG9uID0gdHJ1ZTtcclxuXHJcbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICAgIHRoaXMua2V5ID0gdGhpcy5wYXJhbXMuY29udGV4dC5jcmVhdGVLZXkodGhpcy5wYXJhbXMuY29sdW1uQXBpLCBwYXJhbXMuY29sdW1uKTtcclxuICAgIHRoaXMudmFsdWUgPSB0aGlzLnBhcmFtcy52YWx1ZTtcclxuICAgIHRoaXMucm93SWQgPSB0aGlzLnBhcmFtcy5ub2RlLmlkO1xyXG4gICAgdGhpcy5lZGl0YWJsZSA9IHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50LmdyaWRPcHRpb25zLmVkaXRhYmxlO1xyXG4gICAgdGhpcy5mb3JtR3JvdXAgPSB0aGlzLnBhcmFtcy5jb250ZXh0LmZvcm1Hcm91cC5jb250cm9sc1t0aGlzLnJvd0lkXTtcclxuICAgIGNvbnN0IGJ1dHRvblNldHRpbmdzOiBCdXR0b25TZXR0aW5ncyA9IHRoaXMucGFyYW1zLmNvbnRleHQuY29tcG9uZW50UGFyZW50LmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzO1xyXG4gICAgaWYgKGJ1dHRvblNldHRpbmdzKSB7XHJcbiAgICAgIGlmIChidXR0b25TZXR0aW5ncy5zaG93Um93RGVsZXRlQnV0dG9uICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICB0aGlzLnNob3dSb3dEZWxldGVCdXR0b24gPSBidXR0b25TZXR0aW5ncy5zaG93Um93RGVsZXRlQnV0dG9uO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChidXR0b25TZXR0aW5ncy5zaG93RWRpdEJ1dHRvbiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgdGhpcy5zaG93Um93RWRpdEJ1dHRvbiA9IGJ1dHRvblNldHRpbmdzLnNob3dFZGl0QnV0dG9uO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgc2hvd1VuZG9CdXR0b24oKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5wYXJhbXMgJiZcclxuICAgICAgdGhpcy5wYXJhbXMuZGF0YSAmJlxyXG4gICAgICB0aGlzLnBhcmFtcy5kYXRhLk9wZXJhdGlvblR5cGUgIT09IE9wZXJhdGlvblR5cGUuQ3JlYXRlZCAmJlxyXG4gICAgICB0aGlzLmVkaXRhYmxlICYmXHJcbiAgICAgICghdGhpcy5mb3JtR3JvdXAucHJpc3RpbmUgfHwgdGhpcy5wYXJhbXMuZGF0YS5PcGVyYXRpb25UeXBlID09PSBPcGVyYXRpb25UeXBlLlVwZGF0ZWQpO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlUm93KCkge1xyXG4gICAgdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQucmVtb3ZlUm93KHRoaXMucGFyYW1zLCB0aGlzLnJvd0lkLCB0aGlzLmtleSk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVSZWNvcmQoKSB7XHJcbiAgICB0aGlzLnBhcmFtcy5jb250ZXh0LmNvbXBvbmVudFBhcmVudC5kZWxldGVSZWNvcmQodGhpcy5wYXJhbXMsIHRoaXMucm93SWQsIHRoaXMua2V5KTtcclxuICB9XHJcblxyXG4gIGVkaXRSZWNvcmQoKSB7XHJcbiAgICB0aGlzLnBhcmFtcy5jb250ZXh0LmNvbXBvbmVudFBhcmVudC5lZGl0UmVjb3JkKHRoaXMucGFyYW1zLCB0aGlzLnJvd0lkLCB0aGlzLmtleSk7XHJcbiAgfVxyXG5cclxuICB1bmRvUmVjb3JkKCkge1xyXG4gICAgdGhpcy5wYXJhbXMuY29udGV4dC5jb21wb25lbnRQYXJlbnQudW5kb1JlY29yZCh0aGlzLnBhcmFtcywgdGhpcy5yb3dJZCwgdGhpcy5rZXkpO1xyXG4gIH1cclxufVxyXG4iXX0=