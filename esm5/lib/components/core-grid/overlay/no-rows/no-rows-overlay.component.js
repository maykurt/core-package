/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var NoRowsOverlayComponent = /** @class */ (function () {
    function NoRowsOverlayComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    NoRowsOverlayComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
    };
    NoRowsOverlayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'grid-no-rows-overlay',
                    template: "<div>" +
                        "   {{ 'NoRows' | translate }}" +
                        "</div>"
                }] }
    ];
    return NoRowsOverlayComponent;
}());
export { NoRowsOverlayComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NoRowsOverlayComponent.prototype.params;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tcm93cy1vdmVybGF5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9vdmVybGF5L25vLXJvd3Mvbm8tcm93cy1vdmVybGF5LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUcxQztJQUFBO0lBWUEsQ0FBQzs7Ozs7SUFIQyx1Q0FBTTs7OztJQUFOLFVBQU8sTUFBTTtRQUNYLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7O2dCQVhGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxRQUFRLEVBQUUsT0FBTzt3QkFDZiwrQkFBK0I7d0JBQy9CLFFBQVE7aUJBQ1g7O0lBT0QsNkJBQUM7Q0FBQSxBQVpELElBWUM7U0FOWSxzQkFBc0I7Ozs7OztJQUNqQyx3Q0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSU5vUm93c092ZXJsYXlBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2dyaWQtbm8tcm93cy1vdmVybGF5JyxcclxuICB0ZW1wbGF0ZTogYDxkaXY+YCArXHJcbiAgICBgICAge3sgJ05vUm93cycgfCB0cmFuc2xhdGUgfX1gICtcclxuICAgIGA8L2Rpdj5gXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb1Jvd3NPdmVybGF5Q29tcG9uZW50IGltcGxlbWVudHMgSU5vUm93c092ZXJsYXlBbmd1bGFyQ29tcCB7XHJcbiAgcHJpdmF0ZSBwYXJhbXM6IGFueTtcclxuXHJcbiAgYWdJbml0KHBhcmFtcyk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgfVxyXG59XHJcbiJdfQ==