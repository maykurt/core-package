/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var LoadingOverlayComponent = /** @class */ (function () {
    function LoadingOverlayComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    LoadingOverlayComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
    };
    LoadingOverlayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'grid-loading-overlay',
                    template: "\n  <div class=\"loader\">\n  <div class=\"inner one\"></div>\n  <div class=\"inner two\"></div>\n  <div class=\"inner three\"></div> \n</div>\n  ",
                    styles: [".loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
                }] }
    ];
    return LoadingOverlayComponent;
}());
export { LoadingOverlayComponent };
if (false) {
    /** @type {?} */
    LoadingOverlayComponent.prototype.params;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy1vdmVybGF5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9vdmVybGF5L2xvYWRpbmcvbG9hZGluZy1vdmVybGF5LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUdqRDtJQUFBO0lBaUJBLENBQUM7Ozs7O0lBSEMsd0NBQU07Ozs7SUFBTixVQUFPLE1BQU07UUFDWCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN2QixDQUFDOztnQkFoQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLFFBQVEsRUFBRSxvSkFNVDs7aUJBRUY7O0lBT0QsOEJBQUM7Q0FBQSxBQWpCRCxJQWlCQztTQU5ZLHVCQUF1Qjs7O0lBQ2xDLHlDQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJTG9hZGluZ092ZXJsYXlBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2dyaWQtbG9hZGluZy1vdmVybGF5JyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxkaXYgY2xhc3M9XCJsb2FkZXJcIj5cclxuICA8ZGl2IGNsYXNzPVwiaW5uZXIgb25lXCI+PC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImlubmVyIHR3b1wiPjwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJpbm5lciB0aHJlZVwiPjwvZGl2PiBcclxuPC9kaXY+XHJcbiAgYCxcclxuICBzdHlsZVVybHM6IFsnbG9hZGluZy1vdmVybGF5LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIExvYWRpbmdPdmVybGF5Q29tcG9uZW50IGltcGxlbWVudHMgSUxvYWRpbmdPdmVybGF5QW5ndWxhckNvbXAge1xyXG4gIHBhcmFtczogYW55O1xyXG5cclxuICBhZ0luaXQocGFyYW1zKTogdm9pZCB7XHJcbiAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcclxuICB9XHJcbn1cclxuIl19