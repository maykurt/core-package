/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { CoreGridService } from './services/core-grid.service';
import { SweetAlertService } from '../../services/index';
import { ConfirmDialogOperationType } from '../../enums/index';
import { GridLoadingService } from './services/grid-loading.service';
var CoreGridComponent = /** @class */ (function () {
    function CoreGridComponent(gridService, sweetAlertService) {
        var _this = this;
        this.gridService = gridService;
        this.sweetAlertService = sweetAlertService;
        this.loadOnInit = true;
        this.selectedChanged = new EventEmitter();
        this.doubleClickSelectedChanged = new EventEmitter();
        this.recordEvents = new EventEmitter();
        this.retrievedDataSourceEvent = new EventEmitter();
        this.paginationSelectorSource = [];
        this.paginationSelectorSource = [
            { Id: 1, Definition: 1 },
            { Id: 5, Definition: 5 },
            { Id: 10, Definition: 10 },
            { Id: 25, Definition: 25 },
            { Id: 50, Definition: 50 }
        ];
        this.gridStyle = {
            'height': '500px',
            'margin-top': '-1px'
        };
        this.recordEventsSubscription = this.gridService.recordEventsSubject
            .subscribe(function (data) {
            _this.recordEvents.emit(data);
        });
        this.retrievedDataSourceEventSubscription = this.gridService.retrievedDataSourceSubject
            .subscribe(function (data) {
            _this.retrievedDataSourceEvent.emit(data);
        });
    }
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.gridService.gridOptions = this.gridOptions;
        this.gridService.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    CoreGridComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.isInsideModal && this.isInsideModal) {
            /** @type {?} */
            var height = window.innerHeight * 60 / 100;
            this.gridStyle = {
                'height': height > 500 ? '500px' : height + 'px',
                'margin-top': '-1px'
            };
        }
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.load = /**
     * @return {?}
     */
    function () {
        this.gridService.init(this.gridOptions, true);
    };
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    CoreGridComponent.prototype.setColumnDataSource = /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    function (columnName, dataSource) {
        this.gridService.setColumnDataSource(columnName, dataSource);
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.refreshGridDataSource = /**
     * @return {?}
     */
    function () {
        this.gridService.refreshGridData(true);
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.reloadColumnDefs = /**
     * @return {?}
     */
    function () {
        this.gridService.reloadColumnDefs();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.isDataUpdated = /**
     * @return {?}
     */
    function () {
        return true;
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.addRow = /**
     * @return {?}
     */
    function () {
        this.gridService.addRow();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.revertDataSource = /**
     * @return {?}
     */
    function () {
        this.gridService.revertData();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.getBulkOperationData = /**
     * @return {?}
     */
    function () {
        return this.gridService.getBulkOperationData();
    };
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    CoreGridComponent.prototype.removeRow = /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    function (params, rowId, key) {
        this.gridService.removeRow(params, rowId, key);
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.refreshAndRevertChanges = /**
     * @return {?}
     */
    function () {
        this.gridService.refreshAndRevertChanges();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.validateForm = /**
     * @return {?}
     */
    function () {
        return this.gridService.validateForm();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.showAddButton = /**
     * @return {?}
     */
    function () {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showAddButton !== undefined) {
                return this.gridOptions.buttonSettings.showAddButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.showReloadButton = /**
     * @return {?}
     */
    function () {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showReloadButton !== undefined) {
                return this.gridOptions.buttonSettings.showReloadButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.showRevertButton = /**
     * @return {?}
     */
    function () {
        return this.isChanged() && this.isEditable();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.isEditable = /**
     * @return {?}
     */
    function () {
        return this.gridOptions && this.gridOptions.editable;
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.isChanged = /**
     * @return {?}
     */
    function () {
        return this.gridService.isChanged();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.getTotalCount = /**
     * @return {?}
     */
    function () {
        return this.gridService.getTotalCount();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridComponent.prototype.onSelectionChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var selectedRows = this.gridService.getSelectedRows();
        this.selectedChanged.emit(selectedRows);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridComponent.prototype.onRowDoubleClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var selectedRows = this.gridService.getSelectedRows();
        this.doubleClickSelectedChanged.emit(selectedRows);
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.getSelectedRows = /**
     * @return {?}
     */
    function () {
        return this.gridService.getSelectedRows();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.getSelectedRow = /**
     * @return {?}
     */
    function () {
        return _.head(this.gridService.getSelectedRows());
    };
    /**
     * @param {?} data
     * @return {?}
     */
    CoreGridComponent.prototype.onFirstValue = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data.Id) {
            this.prevItemsPerPage = data.Id;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridComponent.prototype.onChangeItemsPerPage = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        if (this.gridOptions && this.gridOptions.editable) {
            /** @type {?} */
            var bulkData = this.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                    .subscribe(function (res) {
                    if (res && res.value) {
                        _this.gridService.onChangeItemsPerPage(event);
                        _this.prevItemsPerPage = event.Id;
                    }
                    else {
                        _this.gridService.pagingResult.PageSize = _this.prevItemsPerPage;
                    }
                });
            }
            else {
                this.gridService.onChangeItemsPerPage(event);
                this.prevItemsPerPage = event.Id;
            }
        }
        else {
            this.gridService.onChangeItemsPerPage(event);
            this.prevItemsPerPage = event.Id;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CoreGridComponent.prototype.onPageChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        if (event.page !== this.currentPage) {
            if (this.gridOptions && this.gridOptions.editable) {
                /** @type {?} */
                var bulkData = this.getBulkOperationData();
                if (bulkData && bulkData.length > 0) {
                    this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                        .subscribe(function (res) {
                        if (res && res.value) {
                            _this.gridService.onPageChanged(event);
                        }
                        else {
                            _this.currentPage = _this.gridService.pagingResult.CurrentPage;
                        }
                    });
                }
                else {
                    this.gridService.onPageChanged(event);
                }
            }
            else {
                this.gridService.onPageChanged(event);
            }
        }
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.getColumnState = /**
     * @return {?}
     */
    function () {
        this.gridService.getColumnState();
    };
    /**
     * @return {?}
     */
    CoreGridComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.recordEventsSubscription) {
            this.recordEventsSubscription.unsubscribe();
        }
        if (this.retrievedDataSourceEventSubscription) {
            this.retrievedDataSourceEventSubscription.unsubscribe();
        }
    };
    CoreGridComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-core-grid',
                    template: "<div class=\"core-grid\">\r\n  <div class=\"core-grid-header\">\r\n    <h5>{{gridService.gridOptions.title | translate}}</h5>\r\n\r\n    <button *ngIf=\"showReloadButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\"{{'Refresh' | translate}}\"\r\n            (click)=\"refreshGridDataSource()\">\r\n      <core-icon icon=\"sync\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showAddButton()\" type=\"button\" class=\"btn btn-sm btn-info\" title=\"{{'Add' | translate}}\"\r\n            (click)=\"addRow()\">\r\n      <core-icon icon=\"plus\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showRevertButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\" {{'Revert' | translate}}\"\r\n            (click)=\"revertDataSource()\">\r\n      <core-icon icon=\"undo\"></core-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n  <!-- suppressColumnVirtualisation = Set to true so that the grid doesn't virtualise the columns.\r\n  So if you have 100 columns, but only 10 visible due to scrolling, all 100 will always be rendered. -->\r\n\r\n  <ag-grid-angular #coreGrid [ngStyle]=\"gridStyle\" class=\"ag-theme-balham\" [rowHeight]=\"40\"\r\n                   [floatingFiltersHeight]=\"40\" [suppressRowTransform]=\"true\" [sortingOrder]=\"['asc', 'desc']\"\r\n                   [sortable]=\"gridService.gridOptions.enableSorting\" [filter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowBuffer]=\"50\" [suppressColumnVirtualisation]=\"true\" [suppressDragLeaveHidesColumns]=\"true\"\r\n                   [ensureDomOrder]=\"true\" [rowData]=\"gridService.data\" [columnDefs]=\"gridService.columnDefs\"\r\n                   [accentedSort]=\"true\" [floatingFilter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowSelection]=\"gridService.gridOptions.rowSelection\" [context]=\"gridService.getContext(gridService)\"\r\n                   [frameworkComponents]=\"gridService.getComponents()\"\r\n                   [noRowsOverlayComponent]=\"gridService.getNoRowsOverlayComponent()\"\r\n                   [loadingOverlayComponent]=\"gridService.getLoadingOverlayComponent()\"\r\n                   (rowDataChanged)=\"gridService.refreshFormControls()\" (gridReady)=\"gridService.onGridReady($event)\"\r\n                   (columnEverythingChanged)=\"gridService.onColumnEverythingChanged($event)\"\r\n                   (gridSizeChanged)=\"gridService.onGridSizeChanged()\"\r\n                   (cellValueChanged)=\"gridService.onCellValueChanged($event)\"\r\n                   (filterChanged)=\"gridService.onFilterChanged($event)\" (selectionChanged)=\"onSelectionChanged($event)\"\r\n                   (rowDoubleClicked)=\"onRowDoubleClick($event)\">\r\n  </ag-grid-angular>\r\n\r\n  <!-- (sortChanged)=\"gridService.onSortChanged($event)\"  -->\r\n\r\n  <div #inputRef class=\"under-grid-footer\">\r\n    <ng-content select=\"[footer]\"></ng-content>\r\n  </div>\r\n\r\n  <div *ngIf=\"gridService && gridService.pagingResult && (gridService.pagingResult.PageSize || gridService.pagingResult.PageSize === 0)\"\r\n       class=\"core-grid-pager\">\r\n    <div>\r\n      <layout-static-selector class=\"pull-left grid-static-layout-selector\" [dataSource]=\"paginationSelectorSource\"\r\n                              [searchable]=\"false\" [clearable]=\"false\" [(ngModel)]=\"gridService.pagingResult.PageSize\"\r\n                              (changed)=\"onChangeItemsPerPage($event)\" (firstValue)=\"onFirstValue($event)\"\r\n                              ngDefaultControl>\r\n      </layout-static-selector>\r\n\r\n      <pagination class=\"grid-pagination\" [totalItems]=\"gridService.pagingResult.TotalCount\"\r\n                  [itemsPerPage]=\"gridService.pagingResult.PageSize\" [(ngModel)]=\"currentPage\" [maxSize]=\"5\"\r\n                  [rotate]=\"true\" [boundaryLinks]=\"true\" previousText=\"{{'PreviousePage'|translate}}\"\r\n                  nextText=\"{{'NextPage'|translate}}\" firstText=\"{{'FirstPage'|translate}}\"\r\n                  lastText=\"{{'LastPage'|translate}}\" (pageChanged)=\"onPageChanged($event)\">\r\n      </pagination>\r\n\r\n    </div>\r\n\r\n    <div>\r\n      <span> <b>{{'TotalRowCount'|translate}}: </b>{{gridService.pagingResult.TotalCount}}</span>\r\n    </div>\r\n  </div>\r\n</div>",
                    encapsulation: ViewEncapsulation.None,
                    providers: [GridLoadingService, CoreGridService],
                    styles: [".ag-cell{overflow:visible!important}.core-grid-header{font-size:12px!important;background-color:#166dba!important;border:none!important;padding:5px;height:37px;border-radius:2px 2px 0 0}.core-grid-header h5{float:left;margin-top:4px;color:#fff;font-size:14px;padding-left:5px}.btn-sm{float:right;margin-left:8px}.ag-header-cell{background-color:#fff!important;color:#175169!important;font-weight:700!important}.ag-cell,.ag-floating-filter-body{padding-top:1px}.ag-cell input,.ag-floating-filter-body input{height:30px!important;margin-top:3px}.ag-floating-filter-body input{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;height:30px!important;margin-top:4px}.ag-cell ng-select input,.ag-floating-filter-body ng-select input{height:19px!important}.ag-floating-filter-button{padding-top:3px}.core-grid-pager{background:#fff;display:flex;align-items:center;overflow:hidden;font-size:14px;margin-top:-1px;border:1px solid #bdc3c7;justify-content:space-between;padding:5px 12px}.grid-pagination{padding:0;float:left;margin-bottom:0}.grid-static-layout-selector{margin-right:12px;margin-bottom:2px}.ag-floating-filter-button{display:none}.grid-filter-icon{position:absolute;right:-22px;top:2px}.under-grid-footer{background-color:#fff;border-right:1px solid #d7dfe3;border-left:1px solid #d7dfe3;border-top:0}.grid-pagination .page-item.active .page-link{background-color:#166dba!important;border-color:#166dba!important}"]
                }] }
    ];
    /** @nocollapse */
    CoreGridComponent.ctorParameters = function () { return [
        { type: CoreGridService },
        { type: SweetAlertService }
    ]; };
    CoreGridComponent.propDecorators = {
        gridOptions: [{ type: Input }],
        loadOnInit: [{ type: Input }],
        selectedChanged: [{ type: Output }],
        doubleClickSelectedChanged: [{ type: Output }],
        recordEvents: [{ type: Output }],
        retrievedDataSourceEvent: [{ type: Output }],
        loadColumnDataSourceOnInit: [{ type: Input }],
        isInsideModal: [{ type: Input }]
    };
    return CoreGridComponent;
}());
export { CoreGridComponent };
if (false) {
    /** @type {?} */
    CoreGridComponent.prototype.gridOptions;
    /** @type {?} */
    CoreGridComponent.prototype.loadOnInit;
    /** @type {?} */
    CoreGridComponent.prototype.selectedChanged;
    /** @type {?} */
    CoreGridComponent.prototype.doubleClickSelectedChanged;
    /** @type {?} */
    CoreGridComponent.prototype.recordEvents;
    /** @type {?} */
    CoreGridComponent.prototype.retrievedDataSourceEvent;
    /** @type {?} */
    CoreGridComponent.prototype.paginationSelectorSource;
    /** @type {?} */
    CoreGridComponent.prototype.loadColumnDataSourceOnInit;
    /** @type {?} */
    CoreGridComponent.prototype.currentPage;
    /** @type {?} */
    CoreGridComponent.prototype.prevItemsPerPage;
    /** @type {?} */
    CoreGridComponent.prototype.gridStyle;
    /** @type {?} */
    CoreGridComponent.prototype.isInsideModal;
    /** @type {?} */
    CoreGridComponent.prototype.recordEventsSubscription;
    /** @type {?} */
    CoreGridComponent.prototype.retrievedDataSourceEventSubscription;
    /** @type {?} */
    CoreGridComponent.prototype.gridService;
    /**
     * @type {?}
     * @private
     */
    CoreGridComponent.prototype.sweetAlertService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jb3JlLWdyaWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULEtBQUssRUFFTCxpQkFBaUIsRUFDakIsTUFBTSxFQUNOLFlBQVksRUFJYixNQUFNLGVBQWUsQ0FBQztBQUd2QixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFL0QsT0FBTyxFQUNMLGlCQUFpQixFQUNsQixNQUFNLHNCQUFzQixDQUFDO0FBTzlCLE9BQU8sRUFDTCwwQkFBMEIsRUFDM0IsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUdyRTtJQTJCRSwyQkFBbUIsV0FBNEIsRUFBVSxpQkFBb0M7UUFBN0YsaUJBdUJDO1FBdkJrQixnQkFBVyxHQUFYLFdBQVcsQ0FBaUI7UUFBVSxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBbEJwRixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUM1QywrQkFBMEIsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3ZELGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2Qyw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzdELDZCQUF3QixHQUFRLEVBQUUsQ0FBQztRQWNqQyxJQUFJLENBQUMsd0JBQXdCLEdBQUc7WUFDOUIsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUU7WUFDeEIsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUU7WUFDeEIsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUU7WUFDMUIsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUU7WUFDMUIsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUU7U0FDM0IsQ0FBQztRQUVGLElBQUksQ0FBQyxTQUFTLEdBQUc7WUFDZixRQUFRLEVBQUUsT0FBTztZQUNqQixZQUFZLEVBQUUsTUFBTTtTQUNyQixDQUFDO1FBRUYsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CO2FBQ2pFLFNBQVMsQ0FBQyxVQUFDLElBQVM7WUFDbkIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLENBQUMsb0NBQW9DLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQywwQkFBMEI7YUFDcEYsU0FBUyxDQUFDLFVBQUMsSUFBUztZQUNuQixLQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELG9DQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO0lBQzVGLENBQUM7Ozs7O0lBRUQsdUNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFOztnQkFDekMsTUFBTSxHQUFXLE1BQU0sQ0FBQyxXQUFXLEdBQUcsRUFBRSxHQUFHLEdBQUc7WUFFcEQsSUFBSSxDQUFDLFNBQVMsR0FBRztnQkFDZixRQUFRLEVBQUUsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSTtnQkFDaEQsWUFBWSxFQUFFLE1BQU07YUFDckIsQ0FBQztTQUNIO0lBQ0gsQ0FBQzs7OztJQUVELGdDQUFJOzs7SUFBSjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7O0lBRUQsK0NBQW1COzs7OztJQUFuQixVQUFvQixVQUFrQixFQUFFLFVBQWU7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDL0QsQ0FBQzs7OztJQUVELGlEQUFxQjs7O0lBQXJCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVELDRDQUFnQjs7O0lBQWhCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCx5Q0FBYTs7O0lBQWI7UUFDRSxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7SUFFRCxrQ0FBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCw0Q0FBZ0I7OztJQUFoQjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELGdEQUFvQjs7O0lBQXBCO1FBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDakQsQ0FBQzs7Ozs7OztJQUVELHFDQUFTOzs7Ozs7SUFBVCxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsR0FBRztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7SUFFRCxtREFBdUI7OztJQUF2QjtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztJQUM3QyxDQUFDOzs7O0lBRUQsd0NBQVk7OztJQUFaO1FBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFFRCx5Q0FBYTs7O0lBQWI7UUFDRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUU7WUFDdkQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO2dCQUMvRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQzthQUN0RDtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDOzs7O0lBRUQsNENBQWdCOzs7SUFBaEI7UUFDRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUU7WUFDdkQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLEVBQUU7Z0JBQ2xFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7OztJQUVELDRDQUFnQjs7O0lBQWhCO1FBQ0UsT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQy9DLENBQUM7Ozs7SUFFRCxzQ0FBVTs7O0lBQVY7UUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7SUFDdkQsQ0FBQzs7OztJQUVELHFDQUFTOzs7SUFBVDtRQUNFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQseUNBQWE7OztJQUFiO1FBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsOENBQWtCOzs7O0lBQWxCLFVBQW1CLEtBQUs7O1lBQ2hCLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRTtRQUN2RCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUVELDRDQUFnQjs7OztJQUFoQixVQUFpQixLQUFLOztZQUNkLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRTtRQUN2RCxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7SUFFRCwyQ0FBZTs7O0lBQWY7UUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELDBDQUFjOzs7SUFBZDtRQUNFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Ozs7SUFHRCx3Q0FBWTs7OztJQUFaLFVBQWEsSUFBUztRQUNwQixJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDWCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7O0lBRUQsZ0RBQW9COzs7O0lBQXBCLFVBQXFCLEtBQVU7UUFBL0IsaUJBdUJDO1FBdEJDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTs7Z0JBQzNDLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDNUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUM7cUJBQ3RGLFNBQVMsQ0FBQyxVQUFDLEdBQWdCO29CQUMxQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsS0FBSyxFQUFFO3dCQUNwQixLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM3QyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQztxQkFDbEM7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztxQkFDaEU7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQzthQUNsQztTQUVGO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDO1NBQ2xDO0lBRUgsQ0FBQzs7Ozs7SUFFRCx5Q0FBYTs7OztJQUFiLFVBQWMsS0FBVTtRQUF4QixpQkFxQkM7UUFwQkMsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbkMsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFOztvQkFDM0MsUUFBUSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtnQkFDNUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQ0FBaUMsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUM7eUJBQ3RGLFNBQVMsQ0FBQyxVQUFDLEdBQWdCO3dCQUMxQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsS0FBSyxFQUFFOzRCQUNwQixLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdkM7NkJBQU07NEJBQ0wsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7eUJBQzlEO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNO29CQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN2QzthQUVGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3ZDO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQsMENBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsdUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzdDO1FBRUQsSUFBSSxJQUFJLENBQUMsb0NBQW9DLEVBQUU7WUFDN0MsSUFBSSxDQUFDLG9DQUFvQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3pEO0lBQ0gsQ0FBQzs7Z0JBN09GLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixndklBQXlDO29CQUV6QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsU0FBUyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsZUFBZSxDQUFDOztpQkFDakQ7Ozs7Z0JBeEJRLGVBQWU7Z0JBR3RCLGlCQUFpQjs7OzhCQXVCaEIsS0FBSzs2QkFDTCxLQUFLO2tDQUNMLE1BQU07NkNBQ04sTUFBTTsrQkFDTixNQUFNOzJDQUNOLE1BQU07NkNBR04sS0FBSztnQ0FNTCxLQUFLOztJQXdOUix3QkFBQztDQUFBLEFBOU9ELElBOE9DO1NBdk9ZLGlCQUFpQjs7O0lBQzVCLHdDQUFrQzs7SUFDbEMsdUNBQTJCOztJQUMzQiw0Q0FBc0Q7O0lBQ3RELHVEQUFpRTs7SUFDakUseUNBQWlEOztJQUNqRCxxREFBNkQ7O0lBQzdELHFEQUFtQzs7SUFFbkMsdURBQThDOztJQUU5Qyx3Q0FBb0I7O0lBQ3BCLDZDQUF5Qjs7SUFFekIsc0NBQXNCOztJQUN0QiwwQ0FBaUM7O0lBRWpDLHFEQUF1Qzs7SUFDdkMsaUVBQW1EOztJQUV2Qyx3Q0FBbUM7Ozs7O0lBQUUsOENBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgSW5wdXQsXHJcbiAgT25Jbml0LFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgU2ltcGxlQ2hhbmdlcyxcclxuICBPbkNoYW5nZXMsXHJcbiAgT25EZXN0cm95XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgQ29yZUdyaWRTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb3JlLWdyaWQuc2VydmljZSc7XHJcblxyXG5pbXBvcnQge1xyXG4gIFN3ZWV0QWxlcnRTZXJ2aWNlXHJcbn0gZnJvbSAnLi4vLi4vc2VydmljZXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICBHcmlkT3B0aW9ucyxcclxuICBBbGVydFJlc3VsdFxyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gIENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlXHJcbn0gZnJvbSAnLi4vLi4vZW51bXMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgR3JpZExvYWRpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9ncmlkLWxvYWRpbmcuc2VydmljZSc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtY29yZS1ncmlkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29yZS1ncmlkLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb3JlLWdyaWQuY29tcG9uZW50LnNjc3MnXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHByb3ZpZGVyczogW0dyaWRMb2FkaW5nU2VydmljZSwgQ29yZUdyaWRTZXJ2aWNlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZUdyaWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcywgT25EZXN0cm95IHtcclxuICBASW5wdXQoKSBncmlkT3B0aW9uczogR3JpZE9wdGlvbnM7XHJcbiAgQElucHV0KCkgbG9hZE9uSW5pdCA9IHRydWU7XHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgQE91dHB1dCgpIGRvdWJsZUNsaWNrU2VsZWN0ZWRDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICBAT3V0cHV0KCkgcmVjb3JkRXZlbnRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHJldHJpZXZlZERhdGFTb3VyY2VFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIHBhZ2luYXRpb25TZWxlY3RvclNvdXJjZTogYW55ID0gW107XHJcblxyXG4gIEBJbnB1dCgpIGxvYWRDb2x1bW5EYXRhU291cmNlT25Jbml0PzogYm9vbGVhbjtcclxuXHJcbiAgY3VycmVudFBhZ2U6IG51bWJlcjtcclxuICBwcmV2SXRlbXNQZXJQYWdlOiBudW1iZXI7XHJcblxyXG4gIHB1YmxpYyBncmlkU3R5bGU6IGFueTtcclxuICBASW5wdXQoKSBpc0luc2lkZU1vZGFsPzogYm9vbGVhbjtcclxuXHJcbiAgcmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgcmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBncmlkU2VydmljZTogQ29yZUdyaWRTZXJ2aWNlLCBwcml2YXRlIHN3ZWV0QWxlcnRTZXJ2aWNlOiBTd2VldEFsZXJ0U2VydmljZSkge1xyXG4gICAgdGhpcy5wYWdpbmF0aW9uU2VsZWN0b3JTb3VyY2UgPSBbXHJcbiAgICAgIHsgSWQ6IDEsIERlZmluaXRpb246IDEgfSxcclxuICAgICAgeyBJZDogNSwgRGVmaW5pdGlvbjogNSB9LFxyXG4gICAgICB7IElkOiAxMCwgRGVmaW5pdGlvbjogMTAgfSxcclxuICAgICAgeyBJZDogMjUsIERlZmluaXRpb246IDI1IH0sXHJcbiAgICAgIHsgSWQ6IDUwLCBEZWZpbml0aW9uOiA1MCB9XHJcbiAgICBdO1xyXG5cclxuICAgIHRoaXMuZ3JpZFN0eWxlID0ge1xyXG4gICAgICAnaGVpZ2h0JzogJzUwMHB4JyxcclxuICAgICAgJ21hcmdpbi10b3AnOiAnLTFweCdcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yZWNvcmRFdmVudHNTdWJzY3JpcHRpb24gPSB0aGlzLmdyaWRTZXJ2aWNlLnJlY29yZEV2ZW50c1N1YmplY3RcclxuICAgICAgLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgdGhpcy5yZWNvcmRFdmVudHMuZW1pdChkYXRhKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgdGhpcy5yZXRyaWV2ZWREYXRhU291cmNlRXZlbnRTdWJzY3JpcHRpb24gPSB0aGlzLmdyaWRTZXJ2aWNlLnJldHJpZXZlZERhdGFTb3VyY2VTdWJqZWN0XHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgIHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50LmVtaXQoZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRTZXJ2aWNlLmdyaWRPcHRpb25zID0gdGhpcy5ncmlkT3B0aW9ucztcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuaW5pdCh0aGlzLmdyaWRPcHRpb25zLCB0aGlzLmxvYWRPbkluaXQsIHRoaXMubG9hZENvbHVtbkRhdGFTb3VyY2VPbkluaXQpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMuaXNJbnNpZGVNb2RhbCAmJiB0aGlzLmlzSW5zaWRlTW9kYWwpIHtcclxuICAgICAgY29uc3QgaGVpZ2h0OiBudW1iZXIgPSB3aW5kb3cuaW5uZXJIZWlnaHQgKiA2MCAvIDEwMDtcclxuXHJcbiAgICAgIHRoaXMuZ3JpZFN0eWxlID0ge1xyXG4gICAgICAgICdoZWlnaHQnOiBoZWlnaHQgPiA1MDAgPyAnNTAwcHgnIDogaGVpZ2h0ICsgJ3B4JyxcclxuICAgICAgICAnbWFyZ2luLXRvcCc6ICctMXB4J1xyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuaW5pdCh0aGlzLmdyaWRPcHRpb25zLCB0cnVlKTtcclxuICB9XHJcblxyXG4gIHNldENvbHVtbkRhdGFTb3VyY2UoY29sdW1uTmFtZTogc3RyaW5nLCBkYXRhU291cmNlOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2Uuc2V0Q29sdW1uRGF0YVNvdXJjZShjb2x1bW5OYW1lLCBkYXRhU291cmNlKTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hHcmlkRGF0YVNvdXJjZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UucmVmcmVzaEdyaWREYXRhKHRydWUpO1xyXG4gIH1cclxuXHJcbiAgcmVsb2FkQ29sdW1uRGVmcygpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UucmVsb2FkQ29sdW1uRGVmcygpO1xyXG4gIH1cclxuXHJcbiAgaXNEYXRhVXBkYXRlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgYWRkUm93KCk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5hZGRSb3coKTtcclxuICB9XHJcblxyXG4gIHJldmVydERhdGFTb3VyY2UoKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRTZXJ2aWNlLnJldmVydERhdGEoKTtcclxuICB9XHJcblxyXG4gIGdldEJ1bGtPcGVyYXRpb25EYXRhKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5nZXRCdWxrT3BlcmF0aW9uRGF0YSgpO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlUm93KHBhcmFtcywgcm93SWQsIGtleSk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5yZW1vdmVSb3cocGFyYW1zLCByb3dJZCwga2V5KTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hBbmRSZXZlcnRDaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgdGhpcy5ncmlkU2VydmljZS5yZWZyZXNoQW5kUmV2ZXJ0Q2hhbmdlcygpO1xyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVGb3JtKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZ3JpZFNlcnZpY2UudmFsaWRhdGVGb3JtKCk7XHJcbiAgfVxyXG5cclxuICBzaG93QWRkQnV0dG9uKCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMgJiYgdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncykge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93QWRkQnV0dG9uICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5ncmlkT3B0aW9ucy5idXR0b25TZXR0aW5ncy5zaG93QWRkQnV0dG9uO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNob3dSZWxvYWRCdXR0b24oKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzKSB7XHJcbiAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSZWxvYWRCdXR0b24gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdyaWRPcHRpb25zLmJ1dHRvblNldHRpbmdzLnNob3dSZWxvYWRCdXR0b247XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2hvd1JldmVydEJ1dHRvbigpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmlzQ2hhbmdlZCgpICYmIHRoaXMuaXNFZGl0YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGU7XHJcbiAgfVxyXG5cclxuICBpc0NoYW5nZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5pc0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIGdldFRvdGFsQ291bnQoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLmdyaWRTZXJ2aWNlLmdldFRvdGFsQ291bnQoKTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0aW9uQ2hhbmdlZChldmVudCk6IHZvaWQge1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRSb3dzID0gdGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGFuZ2VkLmVtaXQoc2VsZWN0ZWRSb3dzKTtcclxuICB9XHJcblxyXG4gIG9uUm93RG91YmxlQ2xpY2soZXZlbnQpOiB2b2lkIHtcclxuICAgIGNvbnN0IHNlbGVjdGVkUm93cyA9IHRoaXMuZ3JpZFNlcnZpY2UuZ2V0U2VsZWN0ZWRSb3dzKCk7XHJcbiAgICB0aGlzLmRvdWJsZUNsaWNrU2VsZWN0ZWRDaGFuZ2VkLmVtaXQoc2VsZWN0ZWRSb3dzKTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93cygpOiBhbnlbXSB7XHJcbiAgICByZXR1cm4gdGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93KCk6IGFueVtdIHtcclxuICAgIHJldHVybiBfLmhlYWQodGhpcy5ncmlkU2VydmljZS5nZXRTZWxlY3RlZFJvd3MoKSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgb25GaXJzdFZhbHVlKGRhdGE6IGFueSk6IHZvaWQge1xyXG4gICAgaWYgKGRhdGEuSWQpIHtcclxuICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZGF0YS5JZDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zICYmIHRoaXMuZ3JpZE9wdGlvbnMuZWRpdGFibGUpIHtcclxuICAgICAgY29uc3QgYnVsa0RhdGEgPSB0aGlzLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcbiAgICAgIGlmIChidWxrRGF0YSAmJiBidWxrRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5zd2VldEFsZXJ0U2VydmljZS5tYW5hZ2VSZXF1ZXN0T3BlcmF0aW9uV2l0aENvbmZpcm0oQ29uZmlybURpYWxvZ09wZXJhdGlvblR5cGUuVW5kbylcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlcyAmJiByZXMudmFsdWUpIHtcclxuICAgICAgICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50KTtcclxuICAgICAgICAgICAgICB0aGlzLnByZXZJdGVtc1BlclBhZ2UgPSBldmVudC5JZDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLnBhZ2luZ1Jlc3VsdC5QYWdlU2l6ZSA9IHRoaXMucHJldkl0ZW1zUGVyUGFnZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ncmlkU2VydmljZS5vbkNoYW5nZUl0ZW1zUGVyUGFnZShldmVudCk7XHJcbiAgICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZXZlbnQuSWQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uQ2hhbmdlSXRlbXNQZXJQYWdlKGV2ZW50KTtcclxuICAgICAgdGhpcy5wcmV2SXRlbXNQZXJQYWdlID0gZXZlbnQuSWQ7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgb25QYWdlQ2hhbmdlZChldmVudDogYW55KTogdm9pZCB7XHJcbiAgICBpZiAoZXZlbnQucGFnZSAhPT0gdGhpcy5jdXJyZW50UGFnZSkge1xyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLmVkaXRhYmxlKSB7XHJcbiAgICAgICAgY29uc3QgYnVsa0RhdGEgPSB0aGlzLmdldEJ1bGtPcGVyYXRpb25EYXRhKCk7XHJcbiAgICAgICAgaWYgKGJ1bGtEYXRhICYmIGJ1bGtEYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIHRoaXMuc3dlZXRBbGVydFNlcnZpY2UubWFuYWdlUmVxdWVzdE9wZXJhdGlvbldpdGhDb25maXJtKENvbmZpcm1EaWFsb2dPcGVyYXRpb25UeXBlLlVuZG8pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHJlczogQWxlcnRSZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICBpZiAocmVzICYmIHJlcy52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkU2VydmljZS5vblBhZ2VDaGFuZ2VkKGV2ZW50KTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHRoaXMuZ3JpZFNlcnZpY2UucGFnaW5nUmVzdWx0LkN1cnJlbnRQYWdlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuZ3JpZFNlcnZpY2Uub25QYWdlQ2hhbmdlZChldmVudCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmdyaWRTZXJ2aWNlLm9uUGFnZUNoYW5nZWQoZXZlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRDb2x1bW5TdGF0ZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZFNlcnZpY2UuZ2V0Q29sdW1uU3RhdGUoKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmVjb3JkRXZlbnRzU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMucmV0cmlldmVkRGF0YVNvdXJjZUV2ZW50U3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==