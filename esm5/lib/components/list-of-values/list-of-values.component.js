/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, forwardRef, Output, EventEmitter, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as _ from 'lodash';
import { TextComparison } from '../../enums/comparison-type.enum';
import { ListOfValuesService } from './list-of-values.service';
import { ListOfValuesModalComponent } from './list-of-values-modal/list-of-values-modal.component';
var ListOfValuesComponent = /** @class */ (function () {
    function ListOfValuesComponent(listOfValuesService) {
        this.listOfValuesService = listOfValuesService;
        this.isReadOnly = false;
        // @ContentChild(FormTextInputComponent) formTextInputComponent;
        this.lovValueChanged = new EventEmitter();
        this.addToDataSource = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.listOfValuesService.lovServiceResultStateChanged
            .subscribe(function (result) {
            _this.initialData = result;
            _this.setLovFields();
        });
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.openModal = /**
     * @return {?}
     */
    function () {
        this.displayData = {
            display: true,
            gridOptions: this.gridOptions
        };
    };
    /**
     * @param {?} selectedModel
     * @return {?}
     */
    ListOfValuesComponent.prototype.getSelectedRow = /**
     * @param {?} selectedModel
     * @return {?}
     */
    function (selectedModel) {
        var _this = this;
        if (selectedModel) {
            this.KeyColumnValue = selectedModel[this.gridOptions.keyField];
            this.value = selectedModel[this.valueColumnName];
            this.label = '';
            if (this.labelColumnNames instanceof Array) {
                /** @type {?} */
                var labels = (/** @type {?} */ (this.labelColumnNames));
                labels.forEach(function (columnName) {
                    _this.label = _this.label + ' ' + selectedModel[columnName];
                });
            }
            else if (this.labelColumnNames) {
                this.label = selectedModel[this.labelColumnNames.toString()];
            }
            // it will send the grid edit cell
            this.addToDataSource.emit({
                Id: this.listOfValuesService.endPointUrl,
                Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
            });
            this.hasSelectedItem = true;
            this.lovValueChanged.emit({ id: this.KeyColumnValue, value: this.value, label: this.label });
        }
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.gridReload = /**
     * @return {?}
     */
    function () {
        this.lovModal.gridReload();
    };
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @param {?=} dontInitData
     * @return {?}
     */
    ListOfValuesComponent.prototype.setOptions = /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @param {?=} dontInitData
     * @return {?}
     */
    function (gridOptions, valueColumnName, labelColumnNames, dontInitData) {
        if (valueColumnName === void 0) { valueColumnName = 'Id'; }
        if (labelColumnNames === void 0) { labelColumnNames = 'Definition'; }
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
        if (!dontInitData && this.keyColumnValue) {
            this.initData();
        }
        else if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
        }
    };
    /**
     * @param {?} gridOptions
     * @param {?=} initData
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    ListOfValuesComponent.prototype.setGridOptions = /**
     * @param {?} gridOptions
     * @param {?=} initData
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    function (gridOptions, initData, valueColumnName, labelColumnNames) {
        this.gridOptions = gridOptions;
        if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
            if (initData) {
                this.initData();
            }
        }
        if (valueColumnName) {
            this.valueColumnName = valueColumnName;
        }
        if (labelColumnNames) {
            this.labelColumnNames = labelColumnNames;
        }
    };
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    ListOfValuesComponent.prototype.setOptionsWithNoRequest = /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    function (gridOptions, valueColumnName, labelColumnNames) {
        if (valueColumnName === void 0) { valueColumnName = 'Id'; }
        if (labelColumnNames === void 0) { labelColumnNames = 'Definition'; }
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.initData = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.gridOptions && this.keyColumnValue && this.keyColumnValue !== -1) {
            // console.log('initData - gridOptions', this.gridOptions);
            // console.log('initData - dataSource', this.dataSource);
            // console.log('initData - keyColumnValue', this.keyColumnValue);
            if (this.dataSource) {
                /** @type {?} */
                var selectedRowData_1 = this.dataSource.find(function (x) { return x.Id === _this.gridOptions.requestOptions.UrlOptions.endPointUrl
                    && x.Value.id === _this.keyColumnValue; });
                // console.log('initData - valueColumnName', this.valueColumnName);
                // console.log('initData - keyColumnValue', this.keyColumnValue);
                // console.log('initData - selectedRowData', selectedRowData);
                if (selectedRowData_1) {
                    setTimeout(function () {
                        _this.label = selectedRowData_1.Value.label;
                        _this.value = selectedRowData_1.Value.value;
                    }, 10);
                    return;
                }
            }
            if (this.gridOptions.requestOptions && this.gridOptions.requestOptions.UrlOptions) {
                this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                this.fieldIniData = this.gridOptions.keyField;
                if (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined) {
                    if (this.fieldIniData.indexOf('Id') > 0) {
                        this.fieldIniData = this.fieldIniData.substr(0, this.fieldIniData.indexOf('Id')) + '.' + this.fieldIniData.substr(this.fieldIniData.indexOf('Id'), this.fieldIniData.length);
                    }
                }
                this.genericExpression = {
                    FilterGroups: [
                        {
                            Filters: [
                                { PropertyName: this.fieldIniData, Value: this.keyColumnValue, Comparison: TextComparison.EqualTo }
                            ]
                        }
                    ],
                    PageSize: 1,
                    PageNumber: 1
                };
                if (this.gridOptions.requestOptions.CustomFilter) {
                    if (this.gridOptions.requestOptions.CustomFilter.FilterGroups) {
                        this.gridOptions.requestOptions.CustomFilter.FilterGroups.forEach(function (item) {
                            _this.genericExpression.FilterGroups.push(item);
                        });
                    }
                }
                this.listOfValuesService.loadDataByFilterAndPageSetting(this.genericExpression, this.gridOptions.requestOptions.HeaderParameters);
            }
        }
    };
    /**
     * @param {?=} disableSetToKeyColumnValue
     * @return {?}
     */
    ListOfValuesComponent.prototype.clearLov = /**
     * @param {?=} disableSetToKeyColumnValue
     * @return {?}
     */
    function (disableSetToKeyColumnValue) {
        if (!disableSetToKeyColumnValue) {
            this.KeyColumnValue = null;
        }
        else {
            this.keyColumnValue = null;
        }
        this.value = null;
        this.label = null;
        this.hasSelectedItem = false;
        // if (this.formTextInputComponent) {
        //   this.formTextInputComponent.inputValue = null;
        // }
        if (!disableSetToKeyColumnValue) {
            this.lovValueChanged.emit(null);
        }
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.showClearButton = /**
     * @return {?}
     */
    function () {
        return !_.isNil(this.KeyColumnValue);
    };
    /**
     * @param {?} val
     * @return {?}
     */
    ListOfValuesComponent.prototype.onChange = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.onTouched = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(ListOfValuesComponent.prototype, "KeyColumnValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this.keyColumnValue;
        },
        set: /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            this.keyColumnValue = val;
            this.setLovFields();
            this.onChange(val);
            this.onTouched();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} fn
     * @return {?}
     */
    ListOfValuesComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    ListOfValuesComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        // console.log('writeValue1', value);
        // console.log('writeValue2', this.keyColumnValue);
        if (value) {
            if (value !== this.keyColumnValue) {
                this.keyColumnValue = value;
                this.initData();
            }
        }
        else {
            this.keyColumnValue = value;
            this.clearLov(true);
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    ListOfValuesComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.setLovFields = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!_.isNil(this.keyColumnValue) && !_.isNil(this.gridOptions)) {
            /** @type {?} */
            var keyColumn_1 = this.gridOptions.columns.find(function (item) { return item.field === _this.gridOptions.keyField; });
            if (!_.isNil(this.initialData) && keyColumn_1 && keyColumn_1.field) {
                /** @type {?} */
                var filteredObject_1 = this.initialData.find(function (item) { return item[keyColumn_1.field] === _this.keyColumnValue; });
                if (!_.isNil(filteredObject_1)) {
                    this.value = filteredObject_1[this.valueColumnName];
                    this.label = '';
                    this.freeText = '';
                    if (this.labelColumnNames instanceof Array) {
                        /** @type {?} */
                        var labels = (/** @type {?} */ (this.labelColumnNames));
                        labels.forEach(function (columnName) {
                            _this.label = _this.label + ' ' + filteredObject_1[columnName];
                            _this.freeText = _this.freeText + ' ' + filteredObject_1[columnName];
                        });
                    }
                    else if (this.labelColumnNames) {
                        this.label = filteredObject_1[this.labelColumnNames.toString()];
                        this.freeText = filteredObject_1[this.labelColumnNames.toString()];
                    }
                    // it will send the grid edit cell
                    this.addToDataSource.emit({
                        Id: this.listOfValuesService.endPointUrl,
                        Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
                    });
                    this.hasSelectedItem = true;
                }
            }
        }
    };
    /**
     * @return {?}
     */
    ListOfValuesComponent.prototype.getInputFiledWidth = /**
     * @return {?}
     */
    function () {
        if (!this.isReadOnly) {
            if (this.showClearButton()) {
                return 'calc(60% - 76px)';
            }
            else {
                return 'calc(60% - 36px)';
            }
        }
        else {
            return 'calc(60% )';
        }
    };
    ListOfValuesComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-list-of-values',
                    template: "<div class=\"form-lov\">\r\n  <layout-form-text-input [isReadOnly]=\"true\" [(ngModel)]=\"KeyColumnValue\" [hidden]=\"true\"></layout-form-text-input>\r\n  <div class=\"row no-gutters\">\r\n    <div class=\"pr-2\" style=\"width: 40%;\">\r\n      <layout-form-text-input [class]=\"isReadOnly ? '' : 'showLikeEditable'\" [isReadOnly]=\"true\" [(ngModel)]=\"value\">\r\n      </layout-form-text-input>\r\n    </div>\r\n    <div [class.pr-2]=\"!isReadOnly\" [ngStyle]=\"{'width': getInputFiledWidth()}\">\r\n      <layout-form-text-input *ngIf=\"!inputRef.innerHTML.trim() || (inputRef.innerHTML.trim() && hasSelectedItem)\"\r\n        [isReadOnly]=\"true\" [placeholder]=\"placeholder\" [class]=\"isReadOnly ? '' : 'showLikeEditable'\"\r\n        [(ngModel)]=\"label\"></layout-form-text-input>\r\n      <div #inputRef [hidden]=\"hasSelectedItem\">\r\n        <ng-content select=\"layout-form-text-input\"></ng-content>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"!isReadOnly\" class=\"lov-btn-container\" [ngStyle]=\"{'width': showClearButton() ? '76px' : '36px'}\">\r\n      <button type=\"button\" class=\"btn btn-info\" title=\" {{'Search' | translate}}\" (click)=\"openModal()\">\r\n        <core-icon icon=\"search\"></core-icon>\r\n      </button>\r\n      <button type=\"button\" [hidden]=\"!showClearButton()\" class=\"btn btn-danger ml-1\" title=\" {{'Clear' | translate}}\"\r\n        (click)=\"clearLov()\" style=\"float:right;\">\r\n        <core-icon icon=\"times\"></core-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<layout-list-of-values-modal #lovModal [displayData]=\"displayData\" (getSelectedRow)=\"getSelectedRow($event)\">\r\n</layout-list-of-values-modal>",
                    encapsulation: ViewEncapsulation.None,
                    providers: [
                        ListOfValuesService,
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return ListOfValuesComponent; }),
                            multi: true
                        }
                    ],
                    styles: [".showLikeEditable>input{background-color:#fff!important}"]
                }] }
    ];
    /** @nocollapse */
    ListOfValuesComponent.ctorParameters = function () { return [
        { type: ListOfValuesService }
    ]; };
    ListOfValuesComponent.propDecorators = {
        placeholder: [{ type: Input }],
        isReadOnly: [{ type: Input }],
        dataSource: [{ type: Input }],
        lovValueChanged: [{ type: Output }],
        addToDataSource: [{ type: Output }],
        lovModal: [{ type: ViewChild, args: ['lovModal',] }]
    };
    return ListOfValuesComponent;
}());
export { ListOfValuesComponent };
if (false) {
    /** @type {?} */
    ListOfValuesComponent.prototype.placeholder;
    /** @type {?} */
    ListOfValuesComponent.prototype.isReadOnly;
    /** @type {?} */
    ListOfValuesComponent.prototype.dataSource;
    /** @type {?} */
    ListOfValuesComponent.prototype.lovValueChanged;
    /** @type {?} */
    ListOfValuesComponent.prototype.addToDataSource;
    /** @type {?} */
    ListOfValuesComponent.prototype.lovModal;
    /** @type {?} */
    ListOfValuesComponent.prototype.value;
    /** @type {?} */
    ListOfValuesComponent.prototype.label;
    /** @type {?} */
    ListOfValuesComponent.prototype.freeText;
    /** @type {?} */
    ListOfValuesComponent.prototype.hasSelectedItem;
    /** @type {?} */
    ListOfValuesComponent.prototype.keyColumnValue;
    /** @type {?} */
    ListOfValuesComponent.prototype.valueColumnName;
    /** @type {?} */
    ListOfValuesComponent.prototype.labelColumnNames;
    /** @type {?} */
    ListOfValuesComponent.prototype.gridOptions;
    /** @type {?} */
    ListOfValuesComponent.prototype.initialData;
    /** @type {?} */
    ListOfValuesComponent.prototype.displayData;
    /** @type {?} */
    ListOfValuesComponent.prototype.genericExpression;
    /** @type {?} */
    ListOfValuesComponent.prototype.fieldIniData;
    /** @type {?} */
    ListOfValuesComponent.prototype.listOfValuesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekgsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBUTVCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUVsRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUUvRCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUVuRztJQTRDRSwrQkFBbUIsbUJBQXdDO1FBQXhDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUEzQmxELGVBQVUsR0FBRyxLQUFLLENBQUM7O1FBS2xCLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQWEsQ0FBQztRQUNoRCxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFzQnBELENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyw0QkFBNEI7YUFDbEQsU0FBUyxDQUFDLFVBQUMsTUFBYTtZQUN2QixLQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUMxQixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQseUNBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixPQUFPLEVBQUUsSUFBSTtZQUNiLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztTQUM5QixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCw4Q0FBYzs7OztJQUFkLFVBQWUsYUFBYTtRQUE1QixpQkF3QkM7UUF2QkMsSUFBSSxhQUFhLEVBQUU7WUFDakIsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7WUFFakQsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLFlBQVksS0FBSyxFQUFFOztvQkFDcEMsTUFBTSxHQUFhLG1CQUFVLElBQUksQ0FBQyxnQkFBZ0IsRUFBQTtnQkFDeEQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQWtCO29CQUNoQyxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDNUQsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDOUQ7WUFFRCxrQ0FBa0M7WUFDbEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7Z0JBQ3hCLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVztnQkFDeEMsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7YUFDekUsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7U0FDOUY7SUFDSCxDQUFDOzs7O0lBRUQsMENBQVU7OztJQUFWO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7Ozs7OztJQUVELDBDQUFVOzs7Ozs7O0lBQVYsVUFBVyxXQUF3QixFQUFFLGVBQThCLEVBQUUsZ0JBQWtELEVBQUUsWUFBc0I7UUFBMUcsZ0NBQUEsRUFBQSxzQkFBOEI7UUFBRSxpQ0FBQSxFQUFBLCtCQUFrRDtRQUNySCxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztRQUN2QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFFekMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjthQUFNLElBQUksV0FBVyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN4RjtJQUNILENBQUM7Ozs7Ozs7O0lBRUQsOENBQWM7Ozs7Ozs7SUFBZCxVQUFlLFdBQXdCLEVBQUUsUUFBa0IsRUFBRSxlQUF3QixFQUFFLGdCQUF5QjtRQUM5RyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLFdBQVcsRUFBRTtZQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN2RixJQUFJLFFBQVEsRUFBRTtnQkFDWixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7U0FDRjtRQUVELElBQUksZUFBZSxFQUFFO1lBQ25CLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1NBQ3hDO1FBRUQsSUFBSSxnQkFBZ0IsRUFBRTtZQUNwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7U0FDMUM7SUFDSCxDQUFDOzs7Ozs7O0lBRUQsdURBQXVCOzs7Ozs7SUFBdkIsVUFBd0IsV0FBd0IsRUFBRSxlQUE4QixFQUFFLGdCQUFrRDtRQUFsRixnQ0FBQSxFQUFBLHNCQUE4QjtRQUFFLGlDQUFBLEVBQUEsK0JBQWtEO1FBQ2xJLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztJQUMzQyxDQUFDOzs7O0lBRUQsd0NBQVE7OztJQUFSO1FBQUEsaUJBd0RDO1FBdkRDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDekUsMkRBQTJEO1lBQzNELHlEQUF5RDtZQUN6RCxpRUFBaUU7WUFFakUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOztvQkFDYixpQkFBZSxHQUFRLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVzt1QkFDakgsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLGNBQWMsRUFEZ0IsQ0FDaEIsQ0FBQztnQkFDeEMsbUVBQW1FO2dCQUNuRSxpRUFBaUU7Z0JBQ2pFLDhEQUE4RDtnQkFHOUQsSUFBSSxpQkFBZSxFQUFFO29CQUNuQixVQUFVLENBQUM7d0JBQ1QsS0FBSSxDQUFDLEtBQUssR0FBRyxpQkFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7d0JBQ3pDLEtBQUksQ0FBQyxLQUFLLEdBQUcsaUJBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUMzQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBRVAsT0FBTztpQkFDUjthQUNGO1lBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdkYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztnQkFDOUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGNBQWMsS0FBSyxTQUFTLEVBQUU7b0JBQzVILElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUN2QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUM5SztpQkFDRjtnQkFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUc7b0JBQ3ZCLFlBQVksRUFBRTt3QkFDWjs0QkFDRSxPQUFPLEVBQUU7Z0NBQ1AsRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxVQUFVLEVBQUUsY0FBYyxDQUFDLE9BQU8sRUFBRTs2QkFDcEc7eUJBQ0Y7cUJBQ0Y7b0JBQ0QsUUFBUSxFQUFFLENBQUM7b0JBQ1gsVUFBVSxFQUFFLENBQUM7aUJBQ2QsQ0FBQztnQkFFRixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRTtvQkFDaEQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFO3dCQUM3RCxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ3BFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNqRCxDQUFDLENBQUMsQ0FBQztxQkFDSjtpQkFDRjtnQkFFRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDbkk7U0FDRjtJQUNILENBQUM7Ozs7O0lBRUQsd0NBQVE7Ozs7SUFBUixVQUFTLDBCQUFvQztRQUMzQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDNUI7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFFN0IscUNBQXFDO1FBQ3JDLG1EQUFtRDtRQUNuRCxJQUFJO1FBRUosSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQy9CLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7OztJQUVELCtDQUFlOzs7SUFBZjtRQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELHdDQUFROzs7O0lBQVIsVUFBUyxHQUFRO0lBQ2pCLENBQUM7Ozs7SUFFRCx5Q0FBUzs7O0lBQVQ7SUFDQSxDQUFDO0lBRUQsc0JBQUksaURBQWM7Ozs7UUFBbEI7WUFDRSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDN0IsQ0FBQzs7Ozs7UUFFRCxVQUFtQixHQUFHO1lBQ3BCLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDO1lBQzFCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQixDQUFDOzs7T0FQQTs7Ozs7SUFTRCxnREFBZ0I7Ozs7SUFBaEIsVUFBaUIsRUFBRTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELDBDQUFVOzs7O0lBQVYsVUFBVyxLQUFLO1FBQ2QscUNBQXFDO1FBQ3JDLG1EQUFtRDtRQUNuRCxJQUFJLEtBQUssRUFBRTtZQUNULElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2dCQUM1QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyQjtJQUNILENBQUM7Ozs7O0lBRUQsaURBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQUU7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELDRDQUFZOzs7SUFBWjtRQUFBLGlCQWtDQztRQWpDQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTs7Z0JBQ3pELFdBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsS0FBSyxLQUFLLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUF4QyxDQUF3QyxDQUFDO1lBRW5HLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxXQUFTLElBQUksV0FBUyxDQUFDLEtBQUssRUFBRTs7b0JBQ3hELGdCQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsV0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUksQ0FBQyxjQUFjLEVBQTdDLENBQTZDLENBQUM7Z0JBQ3JHLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGdCQUFjLENBQUMsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxnQkFBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFFbEQsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUVuQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsWUFBWSxLQUFLLEVBQUU7OzRCQUNwQyxNQUFNLEdBQWEsbUJBQVUsSUFBSSxDQUFDLGdCQUFnQixFQUFBO3dCQUN4RCxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsVUFBa0I7NEJBQ2hDLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsZ0JBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDM0QsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxnQkFBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUNuRSxDQUFDLENBQUMsQ0FBQztxQkFDSjt5QkFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxnQkFBYyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3dCQUM5RCxJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFjLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7cUJBQ2xFO29CQUdELGtDQUFrQztvQkFDbEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7d0JBQ3hCLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVzt3QkFDeEMsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7cUJBQ3pFLENBQUMsQ0FBQztvQkFFSCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDN0I7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELGtEQUFrQjs7O0lBQWxCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLEVBQUU7Z0JBQzFCLE9BQU8sa0JBQWtCLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsT0FBTyxrQkFBa0IsQ0FBQzthQUMzQjtTQUNGO2FBQU07WUFDTCxPQUFPLFlBQVksQ0FBQztTQUNyQjtJQUNILENBQUM7O2dCQXRTRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsaXJEQUE4QztvQkFFOUMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLFNBQVMsRUFBRTt3QkFDVCxtQkFBbUI7d0JBQ25COzRCQUNFLE9BQU8sRUFBRSxpQkFBaUI7NEJBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLHFCQUFxQixFQUFyQixDQUFxQixDQUFDOzRCQUNwRCxLQUFLLEVBQUUsSUFBSTt5QkFDWjtxQkFDRjs7aUJBQ0Y7Ozs7Z0JBakJRLG1CQUFtQjs7OzhCQW1CekIsS0FBSzs2QkFFTCxLQUFLOzZCQUVMLEtBQUs7a0NBR0wsTUFBTTtrQ0FDTixNQUFNOzJCQUVOLFNBQVMsU0FBQyxVQUFVOztJQThRdkIsNEJBQUM7Q0FBQSxBQXZTRCxJQXVTQztTQXpSWSxxQkFBcUI7OztJQUNoQyw0Q0FBOEI7O0lBRTlCLDJDQUE0Qjs7SUFFNUIsMkNBQTRCOztJQUc1QixnREFBMEQ7O0lBQzFELGdEQUFvRDs7SUFFcEQseUNBQTREOztJQUU1RCxzQ0FBcUI7O0lBQ3JCLHNDQUFxQjs7SUFDckIseUNBQXdCOztJQUN4QixnREFBZ0M7O0lBRWhDLCtDQUFzQjs7SUFFdEIsZ0RBQStCOztJQUMvQixpREFBMkM7O0lBRTNDLDRDQUFnQzs7SUFDaEMsNENBQTBCOztJQUUxQiw0Q0FBd0I7O0lBQ3hCLGtEQUE0Qzs7SUFDNUMsNkNBQXlCOztJQUViLG9EQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgZm9yd2FyZFJlZiwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHtcclxuICBMb3ZSZXN1bHQsXHJcbiAgR3JpZE9wdGlvbnMsXHJcbiAgR2VuZXJpY0V4cHJlc3Npb25cclxufSBmcm9tICcuLi8uLi9tb2RlbHMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgVGV4dENvbXBhcmlzb24gfSBmcm9tICcuLi8uLi9lbnVtcy9jb21wYXJpc29uLXR5cGUuZW51bSc7XHJcblxyXG5pbXBvcnQgeyBMaXN0T2ZWYWx1ZXNTZXJ2aWNlIH0gZnJvbSAnLi9saXN0LW9mLXZhbHVlcy5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IExpc3RPZlZhbHVlc01vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9saXN0LW9mLXZhbHVlcy1tb2RhbC9saXN0LW9mLXZhbHVlcy1tb2RhbC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtbGlzdC1vZi12YWx1ZXMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9saXN0LW9mLXZhbHVlcy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbGlzdC1vZi12YWx1ZXMuY29tcG9uZW50LnNjc3MnXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgTGlzdE9mVmFsdWVzU2VydmljZSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IExpc3RPZlZhbHVlc0NvbXBvbmVudCksXHJcbiAgICAgIG11bHRpOiB0cnVlXHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGlzdE9mVmFsdWVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpIGlzUmVhZE9ubHkgPSBmYWxzZTtcclxuXHJcbiAgQElucHV0KCkgZGF0YVNvdXJjZT86IGFueVtdO1xyXG5cclxuICAvLyBAQ29udGVudENoaWxkKEZvcm1UZXh0SW5wdXRDb21wb25lbnQpIGZvcm1UZXh0SW5wdXRDb21wb25lbnQ7XHJcbiAgQE91dHB1dCgpIGxvdlZhbHVlQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8TG92UmVzdWx0PigpO1xyXG4gIEBPdXRwdXQoKSBhZGRUb0RhdGFTb3VyY2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgQFZpZXdDaGlsZCgnbG92TW9kYWwnKSBsb3ZNb2RhbDogTGlzdE9mVmFsdWVzTW9kYWxDb21wb25lbnQ7XHJcblxyXG4gIHB1YmxpYyB2YWx1ZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBsYWJlbDogc3RyaW5nO1xyXG4gIHB1YmxpYyBmcmVlVGV4dDogc3RyaW5nO1xyXG4gIHB1YmxpYyBoYXNTZWxlY3RlZEl0ZW06IGJvb2xlYW47XHJcblxyXG4gIHB1YmxpYyBrZXlDb2x1bW5WYWx1ZTtcclxuXHJcbiAgcHVibGljIHZhbHVlQ29sdW1uTmFtZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBsYWJlbENvbHVtbk5hbWVzOiBzdHJpbmcgfCBzdHJpbmdbXTtcclxuXHJcbiAgcHVibGljIGdyaWRPcHRpb25zOiBHcmlkT3B0aW9ucztcclxuICBwdWJsaWMgaW5pdGlhbERhdGE6IGFueVtdO1xyXG5cclxuICBwdWJsaWMgZGlzcGxheURhdGE6IGFueTtcclxuICBwdWJsaWMgZ2VuZXJpY0V4cHJlc3Npb246IEdlbmVyaWNFeHByZXNzaW9uO1xyXG4gIHB1YmxpYyBmaWVsZEluaURhdGE6IGFueTtcclxuXHJcbiAgY29uc3RydWN0b3IocHVibGljIGxpc3RPZlZhbHVlc1NlcnZpY2U6IExpc3RPZlZhbHVlc1NlcnZpY2UpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLmxvdlNlcnZpY2VSZXN1bHRTdGF0ZUNoYW5nZWRcclxuICAgICAgLnN1YnNjcmliZSgocmVzdWx0OiBhbnlbXSkgPT4ge1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbERhdGEgPSByZXN1bHQ7XHJcbiAgICAgICAgdGhpcy5zZXRMb3ZGaWVsZHMoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBvcGVuTW9kYWwoKSB7XHJcbiAgICB0aGlzLmRpc3BsYXlEYXRhID0ge1xyXG4gICAgICBkaXNwbGF5OiB0cnVlLFxyXG4gICAgICBncmlkT3B0aW9uczogdGhpcy5ncmlkT3B0aW9uc1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkUm93KHNlbGVjdGVkTW9kZWwpIHtcclxuICAgIGlmIChzZWxlY3RlZE1vZGVsKSB7XHJcbiAgICAgIHRoaXMuS2V5Q29sdW1uVmFsdWUgPSBzZWxlY3RlZE1vZGVsW3RoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGRdO1xyXG4gICAgICB0aGlzLnZhbHVlID0gc2VsZWN0ZWRNb2RlbFt0aGlzLnZhbHVlQ29sdW1uTmFtZV07XHJcblxyXG4gICAgICB0aGlzLmxhYmVsID0gJyc7XHJcbiAgICAgIGlmICh0aGlzLmxhYmVsQ29sdW1uTmFtZXMgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgIGNvbnN0IGxhYmVsczogc3RyaW5nW10gPSA8c3RyaW5nW10+dGhpcy5sYWJlbENvbHVtbk5hbWVzO1xyXG4gICAgICAgIGxhYmVscy5mb3JFYWNoKChjb2x1bW5OYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmxhYmVsICsgJyAnICsgc2VsZWN0ZWRNb2RlbFtjb2x1bW5OYW1lXTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmxhYmVsQ29sdW1uTmFtZXMpIHtcclxuICAgICAgICB0aGlzLmxhYmVsID0gc2VsZWN0ZWRNb2RlbFt0aGlzLmxhYmVsQ29sdW1uTmFtZXMudG9TdHJpbmcoKV07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIGl0IHdpbGwgc2VuZCB0aGUgZ3JpZCBlZGl0IGNlbGxcclxuICAgICAgdGhpcy5hZGRUb0RhdGFTb3VyY2UuZW1pdCh7XHJcbiAgICAgICAgSWQ6IHRoaXMubGlzdE9mVmFsdWVzU2VydmljZS5lbmRQb2ludFVybCxcclxuICAgICAgICBWYWx1ZTogeyBpZDogdGhpcy5LZXlDb2x1bW5WYWx1ZSwgdmFsdWU6IHRoaXMudmFsdWUsIGxhYmVsOiB0aGlzLmxhYmVsIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICB0aGlzLmhhc1NlbGVjdGVkSXRlbSA9IHRydWU7XHJcbiAgICAgIHRoaXMubG92VmFsdWVDaGFuZ2VkLmVtaXQoeyBpZDogdGhpcy5LZXlDb2x1bW5WYWx1ZSwgdmFsdWU6IHRoaXMudmFsdWUsIGxhYmVsOiB0aGlzLmxhYmVsIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ3JpZFJlbG9hZCgpIHtcclxuICAgIHRoaXMubG92TW9kYWwuZ3JpZFJlbG9hZCgpO1xyXG4gIH1cclxuXHJcbiAgc2V0T3B0aW9ucyhncmlkT3B0aW9uczogR3JpZE9wdGlvbnMsIHZhbHVlQ29sdW1uTmFtZTogc3RyaW5nID0gJ0lkJywgbGFiZWxDb2x1bW5OYW1lczogc3RyaW5nIHwgc3RyaW5nW10gPSAnRGVmaW5pdGlvbicsIGRvbnRJbml0RGF0YT86IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuZ3JpZE9wdGlvbnMgPSBncmlkT3B0aW9ucztcclxuICAgIHRoaXMudmFsdWVDb2x1bW5OYW1lID0gdmFsdWVDb2x1bW5OYW1lO1xyXG4gICAgdGhpcy5sYWJlbENvbHVtbk5hbWVzID0gbGFiZWxDb2x1bW5OYW1lcztcclxuXHJcbiAgICBpZiAoIWRvbnRJbml0RGF0YSAmJiB0aGlzLmtleUNvbHVtblZhbHVlKSB7XHJcbiAgICAgIHRoaXMuaW5pdERhdGEoKTtcclxuICAgIH0gZWxzZSBpZiAoZ3JpZE9wdGlvbnMpIHtcclxuICAgICAgdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLnNldExvYWRVcmxTZXR0aW5nKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRHcmlkT3B0aW9ucyhncmlkT3B0aW9uczogR3JpZE9wdGlvbnMsIGluaXREYXRhPzogYm9vbGVhbiwgdmFsdWVDb2x1bW5OYW1lPzogc3RyaW5nLCBsYWJlbENvbHVtbk5hbWVzPzogc3RyaW5nKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRPcHRpb25zID0gZ3JpZE9wdGlvbnM7XHJcbiAgICBpZiAoZ3JpZE9wdGlvbnMpIHtcclxuICAgICAgdGhpcy5saXN0T2ZWYWx1ZXNTZXJ2aWNlLnNldExvYWRVcmxTZXR0aW5nKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuVXJsT3B0aW9ucyk7XHJcbiAgICAgIGlmIChpbml0RGF0YSkge1xyXG4gICAgICAgIHRoaXMuaW5pdERhdGEoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh2YWx1ZUNvbHVtbk5hbWUpIHtcclxuICAgICAgdGhpcy52YWx1ZUNvbHVtbk5hbWUgPSB2YWx1ZUNvbHVtbk5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGxhYmVsQ29sdW1uTmFtZXMpIHtcclxuICAgICAgdGhpcy5sYWJlbENvbHVtbk5hbWVzID0gbGFiZWxDb2x1bW5OYW1lcztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldE9wdGlvbnNXaXRoTm9SZXF1ZXN0KGdyaWRPcHRpb25zOiBHcmlkT3B0aW9ucywgdmFsdWVDb2x1bW5OYW1lOiBzdHJpbmcgPSAnSWQnLCBsYWJlbENvbHVtbk5hbWVzOiBzdHJpbmcgfCBzdHJpbmdbXSA9ICdEZWZpbml0aW9uJykge1xyXG4gICAgdGhpcy5ncmlkT3B0aW9ucyA9IGdyaWRPcHRpb25zO1xyXG4gICAgdGhpcy52YWx1ZUNvbHVtbk5hbWUgPSB2YWx1ZUNvbHVtbk5hbWU7XHJcbiAgICB0aGlzLmxhYmVsQ29sdW1uTmFtZXMgPSBsYWJlbENvbHVtbk5hbWVzO1xyXG4gIH1cclxuXHJcbiAgaW5pdERhdGEoKSB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucyAmJiB0aGlzLmtleUNvbHVtblZhbHVlICYmIHRoaXMua2V5Q29sdW1uVmFsdWUgIT09IC0xKSB7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdpbml0RGF0YSAtIGdyaWRPcHRpb25zJywgdGhpcy5ncmlkT3B0aW9ucyk7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdpbml0RGF0YSAtIGRhdGFTb3VyY2UnLCB0aGlzLmRhdGFTb3VyY2UpO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSBrZXlDb2x1bW5WYWx1ZScsIHRoaXMua2V5Q29sdW1uVmFsdWUpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkUm93RGF0YTogYW55ID0gdGhpcy5kYXRhU291cmNlLmZpbmQoeCA9PiB4LklkID09PSB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMuZW5kUG9pbnRVcmxcclxuICAgICAgICAgICYmIHguVmFsdWUuaWQgPT09IHRoaXMua2V5Q29sdW1uVmFsdWUpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdpbml0RGF0YSAtIHZhbHVlQ29sdW1uTmFtZScsIHRoaXMudmFsdWVDb2x1bW5OYW1lKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnaW5pdERhdGEgLSBrZXlDb2x1bW5WYWx1ZScsIHRoaXMua2V5Q29sdW1uVmFsdWUpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdpbml0RGF0YSAtIHNlbGVjdGVkUm93RGF0YScsIHNlbGVjdGVkUm93RGF0YSk7XHJcblxyXG5cclxuICAgICAgICBpZiAoc2VsZWN0ZWRSb3dEYXRhKSB7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sYWJlbCA9IHNlbGVjdGVkUm93RGF0YS5WYWx1ZS5sYWJlbDtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHNlbGVjdGVkUm93RGF0YS5WYWx1ZS52YWx1ZTtcclxuICAgICAgICAgIH0sIDEwKTtcclxuXHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucyAmJiB0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLlVybE9wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLmxpc3RPZlZhbHVlc1NlcnZpY2Uuc2V0TG9hZFVybFNldHRpbmcodGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5VcmxPcHRpb25zKTtcclxuICAgICAgICB0aGlzLmZpZWxkSW5pRGF0YSA9IHRoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGQ7XHJcbiAgICAgICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IGZhbHNlIHx8IHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuQ3VzdG9tRW5kUG9pbnQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgaWYgKHRoaXMuZmllbGRJbmlEYXRhLmluZGV4T2YoJ0lkJykgPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGRJbmlEYXRhID0gdGhpcy5maWVsZEluaURhdGEuc3Vic3RyKDAsIHRoaXMuZmllbGRJbmlEYXRhLmluZGV4T2YoJ0lkJykpICsgJy4nICsgdGhpcy5maWVsZEluaURhdGEuc3Vic3RyKHRoaXMuZmllbGRJbmlEYXRhLmluZGV4T2YoJ0lkJyksIHRoaXMuZmllbGRJbmlEYXRhLmxlbmd0aCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmdlbmVyaWNFeHByZXNzaW9uID0ge1xyXG4gICAgICAgICAgRmlsdGVyR3JvdXBzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBGaWx0ZXJzOiBbXHJcbiAgICAgICAgICAgICAgICB7IFByb3BlcnR5TmFtZTogdGhpcy5maWVsZEluaURhdGEsIFZhbHVlOiB0aGlzLmtleUNvbHVtblZhbHVlLCBDb21wYXJpc29uOiBUZXh0Q29tcGFyaXNvbi5FcXVhbFRvIH1cclxuICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIF0sXHJcbiAgICAgICAgICBQYWdlU2l6ZTogMSxcclxuICAgICAgICAgIFBhZ2VOdW1iZXI6IDFcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIpIHtcclxuICAgICAgICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLnJlcXVlc3RPcHRpb25zLkN1c3RvbUZpbHRlci5GaWx0ZXJHcm91cHMpIHtcclxuICAgICAgICAgICAgdGhpcy5ncmlkT3B0aW9ucy5yZXF1ZXN0T3B0aW9ucy5DdXN0b21GaWx0ZXIuRmlsdGVyR3JvdXBzLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5nZW5lcmljRXhwcmVzc2lvbi5GaWx0ZXJHcm91cHMucHVzaChpdGVtKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmxpc3RPZlZhbHVlc1NlcnZpY2UubG9hZERhdGFCeUZpbHRlckFuZFBhZ2VTZXR0aW5nKHRoaXMuZ2VuZXJpY0V4cHJlc3Npb24sIHRoaXMuZ3JpZE9wdGlvbnMucmVxdWVzdE9wdGlvbnMuSGVhZGVyUGFyYW1ldGVycyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNsZWFyTG92KGRpc2FibGVTZXRUb0tleUNvbHVtblZhbHVlPzogYm9vbGVhbikge1xyXG4gICAgaWYgKCFkaXNhYmxlU2V0VG9LZXlDb2x1bW5WYWx1ZSkge1xyXG4gICAgICB0aGlzLktleUNvbHVtblZhbHVlID0gbnVsbDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMua2V5Q29sdW1uVmFsdWUgPSBudWxsO1xyXG4gICAgfVxyXG4gICAgdGhpcy52YWx1ZSA9IG51bGw7XHJcbiAgICB0aGlzLmxhYmVsID0gbnVsbDtcclxuICAgIHRoaXMuaGFzU2VsZWN0ZWRJdGVtID0gZmFsc2U7XHJcblxyXG4gICAgLy8gaWYgKHRoaXMuZm9ybVRleHRJbnB1dENvbXBvbmVudCkge1xyXG4gICAgLy8gICB0aGlzLmZvcm1UZXh0SW5wdXRDb21wb25lbnQuaW5wdXRWYWx1ZSA9IG51bGw7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgaWYgKCFkaXNhYmxlU2V0VG9LZXlDb2x1bW5WYWx1ZSkge1xyXG4gICAgICB0aGlzLmxvdlZhbHVlQ2hhbmdlZC5lbWl0KG51bGwpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2hvd0NsZWFyQnV0dG9uKCkge1xyXG4gICAgcmV0dXJuICFfLmlzTmlsKHRoaXMuS2V5Q29sdW1uVmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2UodmFsOiBhbnkpIHtcclxuICB9XHJcblxyXG4gIG9uVG91Y2hlZCgpIHtcclxuICB9XHJcblxyXG4gIGdldCBLZXlDb2x1bW5WYWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLmtleUNvbHVtblZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IEtleUNvbHVtblZhbHVlKHZhbCkge1xyXG4gICAgdGhpcy5rZXlDb2x1bW5WYWx1ZSA9IHZhbDtcclxuICAgIHRoaXMuc2V0TG92RmllbGRzKCk7XHJcbiAgICB0aGlzLm9uQ2hhbmdlKHZhbCk7XHJcbiAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPbkNoYW5nZShmbikge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgd3JpdGVWYWx1ZSh2YWx1ZSkge1xyXG4gICAgLy8gY29uc29sZS5sb2coJ3dyaXRlVmFsdWUxJywgdmFsdWUpO1xyXG4gICAgLy8gY29uc29sZS5sb2coJ3dyaXRlVmFsdWUyJywgdGhpcy5rZXlDb2x1bW5WYWx1ZSk7XHJcbiAgICBpZiAodmFsdWUpIHtcclxuICAgICAgaWYgKHZhbHVlICE9PSB0aGlzLmtleUNvbHVtblZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5rZXlDb2x1bW5WYWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgIHRoaXMuaW5pdERhdGEoKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5rZXlDb2x1bW5WYWx1ZSA9IHZhbHVlO1xyXG4gICAgICB0aGlzLmNsZWFyTG92KHRydWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm4pIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICBzZXRMb3ZGaWVsZHMoKSB7XHJcbiAgICBpZiAoIV8uaXNOaWwodGhpcy5rZXlDb2x1bW5WYWx1ZSkgJiYgIV8uaXNOaWwodGhpcy5ncmlkT3B0aW9ucykpIHtcclxuICAgICAgY29uc3Qga2V5Q29sdW1uID0gdGhpcy5ncmlkT3B0aW9ucy5jb2x1bW5zLmZpbmQoKGl0ZW0pID0+IGl0ZW0uZmllbGQgPT09IHRoaXMuZ3JpZE9wdGlvbnMua2V5RmllbGQpO1xyXG5cclxuICAgICAgaWYgKCFfLmlzTmlsKHRoaXMuaW5pdGlhbERhdGEpICYmIGtleUNvbHVtbiAmJiBrZXlDb2x1bW4uZmllbGQpIHtcclxuICAgICAgICBjb25zdCBmaWx0ZXJlZE9iamVjdCA9IHRoaXMuaW5pdGlhbERhdGEuZmluZCgoaXRlbSkgPT4gaXRlbVtrZXlDb2x1bW4uZmllbGRdID09PSB0aGlzLmtleUNvbHVtblZhbHVlKTtcclxuICAgICAgICBpZiAoIV8uaXNOaWwoZmlsdGVyZWRPYmplY3QpKSB7XHJcbiAgICAgICAgICB0aGlzLnZhbHVlID0gZmlsdGVyZWRPYmplY3RbdGhpcy52YWx1ZUNvbHVtbk5hbWVdO1xyXG5cclxuICAgICAgICAgIHRoaXMubGFiZWwgPSAnJztcclxuICAgICAgICAgIHRoaXMuZnJlZVRleHQgPSAnJztcclxuXHJcbiAgICAgICAgICBpZiAodGhpcy5sYWJlbENvbHVtbk5hbWVzIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgICAgICAgY29uc3QgbGFiZWxzOiBzdHJpbmdbXSA9IDxzdHJpbmdbXT50aGlzLmxhYmVsQ29sdW1uTmFtZXM7XHJcbiAgICAgICAgICAgIGxhYmVscy5mb3JFYWNoKChjb2x1bW5OYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmxhYmVsID0gdGhpcy5sYWJlbCArICcgJyArIGZpbHRlcmVkT2JqZWN0W2NvbHVtbk5hbWVdO1xyXG4gICAgICAgICAgICAgIHRoaXMuZnJlZVRleHQgPSB0aGlzLmZyZWVUZXh0ICsgJyAnICsgZmlsdGVyZWRPYmplY3RbY29sdW1uTmFtZV07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmxhYmVsQ29sdW1uTmFtZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5sYWJlbCA9IGZpbHRlcmVkT2JqZWN0W3RoaXMubGFiZWxDb2x1bW5OYW1lcy50b1N0cmluZygpXTtcclxuICAgICAgICAgICAgdGhpcy5mcmVlVGV4dCA9IGZpbHRlcmVkT2JqZWN0W3RoaXMubGFiZWxDb2x1bW5OYW1lcy50b1N0cmluZygpXTtcclxuICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgLy8gaXQgd2lsbCBzZW5kIHRoZSBncmlkIGVkaXQgY2VsbFxyXG4gICAgICAgICAgdGhpcy5hZGRUb0RhdGFTb3VyY2UuZW1pdCh7XHJcbiAgICAgICAgICAgIElkOiB0aGlzLmxpc3RPZlZhbHVlc1NlcnZpY2UuZW5kUG9pbnRVcmwsXHJcbiAgICAgICAgICAgIFZhbHVlOiB7IGlkOiB0aGlzLktleUNvbHVtblZhbHVlLCB2YWx1ZTogdGhpcy52YWx1ZSwgbGFiZWw6IHRoaXMubGFiZWwgfVxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgdGhpcy5oYXNTZWxlY3RlZEl0ZW0gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0SW5wdXRGaWxlZFdpZHRoKCk6IHN0cmluZyB7XHJcbiAgICBpZiAoIXRoaXMuaXNSZWFkT25seSkge1xyXG4gICAgICBpZiAodGhpcy5zaG93Q2xlYXJCdXR0b24oKSkge1xyXG4gICAgICAgIHJldHVybiAnY2FsYyg2MCUgLSA3NnB4KSc7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuICdjYWxjKDYwJSAtIDM2cHgpJztcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuICdjYWxjKDYwJSApJztcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19