/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DataService } from '../../services/index';
import * as i0 from "@angular/core";
import * as i1 from "../../services/http/data.service";
var ListOfValuesService = /** @class */ (function () {
    function ListOfValuesService(dataService) {
        this.dataService = dataService;
        this.lovServiceResultStateChanged = new Subject();
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    ListOfValuesService.prototype.setLoadUrlSetting = /**
     * @param {?} urlOptions
     * @return {?}
     */
    function (urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    };
    /**
     * @param {?} expression
     * @param {?=} headerParameters
     * @return {?}
     */
    ListOfValuesService.prototype.loadDataByFilterAndPageSetting = /**
     * @param {?} expression
     * @param {?=} headerParameters
     * @return {?}
     */
    function (expression, headerParameters) {
        var _this = this;
        /** @type {?} */
        var reqUrl = "" + this.moduleUrl + this.endPointUrl;
        this.dataService.getListBy(reqUrl, expression, headerParameters)
            .subscribe(function (httpResponse) {
            _this.pagingResult = httpResponse.pagingResult;
            _this.lovData = httpResponse.serviceResult.Result;
            _this.lovServiceResultStateChanged.next(_this.lovData);
        });
    };
    ListOfValuesService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ListOfValuesService.ctorParameters = function () { return [
        { type: DataService }
    ]; };
    /** @nocollapse */ ListOfValuesService.ngInjectableDef = i0.defineInjectable({ factory: function ListOfValuesService_Factory() { return new ListOfValuesService(i0.inject(i1.DataService)); }, token: ListOfValuesService, providedIn: "root" });
    return ListOfValuesService;
}());
export { ListOfValuesService };
if (false) {
    /** @type {?} */
    ListOfValuesService.prototype.lovServiceResultStateChanged;
    /** @type {?} */
    ListOfValuesService.prototype.lovData;
    /** @type {?} */
    ListOfValuesService.prototype.pagingResult;
    /** @type {?} */
    ListOfValuesService.prototype.moduleUrl;
    /** @type {?} */
    ListOfValuesService.prototype.endPointUrl;
    /**
     * @type {?}
     * @private
     */
    ListOfValuesService.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2xpc3Qtb2YtdmFsdWVzL2xpc3Qtb2YtdmFsdWVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQVUvQixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7OztBQUVuRDtJQVdFLDZCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVByQyxpQ0FBNEIsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO0lBUTNELENBQUM7Ozs7O0lBRUQsK0NBQWlCOzs7O0lBQWpCLFVBQWtCLFVBQXNCO1FBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7SUFDNUMsQ0FBQzs7Ozs7O0lBRUQsNERBQThCOzs7OztJQUE5QixVQUErQixVQUE2QixFQUFFLGdCQUFvQztRQUFsRyxpQkFRQzs7WUFQTyxNQUFNLEdBQUcsS0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFhO1FBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLENBQUM7YUFDN0QsU0FBUyxDQUFDLFVBQUMsWUFBbUM7WUFDN0MsS0FBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO1lBQzlDLEtBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDakQsS0FBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkEzQkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFKUSxXQUFXOzs7OEJBWHBCO0NBeUNDLEFBNUJELElBNEJDO1NBekJZLG1CQUFtQjs7O0lBQzlCLDJEQUEyRDs7SUFDM0Qsc0NBQXNCOztJQUN0QiwyQ0FBa0M7O0lBRWxDLHdDQUF5Qjs7SUFDekIsMENBQTJCOzs7OztJQUVmLDBDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHtcclxuICBDb3JlSHR0cFJlc3BvbnNlLFxyXG4gIFBhZ2luZ1Jlc3VsdCxcclxuICBHZW5lcmljRXhwcmVzc2lvbixcclxuICBIZWFkZXJQYXJhbWV0ZXIsXHJcbiAgVXJsT3B0aW9uc1xyXG59IGZyb20gJy4uLy4uL21vZGVscy9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIExpc3RPZlZhbHVlc1NlcnZpY2Uge1xyXG4gIHB1YmxpYyBsb3ZTZXJ2aWNlUmVzdWx0U3RhdGVDaGFuZ2VkID0gbmV3IFN1YmplY3Q8YW55W10+KCk7XHJcbiAgcHVibGljIGxvdkRhdGE6IGFueVtdO1xyXG4gIHB1YmxpYyBwYWdpbmdSZXN1bHQ6IFBhZ2luZ1Jlc3VsdDtcclxuXHJcbiAgcHVibGljIG1vZHVsZVVybDogc3RyaW5nO1xyXG4gIHB1YmxpYyBlbmRQb2ludFVybDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRhdGFTZXJ2aWNlOiBEYXRhU2VydmljZSkge1xyXG4gIH1cclxuXHJcbiAgc2V0TG9hZFVybFNldHRpbmcodXJsT3B0aW9uczogVXJsT3B0aW9ucykge1xyXG4gICAgdGhpcy5tb2R1bGVVcmwgPSB1cmxPcHRpb25zLm1vZHVsZVVybDtcclxuICAgIHRoaXMuZW5kUG9pbnRVcmwgPSB1cmxPcHRpb25zLmVuZFBvaW50VXJsO1xyXG4gIH1cclxuXHJcbiAgbG9hZERhdGFCeUZpbHRlckFuZFBhZ2VTZXR0aW5nKGV4cHJlc3Npb246IEdlbmVyaWNFeHByZXNzaW9uLCBoZWFkZXJQYXJhbWV0ZXJzPzogSGVhZGVyUGFyYW1ldGVyW10pIHtcclxuICAgIGNvbnN0IHJlcVVybCA9IGAke3RoaXMubW9kdWxlVXJsfSR7dGhpcy5lbmRQb2ludFVybH1gO1xyXG4gICAgdGhpcy5kYXRhU2VydmljZS5nZXRMaXN0QnkocmVxVXJsLCBleHByZXNzaW9uLCBoZWFkZXJQYXJhbWV0ZXJzKVxyXG4gICAgICAuc3Vic2NyaWJlKChodHRwUmVzcG9uc2U6IENvcmVIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xyXG4gICAgICAgIHRoaXMucGFnaW5nUmVzdWx0ID0gaHR0cFJlc3BvbnNlLnBhZ2luZ1Jlc3VsdDtcclxuICAgICAgICB0aGlzLmxvdkRhdGEgPSBodHRwUmVzcG9uc2Uuc2VydmljZVJlc3VsdC5SZXN1bHQ7XHJcbiAgICAgICAgdGhpcy5sb3ZTZXJ2aWNlUmVzdWx0U3RhdGVDaGFuZ2VkLm5leHQodGhpcy5sb3ZEYXRhKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==