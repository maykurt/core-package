/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { CoreGridComponent } from '../../core-grid/core-grid.component';
import * as _ from 'lodash';
var ListOfValuesModalComponent = /** @class */ (function () {
    function ListOfValuesModalComponent() {
        this.getSelectedRow = new EventEmitter();
    }
    Object.defineProperty(ListOfValuesModalComponent.prototype, "content", {
        set: /**
         * @param {?} component
         * @return {?}
         */
        function (component) {
            // console.error('setContent', component);
            if (component) {
                this.coreGrid = component;
                this.gridOptions = this.displayData.gridOptions;
                component.load();
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} changes
     * @return {?}
     */
    ListOfValuesModalComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.displayData && this.displayData && this.displayData.gridOptions) {
            this.gridOptions = this.displayData.gridOptions;
        }
    };
    /**
     * @return {?}
     */
    ListOfValuesModalComponent.prototype.save = /**
     * @return {?}
     */
    function () {
        this.getSelectedRow.emit(this.coreGrid.getSelectedRow());
        this.displayData = { display: false };
    };
    /**
     * @return {?}
     */
    ListOfValuesModalComponent.prototype.close = /**
     * @return {?}
     */
    function () {
        this.displayData = { display: false };
    };
    /**
     * @param {?} selectedRows
     * @return {?}
     */
    ListOfValuesModalComponent.prototype.selectedChanged = /**
     * @param {?} selectedRows
     * @return {?}
     */
    function (selectedRows) {
        this.getSelectedRow.emit(_.head(selectedRows));
        this.displayData = { display: false };
    };
    /**
     * @return {?}
     */
    ListOfValuesModalComponent.prototype.gridReload = /**
     * @return {?}
     */
    function () {
        this.coreGrid.refreshGridDataSource();
    };
    ListOfValuesModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-list-of-values-modal',
                    template: "<core-modal [displayData]=\"displayData\" [draggable]=\"false\" [showHeader]=\"false\" [closeOnEscape]=\"false\"\r\n  [closable]=\"false\" [width]=\"65\">\r\n  <layout-core-grid *ngIf=\"displayData && displayData.display\" #coreGrid [gridOptions]=\"gridOptions\"\r\n    [isInsideModal]=\"true\" (doubleClickSelectedChanged)=\"selectedChanged($event)\">\r\n  </layout-core-grid>\r\n  <div class=\"col-sm-12 prl-0 mt-10\">\r\n    <button class=\"btn btn-danger ml-10 fl-r\" type=\"button\" (click)=\"close()\">\r\n      <core-icon icon=\"times\"></core-icon> {{'CancelButton'|translate}}\r\n    </button>\r\n\r\n    <button type=\"button\" class=\"btn btn-success ml-10 fl-r\" (click)=\"save()\">\r\n      <core-icon icon=\"check\"></core-icon> {{'ChooseButton' | translate}}\r\n    </button>\r\n  </div>\r\n</core-modal>",
                    styles: [".table-row-selection{cursor:pointer}.modal-pager{padding:20px 0!important;height:16px!important;display:flex;align-items:center;overflow:hidden;font-size:14px;justify-content:space-between;margin-bottom:10px}.modal-pagination{padding:0;margin-top:6px;margin-right:10px;float:left}.modal-static-layout-selector{margin-top:7px;float:left;margin-right:20px}"]
                }] }
    ];
    /** @nocollapse */
    ListOfValuesModalComponent.ctorParameters = function () { return []; };
    ListOfValuesModalComponent.propDecorators = {
        displayData: [{ type: Input }],
        getSelectedRow: [{ type: Output }],
        content: [{ type: ViewChild, args: ['coreGrid',] }]
    };
    return ListOfValuesModalComponent;
}());
export { ListOfValuesModalComponent };
if (false) {
    /** @type {?} */
    ListOfValuesModalComponent.prototype.displayData;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.getSelectedRow;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.coreGrid;
    /** @type {?} */
    ListOfValuesModalComponent.prototype.gridOptions;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvbGlzdC1vZi12YWx1ZXMvbGlzdC1vZi12YWx1ZXMtbW9kYWwvbGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFFNUcsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDeEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUI7SUFzQkU7UUFmVSxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFnQjlDLENBQUM7SUFmRCxzQkFDSSwrQ0FBTzs7Ozs7UUFEWCxVQUNZLFNBQTRCO1lBQ3RDLDBDQUEwQztZQUMxQyxJQUFJLFNBQVMsRUFBRTtnQkFDYixJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztnQkFDaEQsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ2xCO1FBQ0gsQ0FBQzs7O09BQUE7Ozs7O0lBU0QsZ0RBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO1lBQzNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7U0FDakQ7SUFDSCxDQUFDOzs7O0lBRUQseUNBQUk7OztJQUFKO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1FBRXpELElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7OztJQUVELDBDQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxvREFBZTs7OztJQUFmLFVBQWdCLFlBQVk7UUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBRS9DLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7OztJQUVELCtDQUFVOzs7SUFBVjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUN4QyxDQUFDOztnQkFqREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSw2QkFBNkI7b0JBQ3ZDLCt6QkFBb0Q7O2lCQUVyRDs7Ozs7OEJBRUUsS0FBSztpQ0FDTCxNQUFNOzBCQUNOLFNBQVMsU0FBQyxVQUFVOztJQTBDdkIsaUNBQUM7Q0FBQSxBQWxERCxJQWtEQztTQTdDWSwwQkFBMEI7OztJQUNyQyxpREFBMEI7O0lBQzFCLG9EQUE4Qzs7SUFXOUMsOENBQW1DOztJQUVuQyxpREFBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBJbnB1dCwgVmlld0NoaWxkLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgR3JpZE9wdGlvbnMgfSBmcm9tICcuLi8uLi8uLi9tb2RlbHMvZ3JpZC1vcHRpb25zLm1vZGVsJztcclxuaW1wb3J0IHsgQ29yZUdyaWRDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb3JlLWdyaWQvY29yZS1ncmlkLmNvbXBvbmVudCc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbGF5b3V0LWxpc3Qtb2YtdmFsdWVzLW1vZGFsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbGlzdC1vZi12YWx1ZXMtbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2xpc3Qtb2YtdmFsdWVzLW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIExpc3RPZlZhbHVlc01vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICBASW5wdXQoKSBkaXNwbGF5RGF0YTogYW55O1xyXG4gIEBPdXRwdXQoKSBnZXRTZWxlY3RlZFJvdyA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAVmlld0NoaWxkKCdjb3JlR3JpZCcpXHJcbiAgc2V0IGNvbnRlbnQoY29tcG9uZW50OiBDb3JlR3JpZENvbXBvbmVudCkge1xyXG4gICAgLy8gY29uc29sZS5lcnJvcignc2V0Q29udGVudCcsIGNvbXBvbmVudCk7XHJcbiAgICBpZiAoY29tcG9uZW50KSB7XHJcbiAgICAgIHRoaXMuY29yZUdyaWQgPSBjb21wb25lbnQ7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMgPSB0aGlzLmRpc3BsYXlEYXRhLmdyaWRPcHRpb25zO1xyXG4gICAgICBjb21wb25lbnQubG9hZCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNvcmVHcmlkOiBDb3JlR3JpZENvbXBvbmVudDtcclxuXHJcbiAgcHVibGljIGdyaWRPcHRpb25zOiBHcmlkT3B0aW9ucztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBpZiAoY2hhbmdlcy5kaXNwbGF5RGF0YSAmJiB0aGlzLmRpc3BsYXlEYXRhICYmIHRoaXMuZGlzcGxheURhdGEuZ3JpZE9wdGlvbnMpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucyA9IHRoaXMuZGlzcGxheURhdGEuZ3JpZE9wdGlvbnM7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzYXZlKCkge1xyXG4gICAgdGhpcy5nZXRTZWxlY3RlZFJvdy5lbWl0KHRoaXMuY29yZUdyaWQuZ2V0U2VsZWN0ZWRSb3coKSk7XHJcblxyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHsgZGlzcGxheTogZmFsc2UgfTtcclxuICB9XHJcblxyXG4gIGNsb3NlKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHsgZGlzcGxheTogZmFsc2UgfTtcclxuICB9XHJcblxyXG4gIHNlbGVjdGVkQ2hhbmdlZChzZWxlY3RlZFJvd3MpIHtcclxuICAgIHRoaXMuZ2V0U2VsZWN0ZWRSb3cuZW1pdChfLmhlYWQoc2VsZWN0ZWRSb3dzKSk7XHJcblxyXG4gICAgdGhpcy5kaXNwbGF5RGF0YSA9IHsgZGlzcGxheTogZmFsc2UgfTtcclxuICB9XHJcblxyXG4gIGdyaWRSZWxvYWQoKSB7XHJcbiAgICB0aGlzLmNvcmVHcmlkLnJlZnJlc2hHcmlkRGF0YVNvdXJjZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=