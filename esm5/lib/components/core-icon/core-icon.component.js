/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var CoreIconComponent = /** @class */ (function () {
    function CoreIconComponent() {
    }
    CoreIconComponent.decorators = [
        { type: Component, args: [{
                    template: "\n        <fa-icon *ngIf=\"icon\" [icon]=\"icon\"></fa-icon>\n    ",
                    selector: 'core-icon'
                }] }
    ];
    CoreIconComponent.propDecorators = {
        icon: [{ type: Input }]
    };
    return CoreIconComponent;
}());
export { CoreIconComponent };
if (false) {
    /** @type {?} */
    CoreIconComponent.prototype.icon;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvcmUtaWNvbi9jb3JlLWljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRDtJQUFBO0lBUUEsQ0FBQzs7Z0JBUkEsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxvRUFFVDtvQkFDRCxRQUFRLEVBQUUsV0FBVztpQkFDeEI7Ozt1QkFFSSxLQUFLOztJQUNWLHdCQUFDO0NBQUEsQUFSRCxJQVFDO1NBRlksaUJBQWlCOzs7SUFDMUIsaUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxmYS1pY29uICpuZ0lmPVwiaWNvblwiIFtpY29uXT1cImljb25cIj48L2ZhLWljb24+XHJcbiAgICBgLFxyXG4gICAgc2VsZWN0b3I6ICdjb3JlLWljb24nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlSWNvbkNvbXBvbmVudCB7XHJcbiAgICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbn1cclxuIl19