/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, NgZone, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import 'rxjs/add/operator/filter';
import { BreadcrumbService } from '../../services/custom/breadcrumb.service';
/**
 * @record
 */
function IBreadcrumb() { }
if (false) {
    /** @type {?} */
    IBreadcrumb.prototype.label;
    /** @type {?} */
    IBreadcrumb.prototype.params;
    /** @type {?} */
    IBreadcrumb.prototype.url;
}
var BreadcrumbComponent = /** @class */ (function () {
    function BreadcrumbComponent(breadcrumbService, router, activatedRoute, ngZone) {
        var _this = this;
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.ngZone = ngZone;
        // subscribe to the NavigationEnd event 
        this.routingSubscription = this.router.events.filter(function (event) { return event instanceof NavigationEnd; })
            .subscribe(function (event) {
            // set breadcrumbs
            /** @type {?} */
            var root = _this.activatedRoute.root;
            _this.breadcrumbService.breadcrumbs = _this.getBreadcrumbs(root);
        });
    }
    /**
     * @private
     * @param {?} route
     * @param {?=} url
     * @param {?=} breadcrumbs
     * @return {?}
     */
    BreadcrumbComponent.prototype.getBreadcrumbs = /**
     * @private
     * @param {?} route
     * @param {?=} url
     * @param {?=} breadcrumbs
     * @return {?}
     */
    function (route, url, breadcrumbs) {
        if (url === void 0) { url = ''; }
        if (breadcrumbs === void 0) { breadcrumbs = []; }
        var e_1, _a;
        /** @type {?} */
        var ROUTE_DATA_BREADCRUMB = 'breadcrumb';
        // get the child routes
        /** @type {?} */
        var children = route.children;
        // return if there are no more children
        if (children.length === 0) {
            return breadcrumbs;
        }
        var _loop_1 = function (child) {
            // verify primary route
            if (child.outlet !== PRIMARY_OUTLET) {
                return "continue";
            }
            // verify the custom data property 'breadcrumb' is specified on the route
            if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
                return { value: this_1.getBreadcrumbs(child, url, breadcrumbs) };
            }
            // get the route's URL segment
            /** @type {?} */
            var routeURL = child.snapshot.url.map(function (segment) { return segment.path; }).join('/');
            // append route URL to URL
            url += "/" + routeURL;
            /** @type {?} */
            var currentUrl = this_1.router.url;
            // /organization/operations/company
            /** @type {?} */
            var indexUrl = currentUrl.indexOf(url);
            /** @type {?} */
            var parentUrl = null;
            if (indexUrl !== -1) {
                if (indexUrl === 0 && url === '/') {
                    currentUrl = currentUrl.substring(1);
                    /** @type {?} */
                    var secondIndex = currentUrl.indexOf('/');
                    if (secondIndex !== -1) {
                        parentUrl = currentUrl.substring(0, secondIndex);
                    }
                }
                else {
                    parentUrl = currentUrl.substring(0, indexUrl);
                }
            }
            url = url.replace('//', parentUrl ? parentUrl + '/' : '/');
            if (url === '/' && parentUrl) {
                url = '/' + parentUrl;
            }
            // url = url.replace('//', this.mainRout ? '/' + this.mainRout + '/' : '/');
            // if (url === '/' && this.mainRout) {
            //   url = '/' + this.mainRout;
            // }
            // add breadcrumb
            /** @type {?} */
            var breadcrumb = {
                label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
                url: this_1.getFullPath(child.snapshot),
                customLabel: null
            };
            if (breadcrumbs.findIndex(function (x) { return x.label === breadcrumb.label && x.url === breadcrumb.url; }) === -1) {
                breadcrumbs.push(breadcrumb);
            }
            if (child.snapshot.params && child.snapshot.params.id) {
                /** @type {?} */
                var moreBreadcrumb_1 = {
                    label: child.snapshot.params.id,
                    url: this_1.getFullPath(child.snapshot),
                    customLabel: null
                };
                // find from list and update moreBreadcrumb according to that
                /** @type {?} */
                var breadcrumbsWithCustomLabel = this_1.breadcrumbService.breadcrumbsWithCustomLabels.find(function (x) { return x.label === moreBreadcrumb_1.label && x.url === moreBreadcrumb_1.url; });
                if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    moreBreadcrumb_1['customLabel'] = breadcrumbsWithCustomLabel.customLabel;
                }
                /** @type {?} */
                var foundBreadcrumb = breadcrumbs.find(function (x) { return x.label === moreBreadcrumb_1.label && x.url === moreBreadcrumb_1.url; });
                // check exist if not push, if exist then maybe customLabel was changed and update it 
                if (!foundBreadcrumb) {
                    breadcrumbs.push(moreBreadcrumb_1);
                }
                else if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    foundBreadcrumb.customLabel = breadcrumbsWithCustomLabel.customLabel;
                }
            }
            return { value: this_1.getBreadcrumbs(child, url, breadcrumbs) };
        };
        var this_1 = this;
        try {
            // iterate over each children
            for (var children_1 = tslib_1.__values(children), children_1_1 = children_1.next(); !children_1_1.done; children_1_1 = children_1.next()) {
                var child = children_1_1.value;
                var state_1 = _loop_1(child);
                if (typeof state_1 === "object")
                    return state_1.value;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (children_1_1 && !children_1_1.done && (_a = children_1.return)) _a.call(children_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        // we should never get here, but just in case
        return breadcrumbs;
    };
    /**
     * @param {?} route
     * @return {?}
     */
    BreadcrumbComponent.prototype.getFullPath = /**
     * @param {?} route
     * @return {?}
     */
    function (route) {
        /** @type {?} */
        var relativePath = function (segments) { return segments.reduce(function (a, v) { return a += '/' + v.path; }, ''); };
        /** @type {?} */
        var fullPath = function (routes) { return routes.reduce(function (a, v) { return a += relativePath(v.url); }, ''); };
        return fullPath(route.pathFromRoot);
    };
    /**
     * @param {?} url
     * @return {?}
     */
    BreadcrumbComponent.prototype.onClick = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var _this = this;
        this.ngZone.run(function () {
            _this.router.navigate([url]);
        });
    };
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    };
    BreadcrumbComponent.decorators = [
        { type: Component, args: [{
                    template: "\n  <ol *ngIf=\"breadcrumbService.breadcrumbs && breadcrumbService.breadcrumbs.length > 0\" class=\"breadcrumb\" [class.fixed-breadcrumb]=\"isFixed\">\n    <li *ngFor=\"let breadcrumb of breadcrumbService.breadcrumbs\" (click) =\"onClick(breadcrumb.url)\"  title=\"{{breadcrumb.label | translate}}\">\n      <a><span *ngIf=\"!breadcrumb.customLabel\">{{breadcrumb.label | translate}}</span><span *ngIf=\"breadcrumb.customLabel\">{{breadcrumb.customLabel | translate}}</span></a>\n    </li>\n  </ol>\n  ",
                    selector: 'breadcrumb',
                    styles: [".breadcrumb{margin-bottom:0;background-color:transparent;padding:0 0 0 2px;font-size:10px;font-weight:600}.breadcrumb li{cursor:pointer;color:#333!important}.breadcrumb li a{color:#333!important}.breadcrumb li:not(:last-child):after{content:'\\f054';font-family:FontAwesome;font-style:normal;font-weight:400;text-decoration:inherit;margin-left:10px;margin-right:10px;color:#455a64!important;position:relative;top:1px}.breadcrumb li:last-child{font-weight:600}.fixed-breadcrumb{height:35px;width:calc(100%);background-color:#fff;position:fixed;margin-left:-22px;z-index:49;top:45px;box-shadow:0 5px 5px -2px rgba(0,0,0,.2);padding-left:30px;padding-top:10px}"]
                }] }
    ];
    /** @nocollapse */
    BreadcrumbComponent.ctorParameters = function () { return [
        { type: BreadcrumbService },
        { type: Router },
        { type: ActivatedRoute },
        { type: NgZone }
    ]; };
    BreadcrumbComponent.propDecorators = {
        isFixed: [{ type: Input }]
    };
    return BreadcrumbComponent;
}());
export { BreadcrumbComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.routingSubscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFixed;
    /** @type {?} */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9icmVhZGNydW1iL2JyZWFkY3J1bWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLGFBQWEsRUFBVSxjQUFjLEVBQXNDLE1BQU0saUJBQWlCLENBQUM7QUFDcEksT0FBTywwQkFBMEIsQ0FBQztBQUVsQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7OztBQUU3RSwwQkFJQzs7O0lBSEMsNEJBQWM7O0lBQ2QsNkJBQWU7O0lBQ2YsMEJBQVk7O0FBR2Q7SUFnQkUsNkJBQ1MsaUJBQW9DLEVBQ25DLE1BQWMsRUFDZCxjQUE4QixFQUM5QixNQUFjO1FBSnhCLGlCQVlDO1FBWFEsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNuQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDdEIsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLFlBQVksYUFBYSxFQUE5QixDQUE4QixDQUFDO2FBQzFGLFNBQVMsQ0FBQyxVQUFBLEtBQUs7OztnQkFFUixJQUFJLEdBQW1CLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSTtZQUNyRCxLQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7OztJQUlPLDRDQUFjOzs7Ozs7O0lBQXRCLFVBQXVCLEtBQXFCLEVBQUUsR0FBZ0IsRUFBRSxXQUF1QjtRQUF6QyxvQkFBQSxFQUFBLFFBQWdCO1FBQUUsNEJBQUEsRUFBQSxnQkFBdUI7OztZQUMvRSxxQkFBcUIsR0FBRyxZQUFZOzs7WUFHcEMsUUFBUSxHQUFxQixLQUFLLENBQUMsUUFBUTtRQUVqRCx1Q0FBdUM7UUFDdkMsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN6QixPQUFPLFdBQVcsQ0FBQztTQUNwQjtnQ0FHVSxLQUFLO1lBQ2QsdUJBQXVCO1lBQ3ZCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxjQUFjLEVBQUU7O2FBRXBDO1lBRUQseUVBQXlFO1lBQ3pFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQ0FDdkQsT0FBSyxjQUFjLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxXQUFXLENBQUM7YUFDcEQ7OztnQkFHSyxRQUFRLEdBQVcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLElBQUksRUFBWixDQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBRWxGLDBCQUEwQjtZQUMxQixHQUFHLElBQUksTUFBSSxRQUFVLENBQUM7O2dCQUVsQixVQUFVLEdBQUcsT0FBSyxNQUFNLENBQUMsR0FBRzs7O2dCQUcxQixRQUFRLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O2dCQUNwQyxTQUFTLEdBQUcsSUFBSTtZQUVwQixJQUFJLFFBQVEsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDbkIsSUFBSSxRQUFRLEtBQUssQ0FBQyxJQUFJLEdBQUcsS0FBSyxHQUFHLEVBQUU7b0JBQ2pDLFVBQVUsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDOzt3QkFDL0IsV0FBVyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO29CQUMzQyxJQUFJLFdBQVcsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDdEIsU0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO3FCQUNsRDtpQkFDRjtxQkFBTTtvQkFDTCxTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7aUJBQy9DO2FBQ0Y7WUFFRCxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUUzRCxJQUFJLEdBQUcsS0FBSyxHQUFHLElBQUksU0FBUyxFQUFFO2dCQUU1QixHQUFHLEdBQUcsR0FBRyxHQUFHLFNBQVMsQ0FBQzthQUN2Qjs7Ozs7OztnQkFRSyxVQUFVLEdBQVE7Z0JBQ3RCLEtBQUssRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztnQkFDakQsR0FBRyxFQUFFLE9BQUssV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7Z0JBQ3JDLFdBQVcsRUFBRSxJQUFJO2FBQ2xCO1lBSUQsSUFBSSxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssVUFBVSxDQUFDLEdBQUcsRUFBeEQsQ0FBd0QsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUMvRixXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzlCO1lBR0QsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7O29CQUMvQyxnQkFBYyxHQUFRO29CQUMxQixLQUFLLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDL0IsR0FBRyxFQUFFLE9BQUssV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7b0JBQ3JDLFdBQVcsRUFBRSxJQUFJO2lCQUNsQjs7O29CQUdLLDBCQUEwQixHQUFRLE9BQUssaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxnQkFBYyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLGdCQUFjLENBQUMsR0FBRyxFQUFoRSxDQUFnRSxDQUFDO2dCQUV0SyxJQUFJLDBCQUEwQixJQUFJLDBCQUEwQixDQUFDLFdBQVcsRUFBRTtvQkFDeEUsZ0JBQWMsQ0FBQyxhQUFhLENBQUMsR0FBRywwQkFBMEIsQ0FBQyxXQUFXLENBQUM7aUJBQ3hFOztvQkFFSyxlQUFlLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssZ0JBQWMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxnQkFBYyxDQUFDLEdBQUcsRUFBaEUsQ0FBZ0UsQ0FBQztnQkFDL0csc0ZBQXNGO2dCQUN0RixJQUFJLENBQUMsZUFBZSxFQUFFO29CQUNwQixXQUFXLENBQUMsSUFBSSxDQUFDLGdCQUFjLENBQUMsQ0FBQztpQkFDbEM7cUJBQU0sSUFBSSwwQkFBMEIsSUFBSSwwQkFBMEIsQ0FBQyxXQUFXLEVBQUU7b0JBQy9FLGVBQWUsQ0FBQyxXQUFXLEdBQUcsMEJBQTBCLENBQUMsV0FBVyxDQUFDO2lCQUN0RTthQUNGOzRCQUdNLE9BQUssY0FBYyxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsV0FBVyxDQUFDOzs7O1lBdEZyRCw2QkFBNkI7WUFDN0IsS0FBb0IsSUFBQSxhQUFBLGlCQUFBLFFBQVEsQ0FBQSxrQ0FBQTtnQkFBdkIsSUFBTSxLQUFLLHFCQUFBO3NDQUFMLEtBQUs7OzthQXNGZjs7Ozs7Ozs7O1FBRUQsNkNBQTZDO1FBQzdDLE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRU0seUNBQVc7Ozs7SUFBbEIsVUFBbUIsS0FBNkI7O1lBQ3hDLFlBQVksR0FBRyxVQUFDLFFBQXNCLElBQUssT0FBQSxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBakIsQ0FBaUIsRUFBRSxFQUFFLENBQUMsRUFBaEQsQ0FBZ0Q7O1lBQzNGLFFBQVEsR0FBRyxVQUFDLE1BQWdDLElBQUssT0FBQSxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUF4QixDQUF3QixFQUFFLEVBQUUsQ0FBQyxFQUFyRCxDQUFxRDtRQUU1RyxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFFRCxxQ0FBTzs7OztJQUFQLFVBQVEsR0FBVztRQUFuQixpQkFJQztRQUhDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ2QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN4QztJQUNILENBQUM7O2dCQXpKRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHdmQU1UO29CQUNELFFBQVEsRUFBRSxZQUFZOztpQkFFdkI7Ozs7Z0JBbEJRLGlCQUFpQjtnQkFIakIsTUFBTTtnQkFBRSxjQUFjO2dCQURBLE1BQU07OzswQkF5QmxDLEtBQUs7O0lBOElSLDBCQUFDO0NBQUEsQUEzSkQsSUEySkM7U0FoSlksbUJBQW1COzs7Ozs7SUFDOUIsa0RBQTBDOztJQUMxQyxzQ0FBMkI7O0lBSXpCLGdEQUEyQzs7Ozs7SUFDM0MscUNBQXNCOzs7OztJQUN0Qiw2Q0FBc0M7Ozs7O0lBQ3RDLHFDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBOZ1pvbmUsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIE5hdmlnYXRpb25FbmQsIFBhcmFtcywgUFJJTUFSWV9PVVRMRVQsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFVybFNlZ21lbnQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2ZpbHRlcic7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2N1c3RvbS9icmVhZGNydW1iLnNlcnZpY2UnO1xyXG5cclxuaW50ZXJmYWNlIElCcmVhZGNydW1iIHtcclxuICBsYWJlbDogc3RyaW5nO1xyXG4gIHBhcmFtczogUGFyYW1zO1xyXG4gIHVybDogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICB0ZW1wbGF0ZTogYFxyXG4gIDxvbCAqbmdJZj1cImJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzICYmIGJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzLmxlbmd0aCA+IDBcIiBjbGFzcz1cImJyZWFkY3J1bWJcIiBbY2xhc3MuZml4ZWQtYnJlYWRjcnVtYl09XCJpc0ZpeGVkXCI+XHJcbiAgICA8bGkgKm5nRm9yPVwibGV0IGJyZWFkY3J1bWIgb2YgYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnNcIiAoY2xpY2spID1cIm9uQ2xpY2soYnJlYWRjcnVtYi51cmwpXCIgIHRpdGxlPVwie3ticmVhZGNydW1iLmxhYmVsIHwgdHJhbnNsYXRlfX1cIj5cclxuICAgICAgPGE+PHNwYW4gKm5nSWY9XCIhYnJlYWRjcnVtYi5jdXN0b21MYWJlbFwiPnt7YnJlYWRjcnVtYi5sYWJlbCB8IHRyYW5zbGF0ZX19PC9zcGFuPjxzcGFuICpuZ0lmPVwiYnJlYWRjcnVtYi5jdXN0b21MYWJlbFwiPnt7YnJlYWRjcnVtYi5jdXN0b21MYWJlbCB8IHRyYW5zbGF0ZX19PC9zcGFuPjwvYT5cclxuICAgIDwvbGk+XHJcbiAgPC9vbD5cclxuICBgLFxyXG4gIHNlbGVjdG9yOiAnYnJlYWRjcnVtYicsXHJcbiAgc3R5bGVVcmxzOiBbJy4vYnJlYWRjcnVtYi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iQ29tcG9uZW50IGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuICBwcml2YXRlIHJvdXRpbmdTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBASW5wdXQoKSBpc0ZpeGVkPzogYm9vbGVhbjtcclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGJyZWFkY3J1bWJTZXJ2aWNlOiBCcmVhZGNydW1iU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgbmdab25lOiBOZ1pvbmUpIHtcclxuICAgIC8vIHN1YnNjcmliZSB0byB0aGUgTmF2aWdhdGlvbkVuZCBldmVudCBcclxuICAgIHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbiA9IHRoaXMucm91dGVyLmV2ZW50cy5maWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKVxyXG4gICAgICAuc3Vic2NyaWJlKGV2ZW50ID0+IHtcclxuICAgICAgICAvLyBzZXQgYnJlYWRjcnVtYnNcclxuICAgICAgICBjb25zdCByb290OiBBY3RpdmF0ZWRSb3V0ZSA9IHRoaXMuYWN0aXZhdGVkUm91dGUucm9vdDtcclxuICAgICAgICB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzID0gdGhpcy5nZXRCcmVhZGNydW1icyhyb290KTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIHByaXZhdGUgZ2V0QnJlYWRjcnVtYnMocm91dGU6IEFjdGl2YXRlZFJvdXRlLCB1cmw6IHN0cmluZyA9ICcnLCBicmVhZGNydW1iczogYW55W10gPSBbXSk6IGFueVtdIHtcclxuICAgIGNvbnN0IFJPVVRFX0RBVEFfQlJFQURDUlVNQiA9ICdicmVhZGNydW1iJztcclxuXHJcbiAgICAvLyBnZXQgdGhlIGNoaWxkIHJvdXRlc1xyXG4gICAgY29uc3QgY2hpbGRyZW46IEFjdGl2YXRlZFJvdXRlW10gPSByb3V0ZS5jaGlsZHJlbjtcclxuXHJcbiAgICAvLyByZXR1cm4gaWYgdGhlcmUgYXJlIG5vIG1vcmUgY2hpbGRyZW5cclxuICAgIGlmIChjaGlsZHJlbi5sZW5ndGggPT09IDApIHtcclxuICAgICAgcmV0dXJuIGJyZWFkY3J1bWJzO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGl0ZXJhdGUgb3ZlciBlYWNoIGNoaWxkcmVuXHJcbiAgICBmb3IgKGNvbnN0IGNoaWxkIG9mIGNoaWxkcmVuKSB7XHJcbiAgICAgIC8vIHZlcmlmeSBwcmltYXJ5IHJvdXRlXHJcbiAgICAgIGlmIChjaGlsZC5vdXRsZXQgIT09IFBSSU1BUllfT1VUTEVUKSB7XHJcbiAgICAgICAgY29udGludWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIHZlcmlmeSB0aGUgY3VzdG9tIGRhdGEgcHJvcGVydHkgJ2JyZWFkY3J1bWInIGlzIHNwZWNpZmllZCBvbiB0aGUgcm91dGVcclxuICAgICAgaWYgKCFjaGlsZC5zbmFwc2hvdC5kYXRhLmhhc093blByb3BlcnR5KFJPVVRFX0RBVEFfQlJFQURDUlVNQikpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRCcmVhZGNydW1icyhjaGlsZCwgdXJsLCBicmVhZGNydW1icyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIGdldCB0aGUgcm91dGUncyBVUkwgc2VnbWVudFxyXG4gICAgICBjb25zdCByb3V0ZVVSTDogc3RyaW5nID0gY2hpbGQuc25hcHNob3QudXJsLm1hcChzZWdtZW50ID0+IHNlZ21lbnQucGF0aCkuam9pbignLycpO1xyXG5cclxuICAgICAgLy8gYXBwZW5kIHJvdXRlIFVSTCB0byBVUkxcclxuICAgICAgdXJsICs9IGAvJHtyb3V0ZVVSTH1gO1xyXG5cclxuICAgICAgbGV0IGN1cnJlbnRVcmwgPSB0aGlzLnJvdXRlci51cmw7IC8vIC9vcmdhbml6YXRpb24vb3BlcmF0aW9ucy9jb21wYW55XHJcblxyXG5cclxuICAgICAgY29uc3QgaW5kZXhVcmwgPSBjdXJyZW50VXJsLmluZGV4T2YodXJsKTtcclxuICAgICAgbGV0IHBhcmVudFVybCA9IG51bGw7XHJcblxyXG4gICAgICBpZiAoaW5kZXhVcmwgIT09IC0xKSB7XHJcbiAgICAgICAgaWYgKGluZGV4VXJsID09PSAwICYmIHVybCA9PT0gJy8nKSB7XHJcbiAgICAgICAgICBjdXJyZW50VXJsID0gY3VycmVudFVybC5zdWJzdHJpbmcoMSk7XHJcbiAgICAgICAgICBjb25zdCBzZWNvbmRJbmRleCA9IGN1cnJlbnRVcmwuaW5kZXhPZignLycpO1xyXG4gICAgICAgICAgaWYgKHNlY29uZEluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICBwYXJlbnRVcmwgPSBjdXJyZW50VXJsLnN1YnN0cmluZygwLCBzZWNvbmRJbmRleCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHBhcmVudFVybCA9IGN1cnJlbnRVcmwuc3Vic3RyaW5nKDAsIGluZGV4VXJsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHVybCA9IHVybC5yZXBsYWNlKCcvLycsIHBhcmVudFVybCA/IHBhcmVudFVybCArICcvJyA6ICcvJyk7XHJcblxyXG4gICAgICBpZiAodXJsID09PSAnLycgJiYgcGFyZW50VXJsKSB7XHJcblxyXG4gICAgICAgIHVybCA9ICcvJyArIHBhcmVudFVybDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gdXJsID0gdXJsLnJlcGxhY2UoJy8vJywgdGhpcy5tYWluUm91dCA/ICcvJyArIHRoaXMubWFpblJvdXQgKyAnLycgOiAnLycpO1xyXG4gICAgICAvLyBpZiAodXJsID09PSAnLycgJiYgdGhpcy5tYWluUm91dCkge1xyXG4gICAgICAvLyAgIHVybCA9ICcvJyArIHRoaXMubWFpblJvdXQ7XHJcbiAgICAgIC8vIH1cclxuXHJcbiAgICAgIC8vIGFkZCBicmVhZGNydW1iXHJcbiAgICAgIGNvbnN0IGJyZWFkY3J1bWI6IGFueSA9IHtcclxuICAgICAgICBsYWJlbDogY2hpbGQuc25hcHNob3QuZGF0YVtST1VURV9EQVRBX0JSRUFEQ1JVTUJdLFxyXG4gICAgICAgIHVybDogdGhpcy5nZXRGdWxsUGF0aChjaGlsZC5zbmFwc2hvdCksXHJcbiAgICAgICAgY3VzdG9tTGFiZWw6IG51bGxcclxuICAgICAgfTtcclxuXHJcblxyXG5cclxuICAgICAgaWYgKGJyZWFkY3J1bWJzLmZpbmRJbmRleCh4ID0+IHgubGFiZWwgPT09IGJyZWFkY3J1bWIubGFiZWwgJiYgeC51cmwgPT09IGJyZWFkY3J1bWIudXJsKSA9PT0gLTEpIHtcclxuICAgICAgICBicmVhZGNydW1icy5wdXNoKGJyZWFkY3J1bWIpO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgaWYgKGNoaWxkLnNuYXBzaG90LnBhcmFtcyAmJiBjaGlsZC5zbmFwc2hvdC5wYXJhbXMuaWQpIHtcclxuICAgICAgICBjb25zdCBtb3JlQnJlYWRjcnVtYjogYW55ID0ge1xyXG4gICAgICAgICAgbGFiZWw6IGNoaWxkLnNuYXBzaG90LnBhcmFtcy5pZCxcclxuICAgICAgICAgIHVybDogdGhpcy5nZXRGdWxsUGF0aChjaGlsZC5zbmFwc2hvdCksXHJcbiAgICAgICAgICBjdXN0b21MYWJlbDogbnVsbFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIGZpbmQgZnJvbSBsaXN0IGFuZCB1cGRhdGUgbW9yZUJyZWFkY3J1bWIgYWNjb3JkaW5nIHRvIHRoYXRcclxuICAgICAgICBjb25zdCBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbDogYW55ID0gdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbHMuZmluZCh4ID0+IHgubGFiZWwgPT09IG1vcmVCcmVhZGNydW1iLmxhYmVsICYmIHgudXJsID09PSBtb3JlQnJlYWRjcnVtYi51cmwpO1xyXG5cclxuICAgICAgICBpZiAoYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWwgJiYgYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWwuY3VzdG9tTGFiZWwpIHtcclxuICAgICAgICAgIG1vcmVCcmVhZGNydW1iWydjdXN0b21MYWJlbCddID0gYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWwuY3VzdG9tTGFiZWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBmb3VuZEJyZWFkY3J1bWIgPSBicmVhZGNydW1icy5maW5kKHggPT4geC5sYWJlbCA9PT0gbW9yZUJyZWFkY3J1bWIubGFiZWwgJiYgeC51cmwgPT09IG1vcmVCcmVhZGNydW1iLnVybCk7XHJcbiAgICAgICAgLy8gY2hlY2sgZXhpc3QgaWYgbm90IHB1c2gsIGlmIGV4aXN0IHRoZW4gbWF5YmUgY3VzdG9tTGFiZWwgd2FzIGNoYW5nZWQgYW5kIHVwZGF0ZSBpdCBcclxuICAgICAgICBpZiAoIWZvdW5kQnJlYWRjcnVtYikge1xyXG4gICAgICAgICAgYnJlYWRjcnVtYnMucHVzaChtb3JlQnJlYWRjcnVtYik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbCAmJiBicmVhZGNydW1ic1dpdGhDdXN0b21MYWJlbC5jdXN0b21MYWJlbCkge1xyXG4gICAgICAgICAgZm91bmRCcmVhZGNydW1iLmN1c3RvbUxhYmVsID0gYnJlYWRjcnVtYnNXaXRoQ3VzdG9tTGFiZWwuY3VzdG9tTGFiZWw7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyByZWN1cnNpdmVcclxuICAgICAgcmV0dXJuIHRoaXMuZ2V0QnJlYWRjcnVtYnMoY2hpbGQsIHVybCwgYnJlYWRjcnVtYnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHdlIHNob3VsZCBuZXZlciBnZXQgaGVyZSwgYnV0IGp1c3QgaW4gY2FzZVxyXG4gICAgcmV0dXJuIGJyZWFkY3J1bWJzO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEZ1bGxQYXRoKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KTogc3RyaW5nIHtcclxuICAgIGNvbnN0IHJlbGF0aXZlUGF0aCA9IChzZWdtZW50czogVXJsU2VnbWVudFtdKSA9PiBzZWdtZW50cy5yZWR1Y2UoKGEsIHYpID0+IGEgKz0gJy8nICsgdi5wYXRoLCAnJyk7XHJcbiAgICBjb25zdCBmdWxsUGF0aCA9IChyb3V0ZXM6IEFjdGl2YXRlZFJvdXRlU25hcHNob3RbXSkgPT4gcm91dGVzLnJlZHVjZSgoYSwgdikgPT4gYSArPSByZWxhdGl2ZVBhdGgodi51cmwpLCAnJyk7XHJcblxyXG4gICAgcmV0dXJuIGZ1bGxQYXRoKHJvdXRlLnBhdGhGcm9tUm9vdCk7XHJcbiAgfVxyXG5cclxuICBvbkNsaWNrKHVybDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdXJsXSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnJvdXRpbmdTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==