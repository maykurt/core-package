/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
BaseComponent = /** @class */ (function () {
    function BaseComponent(preventUnsavedChangesService) {
        var _this = this;
        this.preventUnsavedChangesService = preventUnsavedChangesService;
        this.subscriptions = [];
        this.isSaveOperationEnabled = true;
        this.safeSubscription(this.preventUnsavedChangesService.getStatus()
            .subscribe(function (status) {
            if (status === 'waiting') {
                _this.preventUnsavedChangesService.setIsChanged(_this.skipUnsavedChanges ? false : _this.hasUnsavedChanges());
                _this.preventUnsavedChangesService.setStatus('finished');
            }
        }));
    }
    /**
     * @protected
     * @param {?} sub
     * @return {?}
     */
    BaseComponent.prototype.safeSubscription = /**
     * @protected
     * @param {?} sub
     * @return {?}
     */
    function (sub) {
        this.subscriptions.push(sub);
        return sub;
    };
    /**
     * @private
     * @return {?}
     */
    BaseComponent.prototype.unsubscribeAll = /**
     * @private
     * @return {?}
     */
    function () {
        this.subscriptions.forEach(function (subs) {
            if (subs) {
                subs.unsubscribe();
            }
        });
    };
    /**
     * @protected
     * @return {?}
     */
    BaseComponent.prototype.baseNgOnDestroy = /**
     * @protected
     * @return {?}
     */
    function () {
        this.unsubscribeAll();
    };
    return BaseComponent;
}());
/**
 * @abstract
 */
export { BaseComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BaseComponent.prototype.subscriptions;
    /** @type {?} */
    BaseComponent.prototype.skipUnsavedChanges;
    /** @type {?} */
    BaseComponent.prototype.isSaveOperationEnabled;
    /**
     * @type {?}
     * @protected
     */
    BaseComponent.prototype.preventUnsavedChangesService;
    /**
     * @abstract
     * @return {?}
     */
    BaseComponent.prototype.hasUnsavedChanges = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9iYXNlLWNvbXBvbmVudC9iYXNlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUE7Ozs7SUFLRSx1QkFBc0IsNEJBQTBEO1FBQWhGLGlCQVFDO1FBUnFCLGlDQUE0QixHQUE1Qiw0QkFBNEIsQ0FBOEI7UUFKeEUsa0JBQWEsR0FBbUIsRUFBRSxDQUFDO1FBRXBDLDJCQUFzQixHQUFHLElBQUksQ0FBQztRQUduQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFNBQVMsRUFBRTthQUNoRSxTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3hCLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtnQkFDeEIsS0FBSSxDQUFDLDRCQUE0QixDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztnQkFDM0csS0FBSSxDQUFDLDRCQUE0QixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN6RDtRQUNILENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7Ozs7SUFJUyx3Q0FBZ0I7Ozs7O0lBQTFCLFVBQTJCLEdBQWlCO1FBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQzs7Ozs7SUFHTyxzQ0FBYzs7OztJQUF0QjtRQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBa0I7WUFDNUMsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVTLHVDQUFlOzs7O0lBQXpCO1FBQ0UsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFDSCxvQkFBQztBQUFELENBQUMsQUFsQ0QsSUFrQ0M7Ozs7Ozs7Ozs7SUFqQ0Msc0NBQTJDOztJQUMzQywyQ0FBbUM7O0lBQ25DLCtDQUFxQzs7Ozs7SUFFekIscURBQW9FOzs7OztJQVVoRiw0REFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgUHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2N1c3RvbS9wcmV2ZW50LXVuc2F2ZWQtY2hhbmdlcy5zZXJ2aWNlJztcclxuXHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZUNvbXBvbmVudCB7XHJcbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xyXG4gIHB1YmxpYyBza2lwVW5zYXZlZENoYW5nZXM6IGJvb2xlYW47XHJcbiAgcHVibGljIGlzU2F2ZU9wZXJhdGlvbkVuYWJsZWQgPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgcHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZTogUHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZSkge1xyXG4gICAgdGhpcy5zYWZlU3Vic2NyaXB0aW9uKHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5nZXRTdGF0dXMoKVxyXG4gICAgICAuc3Vic2NyaWJlKChzdGF0dXM6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGlmIChzdGF0dXMgPT09ICd3YWl0aW5nJykge1xyXG4gICAgICAgICAgdGhpcy5wcmV2ZW50VW5zYXZlZENoYW5nZXNTZXJ2aWNlLnNldElzQ2hhbmdlZCh0aGlzLnNraXBVbnNhdmVkQ2hhbmdlcyA/IGZhbHNlIDogdGhpcy5oYXNVbnNhdmVkQ2hhbmdlcygpKTtcclxuICAgICAgICAgIHRoaXMucHJldmVudFVuc2F2ZWRDaGFuZ2VzU2VydmljZS5zZXRTdGF0dXMoJ2ZpbmlzaGVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KSk7XHJcbiAgfVxyXG5cclxuICBhYnN0cmFjdCBoYXNVbnNhdmVkQ2hhbmdlcygpOiBib29sZWFuO1xyXG5cclxuICBwcm90ZWN0ZWQgc2FmZVN1YnNjcmlwdGlvbihzdWI6IFN1YnNjcmlwdGlvbik6IFN1YnNjcmlwdGlvbiB7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChzdWIpO1xyXG4gICAgcmV0dXJuIHN1YjtcclxuICB9XHJcblxyXG5cclxuICBwcml2YXRlIHVuc3Vic2NyaWJlQWxsKCkge1xyXG4gICAgdGhpcy5zdWJzY3JpcHRpb25zLmZvckVhY2goKHN1YnM6IFN1YnNjcmlwdGlvbikgPT4ge1xyXG4gICAgICBpZiAoc3Vicykge1xyXG4gICAgICAgIHN1YnMudW5zdWJzY3JpYmUoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgYmFzZU5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy51bnN1YnNjcmliZUFsbCgpO1xyXG4gIH1cclxufVxyXG4iXX0=