/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SystemReferenceDateService, StorageService } from '../../services/index';
import { StorageType, Storage } from '../../enums/index';
var SystemReferenceDateComponent = /** @class */ (function () {
    function SystemReferenceDateComponent(systemReferenceDateService, storageService, fb, translate) {
        var _this = this;
        this.systemReferenceDateService = systemReferenceDateService;
        this.storageService = storageService;
        this.fb = fb;
        this.translate = translate;
        /** @type {?} */
        var currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        this._calendarDateValue = currentsystemRefDate ?
            this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
            this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange
            .subscribe(function (event) {
            if (event) {
                _this.locale = event.lang === 'tr' ? _this.localeTr : _this.localEn;
            }
        });
        this.systemReferenceDateService.getSelectedReferenceDate()
            .subscribe(function (selectedRefDate) {
            if (selectedRefDate !== '') {
                _this._calendarDateValue = _this.systemReferenceDateService.convertDateToDateUTC(selectedRefDate);
            }
        });
    }
    Object.defineProperty(SystemReferenceDateComponent.prototype, "calendarDateValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this._calendarDateValue;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._calendarDateValue = value;
            // this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} selectedDateISO
     * @return {?}
     */
    SystemReferenceDateComponent.prototype.setSelectedDate = /**
     * @param {?} selectedDateISO
     * @return {?}
     */
    function (selectedDateISO) {
        this.systemReferenceDateService.setSelectedReferenceDate(selectedDateISO);
    };
    /**
     * @return {?}
     */
    SystemReferenceDateComponent.prototype.onClose = /**
     * @return {?}
     */
    function () {
        this.checkAndApplyDate();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    SystemReferenceDateComponent.prototype.keyDownFunction = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.keyCode === 13) {
            this.checkAndApplyDate();
            if (this._calendarDateValue && this.systemRefDatePicker && this.systemRefDatePicker.overlayVisible) {
                this.systemRefDatePicker.overlayVisible = false;
            }
        }
    };
    /**
     * @return {?}
     */
    SystemReferenceDateComponent.prototype.checkAndApplyDate = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (this._calendarDateValue) {
            /** @type {?} */
            var selectedDate = this.systemReferenceDateService.convertDateToISO(this._calendarDateValue);
            if (currentsystemRefDate !== selectedDate) {
                this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
            }
        }
        else {
            this._calendarDateValue = currentsystemRefDate ?
                this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
                this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        }
    };
    /**
     * @return {?}
     */
    SystemReferenceDateComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
        if (this.sysRefDateSubscription) {
            this.sysRefDateSubscription.unsubscribe();
        }
    };
    SystemReferenceDateComponent.decorators = [
        { type: Component, args: [{
                    selector: 'layout-system-reference-date',
                    template: " <div class=\"system-reference-container\">\r\n   <p-calendar #systemRefDatePicker class=\"system-reference-datepicker\" [(ngModel)]=\"calendarDateValue\"\r\n     [dateFormat]=\"'dd.mm.yy'\" [showIcon]=\"false\" [showButtonBar]=\"true\" [style]=\"{'width':'100%', 'height': '30px'}\"\r\n     monthNavigator=\"true\" yearNavigator=\"true\" yearRange=\"1930:2030\" [locale]=\"locale\" (onClose)=\"onClose()\"\r\n     (keydown)=\"keyDownFunction($event)\" appendTo=\"body\">\r\n   </p-calendar>\r\n </div>\r\n",
                    styles: [":host /deep/ .ui-calendar .ui-inputtext{height:30px!important;font-size:inherit;width:calc(100%);color:#fff;border:none;cursor:pointer;background:#e30a3a!important;border-color:rgba(255,255,255,.2)!important}:host /deep/ .ui-datepicker table td{padding:.2em!important;font-size:12px}:host /deep/ .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}:host /deep/ .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}"]
                }] }
    ];
    /** @nocollapse */
    SystemReferenceDateComponent.ctorParameters = function () { return [
        { type: SystemReferenceDateService },
        { type: StorageService },
        { type: FormBuilder },
        { type: TranslateService }
    ]; };
    SystemReferenceDateComponent.propDecorators = {
        systemRefDatePicker: [{ type: ViewChild, args: ['systemRefDatePicker',] }]
    };
    return SystemReferenceDateComponent;
}());
export { SystemReferenceDateComponent };
if (false) {
    /** @type {?} */
    SystemReferenceDateComponent.prototype.systemRefDatePicker;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.sysRefDateFormGroup;
    /** @type {?} */
    SystemReferenceDateComponent.prototype._calendarDateValue;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.locale;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.localeTr;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.localEn;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.translateSubscription;
    /** @type {?} */
    SystemReferenceDateComponent.prototype.sysRefDateSubscription;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.systemReferenceDateService;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.storageService;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    SystemReferenceDateComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXJlZmVyZW5jZS1kYXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3N5c3RlbS1kYXRlLXBpY2tlci9zeXN0ZW0tcmVmZXJlbmNlLWRhdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFhLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRSxPQUFPLEVBQWEsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdkQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLGNBQWMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFHekQ7SUFrQkUsc0NBQ1UsMEJBQXNELEVBQ3RELGNBQThCLEVBQzlCLEVBQWUsRUFDZixTQUEyQjtRQUpyQyxpQkFnREM7UUEvQ1MsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLGNBQVMsR0FBVCxTQUFTLENBQWtCOztZQUM3QixvQkFBb0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDN0csSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsMEJBQTBCLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNuSCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBRWhHLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHO1lBQ2IsY0FBYyxFQUFFLENBQUM7WUFDakIsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDO1lBQ3hGLGFBQWEsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQztZQUNoRSxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7WUFDdkQsVUFBVSxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUM7WUFDdEksZUFBZSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDckcsS0FBSyxFQUFFLE9BQU87WUFDZCxLQUFLLEVBQUUsT0FBTztTQUNmLENBQUM7UUFDRiwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRztZQUNkLGNBQWMsRUFBRSxDQUFDO1lBQ2pCLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQztZQUNyRixhQUFhLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDaEUsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQ3ZELFVBQVUsRUFBRSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDO1lBQzNILGVBQWUsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDO1lBQ3JHLEtBQUssRUFBRSxPQUFPO1lBQ2QsS0FBSyxFQUFFLFNBQVM7U0FDakIsQ0FBQztRQUNGLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUNyRCxTQUFTLENBQUMsVUFBQyxLQUFVO1lBQ3BCLElBQUksS0FBSyxFQUFFO2dCQUNULEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUM7YUFDbEU7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLElBQUksQ0FBQywwQkFBMEIsQ0FBQyx3QkFBd0IsRUFBRTthQUN2RCxTQUFTLENBQUMsVUFBQyxlQUF1QjtZQUNqQyxJQUFJLGVBQWUsS0FBSyxFQUFFLEVBQUU7Z0JBQzFCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsMEJBQTBCLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDakc7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBSSwyREFBaUI7Ozs7UUFBckI7WUFDRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUNqQyxDQUFDOzs7OztRQUVELFVBQXNCLEtBQVc7WUFDL0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNoQyxtR0FBbUc7UUFDckcsQ0FBQzs7O09BTEE7Ozs7O0lBT0Qsc0RBQWU7Ozs7SUFBZixVQUFnQixlQUF1QjtRQUNyQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsd0JBQXdCLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDNUUsQ0FBQzs7OztJQUVELDhDQUFPOzs7SUFBUDtRQUNFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBR0Qsc0RBQWU7Ozs7SUFBZixVQUFnQixLQUFVO1FBQ3hCLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxFQUFFLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDekIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUU7Z0JBQ2xHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2FBQ2pEO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQsd0RBQWlCOzs7SUFBakI7O1lBQ1Esb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsY0FBYyxDQUFDO1FBQzdHLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFOztnQkFDckIsWUFBWSxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDOUYsSUFBSSxvQkFBb0IsS0FBSyxZQUFZLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7YUFDakc7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7Z0JBQzVFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3BIO0lBQ0gsQ0FBQzs7OztJQUVELGtEQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMxQztRQUNELElBQUksSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQy9CLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMzQztJQUNILENBQUM7O2dCQXBIRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLDhCQUE4QjtvQkFDeEMsc2dCQUFxRDs7aUJBRXREOzs7O2dCQVJRLDBCQUEwQjtnQkFBRSxjQUFjO2dCQUovQixXQUFXO2dCQUV0QixnQkFBZ0I7OztzQ0FZdEIsU0FBUyxTQUFDLHFCQUFxQjs7SUErR2xDLG1DQUFDO0NBQUEsQUFySEQsSUFxSEM7U0FoSFksNEJBQTRCOzs7SUFDdkMsMkRBQXNEOztJQUV0RCwyREFBK0I7O0lBQy9CLDBEQUF5Qjs7SUFFekIsOENBQVk7O0lBQ1osZ0RBQWM7O0lBQ2QsK0NBQWE7O0lBQ2IsNkRBQW9DOztJQUNwQyw4REFBcUM7Ozs7O0lBSW5DLGtFQUE4RDs7Ozs7SUFDOUQsc0RBQXNDOzs7OztJQUN0QywwQ0FBdUI7Ozs7O0lBQ3ZCLGlEQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmltcG9ydCB7IFN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLCBTdG9yYWdlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2luZGV4JztcclxuaW1wb3J0IHsgU3RvcmFnZVR5cGUsIFN0b3JhZ2UgfSBmcm9tICcuLi8uLi9lbnVtcy9pbmRleCc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdsYXlvdXQtc3lzdGVtLXJlZmVyZW5jZS1kYXRlJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vc3lzdGVtLXJlZmVyZW5jZS1kYXRlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9zeXN0ZW0tcmVmZXJlbmNlLWRhdGUuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3lzdGVtUmVmZXJlbmNlRGF0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XHJcbiAgQFZpZXdDaGlsZCgnc3lzdGVtUmVmRGF0ZVBpY2tlcicpIHN5c3RlbVJlZkRhdGVQaWNrZXI7XHJcblxyXG4gIHN5c1JlZkRhdGVGb3JtR3JvdXA6IEZvcm1Hcm91cDtcclxuICBfY2FsZW5kYXJEYXRlVmFsdWU6IERhdGU7XHJcblxyXG4gIGxvY2FsZTogYW55O1xyXG4gIGxvY2FsZVRyOiBhbnk7XHJcbiAgbG9jYWxFbjogYW55O1xyXG4gIHRyYW5zbGF0ZVN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIHN5c1JlZkRhdGVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBzeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZTogU3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHtcclxuICAgIGNvbnN0IGN1cnJlbnRzeXN0ZW1SZWZEYXRlID0gdGhpcy5zdG9yYWdlU2VydmljZS5nZXRTdG9yZWQoU3RvcmFnZS5TeXN0ZW1SZWZEYXRlLCBTdG9yYWdlVHlwZS5TZXNzaW9uU3RvcmFnZSk7XHJcbiAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IGN1cnJlbnRzeXN0ZW1SZWZEYXRlID9cclxuICAgICAgdGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyhjdXJyZW50c3lzdGVtUmVmRGF0ZSkgOlxyXG4gICAgICB0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLmNvbnZlcnREYXRlVG9EYXRlVVRDKHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0lTTyhuZXcgRGF0ZSkpO1xyXG4gICAgdGhpcy5zZXRTZWxlY3RlZERhdGUodGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvSVNPKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKSk7XHJcblxyXG4gICAgLy8gdW50aWwgdGhlIGJldHRlciBzb2x1dGlvbiBmb3IgdHJhbnNsYXRlLlxyXG4gICAgdGhpcy5sb2NhbEVuID0ge1xyXG4gICAgICBmaXJzdERheU9mV2VlazogMSxcclxuICAgICAgZGF5TmFtZXM6IFsnU3VuZGF5JywgJ01vbmRheScsICdUdWVzZGF5JywgJ1dlZG5lc2RheScsICdUaHVyc2RheScsICdGcmlkYXknLCAnU2F0dXJkYXknXSxcclxuICAgICAgZGF5TmFtZXNTaG9ydDogWydTdW4nLCAnTW9uJywgJ1R1ZScsICdXZWQnLCAnVGh1JywgJ0ZyaScsICdTYXQnXSxcclxuICAgICAgZGF5TmFtZXNNaW46IFsnU3UnLCAnTW8nLCAnVHUnLCAnV2UnLCAnVGgnLCAnRnInLCAnU2EnXSxcclxuICAgICAgbW9udGhOYW1lczogWydKYW51YXJ5JywgJ0ZlYnJ1YXJ5JywgJ01hcmNoJywgJ0FwcmlsJywgJ01heScsICdKdW5lJywgJ0p1bHknLCAnQXVndXN0JywgJ1NlcHRlbWJlcicsICdPY3RvYmVyJywgJ05vdmVtYmVyJywgJ0RlY2VtYmVyJ10sXHJcbiAgICAgIG1vbnRoTmFtZXNTaG9ydDogWydKYW4nLCAnRmViJywgJ01hcicsICdBcHInLCAnTWF5JywgJ0p1bicsICdKdWwnLCAnQXVnJywgJ1NlcCcsICdPY3QnLCAnTm92JywgJ0RlYyddLFxyXG4gICAgICB0b2RheTogJ1RvZGF5JyxcclxuICAgICAgY2xlYXI6ICdDbGVhcidcclxuICAgIH07XHJcbiAgICAvLyB1bnRpbCB0aGUgYmV0dGVyIHNvbHV0aW9uIGZvciB0cmFuc2xhdGUuXHJcbiAgICB0aGlzLmxvY2FsZVRyID0ge1xyXG4gICAgICBmaXJzdERheU9mV2VlazogMSxcclxuICAgICAgZGF5TmFtZXM6IFsnUGF6YXInLCAnUGF6YXJ0ZXNpJywgJ1NhbMSxJywgJ8OHYXLFn2FtYmEnLCAnUGVyxZ9lbWJlJywgJ0N1bWEnLCAnQ3VtYXJ0ZXNpJ10sXHJcbiAgICAgIGRheU5hbWVzU2hvcnQ6IFsnUGF6JywgJ1B0cycsICdTYWwnLCAnw4dhcicsICdQZXInLCAnQ3VtJywgJ0N0cyddLFxyXG4gICAgICBkYXlOYW1lc01pbjogWydQeicsICdQdCcsICdTbCcsICfDh3InLCAnUHInLCAnQ20nLCAnQ3QnXSxcclxuICAgICAgbW9udGhOYW1lczogWydPY2FrJywgJ8WedWJhdCcsICdNYXJ0JywgJ05pc2FuJywgJ01hecSxcycsICdIYXppcmFuJywgJ1RlbW11eicsICdBxJ91c3RvcycsICdFeWzDvGwnLCAnRWtpbScsICdLYXPEsW0nLCAnQXJhbMSxayddLFxyXG4gICAgICBtb250aE5hbWVzU2hvcnQ6IFsnT2NhJywgJ8WedWInLCAnTWFyJywgJ05pcycsICdNYXknLCAnSGF6JywgJ1RlbScsICdBxJ91JywgJ0V5bCcsICdFa2knLCAnS2FzJywgJ0FyYSddLFxyXG4gICAgICB0b2RheTogJ0J1Z8O8bicsXHJcbiAgICAgIGNsZWFyOiAnVGVtaXpsZSdcclxuICAgIH07XHJcbiAgICAvLyB1bnRpbCB0aGUgYmV0dGVyIHNvbHV0aW9uIGZvciB0cmFuc2xhdGUuXHJcbiAgICB0aGlzLmxvY2FsZSA9IHRoaXMubG9jYWxlVHI7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudHJhbnNsYXRlLm9uTGFuZ0NoYW5nZVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmxvY2FsZSA9IGV2ZW50LmxhbmcgPT09ICd0cicgPyB0aGlzLmxvY2FsZVRyIDogdGhpcy5sb2NhbEVuO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgdGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5nZXRTZWxlY3RlZFJlZmVyZW5jZURhdGUoKVxyXG4gICAgICAuc3Vic2NyaWJlKChzZWxlY3RlZFJlZkRhdGU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFJlZkRhdGUgIT09ICcnKSB7XHJcbiAgICAgICAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0RhdGVVVEMoc2VsZWN0ZWRSZWZEYXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNhbGVuZGFyRGF0ZVZhbHVlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlO1xyXG4gIH1cclxuXHJcbiAgc2V0IGNhbGVuZGFyRGF0ZVZhbHVlKHZhbHVlOiBEYXRlKSB7XHJcbiAgICB0aGlzLl9jYWxlbmRhckRhdGVWYWx1ZSA9IHZhbHVlO1xyXG4gICAgLy8gdGhpcy5zZXRTZWxlY3RlZERhdGUodGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvSVNPKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKSk7XHJcbiAgfVxyXG5cclxuICBzZXRTZWxlY3RlZERhdGUoc2VsZWN0ZWREYXRlSVNPOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2Uuc2V0U2VsZWN0ZWRSZWZlcmVuY2VEYXRlKHNlbGVjdGVkRGF0ZUlTTyk7XHJcbiAgfVxyXG5cclxuICBvbkNsb3NlKCk6IHZvaWQge1xyXG4gICAgdGhpcy5jaGVja0FuZEFwcGx5RGF0ZSgpO1xyXG4gIH1cclxuXHJcblxyXG4gIGtleURvd25GdW5jdGlvbihldmVudDogYW55KSB7XHJcbiAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMTMpIHtcclxuICAgICAgdGhpcy5jaGVja0FuZEFwcGx5RGF0ZSgpO1xyXG4gICAgICBpZiAodGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgJiYgdGhpcy5zeXN0ZW1SZWZEYXRlUGlja2VyICYmIHRoaXMuc3lzdGVtUmVmRGF0ZVBpY2tlci5vdmVybGF5VmlzaWJsZSkge1xyXG4gICAgICAgIHRoaXMuc3lzdGVtUmVmRGF0ZVBpY2tlci5vdmVybGF5VmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjaGVja0FuZEFwcGx5RGF0ZSgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGN1cnJlbnRzeXN0ZW1SZWZEYXRlID0gdGhpcy5zdG9yYWdlU2VydmljZS5nZXRTdG9yZWQoU3RvcmFnZS5TeXN0ZW1SZWZEYXRlLCBTdG9yYWdlVHlwZS5TZXNzaW9uU3RvcmFnZSk7XHJcbiAgICBpZiAodGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUpIHtcclxuICAgICAgY29uc3Qgc2VsZWN0ZWREYXRlID0gdGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvSVNPKHRoaXMuX2NhbGVuZGFyRGF0ZVZhbHVlKTtcclxuICAgICAgaWYgKGN1cnJlbnRzeXN0ZW1SZWZEYXRlICE9PSBzZWxlY3RlZERhdGUpIHtcclxuICAgICAgICB0aGlzLnNldFNlbGVjdGVkRGF0ZSh0aGlzLnN5c3RlbVJlZmVyZW5jZURhdGVTZXJ2aWNlLmNvbnZlcnREYXRlVG9JU08odGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUpKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5fY2FsZW5kYXJEYXRlVmFsdWUgPSBjdXJyZW50c3lzdGVtUmVmRGF0ZSA/XHJcbiAgICAgICAgdGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvRGF0ZVVUQyhjdXJyZW50c3lzdGVtUmVmRGF0ZSkgOlxyXG4gICAgICAgIHRoaXMuc3lzdGVtUmVmZXJlbmNlRGF0ZVNlcnZpY2UuY29udmVydERhdGVUb0RhdGVVVEModGhpcy5zeXN0ZW1SZWZlcmVuY2VEYXRlU2VydmljZS5jb252ZXJ0RGF0ZVRvSVNPKG5ldyBEYXRlKSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnRyYW5zbGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc3lzUmVmRGF0ZVN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnN5c1JlZkRhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19