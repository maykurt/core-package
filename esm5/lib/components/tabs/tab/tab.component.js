/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TabsComponent } from '../tabs.component';
var TabComponent = /** @class */ (function () {
    function TabComponent(parent, router, _ngZone) {
        this.parent = parent;
        this.router = router;
        this._ngZone = _ngZone;
        this.title = '';
        // this.step = 0;
    }
    /**
     * @return {?}
     */
    TabComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.parent.addTabs(this.title, this.link);
        this.isCurrent = this.title === this.parent.activeTab;
        if (this.isCurrent && this.link) {
            this.router.navigate([this.link]);
        }
        this.tabChangeSubcriotion = this.parent.tabChange
            .subscribe(function (tab) {
            _this.isCurrent = _this.title === tab.title;
            if (_this.isCurrent && _this.link) {
                _this._ngZone.run(function () { return _this.router.navigate([_this.link]); });
            }
        });
    };
    /**
     * @return {?}
     */
    TabComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.tabChangeSubcriotion) {
            this.tabChangeSubcriotion.unsubscribe();
        }
    };
    TabComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-tab',
                    host: {
                        '[style.display]': 'isCurrent ? "flex" : "none"',
                    },
                    template: "\n    <ng-content></ng-content>\n  "
                }] }
    ];
    /** @nocollapse */
    TabComponent.ctorParameters = function () { return [
        { type: TabsComponent },
        { type: Router },
        { type: NgZone }
    ]; };
    TabComponent.propDecorators = {
        title: [{ type: Input }],
        link: [{ type: Input }]
    };
    return TabComponent;
}());
export { TabComponent };
if (false) {
    /** @type {?} */
    TabComponent.prototype.title;
    /** @type {?} */
    TabComponent.prototype.link;
    /** @type {?} */
    TabComponent.prototype.isCurrent;
    /** @type {?} */
    TabComponent.prototype.tabChangeSubcriotion;
    /** @type {?} */
    TabComponent.prototype.parent;
    /**
     * @type {?}
     * @private
     */
    TabComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    TabComponent.prototype._ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3RhYnMvdGFiL3RhYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUdsRDtJQWdCRSxzQkFBbUIsTUFBcUIsRUFBVSxNQUFjLEVBQVUsT0FBZTtRQUF0RSxXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFOaEYsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQU8xQixpQkFBaUI7SUFDbkIsQ0FBQzs7OztJQUVELCtCQUFROzs7SUFBUjtRQUFBLGlCQWNDO1FBYkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ3RELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDbkM7UUFFRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO2FBQzlDLFNBQVMsQ0FBQyxVQUFDLEdBQUc7WUFDYixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxLQUFLLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQztZQUMxQyxJQUFJLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLElBQUksRUFBRTtnQkFDL0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQWpDLENBQWlDLENBQUMsQ0FBQzthQUMzRDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUdELGtDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzdCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6QztJQUNILENBQUM7O2dCQXpDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLElBQUksRUFBRTt3QkFDSixpQkFBaUIsRUFBRSw2QkFBNkI7cUJBQ2pEO29CQUNELFFBQVEsRUFBRSxxQ0FFVDtpQkFDRjs7OztnQkFYUSxhQUFhO2dCQURiLE1BQU07Z0JBRCtCLE1BQU07Ozt3QkFlakQsS0FBSzt1QkFDTCxLQUFLOztJQStCUixtQkFBQztDQUFBLEFBMUNELElBMENDO1NBakNZLFlBQVk7OztJQUN2Qiw2QkFBNEI7O0lBQzVCLDRCQUF1Qjs7SUFDdkIsaUNBQW1COztJQUNuQiw0Q0FBbUM7O0lBR3ZCLDhCQUE0Qjs7Ozs7SUFBRSw4QkFBc0I7Ozs7O0lBQUUsK0JBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBUYWJzQ29tcG9uZW50IH0gZnJvbSAnLi4vdGFicy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnY29yZS10YWInLFxyXG4gIGhvc3Q6IHtcclxuICAgICdbc3R5bGUuZGlzcGxheV0nOiAnaXNDdXJyZW50ID8gXCJmbGV4XCIgOiBcIm5vbmVcIicsXHJcbiAgfSxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gIGAsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZyA9ICcnO1xyXG4gIEBJbnB1dCgpIGxpbms/OiBzdHJpbmc7XHJcbiAgaXNDdXJyZW50OiBib29sZWFuO1xyXG4gIHRhYkNoYW5nZVN1YmNyaW90aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG5cclxuICBjb25zdHJ1Y3RvcihwdWJsaWMgcGFyZW50OiBUYWJzQ29tcG9uZW50LCBwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIF9uZ1pvbmU6IE5nWm9uZSkge1xyXG4gICAgLy8gdGhpcy5zdGVwID0gMDtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJlbnQuYWRkVGFicyh0aGlzLnRpdGxlLCB0aGlzLmxpbmspO1xyXG4gICAgdGhpcy5pc0N1cnJlbnQgPSB0aGlzLnRpdGxlID09PSB0aGlzLnBhcmVudC5hY3RpdmVUYWI7XHJcbiAgICBpZiAodGhpcy5pc0N1cnJlbnQgJiYgdGhpcy5saW5rKSB7XHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLmxpbmtdKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnRhYkNoYW5nZVN1YmNyaW90aW9uID0gdGhpcy5wYXJlbnQudGFiQ2hhbmdlXHJcbiAgICAgIC5zdWJzY3JpYmUoKHRhYikgPT4ge1xyXG4gICAgICAgIHRoaXMuaXNDdXJyZW50ID0gdGhpcy50aXRsZSA9PT0gdGFiLnRpdGxlO1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ3VycmVudCAmJiB0aGlzLmxpbmspIHtcclxuICAgICAgICAgIHRoaXMuX25nWm9uZS5ydW4oKCkgPT4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMubGlua10pKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMudGFiQ2hhbmdlU3ViY3Jpb3Rpb24pIHtcclxuICAgICAgdGhpcy50YWJDaGFuZ2VTdWJjcmlvdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=