/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
var TabsComponent = /** @class */ (function () {
    function TabsComponent(router, activatedRoute) {
        var _this = this;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.tabChange = new EventEmitter();
        this.tabs = [];
        // this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
        //   .subscribe(event => {
        //     // set breadcrumbs
        //     const foundData = this.tabs.find(x => x.link === this.router.url);
        //     if (foundData) {
        //       this.onTabClick(foundData);
        //     }
        //   });
        this.routingSubscription = this.router.events.pipe(filter(function (event) { return event instanceof NavigationEnd; })).subscribe(function () {
            /** @type {?} */
            var foundData = _this.tabs.find(function (x) { return x.link === _this.router.url; });
            if (foundData) {
                _this.onTabClick(foundData);
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    TabsComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        if (changes.activeTab && !changes.activeTab.isFirstChange()) {
            this.foundData = this.tabs.find(function (x) { return x.title === _this.activeTab; });
            this.tabChange.emit(this.foundData);
        }
    };
    /**
     * @param {?} tab
     * @return {?}
     */
    TabsComponent.prototype.getClassActive = /**
     * @param {?} tab
     * @return {?}
     */
    function (tab) {
        // return this.activeTab === tab.title && !tab.link;
        // (bu alan hep false dönüyo)
        return this.activeTab === tab.title && ((tab.link !== undefined && tab.link !== null) || !tab.link);
    };
    /**
     * @param {?} tab
     * @return {?}
     */
    TabsComponent.prototype.onTabClick = /**
     * @param {?} tab
     * @return {?}
     */
    function (tab) {
        this.activeTab = tab.title;
        this.tabChange.emit(tab);
    };
    /**
     * @param {?} title
     * @param {?} link
     * @return {?}
     */
    TabsComponent.prototype.addTabs = /**
     * @param {?} title
     * @param {?} link
     * @return {?}
     */
    function (title, link) {
        this.tabs.push({ title: title, link: link });
        if (this.router && this.router.url && link === this.router.url) {
            this.activeTab = title;
            // this.tabChange.emit({ title, link });
        }
        // if (title === this.activeTab) {
        //   this.tabChange.emit(this.activeTab);
        //   if (link) {
        //     this.router.navigate([link]);
        //   }
        // }
    };
    /**
     * @return {?}
     */
    TabsComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    };
    TabsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'core-tabs',
                    template: "<nav class=\"navbar navbar-expand-sm navbar-dark justify-content-md-center mb-5\">\r\n  <ul class=\"navbar-nav\">\r\n    <li class=\"nav-item\" *ngFor=\"let tab of tabs\" (click)=\"onTabClick(tab)\" [class.active]=\"getClassActive(tab)\">\r\n      <a *ngIf=\"tab.link\" class=\"nav-link\" [routerLinkActive]=\"['router-link-active']\"\r\n        [routerLink]=\"[tab.link]\">{{tab.title|translate}}</a>\r\n      <a *ngIf=\"!tab.link\" class=\"nav-link\">{{tab.title|translate}}</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n\r\n<ng-content></ng-content>",
                    styles: [".navbar{margin:15px 0!important;padding:0}.navbar-nav{display:flex;width:100%;margin-bottom:0;padding-left:0;padding-right:0;margin-left:0;margin-right:0;box-shadow:0 5px 20px rgba(0,0,0,.05)}.nav-item{font-weight:600;flex:1;text-align:center;padding:0;border:1px solid #e3f1f8;border-left-width:0}.nav-item:first-child{border-left-width:1px}.nav-item a.nav-link{background:#fff;color:#788d96;line-height:24px;border-radius:0;transition:.2s ease-in-out;cursor:pointer;height:100%;display:flex;align-items:center;justify-content:center}.nav-item a.nav-link.router-link-active,.nav-item.active a{color:#175169;border-bottom:2px solid #409fdd}.nav-item a.nav-link:hover{background-color:#e3f1f8;color:#1a2b49}"]
                }] }
    ];
    /** @nocollapse */
    TabsComponent.ctorParameters = function () { return [
        { type: Router },
        { type: ActivatedRoute }
    ]; };
    TabsComponent.propDecorators = {
        activeTab: [{ type: Input }],
        tabChange: [{ type: Output }]
    };
    return TabsComponent;
}());
export { TabsComponent };
if (false) {
    /** @type {?} */
    TabsComponent.prototype.activeTab;
    /** @type {?} */
    TabsComponent.prototype.tabChange;
    /** @type {?} */
    TabsComponent.prototype.tabs;
    /** @type {?} */
    TabsComponent.prototype.foundData;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.routingSubscription;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    TabsComponent.prototype.activatedRoute;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90YWJzL3RhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQWEsWUFBWSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQUM1RyxPQUFPLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUd4RSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEM7SUFhRSx1QkFBb0IsTUFBYyxFQUFVLGNBQThCO1FBQTFFLGlCQW9CQztRQXBCbUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQU5oRSxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQU92QyxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUVmLGdHQUFnRztRQUNoRywwQkFBMEI7UUFDMUIseUJBQXlCO1FBQ3pCLHlFQUF5RTtRQUN6RSx1QkFBdUI7UUFDdkIsb0NBQW9DO1FBQ3BDLFFBQVE7UUFDUixRQUFRO1FBRVIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDaEQsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsQ0FBQyxDQUNoRCxDQUFDLFNBQVMsQ0FBQzs7Z0JBQ0osU0FBUyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBMUIsQ0FBMEIsQ0FBQztZQUNqRSxJQUFJLFNBQVMsRUFBRTtnQkFDYixLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzVCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUNELG1DQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkFLQztRQUpDLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSSxDQUFDLFNBQVMsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO1lBQ2pFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNyQztJQUNILENBQUM7Ozs7O0lBRUQsc0NBQWM7Ozs7SUFBZCxVQUFlLEdBQVE7UUFDckIsb0RBQW9EO1FBQ3BELDZCQUE2QjtRQUM3QixPQUFPLElBQUksQ0FBQyxTQUFTLEtBQUssR0FBRyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0RyxDQUFDOzs7OztJQUVELGtDQUFVOzs7O0lBQVYsVUFBVyxHQUFRO1FBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7Ozs7SUFFRCwrQkFBTzs7Ozs7SUFBUCxVQUFRLEtBQWEsRUFBRSxJQUFZO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO1FBRWhDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxJQUFJLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUU7WUFDOUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsd0NBQXdDO1NBQ3pDO1FBR0Qsa0NBQWtDO1FBQ2xDLHlDQUF5QztRQUN6QyxnQkFBZ0I7UUFDaEIsb0NBQW9DO1FBQ3BDLE1BQU07UUFDTixJQUFJO0lBQ04sQ0FBQzs7OztJQUdELG1DQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN4QztJQUNILENBQUM7O2dCQTFFRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLCtpQkFBb0M7O2lCQUVyQzs7OztnQkFUUSxNQUFNO2dCQUFFLGNBQWM7Ozs0QkFXNUIsS0FBSzs0QkFDTCxNQUFNOztJQW9FVCxvQkFBQztDQUFBLEFBM0VELElBMkVDO1NBdEVZLGFBQWE7OztJQUN4QixrQ0FBMkI7O0lBQzNCLGtDQUF5Qzs7SUFDekMsNkJBQVk7O0lBQ1osa0NBQWU7Ozs7O0lBRWYsNENBQTBDOzs7OztJQUU5QiwrQkFBc0I7Ozs7O0lBQUUsdUNBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBPbkNoYW5nZXMsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcywgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIE5hdmlnYXRpb25FbmQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnY29yZS10YWJzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdGFicy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ3RhYnMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFic0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcywgT25EZXN0cm95IHtcclxuICBASW5wdXQoKSBhY3RpdmVUYWI6IHN0cmluZztcclxuICBAT3V0cHV0KCkgdGFiQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIHRhYnM6IGFueVtdO1xyXG4gIGZvdW5kRGF0YTogYW55O1xyXG5cclxuICBwcml2YXRlIHJvdXRpbmdTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICAgIHRoaXMudGFicyA9IFtdO1xyXG5cclxuICAgIC8vIHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbiA9IHRoaXMucm91dGVyLmV2ZW50cy5maWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKVxyXG4gICAgLy8gICAuc3Vic2NyaWJlKGV2ZW50ID0+IHtcclxuICAgIC8vICAgICAvLyBzZXQgYnJlYWRjcnVtYnNcclxuICAgIC8vICAgICBjb25zdCBmb3VuZERhdGEgPSB0aGlzLnRhYnMuZmluZCh4ID0+IHgubGluayA9PT0gdGhpcy5yb3V0ZXIudXJsKTtcclxuICAgIC8vICAgICBpZiAoZm91bmREYXRhKSB7XHJcbiAgICAvLyAgICAgICB0aGlzLm9uVGFiQ2xpY2soZm91bmREYXRhKTtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgIH0pO1xyXG5cclxuICAgIHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbiA9IHRoaXMucm91dGVyLmV2ZW50cy5waXBlKFxyXG4gICAgICBmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKVxyXG4gICAgKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICBjb25zdCBmb3VuZERhdGEgPSB0aGlzLnRhYnMuZmluZCh4ID0+IHgubGluayA9PT0gdGhpcy5yb3V0ZXIudXJsKTtcclxuICAgICAgaWYgKGZvdW5kRGF0YSkge1xyXG4gICAgICAgIHRoaXMub25UYWJDbGljayhmb3VuZERhdGEpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMuYWN0aXZlVGFiICYmICFjaGFuZ2VzLmFjdGl2ZVRhYi5pc0ZpcnN0Q2hhbmdlKCkpIHtcclxuICAgICAgdGhpcy5mb3VuZERhdGEgPSB0aGlzLnRhYnMuZmluZCh4ID0+IHgudGl0bGUgPT09IHRoaXMuYWN0aXZlVGFiKTtcclxuICAgICAgdGhpcy50YWJDaGFuZ2UuZW1pdCh0aGlzLmZvdW5kRGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRDbGFzc0FjdGl2ZSh0YWI6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgLy8gcmV0dXJuIHRoaXMuYWN0aXZlVGFiID09PSB0YWIudGl0bGUgJiYgIXRhYi5saW5rO1xyXG4gICAgLy8gKGJ1IGFsYW4gaGVwIGZhbHNlIGTDtm7DvHlvKVxyXG4gICAgcmV0dXJuIHRoaXMuYWN0aXZlVGFiID09PSB0YWIudGl0bGUgJiYgKCh0YWIubGluayAhPT0gdW5kZWZpbmVkICYmIHRhYi5saW5rICE9PSBudWxsKSB8fCAhdGFiLmxpbmspO1xyXG4gIH1cclxuXHJcbiAgb25UYWJDbGljayh0YWI6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5hY3RpdmVUYWIgPSB0YWIudGl0bGU7XHJcbiAgICB0aGlzLnRhYkNoYW5nZS5lbWl0KHRhYik7XHJcbiAgfVxyXG5cclxuICBhZGRUYWJzKHRpdGxlOiBzdHJpbmcsIGxpbms6IHN0cmluZykge1xyXG4gICAgdGhpcy50YWJzLnB1c2goeyB0aXRsZSwgbGluayB9KTtcclxuXHJcbiAgICBpZiAodGhpcy5yb3V0ZXIgJiYgdGhpcy5yb3V0ZXIudXJsICYmIGxpbmsgPT09IHRoaXMucm91dGVyLnVybCkge1xyXG4gICAgICB0aGlzLmFjdGl2ZVRhYiA9IHRpdGxlO1xyXG4gICAgICAvLyB0aGlzLnRhYkNoYW5nZS5lbWl0KHsgdGl0bGUsIGxpbmsgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8vIGlmICh0aXRsZSA9PT0gdGhpcy5hY3RpdmVUYWIpIHtcclxuICAgIC8vICAgdGhpcy50YWJDaGFuZ2UuZW1pdCh0aGlzLmFjdGl2ZVRhYik7XHJcbiAgICAvLyAgIGlmIChsaW5rKSB7XHJcbiAgICAvLyAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW2xpbmtdKTtcclxuICAgIC8vICAgfVxyXG4gICAgLy8gfVxyXG4gIH1cclxuXHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucm91dGluZ1N1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnJvdXRpbmdTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19