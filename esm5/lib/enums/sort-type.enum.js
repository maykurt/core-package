/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var SortType = {
    Asc: 0,
    Desc: 1,
    None: 2,
};
export { SortType };
SortType[SortType.Asc] = 'Asc';
SortType[SortType.Desc] = 'Desc';
SortType[SortType.None] = 'None';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ydC10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvc29ydC10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsTUFBTztJQUNQLE9BQVE7SUFDUixPQUFRIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gU29ydFR5cGUge1xyXG4gIEFzYyA9IDAsXHJcbiAgRGVzYyA9IDEsXHJcbiAgTm9uZSA9IDJcclxufVxyXG4iXX0=