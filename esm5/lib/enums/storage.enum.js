/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var Storage = {
    AccessToken: 'access_token',
    RefreshToken: 'refresh_token',
    Expires: 'expires',
    User: 'user',
    SystemRefDate: 'system_ref_date',
    CurrentLanguage: 'current_language',
};
export { Storage };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2VudW1zL3N0b3JhZ2UuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxhQUFjLGNBQWM7SUFDNUIsY0FBZSxlQUFlO0lBQzlCLFNBQVUsU0FBUztJQUNuQixNQUFPLE1BQU07SUFDYixlQUFnQixpQkFBaUI7SUFDakMsaUJBQWlCLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIFN0b3JhZ2Uge1xyXG4gIEFjY2Vzc1Rva2VuID0gJ2FjY2Vzc190b2tlbicsXHJcbiAgUmVmcmVzaFRva2VuID0gJ3JlZnJlc2hfdG9rZW4nLFxyXG4gIEV4cGlyZXMgPSAnZXhwaXJlcycsXHJcbiAgVXNlciA9ICd1c2VyJyxcclxuICBTeXN0ZW1SZWZEYXRlID0gJ3N5c3RlbV9yZWZfZGF0ZScsXHJcbiAgQ3VycmVudExhbmd1YWdlPSAnY3VycmVudF9sYW5ndWFnZSdcclxufVxyXG4iXX0=