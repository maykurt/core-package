/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var OperationType = {
    None: 0,
    Created: 1,
    Updated: 2,
    Deleted: 3,
};
export { OperationType };
OperationType[OperationType.None] = 'None';
OperationType[OperationType.Created] = 'Created';
OperationType[OperationType.Updated] = 'Updated';
OperationType[OperationType.Deleted] = 'Deleted';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9uLXR5cGUuZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lbnVtcy9vcGVyYXRpb24tdHlwZS5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUNFLE9BQVE7SUFDUixVQUFXO0lBQ1gsVUFBVztJQUNYLFVBQVciLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBPcGVyYXRpb25UeXBlIHtcclxuICBOb25lID0gMCxcclxuICBDcmVhdGVkID0gMSxcclxuICBVcGRhdGVkID0gMixcclxuICBEZWxldGVkID0gM1xyXG59XHJcbiJdfQ==