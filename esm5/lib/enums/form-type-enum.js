/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var FormType = {
    New: 'new',
    Edit: 'edit',
};
export { FormType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS10eXBlLWVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvZm9ybS10eXBlLWVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0ksS0FBTSxLQUFLO0lBQ1gsTUFBTyxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gRm9ybVR5cGUge1xyXG4gICAgTmV3ID0gJ25ldycsXHJcbiAgICBFZGl0ID0gJ2VkaXQnXHJcbn1cclxuIl19