/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var Comparison = {
    In: 0,
    LessThanOrEqualTo: 1,
    LessThan: 2,
    IsNullOrWhiteSpace: 3,
    IsNull: 4,
    IsNotNullNorWhiteSpace: 5,
    IsNotNull: 6,
    IsNotEmpty: 7,
    IsEmpty: 8,
    StartsWith: 9,
    GreaterThanOrEqualTo: 10,
    GreaterThan: 11,
    EqualTo: 12,
    EndsWith: 13,
    DoesNotContain: 14,
    Contains: 15,
    Between: 16,
    NotEqualTo: 17,
};
export { Comparison };
Comparison[Comparison.In] = 'In';
Comparison[Comparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
Comparison[Comparison.LessThan] = 'LessThan';
Comparison[Comparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
Comparison[Comparison.IsNull] = 'IsNull';
Comparison[Comparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
Comparison[Comparison.IsNotNull] = 'IsNotNull';
Comparison[Comparison.IsNotEmpty] = 'IsNotEmpty';
Comparison[Comparison.IsEmpty] = 'IsEmpty';
Comparison[Comparison.StartsWith] = 'StartsWith';
Comparison[Comparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
Comparison[Comparison.GreaterThan] = 'GreaterThan';
Comparison[Comparison.EqualTo] = 'EqualTo';
Comparison[Comparison.EndsWith] = 'EndsWith';
Comparison[Comparison.DoesNotContain] = 'DoesNotContain';
Comparison[Comparison.Contains] = 'Contains';
Comparison[Comparison.Between] = 'Between';
Comparison[Comparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
var DefaultComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
export { DefaultComparison };
DefaultComparison[DefaultComparison.EqualTo] = 'EqualTo';
DefaultComparison[DefaultComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
var TextComparison = {
    Contains: 15,
    DoesNotContain: 14,
    EndsWith: 13,
    EqualTo: 12,
    IsEmpty: 8,
    IsNotEmpty: 7,
    IsNotNull: 6,
    IsNotNullNorWhiteSpace: 5,
    IsNull: 4,
    IsNullOrWhiteSpace: 3,
    NotEqualTo: 17,
    StartsWith: 9,
};
export { TextComparison };
TextComparison[TextComparison.Contains] = 'Contains';
TextComparison[TextComparison.DoesNotContain] = 'DoesNotContain';
TextComparison[TextComparison.EndsWith] = 'EndsWith';
TextComparison[TextComparison.EqualTo] = 'EqualTo';
TextComparison[TextComparison.IsEmpty] = 'IsEmpty';
TextComparison[TextComparison.IsNotEmpty] = 'IsNotEmpty';
TextComparison[TextComparison.IsNotNull] = 'IsNotNull';
TextComparison[TextComparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
TextComparison[TextComparison.IsNull] = 'IsNull';
TextComparison[TextComparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
TextComparison[TextComparison.NotEqualTo] = 'NotEqualTo';
TextComparison[TextComparison.StartsWith] = 'StartsWith';
/** @enum {number} */
var NumberComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
export { NumberComparison };
NumberComparison[NumberComparison.Between] = 'Between';
NumberComparison[NumberComparison.EqualTo] = 'EqualTo';
NumberComparison[NumberComparison.GreaterThan] = 'GreaterThan';
NumberComparison[NumberComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
NumberComparison[NumberComparison.LessThan] = 'LessThan';
NumberComparison[NumberComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
NumberComparison[NumberComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
var BooleanComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
export { BooleanComparison };
BooleanComparison[BooleanComparison.EqualTo] = 'EqualTo';
BooleanComparison[BooleanComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
var DateComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
export { DateComparison };
DateComparison[DateComparison.Between] = 'Between';
DateComparison[DateComparison.EqualTo] = 'EqualTo';
DateComparison[DateComparison.GreaterThan] = 'GreaterThan';
DateComparison[DateComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
DateComparison[DateComparison.LessThan] = 'LessThan';
DateComparison[DateComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
DateComparison[DateComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
var ComparisonType = {
    Default: 1,
    Text: 2,
    Number: 3,
    Date: 4,
    Boolean: 5,
};
export { ComparisonType };
ComparisonType[ComparisonType.Default] = 'Default';
ComparisonType[ComparisonType.Text] = 'Text';
ComparisonType[ComparisonType.Number] = 'Number';
ComparisonType[ComparisonType.Date] = 'Date';
ComparisonType[ComparisonType.Boolean] = 'Boolean';
export var ComparisonList;
(function (ComparisonList) {
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function values(comparisonType) {
        return Object.keys(this.getEnumAccordingToType(comparisonType)).filter(function (type) { return isNaN((/** @type {?} */ (type))) && type !== 'values'; }).sort();
    }
    ComparisonList.values = values;
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function getEnumAccordingToType(comparisonType) {
        /** @type {?} */
        var enums;
        if (!comparisonType) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Default) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Text) {
            enums = TextComparison;
        }
        else if (comparisonType === ComparisonType.Number) {
            enums = NumberComparison;
        }
        else if (comparisonType === ComparisonType.Boolean) {
            enums = BooleanComparison;
        }
        else if (comparisonType === ComparisonType.Date) {
            enums = DateComparison;
        }
        return enums;
    }
    ComparisonList.getEnumAccordingToType = getEnumAccordingToType;
    /**
     * @param {?} comparisonType
     * @param {?=} key
     * @param {?=} label
     * @return {?}
     */
    function createKeyLabelArray(comparisonType, key, label) {
        var _this = this;
        if (key === void 0) { key = 'Id'; }
        if (label === void 0) { label = 'Definition'; }
        /** @type {?} */
        var enumKeys = this.values(comparisonType);
        /** @type {?} */
        var newKeyLabelArray = [];
        enumKeys.forEach(function (enumKey) {
            /** @type {?} */
            var newValue = {};
            newValue[key] = _this.getEnumAccordingToType(comparisonType)[enumKey];
            newValue[label] = enumKey;
            newKeyLabelArray.push(newValue);
        });
        return newKeyLabelArray;
    }
    ComparisonList.createKeyLabelArray = createKeyLabelArray;
})(ComparisonList || (ComparisonList = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGFyaXNvbi10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvY29tcGFyaXNvbi10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBRUUsS0FBTTtJQUNOLG9CQUFxQjtJQUNyQixXQUFZO0lBQ1oscUJBQXNCO0lBQ3RCLFNBQVU7SUFDVix5QkFBMEI7SUFDMUIsWUFBYTtJQUNiLGFBQWM7SUFDZCxVQUFXO0lBQ1gsYUFBYztJQUNkLHdCQUF5QjtJQUN6QixlQUFnQjtJQUNoQixXQUFZO0lBQ1osWUFBYTtJQUNiLGtCQUFtQjtJQUNuQixZQUFhO0lBQ2IsV0FBWTtJQUNaLGNBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBTWYsV0FBWTtJQUNaLGNBQWU7Ozs7Ozs7SUFJZixZQUFhO0lBQ2Isa0JBQW1CO0lBQ25CLFlBQWE7SUFDYixXQUFZO0lBQ1osVUFBVztJQUNYLGFBQWM7SUFDZCxZQUFhO0lBQ2IseUJBQTBCO0lBQzFCLFNBQVU7SUFDVixxQkFBc0I7SUFDdEIsY0FBZTtJQUNmLGFBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBSWQsV0FBWTtJQUNaLFdBQVk7SUFDWixlQUFnQjtJQUNoQix3QkFBeUI7SUFDekIsV0FBWTtJQUNaLG9CQUFxQjtJQUNyQixjQUFlOzs7Ozs7Ozs7Ozs7SUFJZixXQUFZO0lBQ1osY0FBZTs7Ozs7OztJQUlmLFdBQVk7SUFDWixXQUFZO0lBQ1osZUFBZ0I7SUFDaEIsd0JBQXlCO0lBQ3pCLFdBQVk7SUFDWixvQkFBcUI7SUFDckIsY0FBZTs7Ozs7Ozs7Ozs7O0lBSWYsVUFBVztJQUNYLE9BQVE7SUFDUixTQUFVO0lBQ1YsT0FBUTtJQUNSLFVBQVc7Ozs7Ozs7O0FBR2IsTUFBTSxLQUFXLGNBQWMsQ0FxQzlCO0FBckNELFdBQWlCLGNBQWM7Ozs7O0lBRTdCLFNBQWdCLE1BQU0sQ0FBQyxjQUErQjtRQUNwRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUNwRSxVQUFDLElBQUksSUFBSyxPQUFBLEtBQUssQ0FBQyxtQkFBSyxJQUFJLEVBQUEsQ0FBQyxJQUFJLElBQUksS0FBSyxRQUFRLEVBQXJDLENBQXFDLENBQ2hELENBQUMsSUFBSSxFQUFFLENBQUM7SUFDWCxDQUFDO0lBSmUscUJBQU0sU0FJckIsQ0FBQTs7Ozs7SUFFRCxTQUFnQixzQkFBc0IsQ0FBQyxjQUErQjs7WUFDaEUsS0FBVTtRQUNkLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDbkIsS0FBSyxHQUFHLGlCQUFpQixDQUFDO1NBQzNCO2FBQU0sSUFBSSxjQUFjLEtBQUssY0FBYyxDQUFDLE9BQU8sRUFBRTtZQUNwRCxLQUFLLEdBQUcsaUJBQWlCLENBQUM7U0FDM0I7YUFBTSxJQUFJLGNBQWMsS0FBSyxjQUFjLENBQUMsSUFBSSxFQUFFO1lBQ2pELEtBQUssR0FBRyxjQUFjLENBQUM7U0FDeEI7YUFBTSxJQUFJLGNBQWMsS0FBSyxjQUFjLENBQUMsTUFBTSxFQUFFO1lBQ25ELEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztTQUMxQjthQUFNLElBQUksY0FBYyxLQUFLLGNBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDcEQsS0FBSyxHQUFHLGlCQUFpQixDQUFDO1NBQzNCO2FBQU0sSUFBSSxjQUFjLEtBQUssY0FBYyxDQUFDLElBQUksRUFBRTtZQUNqRCxLQUFLLEdBQUcsY0FBYyxDQUFDO1NBQ3hCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBaEJlLHFDQUFzQix5QkFnQnJDLENBQUE7Ozs7Ozs7SUFFRCxTQUFnQixtQkFBbUIsQ0FBQyxjQUE4QixFQUFFLEdBQWtCLEVBQUUsS0FBNEI7UUFBcEgsaUJBVUM7UUFWbUUsb0JBQUEsRUFBQSxVQUFrQjtRQUFFLHNCQUFBLEVBQUEsb0JBQTRCOztZQUM5RyxRQUFRLEdBQWEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7O1lBQ2hELGdCQUFnQixHQUFRLEVBQUU7UUFDOUIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQWU7O2dCQUMzQixRQUFRLEdBQVEsRUFBRTtZQUN0QixRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3JFLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxPQUFPLENBQUM7WUFDMUIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxnQkFBZ0IsQ0FBQztJQUMxQixDQUFDO0lBVmUsa0NBQW1CLHNCQVVsQyxDQUFBO0FBQ0gsQ0FBQyxFQXJDZ0IsY0FBYyxLQUFkLGNBQWMsUUFxQzlCIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaXQgaXMgaGVyZSBmb3Igbm90IG1ha2luZyB3b3JzZSBvbiBjb2RlIGZvciB2aWV3cyB0aGF0IHBlb3BsZSB3b3JrIG9uXHJcbmV4cG9ydCBlbnVtIENvbXBhcmlzb24ge1xyXG4gIEluID0gMCxcclxuICBMZXNzVGhhbk9yRXF1YWxUbyA9IDEsXHJcbiAgTGVzc1RoYW4gPSAyLFxyXG4gIElzTnVsbE9yV2hpdGVTcGFjZSA9IDMsXHJcbiAgSXNOdWxsID0gNCxcclxuICBJc05vdE51bGxOb3JXaGl0ZVNwYWNlID0gNSxcclxuICBJc05vdE51bGwgPSA2LFxyXG4gIElzTm90RW1wdHkgPSA3LFxyXG4gIElzRW1wdHkgPSA4LFxyXG4gIFN0YXJ0c1dpdGggPSA5LFxyXG4gIEdyZWF0ZXJUaGFuT3JFcXVhbFRvID0gMTAsXHJcbiAgR3JlYXRlclRoYW4gPSAxMSxcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgRW5kc1dpdGggPSAxMyxcclxuICBEb2VzTm90Q29udGFpbiA9IDE0LFxyXG4gIENvbnRhaW5zID0gMTUsXHJcbiAgQmV0d2VlbiA9IDE2LFxyXG4gIE5vdEVxdWFsVG8gPSAxN1xyXG59XHJcblxyXG5cclxuXHJcbmV4cG9ydCBlbnVtIERlZmF1bHRDb21wYXJpc29uIHtcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgTm90RXF1YWxUbyA9IDE3XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIFRleHRDb21wYXJpc29uIHtcclxuICBDb250YWlucyA9IDE1LFxyXG4gIERvZXNOb3RDb250YWluID0gMTQsXHJcbiAgRW5kc1dpdGggPSAxMyxcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgSXNFbXB0eSA9IDgsXHJcbiAgSXNOb3RFbXB0eSA9IDcsXHJcbiAgSXNOb3ROdWxsID0gNixcclxuICBJc05vdE51bGxOb3JXaGl0ZVNwYWNlID0gNSxcclxuICBJc051bGwgPSA0LFxyXG4gIElzTnVsbE9yV2hpdGVTcGFjZSA9IDMsXHJcbiAgTm90RXF1YWxUbyA9IDE3LFxyXG4gIFN0YXJ0c1dpdGggPSA5XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIE51bWJlckNvbXBhcmlzb24ge1xyXG4gIEJldHdlZW4gPSAxNixcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgR3JlYXRlclRoYW4gPSAxMSxcclxuICBHcmVhdGVyVGhhbk9yRXF1YWxUbyA9IDEwLFxyXG4gIExlc3NUaGFuID0gMixcclxuICBMZXNzVGhhbk9yRXF1YWxUbyA9IDEsXHJcbiAgTm90RXF1YWxUbyA9IDE3XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIEJvb2xlYW5Db21wYXJpc29uIHtcclxuICBFcXVhbFRvID0gMTIsXHJcbiAgTm90RXF1YWxUbyA9IDE3XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIERhdGVDb21wYXJpc29uIHtcclxuICBCZXR3ZWVuID0gMTYsXHJcbiAgRXF1YWxUbyA9IDEyLFxyXG4gIEdyZWF0ZXJUaGFuID0gMTEsXHJcbiAgR3JlYXRlclRoYW5PckVxdWFsVG8gPSAxMCxcclxuICBMZXNzVGhhbiA9IDIsXHJcbiAgTGVzc1RoYW5PckVxdWFsVG8gPSAxLFxyXG4gIE5vdEVxdWFsVG8gPSAxN1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBDb21wYXJpc29uVHlwZSB7XHJcbiAgRGVmYXVsdCA9IDEsXHJcbiAgVGV4dCA9IDIsXHJcbiAgTnVtYmVyID0gMyxcclxuICBEYXRlID0gNCxcclxuICBCb29sZWFuID0gNVxyXG59XHJcblxyXG5leHBvcnQgbmFtZXNwYWNlIENvbXBhcmlzb25MaXN0IHtcclxuXHJcbiAgZXhwb3J0IGZ1bmN0aW9uIHZhbHVlcyhjb21wYXJpc29uVHlwZT86IENvbXBhcmlzb25UeXBlKSB7XHJcbiAgICByZXR1cm4gT2JqZWN0LmtleXModGhpcy5nZXRFbnVtQWNjb3JkaW5nVG9UeXBlKGNvbXBhcmlzb25UeXBlKSkuZmlsdGVyKFxyXG4gICAgICAodHlwZSkgPT4gaXNOYU4oPGFueT50eXBlKSAmJiB0eXBlICE9PSAndmFsdWVzJ1xyXG4gICAgKS5zb3J0KCk7XHJcbiAgfVxyXG5cclxuICBleHBvcnQgZnVuY3Rpb24gZ2V0RW51bUFjY29yZGluZ1RvVHlwZShjb21wYXJpc29uVHlwZT86IENvbXBhcmlzb25UeXBlKSB7XHJcbiAgICBsZXQgZW51bXM6IGFueTtcclxuICAgIGlmICghY29tcGFyaXNvblR5cGUpIHtcclxuICAgICAgZW51bXMgPSBEZWZhdWx0Q29tcGFyaXNvbjtcclxuICAgIH0gZWxzZSBpZiAoY29tcGFyaXNvblR5cGUgPT09IENvbXBhcmlzb25UeXBlLkRlZmF1bHQpIHtcclxuICAgICAgZW51bXMgPSBEZWZhdWx0Q29tcGFyaXNvbjtcclxuICAgIH0gZWxzZSBpZiAoY29tcGFyaXNvblR5cGUgPT09IENvbXBhcmlzb25UeXBlLlRleHQpIHtcclxuICAgICAgZW51bXMgPSBUZXh0Q29tcGFyaXNvbjtcclxuICAgIH0gZWxzZSBpZiAoY29tcGFyaXNvblR5cGUgPT09IENvbXBhcmlzb25UeXBlLk51bWJlcikge1xyXG4gICAgICBlbnVtcyA9IE51bWJlckNvbXBhcmlzb247XHJcbiAgICB9IGVsc2UgaWYgKGNvbXBhcmlzb25UeXBlID09PSBDb21wYXJpc29uVHlwZS5Cb29sZWFuKSB7XHJcbiAgICAgIGVudW1zID0gQm9vbGVhbkNvbXBhcmlzb247XHJcbiAgICB9IGVsc2UgaWYgKGNvbXBhcmlzb25UeXBlID09PSBDb21wYXJpc29uVHlwZS5EYXRlKSB7XHJcbiAgICAgIGVudW1zID0gRGF0ZUNvbXBhcmlzb247XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZW51bXM7XHJcbiAgfVxyXG5cclxuICBleHBvcnQgZnVuY3Rpb24gY3JlYXRlS2V5TGFiZWxBcnJheShjb21wYXJpc29uVHlwZTogQ29tcGFyaXNvblR5cGUsIGtleTogc3RyaW5nID0gJ0lkJywgbGFiZWw6IHN0cmluZyA9ICdEZWZpbml0aW9uJykge1xyXG4gICAgbGV0IGVudW1LZXlzOiBzdHJpbmdbXSA9IHRoaXMudmFsdWVzKGNvbXBhcmlzb25UeXBlKTtcclxuICAgIGxldCBuZXdLZXlMYWJlbEFycmF5OiBhbnkgPSBbXTtcclxuICAgIGVudW1LZXlzLmZvckVhY2goKGVudW1LZXk6IHN0cmluZykgPT4ge1xyXG4gICAgICBsZXQgbmV3VmFsdWU6IGFueSA9IHt9O1xyXG4gICAgICBuZXdWYWx1ZVtrZXldID0gdGhpcy5nZXRFbnVtQWNjb3JkaW5nVG9UeXBlKGNvbXBhcmlzb25UeXBlKVtlbnVtS2V5XTtcclxuICAgICAgbmV3VmFsdWVbbGFiZWxdID0gZW51bUtleTtcclxuICAgICAgbmV3S2V5TGFiZWxBcnJheS5wdXNoKG5ld1ZhbHVlKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIG5ld0tleUxhYmVsQXJyYXk7XHJcbiAgfVxyXG59XHJcbiJdfQ==