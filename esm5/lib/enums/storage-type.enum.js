/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var StorageType = {
    LocalStorage: 'localStorage',
    SessionStorage: 'sessionStorage',
};
export { StorageType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvc3RvcmFnZS10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsY0FBZSxjQUFjO0lBQzdCLGdCQUFpQixnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBTdG9yYWdlVHlwZSB7XHJcbiAgTG9jYWxTdG9yYWdlID0gJ2xvY2FsU3RvcmFnZScsXHJcbiAgU2Vzc2lvblN0b3JhZ2UgPSAnc2Vzc2lvblN0b3JhZ2UnXHJcbn1cclxuIl19