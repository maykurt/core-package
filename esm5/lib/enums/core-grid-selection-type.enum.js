/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var CoreGridSelectionType = {
    None: 'none',
    Single: 'single',
    Multiple: 'multiple',
};
export { CoreGridSelectionType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1ncmlkLXNlbGVjdGlvbi10eXBlLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvY29yZS1ncmlkLXNlbGVjdGlvbi10eXBlLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsTUFBTyxNQUFNO0lBQ2IsUUFBUyxRQUFRO0lBQ2pCLFVBQVcsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIENvcmVHcmlkU2VsZWN0aW9uVHlwZSB7XHJcbiAgTm9uZSA9ICdub25lJyxcclxuICBTaW5nbGUgPSAnc2luZ2xlJyxcclxuICBNdWx0aXBsZSA9ICdtdWx0aXBsZSdcclxufVxyXG4iXX0=