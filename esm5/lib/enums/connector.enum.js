/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var Connector = {
    And: 0,
    Or: 1,
};
export { Connector };
Connector[Connector.And] = 'And';
Connector[Connector.Or] = 'Or';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdG9yLmVudW0uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29yZS9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZW51bXMvY29ubmVjdG9yLmVudW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQ0UsTUFBTztJQUNQLEtBQU0iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBDb25uZWN0b3Ige1xyXG4gIEFuZCA9IDAsXHJcbiAgT3IgPSAxXHJcbn1cclxuIl19