/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var DateSelectionMode = {
    Single: 'single',
    Multiple: 'multiple',
    Range: 'range',
};
export { DateSelectionMode };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1zZWxlY3Rpb24tbW9kZS5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2VudW1zL2RhdGUtc2VsZWN0aW9uLW1vZGUuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxRQUFTLFFBQVE7SUFDakIsVUFBVyxVQUFVO0lBQ3JCLE9BQVEsT0FBTyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIERhdGVTZWxlY3Rpb25Nb2RlIHtcclxuICBTaW5nbGUgPSAnc2luZ2xlJyxcclxuICBNdWx0aXBsZSA9ICdtdWx0aXBsZScsXHJcbiAgUmFuZ2UgPSAncmFuZ2UnXHJcbn1cclxuIl19