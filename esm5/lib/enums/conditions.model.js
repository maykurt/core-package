/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export var Conditions = [
    {
        'name': 'Equal',
        'id': 0
    },
    {
        'name': 'LessThan',
        'id': 1
    },
    {
        'name': 'LessThanOrEqual',
        'id': 2
    },
    {
        'name': 'GreaterThan',
        'id': 3
    },
    {
        'name': 'GreaterThanOrEqual',
        'id': 4
    },
    {
        'name': 'NotEqual',
        'id': 5
    },
    {
        'name': 'Contains',
        'id': 6
    },
    {
        'name': 'StartsWith',
        'id': 7
    },
    {
        'name': 'EndsWith',
        'id': 8
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZGl0aW9ucy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb3JlL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lbnVtcy9jb25kaXRpb25zLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsTUFBTSxLQUFPLFVBQVUsR0FBVTtJQUM3QjtRQUNJLE1BQU0sRUFBRSxPQUFPO1FBQ2YsSUFBSSxFQUFFLENBQUM7S0FDVjtJQUNEO1FBQ0ksTUFBTSxFQUFFLFVBQVU7UUFDbEIsSUFBSSxFQUFFLENBQUM7S0FDVjtJQUNEO1FBQ0ksTUFBTSxFQUFFLGlCQUFpQjtRQUN6QixJQUFJLEVBQUUsQ0FBQztLQUNWO0lBQ0Q7UUFDSSxNQUFNLEVBQUUsYUFBYTtRQUNyQixJQUFJLEVBQUUsQ0FBQztLQUNWO0lBQ0Q7UUFDSSxNQUFNLEVBQUUsb0JBQW9CO1FBQzVCLElBQUksRUFBRSxDQUFDO0tBQ1Y7SUFDRDtRQUNJLE1BQU0sRUFBRSxVQUFVO1FBQ2xCLElBQUksRUFBRSxDQUFDO0tBQ1Y7SUFDRDtRQUNJLE1BQU0sRUFBRSxVQUFVO1FBQ2xCLElBQUksRUFBRSxDQUFDO0tBQ1Y7SUFDRDtRQUNJLE1BQU0sRUFBRSxZQUFZO1FBQ3BCLElBQUksRUFBRSxDQUFDO0tBQ1Y7SUFDRDtRQUNJLE1BQU0sRUFBRSxVQUFVO1FBQ2xCLElBQUksRUFBRSxDQUFDO0tBQ1Y7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBDb25kaXRpb25zOiBhbnlbXSA9IFtcclxuICAgIHtcclxuICAgICAgICAnbmFtZSc6ICdFcXVhbCcsXHJcbiAgICAgICAgJ2lkJzogMFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICAnbmFtZSc6ICdMZXNzVGhhbicsXHJcbiAgICAgICAgJ2lkJzogMVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICAnbmFtZSc6ICdMZXNzVGhhbk9yRXF1YWwnLFxyXG4gICAgICAgICdpZCc6IDJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgJ25hbWUnOiAnR3JlYXRlclRoYW4nLFxyXG4gICAgICAgICdpZCc6IDNcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgJ25hbWUnOiAnR3JlYXRlclRoYW5PckVxdWFsJyxcclxuICAgICAgICAnaWQnOiA0XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgICduYW1lJzogJ05vdEVxdWFsJyxcclxuICAgICAgICAnaWQnOiA1XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgICduYW1lJzogJ0NvbnRhaW5zJyxcclxuICAgICAgICAnaWQnOiA2XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgICduYW1lJzogJ1N0YXJ0c1dpdGgnLFxyXG4gICAgICAgICdpZCc6IDdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgJ25hbWUnOiAnRW5kc1dpdGgnLFxyXG4gICAgICAgICdpZCc6IDhcclxuICAgIH1cclxuXTtcclxuXHJcbiJdfQ==