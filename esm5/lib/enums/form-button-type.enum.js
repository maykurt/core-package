/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var FormButtonType = {
    Save: 1,
    Update: 2,
    Cancel: 3,
};
export { FormButtonType };
FormButtonType[FormButtonType.Save] = 'Save';
FormButtonType[FormButtonType.Update] = 'Update';
FormButtonType[FormButtonType.Cancel] = 'Cancel';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1idXR0b24tdHlwZS5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2VudW1zL2Zvcm0tYnV0dG9uLXR5cGUuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxPQUFRO0lBQ1IsU0FBVTtJQUNWLFNBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBGb3JtQnV0dG9uVHlwZSB7XHJcbiAgU2F2ZSA9IDEsXHJcbiAgVXBkYXRlID0gMixcclxuICBDYW5jZWwgPSAzXHJcbn1cclxuIl19