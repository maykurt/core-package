/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var AlertType = {
    Warning: 'warning',
    Error: 'error',
    Success: 'success',
    Info: 'info',
    Question: 'question',
};
export { AlertType };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtdHlwZS5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2VudW1zL2FsZXJ0LXR5cGUuZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDSSxTQUFVLFNBQVM7SUFDbkIsT0FBUSxPQUFPO0lBQ2YsU0FBVSxTQUFTO0lBQ25CLE1BQU8sTUFBTTtJQUNiLFVBQVcsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIEFsZXJ0VHlwZSB7XHJcbiAgICBXYXJuaW5nID0gJ3dhcm5pbmcnLFxyXG4gICAgRXJyb3IgPSAnZXJyb3InLFxyXG4gICAgU3VjY2VzcyA9ICdzdWNjZXNzJyxcclxuICAgIEluZm8gPSAnaW5mbycsXHJcbiAgICBRdWVzdGlvbiA9ICdxdWVzdGlvbidcclxufVxyXG4iXX0=