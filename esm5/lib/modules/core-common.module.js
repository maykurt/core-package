/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import * as coreGridEditElements from '../components/core-grid/cell/edit/core-grid-edit-elements.index';
import * as coreGridReadOnlyElements from '../components/core-grid/cell/read-only/core-grid-elements.index';
import * as coreGridFilterElements from '../components//core-grid/cell/filter/core-grid-filter-elements.index';
import * as Layouts from '../components/index';
import { ListOfValuesModalComponent } from '../components/list-of-values/list-of-values-modal/list-of-values-modal.component';
import { BsDropdownModule, BsDatepickerModule, TabsModule, CollapseModule, SortableModule, PaginationModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DataService, SweetAlertService, ToastrUtilsService, AuthenticationService, TranslateHelperService, ValidationService } from '../services/index';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { TextMaskModule } from 'angular2-text-mask';
import { ModalModule } from 'ngx-bootstrap';
import { NgxCurrencyModule } from 'ngx-currency';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { FileUploadModule } from 'primeng/fileupload';
import { RouterModule } from '@angular/router';
/** @type {?} */
export var customCurrencyMaskConfig = {
    align: 'left',
    allowNegative: true,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: '',
    suffix: '',
    thousands: '.',
    nullable: true
};
import { SafePipe } from '../pipes/index';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
/**
 * @param {?} http
 * @return {?}
 */
export function HttpLoaderFactory(http) {
    return new TranslateHttpLoader(http);
}
var CoreCommonModule = /** @class */ (function () {
    function CoreCommonModule() {
    }
    CoreCommonModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        coreGridEditElements.coreGridEditElements,
                        coreGridReadOnlyElements.coreGridReadOnlyElements,
                        coreGridFilterElements.coreGridFilterElements,
                        Layouts.LayoutComponents,
                        SafePipe
                    ],
                    imports: [
                        CommonModule,
                        FormsModule,
                        HttpClientModule,
                        ReactiveFormsModule,
                        RouterModule,
                        TextMaskModule,
                        NgSelectModule,
                        FileUploadModule,
                        BsDropdownModule.forRoot(),
                        BsDatepickerModule.forRoot(),
                        TabsModule.forRoot(),
                        ModalModule.forRoot(),
                        CollapseModule.forRoot(),
                        SortableModule.forRoot(),
                        PaginationModule.forRoot(),
                        TranslateModule.forRoot({
                            loader: [
                                { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] }
                            ]
                        }),
                        AgGridModule.withComponents([
                            coreGridEditElements.coreGridEditElements,
                            coreGridReadOnlyElements.coreGridReadOnlyElements,
                            coreGridFilterElements.coreGridFilterElements,
                            Layouts.CommandCellComponent,
                            Layouts.ColumnHeaderComponent,
                            Layouts.NoRowsOverlayComponent,
                            Layouts.LoadingOverlayComponent
                        ]),
                        ToastContainerModule,
                        ToastrModule.forRoot({
                            closeButton: true,
                            progressBar: true
                        }),
                        NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
                        CalendarModule,
                        CheckboxModule,
                        DialogModule,
                        OverlayPanelModule,
                        FontAwesomeModule
                    ],
                    entryComponents: [
                        ListOfValuesModalComponent
                    ],
                    providers: [
                        DataService,
                        SweetAlertService,
                        ToastrUtilsService,
                        AuthenticationService,
                        TranslateHelperService,
                        ValidationService
                    ],
                    exports: [
                        Layouts.LayoutComponents,
                        TranslateModule,
                        SafePipe
                    ],
                    schemas: [
                        CUSTOM_ELEMENTS_SCHEMA,
                        NO_ERRORS_SCHEMA
                    ]
                },] }
    ];
    return CoreCommonModule;
}());
export { CoreCommonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1jb21tb24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvcmUvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZHVsZXMvY29yZS1jb21tb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLGdCQUFnQixFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUN4RyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsVUFBVSxFQUFxQixNQUFNLHNCQUFzQixDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sS0FBSyxvQkFBb0IsTUFBTSxpRUFBaUUsQ0FBQztBQUN4RyxPQUFPLEtBQUssd0JBQXdCLE1BQU0saUVBQWlFLENBQUM7QUFDNUcsT0FBTyxLQUFLLHNCQUFzQixNQUFNLHNFQUFzRSxDQUFDO0FBRS9HLE9BQU8sS0FBSyxPQUFPLE1BQU0scUJBQXFCLENBQUM7QUFFL0MsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sa0ZBQWtGLENBQUM7QUFFOUgsT0FBTyxFQUNMLGdCQUFnQixFQUNoQixrQkFBa0IsRUFDbEIsVUFBVSxFQUNWLGNBQWMsRUFDZCxjQUFjLEVBQ2QsZ0JBQWdCLEVBQ2pCLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFakUsT0FBTyxFQUNMLFdBQVcsRUFDWCxpQkFBaUIsRUFDakIsa0JBQWtCLEVBQ2xCLHFCQUFxQixFQUNyQixzQkFBc0IsRUFDdEIsaUJBQWlCLEVBQ2xCLE1BQU0sbUJBQW1CLENBQUM7QUFFM0IsT0FBTyxFQUFFLFlBQVksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUNoRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFNUMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ2pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzFELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXRELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7QUFFL0MsTUFBTSxLQUFPLHdCQUF3QixHQUFHO0lBQ3RDLEtBQUssRUFBRSxNQUFNO0lBQ2IsYUFBYSxFQUFFLElBQUk7SUFDbkIsU0FBUyxFQUFFLElBQUk7SUFDZixPQUFPLEVBQUUsR0FBRztJQUNaLFNBQVMsRUFBRSxDQUFDO0lBQ1osTUFBTSxFQUFFLEVBQUU7SUFDVixNQUFNLEVBQUUsRUFBRTtJQUNWLFNBQVMsRUFBRSxHQUFHO0lBQ2QsUUFBUSxFQUFFLElBQUk7Q0FDZjtBQUVELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMxQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQzs7Ozs7QUFFckUsTUFBTSxVQUFVLGlCQUFpQixDQUFDLElBQWdCO0lBQ2hELE9BQU8sSUFBSSxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN2QyxDQUFDO0FBRUQ7SUFBQTtJQW9GQSxDQUFDOztnQkFwRkEsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRTt3QkFDWixvQkFBb0IsQ0FBQyxvQkFBb0I7d0JBQ3pDLHdCQUF3QixDQUFDLHdCQUF3Qjt3QkFDakQsc0JBQXNCLENBQUMsc0JBQXNCO3dCQUM3QyxPQUFPLENBQUMsZ0JBQWdCO3dCQUN4QixRQUFRO3FCQUNUO29CQUNELE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLFdBQVc7d0JBQ1gsZ0JBQWdCO3dCQUNoQixtQkFBbUI7d0JBQ25CLFlBQVk7d0JBQ1osY0FBYzt3QkFDZCxjQUFjO3dCQUNkLGdCQUFnQjt3QkFDaEIsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO3dCQUMxQixrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7d0JBQzVCLFVBQVUsQ0FBQyxPQUFPLEVBQUU7d0JBQ3BCLFdBQVcsQ0FBQyxPQUFPLEVBQUU7d0JBQ3JCLGNBQWMsQ0FBQyxPQUFPLEVBQUU7d0JBQ3hCLGNBQWMsQ0FBQyxPQUFPLEVBQUU7d0JBQ3hCLGdCQUFnQixDQUFDLE9BQU8sRUFBRTt3QkFDMUIsZUFBZSxDQUFDLE9BQU8sQ0FBQzs0QkFDdEIsTUFBTSxFQUFFO2dDQUNOLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxVQUFVLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLEVBQUU7NkJBQ2hGO3lCQUNGLENBQUM7d0JBQ0YsWUFBWSxDQUFDLGNBQWMsQ0FBQzs0QkFDMUIsb0JBQW9CLENBQUMsb0JBQW9COzRCQUN6Qyx3QkFBd0IsQ0FBQyx3QkFBd0I7NEJBQ2pELHNCQUFzQixDQUFDLHNCQUFzQjs0QkFDN0MsT0FBTyxDQUFDLG9CQUFvQjs0QkFDNUIsT0FBTyxDQUFDLHFCQUFxQjs0QkFDN0IsT0FBTyxDQUFDLHNCQUFzQjs0QkFDOUIsT0FBTyxDQUFDLHVCQUF1Qjt5QkFDaEMsQ0FBQzt3QkFDRixvQkFBb0I7d0JBQ3BCLFlBQVksQ0FBQyxPQUFPLENBQUM7NEJBQ25CLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixXQUFXLEVBQUUsSUFBSTt5QkFDbEIsQ0FBQzt3QkFDRixpQkFBaUIsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUM7d0JBQ25ELGNBQWM7d0JBQ2QsY0FBYzt3QkFDZCxZQUFZO3dCQUNaLGtCQUFrQjt3QkFDbEIsaUJBQWlCO3FCQUNsQjtvQkFDRCxlQUFlLEVBQUU7d0JBQ2YsMEJBQTBCO3FCQUMzQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1QsV0FBVzt3QkFDWCxpQkFBaUI7d0JBQ2pCLGtCQUFrQjt3QkFDbEIscUJBQXFCO3dCQUNyQixzQkFBc0I7d0JBQ3RCLGlCQUFpQjtxQkFDbEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLE9BQU8sQ0FBQyxnQkFBZ0I7d0JBQ3hCLGVBQWU7d0JBQ2YsUUFBUTtxQkFDVDtvQkFDRCxPQUFPLEVBQUU7d0JBQ1Asc0JBQXNCO3dCQUN0QixnQkFBZ0I7cUJBQ2pCO2lCQUNGOztJQWNELHVCQUFDO0NBQUEsQUFwRkQsSUFvRkM7U0FiWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSwgTk9fRVJST1JTX1NDSEVNQSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlLCBIdHRwQ2xpZW50LCBIVFRQX0lOVEVSQ0VQVE9SUyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5pbXBvcnQgeyBBZ0dyaWRNb2R1bGUgfSBmcm9tICdhZy1ncmlkLWFuZ3VsYXInO1xyXG5pbXBvcnQgKiBhcyBjb3JlR3JpZEVkaXRFbGVtZW50cyBmcm9tICcuLi9jb21wb25lbnRzL2NvcmUtZ3JpZC9jZWxsL2VkaXQvY29yZS1ncmlkLWVkaXQtZWxlbWVudHMuaW5kZXgnO1xyXG5pbXBvcnQgKiBhcyBjb3JlR3JpZFJlYWRPbmx5RWxlbWVudHMgZnJvbSAnLi4vY29tcG9uZW50cy9jb3JlLWdyaWQvY2VsbC9yZWFkLW9ubHkvY29yZS1ncmlkLWVsZW1lbnRzLmluZGV4JztcclxuaW1wb3J0ICogYXMgY29yZUdyaWRGaWx0ZXJFbGVtZW50cyBmcm9tICcuLi9jb21wb25lbnRzLy9jb3JlLWdyaWQvY2VsbC9maWx0ZXIvY29yZS1ncmlkLWZpbHRlci1lbGVtZW50cy5pbmRleCc7XHJcblxyXG5pbXBvcnQgKiBhcyBMYXlvdXRzIGZyb20gJy4uL2NvbXBvbmVudHMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgTGlzdE9mVmFsdWVzTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2xpc3Qtb2YtdmFsdWVzL2xpc3Qtb2YtdmFsdWVzLW1vZGFsL2xpc3Qtb2YtdmFsdWVzLW1vZGFsLmNvbXBvbmVudCc7XHJcblxyXG5pbXBvcnQge1xyXG4gIEJzRHJvcGRvd25Nb2R1bGUsXHJcbiAgQnNEYXRlcGlja2VyTW9kdWxlLFxyXG4gIFRhYnNNb2R1bGUsXHJcbiAgQ29sbGFwc2VNb2R1bGUsXHJcbiAgU29ydGFibGVNb2R1bGUsXHJcbiAgUGFnaW5hdGlvbk1vZHVsZVxyXG59IGZyb20gJ25neC1ib290c3RyYXAnO1xyXG5cclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZUxvYWRlciB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVIdHRwTG9hZGVyIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvaHR0cC1sb2FkZXInO1xyXG5cclxuaW1wb3J0IHtcclxuICBEYXRhU2VydmljZSxcclxuICBTd2VldEFsZXJ0U2VydmljZSxcclxuICBUb2FzdHJVdGlsc1NlcnZpY2UsXHJcbiAgQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gIFRyYW5zbGF0ZUhlbHBlclNlcnZpY2UsXHJcbiAgVmFsaWRhdGlvblNlcnZpY2VcclxufSBmcm9tICcuLi9zZXJ2aWNlcy9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBUb2FzdHJNb2R1bGUsIFRvYXN0Q29udGFpbmVyTW9kdWxlIH0gZnJvbSAnbmd4LXRvYXN0cic7XHJcbmltcG9ydCB7IE5nU2VsZWN0TW9kdWxlIH0gZnJvbSAnQG5nLXNlbGVjdC9uZy1zZWxlY3QnO1xyXG5cclxuaW1wb3J0IHsgVGV4dE1hc2tNb2R1bGUgfSBmcm9tICdhbmd1bGFyMi10ZXh0LW1hc2snO1xyXG5cclxuaW1wb3J0IHsgTW9kYWxNb2R1bGUgfSBmcm9tICduZ3gtYm9vdHN0cmFwJztcclxuXHJcbmltcG9ydCB7IE5neEN1cnJlbmN5TW9kdWxlIH0gZnJvbSAnbmd4LWN1cnJlbmN5JztcclxuaW1wb3J0IHsgQ2FsZW5kYXJNb2R1bGUgfSBmcm9tICdwcmltZW5nL2NhbGVuZGFyJztcclxuaW1wb3J0IHsgQ2hlY2tib3hNb2R1bGUgfSBmcm9tICdwcmltZW5nL2NoZWNrYm94JztcclxuaW1wb3J0IHsgRGlhbG9nTW9kdWxlIH0gZnJvbSAncHJpbWVuZy9kaWFsb2cnO1xyXG5pbXBvcnQgeyBPdmVybGF5UGFuZWxNb2R1bGUgfSBmcm9tICdwcmltZW5nL292ZXJsYXlwYW5lbCc7XHJcbmltcG9ydCB7IEZpbGVVcGxvYWRNb2R1bGUgfSBmcm9tICdwcmltZW5nL2ZpbGV1cGxvYWQnO1xyXG5cclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmV4cG9ydCBjb25zdCBjdXN0b21DdXJyZW5jeU1hc2tDb25maWcgPSB7XHJcbiAgYWxpZ246ICdsZWZ0JyxcclxuICBhbGxvd05lZ2F0aXZlOiB0cnVlLFxyXG4gIGFsbG93WmVybzogdHJ1ZSxcclxuICBkZWNpbWFsOiAnLCcsXHJcbiAgcHJlY2lzaW9uOiAyLFxyXG4gIHByZWZpeDogJycsXHJcbiAgc3VmZml4OiAnJyxcclxuICB0aG91c2FuZHM6ICcuJyxcclxuICBudWxsYWJsZTogdHJ1ZVxyXG59O1xyXG5cclxuaW1wb3J0IHsgU2FmZVBpcGUgfSBmcm9tICcuLi9waXBlcy9pbmRleCc7XHJcbmltcG9ydCB7IEZvbnRBd2Vzb21lTW9kdWxlIH0gZnJvbSAnQGZvcnRhd2Vzb21lL2FuZ3VsYXItZm9udGF3ZXNvbWUnO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIEh0dHBMb2FkZXJGYWN0b3J5KGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICByZXR1cm4gbmV3IFRyYW5zbGF0ZUh0dHBMb2FkZXIoaHR0cCk7XHJcbn1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBjb3JlR3JpZEVkaXRFbGVtZW50cy5jb3JlR3JpZEVkaXRFbGVtZW50cyxcclxuICAgIGNvcmVHcmlkUmVhZE9ubHlFbGVtZW50cy5jb3JlR3JpZFJlYWRPbmx5RWxlbWVudHMsXHJcbiAgICBjb3JlR3JpZEZpbHRlckVsZW1lbnRzLmNvcmVHcmlkRmlsdGVyRWxlbWVudHMsXHJcbiAgICBMYXlvdXRzLkxheW91dENvbXBvbmVudHMsXHJcbiAgICBTYWZlUGlwZVxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIFRleHRNYXNrTW9kdWxlLFxyXG4gICAgTmdTZWxlY3RNb2R1bGUsXHJcbiAgICBGaWxlVXBsb2FkTW9kdWxlLFxyXG4gICAgQnNEcm9wZG93bk1vZHVsZS5mb3JSb290KCksXHJcbiAgICBCc0RhdGVwaWNrZXJNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgVGFic01vZHVsZS5mb3JSb290KCksXHJcbiAgICBNb2RhbE1vZHVsZS5mb3JSb290KCksXHJcbiAgICBDb2xsYXBzZU1vZHVsZS5mb3JSb290KCksXHJcbiAgICBTb3J0YWJsZU1vZHVsZS5mb3JSb290KCksXHJcbiAgICBQYWdpbmF0aW9uTW9kdWxlLmZvclJvb3QoKSxcclxuICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KHtcclxuICAgICAgbG9hZGVyOiBbXHJcbiAgICAgICAgeyBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsIHVzZUZhY3Rvcnk6IEh0dHBMb2FkZXJGYWN0b3J5LCBkZXBzOiBbSHR0cENsaWVudF0gfVxyXG4gICAgICBdXHJcbiAgICB9KSxcclxuICAgIEFnR3JpZE1vZHVsZS53aXRoQ29tcG9uZW50cyhbXHJcbiAgICAgIGNvcmVHcmlkRWRpdEVsZW1lbnRzLmNvcmVHcmlkRWRpdEVsZW1lbnRzLFxyXG4gICAgICBjb3JlR3JpZFJlYWRPbmx5RWxlbWVudHMuY29yZUdyaWRSZWFkT25seUVsZW1lbnRzLFxyXG4gICAgICBjb3JlR3JpZEZpbHRlckVsZW1lbnRzLmNvcmVHcmlkRmlsdGVyRWxlbWVudHMsXHJcbiAgICAgIExheW91dHMuQ29tbWFuZENlbGxDb21wb25lbnQsXHJcbiAgICAgIExheW91dHMuQ29sdW1uSGVhZGVyQ29tcG9uZW50LFxyXG4gICAgICBMYXlvdXRzLk5vUm93c092ZXJsYXlDb21wb25lbnQsXHJcbiAgICAgIExheW91dHMuTG9hZGluZ092ZXJsYXlDb21wb25lbnRcclxuICAgIF0pLFxyXG4gICAgVG9hc3RDb250YWluZXJNb2R1bGUsXHJcbiAgICBUb2FzdHJNb2R1bGUuZm9yUm9vdCh7XHJcbiAgICAgIGNsb3NlQnV0dG9uOiB0cnVlLFxyXG4gICAgICBwcm9ncmVzc0JhcjogdHJ1ZVxyXG4gICAgfSksXHJcbiAgICBOZ3hDdXJyZW5jeU1vZHVsZS5mb3JSb290KGN1c3RvbUN1cnJlbmN5TWFza0NvbmZpZyksXHJcbiAgICBDYWxlbmRhck1vZHVsZSxcclxuICAgIENoZWNrYm94TW9kdWxlLFxyXG4gICAgRGlhbG9nTW9kdWxlLFxyXG4gICAgT3ZlcmxheVBhbmVsTW9kdWxlLFxyXG4gICAgRm9udEF3ZXNvbWVNb2R1bGVcclxuICBdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgTGlzdE9mVmFsdWVzTW9kYWxDb21wb25lbnRcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgRGF0YVNlcnZpY2UsXHJcbiAgICBTd2VldEFsZXJ0U2VydmljZSxcclxuICAgIFRvYXN0clV0aWxzU2VydmljZSxcclxuICAgIEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgIFRyYW5zbGF0ZUhlbHBlclNlcnZpY2UsXHJcbiAgICBWYWxpZGF0aW9uU2VydmljZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgTGF5b3V0cy5MYXlvdXRDb21wb25lbnRzLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgU2FmZVBpcGVcclxuICBdLFxyXG4gIHNjaGVtYXM6IFtcclxuICAgIENVU1RPTV9FTEVNRU5UU19TQ0hFTUEsXHJcbiAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZUNvbW1vbk1vZHVsZSB7XHJcblxyXG4gIC8vIHB1YmxpYyBzdGF0aWMgZm9yUm9vdChlbnZpcm9ubWVudDogYW55KTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgLy8gICByZXR1cm4ge1xyXG4gIC8vICAgICBuZ01vZHVsZTogQ29yZUNvbW1vbk1vZHVsZSxcclxuICAvLyAgICAgcHJvdmlkZXJzOiBbXHJcbiAgLy8gICAgICAge1xyXG4gIC8vICAgICAgICAgcHJvdmlkZTogJ2Vudmlyb25tZW50JywgLy8geW91IGNhbiBhbHNvIHVzZSBJbmplY3Rpb25Ub2tlblxyXG4gIC8vICAgICAgICAgdXNlVmFsdWU6IGVudmlyb25tZW50XHJcbiAgLy8gICAgICAgfVxyXG4gIC8vICAgICBdXHJcbiAgLy8gICB9O1xyXG4gIC8vIH1cclxufVxyXG4iXX0=