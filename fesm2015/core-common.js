import { DomSanitizer } from '@angular/platform-browser';
import { Subject as Subject$1 } from 'rxjs/Subject';
import 'rxjs/add/operator/share';
import Swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JwtHelperService } from '@auth0/angular-jwt';
import 'rxjs/add/operator/filter';
import moment from 'moment';
import emailMask from 'text-mask-addons/dist/emailMask';
import { Subject, from, of, throwError, BehaviorSubject as BehaviorSubject$1 } from 'rxjs';
import { findIndex, cloneDeep, find, head, isNil, isNull, orderBy, assign } from 'lodash';
import { flatMap, map, concatMap, catchError, tap, switchMap, filter, take, finalize, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrService, ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { FileUploadModule } from 'primeng/fileupload';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET, RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Pipe, Injectable, Injector, Component, ChangeDetectorRef, ViewChild, NgZone, forwardRef, Output, EventEmitter, Input, ViewEncapsulation, ElementRef, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, defineInjectable, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule, BsDatepickerModule, TabsModule, CollapseModule, SortableModule, PaginationModule, ModalModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/calendar';
import { FormBuilder, NG_VALUE_ACCESSOR, FormGroup, FormArray, FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const CoreGridCellType = {
    // Edit Cell
    CoreBooleanEditCell: 'coreBooleanEditCell',
    CoreDateEditCell: 'coreDateEditCell',
    CoreLOVEditCell: 'coreLOVEditCell',
    CoreNumericEditCell: 'coreNumericEditCell',
    CoreSelectEditCell: 'coreSelectEditCell',
    CoreTextEditCell: 'coreTextEditCell',
    CoreMaskEditCell: 'coreMaskEditCell',
    CoreCurrencyEditCell: 'coreCurrencyEditCell',
    // Read-Only Cell
    CoreBooleanCell: 'coreBooleanCell',
    CoreLabelCell: 'coreLabelCell',
    CoreSelectCell: 'coreSelectCell',
    CoreDateCell: 'coreDateCell',
    CoreCurrencyCell: 'coreCurrencyCell',
    CoreMaskCell: 'coreMaskCell',
    CoreNumericCell: 'coreNumericCell',
    // Filter Cell
    CoreDateFilterCell: 'coreDateFilterCell',
    CoreSelectFilterCell: 'coreSelectFilterCell',
    CoreTextFilterCell: 'coreTextFilterCell',
    CoreNumericFilterCell: 'coreNumericFilterCell',
    CoreBooleanFilterCell: 'coreBooleanFilterCell',
    CoreCurrencyFilterCell: 'coreCurrencyFilterCell',
    CoreLOVFilterCell: 'coreLOVFilterCell',
    CoreMaskFilterCell: 'coreMaskFilterCell',
    // Command Cell
    CoreCommandCell: 'coreCommandCell',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const CoreGridColumnType = {
    Label: 'label',
    Text: 'text',
    Date: 'date',
    Select: 'select',
    Numeric: 'numeric',
    Boolean: 'boolean',
    Lov: 'lov',
    Mask: 'mask',
    Currency: 'currency',
    NumericLabel: 'numeric-label',
    TextSelect: 'text-select',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const CoreGridSelectionType = {
    None: 'none',
    Single: 'single',
    Multiple: 'multiple',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const Comparison = {
    In: 0,
    LessThanOrEqualTo: 1,
    LessThan: 2,
    IsNullOrWhiteSpace: 3,
    IsNull: 4,
    IsNotNullNorWhiteSpace: 5,
    IsNotNull: 6,
    IsNotEmpty: 7,
    IsEmpty: 8,
    StartsWith: 9,
    GreaterThanOrEqualTo: 10,
    GreaterThan: 11,
    EqualTo: 12,
    EndsWith: 13,
    DoesNotContain: 14,
    Contains: 15,
    Between: 16,
    NotEqualTo: 17,
};
Comparison[Comparison.In] = 'In';
Comparison[Comparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
Comparison[Comparison.LessThan] = 'LessThan';
Comparison[Comparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
Comparison[Comparison.IsNull] = 'IsNull';
Comparison[Comparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
Comparison[Comparison.IsNotNull] = 'IsNotNull';
Comparison[Comparison.IsNotEmpty] = 'IsNotEmpty';
Comparison[Comparison.IsEmpty] = 'IsEmpty';
Comparison[Comparison.StartsWith] = 'StartsWith';
Comparison[Comparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
Comparison[Comparison.GreaterThan] = 'GreaterThan';
Comparison[Comparison.EqualTo] = 'EqualTo';
Comparison[Comparison.EndsWith] = 'EndsWith';
Comparison[Comparison.DoesNotContain] = 'DoesNotContain';
Comparison[Comparison.Contains] = 'Contains';
Comparison[Comparison.Between] = 'Between';
Comparison[Comparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const DefaultComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
DefaultComparison[DefaultComparison.EqualTo] = 'EqualTo';
DefaultComparison[DefaultComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const TextComparison = {
    Contains: 15,
    DoesNotContain: 14,
    EndsWith: 13,
    EqualTo: 12,
    IsEmpty: 8,
    IsNotEmpty: 7,
    IsNotNull: 6,
    IsNotNullNorWhiteSpace: 5,
    IsNull: 4,
    IsNullOrWhiteSpace: 3,
    NotEqualTo: 17,
    StartsWith: 9,
};
TextComparison[TextComparison.Contains] = 'Contains';
TextComparison[TextComparison.DoesNotContain] = 'DoesNotContain';
TextComparison[TextComparison.EndsWith] = 'EndsWith';
TextComparison[TextComparison.EqualTo] = 'EqualTo';
TextComparison[TextComparison.IsEmpty] = 'IsEmpty';
TextComparison[TextComparison.IsNotEmpty] = 'IsNotEmpty';
TextComparison[TextComparison.IsNotNull] = 'IsNotNull';
TextComparison[TextComparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
TextComparison[TextComparison.IsNull] = 'IsNull';
TextComparison[TextComparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
TextComparison[TextComparison.NotEqualTo] = 'NotEqualTo';
TextComparison[TextComparison.StartsWith] = 'StartsWith';
/** @enum {number} */
const NumberComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
NumberComparison[NumberComparison.Between] = 'Between';
NumberComparison[NumberComparison.EqualTo] = 'EqualTo';
NumberComparison[NumberComparison.GreaterThan] = 'GreaterThan';
NumberComparison[NumberComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
NumberComparison[NumberComparison.LessThan] = 'LessThan';
NumberComparison[NumberComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
NumberComparison[NumberComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const BooleanComparison = {
    EqualTo: 12,
    NotEqualTo: 17,
};
BooleanComparison[BooleanComparison.EqualTo] = 'EqualTo';
BooleanComparison[BooleanComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const DateComparison = {
    Between: 16,
    EqualTo: 12,
    GreaterThan: 11,
    GreaterThanOrEqualTo: 10,
    LessThan: 2,
    LessThanOrEqualTo: 1,
    NotEqualTo: 17,
};
DateComparison[DateComparison.Between] = 'Between';
DateComparison[DateComparison.EqualTo] = 'EqualTo';
DateComparison[DateComparison.GreaterThan] = 'GreaterThan';
DateComparison[DateComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
DateComparison[DateComparison.LessThan] = 'LessThan';
DateComparison[DateComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
DateComparison[DateComparison.NotEqualTo] = 'NotEqualTo';
/** @enum {number} */
const ComparisonType = {
    Default: 1,
    Text: 2,
    Number: 3,
    Date: 4,
    Boolean: 5,
};
ComparisonType[ComparisonType.Default] = 'Default';
ComparisonType[ComparisonType.Text] = 'Text';
ComparisonType[ComparisonType.Number] = 'Number';
ComparisonType[ComparisonType.Date] = 'Date';
ComparisonType[ComparisonType.Boolean] = 'Boolean';
var ComparisonList;
(function (ComparisonList) {
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function values(comparisonType) {
        return Object.keys(this.getEnumAccordingToType(comparisonType)).filter((type) => isNaN((/** @type {?} */ (type))) && type !== 'values').sort();
    }
    ComparisonList.values = values;
    /**
     * @param {?=} comparisonType
     * @return {?}
     */
    function getEnumAccordingToType(comparisonType) {
        /** @type {?} */
        let enums;
        if (!comparisonType) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Default) {
            enums = DefaultComparison;
        }
        else if (comparisonType === ComparisonType.Text) {
            enums = TextComparison;
        }
        else if (comparisonType === ComparisonType.Number) {
            enums = NumberComparison;
        }
        else if (comparisonType === ComparisonType.Boolean) {
            enums = BooleanComparison;
        }
        else if (comparisonType === ComparisonType.Date) {
            enums = DateComparison;
        }
        return enums;
    }
    ComparisonList.getEnumAccordingToType = getEnumAccordingToType;
    /**
     * @param {?} comparisonType
     * @param {?=} key
     * @param {?=} label
     * @return {?}
     */
    function createKeyLabelArray(comparisonType, key = 'Id', label = 'Definition') {
        /** @type {?} */
        let enumKeys = this.values(comparisonType);
        /** @type {?} */
        let newKeyLabelArray = [];
        enumKeys.forEach((enumKey) => {
            /** @type {?} */
            let newValue = {};
            newValue[key] = this.getEnumAccordingToType(comparisonType)[enumKey];
            newValue[label] = enumKey;
            newKeyLabelArray.push(newValue);
        });
        return newKeyLabelArray;
    }
    ComparisonList.createKeyLabelArray = createKeyLabelArray;
})(ComparisonList || (ComparisonList = {}));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const Conditions = [
    {
        'name': 'Equal',
        'id': 0
    },
    {
        'name': 'LessThan',
        'id': 1
    },
    {
        'name': 'LessThanOrEqual',
        'id': 2
    },
    {
        'name': 'GreaterThan',
        'id': 3
    },
    {
        'name': 'GreaterThanOrEqual',
        'id': 4
    },
    {
        'name': 'NotEqual',
        'id': 5
    },
    {
        'name': 'Contains',
        'id': 6
    },
    {
        'name': 'StartsWith',
        'id': 7
    },
    {
        'name': 'EndsWith',
        'id': 8
    }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const FormButtonType = {
    Save: 1,
    Update: 2,
    Cancel: 3,
};
FormButtonType[FormButtonType.Save] = 'Save';
FormButtonType[FormButtonType.Update] = 'Update';
FormButtonType[FormButtonType.Cancel] = 'Cancel';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const LovColumnType = {
    Label: 'label',
    Text: 'text',
    Date: 'date',
    Select: 'select',
    Numeric: 'numeric',
    Boolean: 'boolean',
    Lov: 'lov',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const Module = {
    Auth: '/Auth/',
    SignIn: '/Auth/Token',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const OperationType = {
    None: 0,
    Created: 1,
    Updated: 2,
    Deleted: 3,
};
OperationType[OperationType.None] = 'None';
OperationType[OperationType.Created] = 'Created';
OperationType[OperationType.Updated] = 'Updated';
OperationType[OperationType.Deleted] = 'Deleted';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const SortType = {
    Asc: 0,
    Desc: 1,
    None: 2,
};
SortType[SortType.Asc] = 'Asc';
SortType[SortType.Desc] = 'Desc';
SortType[SortType.None] = 'None';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const FormType = {
    New: 'new',
    Edit: 'edit',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const MaskType = {
    Phone: 'phone',
    Mail: 'mail',
    PostCode: 'postCode',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const AlertDismissReason = {
    Cancel: 'cancel',
    Backdrop: 'backdrop',
    Close: 'close',
    Esc: 'esc',
    Timer: 'timer',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const AlertType = {
    Warning: 'warning',
    Error: 'error',
    Success: 'success',
    Info: 'info',
    Question: 'question',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const Connector = {
    And: 0,
    Or: 1,
};
Connector[Connector.And] = 'And';
Connector[Connector.Or] = 'Or';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ConfirmDialogOperationType = {
    Add: 'add',
    Update: 'update',
    Delete: 'delete',
    Undo: 'undo',
    Cancel: 'cancel',
    Refresh: 'refresh',
    Process: 'process',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ToastrMessages = {
    RecordDeleted: 'RecordDeleted',
    RecordAdded: 'RecordAdded',
    RecordUpdated: 'RecordUpdated',
    RecordSaved: 'RecordSaved',
    RecordReverted: 'RecordReverted',
    RecordCanceled: 'RecordCanceled',
    ValidationErrorMessage: 'ValidationErrorMessage',
    GridRefreshed: 'GridRefreshed',
    DateErrorMessage: 'DateErrorMessage',
    ActionCreated: 'ActionCreated',
    RecordNotFoundToDate: 'RecordNotFoundToDate',
    PasswordChangeMessage: 'PasswordChangeMessage',
    ResetPasswordMailMessage: 'ResetPasswordMailMessage',
    ResetPasswordMessage: 'ResetPasswordMessage',
    RelationRecordMessage: 'RelationRecordMessage',
    RelationValidationGroupMessage: 'RelationValidationGroupMessage',
    WorkDetailValidationErrorMessage: 'WorkDetailValidationErrorMessage',
    WorkSubtableValidationErrorMessage: 'WorkSubtableValidationErrorMessage',
    UsurnameAndPasswordMandatory: 'UsurnameAndPasswordMandatory',
    ChangePasswordNewPasswordAndConfirmPasswordMismatch: 'ChangePasswordNewPasswordAndConfirmPasswordMismatch',
    ChangePasswordOldPasswordAndConfirmPasswordSamewithold: 'ChangePasswordOldPasswordAndConfirmPasswordSamewithold',
    PackageNotAbleToChangeModule: 'PackageNotAbleToChangeModule',
    PackageNotAbleToChangeAuthType: 'PackageNotAbleToChangeAuthType',
    RoleNotAbleToChangeModule: 'RoleNotAbleToChangeModule',
    RoleNotAbleToChangePackage: 'RoleNotAbleToChangePackage',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const Storage = {
    AccessToken: 'access_token',
    RefreshToken: 'refresh_token',
    Expires: 'expires',
    User: 'user',
    SystemRefDate: 'system_ref_date',
    CurrentLanguage: 'current_language',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const Headers = {
    CoreId: 'X-CoreId',
    Behaviour: 'X-Behaviour',
    ReferenceDate: 'ReferenceDate',
    PagingHeader: 'X-Paging-Header',
    PagingResponseHeader: 'X-Paging-Response-Header',
    ReferenceDateHeader: 'X-ReferenceDate-Header',
    DisableLoading: 'disableGeneralLoading',
    DisableNavigation: 'disableNavigation',
    FileOperation: 'fileOperation',
    ContentType: 'Content-Type',
    Authentication: 'Authentication',
    BreadcrumbCustomLabel: 'BreadcrumbCustomLabel',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const StorageType = {
    LocalStorage: 'localStorage',
    SessionStorage: 'sessionStorage',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const DateSelectionMode = {
    Single: 'single',
    Multiple: 'multiple',
    Range: 'range',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BaseModuleUrl {
    /**
     * @param {?} url
     */
    constructor(url) {
        this.url = url;
        this.moduleUrl = url;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DateSettings {
    constructor() {
        this.disableUTC = false;
        this.showTimeOnly = false;
        // current year
        this.defaultYear = (new Date()).getFullYear();
        // 0 = january
        this.defaultMonth = 0;
        // 1
        this.defaultDay = 1;
        //'dd.mm.yy'
        this.dateFormat = 'dd.mm.yy';
        this.showYear = false;
        this.showTime = false;
        this.hideCalendarButton = false;
        //  today and clear buttons
        this.showButtonBar = true;
        this.showMonthPicker = false;
        this.enableSeconds = false;
        this.selectionMode = DateSelectionMode.Single;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Filter {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FilterGroup {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LovColumn {
    /**
     * visible parameter default is true
     * @param {?} name
     * @param {?} headerName
     * @param {?} type
     * @param {?=} visible
     * @param {?=} isKeyField
     */
    constructor(name, headerName, type, visible, isKeyField) {
        this.Name = name;
        this.HeaderName = headerName;
        this.Type = type;
        this.Visible = isNil(visible) ? true : visible;
        this.IsKeyField = isNil(isKeyField) ? false : isKeyField;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ConfirmDialogSettings {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GetListByResult {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class ServiceResult {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BaseModel {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SafePipe {
    /**
     * @param {?} sanitizer
     */
    constructor(sanitizer) {
        this.sanitizer = sanitizer;
    }
    /**
     * @param {?} value
     * @param {?} type
     * @return {?}
     */
    transform(value, type) {
        switch (type) {
            case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error(`Invalid safe type specified: ${type}`);
        }
    }
}
SafePipe.decorators = [
    { type: Pipe, args: [{
                name: 'safe'
            },] }
];
/** @nocollapse */
SafePipe.ctorParameters = () => [
    { type: DomSanitizer }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AppConfig {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
    }
    /**
     * @return {?}
     */
    load() {
        return new Promise((resolve, reject) => {
            /** @type {?} */
            const http = this.injector.get(HttpClient);
            http.get('/assets/config/config.json')
                .toPromise()
                .then((response) => {
                AppConfig.settings = (/** @type {?} */ (response));
                resolve();
            }).catch((response) => {
                console.log(response);
                reject('Could not load file /assets/config.json');
            });
        });
    }
}
AppConfig.settings = (/** @type {?} */ ({}));
AppConfig.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AppConfig.ctorParameters = () => [
    { type: Injector }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreModalService {
    constructor() {
        this.isModalDisplayed = new BehaviorSubject(false);
    }
    /**
     * @return {?}
     */
    getModalDisplayStatus() {
        return this.isModalDisplayed.asObservable();
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setModalDisplayStatus(status) {
        this.isModalDisplayed.next(status || false);
    }
}
CoreModalService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreModalService.ctorParameters = () => [];
/** @nocollapse */ CoreModalService.ngInjectableDef = defineInjectable({ factory: function CoreModalService_Factory() { return new CoreModalService(); }, token: CoreModalService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BreadcrumbService {
    constructor() {
        this.breadcrumbsWithCustomLabels = [];
    }
    /**
     * @param {?} label
     * @param {?} url
     * @param {?} customLabel
     * @return {?}
     */
    addCustomLabel(label, url, customLabel) {
        /** @type {?} */
        const indexOfBreadcrumbItem = this.breadcrumbsWithCustomLabels.findIndex(x => x.label === label && x.url === url);
        if (indexOfBreadcrumbItem !== -1) {
            this.breadcrumbsWithCustomLabels[indexOfBreadcrumbItem]['customLabel'] = customLabel;
        }
        else {
            /** @type {?} */
            const breadcrumb = {
                label,
                url,
                customLabel
            };
            this.breadcrumbsWithCustomLabels.push(breadcrumb);
        }
        // update the current list after send
        if (this.breadcrumbs) {
            /** @type {?} */
            const currentBreadcrumbItem = this.breadcrumbs.find(x => x.label === label && x.url === url);
            if (currentBreadcrumbItem) {
                currentBreadcrumbItem['customLabel'] = customLabel;
            }
        }
    }
}
BreadcrumbService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BreadcrumbService.ctorParameters = () => [];
/** @nocollapse */ BreadcrumbService.ngInjectableDef = defineInjectable({ factory: function BreadcrumbService_Factory() { return new BreadcrumbService(); }, token: BreadcrumbService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingService {
    constructor() {
        this.loadingRequests = [];
        this.loadingBarDisplayStatus = new BehaviorSubject(false);
    }
    /**
     * @return {?}
     */
    getLoadingBarDisplayStatus() {
        return this.loadingBarDisplayStatus.asObservable();
    }
    /**
     * @param {?} request
     * @return {?}
     */
    insertLoadingRequest(request) {
        if (!isNull(request)) {
            this.loadingRequests.push(request);
            this.checkLoadingDisplayStatus();
        }
    }
    /**
     * @param {?} request
     * @return {?}
     */
    removeLoadingRequest(request) {
        if (!isNull(request)) {
            /** @type {?} */
            const requestIndex = this.loadingRequests.indexOf(request);
            if (requestIndex !== -1) {
                this.loadingRequests.splice(requestIndex, 1);
            }
            this.checkLoadingDisplayStatus();
        }
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setLoadingBarSetting(status) {
        this.loadingBarDisplayStatus.next(status);
    }
    /**
     * @return {?}
     */
    checkLoadingDisplayStatus() {
        if (this.loadingRequests && this.loadingRequests.length > 0) {
            this.setLoadingBarSetting(true);
        }
        else {
            this.setLoadingBarSetting(false);
        }
    }
}
LoadingService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LoadingService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PreventUnsavedChangesService {
    constructor() {
        this.status = new Subject();
    }
    /**
     * @return {?}
     */
    getIsChanged() {
        return this.isChanged;
    }
    /**
     * @param {?} isChanged
     * @return {?}
     */
    setIsChanged(isChanged) {
        this.isChanged = isChanged;
    }
    /**
     * @return {?}
     */
    getStatus() {
        return this.status.asObservable();
    }
    /**
     * @param {?} status
     * @return {?}
     */
    setStatus(status) {
        this.status.next(status);
    }
}
PreventUnsavedChangesService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PreventUnsavedChangesService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StorageService {
    constructor() {
        this.onSubject = new Subject$1();
        this.changes = this.onSubject.asObservable().share();
        this.start();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.stop();
    }
    /**
     * @param {?=} storageType
     * @return {?}
     */
    getStorage(storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        /** @type {?} */
        let s = [];
        for (let i = 0; i < localStorage.length; i++) {
            s.push({
                key: storage === StorageType.LocalStorage ? localStorage.key(i) : sessionStorage.key(i),
                value: storage === StorageType.LocalStorage ?
                    JSON.parse(localStorage.getItem(localStorage.key(i))) : JSON.parse(localStorage.getItem(sessionStorage.key(i)))
            });
        }
        return s;
    }
    /**
     * @param {?} key
     * @param {?} data
     * @param {?=} storageType
     * @return {?}
     */
    store(key, data, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.setItem(key, JSON.stringify(data));
        }
        else {
            sessionStorage.setItem(key, JSON.stringify(data));
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: data });
    }
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    getStored(key, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        try {
            if (storage === StorageType.LocalStorage) {
                return JSON.parse(localStorage.getItem(key));
            }
            else {
                return JSON.parse(sessionStorage.getItem(key));
            }
        }
        catch (e) {
            if (storage === StorageType.LocalStorage) {
                return localStorage.getItem(key);
            }
            else {
                return sessionStorage.getItem(key);
            }
        }
    }
    /**
     * @param {?} key
     * @param {?=} storageType
     * @return {?}
     */
    clear(key, storageType) {
        /** @type {?} */
        let storage = storageType || StorageType.LocalStorage;
        if (storage === StorageType.LocalStorage) {
            localStorage.removeItem(key);
        }
        else {
            sessionStorage.removeItem(key);
        }
        // the local application doesn't seem to catch changes to localStorage...
        this.onSubject.next({ key: key, value: null });
    }
    /**
     * @private
     * @return {?}
     */
    start() {
        window.addEventListener('storage', this.storageEventListener.bind(this));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    storageEventListener(event) {
        if (event.storageArea == localStorage) {
            /** @type {?} */
            let v;
            try {
                v = JSON.parse(event.newValue);
            }
            catch (e) {
                v = event.newValue;
            }
            this.onSubject.next({ key: event.key, value: v });
        }
    }
    /**
     * @private
     * @return {?}
     */
    stop() {
        window.removeEventListener('storage', this.storageEventListener.bind(this));
        this.onSubject.complete();
    }
}
StorageService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
StorageService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TranslateHelperService {
    /**
     * @param {?} translate
     * @param {?} storageService
     */
    constructor(translate, storageService) {
        this.translate = translate;
        this.storageService = storageService;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    setInitialLanguage(lang) {
        this.translate.setDefaultLang(lang);
    }
    /**
     * @param {?} language
     * @return {?}
     */
    setSelectedLanguage(language) {
        this.translate.use(language);
        this.storageService.store(Storage.CurrentLanguage, language);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    instant(key) {
        /** @type {?} */
        const result = new Promise(resolve => {
            this.translate.get(key).toPromise();
        });
        return result;
    }
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return this.translate.get(key);
    }
}
TranslateHelperService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
TranslateHelperService.ctorParameters = () => [
    { type: TranslateService },
    { type: StorageService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SweetAlertService {
    /**
     * @param {?} translate
     */
    constructor(translate) {
        this.translate = translate;
        this._swal = Swal;
        // this.translate.setInitialLanguage('tr');
    }
    // openConfirmPopup() {
    //   const swalTitle = 'ConfirmDialogAreYouSure';
    //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
    //   const swalConfirmBtnText = 'ConfirmButtonText';
    //   const swalCancelBtnText = 'CancelModalButton';
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
    //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
    //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
    //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
    //   const swalCancelBtnText = 'CancelModalButton';
    //   const swalType = type || AlertType.Warning;
    //
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
    // }
    /**
     * @param {?=} title
     * @param {?=} text
     * @param {?=} type
     * @param {?=} confirmButtonText
     * @param {?=} showCancelButton
     * @return {?}
     */
    preventUnsavedChangedPopup(title, text, type, confirmButtonText, showCancelButton = true) {
        /** @type {?} */
        const swalTitle = title ? title : 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        const swalText = text ? text : 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning, showCancelButton);
    }
    /**
     * @param {?} confirmDialogOperationType
     * @param {?=} confirmDialogSettings
     * @return {?}
     */
    manageRequestOperationWithConfirm(confirmDialogOperationType, confirmDialogSettings) {
        switch (confirmDialogOperationType) {
            case ConfirmDialogOperationType.Add:
                return this.getAddOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Update:
                return this.getUpdateOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Delete:
                return this.getDeleteOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Undo:
                return this.getUndoOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Cancel:
                return this.getCancelOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Refresh:
                return this.getRefreshOperationConfirmPopupWithConfirm(confirmDialogSettings);
            case ConfirmDialogOperationType.Process:
                return this.getProcessOperationConfirmPopupWithConfirm(confirmDialogSettings);
            default:
                break;
        }
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getProcessOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogProcessCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogProcessInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogProcessOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogProcessCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getAddOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getUpdateOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getDeleteOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogDeleteRecordCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogDeleteRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogDeleteRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogDeleteRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getUndoOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogNotAbleToRevertRecordCapiton';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogNotAbleToRevertRecordInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogNotAbleToRevertRecordOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogNotAbleToRevertRecordCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getCancelOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogLoseChangesCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogLoseChangesInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogLoseChangesOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogLoseChangesCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @private
     * @param {?=} settings
     * @return {?}
     */
    getRefreshOperationConfirmPopupWithConfirm(settings = {}) {
        /** @type {?} */
        const swalTitle = settings.Title || 'ConfirmDialogRefreshCaption';
        /** @type {?} */
        const swalText = settings.Text || 'ConfirmDialogRefreshInfo';
        /** @type {?} */
        const swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogRefreshsOk';
        /** @type {?} */
        const swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogRefreshCancel';
        return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    }
    /**
     * @param {?} result
     * @param {?=} settings
     * @return {?}
     */
    confirmOperationPopup(result, settings = {}) {
        if (result.value) {
            /** @type {?} */
            const swalTitle = settings.title || 'Deleted!';
            /** @type {?} */
            const swalText = settings.text || 'Your file has been deleted.';
            /** @type {?} */
            const swalType = settings.type || AlertType.Success;
            return this.openCompletedConfirmDialog(swalTitle, swalText, swalType);
        }
        else if (result.dismiss === this._swal.DismissReason.cancel) {
            return this.openCompletedConfirmDialog('Cancelled', 'OperationCanceled', AlertType.Error);
        }
    }
    // loadingSweetAlert() {
    //   swal.showLoading();
    // }
    //
    // cancelLoadingSweetAlert() {
    //   swal.hideLoading();
    // }
    // cancelEditFormConfirmModal() {
    //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
    //   const swalText = 'ConfirmDialogLoseChangesInfo';
    //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
    //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
    //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
    // }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?=} swalType
     * @return {?}
     */
    openCompletedConfirmDialog(swalTitle, swalText, swalType = AlertType.Info) {
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalType = swalType || AlertType.Info;
        return this.translate.get([swalTitle, swalText])
            .pipe(flatMap(translatedResult => from(Swal.fire(translatedResult[swalTitle], translatedResult[swalText], swalType)), (translatedResult, result) => {
            /** @type {?} */
            const alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    }
    /**
     * @private
     * @param {?} swalTitle
     * @param {?} swalText
     * @param {?} swalConfirmBtnText
     * @param {?} swalCancelBtnText
     * @param {?} swalType
     * @param {?=} showCancelButton
     * @return {?}
     */
    openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton = true) {
        swalTitle = swalTitle || '';
        swalText = swalText || '';
        swalConfirmBtnText = swalConfirmBtnText || '';
        swalCancelBtnText = swalCancelBtnText || '';
        return this.translate.get([swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText])
            .pipe(flatMap(translatedResult => from(Swal.fire({
            title: translatedResult[swalTitle],
            text: translatedResult[swalText],
            type: swalType ? swalType : AlertType.Warning,
            confirmButtonText: translatedResult[swalConfirmBtnText],
            showCancelButton: showCancelButton,
            cancelButtonText: translatedResult[swalCancelBtnText],
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-info mr-20',
            cancelButtonClass: 'btn btn-danger',
            customClass: 'sweet-confirm-dialog',
        })), (translatedResult, result) => {
            /** @type {?} */
            const alertResult = {};
            if (result.value) {
                alertResult.value = result.value;
            }
            if (result.dismiss) {
                alertResult.dismiss = (/** @type {?} */ (result.dismiss.toString()));
            }
            return alertResult;
        }));
    }
}
SweetAlertService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SweetAlertService.ctorParameters = () => [
    { type: TranslateHelperService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SystemReferenceDateService {
    /**
     * @param {?} storageService
     */
    constructor(storageService) {
        this.storageService = storageService;
        this.systemDate = '';
        this.dateChange = new BehaviorSubject('');
    }
    /**
     * @param {?} newDate
     * @return {?}
     */
    setSelectedReferenceDate(newDate) {
        this.systemDate = newDate;
        this.storageService.store(Storage.SystemRefDate, newDate, StorageType.SessionStorage);
        this.dateChange.next(newDate);
    }
    /**
     * @return {?}
     */
    checkIsNullAndChangeCurrentDateWithStorage() {
        /** @type {?} */
        const systemDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (systemDate && systemDate !== this.systemDate) {
            this.systemDate = systemDate;
            this.dateChange.next(systemDate);
        }
        else if (!systemDate) {
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    getSelectedReferenceDate() {
        return this.dateChange.asObservable();
    }
    /**
     * @return {?}
     */
    getSystemDate() {
        return this.systemDate;
    }
    /**
     * @return {?}
     */
    getCurrentDateAsISO() {
        return this.convertDateToISO(new Date);
    }
    /**
     * @param {?} date
     * @return {?}
     */
    convertDateToISO(date) {
        /** @type {?} */
        const newDate = new Date(date);
        /** @type {?} */
        const year = newDate.getFullYear();
        /** @type {?} */
        let month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        let day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    }
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    convertDateToDateUTC(dateAsString) {
        /** @type {?} */
        const newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    }
}
SystemReferenceDateService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SystemReferenceDateService.ctorParameters = () => [
    { type: StorageService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// declare let toastr: any;
class ToastrUtilsService {
    /**
     * @param {?} toastr
     * @param {?} translate
     */
    constructor(toastr, translate) {
        this.toastr = toastr;
        this.translate = translate;
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    success(message, title) {
        this.translateValue(message, 'success', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    info(message, title) {
        this.translateValue(message, 'info', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    warning(message, title) {
        this.translateValue(message, 'warning', title);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @return {?}
     */
    error(message, title) {
        this.translateValue(message, 'error', title);
    }
    /**
     * @param {?} message
     * @param {?} messageType
     * @param {?=} title
     * @return {?}
     */
    translateValue(message, messageType, title) {
        title = title || '';
        message = message || '';
        this.translate.get([message, title]).subscribe((translatedResult) => {
            if (messageType === 'success') {
                this.toastr.success(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'info') {
                this.toastr.info(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'warning') {
                this.toastr.warning(translatedResult[message] || message, translatedResult[title] || title);
            }
            else if (messageType === 'error') {
                this.toastr.error(translatedResult[message] || message, translatedResult[title] || title);
            }
        }, (error) => {
        });
    }
}
ToastrUtilsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ToastrUtilsService.ctorParameters = () => [
    { type: ToastrService },
    { type: TranslateHelperService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ValidationService {
    /**
     * @param {?} control
     * @return {?}
     */
    static tcValidate(control) {
        /** @type {?} */
        let value = control.value;
        // event.target.value;
        /** @type {?} */
        let isZero = false;
        value = String(value);
        // Sadece rakamlardan oluşup oluşmadığını ve 11 haneli olup olmadığını kontrol ediyoruz.
        /** @type {?} */
        const isEleven = /^[0-9]{11}$/.test(value);
        // İlk rakamın 0 ile başlama kontrolü
        /** @type {?} */
        const firstValue = value.substr(0, 1);
        if (firstValue === '0') {
            isZero = true;
        }
        else {
            isZero = false;
        }
        /** @type {?} */
        let totalX = 0;
        for (let i = 0; i < 10; i++) {
            totalX += Number(value.substr(i, 1));
        }
        // İlk 10 hanesinin toplamınınn 11. haneyi verme kontrolü
        /** @type {?} */
        const isRuleX = String(totalX % 10) === value.substr(10, 1);
        // tek ve çift hanelerin toplamlarının atanacağı degişkenleri tanımlıyoruz
        /** @type {?} */
        let totalY1 = 0;
        /** @type {?} */
        let totalY2 = 0;
        // tek hanelerdeki sayıları toplar
        for (let j = 0; j < 10; j += 2) {
            totalY1 += Number(value.substr(j, 1));
        }
        // çift hanelerdeki sayıları toplar
        for (let k = 1; k < 9; k += 2) {
            totalY2 += Number(value.substr(k, 1));
        }
        /** @type {?} */
        const isRuleY = String(((totalY1 * 7) - totalY2) % 10) === value.substr(9, 1);
        /** @type {?} */
        const deger = isEleven && !isZero && isRuleX && isRuleY;
        // this.isTrue = deger === true ? true : false;
        if (!(deger === true ? true : false)) {
            return { 'identificationNumberIsNotValid': true };
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} control
     * @return {?}
     */
    static spaceControl(control) {
        /** @type {?} */
        let value = control.value;
        value = String(value);
        /** @type {?} */
        let usingSpace = false;
        if (value === '' || value.indexOf(' ') > -1) {
            usingSpace = true;
        }
        if (usingSpace) {
            return { 'usingSpaceValid': true };
        }
        else {
            return null;
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PreventUnsavedChangesGuard {
    /**
     * @param {?} sweetAlert
     * @param {?} preventUnsavedChangesService
     */
    constructor(sweetAlert, preventUnsavedChangesService) {
        this.sweetAlert = sweetAlert;
        this.preventUnsavedChangesService = preventUnsavedChangesService;
    }
    /**
     * @return {?}
     */
    canDeactivate() {
        try {
            setTimeout(() => {
                this.preventUnsavedChangesService.setStatus('waiting');
            }, 100);
            return this.preventUnsavedChangesService.getStatus()
                .pipe(concatMap((result) => {
                if (result === 'finished') {
                    /** @type {?} */
                    const isChanged = this.preventUnsavedChangesService.getIsChanged();
                    if (isChanged) {
                        return this.sweetAlert.preventUnsavedChangedPopup()
                            .pipe(map((alertResult) => {
                            // console.log('canDeactivate in pipe', alertResult);
                            if (alertResult.value) {
                                this.preventUnsavedChangesService.setIsChanged(false);
                                return true;
                            }
                            else {
                                return false;
                            }
                        }));
                    }
                    else {
                        this.preventUnsavedChangesService.setIsChanged(false);
                        return of(true);
                    }
                }
                // return result;
            }), catchError((error) => {
                console.error(error);
                return of(true);
            }));
        }
        catch (err) {
            console.error(err);
            return true;
        }
    }
}
PreventUnsavedChangesGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PreventUnsavedChangesGuard.ctorParameters = () => [
    { type: SweetAlertService },
    { type: PreventUnsavedChangesService }
];
/** @nocollapse */ PreventUnsavedChangesGuard.ngInjectableDef = defineInjectable({ factory: function PreventUnsavedChangesGuard_Factory() { return new PreventUnsavedChangesGuard(inject(SweetAlertService), inject(PreventUnsavedChangesService)); }, token: PreventUnsavedChangesGuard, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BaseHttpService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    httpGet(requestUrl = '', headers) {
        return this.httpClient.get(requestUrl, {
            headers: headers,
            observe: 'response'
        });
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    httpGetFile(requestUrl = '', headers) {
        return this.httpClient.get(requestUrl, {
            headers: headers,
            observe: 'response',
            responseType: 'blob'
        });
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    httpPost(requestUrl = '', data, headers) {
        /** @type {?} */
        let fileOperation;
        if (headers) {
            fileOperation = headers.get(Headers.FileOperation);
        }
        return this.httpClient.post(requestUrl, fileOperation ? data : JSON.stringify(data), {
            headers: headers
        });
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} data
     * @param {?=} headers
     * @return {?}
     */
    httpPatch(requestUrl = '', data, headers) {
        /** @type {?} */
        let fileOperation;
        if (headers) {
            fileOperation = headers.get(Headers.FileOperation);
        }
        return this.httpClient.patch(requestUrl, fileOperation ? data : JSON.stringify(data), {
            headers: headers
        });
    }
    /**
     * @template T
     * @param {?=} requestUrl
     * @param {?=} headers
     * @return {?}
     */
    httpDelete(requestUrl = '', headers) {
        return this.httpClient.delete(requestUrl, {
            headers: headers
        });
    }
    /**
     * @protected
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    getHttpHeader(filterQuery, headerParameters) {
        /** @type {?} */
        let httpHeaders = new HttpHeaders();
        if (filterQuery) {
            httpHeaders = httpHeaders.append('Charset', 'UTF-8');
            /** @type {?} */
            const newFilterQuery = JSON.parse(JSON.stringify(filterQuery));
            if (newFilterQuery && newFilterQuery.FilterGroups && newFilterQuery.FilterGroups.length > 0) {
                newFilterQuery.FilterGroups.forEach((filterGroup) => {
                    if (filterGroup.Filters && filterGroup.Filters.length > 0) {
                        filterGroup.Filters.forEach((filter$$1) => {
                            if (filter$$1.Value) {
                                filter$$1.Value = encodeURIComponent(filter$$1.Value);
                            }
                            if (filter$$1.OtherValue) {
                                filter$$1.OtherValue = encodeURIComponent(filter$$1.OtherValue);
                            }
                        });
                    }
                });
            }
            httpHeaders = httpHeaders.append(Headers.PagingHeader, JSON.stringify(newFilterQuery));
        }
        if (headerParameters) {
            headerParameters.forEach((headerParameter) => {
                if (headerParameter) {
                    httpHeaders = httpHeaders.append(headerParameter.header, headerParameter.value);
                }
            });
        }
        return httpHeaders;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BaseService extends BaseHttpService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        super(httpClient);
        this.httpClient = httpClient;
        this.endpoint = '';
        this.moduleUrl = '';
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    create(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPost(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    update(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpPatch(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    delete(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    deleteById(id, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getList(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getListBy(filterQuery, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/GetListBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    get(endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, null, headerParameters);
    }
    /**
     * @template T
     * @param {?} id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getById(id, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint;
        return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, null, headerParameters);
    }
    /**
     * @template T
     * @param {?} filterQuery
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getBy(filterQuery, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/GetBy';
        return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    bulkOperation(resource, endPointUrl, headerParameters) {
        endPointUrl = endPointUrl || this.endpoint + '/BulkOperation';
        return this.update(resource, endPointUrl, headerParameters)
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @private
     * @template T
     * @param {?} url
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    callHttpGet(url, filterQuery, headerParameters) {
        /** @type {?} */
        const isFileOPeration = this.isFileOPeration(headerParameters);
        if (isFileOPeration) {
            return this.httpGetFile(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map((response) => {
                return this.wrapHttpResponse(response);
            }));
        }
        else {
            return this.httpGet(url, this.getHttpHeader(filterQuery, headerParameters))
                .pipe(map((response) => {
                return this.wrapHttpResponse(response);
            }));
        }
    }
    /**
     * @private
     * @param {?=} headerParameters
     * @return {?}
     */
    isFileOPeration(headerParameters) {
        if (headerParameters) {
            /** @type {?} */
            const fileOPeration = headerParameters.find(x => x && x.header === Headers.FileOperation);
            return fileOPeration ? true : false;
        }
        else {
            return false;
        }
    }
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    wrapHttpResponse(response) {
        /** @type {?} */
        let pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        const responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        const coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    }
}
BaseService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
BaseService.ctorParameters = () => [
    { type: HttpClient }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DataService extends BaseHttpService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        super(httpClient);
        this.httpClient = httpClient;
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    create(resource, endPointUrl = '', headerParameters) {
        return this.httpPost(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} resource
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    update(resource, endPointUrl = '', headerParameters) {
        return this.httpPatch(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    delete(Id, endPointUrl = '', headerParameters) {
        return this.httpDelete(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    getList(endPointUrl = '', headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    getListBy(endPointUrl = '', filterQuery, headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?} Id
     * @param {?=} endPointUrl
     * @param {?=} headerParameters
     * @return {?}
     */
    get(Id, endPointUrl = '', headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @template T
     * @param {?=} endPointUrl
     * @param {?=} filterQuery
     * @param {?=} headerParameters
     * @return {?}
     */
    getBy(endPointUrl = '', filterQuery, headerParameters) {
        return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
            .pipe(map((response) => {
            return this.wrapHttpResponse(response);
        }));
    }
    /**
     * @private
     * @template T
     * @param {?} response
     * @return {?}
     */
    wrapHttpResponse(response) {
        /** @type {?} */
        let pagingResult;
        if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
            pagingResult = (/** @type {?} */ (JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
        }
        /** @type {?} */
        const responseBody = (/** @type {?} */ ((response.body || response)));
        /** @type {?} */
        const coreHttpResponse = {
            serviceResult: responseBody
        };
        if (pagingResult) {
            coreHttpResponse.pagingResult = pagingResult;
        }
        return coreHttpResponse;
    }
}
DataService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthenticationService extends BaseService {
    // @Inject(StorageService) private storageService: StorageService
    /**
     * @param {?} httpClient
     * @param {?} storageService
     * @param {?} router
     */
    constructor(httpClient, storageService, router) {
        super(httpClient);
        this.httpClient = httpClient;
        this.storageService = storageService;
        this.router = router;
        this.chagePasswordEndPoint = 'User/ChangePassword';
        this.changePasswordWithTokenEndPoint = 'User/ChangePasswordWithToken';
        this.isSuccess = new Subject();
        this.afterLoginUrl = '/dashboard';
        this.beforeLoginUrl = '/login';
        this.jwtHelperService = new JwtHelperService();
    }
    /**
     * @param {?} credentials
     * @param {?=} disableNavigation
     * @return {?}
     */
    login(credentials, disableNavigation) {
        /** @type {?} */
        const headerParameters = [];
        if (disableNavigation) {
            headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
        }
        /** @type {?} */
        const data = {
            grant_type: 'password',
            username: credentials.username,
            password: credentials.password,
            client_id: AppConfig.settings.aad.clientId
        };
        return this.create(data, Module.SignIn, headerParameters)
            .pipe(map((response) => {
            this.handdleTokenResponse(response);
        }));
    }
    /**
     * @param {?} response
     * @return {?}
     */
    handdleTokenResponse(response) {
        if (response && response.serviceResult.IsSuccess) {
            this.storageService.store(Storage.AccessToken, response.serviceResult.Result.access_token.token);
            this.storageService.store(Storage.RefreshToken, response.serviceResult.Result.refresh_token);
            /** @type {?} */
            const decodedToken = this.jwtHelperService.decodeToken(response.serviceResult.Result.access_token.token);
            /** @type {?} */
            const expirationDate = this.jwtHelperService.getTokenExpirationDate(response.serviceResult.Result.access_token.token);
            this.storageService.store(Storage.Expires, expirationDate);
            /** @type {?} */
            const userData = {
                username: decodedToken.sub,
                employeeId: decodedToken.employee_id,
                id: decodedToken.id
            };
            this.setUserAfterToken(userData);
        }
    }
    /**
     * @param {?=} disableNavigation
     * @param {?=} disableLoading
     * @return {?}
     */
    refreshToken(disableNavigation, disableLoading) {
        /** @type {?} */
        const headerParameters = [];
        /** @type {?} */
        const token = this.storageService.getStored(Storage.RefreshToken);
        if (disableNavigation) {
            headerParameters.push({ header: Headers.DisableNavigation, value: Headers.DisableNavigation });
        }
        if (disableLoading) {
            headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
        }
        /** @type {?} */
        const data = {
            grant_type: 'refresh_token',
            client_id: AppConfig.settings.aad.clientId,
            refresh_token: token
        };
        return this.create(data, Module.SignIn, headerParameters)
            .pipe(tap((response) => {
            this.handdleTokenResponse(response);
        }));
    }
    /**
     * @return {?}
     */
    loggedIn() {
        /** @type {?} */
        const token = this.storageService.getStored(Storage.AccessToken);
        if (token) {
            /** @type {?} */
            const isExpired = this.jwtHelperService.isTokenExpired(token);
            return !isExpired;
        }
        else {
            this.cleanLocalStorage();
            return false;
        }
    }
    /**
     * @return {?}
     */
    getTimeoutMiliseconds() {
        /** @type {?} */
        const expires = this.storageService.getStored(Storage.Expires);
        return moment(expires).valueOf() - moment().valueOf() - 120000;
    }
    /**
     * @return {?}
     */
    getAuthToken() {
        return this.storageService.getStored(Storage.AccessToken);
    }
    /**
     * @return {?}
     */
    cleanLocalStorage() {
        this.storageService.clear(Storage.AccessToken);
        this.storageService.clear(Storage.RefreshToken);
        this.storageService.clear(Storage.Expires);
        this.storageService.clear(Storage.User);
        this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
    }
    /**
     * @param {?} userData
     * @return {?}
     */
    setUserAfterToken(userData) {
        this.storageService.store(Storage.User, userData);
    }
    /**
     * @return {?}
     */
    getUserDataLoggedIn() {
        return this.storageService.getStored(Storage.User);
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    forgotPassword(formValue) {
        /** @type {?} */
        const email = formValue.email;
        /** @type {?} */
        const isLocked = formValue.isLocked;
        this.httpClient.get(AppConfig.settings.env.apiUrl + Module.Auth + 'User/ForgotPassword?email=' + email + '&isLocked=' + isLocked)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
                // token alınacak
            }
        });
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    changePassword(formValue) {
        this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.chagePasswordEndPoint, formValue)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
            }
        });
    }
    /**
     * @param {?} formValue
     * @return {?}
     */
    changePasswordWithToken(formValue) {
        this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.changePasswordWithTokenEndPoint, formValue)
            .subscribe((res) => {
            if (res['IsSuccess']) {
                this.isSuccess.next(res['IsSuccess']);
            }
        });
    }
    /**
     * @param {?=} disableNavigation
     * @return {?}
     */
    logout(disableNavigation) {
        // disableNavigation it is for session-expired
        this.storageService.clear(Storage.AccessToken);
        this.storageService.clear(Storage.RefreshToken);
        this.storageService.clear(Storage.Expires);
        this.storageService.clear(Storage.User);
        if (!disableNavigation) {
            // it makes to nagivate signin when cleared in master.app.ts
            this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
            this.router.navigate([this.beforeLoginUrl]);
        }
    }
}
AuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationService.ctorParameters = () => [
    { type: HttpClient },
    { type: StorageService },
    { type: Router }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RequestInterceptor {
    /**
     * @param {?} injector
     * @param {?} loadingService
     * @param {?} storageService
     * @param {?} router
     * @param {?} toastr
     */
    constructor(injector, loadingService, storageService, router, toastr) {
        this.injector = injector;
        this.loadingService = loadingService;
        this.storageService = storageService;
        this.router = router;
        this.toastr = toastr;
        this.isRefreshingToken = false;
        this.tokenSubject = new BehaviorSubject$1(null);
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        this.authenticationService = this.injector.get(AuthenticationService);
        this.loadingService = this.injector.get(LoadingService);
        // it is for disabling general loading
        /** @type {?} */
        let disableGeneralLoading;
        // it is for session-expired to avoid navitaion
        /** @type {?} */
        let disableNavigation;
        // resim gönderme de content type eklemin diye
        /** @type {?} */
        let fileOperation;
        // breadcrumba custom label gönderme
        /** @type {?} */
        let isBreadcrumbCustomLabel;
        /** @type {?} */
        let breadcrumbCustomLabel;
        // controls the header if it exist
        if (request.headers.has(Headers.DisableLoading)) {
            disableGeneralLoading = true;
            request = request.clone({ headers: request.headers.delete(Headers.DisableLoading) });
        }
        // controls the header if it exist
        if (request.headers.has(Headers.DisableNavigation)) {
            disableNavigation = true;
            request = request.clone({ headers: request.headers.delete(Headers.DisableNavigation) });
        }
        // controls the header if it exist
        if (request.headers.has(Headers.FileOperation)) {
            fileOperation = true;
            request = request.clone({ headers: request.headers.delete(Headers.FileOperation) });
        }
        // disabling loading when getting en tr
        if (request.url.indexOf('assets/i18n') !== -1) {
            disableGeneralLoading = true;
        }
        // controls the header if it exist
        if (request.headers.has(Headers.BreadcrumbCustomLabel)) {
            this.breadcrumbService = this.injector.get(BreadcrumbService);
            isBreadcrumbCustomLabel = true;
            breadcrumbCustomLabel = request.headers.getAll(Headers.BreadcrumbCustomLabel);
            request = request.clone({ headers: request.headers.delete(Headers.BreadcrumbCustomLabel) });
        }
        // if it is not set true then send request url to loading service
        if (!disableGeneralLoading) {
            this.loadingService.insertLoadingRequest(request.url);
        }
        if (!request.headers.has(Headers.ContentType) && !fileOperation) {
            request = request.clone({ headers: request.headers.set(Headers.ContentType, 'application/json') });
        }
        request = request.clone({
            headers: request.headers.set('Accept', 'application/json')
        });
        /** @type {?} */
        const refDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        request = request.clone({
            headers: request.headers.set(Headers.ReferenceDateHeader, refDate || new Date().toISOString())
        });
        return next.handle(this.addTokenToRequest(request, this.authenticationService.getAuthToken()))
            .pipe(map((event) => {
            if (event instanceof HttpResponse) {
                /** @type {?} */
                const response = (/** @type {?} */ (event));
                if (response && response.body) {
                    if (response.body.hasOwnProperty('IsSuccess') && !response.body.IsSuccess) {
                        /** @type {?} */
                        let errorMessage = '0001: An Error Occurred';
                        if (response.body.ErrorMessage instanceof Array) {
                            errorMessage = (/** @type {?} */ (response.body.ErrorMessage));
                            errorMessage.forEach((message) => {
                                this.toastr.error(message);
                            });
                        }
                        else {
                            errorMessage = (/** @type {?} */ (response.body.ErrorMessage));
                            this.toastr.error(response.body.ErrorMessage);
                        }
                        return throwError(errorMessage);
                    }
                    else if (isBreadcrumbCustomLabel) {
                        this.handdleResponseForBreadcrumb(response.body, breadcrumbCustomLabel);
                    }
                }
            }
            return event;
        }), catchError((err) => {
            if (!disableGeneralLoading) {
                this.loadingService.removeLoadingRequest(request.url);
            }
            if (err instanceof HttpErrorResponse) {
                switch (((/** @type {?} */ (err))).status) {
                    case 0:
                        this.showError(err);
                        break;
                    case 401:
                        return this.handle401Error(request, next, err);
                    case 400:
                        return this.handle400Error(err, disableNavigation);
                    case 500:
                        this.showError(err);
                        break;
                }
            }
            return throwError(err);
        }), finalize(() => {
            if (!disableGeneralLoading) {
                this.loadingService.removeLoadingRequest(request.url);
            }
        }));
    }
    /**
     * @private
     * @param {?} response
     * @param {?} breadcrumbCustomLabel
     * @return {?}
     */
    handdleResponseForBreadcrumb(response, breadcrumbCustomLabel) {
        if (response.IsSuccess && response.Result && this.breadcrumbService) {
            // came from headers
            /** @type {?} */
            let breadcrumbLabels = [];
            // resposne data
            /** @type {?} */
            let data;
            /** @type {?} */
            let generatedCustomLabel = '';
            // checkh array or string and convert into array
            if (breadcrumbCustomLabel) {
                if (breadcrumbCustomLabel instanceof Array) {
                    if (breadcrumbCustomLabel.indexOf(Headers.BreadcrumbCustomLabel) === -1) {
                        breadcrumbLabels = breadcrumbCustomLabel;
                    }
                }
                else if (breadcrumbCustomLabel !== Headers.BreadcrumbCustomLabel) {
                    breadcrumbLabels.push(breadcrumbCustomLabel);
                }
            }
            // check result is array, and convert into single data object
            if (response.Result instanceof Array) {
                if (response.Result.length > 0) {
                    data = response.Result[0];
                }
            }
            else {
                data = response.Result;
            }
            if (data) {
                if (breadcrumbLabels && breadcrumbLabels.length > 0) {
                    // use header value
                    breadcrumbLabels.forEach(element => {
                        if (data[element]) {
                            generatedCustomLabel += data[element] + ' ';
                        }
                    });
                }
                else if (data.hasOwnProperty('Definition') && data.Definition) {
                    // use default label
                    generatedCustomLabel = data.Definition;
                }
                else if (data.hasOwnProperty('Description') && data.Description) {
                    // use default label
                    generatedCustomLabel = data.Description;
                }
            }
            /** @type {?} */
            const splitedUrl = this.router.url.split('/');
            if (splitedUrl && generatedCustomLabel !== '') {
                this.breadcrumbService.addCustomLabel(splitedUrl[splitedUrl.length - 1], this.router.url, generatedCustomLabel);
            }
        }
    }
    /**
     * @private
     * @param {?} request
     * @param {?} token
     * @return {?}
     */
    addTokenToRequest(request, token) {
        // it is for not adding token to header when login or refresh token
        if (token && request.url.lastIndexOf('Token') === -1) {
            return request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
        }
        else {
            return request;
        }
    }
    /**
     * @param {?} error
     * @param {?=} disableNavigation
     * @return {?}
     */
    handle400Error(error, disableNavigation) {
        this.showError(error);
        this.authenticationService.logout(disableNavigation);
        return throwError(error);
    }
    /**
     * @private
     * @param {?} request
     * @param {?} next
     * @param {?} error
     * @return {?}
     */
    handle401Error(request, next, error) {
        /** @type {?} */
        const authToken = this.authenticationService.getAuthToken();
        // TOken var mı kontrolü yoksa login yapıyordur
        if (authToken) {
            if (!this.isRefreshingToken) {
                this.isRefreshingToken = true;
                // Reset here so that the following requests wait until the token
                // comes back from the refreshToken call.
                this.tokenSubject.next(null);
                return this.authenticationService.refreshToken()
                    .pipe(switchMap((response) => {
                    if (response) {
                        this.authenticationService.handdleTokenResponse(response);
                        /** @type {?} */
                        const token = this.authenticationService.getAuthToken();
                        this.tokenSubject.next(token);
                        return next.handle(this.addTokenToRequest(request, token));
                    }
                    else {
                        return throwError(error);
                    }
                }), catchError(err => {
                    this.showError(err);
                    this.authenticationService.logout();
                    return throwError(err);
                }), finalize(() => {
                    this.isRefreshingToken = false;
                }));
            }
            else {
                this.isRefreshingToken = false;
                return this.tokenSubject
                    .pipe(filter(token => token != null), take(1), switchMap(token => {
                    if (token) {
                        return next.handle(this.addTokenToRequest(request, token));
                    }
                    else {
                        this.authenticationService.logout();
                        return throwError(error);
                    }
                }));
            }
        }
        else {
            this.showError(error);
            this.authenticationService.logout();
            return throwError(error);
        }
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    showError(error) {
        if (error && error.error && error.error.ErrorMessage) {
            this.toastr.error(error.error.ErrorMessage);
        }
        else if (error && error.message) {
            this.toastr.error(error.message);
        }
    }
}
RequestInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
RequestInterceptor.ctorParameters = () => [
    { type: Injector },
    { type: LoadingService },
    { type: StorageService },
    { type: Router },
    { type: ToastrUtilsService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CommandCellComponent {
    constructor() {
        this.showRowDeleteButton = true;
        this.showRowEditButton = true;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
        this.editable = this.params.context.componentParent.gridOptions.editable;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        /** @type {?} */
        const buttonSettings = this.params.context.componentParent.gridOptions.buttonSettings;
        if (buttonSettings) {
            if (buttonSettings.showRowDeleteButton !== undefined) {
                this.showRowDeleteButton = buttonSettings.showRowDeleteButton;
            }
            if (buttonSettings.showEditButton !== undefined) {
                this.showRowEditButton = buttonSettings.showEditButton;
            }
        }
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
    /**
     * @return {?}
     */
    showUndoButton() {
        return this.params &&
            this.params.data &&
            this.params.data.OperationType !== OperationType.Created &&
            this.editable &&
            (!this.formGroup.pristine || this.params.data.OperationType === OperationType.Updated);
    }
    /**
     * @return {?}
     */
    removeRow() {
        this.params.context.componentParent.removeRow(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    deleteRecord() {
        this.params.context.componentParent.deleteRecord(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    editRecord() {
        this.params.context.componentParent.editRecord(this.params, this.rowId, this.key);
    }
    /**
     * @return {?}
     */
    undoRecord() {
        this.params.context.componentParent.undoRecord(this.params, this.rowId, this.key);
    }
}
CommandCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-command-cell',
                template: `
    <div *ngIf="formGroup" style="margin-top: 4px;display: flex;" >
      <span  class="command-cell-item">
      <button *ngIf="showUndoButton()"  type="button" style="height: 20px" (click)="undoRecord()" class="pull-right btn-grid-inline btn-grid-edit">
      <core-icon icon="undo"></core-icon>
      </button>

				<button *ngIf="!editable && showRowDeleteButton"  type="button" style="height: 20px" (click)="deleteRecord()" class="pull-right btn-grid-inline btn-grid-delete">
          <core-icon icon="trash-alt"></core-icon>
				</button>

				<button *ngIf="editable && showRowDeleteButton"  type="button" style="height: 20px" (click)="removeRow()" class="pull-right btn-grid-inline btn-grid-delete">
          <core-icon icon="trash-alt"></core-icon>
				</button>

				<button *ngIf="!editable && showRowEditButton"  type="button" style="height: 20px" (click)="editRecord()" class="pull-right btn-grid-inline btn-grid-edit">
        <core-icon icon="edit"></core-icon>
        </button>
      </span>
    </div>
  `,
                styles: [".command-cell-item{margin-left:auto;margin-right:auto}.btn-grid-inline{color:#166dba!important;background:0 0!important;border:none!important;cursor:pointer;font-size:16px}.btn-grid-inline:focus{outline:0}.btn-grid-delete:hover{color:#e30a3a!important}.btn-grid-edit:hover{color:#17a2b8!important}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            textFilterValue: TextComparison.Contains
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        // this.dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.column.colDef.columnType === CoreGridColumnType.TextSelect) {
                            if (params.serviceAccess) {
                                params.serviceAccess.textSelectFindValuesOnFilter(params.field, params.column.colDef.cellEditorParams.settings, this.buildModel());
                            }
                        }
                        else {
                            if (params.serviceAccess && params.serviceAccess.filter) {
                                params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                            }
                            this.params.onFloatingFilterChanged({ model: this.buildModel() });
                            this.prevValue = this.currentValue;
                        }
                    }
                    else {
                        this.currentValue = this.prevValue || null;
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        /** @type {?} */
        let value;
        /** @type {?} */
        const type = this.formGroup.get('textFilterValue').value;
        if (this.currentValue) {
            value = this.currentValue;
        }
        else if (type === TextComparison.IsNullOrWhiteSpace ||
            type === TextComparison.IsNotNullNorWhiteSpace ||
            type === TextComparison.IsNull ||
            type === TextComparison.IsNotNull ||
            type === TextComparison.IsNotEmpty ||
            type === TextComparison.IsEmpty) {
            value = ' ';
        }
        return {
            filterType: 'string',
            type: type.toString(),
            filter: value
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Text).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
TextFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-text-filter-cell',
                template: `
    <div class="input-group">
      <layout-form-text-input style="width: 100%;"
          [(ngModel)]="currentValue"
          [clearable] = "true"
          placeholder="{{filterKey|translate}}"
          (ngModelChange)="valueChanged($event)">
      </layout-form-text-input>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
          <layout-static-selector formControlName="textFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>
  `
            }] }
];
/** @nocollapse */
TextFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            this.settings = this.params.column.getColDef().cellEditorParams.settings;
            this.getListData();
        }
        this.dataSubscription = this.params.column.getColDef().cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.column.getColDef().cellEditorParams.dataSource) {
            this.listData = this.params.column['colDef'].cellEditorParams.dataSource;
            // this.clearSubscription();
        }
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.currentValue = newValue && newValue[this.settings.valueField] ? newValue[this.settings.valueField] : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.settings && ((/** @type {?} */ (this.settings))).sameFieldWith ?
                            ((/** @type {?} */ (this.settings))).sameFieldWith : this.params.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.currentValue = null;
                    this.formGroup.patchValue({
                        filterValue: this.prevValue || (this.prevValue && parseInt(this.prevValue, 10) === 0) ? this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            /** @type {?} */
            const newValue = {};
            newValue[this.settings.valueField] = this.currentValue;
            this.valueChanged(newValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    clearSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearSubscription();
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
SelectFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-filter-cell',
                template: `
    <div class="input-group">
      <div *ngIf="settings" [formGroup]="formGroup" style="width:100%;    padding-top: 4px;">
        <layout-static-selector
          #selector
          formControlName="filterValue"
          (changed)="valueChanged($event)"
          [dataSource]="listData"
          placeholder="{{filterKey|translate}}"
          [valueField]="this.settings.valueField"
          [labelField]="this.settings.labelField"
        >
        </layout-static-selector>
      </div>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="selectorFormGroup" [formGroup]="selectorFormGroup" style="width:100%;">
          <layout-static-selector formControlName="selectFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>

  `,
                styles: [":host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container .ng-placeholder{font-weight:400;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#6c7591}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
            }] }
];
/** @nocollapse */
SelectFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DateFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.selectionMode = DateSelectionMode.Single;
        this.settings = new DateSettings();
        this.formGroup = this.fb.group({
            dateFilterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectorDateFilterValue: DateComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column) {
            /** @type {?} */
            const column = this.params.column;
            if (column.colDef && column.colDef.cellEditorParams && column.colDef.cellEditorParams.settings) {
                this.settings = Object.assign((/** @type {?} */ ({})), column.colDef.cellEditorParams.settings);
            }
        }
        this.settings.hideCalendarButton = true;
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    /** @type {?} */
                    const selectedDateComparison = this.selectorFormGroup.get('selectorDateFilterValue').value;
                    if (selectedDateComparison === DateComparison.Between) {
                        this.selectionMode = DateSelectionMode.Range;
                    }
                    else {
                        this.selectionMode = DateSelectionMode.Single;
                    }
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.params.field, this.buildModel(newValue));
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel(newValue) });
                }
                else {
                    this.formGroup.patchValue({
                        dateFilterValue: null
                    });
                }
            });
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id) {
            this.valueChanged(this.formGroup.get('dateFilterValue').value);
        }
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    buildModel(newValue) {
        /** @type {?} */
        const model = {
            filterType: 'string',
            type: this.selectorFormGroup.get('selectorDateFilterValue').value,
            filter: null,
            other: null
        };
        if (newValue instanceof Array) {
            model.filter = newValue[0];
            model.other = newValue[1];
        }
        else {
            model.filter = newValue;
        }
        return model;
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Date).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Date);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
DateFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-date-filter-cell',
                template: `

  <div class="input-group">
    <div *ngIf="formGroup"
      [formGroup]="formGroup"
      class="input-group" style="width:100%;">
      <layout-form-datepicker-input style="width:100%;"
      formControlName="dateFilterValue"
      [settings]= "settings"
      [selectionMode]="selectionMode"
      placeholder="{{filterKey|translate}}"
      (dateChanges)= "valueChanged($event)"></layout-form-datepicker-input>
    </div>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="selectorFormGroup" [formGroup]="selectorFormGroup" style="width:100%;">
        <layout-static-selector formControlName="selectorDateFilterValue"
        (changed)="changed($event)"
        [dataSource]="dataSource"
        placeholder="{{placeholder |translate}} {{filterKey|translate}}"
        [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>

  `,
                styles: [":host /deep/ .custom-ng-select{min-width:auto!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ui-calendar .ui-inputtext{margin-top:6px}"]
            }] }
];
/** @nocollapse */
DateFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NumericFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.field = params.field;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        if (params.column
            && params.column.colDef
            && params.column.colDef.cellEditorParams
            && params.column.colDef.cellEditorParams.settings
            && params.column.colDef.cellEditorParams.settings.fields
            && params.column.colDef.cellEditorParams.settings.fields.length > 0) {
            this.field = head(params.column.colDef.cellEditorParams.settings.fields);
        }
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000), distinctUntilChanged(), // https://rxjs-dev.firebaseapp.com/api/operators/distinctUntilChanged
        flatMap(filterValueResult => from(params.serviceAccess.callUndoConfirmAlert()), (filterValueResult, confirmrResult) => {
            return { filterValueResult, confirmrResult };
        }))
            .subscribe(result => {
            if (result.confirmrResult.value) {
                this.currentValue = result.filterValueResult;
                if (params.serviceAccess && params.serviceAccess.filter) {
                    params.serviceAccess.applyChangesToFilter(this.field, this.buildModel());
                }
                this.params.onFloatingFilterChanged({ model: this.buildModel() });
                this.prevValue = this.currentValue;
            }
            else {
                this.canceledValue = result.filterValueResult;
                this.currentValue = this.prevValue || this.prevValue === 0 ? this.prevValue : null;
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
        if (this.canceledValue === newValue) {
            this.onFilter();
        }
    }
    // fastest solution copy paste from top
    /**
     * @return {?}
     */
    onFilter() {
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.canceledValue = this.currentValue;
                    this.currentValue = this.prevValue || this.prevValue === 0 ? this.prevValue : null;
                }
            });
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'number',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue || (this.currentValue === 0) ?
                (this.params.customFilterFunction ? this.params.customFilterFunction(this.currentValue.toString()) : this.currentValue.toString()) : null
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
NumericFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-numeric-filter-cell',
                template: `
  <div class="input-group">
    <layout-form-number-input style="width: 100%;"
        [(ngModel)]="currentValue"
        [clearable] = "true"
        placeholder="{{filterKey|translate}}"
        (ngModelChange)="valueChanged($event)">
    </layout-form-number-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
NumericFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BooleanFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.listData = [];
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            booleanFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.listData.push({ Id: 'true', Definition: 'True' });
        this.listData.push({ Id: 'false', Definition: 'False' });
    }
    /**
     * @return {?}
     */
    get model() {
        return this._model;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set model(val) {
        this._model = val;
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        if (newValue) {
            this.currentValue = newValue.Id;
        }
        else {
            this.currentValue = null;
        }
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.params.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.model;
                }
                else {
                    this.model = this.prevValue || null;
                    this.currentValue = this.model;
                }
            });
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        if (!parentModel) {
            this.currentValue = null;
        }
        else {
            this.currentValue = parentModel.filter;
        }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id) {
            this.valueChanged({ Id: this.currentValue });
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.formGroup.get('booleanFilterValue').value,
            filter: this.currentValue === null || this.currentValue === undefined ? null : this.currentValue.toString()
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
BooleanFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-boolean-filter-cell',
                template: `
    <div class="input-group">
      <div style="width:100%;">
      <ng-select class="custom-ng-select"
      [items]="listData"
      [id]="key"
      bindLabel="Definition"
      [(ngModel)]="model"
      placeholder="{{filterKey|translate}}"
      (change)="valueChanged($event)"
      bindValue="Id" appendTo="body">
      </ng-select>
      </div>

      <div class="grid-filter-icon">
       <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
          <layout-static-selector formControlName="booleanFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>
  `,
                styles: [".custom-ng-select{height:30px!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important;padding-top:2px}:host /deep/ .ng-select-container .ng-has-value{padding-top:8px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
            }] }
];
/** @nocollapse */
BooleanFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CurrencyFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                        }
                        this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    }
                    else {
                        this.currentValue = null;
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
CurrencyFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-currency-filter-cell',
                template: `
  <div class="input-group">
    <layout-form-currency-input style="width: 100%;"
        [(ngModel)]="currentValue"
        clearable = "true"
        placeholder="{{filterKey|translate}}"
        (ngModelChange)="valueChanged($event)">
    </layout-form-currency-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
CurrencyFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ListOfValuesService {
    /**
     * @param {?} dataService
     */
    constructor(dataService) {
        this.dataService = dataService;
        this.lovServiceResultStateChanged = new Subject();
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    setLoadUrlSetting(urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    }
    /**
     * @param {?} expression
     * @param {?=} headerParameters
     * @return {?}
     */
    loadDataByFilterAndPageSetting(expression, headerParameters) {
        /** @type {?} */
        const reqUrl = `${this.moduleUrl}${this.endPointUrl}`;
        this.dataService.getListBy(reqUrl, expression, headerParameters)
            .subscribe((httpResponse) => {
            this.pagingResult = httpResponse.pagingResult;
            this.lovData = httpResponse.serviceResult.Result;
            this.lovServiceResultStateChanged.next(this.lovData);
        });
    }
}
ListOfValuesService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ListOfValuesService.ctorParameters = () => [
    { type: DataService }
];
/** @nocollapse */ ListOfValuesService.ngInjectableDef = defineInjectable({ factory: function ListOfValuesService_Factory() { return new ListOfValuesService(inject(DataService)); }, token: ListOfValuesService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ListOfValuesModalComponent {
    constructor() {
        this.getSelectedRow = new EventEmitter();
    }
    /**
     * @param {?} component
     * @return {?}
     */
    set content(component) {
        // console.error('setContent', component);
        if (component) {
            this.coreGrid = component;
            this.gridOptions = this.displayData.gridOptions;
            component.load();
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.displayData && this.displayData && this.displayData.gridOptions) {
            this.gridOptions = this.displayData.gridOptions;
        }
    }
    /**
     * @return {?}
     */
    save() {
        this.getSelectedRow.emit(this.coreGrid.getSelectedRow());
        this.displayData = { display: false };
    }
    /**
     * @return {?}
     */
    close() {
        this.displayData = { display: false };
    }
    /**
     * @param {?} selectedRows
     * @return {?}
     */
    selectedChanged(selectedRows) {
        this.getSelectedRow.emit(head(selectedRows));
        this.displayData = { display: false };
    }
    /**
     * @return {?}
     */
    gridReload() {
        this.coreGrid.refreshGridDataSource();
    }
}
ListOfValuesModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-list-of-values-modal',
                template: "<core-modal [displayData]=\"displayData\" [draggable]=\"false\" [showHeader]=\"false\" [closeOnEscape]=\"false\"\r\n  [closable]=\"false\" [width]=\"65\">\r\n  <layout-core-grid *ngIf=\"displayData && displayData.display\" #coreGrid [gridOptions]=\"gridOptions\"\r\n    [isInsideModal]=\"true\" (doubleClickSelectedChanged)=\"selectedChanged($event)\">\r\n  </layout-core-grid>\r\n  <div class=\"col-sm-12 prl-0 mt-10\">\r\n    <button class=\"btn btn-danger ml-10 fl-r\" type=\"button\" (click)=\"close()\">\r\n      <core-icon icon=\"times\"></core-icon> {{'CancelButton'|translate}}\r\n    </button>\r\n\r\n    <button type=\"button\" class=\"btn btn-success ml-10 fl-r\" (click)=\"save()\">\r\n      <core-icon icon=\"check\"></core-icon> {{'ChooseButton' | translate}}\r\n    </button>\r\n  </div>\r\n</core-modal>",
                styles: [".table-row-selection{cursor:pointer}.modal-pager{padding:20px 0!important;height:16px!important;display:flex;align-items:center;overflow:hidden;font-size:14px;justify-content:space-between;margin-bottom:10px}.modal-pagination{padding:0;margin-top:6px;margin-right:10px;float:left}.modal-static-layout-selector{margin-top:7px;float:left;margin-right:20px}"]
            }] }
];
/** @nocollapse */
ListOfValuesModalComponent.ctorParameters = () => [];
ListOfValuesModalComponent.propDecorators = {
    displayData: [{ type: Input }],
    getSelectedRow: [{ type: Output }],
    content: [{ type: ViewChild, args: ['coreGrid',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ListOfValuesComponent {
    /**
     * @param {?} listOfValuesService
     */
    constructor(listOfValuesService) {
        this.listOfValuesService = listOfValuesService;
        this.isReadOnly = false;
        // @ContentChild(FormTextInputComponent) formTextInputComponent;
        this.lovValueChanged = new EventEmitter();
        this.addToDataSource = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.listOfValuesService.lovServiceResultStateChanged
            .subscribe((result) => {
            this.initialData = result;
            this.setLovFields();
        });
    }
    /**
     * @return {?}
     */
    openModal() {
        this.displayData = {
            display: true,
            gridOptions: this.gridOptions
        };
    }
    /**
     * @param {?} selectedModel
     * @return {?}
     */
    getSelectedRow(selectedModel) {
        if (selectedModel) {
            this.KeyColumnValue = selectedModel[this.gridOptions.keyField];
            this.value = selectedModel[this.valueColumnName];
            this.label = '';
            if (this.labelColumnNames instanceof Array) {
                /** @type {?} */
                const labels = (/** @type {?} */ (this.labelColumnNames));
                labels.forEach((columnName) => {
                    this.label = this.label + ' ' + selectedModel[columnName];
                });
            }
            else if (this.labelColumnNames) {
                this.label = selectedModel[this.labelColumnNames.toString()];
            }
            // it will send the grid edit cell
            this.addToDataSource.emit({
                Id: this.listOfValuesService.endPointUrl,
                Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
            });
            this.hasSelectedItem = true;
            this.lovValueChanged.emit({ id: this.KeyColumnValue, value: this.value, label: this.label });
        }
    }
    /**
     * @return {?}
     */
    gridReload() {
        this.lovModal.gridReload();
    }
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @param {?=} dontInitData
     * @return {?}
     */
    setOptions(gridOptions, valueColumnName = 'Id', labelColumnNames = 'Definition', dontInitData) {
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
        if (!dontInitData && this.keyColumnValue) {
            this.initData();
        }
        else if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
        }
    }
    /**
     * @param {?} gridOptions
     * @param {?=} initData
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    setGridOptions(gridOptions, initData, valueColumnName, labelColumnNames) {
        this.gridOptions = gridOptions;
        if (gridOptions) {
            this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
            if (initData) {
                this.initData();
            }
        }
        if (valueColumnName) {
            this.valueColumnName = valueColumnName;
        }
        if (labelColumnNames) {
            this.labelColumnNames = labelColumnNames;
        }
    }
    /**
     * @param {?} gridOptions
     * @param {?=} valueColumnName
     * @param {?=} labelColumnNames
     * @return {?}
     */
    setOptionsWithNoRequest(gridOptions, valueColumnName = 'Id', labelColumnNames = 'Definition') {
        this.gridOptions = gridOptions;
        this.valueColumnName = valueColumnName;
        this.labelColumnNames = labelColumnNames;
    }
    /**
     * @return {?}
     */
    initData() {
        if (this.gridOptions && this.keyColumnValue && this.keyColumnValue !== -1) {
            // console.log('initData - gridOptions', this.gridOptions);
            // console.log('initData - dataSource', this.dataSource);
            // console.log('initData - keyColumnValue', this.keyColumnValue);
            if (this.dataSource) {
                /** @type {?} */
                const selectedRowData = this.dataSource.find(x => x.Id === this.gridOptions.requestOptions.UrlOptions.endPointUrl
                    && x.Value.id === this.keyColumnValue);
                // console.log('initData - valueColumnName', this.valueColumnName);
                // console.log('initData - keyColumnValue', this.keyColumnValue);
                // console.log('initData - selectedRowData', selectedRowData);
                if (selectedRowData) {
                    setTimeout(() => {
                        this.label = selectedRowData.Value.label;
                        this.value = selectedRowData.Value.value;
                    }, 10);
                    return;
                }
            }
            if (this.gridOptions.requestOptions && this.gridOptions.requestOptions.UrlOptions) {
                this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                this.fieldIniData = this.gridOptions.keyField;
                if (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined) {
                    if (this.fieldIniData.indexOf('Id') > 0) {
                        this.fieldIniData = this.fieldIniData.substr(0, this.fieldIniData.indexOf('Id')) + '.' + this.fieldIniData.substr(this.fieldIniData.indexOf('Id'), this.fieldIniData.length);
                    }
                }
                this.genericExpression = {
                    FilterGroups: [
                        {
                            Filters: [
                                { PropertyName: this.fieldIniData, Value: this.keyColumnValue, Comparison: TextComparison.EqualTo }
                            ]
                        }
                    ],
                    PageSize: 1,
                    PageNumber: 1
                };
                if (this.gridOptions.requestOptions.CustomFilter) {
                    if (this.gridOptions.requestOptions.CustomFilter.FilterGroups) {
                        this.gridOptions.requestOptions.CustomFilter.FilterGroups.forEach(item => {
                            this.genericExpression.FilterGroups.push(item);
                        });
                    }
                }
                this.listOfValuesService.loadDataByFilterAndPageSetting(this.genericExpression, this.gridOptions.requestOptions.HeaderParameters);
            }
        }
    }
    /**
     * @param {?=} disableSetToKeyColumnValue
     * @return {?}
     */
    clearLov(disableSetToKeyColumnValue) {
        if (!disableSetToKeyColumnValue) {
            this.KeyColumnValue = null;
        }
        else {
            this.keyColumnValue = null;
        }
        this.value = null;
        this.label = null;
        this.hasSelectedItem = false;
        // if (this.formTextInputComponent) {
        //   this.formTextInputComponent.inputValue = null;
        // }
        if (!disableSetToKeyColumnValue) {
            this.lovValueChanged.emit(null);
        }
    }
    /**
     * @return {?}
     */
    showClearButton() {
        return !isNil(this.KeyColumnValue);
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get KeyColumnValue() {
        return this.keyColumnValue;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set KeyColumnValue(val) {
        this.keyColumnValue = val;
        this.setLovFields();
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        // console.log('writeValue1', value);
        // console.log('writeValue2', this.keyColumnValue);
        if (value) {
            if (value !== this.keyColumnValue) {
                this.keyColumnValue = value;
                this.initData();
            }
        }
        else {
            this.keyColumnValue = value;
            this.clearLov(true);
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @return {?}
     */
    setLovFields() {
        if (!isNil(this.keyColumnValue) && !isNil(this.gridOptions)) {
            /** @type {?} */
            const keyColumn = this.gridOptions.columns.find((item) => item.field === this.gridOptions.keyField);
            if (!isNil(this.initialData) && keyColumn && keyColumn.field) {
                /** @type {?} */
                const filteredObject = this.initialData.find((item) => item[keyColumn.field] === this.keyColumnValue);
                if (!isNil(filteredObject)) {
                    this.value = filteredObject[this.valueColumnName];
                    this.label = '';
                    this.freeText = '';
                    if (this.labelColumnNames instanceof Array) {
                        /** @type {?} */
                        const labels = (/** @type {?} */ (this.labelColumnNames));
                        labels.forEach((columnName) => {
                            this.label = this.label + ' ' + filteredObject[columnName];
                            this.freeText = this.freeText + ' ' + filteredObject[columnName];
                        });
                    }
                    else if (this.labelColumnNames) {
                        this.label = filteredObject[this.labelColumnNames.toString()];
                        this.freeText = filteredObject[this.labelColumnNames.toString()];
                    }
                    // it will send the grid edit cell
                    this.addToDataSource.emit({
                        Id: this.listOfValuesService.endPointUrl,
                        Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
                    });
                    this.hasSelectedItem = true;
                }
            }
        }
    }
    /**
     * @return {?}
     */
    getInputFiledWidth() {
        if (!this.isReadOnly) {
            if (this.showClearButton()) {
                return 'calc(60% - 76px)';
            }
            else {
                return 'calc(60% - 36px)';
            }
        }
        else {
            return 'calc(60% )';
        }
    }
}
ListOfValuesComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-list-of-values',
                template: "<div class=\"form-lov\">\r\n  <layout-form-text-input [isReadOnly]=\"true\" [(ngModel)]=\"KeyColumnValue\" [hidden]=\"true\"></layout-form-text-input>\r\n  <div class=\"row no-gutters\">\r\n    <div class=\"pr-2\" style=\"width: 40%;\">\r\n      <layout-form-text-input [class]=\"isReadOnly ? '' : 'showLikeEditable'\" [isReadOnly]=\"true\" [(ngModel)]=\"value\">\r\n      </layout-form-text-input>\r\n    </div>\r\n    <div [class.pr-2]=\"!isReadOnly\" [ngStyle]=\"{'width': getInputFiledWidth()}\">\r\n      <layout-form-text-input *ngIf=\"!inputRef.innerHTML.trim() || (inputRef.innerHTML.trim() && hasSelectedItem)\"\r\n        [isReadOnly]=\"true\" [placeholder]=\"placeholder\" [class]=\"isReadOnly ? '' : 'showLikeEditable'\"\r\n        [(ngModel)]=\"label\"></layout-form-text-input>\r\n      <div #inputRef [hidden]=\"hasSelectedItem\">\r\n        <ng-content select=\"layout-form-text-input\"></ng-content>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"!isReadOnly\" class=\"lov-btn-container\" [ngStyle]=\"{'width': showClearButton() ? '76px' : '36px'}\">\r\n      <button type=\"button\" class=\"btn btn-info\" title=\" {{'Search' | translate}}\" (click)=\"openModal()\">\r\n        <core-icon icon=\"search\"></core-icon>\r\n      </button>\r\n      <button type=\"button\" [hidden]=\"!showClearButton()\" class=\"btn btn-danger ml-1\" title=\" {{'Clear' | translate}}\"\r\n        (click)=\"clearLov()\" style=\"float:right;\">\r\n        <core-icon icon=\"times\"></core-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<layout-list-of-values-modal #lovModal [displayData]=\"displayData\" (getSelectedRow)=\"getSelectedRow($event)\">\r\n</layout-list-of-values-modal>",
                encapsulation: ViewEncapsulation.None,
                providers: [
                    ListOfValuesService,
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => ListOfValuesComponent),
                        multi: true
                    }
                ],
                styles: [".showLikeEditable>input{background-color:#fff!important}"]
            }] }
];
/** @nocollapse */
ListOfValuesComponent.ctorParameters = () => [
    { type: ListOfValuesService }
];
ListOfValuesComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    dataSource: [{ type: Input }],
    lovValueChanged: [{ type: Output }],
    addToDataSource: [{ type: Output }],
    lovModal: [{ type: ViewChild, args: ['lovModal',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LovFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            filterValue: null
        });
        this.selectorFormGroup = this.fb.group({
            selectFilterValue: DefaultComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.hideFilter = this.params.hideFilter;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
            }
            else {
                console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
        }
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.settings) {
                this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField);
            }
        }
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.currentValue = newValue && newValue.id ? newValue.id : null;
        if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
            this.params.serviceAccess.callUndoConfirmAlert()
                .subscribe((result) => {
                if (result.value) {
                    if (this.params.serviceAccess && this.params.serviceAccess.filter) {
                        this.params.serviceAccess.applyChangesToFilter(this.params.field, this.buildModel());
                    }
                    this.params.onFloatingFilterChanged({ model: this.buildModel() });
                    this.prevValue = this.currentValue;
                }
                else {
                    this.currentValue = null;
                    this.formGroup.patchValue({
                        filterValue: this.prevValue || (this.prevValue && parseInt(this.prevValue, 10) === 0) ? this.prevValue : null
                    });
                }
            });
        }
        else {
            this.prevValue = this.currentValue;
        }
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && (data.Id || data.Id === 0) && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.selectorFormGroup.get('selectFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Default).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Default);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
LovFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-lov-filter-cell',
                template: `
    <div [hidden]="hideFilter" class="input-group">
      <div [formGroup]="formGroup" style="width: calc(100%);">
        <layout-list-of-values #listOfValue
        [id]="key"
        formControlName="filterValue"
        placeholder="{{filterKey|translate}}"
        (lovValueChanged)="valueChanged($event)"></layout-list-of-values>
      </div>

      <div class="grid-filter-icon">
        <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
      </div>

      <p-overlayPanel #op appendTo="body" dismissable="false">
        <div *ngIf="selectorFormGroup" [formGroup]="selectorFormGroup" style="width:100%;">
          <layout-static-selector
          formControlName="selectFilterValue"
          (changed)="changed($event)"
          [dataSource]="dataSource"
          placeholder="{{filterKey|translate}}"
          [clearable]="false"
          [searchable]="false">
          </layout-static-selector>
        </div>
      </p-overlayPanel>
    </div>

  `,
                styles: [":host /deep/ .form-lov .btn-info{background-color:#175169!important;border-color:#175169!important}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
            }] }
];
/** @nocollapse */
LovFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
LovFilterCellComponent.propDecorators = {
    listOfValue: [{ type: ViewChild, args: ['listOfValue',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MaskFilterCellComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.modelChanged = new Subject();
        this.dataSource = [];
        this.filterKey = 'GridFilter';
        this.formGroup = this.fb.group({
            numberFilterValue: NumberComparison.EqualTo
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
        this.initiateComparisonList();
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.initiateComparisonList();
            }
        });
        if (this.params.column.getColDef().cellEditorParams) {
            if (this.params.column.getColDef().cellEditorParams.settings) {
                this.settings = this.params.column.getColDef().cellEditorParams.settings;
                this.maskType = this.settings.maskType;
                this.maxLength = this.settings.maxLength;
            }
        }
        this.modelChangedSubscription = this.modelChanged.pipe(debounceTime(1000)).subscribe(model => {
            this.currentValue = model;
            if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                params.serviceAccess.callUndoConfirmAlert()
                    .subscribe((result) => {
                    if (result.value) {
                        if (params.serviceAccess && params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(params.field, this.buildModel());
                        }
                        this.params.onFloatingFilterChanged({ model: this.buildModel() });
                        this.prevValue = this.currentValue;
                    }
                    else {
                        this.currentValue = this.prevValue || '';
                    }
                });
            }
        });
    }
    /**
     * @param {?} newValue
     * @return {?}
     */
    valueChanged(newValue) {
        this.modelChanged.next(newValue);
    }
    /**
     * @param {?} parentModel
     * @return {?}
     */
    onParentModelChanged(parentModel) {
        // *****will be done when needed*****
        // if (!parentModel) {
        //   this.currentValue = null;
        // } else {
        //   this.currentValue = parentModel.filter;
        // }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    changed(data) {
        if (data && data.Id && this.currentValue) {
            this.valueChanged(this.currentValue);
        }
    }
    /**
     * @return {?}
     */
    buildModel() {
        return {
            filterType: 'text',
            type: this.formGroup.get('numberFilterValue').value,
            // can be send in here
            filter: this.currentValue
        };
    }
    /**
     * @return {?}
     */
    initiateComparisonList() {
        /** @type {?} */
        const keyList = ComparisonList.createKeyLabelArray(ComparisonType.Number).map(x => x.Definition);
        /** @type {?} */
        const dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Number);
        this.translate.get(keyList)
            .subscribe((result) => {
            /** @type {?} */
            let counter = 0;
            keyList.forEach(key => {
                /** @type {?} */
                const translatedValue = result[key];
                dataSource[counter].Definition = translatedValue;
                counter++;
            });
            this.dataSource = dataSource;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.modelChangedSubscription) {
            this.modelChangedSubscription.unsubscribe();
        }
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
    }
}
MaskFilterCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-mask-filter-cell',
                template: `
  <div class="input-group">
  <layout-form-mask-input style="width: 100%;"
    [maskType]="maskType"
    [(ngModel)]="currentValue"
    [clearable] = "true"
    placeholder="{{filterKey|translate}}"
    (ngModelChange)="valueChanged($event)">
  </layout-form-mask-input>

    <div class="grid-filter-icon">
      <core-icon #actualTarget icon="filter" (click)="op.toggle($event)"></core-icon>
    </div>

    <p-overlayPanel #op appendTo="body" dismissable="false">
      <div *ngIf="formGroup" [formGroup]="formGroup" style="width:100%;">
        <layout-static-selector formControlName="numberFilterValue" (changed)="changed($event)" [dataSource]="dataSource" [clearable]="false" [searchable]="false">
        </layout-static-selector>
      </div>
    </p-overlayPanel>
  </div>
  `
            }] }
];
/** @nocollapse */
MaskFilterCellComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params
            && this.params.colDef
            && this.params.colDef.cellEditorParams
            && this.params.colDef.cellEditorParams.settings
            && this.params.colDef.cellEditorParams.settings.sameFieldWith) {
            // this.value = this.params.node.data[this.params.colDef.cellEditorParams.settings.sameFieldWith];
            /** @type {?} */
            const fields = this.params.colDef.cellEditorParams.settings.sameFieldWith.split('.');
            /** @type {?} */
            let data = Object.assign({}, this.params.node.data);
            fields.forEach((field) => {
                data = data[field] || null;
            });
            this.value = data;
        }
        // this.params.node.data["PagePanel"]["PageId"]
        // 58
        // this.params.node.data["PagePanel.PageId"]
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
        }
        return true;
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.colDef.cellEditorParams.dataSource) {
            this.listData = this.params.colDef.cellEditorParams.dataSource;
            // this.clearSubscription();
            /** @type {?} */
            const data = this.listData.find(x => x[this.params.colDef.cellEditorParams.settings.valueField] === this.value);
            if (data) {
                this.definition = '';
                if (this.params.colDef.cellEditorParams.settings.labelField instanceof Array) {
                    /** @type {?} */
                    const labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.labelField));
                    labels.forEach((columnName) => {
                        this.definition = this.definition + ' ' + data[columnName];
                    });
                }
                else if (this.params.colDef.cellEditorParams.settings.labelField) {
                    this.definition = data[this.params.colDef.cellEditorParams.settings.labelField];
                }
            }
            if (this.definition) {
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.definition);
                }
            }
        }
    }
    /**
     * @return {?}
     */
    clearSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearSubscription();
    }
}
SelectCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-cell',
                template: `
  <div *ngIf="definition" class="grid-none-editable-select-cell user-select-text text-ellipsis"  title="{{definition}}">
    {{ definition }}
  </div>
  `,
                styles: [`
      .grid-none-editable-select-cell {
        padding-top: 5px;
        white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
      }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LabelCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this._value = this.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.value = '';
                if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                    /** @type {?} */
                    const labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.fields));
                    labels.forEach((columnName) => {
                        this.value = this.value + ' ' + this.params.node.data[columnName];
                    });
                }
            }
        }
        this._value = this.value;
        if (this.params.colDef.cellEditorParams.customDisplayFunction) {
            this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
}
LabelCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-label-cell',
                template: `
    <div *ngIf="value" class="grid-none-editable-label-cell user-select-text text-ellipsis" title="{{_value}}">
      {{ _value }}
    </div>
  `,
                styles: [`
    .grid-none-editable-label-cell {
      padding-top: 5px;
      white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DateCellComponent {
    constructor() {
        this.defaultDateFormat = 'DD.MM.YYYY';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        if (this.params && this.params.colDef && this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
        }
        if (this.settings) {
            if (this.settings.showYear) {
                this.defaultDateFormat = 'YYYY';
            }
            if (this.settings.showTimeOnly) {
                this.defaultDateFormat = 'HH:mm';
            }
            if (this.settings.showTime) {
                this.defaultDateFormat = 'DD.MM.YYYY HH:mm';
            }
            if (this.settings.dateFormat) {
                this.defaultDateFormat = this.settings.dateFormat;
            }
        }
        if (this.params.value) {
            this.value = moment(this.params.value).format(this.defaultDateFormat);
        }
        else {
            this.value = null;
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.value = this.params.value;
        if (this.defaultDateFormat && this.value) {
            this.value = moment(this.value).format(this.defaultDateFormat);
        }
        return true;
    }
}
DateCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-date-cell',
                template: `
      <div *ngIf="value" class="grid-none-editable-date-cell user-select-text text-ellipsis">
        {{ value }}
      </div>
  `,
                styles: [`
      .grid-none-editable-date-cell {
        padding-top: 5px;
      }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BooleanCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        this.formGroup.at(this.key).disable({ onlySelf: true });
        this.formGroup.at(this.key).patchValue(this.value);
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
}
BooleanCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-boolean-cell',
                template: `
    <div *ngIf="formGroup" [formGroup]="formGroup" class="grid-boolean-container">
		<layout-form-checkbox [formControl]="formGroup.controls[key]" isReadOnly="true"></layout-form-checkbox>
    </div>
  `,
                styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CurrencyCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
}
CurrencyCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-currency-cell',
                template: `
    <div *ngIf="formGroup" [formGroup]="formGroup">
      <layout-form-currency-input
        [formControlName]="key"
        class="coreGridLabelCellDisable align-middle"
        [isReadOnly]="true">
      </layout-form-currency-input>
    </div>
  `,
                styles: [`
      .coreGridLabelCellDisable /deep/ input {
        background: rgba(0, 0, 0, 0);
        border: none;
        width: 100%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MaskCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.maskType = this.settings.maskType;
            }
            else {
                console.error(this.params.colDef.field + ' settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' settings is not defined');
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
}
MaskCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-mask-cell',
                template: `
    <div *ngIf="formGroup" [formGroup]="formGroup">
      <layout-form-mask-input
        class="coreGridLabelCellDisable align-middle"
        [formControlName]="key"
        [maskType]="maskType"
        [isReadOnly]="true">
      </layout-form-mask-input>
    </div>
  `,
                styles: [`
      .coreGridLabelCellDisable /deep/ input {
        background: rgba(0, 0, 0, 0);
        border: none;
        width: 100%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NumericCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this._value = this.value;
        this.rowId = this.params.node.id;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        // done for experimental,
        // use case is below
        // {
        // field: 'Department',
        // columnType: CoreGridColumnType.Numeric,
        // settings: {
        //   fields: ['DepartmentId'] //for this reason
        // },
        // headerName: 'DepartmentHistoryDepartmentId'
        // },
        // {
        //   field: 'DepartmentId',
        //   columnType: CoreGridColumnType.Lov,
        //   visible: true,
        //   settings: {
        //     options: this.departmentHistoryLovGridOptions,
        //     valueField: 'DepartmentId',
        //     labelField: ['Definition']
        //   }
        // }
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                    /** @type {?} */
                    const labels = (/** @type {?} */ (this.params.colDef.cellEditorParams.settings.fields));
                    /** @type {?} */
                    let value = '';
                    labels.forEach((columnName) => {
                        if (this.params.node.data[columnName]) {
                            value += this.params.node.data[columnName] + ' ';
                        }
                    });
                    if (value !== '') {
                        this.value = value;
                    }
                }
            }
        }
        this._value = this.value;
        if (this.params.colDef.cellEditorParams.customDisplayFunction) {
            this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
        }
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
}
NumericCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-numeric-cell',
                template: `
  <div *ngIf="value || value === 0" class="grid-none-editable-numeric-cell user-select-text text-ellipsis" title="{{_value}}">
    {{ _value }}
  </div>
  `,
                styles: [`
    .grid-none-editable-numeric-cell {
      padding-top: 5px;
      white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    }
    `]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value.toString() : '';
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
TextEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-text-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-form-text-input
        [class.not-valid]="!isValid()"
        [formControlName]="key"
        [isReadOnly]="isReadOnly"
        (changed)="onValueChanged()">
      </layout-form-text-input>
    </div>
  `,
                styles: [""]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        this.gridService = this.params.context.componentParent;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
            this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
        }
        this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(x => {
            this.getListData();
        });
    }
    /**
     * @return {?}
     */
    createListeningSubscription() {
        if (this.filterByField) {
            // clear if suscription has
            this.clearfilterByFieldSubscription();
            // if formgroup not exist, get formgroup
            if (!this.formGroup) {
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
            }
            if (this.formGroup) {
                // get key in order to track changes
                /** @type {?} */
                const columnKeyToListen = this.getFilterByFieldKey();
                // for initial value fill according to that
                /** @type {?} */
                const formgroupForKeyToListen = this.formGroup.at(columnKeyToListen);
                if (formgroupForKeyToListen && (formgroupForKeyToListen.value || formgroupForKeyToListen.value === 0)) {
                    this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                        .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                }
                // listen that cloumn changes at that field
                this.filterByFieldSubscription = this.formGroup.at(columnKeyToListen).valueChanges
                    .subscribe((data) => {
                    if (!data) {
                        // if the data set to null in that column, then set selft to null
                        this.setValue(null);
                        // clear list because data comes according to this data value
                        this.listData = [];
                    }
                    else {
                        // clear list in order to assign new ones
                        this.listData = [];
                        // fill the list according to value at that column
                        this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                            .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                        // look the current selected id, if it is exist in new listData,
                        /** @type {?} */
                        const foundData = this.listData.find(x => x[this.settings.valueField] === this.formGroup.at(this.key).value);
                        if (!foundData) {
                            // if it is not clear data of select
                            this.setValue(null);
                            // somehow lisData broken, in order to avoid this, I re assigned that
                            this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                                .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
                        }
                    }
                });
            }
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        if (this.params.colDef.cellEditorParams) {
            this.getListData();
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            else {
                console.error(this.params.colDef.field + ' lov settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' data source or lov settings is not defined');
        }
        return true;
    }
    /**
     * @return {?}
     */
    getListData() {
        if (this.params.colDef.cellEditorParams.dataSource) {
            // get filterByField
            if (this.params.colDef.cellEditorParams.settings) {
                this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
            }
            if (!this.filterByField) {
                // if filterByField not exist, then assign orginal data source
                this.listData = this.params.colDef.cellEditorParams.dataSource;
            }
            else {
                // if exist, get the key, then filter according to that
                /** @type {?} */
                const columnKeyToListen = this.getFilterByFieldKey();
                this.listData = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)))
                    .filter(x => x[this.filterByField] === this.formGroup.at(columnKeyToListen).value);
            }
            this.clearDataSubscription();
            this.createListeningSubscription();
        }
    }
    /**
     * @return {?}
     */
    getFilterByFieldKey() {
        // get find Column according to  filterByField
        /** @type {?} */
        const column = this.gridService.allGridColumns.find(x => x.colDef.field === this.filterByField);
        // createKey with that column in order to use for tracking
        /** @type {?} */
        const columnKeyToListen = this.params.context.createKey(this.params.columnApi, column);
        return columnKeyToListen;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onValueChanged(data) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.value = value;
        this.formGroup.at(this.key).patchValue(value);
        this.params.setValue(value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
    /**
     * @return {?}
     */
    clearDataSubscription() {
        if (this.dataSubscription) {
            this.dataSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    clearfilterByFieldSubscription() {
        if (this.filterByFieldSubscription) {
            this.filterByFieldSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearDataSubscription();
        this.clearfilterByFieldSubscription();
    }
}
SelectEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-select-edit-cell',
                template: `
    <div
      *ngIf="formGroup && this.settings && this.settings.valueField"
      [formGroup]="formGroup">
      <layout-static-selector
        #selector
				[class.not-valid]="!isValid()"
        [formControlName]="key"
        (changed)="onValueChanged($event)"
        [dataSource]="listData"
        [isReadOnly]="isReadOnly"
        [valueField]="this.settings.valueField"
        [labelField]="this.settings.labelField"
      >
      </layout-static-selector>
    </div>
  `,
                styles: [":host /deep/ .custom-ng-select{margin-top:2px}:host /deep/ .not-valid .custom-ng-select .ng-select-container{border:1px solid red}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:1px}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NumericEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
NumericEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-numeric-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-form-number-input
        [class.not-valid]="!isValid()"
        [formControlName]="key"
        [isReadOnly]="isReadOnly"
        (changed)="onValueChanged()">
      </layout-form-number-input>
    </div>
  `,
                styles: [""]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LovEditCellComponent {
    /**
     * @param {?} component
     * @return {?}
     */
    set content(component) {
        if (component) {
            this.setLovComponent(component);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // console.log(params);
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? +this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        this.dataSource = ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource)));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        // console.log(params);
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
            }
            else {
                console.error(this.params.colDef.field + 'lov Setting is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + 'lov Setting is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        // console.log('patch-value', this.value);
        if (this.formGroup && this.params.colDef.cellEditorParams.customFunctionForRowValueChanges) {
            this.clearRowChangesSubscription();
            this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
            setTimeout(() => {
            });
            this.rowChangesSubscription = this.formGroup.valueChanges
                .subscribe((rowData) => {
                this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
            });
        }
        return true;
    }
    /**
     * @param {?} component
     * @return {?}
     */
    setLovComponent(component) {
        this.listOfValue = component;
        if (this.settings) {
            this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField, this.settings.dontInitData);
        }
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    lovValueChanged(event) {
        this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        // console.log('lovValueChanged', this.params);
        if (event && this.listOfValue) {
            ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push({
                Id: this.listOfValue.listOfValuesService.endPointUrl,
                Value: event
            });
            this.dataSource = this.params.colDef.cellEditorParams.dataSource;
            // console.log('dataSource', this.dataSource);
        }
        this.params.setValue(this.value);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onAddToDataSource(data) {
        if (!this.params.colDef.cellEditorParams.dataSource) {
            this.params.colDef.cellEditorParams.dataSource = [];
        }
        ((/** @type {?} */ (this.params.colDef.cellEditorParams.dataSource))).push(data);
        this.dataSource = this.params.colDef.cellEditorParams.dataSource;
    }
    /**
     * @return {?}
     */
    clearRowChangesSubscription() {
        if (this.rowChangesSubscription) {
            this.rowChangesSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.clearRowChangesSubscription();
    }
}
LovEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-lov-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-list-of-values
      [class.not-valid]="!isValid()"
      #listOfValue
      [dataSource]="dataSource"
      [isReadOnly]="isReadOnly"
      [formControlName]="key"
      (lovValueChanged)="lovValueChanged($event)"
      (addToDataSource)="onAddToDataSource($event)"
      ></layout-list-of-values>
    </div>
  `,
                styles: [".not-valid input{border:1px solid red}:host /deep/ .lov-btn-container{margin-top:3px}"]
            }] }
];
LovEditCellComponent.propDecorators = {
    content: [{ type: ViewChild, args: ['listOfValue',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DateEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = params.context.createKey(this.params.columnApi, this.params.column);
        this.value = this.params.value ? this.params.value : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
        if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
            this.settings = this.params.colDef.cellEditorParams.settings;
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @param {?} date
     * @return {?}
     */
    onDateChanges(date) {
        this.value = this.formGroup.at(this.key).value ? this.formGroup.at(this.key).value : null;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
DateEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-date-edit-cell',
                template: `
    <div *ngIf="formGroup"
      [formGroup]="formGroup"
      class="input-group" style="width:100%; height:30px;margin-top:2px;">
      <layout-form-datepicker-input style="width:100%;"
			[class.not-valid]="!isValid()"
      [formControlName]="key"
      [isReadOnly]="isReadOnly"
		  [settings]= "settings"
      (dateChanges)= "onDateChanges($event)"></layout-form-datepicker-input>
    </div>
  `,
                styles: [":host /deep/ .not-valid .ui-calendar input{border:1px solid red}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BooleanEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        if (this.params.value === undefined) {
            this.value = false;
            this.params.setValue(this.value);
        }
        else {
            this.value = this.params.value;
        }
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
BooleanEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-boolean-edit-cell',
                template: `
    <div  *ngIf="formGroup"  class="grid-boolean-container" [formGroup]="formGroup">
      <layout-form-checkbox
        [class.not-valid]="!isValid()"
        [formControlName]="key"
        [isReadOnly]="isReadOnly"
        (checkChanged)="onValueChanged()">
      </layout-form-checkbox>
    </div>
  `,
                styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MaskEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = this.params.value ? this.params.value.toString() : '';
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        if (this.params.colDef.cellEditorParams) {
            if (this.params.colDef.cellEditorParams.settings) {
                this.settings = this.params.colDef.cellEditorParams.settings;
                this.maskType = this.settings.maskType;
            }
            else {
                console.error(this.params.colDef.field + ' settings is not defined');
            }
        }
        else {
            console.error(this.params.colDef.field + ' settings is not defined');
        }
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
MaskEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-mask-edit-cell',
                template: `
    <div *ngIf="formGroup" [formGroup]="formGroup">
    <layout-form-mask-input
      [class.not-valid]="!isValid()"
      [formControlName]="key"
      [maskType]="maskType"
      [isReadOnly]="isReadOnly"
      (changed)="onValueChanged()"
      [isReadOnly]="true">
    </layout-form-mask-input>
    </div>
  `,
                styles: [""]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CurrencyEditCellComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.key = this.params.context.createKey(this.params.columnApi, params.column);
        this.value = (this.params.value || this.params.value === 0) ? this.params.value.toString() : null;
        this.rowId = this.params.node.id;
        if (this.params.colDef.cellEditorParams.customEditableFunction) {
            this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    refresh(params) {
        this.params = params;
        this.formGroup = this.params.context.formGroup.controls[this.rowId];
        if (this.formGroup) {
            this.formGroup.at(this.key).patchValue(this.value);
        }
        return true;
    }
    /**
     * @return {?}
     */
    onValueChanged() {
        this.value = this.formGroup.at(this.key).value;
        this.params.setValue(this.value);
    }
    /**
     * @return {?}
     */
    isValid() {
        return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
    }
}
CurrencyEditCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid-currency-edit-cell',
                template: `
    <div
      *ngIf="formGroup"
      [formGroup]="formGroup">
      <layout-form-currency-input
        [class.not-valid]="!isValid()"
        [formControlName]="key"
        [isReadOnly]="isReadOnly"
        (changed)="onValueChanged()">
      </layout-form-currency-input>
    </div>
  `,
                styles: [""]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ColumnHeaderComponent {
    /**
     * @param {?} elementRef
     * @param {?} sweetAlertService
     */
    constructor(elementRef, sweetAlertService) {
        this.sweetAlertService = sweetAlertService;
        this.elementRef = elementRef;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.coreGridService = this.params.context.componentParent;
        this.coldId = this.params.column.getColId();
        this.params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        this.onSortChanged();
    }
    /**
     * @return {?}
     */
    onMenuClick() {
        this.params.showColumnMenu(this.querySelector('.customHeaderMenuButton'));
    }
    /**
     * @param {?} order
     * @param {?} event
     * @return {?}
     */
    onSortRequested(order, event) {
        this.params.setSort(order, event.shiftKey);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onClickHeader(event) {
        if (this.params.enableSorting) {
            /** @type {?} */
            const coreGridService = this.params.context.componentParent;
            /** @type {?} */
            const bulkData = coreGridService.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Cancel)
                    .subscribe((res) => {
                    if (res && res.value) {
                        this.setSort(event);
                    }
                });
            }
            else {
                this.setSort(event);
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    setSort(event) {
        if (this.sorted === '') {
            this.sorted = 'desc';
        }
        else if (this.sorted === 'desc') {
            this.sorted = 'asc';
        }
        else if (this.sorted === 'asc') {
            this.sorted = 'desc';
        }
        /** @type {?} */
        const sortModel = {
            colId: this.coldId,
            sort: this.sorted
        };
        this.coreGridService.onSortChanged(sortModel);
        // this.params.setSort(this.sorted, event.shiftKey);
    }
    /**
     * @return {?}
     */
    onSortChanged() {
        if (this.params.column.isSortAscending()) {
            this.sorted = 'asc';
        }
        else if (this.params.column.isSortDescending()) {
            this.sorted = 'desc';
        }
        else {
            this.sorted = '';
        }
    }
    /**
     * @return {?}
     */
    isColumnSorted() {
        return this.coreGridService.currentSortedColId === this.coldId;
    }
    /**
     * @private
     * @param {?} selector
     * @return {?}
     */
    querySelector(selector) {
        return (/** @type {?} */ (this.elementRef.nativeElement.querySelector('.customHeaderMenuButton', selector)));
    }
}
ColumnHeaderComponent.decorators = [
    { type: Component, args: [{
                template: "<!-- <div  (click) = \"onClickHeader($event)\" style= \"height: 100%; width: 100%;\">\r\n    <div class=\"cutomLabelContainer\">\r\n        <div class=\"customHeaderLabel\">\r\n            {{params.displayName | translate}}\r\n        </div>\r\n\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'desc'\" class=\"{{'customSortDownLabel'+ (this.sorted === 'desc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('desc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-down\"></i>\r\n        </div>\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'asc'\"  class=\"{{'customSortUpLabel'+ (this.sorted === 'asc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('asc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-up\"></i>\r\n        </div>\r\n    </div>\r\n\r\n    <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->\r\n\r\n<div class=\"ag-cell-label-container\" role=\"presentation\">\r\n    <div class=\"ag-header-cell-label\" role=\"presentation\" (click)=\"onClickHeader($event); $event.stopPropagation();\">\r\n        <span class=\"ag-header-cell-text\" role=\"columnheader\">{{params.displayName | translate}}</span>\r\n        <!-- <span ref=\"eFilter\" class=\"ag-header-icon ag-filter-icon\"></span> -->\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'asc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-ascending-icon sort-icons\"></span>\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'desc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-descending-icon sort-icons\"></span>\r\n        <!-- <span ref=\"eSortNone\" class=\"ag-header-icon ag-sort-none-icon\" ></span> -->\r\n    </div>\r\n    <!-- <span [hidden]=\"!params.enableMenu\"  ref=\"eMenu\" class=\"ag-header-icon ag-header-cell-menu-button\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <span class=\"ag-icon ag-icon-menu\"></span>\r\n    </span> -->\r\n    <!-- <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->",
                styles: [".customHeaderMenuButton{position:absolute;right:15px;top:0}.customSortDownLabel{float:left;margin-left:7px;margin-top:1px}.customSortUpLabel{float:left;margin-left:7px}.customSortRemoveLabel{float:left;font-size:11px;margin-left:3px}.cutomLabelContainer{width:calc(100% - 30px);height:100%;overflow:hidden}.customHeaderLabel{margin-left:5px;float:left}.active{color:#6495ed}.sort-icons{margin-top:8px!important}"]
            }] }
];
/** @nocollapse */
ColumnHeaderComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: SweetAlertService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NoRowsOverlayComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
    }
}
NoRowsOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'grid-no-rows-overlay',
                template: `<div>` +
                    `   {{ 'NoRows' | translate }}` +
                    `</div>`
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingOverlayComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
    }
}
LoadingOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'grid-loading-overlay',
                template: `
  <div class="loader">
  <div class="inner one"></div>
  <div class="inner two"></div>
  <div class="inner three"></div> 
</div>
  `,
                styles: [".loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GridLoadingService extends LoadingService {
    constructor() {
        super();
    }
}
GridLoadingService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
GridLoadingService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreGridService {
    /**
     * @param {?} dataService
     * @param {?} toastr
     * @param {?} loadingService
     * @param {?} sweetAlertService
     * @param {?} ngZone
     * @param {?} router
     * @param {?} route
     */
    constructor(dataService, toastr, loadingService, sweetAlertService, ngZone, router, route) {
        this.dataService = dataService;
        this.toastr = toastr;
        this.loadingService = loadingService;
        this.sweetAlertService = sweetAlertService;
        this.ngZone = ngZone;
        this.router = router;
        this.route = route;
        this.pagingResult = {};
        this.data = [];
        this.originalRowData = [];
        this.deletedData = [];
        this.filter = [];
        this.filterGroupList = [];
        this.gridForm = new FormGroup({});
        this.loadOnInit = true;
        this.isDataChanged = false;
        this.isLoad = false;
        this.columnDefs = [];
        this.allGridColumns = [];
        this.recordEventsSubject = new Subject();
        this.retrievedDataSourceSubject = new Subject();
        this.loadingDisplayStatusSubscription = this.loadingService.getLoadingBarDisplayStatus().subscribe((data) => {
            this.loadingDisplayStatus = data;
            this.loadingOverlay();
        });
    }
    /**
     * @param {?} options
     * @param {?} loadOnInit
     * @param {?=} loadColumnDataSourceOnInit
     * @return {?}
     */
    init(options, loadOnInit, loadColumnDataSourceOnInit) {
        this.loadOnInit = loadOnInit;
        this.loadColumnDataSourceOnInit = loadColumnDataSourceOnInit;
        this.gridOptions = options;
        if (this.gridApi) {
            this.pagingResult = {
                CurrentPage: 1,
                PageSize: options.requestOptions.CustomFilter.PageSize
            };
            this.reloadColumnDefs();
            this.setLoadUrlSetting();
            if (this.loadOnInit === true) {
                this.isLoad = true;
                this.refreshGridData();
            }
        }
    }
    /**
     * @return {?}
     */
    setLoadUrlSetting() {
        this.urlOptions = {
            moduleUrl: this.gridOptions.requestOptions.UrlOptions.moduleUrl,
            endPointUrl: this.gridOptions.requestOptions.UrlOptions.endPointUrl
        };
    }
    /**
     * @private
     * @return {?}
     */
    createColumns() {
        if (this.gridOptions && this.gridOptions.enableCheckboxSelection) {
            /** @type {?} */
            const selectColumn = {
                headerName: 'Select',
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: true,
                checkboxSelection: true,
                minWidth: 40,
                maxWidth: 40,
                cellStyle: { 'padding-top': '5px' },
                showRowGroup: false,
                enableRowGroup: false,
                enablePivot: false,
                suppressFilter: true,
                menuTabs: []
            };
            this.columnDefs.push(selectColumn);
        }
        this.gridOptions.columns.forEach(item => {
            /** @type {?} */
            let cellRenderer;
            /** @type {?} */
            let columEditable = this.gridOptions.editable;
            if (item.editable !== undefined) {
                columEditable = item.editable;
            }
            if (columEditable) {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreTextEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreLOVEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskEditCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyEditCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
            }
            else {
                if (item.columnType === CoreGridColumnType.Label) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Text) {
                    cellRenderer = CoreGridCellType.CoreLabelCell;
                }
                else if (item.columnType === CoreGridColumnType.Date) {
                    cellRenderer = CoreGridCellType.CoreDateCell;
                }
                else if (item.columnType === CoreGridColumnType.Select) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Numeric) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.Boolean) {
                    cellRenderer = CoreGridCellType.CoreBooleanCell;
                }
                else if (item.columnType === CoreGridColumnType.Lov) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
                else if (item.columnType === CoreGridColumnType.Mask) {
                    cellRenderer = CoreGridCellType.CoreMaskCell;
                }
                else if (item.columnType === CoreGridColumnType.Currency) {
                    cellRenderer = CoreGridCellType.CoreCurrencyCell;
                }
                else if (item.columnType === CoreGridColumnType.NumericLabel) {
                    cellRenderer = CoreGridCellType.CoreNumericCell;
                }
                else if (item.columnType === CoreGridColumnType.TextSelect) {
                    cellRenderer = CoreGridCellType.CoreSelectCell;
                }
            }
            if (item.visible !== false) {
                item.visible = true;
            }
            else {
                item.visible = false;
            }
            /** @type {?} */
            let headerName = item.field;
            if (this.gridOptions
                && this.gridOptions.requestOptions
                && this.gridOptions.requestOptions.UrlOptions
                && this.gridOptions.requestOptions.UrlOptions.endPointUrl) {
                /** @type {?} */
                const endPointUrl = this.gridOptions.requestOptions.UrlOptions.endPointUrl;
                if (endPointUrl.indexOf('/') !== -1) {
                    headerName = endPointUrl.split('/')[0] + item.field;
                }
                else {
                    headerName = endPointUrl + item.field;
                }
            }
            /** @type {?} */
            const column = {
                field: item.field,
                headerName: item.headerName || headerName,
                cellRenderer,
                hide: !item.visible,
                suppressSizeToFit: null,
                filter: this.gridOptions.enableFilter || false,
                sortable: this.gridOptions.enableSorting || false,
                resizable: true,
                cellEditorParams: {
                    dataSource: null,
                    settings: null,
                    data: null,
                    customDisplayFunction: item.customDisplayFunction || null,
                    customEditableFunction: item.customEditableFunction || null,
                    customFunctionForRowValueChanges: item.customFunctionForRowValueChanges || null
                },
                floatingFilterComponentParams: {
                    hideFilter: item.hideFilter || false,
                    serviceAccess: this,
                    field: item.field,
                    customFilterFunction: item.customFilterFunction || null
                },
                customFieldForFilter: item.customFieldForFilter,
                customFieldForSort: item.customFieldForSort,
                getDataSourceAfterDataLoaded: item.getDataSourceAfterDataLoaded,
                columnType: item.columnType
            };
            if (item.field === this.gridOptions.keyField) {
                column.suppressSizeToFit = true;
            }
            if (item.columnType === CoreGridColumnType.Date && (this.gridOptions.editable || column['editable'])) {
                column['minWidth'] = 135;
            }
            if (item.minWidth) {
                column['minWidth'] = item.minWidth;
            }
            if (item.maxWidth) {
                column['maxWidth'] = item.maxWidth;
            }
            if (item.dataSource) {
                column.cellEditorParams.dataSource = item.dataSource;
            }
            if (item.settings) {
                column.cellEditorParams.settings = item.settings;
            }
            if ((item.columnType === CoreGridColumnType.Select || cellRenderer === CoreGridCellType.CoreSelectCell)
                && item.settings) {
                column.cellEditorParams.data = new Subject();
                if (!item.getDataSourceAfterDataLoaded) {
                    this.getColumnDataSource(column.cellEditorParams.data, column);
                }
            }
            /** @type {?} */
            const index = findIndex(this.columnDefs, ['field', item.field]);
            if (index < 0) {
                this.columnDefs.push(column);
            }
            else {
                this.columnDefs[index] = column;
            }
        });
        this.setFilterComponents();
        this.addCommandColumn();
    }
    /**
     * @return {?}
     */
    reloadColumnDefs() {
        this.createColumns();
        this.reloadGridColumnDefs();
    }
    /**
     * @return {?}
     */
    getColumnState() {
        console.log(this.columnApi.getColumnState());
    }
    /**
     * @param {?} data
     * @param {?} column
     * @param {?=} extraFilters
     * @return {?}
     */
    getColumnDataSource(data, column, extraFilters) {
        if (this.loadOnInit || this.loadColumnDataSourceOnInit) {
            /** @type {?} */
            const settings = (/** @type {?} */ (column.cellEditorParams.settings));
            /** @type {?} */
            let requestOptions;
            if (settings) {
                if (column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.TextSelect) {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).requestOptions));
                }
                else {
                    requestOptions = JSON.parse(JSON.stringify(((/** @type {?} */ (settings))).options.requestOptions));
                }
            }
            if (requestOptions) {
                /** @type {?} */
                const reqUrl = requestOptions.UrlOptions.moduleUrl + requestOptions.UrlOptions.endPointUrl;
                /** @type {?} */
                let disableGeneralLoading = false;
                if (!this.gridOptions.disableSelfLoading) {
                    disableGeneralLoading = true;
                    this.loadingService.insertLoadingRequest(reqUrl);
                }
                /** @type {?} */
                const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                if (extraFilters && extraFilters.length > 0) {
                    if (requestOptions.CustomFilter && requestOptions.CustomFilter.FilterGroups) {
                        if (requestOptions.CustomFilter.FilterGroups.length === 0) {
                            requestOptions.CustomFilter.FilterGroups = [{
                                    Filters: []
                                }];
                        }
                        else if (requestOptions.CustomFilter.FilterGroups.length > 0) {
                            requestOptions.CustomFilter.FilterGroups.unshift({
                                Filters: []
                            });
                        }
                    }
                    else if (!requestOptions.CustomFilter) {
                        requestOptions.CustomFilter = {
                            FilterGroups: [{
                                    Filters: []
                                }]
                        };
                    }
                    else if (requestOptions.CustomFilter && !requestOptions.CustomFilter.FilterGroups) {
                        requestOptions.CustomFilter.FilterGroups = [{
                                Filters: []
                            }];
                    }
                    if (requestOptions.CustomFilter &&
                        requestOptions.CustomFilter.FilterGroups &&
                        requestOptions.CustomFilter.FilterGroups.length > 0) {
                        requestOptions.CustomFilter.FilterGroups[0].Filters = extraFilters;
                    }
                }
                this.dataService.getListBy(reqUrl, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                    .pipe(finalize(() => {
                    if (!this.gridOptions.disableSelfLoading) {
                        this.loadingService.removeLoadingRequest(reqUrl);
                    }
                }))
                    .subscribe((httpResponse) => {
                    /** @type {?} */
                    const lovData = httpResponse.serviceResult.Result;
                    /** @type {?} */
                    const dataSource = lovData.map(x => Object.assign({}, x));
                    /** @type {?} */
                    const colIndex = findIndex(this.columnDefs, ['field', column.field]);
                    column.cellEditorParams.dataSource = dataSource;
                    this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                    data.next({ data: true });
                });
            }
        }
    }
    /**
     * @return {?}
     */
    getComponents() {
        /** @type {?} */
        const components = {};
        // Edit Cell
        components[CoreGridCellType.CoreBooleanEditCell] = BooleanEditCellComponent;
        components[CoreGridCellType.CoreDateEditCell] = DateEditCellComponent;
        components[CoreGridCellType.CoreLOVEditCell] = LovEditCellComponent;
        components[CoreGridCellType.CoreNumericEditCell] = NumericEditCellComponent;
        components[CoreGridCellType.CoreSelectEditCell] = SelectEditCellComponent;
        components[CoreGridCellType.CoreTextEditCell] = TextEditCellComponent;
        components[CoreGridCellType.CoreMaskEditCell] = MaskEditCellComponent;
        components[CoreGridCellType.CoreCurrencyEditCell] = CurrencyEditCellComponent;
        // Read-Only Cell
        components[CoreGridCellType.CoreBooleanCell] = BooleanCellComponent;
        components[CoreGridCellType.CoreLabelCell] = LabelCellComponent;
        components[CoreGridCellType.CoreDateCell] = DateCellComponent;
        components[CoreGridCellType.CoreSelectCell] = SelectCellComponent;
        components[CoreGridCellType.CoreCurrencyCell] = CurrencyCellComponent;
        components[CoreGridCellType.CoreMaskCell] = MaskCellComponent;
        components[CoreGridCellType.CoreNumericCell] = NumericCellComponent;
        // Filter Cell
        components[CoreGridCellType.CoreDateFilterCell] = DateFilterCellComponent;
        components[CoreGridCellType.CoreSelectFilterCell] = SelectFilterCellComponent;
        components[CoreGridCellType.CoreTextFilterCell] = TextFilterCellComponent;
        components[CoreGridCellType.CoreNumericFilterCell] = NumericFilterCellComponent;
        components[CoreGridCellType.CoreBooleanFilterCell] = BooleanFilterCellComponent;
        components[CoreGridCellType.CoreCurrencyFilterCell] = CurrencyFilterCellComponent;
        components[CoreGridCellType.CoreLOVFilterCell] = LovFilterCellComponent;
        components[CoreGridCellType.CoreMaskFilterCell] = MaskFilterCellComponent;
        // Command Cell
        components[CoreGridCellType.CoreCommandCell] = CommandCellComponent;
        components['agColumnHeader'] = ColumnHeaderComponent;
        components['customNoRowsOverlay'] = NoRowsOverlayComponent;
        components['customLoadingOverlay'] = LoadingOverlayComponent;
        return components;
    }
    /**
     * @return {?}
     */
    setFilterComponents() {
        this.columnDefs.forEach(item => {
            if (item.cellRenderer === CoreGridCellType.CoreBooleanEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreDateCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLOVEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreLOVFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreTextEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreBooleanCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreLabelCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreMaskCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyEditCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreCurrencyCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreNumericCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell
                && item.columnType === CoreGridColumnType.TextSelect) {
                item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
            }
            else if (item.cellRenderer === CoreGridCellType.CoreSelectCell) {
                item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
            }
        });
    }
    /**
     * @private
     * @param {?} genericExpression
     * @return {?}
     */
    loadDataByFilterAndPageSetting(genericExpression) {
        /** @type {?} */
        const reqUrl = `${this.urlOptions.moduleUrl}${this.urlOptions.endPointUrl}`;
        /** @type {?} */
        let headerParams = [];
        if (this.gridOptions && this.gridOptions.requestOptions && this.gridOptions.requestOptions.HeaderParameters) {
            headerParams = this.gridOptions.requestOptions.HeaderParameters;
        }
        if (!this.gridOptions.disableSelfLoading) {
            this.loadingService.insertLoadingRequest(reqUrl);
            /** @type {?} */
            const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            headerParams.push(disableLoadingHeader);
        }
        return this.dataService.getListBy(reqUrl, genericExpression, headerParams)
            .pipe(finalize(() => {
            if (!this.gridOptions.disableSelfLoading) {
                this.loadingService.removeLoadingRequest(reqUrl);
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    loadGridViewDataByOptionsUrlAndFilters() {
        if (this.gridOptions) {
            /** @type {?} */
            const searchFilter = cloneDeep(this.gridOptions.requestOptions.CustomFilter);
            if (this.pagingResult) {
                searchFilter.PageNumber = this.pagingResult.CurrentPage;
                searchFilter.PageSize = this.pagingResult.PageSize;
            }
            searchFilter.FilterGroups = searchFilter.FilterGroups || [];
            this.filter.forEach(element => {
                /** @type {?} */
                let name;
                /** @type {?} */
                const foundColumn = this.gridOptions.columns.find(x => x.field === element.key);
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (element.key.indexOf('Id') > 0 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = element.key.substr(0, element.key.indexOf('Id')) +
                            '.' + element.key.substr(element.key.indexOf('Id'), element.key.length);
                    }
                    else {
                        name = element.key;
                    }
                }
                /** @type {?} */
                const group = {
                    Filters: [{
                            PropertyName: name,
                            Value: element.value['filter'],
                            Comparison: element.value['type'],
                            OtherValue: element.value['other'] || null
                        }]
                };
                searchFilter.FilterGroups.push(group);
            });
            // it is for CoreGridColumnType.TextSelect
            this.filterGroupList.forEach((filterGroupItem) => {
                searchFilter.FilterGroups.unshift(filterGroupItem.value);
            });
            // if (searchFilter.FilterGroups.length > 0) {
            //   searchFilter.FilterGroups[searchFilter.FilterGroups.length - 1]['Connector'] = Connector.Or;
            // }
            this.loadDataByFilterAndPageSetting(searchFilter)
                .subscribe((httpResponse) => {
                this.pagingResult = httpResponse.pagingResult;
                this.data = httpResponse.serviceResult.Result;
                if (this.data) {
                    this.originalRowData = this.data.map(item => Object.assign({}, item));
                }
                this.retrievedDataSourceSubject.next(this.data);
                if (!this.data || (this.data && this.data.length === 0)) {
                    this.loadingOverlay();
                }
                this.loadDataSourceAfterGetGridData();
            });
        }
    }
    /**
     * @return {?}
     */
    loadDataSourceAfterGetGridData() {
        /** @type {?} */
        const needToLoadDataSourceColumns = this.columnDefs.filter(x => x.getDataSourceAfterDataLoaded === true);
        needToLoadDataSourceColumns.forEach(column => {
            /** @type {?} */
            const columnValues = this.data.map(x => x[column.field]);
            /** @type {?} */
            const newFilters = [];
            columnValues.forEach((columnValue) => {
                /** @type {?} */
                const newFilter = {
                    PropertyName: column.cellEditorParams.settings.valueField,
                    Value: columnValue,
                    Comparison: Comparison.EqualTo,
                    Connector: Connector.Or
                };
                /** @type {?} */
                let name = column.cellEditorParams.settings.valueField;
                // can be changable
                if (name) {
                    if (column.cellEditorParams.settings.valueField.indexOf('Id') !== -1 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = name.substr(0, name.indexOf('Id')) +
                            '.' + name.substr(name.indexOf('Id'), name.length);
                    }
                }
                newFilter.PropertyName = name;
                newFilters.push(newFilter);
            });
            if (newFilters.length > 0) {
                newFilters[newFilters.length - 1].Connector = Connector.And;
            }
            if (newFilters.length > 0) {
                this.getColumnDataSource(column.cellEditorParams.data, column, newFilters);
            }
        });
        // getColumnDataSource 
    }
    /**
     * @private
     * @return {?}
     */
    addCommandColumn() {
        /** @type {?} */
        let showRowDeleteButton = true;
        /** @type {?} */
        let showRowEditButton = true;
        /** @type {?} */
        let buttonCounts = 1;
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showRowDeleteButton !== undefined) {
                showRowDeleteButton = this.gridOptions.buttonSettings.showRowDeleteButton;
                if (showRowDeleteButton) {
                    buttonCounts++;
                }
            }
            if (this.gridOptions.buttonSettings.showEditButton !== undefined) {
                showRowEditButton = this.gridOptions.buttonSettings.showEditButton;
                if (showRowEditButton) {
                    buttonCounts++;
                }
            }
        }
        // it is for delete and revert for inline grid
        // if (this.gridOptions.editable && buttonCounts === 1) {
        //   buttonCounts = 2;
        // }
        if (showRowDeleteButton || showRowEditButton) {
            /** @type {?} */
            const index = findIndex(this.columnDefs, ['field', 'CommandColumn']);
            if (index < 0) {
                /** @type {?} */
                const column = {
                    headerName: '',
                    field: 'CommandColumn',
                    editable: false,
                    cellRenderer: CoreGridCellType.CoreCommandCell,
                    filter: false,
                    sortable: false,
                    minWidth: buttonCounts * 50,
                    maxWidth: buttonCounts * 50
                };
                this.columnDefs.push(column);
            }
            // this.columnDefs.push();
        }
    }
    /**
     * @private
     * @return {?}
     */
    createFormControls() {
        /** @type {?} */
        const columns = this.columnApi.getAllColumns();
        /** @type {?} */
        const gridFormGroup = ((/** @type {?} */ (this.gridForm)));
        // clear out old form group controls if switching between data sources
        /** @type {?} */
        const controlNames = Object.keys(gridFormGroup.controls);
        controlNames.forEach((controlName) => {
            gridFormGroup.removeControl(controlName);
        });
        this.gridApi.forEachNode((rowNode) => {
            /** @type {?} */
            const formArray = new FormArray([]);
            columns.filter((column) => column.getColDef().field !== 'CommandColumn')
                .forEach((column) => {
                /** @type {?} */
                const key = this.createKey(this.columnApi, column);
                // the cells will use this same createKey method
                /** @type {?} */
                const col = find(this.gridOptions.columns, ['field', column.getColDef().field]);
                /** @type {?} */
                const validation = [];
                if (col) {
                    if (col.validation) {
                        if (col.validation.required) {
                            validation.push(Validators.required);
                        }
                        if (col.validation.email) {
                            validation.push(Validators.email);
                        }
                        if (col.validation.requiredTrue) {
                            validation.push(Validators.requiredTrue);
                        }
                        if (col.validation.minValue) {
                            validation.push(Validators.min(col.validation.minValue));
                        }
                        if (col.validation.maxValue) {
                            validation.push(Validators.max(col.validation.maxValue));
                        }
                        if (col.validation.minLength) {
                            validation.push(Validators.minLength(col.validation.minLength));
                        }
                        if (col.validation.maxLength) {
                            validation.push(Validators.maxLength(col.validation.maxLength));
                        }
                        if (col.validation.regex) {
                            validation.push(Validators.pattern(col.validation.regex));
                        }
                    }
                }
                formArray.setControl((/** @type {?} */ (key)), new FormControl('', { validators: validation }));
            });
            gridFormGroup.addControl((/** @type {?} */ (rowNode.id)), formArray);
        });
    }
    /**
     * @private
     * @return {?}
     */
    reloadGridColumnDefs() {
        // this.columnDefs = columnDefs;
        if (this.gridApi) {
            this.gridApi.setColumnDefs(this.columnDefs);
            this.columnApi.resetColumnState();
        }
        this.refreshFormControls();
        if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    setColumnDataSource(columnName, dataSource) {
        /** @type {?} */
        const index = findIndex(this.gridOptions.columns, ['field', columnName]);
        this.gridOptions.columns[index].dataSource = dataSource;
        if (this.gridApi) {
            this.gridApi.refreshCells({ columns: [columnName], force: true });
        }
    }
    /**
     * @param {?} column
     * @return {?}
     */
    setColumnLovOptions(column) {
        if ((column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.Lov) && column.settings) {
            /** @type {?} */
            const settings = (/** @type {?} */ (column.settings));
            /** @type {?} */
            let requestOptions;
            if (column.columnType === CoreGridColumnType.Select) {
                requestOptions = ((/** @type {?} */ (settings))).requestOptions;
            }
            else {
                requestOptions = ((/** @type {?} */ (settings))).options.requestOptions;
            }
            /** @type {?} */
            const reqUrl = requestOptions.UrlOptions.moduleUrl + '/'
                + requestOptions.UrlOptions.endPointUrl;
            /** @type {?} */
            let disableGeneralLoading = false;
            if (!this.gridOptions.disableSelfLoading) {
                disableGeneralLoading = true;
                this.loadingService.insertLoadingRequest(reqUrl);
            }
            /** @type {?} */
            const disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
            this.dataService.getListBy(reqUrl, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                .pipe(finalize(() => {
                if (!this.gridOptions.disableSelfLoading) {
                    this.loadingService.removeLoadingRequest(reqUrl);
                }
            }))
                .subscribe((httpResponse) => {
                /** @type {?} */
                const lovData = httpResponse.serviceResult.Result;
                /** @type {?} */
                const dataSource = lovData.map(x => Object.assign({}, x));
                /** @type {?} */
                const colIndex = findIndex(this.columnDefs, ['field', column.field]);
                column.dataSource = dataSource;
                this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                this.columnDefs[colIndex].cellEditorParams.settings = settings;
                if (this.gridApi) {
                    this.gridApi.refreshCells({ columns: [column.field], force: true });
                }
            });
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    onGridReady(params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        this.refreshFormControls();
        if (this.loadingDisplayStatus) {
            this.gridApi.showLoadingOverlay();
        }
        if (this.isLoad === false) {
            this.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
        }
        this.gridApi.sizeColumnsToFit();
    }
    /**
     * @param {?} params
     * @return {?}
     */
    onColumnEverythingChanged(params) {
        this.allGridColumns = params.columnApi.getAllColumns();
    }
    /**
     * @return {?}
     */
    onGridSizeChanged() {
        if (this.gridApi) {
            if (this.gridOptions && !this.gridOptions.disableSizeToFit && this.gridApi) {
                this.gridApi.sizeColumnsToFit();
            }
            this.gridApi.refreshCells({ force: true });
        }
    }
    /**
     * @private
     * @param {?} columnApi
     * @param {?} column
     * @return {?}
     */
    createKey(columnApi, column) {
        return columnApi.getAllColumns().indexOf(column);
    }
    /**
     * @param {?} self
     * @return {?}
     */
    getContext(self) {
        return {
            componentParent: self,
            formGroup: this.gridForm,
            createKey: this.createKey
        };
    }
    /**
     * @return {?}
     */
    refreshFormControls() {
        if (this.gridApi) {
            this.createFormControls();
            this.gridApi.refreshCells({ force: true });
        }
    }
    /**
     * @param {?=} isCheckBulkData
     * @return {?}
     */
    refreshGridData(isCheckBulkData) {
        /** @type {?} */
        const bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0 && isCheckBulkData) {
            this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Refresh)
                .subscribe((res) => {
                if (res && res.value) {
                    this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
                    this.refreshGrid();
                    this.toastr.success(ToastrMessages.GridRefreshed);
                }
            });
        }
        else {
            this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
            this.refreshGrid();
        }
    }
    /**
     * @return {?}
     */
    refreshGrid() {
        this.deletedData = [];
        this.isDataChanged = false;
        this.loadGridViewDataByOptionsUrlAndFilters();
        if (this.isLoad === false) {
            this.init(this.gridOptions, true);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onCellValueChanged(item) {
        this.isDataChanged = true;
        if (item.data.OperationType !== OperationType.Created) {
            item.data.OperationType = OperationType.Updated;
        }
    }
    /**
     * @param {?} sortModel
     * @return {?}
     */
    onSortChanged(sortModel) {
        if (sortModel) {
            // const sortModel = _.head(this.gridApi.getSortModel());
            this.currentSortedColId = sortModel.colId;
            /** @type {?} */
            const columnDef = (/** @type {?} */ (this.gridApi.getColumnDef(sortModel.colId)));
            /** @type {?} */
            let name = columnDef.customFieldForSort;
            if (!name) {
                if (columnDef.field.indexOf('Id') > 0 &&
                    (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                    name = columnDef.field.substr(0, columnDef.field.indexOf('Id'));
                }
                else {
                    name = columnDef.field;
                }
            }
            /** @type {?} */
            const sort = [{
                    PropertyName: name,
                    Type: sortModel.sort === 'asc' ? SortType.Asc : SortType.Desc
                }];
            this.gridOptions.requestOptions.CustomFilter.Sort = sort;
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onFilterChanged(event) {
        // this.filter = Object.entries(this.gridApi.getFilterModel()).map(([key, value]) => ({ key, value }));
        this.loadGridViewDataByOptionsUrlAndFilters();
    }
    /**
     * @param {?} field
     * @param {?} model
     * @return {?}
     */
    applyChangesToFilter(field, model) {
        this.addToListForFilter(this.filter, model.filter || model.filter === 0 ? model : null, field);
    }
    /**
     * @param {?} listArray
     * @param {?} value
     * @param {?} key
     * @return {?}
     */
    addToListForFilter(listArray, value, key) {
        /** @type {?} */
        const foundDataIndex = listArray.findIndex(x => x.key === key);
        if (value) {
            /** @type {?} */
            const dataToSend = {
                key,
                value
            };
            if (foundDataIndex !== -1) {
                listArray[foundDataIndex] = dataToSend;
            }
            else {
                listArray.push(dataToSend);
            }
        }
        else {
            if (foundDataIndex !== -1) {
                listArray.splice(foundDataIndex, 1);
            }
        }
    }
    // called from text filter for CoreGridColumnType.TextSelect
    /**
     * @param {?} field
     * @param {?} settings
     * @param {?} model
     * @return {?}
     */
    textSelectFindValuesOnFilter(field, settings, model) {
        /** @type {?} */
        const foundColumn = this.gridOptions.columns.find(x => x.field === field);
        if (model.filter) {
            /** @type {?} */
            const filter$$1 = {
                PageSize: 100,
                FilterGroups: [
                    {
                        Filters: [
                            {
                                PropertyName: (/** @type {?} */ (settings.labelField)),
                                Value: model.filter,
                                Comparison: model.type,
                                Connector: Connector.And
                            },
                        ]
                    }
                ]
            };
            this.dataService.getListBy(settings.requestOptions.UrlOptions.moduleUrl + settings.requestOptions.UrlOptions.endPointUrl, filter$$1)
                .subscribe((httpResponse) => {
                /** @type {?} */
                const result = httpResponse.serviceResult.Result || [];
                /** @type {?} */
                const idList = result.map(x => x[settings.valueField]).filter(this.onlyUnique);
                /** @type {?} */
                const newFilters = [];
                /** @type {?} */
                let name = '';
                if (foundColumn) {
                    name = foundColumn.customFieldForFilter;
                }
                if (!name) {
                    if (field.indexOf('Id') > 0 &&
                        (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                        name = field.substr(0, field.indexOf('Id')) +
                            '.' + field.substr(field.indexOf('Id'), field.length);
                    }
                    else {
                        name = field;
                    }
                }
                idList.forEach((id) => {
                    newFilters.push({
                        PropertyName: name,
                        Value: id,
                        Comparison: Comparison.EqualTo,
                        Connector: Connector.Or
                    });
                });
                if (newFilters.length > 0) {
                    newFilters[newFilters.length - 1].Connector = Connector.And;
                }
                /** @type {?} */
                const filterGroup = {
                    Connector: Connector.And,
                    Filters: newFilters
                };
                if (newFilters.length > 0) {
                    this.addToListForFilter(this.filterGroupList, filterGroup, field);
                    this.loadGridViewDataByOptionsUrlAndFilters();
                }
                else {
                    // gridi boşalmak lazım
                    this.data = [];
                    this.pagingResult.TotalCount = 0;
                }
            });
        }
        else {
            this.addToListForFilter(this.filterGroupList, null, field);
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @param {?} value
     * @param {?} index
     * @param {?} self
     * @return {?}
     */
    onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onPageChanged(event) {
        this.pagingResult.CurrentPage = event.page;
        this.pagingResult.PageSize = event.itemsPerPage;
        this.loadGridViewDataByOptionsUrlAndFilters();
    }
    /**
     * @param {?} pageSizeEvent
     * @return {?}
     */
    onChangeItemsPerPage(pageSizeEvent) {
        if (pageSizeEvent.Id) {
            this.pagingResult.PageSize = pageSizeEvent.Id;
        }
        if (this.pagingResult.TotalCount > 0) {
            this.loadGridViewDataByOptionsUrlAndFilters();
        }
    }
    /**
     * @return {?}
     */
    getBulkOperationData() {
        /** @type {?} */
        const resultData = [];
        if (this.gridOptions && this.gridOptions.editable) {
            this.validateForm();
            this.deletedData
                .forEach((obj) => {
                resultData.push(obj);
            });
            this.data.forEach(obj => {
                if (obj.OperationType !== OperationType.None) {
                    resultData.push(obj);
                }
            });
        }
        return resultData;
    }
    /**
     * @param {?=} formGroup
     * @return {?}
     */
    validateForm(formGroup = this.gridForm) {
        Object.keys(formGroup.controls).forEach(field => {
            /** @type {?} */
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                this.validateForm(control);
            }
            else if (control instanceof FormArray) {
                Object.keys(control.controls).forEach(c => {
                    /** @type {?} */
                    const ctrl = control.get(c);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
        });
        return formGroup.valid;
    }
    /**
     * @return {?}
     */
    revertData() {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe((res) => {
            if (res && res.value) {
                this.isDataChanged = false;
                this.deletedData = [];
                this.data = this.originalRowData.map(item => Object.assign({}, item));
                this.gridApi.setRowData(this.data);
                this.toastr.success(ToastrMessages.RecordReverted);
                this.recordEventsSubject.next({ type: 'RevertedGrid', record: this.data });
            }
        });
    }
    /**
     * @return {?}
     */
    addRow() {
        if (this.gridOptions.editable) {
            /** @type {?} */
            const addedObject = { Id: -1, OperationType: OperationType.Created };
            this.gridOptions.columns.filter(x => x.field !== 'Id').forEach(element => {
                addedObject[element.field] = element.defaultValue;
            });
            this.data.unshift(addedObject);
            this.isDataChanged = true;
            // this.gridApi.updateRowData({
            //   add: [addedObject],
            //   addIndex: 0
            // });
            this.gridApi.setRowData(this.data);
            this.refreshFormControls();
            this.recordEventsSubject.next({ type: 'Add', record: addedObject });
        }
        else {
            this.ngZone.run(() => {
                if (this.gridOptions.addUrl) {
                    this.router.navigate([this.gridOptions.addUrl]);
                }
                else {
                    this.router.navigate(['new'], { relativeTo: this.route });
                }
            });
        }
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    editRecord(params, rowId, key) {
        if (this.gridOptions.editUrlBlank) {
            if (this.gridOptions.editUrl) {
                window.open(window.location.origin + '/' + this.gridOptions.editUrl + '/' + params.data[this.gridOptions.keyField]);
            }
            else {
                window.open(this.router.url + '/' + 'edit/' + params.data[this.gridOptions.keyField]);
            }
        }
        else {
            this.ngZone.run(() => {
                if (this.gridOptions.editUrl) {
                    this.router.navigate([this.gridOptions.editUrl, params.data[this.gridOptions.keyField]]);
                }
                else {
                    this.router.navigate(['edit', params.data[this.gridOptions.keyField]], { relativeTo: this.route });
                }
                // this.handleBreadcrumbLabel(params.data);
            });
        }
    }
    // handleBreadcrumbLabel(rowData: any): void {
    //   if (this.gridOptions &&
    //     this.gridOptions.breadcrumbLabel &&
    //     rowData &&
    //     this.gridOptions.breadcrumbLabel &&
    //     this.gridOptions.keyField &&
    //     rowData[this.gridOptions.keyField]) {
    //     let editUrl: string;
    //     const keyFieldValue = rowData[this.gridOptions.keyField];
    //     if (this.gridOptions.editUrl) {
    //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
    //     } else {
    //       editUrl = this.router.url + '/edit/' + keyFieldValue;
    //     }
    //     let customLabel = '';
    //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
    //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
    //       if (foundColumn && foundColumn.colDef) {
    //         if (foundColumn.colDef.cellEditorParams &&
    //           foundColumn.colDef.cellEditorParams.dataSource &&
    //           foundColumn.colDef.cellEditorParams.settings) {
    //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
    //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
    //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
    //           customLabel += foundData[labelField] + ' ';
    //         } else {
    //           customLabel += rowData[columnField] + ' ';
    //         }
    //       }
    //     });
    //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
    //   }
    // }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    undoRecord(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
            .subscribe((result) => {
            if (result.value) {
                /** @type {?} */
                const revertedObject = this.data[rowId];
                /** @type {?} */
                const orginalObject = this.originalRowData
                    .find(x => revertedObject && x[this.gridOptions.keyField] === revertedObject[this.gridOptions.keyField]);
                this.data[rowId] = orginalObject ? JSON.parse(JSON.stringify(orginalObject)) : null;
                params.node.data = this.data[rowId];
                this.gridApi.redrawRows({ rowNodes: [params.node] });
                this.gridApi.refreshCells({ rowNodes: [params.node], force: true });
                params.context.formGroup.controls[rowId].markAsPristine();
            }
        });
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    deleteRecord(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe((result) => {
            if (result.value) {
                /** @type {?} */
                const deleteUrl = this.gridOptions.deleteEndPointUrl ? this.gridOptions.deleteEndPointUrl : this.urlOptions.endPointUrl;
                /** @type {?} */
                const reqUrl = `${this.urlOptions.moduleUrl}${deleteUrl}`;
                this.dataService.delete(this.data[rowId][this.gridOptions.keyField], reqUrl)
                    .subscribe((res) => {
                    this.recordEventsSubject.next({ type: 'Delete', record: this.data[rowId] });
                    // const indexOfData = this.data.indexOf(params.data);
                    if (this.pagingResult.TotalCount > 10) {
                        this.refreshAndRevertChanges();
                    }
                    else {
                        this.data.splice(rowId, 1);
                        if (this.pagingResult.TotalCount && this.pagingResult.TotalCount > 0) {
                            this.pagingResult.TotalCount--;
                        }
                        this.gridApi.setRowData(this.data);
                    }
                    this.toastr.success(ToastrMessages.RecordDeleted);
                });
            }
            else {
                this.sweetAlertService.confirmOperationPopup(result);
            }
        });
    }
    /**
     * @return {?}
     */
    callUndoConfirmAlert() {
        /** @type {?} */
        const bulkData = this.getBulkOperationData();
        if (bulkData && bulkData.length > 0) {
            return this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo);
        }
        else {
            return of({ value: true });
        }
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    removeRow(params, rowId, key) {
        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
            .subscribe((res) => {
            if (res && res.value) {
                // this.data[rowId] = Object.assign({}, this.originalRowData[rowId]);
                /** @type {?} */
                const deletedObject = this.data[rowId];
                this.data.splice(rowId, 1);
                this.recordEventsSubject.next({ type: 'Delete', record: deletedObject });
                if (params.data.OperationType !== OperationType.Created) {
                    /** @type {?} */
                    const orginalDeletedObject = this.originalRowData
                        .find(x => x[this.gridOptions.keyField] === deletedObject[this.gridOptions.keyField]);
                    if (orginalDeletedObject) {
                        orginalDeletedObject['OperationType'] = OperationType.Deleted;
                        this.deletedData.push(orginalDeletedObject);
                    }
                    else {
                        deletedObject['OperationType'] = OperationType.Deleted;
                        this.deletedData.push(deletedObject);
                    }
                }
                this.isDataChanged = true;
                this.gridApi.setRowData(this.data);
                // if (!this.gridOptions.editable) {
                //   this.toastr.success(ToastrMessages.RecordDeleted);
                // }
            }
        });
    }
    /**
     * @return {?}
     */
    refreshAndRevertChanges() {
        this.deletedData = [];
        this.isDataChanged = false;
        this.refreshGridData();
    }
    /**
     * @return {?}
     */
    isChanged() {
        return this.isDataChanged;
    }
    /**
     * @return {?}
     */
    getTotalCount() {
        return this.pagingResult.TotalCount - this.deletedData.length;
    }
    /**
     * @return {?}
     */
    getNoRowsOverlayComponent() {
        return 'customNoRowsOverlay';
    }
    /**
     * @return {?}
     */
    getLoadingOverlayComponent() {
        return 'customLoadingOverlay';
    }
    /**
     * @return {?}
     */
    getSelectedRows() {
        return this.gridApi.getSelectedRows();
    }
    /**
     * @return {?}
     */
    loadingOverlay() {
        if (this.gridApi) {
            if (this.loadingDisplayStatus) {
                this.gridApi.showLoadingOverlay();
            }
            else {
                this.gridApi.hideOverlay();
                if (!this.data || (this.data && this.data.length === 0)) {
                    this.gridApi.showNoRowsOverlay();
                }
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.loadingDisplayStatusSubscription) {
            this.loadingDisplayStatusSubscription.unsubscribe();
        }
    }
}
CoreGridService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreGridService.ctorParameters = () => [
    { type: DataService },
    { type: ToastrUtilsService },
    { type: GridLoadingService },
    { type: SweetAlertService },
    { type: NgZone },
    { type: Router },
    { type: ActivatedRoute }
];
/** @nocollapse */ CoreGridService.ngInjectableDef = defineInjectable({ factory: function CoreGridService_Factory() { return new CoreGridService(inject(DataService), inject(ToastrUtilsService), inject(GridLoadingService), inject(SweetAlertService), inject(NgZone), inject(Router), inject(ActivatedRoute)); }, token: CoreGridService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreGridComponent {
    /**
     * @param {?} gridService
     * @param {?} sweetAlertService
     */
    constructor(gridService, sweetAlertService) {
        this.gridService = gridService;
        this.sweetAlertService = sweetAlertService;
        this.loadOnInit = true;
        this.selectedChanged = new EventEmitter();
        this.doubleClickSelectedChanged = new EventEmitter();
        this.recordEvents = new EventEmitter();
        this.retrievedDataSourceEvent = new EventEmitter();
        this.paginationSelectorSource = [];
        this.paginationSelectorSource = [
            { Id: 1, Definition: 1 },
            { Id: 5, Definition: 5 },
            { Id: 10, Definition: 10 },
            { Id: 25, Definition: 25 },
            { Id: 50, Definition: 50 }
        ];
        this.gridStyle = {
            'height': '500px',
            'margin-top': '-1px'
        };
        this.recordEventsSubscription = this.gridService.recordEventsSubject
            .subscribe((data) => {
            this.recordEvents.emit(data);
        });
        this.retrievedDataSourceEventSubscription = this.gridService.retrievedDataSourceSubject
            .subscribe((data) => {
            this.retrievedDataSourceEvent.emit(data);
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.gridService.gridOptions = this.gridOptions;
        this.gridService.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.isInsideModal && this.isInsideModal) {
            /** @type {?} */
            const height = window.innerHeight * 60 / 100;
            this.gridStyle = {
                'height': height > 500 ? '500px' : height + 'px',
                'margin-top': '-1px'
            };
        }
    }
    /**
     * @return {?}
     */
    load() {
        this.gridService.init(this.gridOptions, true);
    }
    /**
     * @param {?} columnName
     * @param {?} dataSource
     * @return {?}
     */
    setColumnDataSource(columnName, dataSource) {
        this.gridService.setColumnDataSource(columnName, dataSource);
    }
    /**
     * @return {?}
     */
    refreshGridDataSource() {
        this.gridService.refreshGridData(true);
    }
    /**
     * @return {?}
     */
    reloadColumnDefs() {
        this.gridService.reloadColumnDefs();
    }
    /**
     * @return {?}
     */
    isDataUpdated() {
        return true;
    }
    /**
     * @return {?}
     */
    addRow() {
        this.gridService.addRow();
    }
    /**
     * @return {?}
     */
    revertDataSource() {
        this.gridService.revertData();
    }
    /**
     * @return {?}
     */
    getBulkOperationData() {
        return this.gridService.getBulkOperationData();
    }
    /**
     * @param {?} params
     * @param {?} rowId
     * @param {?} key
     * @return {?}
     */
    removeRow(params, rowId, key) {
        this.gridService.removeRow(params, rowId, key);
    }
    /**
     * @return {?}
     */
    refreshAndRevertChanges() {
        this.gridService.refreshAndRevertChanges();
    }
    /**
     * @return {?}
     */
    validateForm() {
        return this.gridService.validateForm();
    }
    /**
     * @return {?}
     */
    showAddButton() {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showAddButton !== undefined) {
                return this.gridOptions.buttonSettings.showAddButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    /**
     * @return {?}
     */
    showReloadButton() {
        if (this.gridOptions && this.gridOptions.buttonSettings) {
            if (this.gridOptions.buttonSettings.showReloadButton !== undefined) {
                return this.gridOptions.buttonSettings.showReloadButton;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    /**
     * @return {?}
     */
    showRevertButton() {
        return this.isChanged() && this.isEditable();
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.gridOptions && this.gridOptions.editable;
    }
    /**
     * @return {?}
     */
    isChanged() {
        return this.gridService.isChanged();
    }
    /**
     * @return {?}
     */
    getTotalCount() {
        return this.gridService.getTotalCount();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSelectionChanged(event) {
        /** @type {?} */
        const selectedRows = this.gridService.getSelectedRows();
        this.selectedChanged.emit(selectedRows);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onRowDoubleClick(event) {
        /** @type {?} */
        const selectedRows = this.gridService.getSelectedRows();
        this.doubleClickSelectedChanged.emit(selectedRows);
    }
    /**
     * @return {?}
     */
    getSelectedRows() {
        return this.gridService.getSelectedRows();
    }
    /**
     * @return {?}
     */
    getSelectedRow() {
        return head(this.gridService.getSelectedRows());
    }
    /**
     * @param {?} data
     * @return {?}
     */
    onFirstValue(data) {
        if (data.Id) {
            this.prevItemsPerPage = data.Id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onChangeItemsPerPage(event) {
        if (this.gridOptions && this.gridOptions.editable) {
            /** @type {?} */
            const bulkData = this.getBulkOperationData();
            if (bulkData && bulkData.length > 0) {
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                    .subscribe((res) => {
                    if (res && res.value) {
                        this.gridService.onChangeItemsPerPage(event);
                        this.prevItemsPerPage = event.Id;
                    }
                    else {
                        this.gridService.pagingResult.PageSize = this.prevItemsPerPage;
                    }
                });
            }
            else {
                this.gridService.onChangeItemsPerPage(event);
                this.prevItemsPerPage = event.Id;
            }
        }
        else {
            this.gridService.onChangeItemsPerPage(event);
            this.prevItemsPerPage = event.Id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onPageChanged(event) {
        if (event.page !== this.currentPage) {
            if (this.gridOptions && this.gridOptions.editable) {
                /** @type {?} */
                const bulkData = this.getBulkOperationData();
                if (bulkData && bulkData.length > 0) {
                    this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                        .subscribe((res) => {
                        if (res && res.value) {
                            this.gridService.onPageChanged(event);
                        }
                        else {
                            this.currentPage = this.gridService.pagingResult.CurrentPage;
                        }
                    });
                }
                else {
                    this.gridService.onPageChanged(event);
                }
            }
            else {
                this.gridService.onPageChanged(event);
            }
        }
    }
    /**
     * @return {?}
     */
    getColumnState() {
        this.gridService.getColumnState();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.recordEventsSubscription) {
            this.recordEventsSubscription.unsubscribe();
        }
        if (this.retrievedDataSourceEventSubscription) {
            this.retrievedDataSourceEventSubscription.unsubscribe();
        }
    }
}
CoreGridComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-core-grid',
                template: "<div class=\"core-grid\">\r\n  <div class=\"core-grid-header\">\r\n    <h5>{{gridService.gridOptions.title | translate}}</h5>\r\n\r\n    <button *ngIf=\"showReloadButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\"{{'Refresh' | translate}}\"\r\n            (click)=\"refreshGridDataSource()\">\r\n      <core-icon icon=\"sync\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showAddButton()\" type=\"button\" class=\"btn btn-sm btn-info\" title=\"{{'Add' | translate}}\"\r\n            (click)=\"addRow()\">\r\n      <core-icon icon=\"plus\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showRevertButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\" {{'Revert' | translate}}\"\r\n            (click)=\"revertDataSource()\">\r\n      <core-icon icon=\"undo\"></core-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n  <!-- suppressColumnVirtualisation = Set to true so that the grid doesn't virtualise the columns.\r\n  So if you have 100 columns, but only 10 visible due to scrolling, all 100 will always be rendered. -->\r\n\r\n  <ag-grid-angular #coreGrid [ngStyle]=\"gridStyle\" class=\"ag-theme-balham\" [rowHeight]=\"40\"\r\n                   [floatingFiltersHeight]=\"40\" [suppressRowTransform]=\"true\" [sortingOrder]=\"['asc', 'desc']\"\r\n                   [sortable]=\"gridService.gridOptions.enableSorting\" [filter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowBuffer]=\"50\" [suppressColumnVirtualisation]=\"true\" [suppressDragLeaveHidesColumns]=\"true\"\r\n                   [ensureDomOrder]=\"true\" [rowData]=\"gridService.data\" [columnDefs]=\"gridService.columnDefs\"\r\n                   [accentedSort]=\"true\" [floatingFilter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowSelection]=\"gridService.gridOptions.rowSelection\" [context]=\"gridService.getContext(gridService)\"\r\n                   [frameworkComponents]=\"gridService.getComponents()\"\r\n                   [noRowsOverlayComponent]=\"gridService.getNoRowsOverlayComponent()\"\r\n                   [loadingOverlayComponent]=\"gridService.getLoadingOverlayComponent()\"\r\n                   (rowDataChanged)=\"gridService.refreshFormControls()\" (gridReady)=\"gridService.onGridReady($event)\"\r\n                   (columnEverythingChanged)=\"gridService.onColumnEverythingChanged($event)\"\r\n                   (gridSizeChanged)=\"gridService.onGridSizeChanged()\"\r\n                   (cellValueChanged)=\"gridService.onCellValueChanged($event)\"\r\n                   (filterChanged)=\"gridService.onFilterChanged($event)\" (selectionChanged)=\"onSelectionChanged($event)\"\r\n                   (rowDoubleClicked)=\"onRowDoubleClick($event)\">\r\n  </ag-grid-angular>\r\n\r\n  <!-- (sortChanged)=\"gridService.onSortChanged($event)\"  -->\r\n\r\n  <div #inputRef class=\"under-grid-footer\">\r\n    <ng-content select=\"[footer]\"></ng-content>\r\n  </div>\r\n\r\n  <div *ngIf=\"gridService && gridService.pagingResult && (gridService.pagingResult.PageSize || gridService.pagingResult.PageSize === 0)\"\r\n       class=\"core-grid-pager\">\r\n    <div>\r\n      <layout-static-selector class=\"pull-left grid-static-layout-selector\" [dataSource]=\"paginationSelectorSource\"\r\n                              [searchable]=\"false\" [clearable]=\"false\" [(ngModel)]=\"gridService.pagingResult.PageSize\"\r\n                              (changed)=\"onChangeItemsPerPage($event)\" (firstValue)=\"onFirstValue($event)\"\r\n                              ngDefaultControl>\r\n      </layout-static-selector>\r\n\r\n      <pagination class=\"grid-pagination\" [totalItems]=\"gridService.pagingResult.TotalCount\"\r\n                  [itemsPerPage]=\"gridService.pagingResult.PageSize\" [(ngModel)]=\"currentPage\" [maxSize]=\"5\"\r\n                  [rotate]=\"true\" [boundaryLinks]=\"true\" previousText=\"{{'PreviousePage'|translate}}\"\r\n                  nextText=\"{{'NextPage'|translate}}\" firstText=\"{{'FirstPage'|translate}}\"\r\n                  lastText=\"{{'LastPage'|translate}}\" (pageChanged)=\"onPageChanged($event)\">\r\n      </pagination>\r\n\r\n    </div>\r\n\r\n    <div>\r\n      <span> <b>{{'TotalRowCount'|translate}}: </b>{{gridService.pagingResult.TotalCount}}</span>\r\n    </div>\r\n  </div>\r\n</div>",
                encapsulation: ViewEncapsulation.None,
                providers: [GridLoadingService, CoreGridService],
                styles: [".ag-cell{overflow:visible!important}.core-grid-header{font-size:12px!important;background-color:#166dba!important;border:none!important;padding:5px;height:37px;border-radius:2px 2px 0 0}.core-grid-header h5{float:left;margin-top:4px;color:#fff;font-size:14px;padding-left:5px}.btn-sm{float:right;margin-left:8px}.ag-header-cell{background-color:#fff!important;color:#175169!important;font-weight:700!important}.ag-cell,.ag-floating-filter-body{padding-top:1px}.ag-cell input,.ag-floating-filter-body input{height:30px!important;margin-top:3px}.ag-floating-filter-body input{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;height:30px!important;margin-top:4px}.ag-cell ng-select input,.ag-floating-filter-body ng-select input{height:19px!important}.ag-floating-filter-button{padding-top:3px}.core-grid-pager{background:#fff;display:flex;align-items:center;overflow:hidden;font-size:14px;margin-top:-1px;border:1px solid #bdc3c7;justify-content:space-between;padding:5px 12px}.grid-pagination{padding:0;float:left;margin-bottom:0}.grid-static-layout-selector{margin-right:12px;margin-bottom:2px}.ag-floating-filter-button{display:none}.grid-filter-icon{position:absolute;right:-22px;top:2px}.under-grid-footer{background-color:#fff;border-right:1px solid #d7dfe3;border-left:1px solid #d7dfe3;border-top:0}.grid-pagination .page-item.active .page-link{background-color:#166dba!important;border-color:#166dba!important}"]
            }] }
];
/** @nocollapse */
CoreGridComponent.ctorParameters = () => [
    { type: CoreGridService },
    { type: SweetAlertService }
];
CoreGridComponent.propDecorators = {
    gridOptions: [{ type: Input }],
    loadOnInit: [{ type: Input }],
    selectedChanged: [{ type: Output }],
    doubleClickSelectedChanged: [{ type: Output }],
    recordEvents: [{ type: Output }],
    retrievedDataSourceEvent: [{ type: Output }],
    loadColumnDataSourceOnInit: [{ type: Input }],
    isInsideModal: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreModalComponent {
    /**
     * @param {?} coreModalService
     */
    constructor(coreModalService) {
        this.coreModalService = coreModalService;
        this.lockBackground = true;
        this.closeOnEscape = true;
        this.maximizable = false;
        this.draggable = true;
        this.closable = true;
        this.showHeader = true;
        this.width = 50;
        this.modalWidth = (window.screen.width) * this.width / 100;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.displayData) {
            if (this.displayData && this.displayData.display) {
                this._display = true;
            }
            else {
                this._display = false;
            }
        }
        if (changes.width) {
            if (this.width) {
                this.modalWidth = (window.screen.width) * this.width / 100;
            }
            else {
                this.modalWidth = null;
            }
        }
    }
    /**
     * @return {?}
     */
    openModal() {
        this.displayData = {
            display: true
        };
        this._display = true;
    }
    /**
     * @return {?}
     */
    closeModal() {
        this.displayData = {
            display: false
        };
        this._display = false;
    }
    /**
     * @return {?}
     */
    onDialogShow() {
        this.coreModalService.setModalDisplayStatus(true);
    }
    /**
     * @return {?}
     */
    onDialogHide() {
        this.coreModalService.setModalDisplayStatus(false);
    }
}
CoreModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-modal',
                template: "<p-dialog appendTo=\"body\" focusOnShow=\"false\" [(visible)]=\"_display\" [(modal)]=\"lockBackground\"\r\n  [(maximizable)]=\"maximizable\" [(draggable)]=\"draggable\" [(closable)]=\"closable\" [(showHeader)]=\"showHeader\"\r\n  [(closeOnEscape)]=\"closeOnEscape\" [autoZIndex]=\"false\" [blockScroll]=\"true\" [baseZIndex]=\"1001\" [width]=\"modalWidth\"\r\n  (onShow)=\"onDialogShow()\" (onHide)=\"onDialogHide()\">\r\n  <p-header *ngIf=\"modalTitle\">\r\n    {{modalTitle | translate}}\r\n  </p-header>\r\n  <div>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</p-dialog>",
                styles: [".active-item:hover{z-index:2;color:#fff;background-color:#007bff;border-color:#007bff;cursor:pointer}::ng-deep .ui-dialog .ui-dialog-titlebar{background:#2d5f8b!important;color:#fff!important;font-weight:700!important;font-size:18px!important;padding:10px 15px!important;cursor:pointer!important;border:none!important}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:not(:hover){color:#fff!important;margin-top:2px}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:hover{margin-top:2px}::ng-deep .ui-dialog .ui-dialog-content{padding:30px!important}::ng-deep .ui-dialog{z-index:1001}::ng-deep .ui-widget-overlay{z-index:1000}:host /deep/ .ng-dropdown-panel.ng-select-bottom{position:fixed!important}"]
            }] }
];
/** @nocollapse */
CoreModalComponent.ctorParameters = () => [
    { type: CoreModalService }
];
CoreModalComponent.propDecorators = {
    modalTitle: [{ type: Input }],
    lockBackground: [{ type: Input }],
    closeOnEscape: [{ type: Input }],
    maximizable: [{ type: Input }],
    draggable: [{ type: Input }],
    closable: [{ type: Input }],
    showHeader: [{ type: Input }],
    displayData: [{ type: Input }],
    width: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// @Inject(DOCUMENT) private document: Document
class CoreOrgChartComponent {
    /**
     * @param {?} ngZone
     */
    constructor(ngZone) {
        this.ngZone = ngZone;
        this.clickedExport = new EventEmitter();
    }
    /**
     * @param {?} simpleChanges
     * @return {?}
     */
    ngOnChanges(simpleChanges) {
        if (simpleChanges.export && this.export) {
            this.onExport();
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.datasource = {
            name: 'A',
            title: 'Yönetim Kurulu Başkanı',
            className: 'org-node-blue',
            gender: 'M',
            office: 'Ankara',
            imgUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAvCAYAAABOmTCPAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAEmJJREFUeJztnQtUU1e6gK3WKgVR3oS8ICQQEgLhaZAY5SUvBQGFKmqtiIooUh+oKFqtU2d0zbTT1lerrk7vmtVqOzNt1207Tqe3qzO3M7Vzrd65szq9vY7kvPMgBAStWvXcvU8IQnJyEsAAsfxr7RUXyb/3f/79nX+//nOcNGlCJmRCHgipLTxEyNSfEgnpv2cruET1KZmqvdC5tknnbVuun3rd31hU/htkRoQZi074FyaWsxe+jKCy87c56mMiVQIWLf8O/Kbdpa5AhhLSpHYiRZvlqG9pPRCPy1P+jvGlqCt9NCoWxWVJ7abyJWXe9gdVvDQfF8gITBjn8npwsRzDoxVfYGJFlLft8SkhYhP/jIYIaSw8mrWggZE0Lk6gO5atXu5tW3qOnQg05pVcQZ4IorEoKY3xYtlLmJgmM3RHHPWR8Og0lBfjWg8WcE24UE4TiowCR31ry/5kXKK8C+t3pY+GiGgAFG0sKVvlbX8YcxZVMf0QwX1NaKSkE42IEXvbHp8S4JQLIALaQGIpaIiAxmNVdEdtXYW3bek5fjLAmF/6V2RaMM1lE+xsMnPe8476SGSsGkTUOy71YAGQ4KIEmlBm5jjqW3fuV+GxiT0MTK78EQqgj06gjaXly7ztD0NeWRkDdaTE9fVAm3ixGChCb9vjU4LwJJ9wOi24H+xKb9vykMD+YQLsCZlEZuo+w4KFLkGaAHsCbJ8Uo27Bn9BZggmwJ02A/UgJlamzLR4nwPZlsNEJsB2ESM76AoW7AOMA7N4TJwMB2N95BHaG7iVH/ZGC3dmyLxWATeMegG0qLVvjbX9QmvwaBmoOsHEb2LeRSEmMt+3xKSHU2QBs0bgAuxtEbEN+yZv6aUH/jfKllwCk7CVcfIWcnbvJUX+kYHft3h8HwP5PJFx82VXbSJjoMhot/5oqqSz1tj+My1bngfauoBHRX7u0JyoW2Cr9BOXLeN62x6cEl6d+yTn0jiLYUIj8Ur9r04Nn6gVSfz0/lr1EiGYYsoumOuqOFOwbew9OxqQq/2vhogCXbYcLA5DohACsqGaKt31hffkXk/XBUYH6cLFLe65FSWaggjg/MlrxmLft8SnBpaqvmHkcF9jSJLqjspYVbGJuwePEvAV+hDbXj8wueHy07R8oIwV7Qh4IpcmdbK7bwDM1bk0xrducbV6/Kd3U0urxqGAsr5lmfbY1xlD9dKa5EeivbUw1Vq8K96bNgwSXJbkHO0ZJW5av6QfbUFIZb6zbsI7KLngVT9Z8hKdoLuJJmReJJM2HBl3hcUNpdZOxqlZjrm+YNhRbbr569gmwYFrS7jdrCyqUNqKCWPYSKd6szyrSOOqPFGxz66EQTJZUj0aINrlqG0TsTWiMvNFYUhXnyTUZ122aQWbqVIZFNSuMRVVtVMa8X1KFC08bK6rP4uqs49T8wkNUbvEqQ/lSpzmyde/hGCRUsBmJELv2BV+yGUTs1YhIMcNRv+vgIb5l+y5tx47dGtby7HadZc++tO4LFwL67a1aLuh4um4dmZL9OzBSk5gw/g4mkN0Dnz9gsYkkqc1701zX4HRq26+/ZEUaVVy5n0jK/DNYi1gxYdxdTAj0BXF3QIBEyPmFZ81bd+R64rsRCSqQ/RfG41hxz+LTwKgfeo68OK/zhZ8qiaycX+PihOvozEgaDQLfhYL5eYjQdtQMFqHwb8zfeRKazM77jMwpWmNpevYJT2zpOX4CLh6/9WxXRPeio/7Id0X2pcDFo2e7ImV1XNdCli0XE3Ny28is3C/x6IS7cOcJDYpi/IXO5NFoQAT4N/BXsJBGZvFoImXONx0NzaGD6phTUOPhrsgtlGXxSGpy2tBw0B4vhrUgAWGg3SxL1yuvhnbUN042Ln6qDZMmIkhgJBPQ+o/zYfsRElv/QtuDBLShuOLgwLaMzS0CQ27pafD7u4x+qBDoRD/QhZ99KRoIqJssXnzGsn1PgKPND03QyJhLnCDA3AqR/IaxsOxjXKKkEL9QALLI9h2Ej61EgRIOHOcfwegTaXO+ImtWuR36H5XtPqK4fCMmS9ZjoAMZgOGuE4e/UNgeX9Zh1BUP2rIb6T62IX1eG7xxsLBo1oL4h9OkOktv3b6rFE/M+JABclbUA1vZ2oP2ArCRJ8Npy/rG3bCd7qNHs1FxwjUU1IeGRQ/ggE0/juFHP3UmvDl+8/35cx4FvSELypNwg81nPu8j8E6F0ZvLaBYnQEigA5FZgu+tO1pXc9nyKIBtqd/4cyQwgolsGExS8sBfEGww7GPmhdWDMvRGDLYmtxXlmGYyRSTvxWJV3f2JcJ70rTCeRgLB9cUoblp3tu4kU7Pa0SfDuG+IgUUQB/wootunBNLWHS173EI6HAFz7Ctcc+z+AjvJE5hZC7jLgSOQgMj7vadPVruyxdfBtjbvaNBPC2Eimsc3vx1sSSJmLl06umDDoAXqhpD134Se9mlf0IJTUGZU4A+RCQg3GB1QUXzX9b37ZJ4T66EQiRn/QD29U0dS7Hd5eEx3z7HjKjZbfBnsnpdf5ZGJ6RQyPYTptKH4ZszAfhhlJAEP+AmO5pY161qGTq4bIRRp/zMqYPfBrQfQGhcs/OyHP/7eaR/Yl8E2121cq/cLGVpHM/nUA6Yivgj2SAozXxfSZNb8PwyPXg4ZVbAZKMGiEixSrAcOVTna4stgG6trTyDTPIjWsDPhzsgMMA+HOyVgOEb8w+AC3QzAHrRP/MiDDQu4Nlyi/HZ49HIIEaf+BsI2ZLCddkI814XDD5Vb+ImjLb4KNpGR/TguV3+AhbkJEHxb+2DBdoeau+B9SpPzAjU37wyePPsyIVZgFm3hoC07nwPbfu1DmW+DUQvc1O3DJ9iF4NKkfzL7jJ4aDp0MFxtw4QAXSXDhACNQqNjji4G7K3hcUk/nrlb1QFt8FWxMkTIVi1X+O7NvG+VmBwREafPKZ3YMbNeyfsss85aWLP1zh4MG/n1UwYaPmMFtSeZMQkh7zITdBnhTQz24mIR1QEag3e6Cng3sa8Nhl1NwmYdgwyG0b4Mej0tuJ9LnHrfUb9xqqlm5G1dnvYtLVT0IBD3CffRn6gE3RMe6DYMSmXwVbAaigoVn4JSC2afluvZgAW1tafEogWpUwIZ+Br+BfQLm+RYiWfNXIjH9G2gnVzrzIH3oEzilyNS9ZVq/eaX5mfV1aKzyU2b/3h3cYwp2X6SGHQfmkr80b93plDNg2bRV0x4i+htzYsVxkmm/GLjBbyyrOjuwDl8Gu2P9pia3i0cYHMBoReUV/an35DG3SVSjAjbc7gM2k6rM8x2NzQnMtezZP920uq4VE8qYE1N3QQqMVHese/YPemuAZc8+f+PCqj8g00PHKdh98yUIG4jQr3DVdf3s6TAqO+8qnENzOhM6ZAaPptLnft65paW/g3tOnPJZsHt/e04OAOlmrp1rAcmT2La4Vq05465vvL+PLaMRsIglNPP/YWna7nQCaF5e+y7iF8Z9s4KRF6wv2i07WwOd/Hn4aBU82GOmruMPbHDx4OJwVcbVO19+EeSuPutPD9cgwYK7zIKUq87ASJpM116xNG/vr9OXwYbS2dj0QvtjMx4cK7uK2iAKwlM3y8bmw1y+HI2IjQbzaTJF+z5b+6blzzQxbHAlycF+lqmuGgoKZzrqm5c/rcOi5beYvJFRBztG+Z07x+mnh9CmiprXPKmv98yZIEKRhrsbwrAQMKeLT/6OKCgS9Oueet2nwb715edPUEVlH+unzOQ+noYHE2BKon98Jt21/9B2V74cFbBBNCWUsy+wtY/Hp61nroEjYjM3sUx1zVReEeLUH0Hi+UD/BucuidfAFidc5TIcfgc7k9TltXpSX2fbwamkOusSyjX8wKgVEE4Tadnt5m27+re4ek++5tNgQ+l6+XgokTr3PPJkKM1A5WpaAo+UYTJRYOT9ntPHWBeTowI2CECESsMKNljgb+T0pRuw0fDYHLDeuslZh9fAFiX8ixNsUJCACLjQc0oTZZOuX70RQKg1V5ltQC6wYWZZWrbeumOXxK7b8wiAbRfjqrW7QVS+z2yJcsEdGEXjArmx9+Rpp7TTsQYbkygaUOZ3w4vYaKRsDMGWKP+P03FwPgxAIzQ5l25dueg2xfD7U6fyQH230TDufW2UWXSk/K+ppJxv132UwIZiKCpbhYvj76Eh3KMXPLE0Lq0976Q/xmBTGu065neRvgh2gvrvnNl9cH4Ej8Gnh9Ldx17a7K6+juLy/9DDLR43215wNU6mzf26s3mXv13X1xePrP6oW/dvcI3CBSeM6mDqdrv76ItzBuqONdiGovJ622kph+3jFWwyc+6HTEThPAq25TeAC7zZuXtfPVs9XceOhxpKK34NgXWbBgv3sacH06bSqncH1tFz7MQjB7b5mboyNFzMvUsEzwjANZuqV54cqDvWYHc0PbvGdjDH4Y/xCra5uPIIkxTvLnnHvtgBhUjN/oTUFjR0bN6abVpVX0DOyTtAJGX+sz8yCdwn1iMBkbS1oal5oC2+DLb54pXJbP4FgaMB+O6eu0MwJn8mc97l3heO9D8uNdZgU4uXrkT50vuc233jFezOnburmTxprsbtxZ6ZNosHFjzgRohW3MCjFbdtz7LBOjxLVoe/xaWq611HX1QMtMWXTx57P74wuWPDljYiXfscVV5d2HnwULZhyYq1WIyCsNXHvUDvexuA2bywMtFe51iDjYjkNeA3dzn1xyvYN869G4GJlRSEdUgJ8jC/AHayPR/AUz0YncAc3FhWdc7RFl8GG4q58qkL7ZMDmCNq5rf294x7EjDASIjHJd+gKqv7X0g/1mCTReWV8EFuzvXBeAUbimVD40/0kwOHBOewSl8HgqlKb+9rp5Ic7fB5sJfWvqeHL62HYA8lOw76hYnYqm5qUWW6vb7ROaARujygAaPOIkyccJvrTGJcg3378wsRWFzyNf2UWUN+rGkoncc82Dsjku7e27aFzY5HAuypQcPyDXz6n8zQXun6+Ut+9vpGA2xkZhRNZeV8zNYfZMnifEwYd9Mnd0XsAiJoFXxsCx7GuFv8DbkwTyWLmSmIoaLmlCsbfrRgAzjgMXzXrtZBp7ujAjYINIa84o/Y+gNXpukACzc43zsz3sGG0rW3dTvchmNSTx9W5O6bfsAXpVDzik93NG1z+Rq0HyXYIIjA6Quhyrh069z5QYlEowU2Nb+QFezOfc9p8FhFD8Zx2OYTYEOxbNm6AQkT9zJvcxriopANIuYRe77sprF29T53bf+owO5LLkJgzow689tbH33g9AqCUQEbjNBULnvEtu7ck4LFKLvR8EcAbCjGhi1ZRELqV3CIRMEcbNB7JzjSMZlPmLgOn76wv+YsXfvHzpa9c9y3+jDAlowp2KbF1Rc8WoQz26Zi5i1RhCLt4u0P3pOy+cPrYIM+Zd4aUPkUa9oqKs9QgpHbig0zuw+NkBZ48tAJLpSbXDHx0KVjxVp/Y1HlZjxp9mVMGH8X9Q+zPVkdLLQ9NcE8ORHT97os2zv74LDGPDomSbQSyZrPDEtqa4bSZs8rxwIN8wra9ZOetO2X9z1/51RAG4Q6y+mhB/3MqEwkhO9ar++pcLgYwqXJJY761m17UsFiyfZ+Ohf6zDVGSWjDgmKnE9iOlWve0T8WwCR4wYDg8uaEIxlP0mMsWnzMWLbUZY67QVu4jBnxgrivCb4LDwniS5z0U7U/Q6C+K12gp5/kR5sqqv/C1r6pfqMCF8Uz00iXbQcCX4nivjeWLAxz1Ed48flolLQXBJt74PMOa+FJ7oGIjbjygdfEsnn7FNPSlYVUec1REqyecblaj0kSb+NiOQM1/D8gAcjXcdXsK+TcgreoqmXbTPWb0obTVvcbb/oZl654HhPL3yFS5rwN4GUviRnnDMWVTjcNkqqLBt+/5VIPFlXmOTJd9zaVU5roqG9+/oiYnJ3zBqzflT6uTD9HZuje7qhdo3XU72o7oCSyC/aQugXHSbXmsqtjdObxsNwi1ig5UIzL1mZAe4mk2a6vR605h6vnnMBTtKGO+tSi6goc6rvSTda8DcB9x7x2I+sLa3rfez+cml/0Oi5Pce2PxMzzVFbOy50rnnZ626telBiE8KW5CF9SjPBj81kLL6YQi1V4NKJ7VQz5pSJCmaGm8hdqjA3NWtCJmcDxSkNFbfBY2zaexHLwJ/XMCzwdh3GY324D2ymbb0ImZNyL5fCRjcxawRXYOYW/G2sbJ2RChiyWfQebEban1h+A/duxtnFCJmTIYmk70ML8n/D2XQ17gQtHsOCi5i1gPcaekMHy/wuA7v+MZ1yPAAAAAElFTkSuQmCC',
            children: []
        };
        /** @type {?} */
        const nodeTemplate = (data) => {
            /** @type {?} */
            let office = ``;
            /** @type {?} */
            const title = `<div class="title">${data.name}</div>`;
            /** @type {?} */
            let content = `<div class="content" title="${data.title}"> ${data.title} </div>`;
            if (data.gender) {
                if (data.gender === 'F') {
                    content = `
          <div class="content">
          <img src="./assets/images/user-female.png" class="org-node-pic"> ${data.title}
          </div>`;
                }
                else {
                    content = `
          <div class="content">
          <img src="./assets/images/user.png" class="org-node-pic"> ${data.title}
          </div>`;
                }
            }
            if (data.imgUrl) {
                content = `
        <div class="content">
        <img src="${data.imgUrl}" class="org-node-pic"> ${data.title}
        </div>`;
            }
            if (data.office) {
                office = `<span class="office">${data.office}</span>`;
            }
            /** @type {?} */
            let template = '';
            template += office;
            template += title;
            template += content;
            return template;
        };
        // dragable false çünkü ekrandaki order bozuluyor sapıtıyor
        this.orgChart = $('.chart-container').orgchart({
            data: this.datasource,
            nodeTemplate,
            verticalLevel: 5,
            visibleLevel: 4,
            nodeContent: 'title',
            exportButton: false,
            // exportFilename: 'MyOrgChart',
            // exportFileextension: 'png',
            draggable: false,
            pan: false,
            zoom: false,
        });
    }
    /**
     * @param {?} orgChart
     * @param {?} datasource
     * @return {?}
     */
    refreshOrgChart(orgChart, datasource) {
        // $('.chart-container').empty();
        orgChart.init({ data: JSON.parse(datasource) });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    fileChange(event) {
        /** @type {?} */
        const fileList = event.target.files;
        if (fileList.length > 0) {
            /** @type {?} */
            const file = fileList[0];
            /** @type {?} */
            const formData = new FormData();
            formData.append('uploadFile', file, file.name);
            if (file) {
                this.readFile(file, this.refreshOrgChart, this.orgChart);
            }
        }
    }
    /**
     * @param {?} file
     * @param {?} refreshOrgChart
     * @param {?} orgChart
     * @return {?}
     */
    readFile(file, refreshOrgChart, orgChart) {
        /** @type {?} */
        const reader = new FileReader();
        reader.readAsText(file, 'UTF-8');
        reader.onload = (evt) => {
            /** @type {?} */
            const datasource = ((/** @type {?} */ ((evt.target)))).result;
            this.datasource = datasource;
            refreshOrgChart(orgChart, datasource);
        };
        reader.onerror = (evt) => {
            console.log('error reading file');
        };
    }
    /**
     * @param {?} zoomType
     * @return {?}
     */
    onClickZoom(zoomType) {
        console.log(zoomType);
        console.log(this.orgChart);
        /** @type {?} */
        let currentZoom = parseFloat($('.orgchart').css('zoom'));
        this.orgChart.setChartScale(this.orgChart.$chart, zoomType === '+' ? currentZoom += 0.2 : currentZoom -= 0.2);
    }
    /**
     * @return {?}
     */
    onClickExport() {
        this.clickedExport.emit({ data: true });
    }
    /**
     * @return {?}
     */
    onExport() {
        this.orgChart.export('organization-schema', 'png');
    }
    /**
     * @return {?}
     */
    onClickResetAll() {
        this.ngZone.run(() => {
            $('.orgchart').css('transform', ''); // remove the tansform settings
            this.orgChart.init({ data: this.datasource });
        });
    }
}
CoreOrgChartComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-org-chart',
                template: "<input id=\"file-upload\" type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".json\">\r\n\r\n\r\n\r\n<div class='chart-container'>\r\n</div>\r\n\r\n<div class=\"fixed-operation-bar\">\r\n\r\n  <!-- <label for=\"file-upload\" class=\"btn btn-primary   fl-l\">\r\n    <core-icon class=\"main-icon\" icon=\"upload\"></core-icon>\r\n    {{ 'FileUpload' | translate}}\r\n  </label> -->\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickResetAll()\" class=\"btn btn-danger ml-10 fl-r\" title=\"{{'Reset'| translate}}\">\r\n    <core-icon icon=\"times\"></core-icon> {{'ResetAll' | translate}}\r\n  </button> -->\r\n\r\n  <button type=\"button\" (click)=\"onClickExport()\" class=\"btn btn-primary ml-10 fl-r\" title=\"{{'Export'| translate}}\">\r\n    <core-icon icon=\"download\"></core-icon> {{'Export' | translate}}\r\n  </button>\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickZoom('-')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomOut'| translate}}\">\r\n    <core-icon icon=\"search-minus\"></core-icon> {{'ZoomOut' | translate}}\r\n  </button>\r\n\r\n  <button type=\"button\" (click)=\"onClickZoom('+')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomIn'| translate}}\">\r\n    <core-icon icon=\"search-plus\"></core-icon> {{'ZoomIn' | translate}}\r\n  </button> -->\r\n</div>",
                styles: [".chart-container.canvasContainer{position:initial}:host /deep/ .orgchart{background:#fff}input[type=file]{display:none}.btn{cursor:pointer}:host /deep/ .chart-container.canvasContainer{position:initial}:host /deep/ .orgchart .node .content{height:auto;padding:5px}:host /deep/ .orgchart .node .content .org-node-pic{width:40px}:host /deep/ .orgchart .node{width:auto}:host /deep/ .orgchart .node .office{font-weight:600}:host /deep/ .orgchart .node .title{padding:1px 5px}:host /deep/ .orgchart .node .title .symbol{margin-top:2px;margin-left:0}:host /deep/ .orgchart .node .title .fa-group:before,:host /deep/ .orgchart .node .title .fa-users:before{padding-right:4px}.chart-container{zoom:1}:host /deep/ .org-node-blue .title{background-color:#007bff}:host /deep/ .org-node-blue .content{border-color:#007bff}:host /deep/ .org-node-blue-light .title{background-color:#e9f5ff}:host /deep/ .org-node-blue-light .content{border-color:#e9f5ff}:host /deep/ .org-node-blue-dark .title{background-color:#166dba}:host /deep/ .org-node-blue-dark .content{border-color:#166dba}:host /deep/ .org-node-blue-extra-dark .title{background-color:#15233d}:host /deep/ .org-node-blue-extra-dark .content{border-color:#15233d}:host /deep/ .org-node-purple .title{background-color:#8622cd}:host /deep/ .org-node-purple .content{border-color:#8622cd}:host /deep/ .org-node-pink .title{background-color:#e83e8c}:host /deep/ .org-node-pink .content{border-color:#e83e8c}:host /deep/ .org-node-red .title{background-color:#e30a3a}:host /deep/ .org-node-red .content{border-color:#e30a3a}:host /deep/ .org-node-orange .title{background-color:#fd7e14}:host /deep/ .org-node-orange .content{border-color:#fd7e14}:host /deep/ .org-node-orange-light .title{background-color:#ffb822}:host /deep/ .org-node-orange-light .content{border-color:#ffb822}:host /deep/ .org-node-yellow .title{background-color:#ffc107}:host /deep/ .org-node-yellow .content{border-color:#ffc107}:host /deep/ .org-node-yellow-light .title{background-color:#fff589}:host /deep/ .org-node-yellow-light .content{border-color:#fff589}:host /deep/ .org-node-green .title{background-color:#28a745}:host /deep/ .org-node-green .content{border-color:#28a745}:host /deep/ .org-node-indigo .title{background-color:#6610f2}:host /deep/ .org-node-indigo .content{border-color:#6610f2}:host /deep/ .org-node-teal .title{background-color:#20c997}:host /deep/ .org-node-teal .content{border-color:#20c997}:host /deep/ .org-node-cyan .title{background-color:#17a2b8}:host /deep/ .org-node-cyan .content{border-color:#17a2b8}:host /deep/ .org-node-gray-dark .title{background-color:#343a40}:host /deep/ .org-node-gray-dark .content{border-color:#343a40}"]
            }] }
];
/** @nocollapse */
CoreOrgChartComponent.ctorParameters = () => [
    { type: NgZone }
];
CoreOrgChartComponent.propDecorators = {
    export: [{ type: Input }],
    clickedExport: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BreadcrumbComponent {
    /**
     * @param {?} breadcrumbService
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} ngZone
     */
    constructor(breadcrumbService, router, activatedRoute, ngZone) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.ngZone = ngZone;
        // subscribe to the NavigationEnd event 
        this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
            .subscribe(event => {
            // set breadcrumbs
            /** @type {?} */
            const root = this.activatedRoute.root;
            this.breadcrumbService.breadcrumbs = this.getBreadcrumbs(root);
        });
    }
    /**
     * @private
     * @param {?} route
     * @param {?=} url
     * @param {?=} breadcrumbs
     * @return {?}
     */
    getBreadcrumbs(route, url = '', breadcrumbs = []) {
        /** @type {?} */
        const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
        // get the child routes
        /** @type {?} */
        const children = route.children;
        // return if there are no more children
        if (children.length === 0) {
            return breadcrumbs;
        }
        // iterate over each children
        for (const child of children) {
            // verify primary route
            if (child.outlet !== PRIMARY_OUTLET) {
                continue;
            }
            // verify the custom data property 'breadcrumb' is specified on the route
            if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
                return this.getBreadcrumbs(child, url, breadcrumbs);
            }
            // get the route's URL segment
            /** @type {?} */
            const routeURL = child.snapshot.url.map(segment => segment.path).join('/');
            // append route URL to URL
            url += `/${routeURL}`;
            /** @type {?} */
            let currentUrl = this.router.url;
            // /organization/operations/company
            /** @type {?} */
            const indexUrl = currentUrl.indexOf(url);
            /** @type {?} */
            let parentUrl = null;
            if (indexUrl !== -1) {
                if (indexUrl === 0 && url === '/') {
                    currentUrl = currentUrl.substring(1);
                    /** @type {?} */
                    const secondIndex = currentUrl.indexOf('/');
                    if (secondIndex !== -1) {
                        parentUrl = currentUrl.substring(0, secondIndex);
                    }
                }
                else {
                    parentUrl = currentUrl.substring(0, indexUrl);
                }
            }
            url = url.replace('//', parentUrl ? parentUrl + '/' : '/');
            if (url === '/' && parentUrl) {
                url = '/' + parentUrl;
            }
            // url = url.replace('//', this.mainRout ? '/' + this.mainRout + '/' : '/');
            // if (url === '/' && this.mainRout) {
            //   url = '/' + this.mainRout;
            // }
            // add breadcrumb
            /** @type {?} */
            const breadcrumb = {
                label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
                url: this.getFullPath(child.snapshot),
                customLabel: null
            };
            if (breadcrumbs.findIndex(x => x.label === breadcrumb.label && x.url === breadcrumb.url) === -1) {
                breadcrumbs.push(breadcrumb);
            }
            if (child.snapshot.params && child.snapshot.params.id) {
                /** @type {?} */
                const moreBreadcrumb = {
                    label: child.snapshot.params.id,
                    url: this.getFullPath(child.snapshot),
                    customLabel: null
                };
                // find from list and update moreBreadcrumb according to that
                /** @type {?} */
                const breadcrumbsWithCustomLabel = this.breadcrumbService.breadcrumbsWithCustomLabels.find(x => x.label === moreBreadcrumb.label && x.url === moreBreadcrumb.url);
                if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    moreBreadcrumb['customLabel'] = breadcrumbsWithCustomLabel.customLabel;
                }
                /** @type {?} */
                const foundBreadcrumb = breadcrumbs.find(x => x.label === moreBreadcrumb.label && x.url === moreBreadcrumb.url);
                // check exist if not push, if exist then maybe customLabel was changed and update it 
                if (!foundBreadcrumb) {
                    breadcrumbs.push(moreBreadcrumb);
                }
                else if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                    foundBreadcrumb.customLabel = breadcrumbsWithCustomLabel.customLabel;
                }
            }
            // recursive
            return this.getBreadcrumbs(child, url, breadcrumbs);
        }
        // we should never get here, but just in case
        return breadcrumbs;
    }
    /**
     * @param {?} route
     * @return {?}
     */
    getFullPath(route) {
        /** @type {?} */
        const relativePath = (segments) => segments.reduce((a, v) => a += '/' + v.path, '');
        /** @type {?} */
        const fullPath = (routes) => routes.reduce((a, v) => a += relativePath(v.url), '');
        return fullPath(route.pathFromRoot);
    }
    /**
     * @param {?} url
     * @return {?}
     */
    onClick(url) {
        this.ngZone.run(() => {
            this.router.navigate([url]);
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    }
}
BreadcrumbComponent.decorators = [
    { type: Component, args: [{
                template: `
  <ol *ngIf="breadcrumbService.breadcrumbs && breadcrumbService.breadcrumbs.length > 0" class="breadcrumb" [class.fixed-breadcrumb]="isFixed">
    <li *ngFor="let breadcrumb of breadcrumbService.breadcrumbs" (click) ="onClick(breadcrumb.url)"  title="{{breadcrumb.label | translate}}">
      <a><span *ngIf="!breadcrumb.customLabel">{{breadcrumb.label | translate}}</span><span *ngIf="breadcrumb.customLabel">{{breadcrumb.customLabel | translate}}</span></a>
    </li>
  </ol>
  `,
                selector: 'breadcrumb',
                styles: [".breadcrumb{margin-bottom:0;background-color:transparent;padding:0 0 0 2px;font-size:10px;font-weight:600}.breadcrumb li{cursor:pointer;color:#333!important}.breadcrumb li a{color:#333!important}.breadcrumb li:not(:last-child):after{content:'\\f054';font-family:FontAwesome;font-style:normal;font-weight:400;text-decoration:inherit;margin-left:10px;margin-right:10px;color:#455a64!important;position:relative;top:1px}.breadcrumb li:last-child{font-weight:600}.fixed-breadcrumb{height:35px;width:calc(100%);background-color:#fff;position:fixed;margin-left:-22px;z-index:49;top:45px;box-shadow:0 5px 5px -2px rgba(0,0,0,.2);padding-left:30px;padding-top:10px}"]
            }] }
];
/** @nocollapse */
BreadcrumbComponent.ctorParameters = () => [
    { type: BreadcrumbService },
    { type: Router },
    { type: ActivatedRoute },
    { type: NgZone }
];
BreadcrumbComponent.propDecorators = {
    isFixed: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormCheckboxComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.checkChanged = new EventEmitter();
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) { }
    /**
     * @return {?}
     */
    onTouched() { }
    /**
     * @return {?}
     */
    get inputValue() {
        return this.value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this.value = val;
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onCheckChanged(event) {
        this.checkChanged.emit(event);
    }
}
FormCheckboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-checkbox',
                template: `
	<div class="form-checkbox" [class.disable-selection]="isReadOnly">
    <p-checkbox
    [hidden]="hidden"
    [(ngModel)]="inputValue"
    [disabled]="isDisabled"
    binary="true"
    (onChange)="onCheckChanged($event)"
    >
    </p-checkbox>
	</div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormCheckboxComponent),
                        multi: true
                    }
                ],
                styles: [":host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-chkbox .ui-chkbox-box{opacity:.6;border:1px solid #a9a9a9!important;background-color:#ced4da!important;color:#000}"]
            }] }
];
FormCheckboxComponent.propDecorators = {
    value: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    checkChanged: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormTextInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
        this.numberOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeypress(event) {
        if (this.disableSpace) {
            if (event && event.code === 'Space') {
                return false;
            }
        }
        if (this.numberOnly) {
            /** @type {?} */
            const charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        return true;
    }
}
FormTextInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-text-input',
                template: `
  <div style="width:100%;">
    <input type="text"
           class="form-control form-control-sm fixed-height-input form-input"
           name="inputValue"
           autocomplete="off"
           [(ngModel)]="inputValue"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           [class.pr-25]="clearable && _value"
           [attr.maxlength]="maxLength"
           placeholder="{{placeholder|translate}}"
           [hidden]="hidden"
           (keypress)="onKeypress($event)"
           >
    <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormTextInputComponent),
                        multi: true
                    }
                ],
                styles: [""]
            }] }
];
FormTextInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    clearable: [{ type: Input }],
    disableSpace: [{ type: Input }],
    maxLength: [{ type: Input }],
    changed: [{ type: Output }],
    numberOnly: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormNumberInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.maxLength && this.maxLength) {
            /** @type {?} */
            let value = '';
            for (let i = 0; i < this.maxLength; i++) {
                value += '1';
            }
            this.maxValue = parseInt(value, 10) * 9;
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyUp(event) {
        if (this.maxValue && this.maxLength && this.inputValue && this.inputValue > this.maxValue) {
            this.inputValue = parseInt(this.inputValue.toString().slice(0, this.maxLength), 10);
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    numberOnly(event) {
        if (event && event.code === 'KeyE') {
            return false;
        }
        return true;
    }
}
FormNumberInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-number-input',
                template: `
    <div style="width:100%;">
      <input type="number"
             class="form-control form-control-sm fixed-height-input form-input"
             name="inputValue"
             autocomplete="off"
             [(ngModel)]="inputValue"
             [disabled]="isDisabled"
             [readonly]="isReadOnly"
             [class.pr-25]="clearable && _value"
             (keyup)="onKeyUp($event)"
             (keypress)="numberOnly($event)"
              placeholder="{{placeholder|translate}}"
             [hidden]="hidden">
      <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
    </div>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormNumberInputComponent),
                        multi: true
                    }
                ],
                styles: ["input::-webkit-inner-spin-button,input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}"]
            }] }
];
FormNumberInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }],
    maxLength: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormPasswordInputComponent {
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
}
FormPasswordInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-password-input',
                template: `
    <input type="password"
           class="form-control form-control-sm fixed-height-input form-input"
           name="inputValue"
           placeholder="{{placeholder|translate}}"
           [(ngModel)]="inputValue">
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormPasswordInputComponent),
                        multi: true
                    }
                ]
            }] }
];
FormPasswordInputComponent.propDecorators = {
    placeholder: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormCurrencyInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.changed = new EventEmitter();
        this.clearable = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    }
}
FormCurrencyInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-currency-input',
                template: `
  <div style="width:100%;">
    <input currencyMask
           class="form-control form-control-sm fixed-height-input form-input"
           autocomplete="off"
           name="inputValue"
           [(ngModel)]="inputValue"
           (keydown)="onKeyDown($event)"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           placeholder="{{placeholder|translate}}"
           [class.readonly-input] = "isReadOnly"
           [class.pr-25]="clearable && _value"
           [hidden]="hidden">
     <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>

  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormCurrencyInputComponent),
                        multi: true
                    }
                ],
                styles: [".readonly-input:focus{outline:0;box-shadow:none}"]
            }] }
];
FormCurrencyInputComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    changed: [{ type: Output }],
    clearable: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormTextAreaComponent {
    constructor() {
        this.isReadOnly = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        if (this.isReadOnly) {
            event.preventDefault();
        }
    }
}
FormTextAreaComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-textarea',
                template: `
    <textarea name="inputValue" class="form-textarea"
              id="inputValue" style="min-width: 100%;"
              rows="5"
              [attr.maxlength]="maxLength"
              [class.readonly-input] = "isReadOnly"
              [readonly]="isReadOnly"
              [(ngModel)]="inputValue"
              (keydown)="onKeyDown($event)"
              placeholder="{{placeholder|translate}}"
              ></textarea>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormTextAreaComponent),
                        multi: true
                    }
                ],
                styles: [".readonly-input{background-color:#e9ecef;cursor:default}"]
            }] }
];
FormTextAreaComponent.propDecorators = {
    placeholder: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    maxLength: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormDatepickerInputComponent {
    /**
     * @param {?} fb
     * @param {?} translate
     */
    constructor(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.isReadOnly = false;
        this.dateChanges = new EventEmitter();
        this._selectionMode = DateSelectionMode.Single;
        this.onChange = () => { };
        this.onTouched = () => { };
        this.view = 'date';
        this.yearDataSource = [];
        this.defaultSettings = new DateSettings();
        this.formGroup = this.fb.group({
            Year: null
        });
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange.subscribe((event) => {
            if (event) {
                this.locale = event.lang === 'tr' ? this.localeTr : this.localEn;
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.selectionMode && this.selectionMode) {
            this._selectionMode = this.selectionMode;
        }
        if (changes.settings && this.settings) {
            this.defaultSettings = assign({}, this.defaultSettings, this.settings);
            if (this.settings.selectionMode && !this.selectionMode) {
                this._selectionMode = this.settings.selectionMode;
            }
            if (this.settings.showYear) {
                this.defaultSettings.dateFormat = 'yy';
                this.initiateYearDataSource();
            }
            if (this.settings.showTimeOnly) {
                this.defaultSettings.disableUTC = true;
                this.defaultSettings.hideCalendarButton = true;
                this.defaultSettings.showButtonBar = false;
            }
            if (this.settings.showTime) {
                this.defaultSettings.disableUTC = true;
            }
            if (this.settings.showMonthPicker) {
                this.view = 'month';
            }
            if (this.settings.minDate) {
                this.minDate = this.convertDateToDateUTC(this.settings.minDate);
            }
            if (this.settings.maxDate) {
                this.maxDate = this.convertDateToDateUTC(this.settings.maxDate);
            }
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        if (value) {
            if (value.indexOf('T') !== -1 && !this.defaultSettings.disableUTC && !this.defaultSettings.showTimeOnly && !this.defaultSettings.showTime) {
                // can be done via moment
                /** @type {?} */
                const splittedValue = value.split('T');
                splittedValue[1] = '00:00:00.000Z';
                value = splittedValue.join('T');
            }
            if (this.selectedDateString !== value) {
                // due to UTC, without being influenced timezone changes, it creates date according to value
                // in order to avoid reflecting timezone changes to UI
                // console.log('value ', value);
                // console.log('moment(value).toDate() ', moment(value).toDate());
                // console.log('this.convertDateToDateUTC(value) ', this.convertDateToDateUTC(value));
                if (!this.usedMometToDate && (this.defaultSettings.showTimeOnly || this.defaultSettings.showTime)) {
                    // first initiation needs to be done this way
                    this._calendarDateValue = moment(value).toDate();
                    this.usedMometToDate = true;
                }
                else {
                    // after the initiation needs to be used this way
                    this._calendarDateValue = this.convertDateToDateUTC(value);
                }
                this.formGroup.setValue({
                    Year: moment(value).format('YYYY')
                });
                this.selectedDateString = value;
            }
        }
        else {
            this._calendarDateValue = null;
            this.selectedDateString = null;
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @return {?}
     */
    get selectedDate() {
        return this.selectedDateString;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set selectedDate(value) {
        this.selectedDateString = value;
        // console.log('selectedDate', this.selectedDateString);
        this.onChange(this.selectedDateString);
        this.dateChanges.emit(this.selectedDateString);
        this.onTouched();
    }
    /**
     * @return {?}
     */
    get calendarDateValue() {
        return this._calendarDateValue;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set calendarDateValue(value) {
        this._calendarDateValue = value;
        this.onChangeDate();
    }
    /**
     * @return {?}
     */
    onChangeDate() {
        if (this._selectionMode === DateSelectionMode.Range && this._calendarDateValue) {
            /** @type {?} */
            const dateRangeList = (/** @type {?} */ ((/** @type {?} */ (this._calendarDateValue))));
            /** @type {?} */
            const newSelectedDateList = [];
            dateRangeList.forEach((date) => {
                if (date) {
                    newSelectedDateList.push(this.convertDateToISO(date));
                }
            });
            if (newSelectedDateList.length === 2) {
                this.selectedDate = newSelectedDateList;
            }
        }
        else {
            if (this.selectedDateString && !this._calendarDateValue) {
                this.selectedDate = null;
            }
            else if (this._calendarDateValue) {
                if (this.defaultSettings.showYear) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                }
                if (this.defaultSettings.showTimeOnly) {
                    this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                    this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                    this._calendarDateValue.setFullYear(this.defaultSettings.defaultYear);
                }
                if (!this.defaultSettings.enableSeconds && this._calendarDateValue.setSeconds) {
                    this._calendarDateValue.setSeconds(0);
                }
                /** @type {?} */
                let calendarDateValueISO = this.convertDateToISO(this._calendarDateValue);
                if (this.defaultSettings.disableUTC) {
                    calendarDateValueISO = moment(this._calendarDateValue).format('YYYY-MM-DDTHH:mm:ss');
                }
                if (calendarDateValueISO !== this.selectedDateString) {
                    this.selectedDate = calendarDateValueISO;
                }
            }
        }
    }
    /**
     * @return {?}
     */
    initiateYearDataSource() {
        /** @type {?} */
        const dataSource = [];
        for (let i = ((new Date()).getFullYear() + 30); i > 1922; i--) {
            dataSource.push({
                Value: i.toString(),
                Label: i.toString()
            });
        }
        dataSource.push({ Value: '2900', Label: '2900' });
        this.yearDataSource = dataSource;
    }
    /**
     * @param {?} selectedData
     * @return {?}
     */
    onYearValueChanged(selectedData) {
        if (selectedData) {
            /** @type {?} */
            let month = (this.defaultSettings.defaultMonth + 1).toString();
            if (month.length < 2) {
                month = '0' + month;
            }
            /** @type {?} */
            let day = (this.defaultSettings.defaultDay).toString();
            if (day.length < 2) {
                day = '0' + day;
            }
            this.selectedDate = selectedData['Value'] + '-' + month + '-' + day + 'T00:00:00.000Z';
        }
        else {
            this.selectedDate = null;
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    convertDateToISO(date) {
        /** @type {?} */
        const newDate = new Date(date);
        /** @type {?} */
        const year = newDate.getFullYear();
        /** @type {?} */
        let month = '' + (newDate.getMonth() + 1);
        /** @type {?} */
        let day = '' + newDate.getDate();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-') + 'T00:00:00.000Z';
    }
    /**
     * @param {?} dateAsString
     * @return {?}
     */
    convertDateToDateUTC(dateAsString) {
        /** @type {?} */
        const newDate = new Date(dateAsString);
        return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.translateSubscription.unsubscribe();
    }
}
FormDatepickerInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-datepicker-input',
                template: "<div class=\"dp-container form-datepicker-input\" [class.disable-selection]=\"isReadOnly\"\r\n     [class.hidden-btn]=\"defaultSettings.hideCalendarButton\">\r\n  <p-calendar class=\"calendar-fit\" *ngIf=\"!defaultSettings.showYear\" [(ngModel)]=\"calendarDateValue\"\r\n              [dateFormat]=\"defaultSettings.dateFormat\" [timeOnly]=\"defaultSettings.showTimeOnly\"\r\n              [disabled]=\"isDisabled\" [showTime]=\"defaultSettings.showTime\"\r\n              [showIcon]=\"!defaultSettings.hideCalendarButton\" [showButtonBar]=\"defaultSettings.showButtonBar\"\r\n              [minDate]=\"minDate\" [maxDate]=\"maxDate\" [locale]=\"locale\" [view]=\"view\" [selectionMode]=\"_selectionMode\"\r\n              [style]=\"{'width':'100%', 'height': '30px', 'top': '-2px'}\" monthNavigator=\"true\" yearNavigator=\"true\"\r\n              yearRange=\"1930:2030\" placeholder=\"{{placeholder|translate}}\" appendTo=\"body\"></p-calendar>\r\n  <!--\r\n  <div [hidden]=\"hideCalendarButton\" class=\"dp-btn-container\">\r\n    <button class=\"btn btn-success dp-btn\" type=\"button\" (click)=\"dpStart.show(); $event.stopPropagation();\">\r\n      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n    </button>\r\n  </div> -->\r\n</div>\r\n\r\n<div *ngIf=\"formGroup && defaultSettings.showYear\" [formGroup]=\"formGroup\" class=\"form-datepicker-input\"\r\n     [class.disable-selection]=\"isReadOnly\">\r\n  <layout-static-selector formControlName=\"Year\" [dataSource]=\"yearDataSource\" [valueField]=\"'Value'\"\r\n                          [labelField]=\"'Label'\" [isDisabled]=\"isDisabled\" [placeholder]=\"placeholder\"\r\n                          (changed)=\"onYearValueChanged($event)\"></layout-static-selector>\r\n</div>",
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormDatepickerInputComponent),
                        multi: true
                    }
                ],
                styles: [".dp-btn-container,.dp-container{display:flex}.dp-input{height:26px!important;border-radius:3px 0 0 3px}.dp-btn{border-radius:0 3px 3px 0;border:none;height:26px}.bs-timepicker-field{height:30px!important;width:50px!important}:host /deep/ .ui-calendar .ui-inputtext{border:1px solid #ced4da;height:30px!important;font-size:inherit;width:calc(100% - 33px);border-right:1px solid #ced4da;padding-left:10px}:host /deep/ .ui-calendar .ui-calendar-button{height:30px;top:2px;background-color:#3594e8;border-color:#3594e8}:host /deep/ .ui-calendar .ui-inputtext:disabled{background-color:#ebebe4}:host /deep/ .ui-calendar .ui-state-disabled,:host /deep/ .ui-calendar .ui-widget:disabled{opacity:1}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext{width:calc(100%)}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext:hover{border-right:1px solid #000!important}::ng-deep .ui-datepicker table td{padding:.2em!important;font-size:12px}::ng-deep .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}::ng-deep .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}.calendar-fit{width:calc(100%)}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-calendar .ui-inputtext{background-color:#e9ecef}"]
            }] }
];
/** @nocollapse */
FormDatepickerInputComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: TranslateService }
];
FormDatepickerInputComponent.propDecorators = {
    settings: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    dateChanges: [{ type: Output }],
    placeholder: [{ type: Input }],
    selectionMode: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormMaskInputComponent {
    constructor() {
        this.isDisabled = false;
        this.isReadOnly = false;
        this.hidden = false;
        this.clearable = false;
        this.changed = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.maskType) {
            if (this.maskType === MaskType.Phone) {
                this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
                this.placeholder = '(__) ___-____';
            }
            else if (this.maskType === MaskType.Mail) {
                this.mask = emailMask;
                // this.placeholder = 'john@smith.com';
                this.placeholder = '';
            }
            else if (this.maskType === MaskType.PostCode) {
                this.mask = [/\d/, /\d/, /\d/, /\d/, /\d/];
                // this.placeholder = '38000';
                this.placeholder = '';
            }
        }
        if (changes.placeholder && changes.placeholder.currentValue) {
            this.placeholder = changes.placeholder.currentValue;
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get inputValue() {
        return this._value;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inputValue(val) {
        if (val) {
            if (this.maskType === MaskType.Phone) {
                val = val.replace(/\D+/g, '');
            }
        }
        this._value = val;
        this.onChange(val);
        this.onTouched();
        this.changed.emit(this._value);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this._value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
}
FormMaskInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-form-mask-input',
                template: `

  <div style="width:100%;">
    <input type="text"
           class="form-control form-control-sm fixed-height-input form-input"
           name="inputValue"
           autocomplete="off"
           type="text"
           [textMask]="{mask: mask}"
           [(ngModel)]="inputValue"
           [disabled]="isDisabled"
           [readonly]="isReadOnly"
           [class.pr-25]="clearable && _value"
           [hidden]="hidden"
           [placeholder]="placeholder"
           >
    <core-icon icon="times" *ngIf="clearable && _value" class="searchclear" (click)="inputValue = null"></core-icon>
  </div>
`,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => FormMaskInputComponent),
                        multi: true
                    }
                ]
            }] }
];
/** @nocollapse */
FormMaskInputComponent.ctorParameters = () => [];
FormMaskInputComponent.propDecorators = {
    maskType: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    hidden: [{ type: Input }],
    placeholder: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const components = [
    FormTextInputComponent,
    FormNumberInputComponent,
    FormPasswordInputComponent,
    FormCurrencyInputComponent,
    FormTextAreaComponent,
    FormCheckboxComponent,
    FormDatepickerInputComponent,
    FormMaskInputComponent
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectorService {
    /**
     * @param {?} dataService
     */
    constructor(dataService) {
        this.dataService = dataService;
        this.listStateChanged = new Subject();
        this.counter = 0;
    }
    /**
     * @param {?} urlOptions
     * @return {?}
     */
    setLoadUrlSetting(urlOptions) {
        this.moduleUrl = urlOptions.moduleUrl;
        this.endPointUrl = urlOptions.endPointUrl;
    }
    /**
     * @return {?}
     */
    getLoadUrlSetting() {
        return (/** @type {?} */ ({
            moduleUrl: this.moduleUrl,
            endPointUrl: this.endPointUrl
        }));
    }
    /**
     * @param {?} headerParameters
     * @return {?}
     */
    setHeaders(headerParameters) {
        this.headerParameters = headerParameters;
    }
    /**
     * @return {?}
     */
    getHeaders() {
        return this.headerParameters;
    }
    /**
     * @param {?} expression
     * @return {?}
     */
    loadDataByFilterAndPageSetting(expression) {
        /** @type {?} */
        const reqUrl = `${this.moduleUrl}${this.endPointUrl}`;
        this.dataService.getListBy(reqUrl, expression, this.headerParameters)
            .subscribe((httpResponse) => {
            this.listStateChanged.next(httpResponse.serviceResult.Result.slice());
        });
    }
}
SelectorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SelectorService.ctorParameters = () => [
    { type: DataService }
];
/** @nocollapse */ SelectorService.ngInjectableDef = defineInjectable({ factory: function SelectorService_Factory() { return new SelectorService(inject(DataService)); }, token: SelectorService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectorComponent {
    /**
     * @param {?} selectorService
     */
    constructor(selectorService) {
        this.selectorService = selectorService;
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
        this.value = 'Id';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.selectorService.listStateChanged
            .subscribe((result) => {
            if (result && result.length > 0 && this._labelField === 'CombinedFields') {
                result.map((item) => {
                    /** @type {?} */
                    let combinedValue = '';
                    ((/** @type {?} */ (this.labelField))).forEach((field) => {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -this.seperator.length) : null;
                    return item;
                });
            }
            this.listData = orderBy(result, [
                'OrderNo',
                z => z[this._labelField] && (typeof z[this._labelField] === 'string') ? z[this._labelField].toLowerCase() : z[this._labelField],
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                this._labelField
            ], ['asc']);
            this.findReadonlyValue();
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.requestOptions && this.requestOptions) {
            this.urlOptions = this.requestOptions.UrlOptions;
            this.customFilter = this.requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(this.requestOptions.HeaderParameters || []);
            this.load();
        }
    }
    /**
     * @param {?} requestOptions
     * @param {?=} value
     * @param {?=} labelField
     * @param {?=} seperator
     * @return {?}
     */
    setRequestOptions(requestOptions, value = null, labelField = null, seperator = ' ') {
        if (value) {
            this.value = value;
            this.model = value;
        }
        if (labelField) {
            this.labelField = labelField;
        }
        if (seperator) {
            this.seperator = seperator;
        }
        if (this.labelField instanceof Array) {
            this._labelField = 'CombinedFields';
        }
        else if (this.labelField) {
            this._labelField = (/** @type {?} */ (this.labelField));
        }
        if (requestOptions) {
            this.urlOptions = requestOptions.UrlOptions;
            this.customFilter = requestOptions.CustomFilter;
            this.selectorService.setLoadUrlSetting(this.urlOptions);
            this.selectorService.setHeaders(requestOptions.HeaderParameters || []);
        }
    }
    /**
     * @return {?}
     */
    load() {
        this.getDataSource();
    }
    /**
     * @return {?}
     */
    reload() {
        this.getDataSource();
    }
    /**
     * @return {?}
     */
    reset() {
        this.listData = [];
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get Model() {
        return this.model;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set Model(val) {
        this.model = val;
        this.onChange(val);
        this.onTouched();
        if (val) {
            if (this.listData) {
                /** @type {?} */
                const data = this.listData.find(x => x[this.value] === this.model);
                if (data) {
                    this.changed.emit(data);
                    this.readOnlyValue = data[this._labelField];
                }
            }
            else {
                /** @type {?} */
                const emitData = {};
                emitData[this.value] = this.model;
                this.readOnlyValue = this.model;
                this.changed.emit(emitData);
            }
        }
        else {
            this.changed.emit(null);
            this.readOnlyValue = null;
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.model = value;
        this.findReadonlyValue();
    }
    /**
     * @return {?}
     */
    findReadonlyValue() {
        this.readOnlyValue = null;
        if (this.listData) {
            if (this.model || this.model === 0) {
                /** @type {?} */
                const data = this.listData.find(x => x[this.value] === this.model);
                if (data) {
                    this.readOnlyValue = data[this._labelField];
                    if (!this.isFirstValueSend) {
                        this.firstValue.emit(data);
                        this.isFirstValueSend = true;
                    }
                }
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    getDataSource() {
        this.selectorService.loadDataByFilterAndPageSetting(this.customFilter);
    }
}
SelectorComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-selector',
                template: `
    <ng-select *ngIf="!isReadOnly"
      class="custom-ng-select form-selector"
      [items]="listData"
      [searchable]="searchable"
			[clearable]="clearable"
      [bindValue]="value"
      [bindLabel]="_labelField"
      [(ngModel)]="Model"
      [disabled]="isDisabled"
    >
    </ng-select>
    <layout-form-text-input class="readonly-text-input" *ngIf="isReadOnly"
      [isReadOnly]="isReadOnly"
      [isDisabled]="isDisabled"
      [(ngModel)]="readOnlyValue"></layout-form-text-input>
  `,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => SelectorComponent),
                        multi: true
                    },
                    SelectorService
                ],
                styles: [".custom-ng-select{height:30px!important;min-width:100px;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
            }] }
];
/** @nocollapse */
SelectorComponent.ctorParameters = () => [
    { type: SelectorService }
];
SelectorComponent.propDecorators = {
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    searchable: [{ type: Input }],
    clearable: [{ type: Input }],
    requestOptions: [{ type: Input }],
    changed: [{ type: Output }],
    firstValue: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StaticSelectorComponent {
    constructor() {
        this.valueField = 'Id';
        this.labelField = 'Definition';
        this._labelField = 'Definition';
        this.seperator = ' ';
        this.isDisabled = false;
        this.isReadOnly = false;
        this.searchable = true;
        this.clearable = true;
        this.changed = new EventEmitter();
        // it is for getting the first selected value
        this.firstValue = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.dataSource && this.selector && this.model) {
            if (this.dataSource && this.dataSource.length > 500) {
                this.virtualScroll = true;
            }
            // this.dataSource = [...this.dataSource, changes.dataSource];
            /** @type {?} */
            const index = findIndex(this.dataSource, ['Id', this.Model]);
            if (index === -1) {
                this.model = null;
            }
        }
        if (changes.dataSource && this.dataSource) {
            this.dataSource = orderBy(this.dataSource, [
                'OrderNo',
                z => z[this._labelField] && (typeof z[this._labelField] === 'string') ? z[this._labelField].toLowerCase() : z[this._labelField],
                // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                this._labelField
            ], ['asc']);
            this.findDisplayValue();
        }
        if (changes.labelField && this.labelField) {
            if (changes.labelField.currentValue instanceof Array) {
                this._labelField = 'CombinedFields';
            }
            else {
                this._labelField = (/** @type {?} */ (this.labelField));
            }
        }
        if (changes.dataSource || changes.labelField || changes.seperator) {
            if (this.dataSource && this.dataSource.length > 0 && this._labelField === 'CombinedFields') {
                this.dataSource.map((item) => {
                    /** @type {?} */
                    let combinedValue = '';
                    ((/** @type {?} */ (this.labelField))).forEach((field) => {
                        if (item[field]) {
                            combinedValue = combinedValue + item[field] + this.seperator;
                        }
                    });
                    item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -this.seperator.length) : null;
                    return item;
                });
                this.findDisplayValue();
            }
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    onChange(val) {
    }
    /**
     * @return {?}
     */
    onTouched() {
    }
    /**
     * @return {?}
     */
    get Model() {
        return this.model;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set Model(val) {
        this.model = val;
        this.onChange(val);
        this.onTouched();
        if (val) {
            if (this.dataSource) {
                /** @type {?} */
                const data = this.dataSource.find(x => x[this.valueField] === this.model);
                if (data) {
                    this.value = data[this._labelField];
                    this.changed.emit(data);
                }
            }
            else {
                /** @type {?} */
                const emitData = {};
                emitData[this.valueField] = this.model;
                this.changed.emit(emitData);
            }
        }
        else {
            this.changed.emit(null);
        }
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.model = value;
        this.findDisplayValue();
    }
    /**
     * @return {?}
     */
    findDisplayValue() {
        // if (this.isReadOnly)
        if (this.dataSource && this.dataSource.length > 0 && (this.model || this.model === 0)) {
            /** @type {?} */
            const data = this.dataSource.find(x => x[this.valueField] === this.model);
            if (data) {
                this.value = data[this._labelField];
                if (!this.isFirstValueSend) {
                    this.firstValue.emit(data);
                }
            }
        }
    }
}
StaticSelectorComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-static-selector',
                template: `
	<ng-select *ngIf="!isReadOnly"
		#selector
      	class="custom-ng-select form-selector"
		[items]="dataSource"
		[searchable]="searchable"
		[clearable]="clearable"
      	[(ngModel)]="Model"
		[bindValue]="valueField"
		[bindLabel]="_labelField"
		[disabled]="isDisabled"
    appendTo= "body"
    [virtualScroll]="virtualScroll"
    placeholder="{{placeholder|translate}}"
		ngDefaultControl
	>
	</ng-select>
	<layout-form-text-input class="readonly-text-input" *ngIf="isReadOnly" [isReadOnly]="isReadOnly" [isDisabled]="isDisabled" [(ngModel)]="value"></layout-form-text-input>
  	`,
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef(() => StaticSelectorComponent),
                        multi: true
                    }
                ],
                styles: [".custom-ng-select{height:30px!important;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
            }] }
];
StaticSelectorComponent.propDecorators = {
    dataSource: [{ type: Input }],
    valueField: [{ type: Input }],
    labelField: [{ type: Input }],
    seperator: [{ type: Input }],
    isDisabled: [{ type: Input }],
    isReadOnly: [{ type: Input }],
    searchable: [{ type: Input }],
    clearable: [{ type: Input }],
    changed: [{ type: Output }],
    placeholder: [{ type: Input }],
    selector: [{ type: ViewChild, args: ['selector',] }],
    firstValue: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TabsComponent {
    /**
     * @param {?} router
     * @param {?} activatedRoute
     */
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.tabChange = new EventEmitter();
        this.tabs = [];
        // this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
        //   .subscribe(event => {
        //     // set breadcrumbs
        //     const foundData = this.tabs.find(x => x.link === this.router.url);
        //     if (foundData) {
        //       this.onTabClick(foundData);
        //     }
        //   });
        this.routingSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
            /** @type {?} */
            const foundData = this.tabs.find(x => x.link === this.router.url);
            if (foundData) {
                this.onTabClick(foundData);
            }
        });
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.activeTab && !changes.activeTab.isFirstChange()) {
            this.foundData = this.tabs.find(x => x.title === this.activeTab);
            this.tabChange.emit(this.foundData);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    getClassActive(tab) {
        // return this.activeTab === tab.title && !tab.link;
        // (bu alan hep false dönüyo)
        return this.activeTab === tab.title && ((tab.link !== undefined && tab.link !== null) || !tab.link);
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    onTabClick(tab) {
        this.activeTab = tab.title;
        this.tabChange.emit(tab);
    }
    /**
     * @param {?} title
     * @param {?} link
     * @return {?}
     */
    addTabs(title, link) {
        this.tabs.push({ title, link });
        if (this.router && this.router.url && link === this.router.url) {
            this.activeTab = title;
            // this.tabChange.emit({ title, link });
        }
        // if (title === this.activeTab) {
        //   this.tabChange.emit(this.activeTab);
        //   if (link) {
        //     this.router.navigate([link]);
        //   }
        // }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.routingSubscription) {
            this.routingSubscription.unsubscribe();
        }
    }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-tabs',
                template: "<nav class=\"navbar navbar-expand-sm navbar-dark justify-content-md-center mb-5\">\r\n  <ul class=\"navbar-nav\">\r\n    <li class=\"nav-item\" *ngFor=\"let tab of tabs\" (click)=\"onTabClick(tab)\" [class.active]=\"getClassActive(tab)\">\r\n      <a *ngIf=\"tab.link\" class=\"nav-link\" [routerLinkActive]=\"['router-link-active']\"\r\n        [routerLink]=\"[tab.link]\">{{tab.title|translate}}</a>\r\n      <a *ngIf=\"!tab.link\" class=\"nav-link\">{{tab.title|translate}}</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n\r\n<ng-content></ng-content>",
                styles: [".navbar{margin:15px 0!important;padding:0}.navbar-nav{display:flex;width:100%;margin-bottom:0;padding-left:0;padding-right:0;margin-left:0;margin-right:0;box-shadow:0 5px 20px rgba(0,0,0,.05)}.nav-item{font-weight:600;flex:1;text-align:center;padding:0;border:1px solid #e3f1f8;border-left-width:0}.nav-item:first-child{border-left-width:1px}.nav-item a.nav-link{background:#fff;color:#788d96;line-height:24px;border-radius:0;transition:.2s ease-in-out;cursor:pointer;height:100%;display:flex;align-items:center;justify-content:center}.nav-item a.nav-link.router-link-active,.nav-item.active a{color:#175169;border-bottom:2px solid #409fdd}.nav-item a.nav-link:hover{background-color:#e3f1f8;color:#1a2b49}"]
            }] }
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
TabsComponent.propDecorators = {
    activeTab: [{ type: Input }],
    tabChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TabComponent {
    /**
     * @param {?} parent
     * @param {?} router
     * @param {?} _ngZone
     */
    constructor(parent, router, _ngZone) {
        this.parent = parent;
        this.router = router;
        this._ngZone = _ngZone;
        this.title = '';
        // this.step = 0;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.parent.addTabs(this.title, this.link);
        this.isCurrent = this.title === this.parent.activeTab;
        if (this.isCurrent && this.link) {
            this.router.navigate([this.link]);
        }
        this.tabChangeSubcriotion = this.parent.tabChange
            .subscribe((tab) => {
            this.isCurrent = this.title === tab.title;
            if (this.isCurrent && this.link) {
                this._ngZone.run(() => this.router.navigate([this.link]));
            }
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.tabChangeSubcriotion) {
            this.tabChangeSubcriotion.unsubscribe();
        }
    }
}
TabComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-tab',
                host: {
                    '[style.display]': 'isCurrent ? "flex" : "none"',
                },
                template: `
    <ng-content></ng-content>
  `
            }] }
];
/** @nocollapse */
TabComponent.ctorParameters = () => [
    { type: TabsComponent },
    { type: Router },
    { type: NgZone }
];
TabComponent.propDecorators = {
    title: [{ type: Input }],
    link: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormWizardComponent {
    /**
     * @param {?} toastrUtilsService
     */
    constructor(toastrUtilsService) {
        this.toastrUtilsService = toastrUtilsService;
        this.finishText = 'Finish';
        this.step = 1;
        this.finish = new EventEmitter();
        this.stepChange = new EventEmitter();
        this.steps = [];
        this.isValid = true;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.step) {
            this.stepChange.emit(this.step);
        }
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getClassActive(index) {
        return this.step === index + 1;
    }
    /**
     * @param {?} isNext
     * @return {?}
     */
    onStepChanged(isNext) {
        if (isNext) {
            if (this.isValid == true) {
                this.step = this.step + 1;
                this.stepChange.emit(this.step);
            }
            else {
                this.toastrUtilsService.error('FormIsNotValid', 'NotValid');
            }
        }
        else {
            this.step = this.step - 1;
            this.stepChange.emit(this.step);
        }
    }
    /**
     * @return {?}
     */
    isOnFirstStep() {
        return this.step === 1;
    }
    /**
     * @return {?}
     */
    isOnFinalStep() {
        return this.step === (this.steps ? this.steps.length : 0);
    }
    /**
     * @return {?}
     */
    getClassNameAToSteps() {
        if (this.steps && this.steps.length > 0) {
            return 'wizard wizard-steps-' + this.steps.length;
        }
        else {
            return 'wizard wizard-steps-1';
        }
    }
    /**
     * @param {?} title
     * @return {?}
     */
    addStep(title) {
        /** @type {?} */
        const newStep = {
            Header: title
        };
        this.steps.push(newStep);
        return this.steps.length;
    }
}
FormWizardComponent.decorators = [
    { type: Component, args: [{
                selector: 'form-wizard',
                template: "<ul *ngIf=\"steps\" [className]=\"getClassNameAToSteps()\">\r\n  <li *ngFor=\"let step of steps; let i=index;\">\r\n    <div *ngIf=\"step.Header\" class=\"step-header\">\r\n      {{step.Header}}\r\n    </div>\r\n    <div class=\"step\" [class.complete]=\"step.Completed\" [class.active]=\"getClassActive(i)\">\r\n      <span>{{i + 1}}</span>\r\n    </div>\r\n  </li>\r\n</ul>\r\n<div class=\" col-sm-12 prl-0 ptb-10\">\r\n  <ng-content></ng-content>\r\n</div>\r\n<div class=\"my-wizard__footer\">\r\n  <button class=\"btn btn-warning\" [style.visibility]=\"isOnFirstStep() ? 'hidden' : 'visible'\"\r\n    (click)=\"onStepChanged(false)\">\r\n    <core-icon icon=\"arrow-alt-circle-left\"></core-icon> Previous\r\n  </button>\r\n  <!-- {{step}} / {{steps.length}} -->\r\n  <button class=\"btn btn-info\" *ngIf=\"!isOnFinalStep()\" (click)=\"onStepChanged(true)\">\r\n    Next <core-icon icon=\"arrow-alt-circle-right\"></core-icon>\r\n  </button>\r\n  <!-- <button\r\n    *ngIf=\"isOnFinalStep()\"\r\n    (click)=\"finish.emit(step + 1)\">\r\n    {{finishText}}\r\n  </button> -->\r\n</div>",
                styles: [":host{display:flex;flex-direction:column}.my-wizard__footer{display:flex;align-items:center;justify-content:space-between;flex-shrink:0}.wizard{padding:0;margin:0;list-style:none;width:100%;display:table;position:relative}.wizard .step-header{color:#000;position:relative;width:100%;text-align:center;margin-top:-15px;top:-10px}.wizard>*{display:table-cell;text-align:center}.wizard>* .step{width:30px;height:30px;border-radius:30px;border:1px solid #bdc3c7;color:#bdc3c7;font-weight:300;background:#fff;text-align:center;display:inline-block;position:relative;box-sizing:content-box;cursor:default}.wizard>* .step.complete{border-color:#1abc9c;color:#1abc9c}.wizard>* .step.active{border-color:#3498db;color:#3498db;font-weight:500;border-width:2px;margin-top:-1px}.wizard>* .step:after{content:\" \";display:block;width:30px;height:30px;background-color:#fff;position:absolute;z-index:-1;border:10px solid #fff;top:-10px;left:-10px;box-sizing:content-box}.wizard>* .step>*{line-height:30px}.wizard:after{content:\" \";border-bottom:1px dotted rgba(0,0,0,.2);position:absolute;z-index:-2;top:50%}.wizard-steps-1>*{width:100%}.wizard-steps-1:after{left:50%;right:50%}.wizard-steps-2>*{width:50%}.wizard-steps-2:after{left:25%;right:25%}.wizard-steps-3>*{width:33.33333%}.wizard-steps-3:after{left:16.66667%;right:16.66667%}.wizard-steps-4>*{width:25%}.wizard-steps-4:after{left:12.5%;right:12.5%}.wizard-steps-5>*{width:20%}.wizard-steps-5:after{left:10%;right:10%}.wizard-steps-6>*{width:16.66667%}.wizard-steps-6:after{left:8.33333%;right:8.33333%}.wizard-steps-7>*{width:14.28571%}.wizard-steps-7:after{left:7.14286%;right:7.14286%}.wizard-steps-8>*{width:12.5%}.wizard-steps-8:after{left:6.25%;right:6.25%}.wizard-steps-9>*{width:11.11111%}.wizard-steps-9:after{left:5.55556%;right:5.55556%}.wizard-steps-10>*{width:10%}.wizard-steps-10:after{left:5%;right:5%}.wizard-steps-11>*{width:9.09091%}.wizard-steps-11:after{left:4.54545%;right:4.54545%}.wizard-steps-12>*{width:8.33333%}.wizard-steps-12:after{left:4.16667%;right:4.16667%}.wizard-steps-13>*{width:7.69231%}.wizard-steps-13:after{left:3.84615%;right:3.84615%}.wizard-steps-14>*{width:7.14286%}.wizard-steps-14:after{left:3.57143%;right:3.57143%}.wizard-steps-15>*{width:6.66667%}.wizard-steps-15:after{left:3.33333%;right:3.33333%}.wizard-steps-16>*{width:6.25%}.wizard-steps-16:after{left:3.125%;right:3.125%}.wizard-steps-17>*{width:5.88235%}.wizard-steps-17:after{left:2.94118%;right:2.94118%}.wizard-steps-18>*{width:5.55556%}.wizard-steps-18:after{left:2.77778%;right:2.77778%}.wizard-steps-19>*{width:5.26316%}.wizard-steps-19:after{left:2.63158%;right:2.63158%}.wizard-steps-20>*{width:5%}.wizard-steps-20:after{left:2.5%;right:2.5%}"]
            }] }
];
/** @nocollapse */
FormWizardComponent.ctorParameters = () => [
    { type: ToastrUtilsService }
];
FormWizardComponent.propDecorators = {
    finishText: [{ type: Input }],
    step: [{ type: Input }],
    finish: [{ type: Output }],
    stepChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormWizardStepComponent {
    /**
     * @param {?} parent
     */
    constructor(parent) {
        this.parent = parent;
        this.title = '';
        this.isValid = true;
        // this.step = 0;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.isValid) {
            console.log(this.isValid);
            this.parent.isValid = this.isValid;
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.step = this.parent.addStep(this.title);
        this.isCurrent = this.step === this.parent.step;
        this.stepChangeSubcriotion = this.parent.stepChange.subscribe(step => {
            this.isCurrent = this.step === step;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.stepChangeSubcriotion) {
            this.stepChangeSubcriotion.unsubscribe();
        }
    }
}
FormWizardStepComponent.decorators = [
    { type: Component, args: [{
                selector: 'wizard-step',
                host: {
                    '[style.display]': 'isCurrent ? "flex" : "none"',
                },
                template: `
    <ng-content></ng-content>
  `
            }] }
];
/** @nocollapse */
FormWizardStepComponent.ctorParameters = () => [
    { type: FormWizardComponent }
];
FormWizardStepComponent.propDecorators = {
    title: [{ type: Input }],
    isValid: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CoreIconComponent {
}
CoreIconComponent.decorators = [
    { type: Component, args: [{
                template: `
        <fa-icon *ngIf="icon" [icon]="icon"></fa-icon>
    `,
                selector: 'core-icon'
            }] }
];
CoreIconComponent.propDecorators = {
    icon: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class BaseComponent {
    /**
     * @param {?} preventUnsavedChangesService
     */
    constructor(preventUnsavedChangesService) {
        this.preventUnsavedChangesService = preventUnsavedChangesService;
        this.subscriptions = [];
        this.isSaveOperationEnabled = true;
        this.safeSubscription(this.preventUnsavedChangesService.getStatus()
            .subscribe((status) => {
            if (status === 'waiting') {
                this.preventUnsavedChangesService.setIsChanged(this.skipUnsavedChanges ? false : this.hasUnsavedChanges());
                this.preventUnsavedChangesService.setStatus('finished');
            }
        }));
    }
    /**
     * @protected
     * @param {?} sub
     * @return {?}
     */
    safeSubscription(sub) {
        this.subscriptions.push(sub);
        return sub;
    }
    /**
     * @private
     * @return {?}
     */
    unsubscribeAll() {
        this.subscriptions.forEach((subs) => {
            if (subs) {
                subs.unsubscribe();
            }
        });
    }
    /**
     * @protected
     * @return {?}
     */
    baseNgOnDestroy() {
        this.unsubscribeAll();
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SystemReferenceDateComponent {
    /**
     * @param {?} systemReferenceDateService
     * @param {?} storageService
     * @param {?} fb
     * @param {?} translate
     */
    constructor(systemReferenceDateService, storageService, fb, translate) {
        this.systemReferenceDateService = systemReferenceDateService;
        this.storageService = storageService;
        this.fb = fb;
        this.translate = translate;
        /** @type {?} */
        const currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        this._calendarDateValue = currentsystemRefDate ?
            this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
            this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
        // until the better solution for translate.
        this.localEn = {
            firstDayOfWeek: 1,
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear'
        };
        // until the better solution for translate.
        this.localeTr = {
            firstDayOfWeek: 1,
            dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
            dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
            monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
            monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
            today: 'Bugün',
            clear: 'Temizle'
        };
        // until the better solution for translate.
        this.locale = this.localeTr;
        this.translateSubscription = this.translate.onLangChange
            .subscribe((event) => {
            if (event) {
                this.locale = event.lang === 'tr' ? this.localeTr : this.localEn;
            }
        });
        this.systemReferenceDateService.getSelectedReferenceDate()
            .subscribe((selectedRefDate) => {
            if (selectedRefDate !== '') {
                this._calendarDateValue = this.systemReferenceDateService.convertDateToDateUTC(selectedRefDate);
            }
        });
    }
    /**
     * @return {?}
     */
    get calendarDateValue() {
        return this._calendarDateValue;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set calendarDateValue(value) {
        this._calendarDateValue = value;
        // this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
    }
    /**
     * @param {?} selectedDateISO
     * @return {?}
     */
    setSelectedDate(selectedDateISO) {
        this.systemReferenceDateService.setSelectedReferenceDate(selectedDateISO);
    }
    /**
     * @return {?}
     */
    onClose() {
        this.checkAndApplyDate();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.checkAndApplyDate();
            if (this._calendarDateValue && this.systemRefDatePicker && this.systemRefDatePicker.overlayVisible) {
                this.systemRefDatePicker.overlayVisible = false;
            }
        }
    }
    /**
     * @return {?}
     */
    checkAndApplyDate() {
        /** @type {?} */
        const currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
        if (this._calendarDateValue) {
            /** @type {?} */
            const selectedDate = this.systemReferenceDateService.convertDateToISO(this._calendarDateValue);
            if (currentsystemRefDate !== selectedDate) {
                this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
            }
        }
        else {
            this._calendarDateValue = currentsystemRefDate ?
                this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
                this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.translateSubscription) {
            this.translateSubscription.unsubscribe();
        }
        if (this.sysRefDateSubscription) {
            this.sysRefDateSubscription.unsubscribe();
        }
    }
}
SystemReferenceDateComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-system-reference-date',
                template: " <div class=\"system-reference-container\">\r\n   <p-calendar #systemRefDatePicker class=\"system-reference-datepicker\" [(ngModel)]=\"calendarDateValue\"\r\n     [dateFormat]=\"'dd.mm.yy'\" [showIcon]=\"false\" [showButtonBar]=\"true\" [style]=\"{'width':'100%', 'height': '30px'}\"\r\n     monthNavigator=\"true\" yearNavigator=\"true\" yearRange=\"1930:2030\" [locale]=\"locale\" (onClose)=\"onClose()\"\r\n     (keydown)=\"keyDownFunction($event)\" appendTo=\"body\">\r\n   </p-calendar>\r\n </div>\r\n",
                styles: [":host /deep/ .ui-calendar .ui-inputtext{height:30px!important;font-size:inherit;width:calc(100%);color:#fff;border:none;cursor:pointer;background:#e30a3a!important;border-color:rgba(255,255,255,.2)!important}:host /deep/ .ui-datepicker table td{padding:.2em!important;font-size:12px}:host /deep/ .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}:host /deep/ .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}"]
            }] }
];
/** @nocollapse */
SystemReferenceDateComponent.ctorParameters = () => [
    { type: SystemReferenceDateService },
    { type: StorageService },
    { type: FormBuilder },
    { type: TranslateService }
];
SystemReferenceDateComponent.propDecorators = {
    systemRefDatePicker: [{ type: ViewChild, args: ['systemRefDatePicker',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const LayoutComponents = [
    CoreIconComponent,
    CoreGridComponent,
    CommandCellComponent,
    ListOfValuesModalComponent,
    ListOfValuesComponent,
    SelectorComponent,
    components,
    StaticSelectorComponent,
    CoreModalComponent,
    ColumnHeaderComponent,
    NoRowsOverlayComponent,
    LoadingOverlayComponent,
    FormWizardComponent,
    FormWizardStepComponent,
    TabsComponent,
    TabComponent,
    CoreOrgChartComponent,
    BreadcrumbComponent
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const coreGridEditElements = [
    BooleanEditCellComponent,
    DateEditCellComponent,
    LovEditCellComponent,
    NumericEditCellComponent,
    SelectEditCellComponent,
    TextEditCellComponent,
    MaskEditCellComponent,
    CurrencyEditCellComponent
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const coreGridReadOnlyElements = [
    BooleanCellComponent,
    LabelCellComponent,
    SelectCellComponent,
    DateCellComponent,
    CurrencyCellComponent,
    MaskCellComponent,
    NumericCellComponent
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const coreGridFilterElements = [
    DateFilterCellComponent,
    TextFilterCellComponent,
    SelectFilterCellComponent,
    NumericFilterCellComponent,
    BooleanFilterCellComponent,
    CurrencyFilterCellComponent,
    LovFilterCellComponent,
    MaskFilterCellComponent
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const customCurrencyMaskConfig = {
    align: 'left',
    allowNegative: true,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: '',
    suffix: '',
    thousands: '.',
    nullable: true
};
/**
 * @param {?} http
 * @return {?}
 */
function HttpLoaderFactory(http) {
    return new TranslateHttpLoader(http);
}
class CoreCommonModule {
}
CoreCommonModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    coreGridEditElements,
                    coreGridReadOnlyElements,
                    coreGridFilterElements,
                    LayoutComponents,
                    SafePipe
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    HttpClientModule,
                    ReactiveFormsModule,
                    RouterModule,
                    TextMaskModule,
                    NgSelectModule,
                    FileUploadModule,
                    BsDropdownModule.forRoot(),
                    BsDatepickerModule.forRoot(),
                    TabsModule.forRoot(),
                    ModalModule.forRoot(),
                    CollapseModule.forRoot(),
                    SortableModule.forRoot(),
                    PaginationModule.forRoot(),
                    TranslateModule.forRoot({
                        loader: [
                            { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] }
                        ]
                    }),
                    AgGridModule.withComponents([
                        coreGridEditElements,
                        coreGridReadOnlyElements,
                        coreGridFilterElements,
                        CommandCellComponent,
                        ColumnHeaderComponent,
                        NoRowsOverlayComponent,
                        LoadingOverlayComponent
                    ]),
                    ToastContainerModule,
                    ToastrModule.forRoot({
                        closeButton: true,
                        progressBar: true
                    }),
                    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
                    CalendarModule,
                    CheckboxModule,
                    DialogModule,
                    OverlayPanelModule,
                    FontAwesomeModule
                ],
                entryComponents: [
                    ListOfValuesModalComponent
                ],
                providers: [
                    DataService,
                    SweetAlertService,
                    ToastrUtilsService,
                    AuthenticationService,
                    TranslateHelperService,
                    ValidationService
                ],
                exports: [
                    LayoutComponents,
                    TranslateModule,
                    SafePipe
                ],
                schemas: [
                    CUSTOM_ELEMENTS_SCHEMA,
                    NO_ERRORS_SCHEMA
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingComponent {
    /**
     * @param {?} loadingService
     * @param {?} changeDetector
     */
    constructor(loadingService, changeDetector) {
        this.loadingService = loadingService;
        this.changeDetector = changeDetector;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loadingService.getLoadingBarDisplayStatus()
            .subscribe((status) => {
            if (this.showLoading !== status) {
                this.showLoading = status;
                this.detectChanges();
            }
        });
    }
    /**
     * @return {?}
     */
    detectChanges() {
        setTimeout(() => {
            if (!this.changeDetector['destroyed']) {
                this.changeDetector.detectChanges();
            }
        }, 251);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.loadingService.loadingBarDisplayStatus.unsubscribe();
    }
}
LoadingComponent.decorators = [
    { type: Component, args: [{
                selector: 'layout-loading',
                template: "<div *ngIf=\"showLoading\" class=\"core-loader\">\r\n  <div id=\"loading-wrapper\">\r\n    <!-- <div id=\"loading-text\">Y\u00FCkleniyor</div> -->\r\n    <div class=\"loader\">\r\n      <div class=\"inner one\"></div>\r\n      <div class=\"inner two\"></div>\r\n      <div class=\"inner three\"></div>\r\n    </div>\r\n  </div>\r\n</div>",
                styles: ["#loading-wrapper{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;background-color:rgba(0,0,0,.05)}.core-loader{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;display:block;background-position:center center;background-repeat:no-repeat;background-color:transparent!important}.loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px;z-index:999}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
            }] }
];
/** @nocollapse */
LoadingComponent.ctorParameters = () => [
    { type: LoadingService },
    { type: ChangeDetectorRef }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingLibModule {
}
LoadingLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    LoadingComponent
                ],
                imports: [
                    CommonModule
                ],
                providers: [
                // LoadingService
                ],
                exports: [
                    LoadingComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// import { SystemReferenceDateService } from './shared/services/system-reference-date.service';
class SystemReferanceDateLibModule {
}
SystemReferanceDateLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    SystemReferenceDateComponent
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    CalendarModule,
                    BsDatepickerModule.forRoot()
                ],
                exports: [
                    SystemReferenceDateComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CoreGridCellType, CoreGridColumnType, CoreGridSelectionType, Comparison, DefaultComparison, TextComparison, NumberComparison, BooleanComparison, DateComparison, ComparisonType, ComparisonList, Conditions, FormButtonType, LovColumnType, Module, OperationType, SortType, FormType, MaskType, AlertDismissReason, AlertType, Connector, ConfirmDialogOperationType, ToastrMessages, Storage, Headers, StorageType, DateSelectionMode, BaseModuleUrl, DateSettings, Filter, FilterGroup, LovColumn, ConfirmDialogSettings, GetListByResult, ServiceResult, BaseModel, SafePipe, AppConfig, CoreModalService, BreadcrumbService, LoadingService, PreventUnsavedChangesService, StorageService, SweetAlertService, SystemReferenceDateService, ToastrUtilsService, TranslateHelperService, ValidationService, PreventUnsavedChangesGuard, BaseHttpService, BaseService, DataService, RequestInterceptor, AuthenticationService, LayoutComponents, BaseComponent, CoreGridComponent, ColumnHeaderComponent, NoRowsOverlayComponent, LoadingOverlayComponent, CommandCellComponent, CoreModalComponent, CoreOrgChartComponent, BreadcrumbComponent, ListOfValuesModalComponent, ListOfValuesComponent, SelectorComponent, StaticSelectorComponent, TabsComponent, TabComponent, FormWizardComponent, FormWizardStepComponent, SystemReferenceDateComponent, CoreIconComponent, components, FormCheckboxComponent, FormTextInputComponent, FormNumberInputComponent, FormPasswordInputComponent, FormCurrencyInputComponent, FormTextAreaComponent, FormDatepickerInputComponent, FormMaskInputComponent, HttpLoaderFactory, customCurrencyMaskConfig, CoreCommonModule, LoadingLibModule, SystemReferanceDateLibModule, BooleanEditCellComponent as ɵf, coreGridEditElements as ɵe, CurrencyEditCellComponent as ɵm, DateEditCellComponent as ɵg, LovEditCellComponent as ɵh, MaskEditCellComponent as ɵl, NumericEditCellComponent as ɵi, SelectEditCellComponent as ɵj, TextEditCellComponent as ɵk, BooleanFilterCellComponent as ɵba, coreGridFilterElements as ɵv, CurrencyFilterCellComponent as ɵbb, DateFilterCellComponent as ɵw, LovFilterCellComponent as ɵbc, MaskFilterCellComponent as ɵbd, NumericFilterCellComponent as ɵz, SelectFilterCellComponent as ɵy, TextFilterCellComponent as ɵx, BooleanCellComponent as ɵo, coreGridReadOnlyElements as ɵn, CurrencyCellComponent as ɵs, DateCellComponent as ɵr, LabelCellComponent as ɵp, MaskCellComponent as ɵt, NumericCellComponent as ɵu, SelectCellComponent as ɵq, CoreGridService as ɵb, GridLoadingService as ɵa, ListOfValuesService as ɵc, LoadingComponent as ɵbe, SelectorService as ɵd };

//# sourceMappingURL=core-common.js.map