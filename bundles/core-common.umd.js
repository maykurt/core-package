(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/platform-browser'), require('rxjs/Subject'), require('rxjs/add/operator/share'), require('sweetalert2'), require('rxjs/BehaviorSubject'), require('@auth0/angular-jwt'), require('rxjs/add/operator/filter'), require('moment'), require('text-mask-addons/dist/emailMask'), require('rxjs'), require('lodash'), require('rxjs/operators'), require('@angular/common/http'), require('ag-grid-angular'), require('@ngx-translate/core'), require('@ngx-translate/http-loader'), require('ngx-toastr'), require('@ng-select/ng-select'), require('angular2-text-mask'), require('ngx-currency'), require('primeng/checkbox'), require('primeng/dialog'), require('primeng/overlaypanel'), require('primeng/fileupload'), require('@angular/router'), require('@fortawesome/angular-fontawesome'), require('@angular/core'), require('@angular/common'), require('ngx-bootstrap'), require('primeng/calendar'), require('@angular/forms')) :
    typeof define === 'function' && define.amd ? define('@core/common', ['exports', '@angular/platform-browser', 'rxjs/Subject', 'rxjs/add/operator/share', 'sweetalert2', 'rxjs/BehaviorSubject', '@auth0/angular-jwt', 'rxjs/add/operator/filter', 'moment', 'text-mask-addons/dist/emailMask', 'rxjs', 'lodash', 'rxjs/operators', '@angular/common/http', 'ag-grid-angular', '@ngx-translate/core', '@ngx-translate/http-loader', 'ngx-toastr', '@ng-select/ng-select', 'angular2-text-mask', 'ngx-currency', 'primeng/checkbox', 'primeng/dialog', 'primeng/overlaypanel', 'primeng/fileupload', '@angular/router', '@fortawesome/angular-fontawesome', '@angular/core', '@angular/common', 'ngx-bootstrap', 'primeng/calendar', '@angular/forms'], factory) :
    (factory((global.core = global.core || {}, global.core.common = {}),global.ng.platformBrowser,global.rxjs.Subject,global.rxjs['add/operator/share'],global.Swal,global.rxjs.BehaviorSubject,global.angularJwt,global.rxjs['add/operator/filter'],global.moment,global.emailMask,global.rxjs,global._,global.rxjs.operators,global.ng.common.http,global.agGridAngular,global.core,global.httpLoader,global.ngxToastr,global.ngSelect,global.angular2TextMask,global.ngxCurrency,global.checkbox,global.dialog,global.overlaypanel,global.fileupload,global.ng.router,global.angularFontawesome,global.ng.core,global.ng.common,global.ngxBootstrap,global.calendar,global.ng.forms));
}(this, (function (exports,platformBrowser,Subject,share,Swal,BehaviorSubject,angularJwt,filter,moment,emailMask,rxjs,_,operators,http,agGridAngular,core,httpLoader,ngxToastr,ngSelect,angular2TextMask,ngxCurrency,checkbox,dialog,overlaypanel,fileupload,i5,angularFontawesome,i0,common,ngxBootstrap,calendar,forms) { 'use strict';

    Swal = Swal && Swal.hasOwnProperty('default') ? Swal['default'] : Swal;
    moment = moment && moment.hasOwnProperty('default') ? moment['default'] : moment;
    emailMask = emailMask && emailMask.hasOwnProperty('default') ? emailMask['default'] : emailMask;

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var CoreGridCellType = {
        // Edit Cell
        CoreBooleanEditCell: 'coreBooleanEditCell',
        CoreDateEditCell: 'coreDateEditCell',
        CoreLOVEditCell: 'coreLOVEditCell',
        CoreNumericEditCell: 'coreNumericEditCell',
        CoreSelectEditCell: 'coreSelectEditCell',
        CoreTextEditCell: 'coreTextEditCell',
        CoreMaskEditCell: 'coreMaskEditCell',
        CoreCurrencyEditCell: 'coreCurrencyEditCell',
        // Read-Only Cell
        CoreBooleanCell: 'coreBooleanCell',
        CoreLabelCell: 'coreLabelCell',
        CoreSelectCell: 'coreSelectCell',
        CoreDateCell: 'coreDateCell',
        CoreCurrencyCell: 'coreCurrencyCell',
        CoreMaskCell: 'coreMaskCell',
        CoreNumericCell: 'coreNumericCell',
        // Filter Cell
        CoreDateFilterCell: 'coreDateFilterCell',
        CoreSelectFilterCell: 'coreSelectFilterCell',
        CoreTextFilterCell: 'coreTextFilterCell',
        CoreNumericFilterCell: 'coreNumericFilterCell',
        CoreBooleanFilterCell: 'coreBooleanFilterCell',
        CoreCurrencyFilterCell: 'coreCurrencyFilterCell',
        CoreLOVFilterCell: 'coreLOVFilterCell',
        CoreMaskFilterCell: 'coreMaskFilterCell',
        // Command Cell
        CoreCommandCell: 'coreCommandCell',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var CoreGridColumnType = {
        Label: 'label',
        Text: 'text',
        Date: 'date',
        Select: 'select',
        Numeric: 'numeric',
        Boolean: 'boolean',
        Lov: 'lov',
        Mask: 'mask',
        Currency: 'currency',
        NumericLabel: 'numeric-label',
        TextSelect: 'text-select',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var CoreGridSelectionType = {
        None: 'none',
        Single: 'single',
        Multiple: 'multiple',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var Comparison = {
        In: 0,
        LessThanOrEqualTo: 1,
        LessThan: 2,
        IsNullOrWhiteSpace: 3,
        IsNull: 4,
        IsNotNullNorWhiteSpace: 5,
        IsNotNull: 6,
        IsNotEmpty: 7,
        IsEmpty: 8,
        StartsWith: 9,
        GreaterThanOrEqualTo: 10,
        GreaterThan: 11,
        EqualTo: 12,
        EndsWith: 13,
        DoesNotContain: 14,
        Contains: 15,
        Between: 16,
        NotEqualTo: 17,
    };
    Comparison[Comparison.In] = 'In';
    Comparison[Comparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
    Comparison[Comparison.LessThan] = 'LessThan';
    Comparison[Comparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
    Comparison[Comparison.IsNull] = 'IsNull';
    Comparison[Comparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
    Comparison[Comparison.IsNotNull] = 'IsNotNull';
    Comparison[Comparison.IsNotEmpty] = 'IsNotEmpty';
    Comparison[Comparison.IsEmpty] = 'IsEmpty';
    Comparison[Comparison.StartsWith] = 'StartsWith';
    Comparison[Comparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
    Comparison[Comparison.GreaterThan] = 'GreaterThan';
    Comparison[Comparison.EqualTo] = 'EqualTo';
    Comparison[Comparison.EndsWith] = 'EndsWith';
    Comparison[Comparison.DoesNotContain] = 'DoesNotContain';
    Comparison[Comparison.Contains] = 'Contains';
    Comparison[Comparison.Between] = 'Between';
    Comparison[Comparison.NotEqualTo] = 'NotEqualTo';
    /** @enum {number} */
    var DefaultComparison = {
        EqualTo: 12,
        NotEqualTo: 17,
    };
    DefaultComparison[DefaultComparison.EqualTo] = 'EqualTo';
    DefaultComparison[DefaultComparison.NotEqualTo] = 'NotEqualTo';
    /** @enum {number} */
    var TextComparison = {
        Contains: 15,
        DoesNotContain: 14,
        EndsWith: 13,
        EqualTo: 12,
        IsEmpty: 8,
        IsNotEmpty: 7,
        IsNotNull: 6,
        IsNotNullNorWhiteSpace: 5,
        IsNull: 4,
        IsNullOrWhiteSpace: 3,
        NotEqualTo: 17,
        StartsWith: 9,
    };
    TextComparison[TextComparison.Contains] = 'Contains';
    TextComparison[TextComparison.DoesNotContain] = 'DoesNotContain';
    TextComparison[TextComparison.EndsWith] = 'EndsWith';
    TextComparison[TextComparison.EqualTo] = 'EqualTo';
    TextComparison[TextComparison.IsEmpty] = 'IsEmpty';
    TextComparison[TextComparison.IsNotEmpty] = 'IsNotEmpty';
    TextComparison[TextComparison.IsNotNull] = 'IsNotNull';
    TextComparison[TextComparison.IsNotNullNorWhiteSpace] = 'IsNotNullNorWhiteSpace';
    TextComparison[TextComparison.IsNull] = 'IsNull';
    TextComparison[TextComparison.IsNullOrWhiteSpace] = 'IsNullOrWhiteSpace';
    TextComparison[TextComparison.NotEqualTo] = 'NotEqualTo';
    TextComparison[TextComparison.StartsWith] = 'StartsWith';
    /** @enum {number} */
    var NumberComparison = {
        Between: 16,
        EqualTo: 12,
        GreaterThan: 11,
        GreaterThanOrEqualTo: 10,
        LessThan: 2,
        LessThanOrEqualTo: 1,
        NotEqualTo: 17,
    };
    NumberComparison[NumberComparison.Between] = 'Between';
    NumberComparison[NumberComparison.EqualTo] = 'EqualTo';
    NumberComparison[NumberComparison.GreaterThan] = 'GreaterThan';
    NumberComparison[NumberComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
    NumberComparison[NumberComparison.LessThan] = 'LessThan';
    NumberComparison[NumberComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
    NumberComparison[NumberComparison.NotEqualTo] = 'NotEqualTo';
    /** @enum {number} */
    var BooleanComparison = {
        EqualTo: 12,
        NotEqualTo: 17,
    };
    BooleanComparison[BooleanComparison.EqualTo] = 'EqualTo';
    BooleanComparison[BooleanComparison.NotEqualTo] = 'NotEqualTo';
    /** @enum {number} */
    var DateComparison = {
        Between: 16,
        EqualTo: 12,
        GreaterThan: 11,
        GreaterThanOrEqualTo: 10,
        LessThan: 2,
        LessThanOrEqualTo: 1,
        NotEqualTo: 17,
    };
    DateComparison[DateComparison.Between] = 'Between';
    DateComparison[DateComparison.EqualTo] = 'EqualTo';
    DateComparison[DateComparison.GreaterThan] = 'GreaterThan';
    DateComparison[DateComparison.GreaterThanOrEqualTo] = 'GreaterThanOrEqualTo';
    DateComparison[DateComparison.LessThan] = 'LessThan';
    DateComparison[DateComparison.LessThanOrEqualTo] = 'LessThanOrEqualTo';
    DateComparison[DateComparison.NotEqualTo] = 'NotEqualTo';
    /** @enum {number} */
    var ComparisonType = {
        Default: 1,
        Text: 2,
        Number: 3,
        Date: 4,
        Boolean: 5,
    };
    ComparisonType[ComparisonType.Default] = 'Default';
    ComparisonType[ComparisonType.Text] = 'Text';
    ComparisonType[ComparisonType.Number] = 'Number';
    ComparisonType[ComparisonType.Date] = 'Date';
    ComparisonType[ComparisonType.Boolean] = 'Boolean';
    (function (ComparisonList) {
        /**
         * @param {?=} comparisonType
         * @return {?}
         */
        function values(comparisonType) {
            return Object.keys(this.getEnumAccordingToType(comparisonType)).filter(function (type) { return isNaN(( /** @type {?} */(type))) && type !== 'values'; }).sort();
        }
        ComparisonList.values = values;
        /**
         * @param {?=} comparisonType
         * @return {?}
         */
        function getEnumAccordingToType(comparisonType) {
            /** @type {?} */
            var enums;
            if (!comparisonType) {
                enums = DefaultComparison;
            }
            else if (comparisonType === ComparisonType.Default) {
                enums = DefaultComparison;
            }
            else if (comparisonType === ComparisonType.Text) {
                enums = TextComparison;
            }
            else if (comparisonType === ComparisonType.Number) {
                enums = NumberComparison;
            }
            else if (comparisonType === ComparisonType.Boolean) {
                enums = BooleanComparison;
            }
            else if (comparisonType === ComparisonType.Date) {
                enums = DateComparison;
            }
            return enums;
        }
        ComparisonList.getEnumAccordingToType = getEnumAccordingToType;
        /**
         * @param {?} comparisonType
         * @param {?=} key
         * @param {?=} label
         * @return {?}
         */
        function createKeyLabelArray(comparisonType, key, label) {
            var _this = this;
            if (key === void 0) {
                key = 'Id';
            }
            if (label === void 0) {
                label = 'Definition';
            }
            /** @type {?} */
            var enumKeys = this.values(comparisonType);
            /** @type {?} */
            var newKeyLabelArray = [];
            enumKeys.forEach(function (enumKey) {
                /** @type {?} */
                var newValue = {};
                newValue[key] = _this.getEnumAccordingToType(comparisonType)[enumKey];
                newValue[label] = enumKey;
                newKeyLabelArray.push(newValue);
            });
            return newKeyLabelArray;
        }
        ComparisonList.createKeyLabelArray = createKeyLabelArray;
    })(exports.ComparisonList || (exports.ComparisonList = {}));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var Conditions = [
        {
            'name': 'Equal',
            'id': 0
        },
        {
            'name': 'LessThan',
            'id': 1
        },
        {
            'name': 'LessThanOrEqual',
            'id': 2
        },
        {
            'name': 'GreaterThan',
            'id': 3
        },
        {
            'name': 'GreaterThanOrEqual',
            'id': 4
        },
        {
            'name': 'NotEqual',
            'id': 5
        },
        {
            'name': 'Contains',
            'id': 6
        },
        {
            'name': 'StartsWith',
            'id': 7
        },
        {
            'name': 'EndsWith',
            'id': 8
        }
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var FormButtonType = {
        Save: 1,
        Update: 2,
        Cancel: 3,
    };
    FormButtonType[FormButtonType.Save] = 'Save';
    FormButtonType[FormButtonType.Update] = 'Update';
    FormButtonType[FormButtonType.Cancel] = 'Cancel';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var LovColumnType = {
        Label: 'label',
        Text: 'text',
        Date: 'date',
        Select: 'select',
        Numeric: 'numeric',
        Boolean: 'boolean',
        Lov: 'lov',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var Module = {
        Auth: '/Auth/',
        SignIn: '/Auth/Token',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var OperationType = {
        None: 0,
        Created: 1,
        Updated: 2,
        Deleted: 3,
    };
    OperationType[OperationType.None] = 'None';
    OperationType[OperationType.Created] = 'Created';
    OperationType[OperationType.Updated] = 'Updated';
    OperationType[OperationType.Deleted] = 'Deleted';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var SortType = {
        Asc: 0,
        Desc: 1,
        None: 2,
    };
    SortType[SortType.Asc] = 'Asc';
    SortType[SortType.Desc] = 'Desc';
    SortType[SortType.None] = 'None';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var FormType = {
        New: 'new',
        Edit: 'edit',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var MaskType = {
        Phone: 'phone',
        Mail: 'mail',
        PostCode: 'postCode',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var AlertDismissReason = {
        Cancel: 'cancel',
        Backdrop: 'backdrop',
        Close: 'close',
        Esc: 'esc',
        Timer: 'timer',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var AlertType = {
        Warning: 'warning',
        Error: 'error',
        Success: 'success',
        Info: 'info',
        Question: 'question',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var Connector = {
        And: 0,
        Or: 1,
    };
    Connector[Connector.And] = 'And';
    Connector[Connector.Or] = 'Or';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var ConfirmDialogOperationType = {
        Add: 'add',
        Update: 'update',
        Delete: 'delete',
        Undo: 'undo',
        Cancel: 'cancel',
        Refresh: 'refresh',
        Process: 'process',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var ToastrMessages = {
        RecordDeleted: 'RecordDeleted',
        RecordAdded: 'RecordAdded',
        RecordUpdated: 'RecordUpdated',
        RecordSaved: 'RecordSaved',
        RecordReverted: 'RecordReverted',
        RecordCanceled: 'RecordCanceled',
        ValidationErrorMessage: 'ValidationErrorMessage',
        GridRefreshed: 'GridRefreshed',
        DateErrorMessage: 'DateErrorMessage',
        ActionCreated: 'ActionCreated',
        RecordNotFoundToDate: 'RecordNotFoundToDate',
        PasswordChangeMessage: 'PasswordChangeMessage',
        ResetPasswordMailMessage: 'ResetPasswordMailMessage',
        ResetPasswordMessage: 'ResetPasswordMessage',
        RelationRecordMessage: 'RelationRecordMessage',
        RelationValidationGroupMessage: 'RelationValidationGroupMessage',
        WorkDetailValidationErrorMessage: 'WorkDetailValidationErrorMessage',
        WorkSubtableValidationErrorMessage: 'WorkSubtableValidationErrorMessage',
        UsurnameAndPasswordMandatory: 'UsurnameAndPasswordMandatory',
        ChangePasswordNewPasswordAndConfirmPasswordMismatch: 'ChangePasswordNewPasswordAndConfirmPasswordMismatch',
        ChangePasswordOldPasswordAndConfirmPasswordSamewithold: 'ChangePasswordOldPasswordAndConfirmPasswordSamewithold',
        PackageNotAbleToChangeModule: 'PackageNotAbleToChangeModule',
        PackageNotAbleToChangeAuthType: 'PackageNotAbleToChangeAuthType',
        RoleNotAbleToChangeModule: 'RoleNotAbleToChangeModule',
        RoleNotAbleToChangePackage: 'RoleNotAbleToChangePackage',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var Storage = {
        AccessToken: 'access_token',
        RefreshToken: 'refresh_token',
        Expires: 'expires',
        User: 'user',
        SystemRefDate: 'system_ref_date',
        CurrentLanguage: 'current_language',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var Headers = {
        CoreId: 'X-CoreId',
        Behaviour: 'X-Behaviour',
        ReferenceDate: 'ReferenceDate',
        PagingHeader: 'X-Paging-Header',
        PagingResponseHeader: 'X-Paging-Response-Header',
        ReferenceDateHeader: 'X-ReferenceDate-Header',
        DisableLoading: 'disableGeneralLoading',
        DisableNavigation: 'disableNavigation',
        FileOperation: 'fileOperation',
        ContentType: 'Content-Type',
        Authentication: 'Authentication',
        BreadcrumbCustomLabel: 'BreadcrumbCustomLabel',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var StorageType = {
        LocalStorage: 'localStorage',
        SessionStorage: 'sessionStorage',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {string} */
    var DateSelectionMode = {
        Single: 'single',
        Multiple: 'multiple',
        Range: 'range',
    };

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BaseModuleUrl = /** @class */ (function () {
        function BaseModuleUrl(url) {
            this.url = url;
            this.moduleUrl = url;
        }
        return BaseModuleUrl;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DateSettings = /** @class */ (function () {
        function DateSettings() {
            this.disableUTC = false;
            this.showTimeOnly = false;
            // current year
            this.defaultYear = (new Date()).getFullYear();
            // 0 = january
            this.defaultMonth = 0;
            // 1
            this.defaultDay = 1;
            //'dd.mm.yy'
            this.dateFormat = 'dd.mm.yy';
            this.showYear = false;
            this.showTime = false;
            this.hideCalendarButton = false;
            //  today and clear buttons
            this.showButtonBar = true;
            this.showMonthPicker = false;
            this.enableSeconds = false;
            this.selectionMode = DateSelectionMode.Single;
        }
        return DateSettings;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Filter = /** @class */ (function () {
        function Filter() {
        }
        return Filter;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FilterGroup = /** @class */ (function () {
        function FilterGroup() {
        }
        return FilterGroup;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LovColumn = /** @class */ (function () {
        /** visible parameter default is true */
        function LovColumn(name, headerName, type, visible, isKeyField) {
            this.Name = name;
            this.HeaderName = headerName;
            this.Type = type;
            this.Visible = _.isNil(visible) ? true : visible;
            this.IsKeyField = _.isNil(isKeyField) ? false : isKeyField;
        }
        return LovColumn;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ConfirmDialogSettings = /** @class */ (function () {
        function ConfirmDialogSettings() {
        }
        return ConfirmDialogSettings;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GetListByResult = /** @class */ (function () {
        function GetListByResult() {
        }
        return GetListByResult;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var /**
     * @template T
     */ ServiceResult = /** @class */ (function () {
        function ServiceResult() {
        }
        return ServiceResult;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BaseModel = /** @class */ (function () {
        function BaseModel() {
        }
        return BaseModel;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SafePipe = /** @class */ (function () {
        function SafePipe(sanitizer) {
            this.sanitizer = sanitizer;
        }
        /**
         * @param {?} value
         * @param {?} type
         * @return {?}
         */
        SafePipe.prototype.transform = /**
         * @param {?} value
         * @param {?} type
         * @return {?}
         */
            function (value, type) {
                switch (type) {
                    case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
                    case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
                    case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
                    case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
                    case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
                    default: throw new Error("Invalid safe type specified: " + type);
                }
            };
        SafePipe.decorators = [
            { type: i0.Pipe, args: [{
                        name: 'safe'
                    },] }
        ];
        /** @nocollapse */
        SafePipe.ctorParameters = function () {
            return [
                { type: platformBrowser.DomSanitizer }
            ];
        };
        return SafePipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AppConfig = /** @class */ (function () {
        function AppConfig(injector) {
            this.injector = injector;
        }
        /**
         * @return {?}
         */
        AppConfig.prototype.load = /**
         * @return {?}
         */
            function () {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    /** @type {?} */
                    var http$$1 = _this.injector.get(http.HttpClient);
                    http$$1.get('/assets/config/config.json')
                        .toPromise()
                        .then(function (response) {
                        AppConfig.settings = ( /** @type {?} */(response));
                        resolve();
                    }).catch(function (response) {
                        console.log(response);
                        reject('Could not load file /assets/config.json');
                    });
                });
            };
        AppConfig.settings = ( /** @type {?} */({}));
        AppConfig.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        AppConfig.ctorParameters = function () {
            return [
                { type: i0.Injector }
            ];
        };
        return AppConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreModalService = /** @class */ (function () {
        function CoreModalService() {
            this.isModalDisplayed = new BehaviorSubject.BehaviorSubject(false);
        }
        /**
         * @return {?}
         */
        CoreModalService.prototype.getModalDisplayStatus = /**
         * @return {?}
         */
            function () {
                return this.isModalDisplayed.asObservable();
            };
        /**
         * @param {?} status
         * @return {?}
         */
        CoreModalService.prototype.setModalDisplayStatus = /**
         * @param {?} status
         * @return {?}
         */
            function (status) {
                this.isModalDisplayed.next(status || false);
            };
        CoreModalService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        CoreModalService.ctorParameters = function () { return []; };
        /** @nocollapse */ CoreModalService.ngInjectableDef = i0.defineInjectable({ factory: function CoreModalService_Factory() { return new CoreModalService(); }, token: CoreModalService, providedIn: "root" });
        return CoreModalService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BreadcrumbService = /** @class */ (function () {
        function BreadcrumbService() {
            this.breadcrumbsWithCustomLabels = [];
        }
        /**
         * @param {?} label
         * @param {?} url
         * @param {?} customLabel
         * @return {?}
         */
        BreadcrumbService.prototype.addCustomLabel = /**
         * @param {?} label
         * @param {?} url
         * @param {?} customLabel
         * @return {?}
         */
            function (label, url, customLabel) {
                /** @type {?} */
                var indexOfBreadcrumbItem = this.breadcrumbsWithCustomLabels.findIndex(function (x) { return x.label === label && x.url === url; });
                if (indexOfBreadcrumbItem !== -1) {
                    this.breadcrumbsWithCustomLabels[indexOfBreadcrumbItem]['customLabel'] = customLabel;
                }
                else {
                    /** @type {?} */
                    var breadcrumb = {
                        label: label,
                        url: url,
                        customLabel: customLabel
                    };
                    this.breadcrumbsWithCustomLabels.push(breadcrumb);
                }
                // update the current list after send
                if (this.breadcrumbs) {
                    /** @type {?} */
                    var currentBreadcrumbItem = this.breadcrumbs.find(function (x) { return x.label === label && x.url === url; });
                    if (currentBreadcrumbItem) {
                        currentBreadcrumbItem['customLabel'] = customLabel;
                    }
                }
            };
        BreadcrumbService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        BreadcrumbService.ctorParameters = function () { return []; };
        /** @nocollapse */ BreadcrumbService.ngInjectableDef = i0.defineInjectable({ factory: function BreadcrumbService_Factory() { return new BreadcrumbService(); }, token: BreadcrumbService, providedIn: "root" });
        return BreadcrumbService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingService = /** @class */ (function () {
        function LoadingService() {
            this.loadingRequests = [];
            this.loadingBarDisplayStatus = new BehaviorSubject.BehaviorSubject(false);
        }
        /**
         * @return {?}
         */
        LoadingService.prototype.getLoadingBarDisplayStatus = /**
         * @return {?}
         */
            function () {
                return this.loadingBarDisplayStatus.asObservable();
            };
        /**
         * @param {?} request
         * @return {?}
         */
        LoadingService.prototype.insertLoadingRequest = /**
         * @param {?} request
         * @return {?}
         */
            function (request) {
                if (!_.isNull(request)) {
                    this.loadingRequests.push(request);
                    this.checkLoadingDisplayStatus();
                }
            };
        /**
         * @param {?} request
         * @return {?}
         */
        LoadingService.prototype.removeLoadingRequest = /**
         * @param {?} request
         * @return {?}
         */
            function (request) {
                if (!_.isNull(request)) {
                    /** @type {?} */
                    var requestIndex = this.loadingRequests.indexOf(request);
                    if (requestIndex !== -1) {
                        this.loadingRequests.splice(requestIndex, 1);
                    }
                    this.checkLoadingDisplayStatus();
                }
            };
        /**
         * @param {?} status
         * @return {?}
         */
        LoadingService.prototype.setLoadingBarSetting = /**
         * @param {?} status
         * @return {?}
         */
            function (status) {
                this.loadingBarDisplayStatus.next(status);
            };
        /**
         * @return {?}
         */
        LoadingService.prototype.checkLoadingDisplayStatus = /**
         * @return {?}
         */
            function () {
                if (this.loadingRequests && this.loadingRequests.length > 0) {
                    this.setLoadingBarSetting(true);
                }
                else {
                    this.setLoadingBarSetting(false);
                }
            };
        LoadingService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        LoadingService.ctorParameters = function () { return []; };
        return LoadingService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PreventUnsavedChangesService = /** @class */ (function () {
        function PreventUnsavedChangesService() {
            this.status = new rxjs.Subject();
        }
        /**
         * @return {?}
         */
        PreventUnsavedChangesService.prototype.getIsChanged = /**
         * @return {?}
         */
            function () {
                return this.isChanged;
            };
        /**
         * @param {?} isChanged
         * @return {?}
         */
        PreventUnsavedChangesService.prototype.setIsChanged = /**
         * @param {?} isChanged
         * @return {?}
         */
            function (isChanged) {
                this.isChanged = isChanged;
            };
        /**
         * @return {?}
         */
        PreventUnsavedChangesService.prototype.getStatus = /**
         * @return {?}
         */
            function () {
                return this.status.asObservable();
            };
        /**
         * @param {?} status
         * @return {?}
         */
        PreventUnsavedChangesService.prototype.setStatus = /**
         * @param {?} status
         * @return {?}
         */
            function (status) {
                this.status.next(status);
            };
        PreventUnsavedChangesService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        PreventUnsavedChangesService.ctorParameters = function () { return []; };
        return PreventUnsavedChangesService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var StorageService = /** @class */ (function () {
        function StorageService() {
            this.onSubject = new Subject.Subject();
            this.changes = this.onSubject.asObservable().share();
            this.start();
        }
        /**
         * @return {?}
         */
        StorageService.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.stop();
            };
        /**
         * @param {?=} storageType
         * @return {?}
         */
        StorageService.prototype.getStorage = /**
         * @param {?=} storageType
         * @return {?}
         */
            function (storageType) {
                /** @type {?} */
                var storage = storageType || StorageType.LocalStorage;
                /** @type {?} */
                var s = [];
                for (var i = 0; i < localStorage.length; i++) {
                    s.push({
                        key: storage === StorageType.LocalStorage ? localStorage.key(i) : sessionStorage.key(i),
                        value: storage === StorageType.LocalStorage ?
                            JSON.parse(localStorage.getItem(localStorage.key(i))) : JSON.parse(localStorage.getItem(sessionStorage.key(i)))
                    });
                }
                return s;
            };
        /**
         * @param {?} key
         * @param {?} data
         * @param {?=} storageType
         * @return {?}
         */
        StorageService.prototype.store = /**
         * @param {?} key
         * @param {?} data
         * @param {?=} storageType
         * @return {?}
         */
            function (key, data, storageType) {
                /** @type {?} */
                var storage = storageType || StorageType.LocalStorage;
                if (storage === StorageType.LocalStorage) {
                    localStorage.setItem(key, JSON.stringify(data));
                }
                else {
                    sessionStorage.setItem(key, JSON.stringify(data));
                }
                // the local application doesn't seem to catch changes to localStorage...
                this.onSubject.next({ key: key, value: data });
            };
        /**
         * @param {?} key
         * @param {?=} storageType
         * @return {?}
         */
        StorageService.prototype.getStored = /**
         * @param {?} key
         * @param {?=} storageType
         * @return {?}
         */
            function (key, storageType) {
                /** @type {?} */
                var storage = storageType || StorageType.LocalStorage;
                try {
                    if (storage === StorageType.LocalStorage) {
                        return JSON.parse(localStorage.getItem(key));
                    }
                    else {
                        return JSON.parse(sessionStorage.getItem(key));
                    }
                }
                catch (e) {
                    if (storage === StorageType.LocalStorage) {
                        return localStorage.getItem(key);
                    }
                    else {
                        return sessionStorage.getItem(key);
                    }
                }
            };
        /**
         * @param {?} key
         * @param {?=} storageType
         * @return {?}
         */
        StorageService.prototype.clear = /**
         * @param {?} key
         * @param {?=} storageType
         * @return {?}
         */
            function (key, storageType) {
                /** @type {?} */
                var storage = storageType || StorageType.LocalStorage;
                if (storage === StorageType.LocalStorage) {
                    localStorage.removeItem(key);
                }
                else {
                    sessionStorage.removeItem(key);
                }
                // the local application doesn't seem to catch changes to localStorage...
                this.onSubject.next({ key: key, value: null });
            };
        /**
         * @private
         * @return {?}
         */
        StorageService.prototype.start = /**
         * @private
         * @return {?}
         */
            function () {
                window.addEventListener('storage', this.storageEventListener.bind(this));
            };
        /**
         * @private
         * @param {?} event
         * @return {?}
         */
        StorageService.prototype.storageEventListener = /**
         * @private
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (event.storageArea == localStorage) {
                    /** @type {?} */
                    var v = void 0;
                    try {
                        v = JSON.parse(event.newValue);
                    }
                    catch (e) {
                        v = event.newValue;
                    }
                    this.onSubject.next({ key: event.key, value: v });
                }
            };
        /**
         * @private
         * @return {?}
         */
        StorageService.prototype.stop = /**
         * @private
         * @return {?}
         */
            function () {
                window.removeEventListener('storage', this.storageEventListener.bind(this));
                this.onSubject.complete();
            };
        StorageService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        StorageService.ctorParameters = function () { return []; };
        return StorageService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TranslateHelperService = /** @class */ (function () {
        function TranslateHelperService(translate, storageService) {
            this.translate = translate;
            this.storageService = storageService;
        }
        /**
         * @param {?} lang
         * @return {?}
         */
        TranslateHelperService.prototype.setInitialLanguage = /**
         * @param {?} lang
         * @return {?}
         */
            function (lang) {
                this.translate.setDefaultLang(lang);
            };
        /**
         * @param {?} language
         * @return {?}
         */
        TranslateHelperService.prototype.setSelectedLanguage = /**
         * @param {?} language
         * @return {?}
         */
            function (language) {
                this.translate.use(language);
                this.storageService.store(Storage.CurrentLanguage, language);
            };
        /**
         * @param {?} key
         * @return {?}
         */
        TranslateHelperService.prototype.instant = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                var _this = this;
                /** @type {?} */
                var result = new Promise(function (resolve) {
                    _this.translate.get(key).toPromise();
                });
                return result;
            };
        /**
         * @param {?} key
         * @return {?}
         */
        TranslateHelperService.prototype.get = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                return this.translate.get(key);
            };
        TranslateHelperService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        TranslateHelperService.ctorParameters = function () {
            return [
                { type: core.TranslateService },
                { type: StorageService }
            ];
        };
        return TranslateHelperService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SweetAlertService = /** @class */ (function () {
        function SweetAlertService(translate) {
            this.translate = translate;
            this._swal = Swal;
            // this.translate.setInitialLanguage('tr');
        }
        // openConfirmPopup() {
        //   const swalTitle = 'ConfirmDialogAreYouSure';
        //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
        //   const swalConfirmBtnText = 'ConfirmButtonText';
        //   const swalCancelBtnText = 'CancelModalButton';
        //
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
        // }
        // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
        //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
        //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
        //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
        //   const swalCancelBtnText = 'CancelModalButton';
        //   const swalType = type || AlertType.Warning;
        //
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
        // }
        // openConfirmPopup() {
        //   const swalTitle = 'ConfirmDialogAreYouSure';
        //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
        //   const swalConfirmBtnText = 'ConfirmButtonText';
        //   const swalCancelBtnText = 'CancelModalButton';
        //
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
        // }
        // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
        //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
        //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
        //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
        //   const swalCancelBtnText = 'CancelModalButton';
        //   const swalType = type || AlertType.Warning;
        //
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
        // }
        /**
         * @param {?=} title
         * @param {?=} text
         * @param {?=} type
         * @param {?=} confirmButtonText
         * @param {?=} showCancelButton
         * @return {?}
         */
        SweetAlertService.prototype.preventUnsavedChangedPopup =
            // openConfirmPopup() {
            //   const swalTitle = 'ConfirmDialogAreYouSure';
            //   const swalText = 'ConfirmDialogNotAbleToRevertRecord';
            //   const swalConfirmBtnText = 'ConfirmButtonText';
            //   const swalCancelBtnText = 'CancelModalButton';
            //
            //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            // }
            // openDirtyFormPopup(title?: string, text?: string, type?: AlertType, confirmButtonText?: string, showCancelButton = true) {
            //   const swalTitle = title ? title : 'ConfirmDialogAreYouSure';
            //   const swalText = text ? text : 'ConfirmDialogLoseChanges';
            //   const swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmButtonContinue';
            //   const swalCancelBtnText = 'CancelModalButton';
            //   const swalType = type || AlertType.Warning;
            //
            //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton);
            // }
            /**
             * @param {?=} title
             * @param {?=} text
             * @param {?=} type
             * @param {?=} confirmButtonText
             * @param {?=} showCancelButton
             * @return {?}
             */
            function (title, text, type, confirmButtonText, showCancelButton) {
                if (showCancelButton === void 0) {
                    showCancelButton = true;
                }
                /** @type {?} */
                var swalTitle = title ? title : 'ConfirmDialogLoseChangesCaption';
                /** @type {?} */
                var swalText = text ? text : 'ConfirmDialogLoseChangesInfo';
                /** @type {?} */
                var swalConfirmBtnText = confirmButtonText ? confirmButtonText : 'ConfirmDialogLoseChangesOk';
                /** @type {?} */
                var swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning, showCancelButton);
            };
        /**
         * @param {?} confirmDialogOperationType
         * @param {?=} confirmDialogSettings
         * @return {?}
         */
        SweetAlertService.prototype.manageRequestOperationWithConfirm = /**
         * @param {?} confirmDialogOperationType
         * @param {?=} confirmDialogSettings
         * @return {?}
         */
            function (confirmDialogOperationType, confirmDialogSettings) {
                switch (confirmDialogOperationType) {
                    case ConfirmDialogOperationType.Add:
                        return this.getAddOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Update:
                        return this.getUpdateOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Delete:
                        return this.getDeleteOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Undo:
                        return this.getUndoOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Cancel:
                        return this.getCancelOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Refresh:
                        return this.getRefreshOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    case ConfirmDialogOperationType.Process:
                        return this.getProcessOperationConfirmPopupWithConfirm(confirmDialogSettings);
                    default:
                        break;
                }
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getProcessOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogProcessCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogProcessInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogProcessOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogProcessCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getAddOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getUpdateOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogSaveRecordCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogSaveRecordInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogSaveRecordOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogSaveRecordCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Info);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getDeleteOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogDeleteRecordCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogDeleteRecordInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogDeleteRecordOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogDeleteRecordCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getUndoOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogNotAbleToRevertRecordCapiton';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogNotAbleToRevertRecordInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogNotAbleToRevertRecordOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogNotAbleToRevertRecordCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getCancelOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogLoseChangesCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogLoseChangesInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogLoseChangesOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogLoseChangesCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            };
        /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.getRefreshOperationConfirmPopupWithConfirm = /**
         * @private
         * @param {?=} settings
         * @return {?}
         */
            function (settings) {
                if (settings === void 0) {
                    settings = {};
                }
                /** @type {?} */
                var swalTitle = settings.Title || 'ConfirmDialogRefreshCaption';
                /** @type {?} */
                var swalText = settings.Text || 'ConfirmDialogRefreshInfo';
                /** @type {?} */
                var swalConfirmBtnText = settings.ConfirmBtnText || 'ConfirmDialogRefreshsOk';
                /** @type {?} */
                var swalCancelBtnText = settings.CancelBtnText || 'ConfirmDialogRefreshCancel';
                return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            };
        /**
         * @param {?} result
         * @param {?=} settings
         * @return {?}
         */
        SweetAlertService.prototype.confirmOperationPopup = /**
         * @param {?} result
         * @param {?=} settings
         * @return {?}
         */
            function (result, settings) {
                if (settings === void 0) {
                    settings = {};
                }
                if (result.value) {
                    /** @type {?} */
                    var swalTitle = settings.title || 'Deleted!';
                    /** @type {?} */
                    var swalText = settings.text || 'Your file has been deleted.';
                    /** @type {?} */
                    var swalType = settings.type || AlertType.Success;
                    return this.openCompletedConfirmDialog(swalTitle, swalText, swalType);
                }
                else if (result.dismiss === this._swal.DismissReason.cancel) {
                    return this.openCompletedConfirmDialog('Cancelled', 'OperationCanceled', AlertType.Error);
                }
            };
        // loadingSweetAlert() {
        //   swal.showLoading();
        // }
        //
        // cancelLoadingSweetAlert() {
        //   swal.hideLoading();
        // }
        // cancelEditFormConfirmModal() {
        //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
        //   const swalText = 'ConfirmDialogLoseChangesInfo';
        //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
        //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
        // }
        // loadingSweetAlert() {
        //   swal.showLoading();
        // }
        //
        // cancelLoadingSweetAlert() {
        //   swal.hideLoading();
        // }
        // cancelEditFormConfirmModal() {
        //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
        //   const swalText = 'ConfirmDialogLoseChangesInfo';
        //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
        //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
        //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
        // }
        /**
         * @private
         * @param {?} swalTitle
         * @param {?} swalText
         * @param {?=} swalType
         * @return {?}
         */
        SweetAlertService.prototype.openCompletedConfirmDialog =
            // loadingSweetAlert() {
            //   swal.showLoading();
            // }
            //
            // cancelLoadingSweetAlert() {
            //   swal.hideLoading();
            // }
            // cancelEditFormConfirmModal() {
            //   const swalTitle = 'ConfirmDialogLoseChangesCaption';
            //   const swalText = 'ConfirmDialogLoseChangesInfo';
            //   const swalConfirmBtnText = 'ConfirmDialogLoseChangesOk';
            //   const swalCancelBtnText = 'ConfirmDialogLoseChangesCancel';
            //   return this.openConfirmDialog(swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, AlertType.Warning);
            // }
            /**
             * @private
             * @param {?} swalTitle
             * @param {?} swalText
             * @param {?=} swalType
             * @return {?}
             */
            function (swalTitle, swalText, swalType) {
                if (swalType === void 0) {
                    swalType = AlertType.Info;
                }
                swalTitle = swalTitle || '';
                swalText = swalText || '';
                swalType = swalType || AlertType.Info;
                return this.translate.get([swalTitle, swalText])
                    .pipe(operators.flatMap(function (translatedResult) { return rxjs.from(Swal.fire(translatedResult[swalTitle], translatedResult[swalText], swalType)); }, function (translatedResult, result) {
                    /** @type {?} */
                    var alertResult = {};
                    if (result.value) {
                        alertResult.value = result.value;
                    }
                    if (result.dismiss) {
                        alertResult.dismiss = ( /** @type {?} */(result.dismiss.toString()));
                    }
                    return alertResult;
                }));
            };
        /**
         * @private
         * @param {?} swalTitle
         * @param {?} swalText
         * @param {?} swalConfirmBtnText
         * @param {?} swalCancelBtnText
         * @param {?} swalType
         * @param {?=} showCancelButton
         * @return {?}
         */
        SweetAlertService.prototype.openConfirmDialog = /**
         * @private
         * @param {?} swalTitle
         * @param {?} swalText
         * @param {?} swalConfirmBtnText
         * @param {?} swalCancelBtnText
         * @param {?} swalType
         * @param {?=} showCancelButton
         * @return {?}
         */
            function (swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText, swalType, showCancelButton) {
                if (showCancelButton === void 0) {
                    showCancelButton = true;
                }
                swalTitle = swalTitle || '';
                swalText = swalText || '';
                swalConfirmBtnText = swalConfirmBtnText || '';
                swalCancelBtnText = swalCancelBtnText || '';
                return this.translate.get([swalTitle, swalText, swalConfirmBtnText, swalCancelBtnText])
                    .pipe(operators.flatMap(function (translatedResult) {
                    return rxjs.from(Swal.fire({
                        title: translatedResult[swalTitle],
                        text: translatedResult[swalText],
                        type: swalType ? swalType : AlertType.Warning,
                        confirmButtonText: translatedResult[swalConfirmBtnText],
                        showCancelButton: showCancelButton,
                        cancelButtonText: translatedResult[swalCancelBtnText],
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-info mr-20',
                        cancelButtonClass: 'btn btn-danger',
                        customClass: 'sweet-confirm-dialog',
                    }));
                }, function (translatedResult, result) {
                    /** @type {?} */
                    var alertResult = {};
                    if (result.value) {
                        alertResult.value = result.value;
                    }
                    if (result.dismiss) {
                        alertResult.dismiss = ( /** @type {?} */(result.dismiss.toString()));
                    }
                    return alertResult;
                }));
            };
        SweetAlertService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        SweetAlertService.ctorParameters = function () {
            return [
                { type: TranslateHelperService }
            ];
        };
        return SweetAlertService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SystemReferenceDateService = /** @class */ (function () {
        function SystemReferenceDateService(storageService) {
            this.storageService = storageService;
            this.systemDate = '';
            this.dateChange = new BehaviorSubject.BehaviorSubject('');
        }
        /**
         * @param {?} newDate
         * @return {?}
         */
        SystemReferenceDateService.prototype.setSelectedReferenceDate = /**
         * @param {?} newDate
         * @return {?}
         */
            function (newDate) {
                this.systemDate = newDate;
                this.storageService.store(Storage.SystemRefDate, newDate, StorageType.SessionStorage);
                this.dateChange.next(newDate);
            };
        /**
         * @return {?}
         */
        SystemReferenceDateService.prototype.checkIsNullAndChangeCurrentDateWithStorage = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var systemDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
                if (systemDate && systemDate !== this.systemDate) {
                    this.systemDate = systemDate;
                    this.dateChange.next(systemDate);
                }
                else if (!systemDate) {
                    return true;
                }
                return false;
            };
        /**
         * @return {?}
         */
        SystemReferenceDateService.prototype.getSelectedReferenceDate = /**
         * @return {?}
         */
            function () {
                return this.dateChange.asObservable();
            };
        /**
         * @return {?}
         */
        SystemReferenceDateService.prototype.getSystemDate = /**
         * @return {?}
         */
            function () {
                return this.systemDate;
            };
        /**
         * @return {?}
         */
        SystemReferenceDateService.prototype.getCurrentDateAsISO = /**
         * @return {?}
         */
            function () {
                return this.convertDateToISO(new Date);
            };
        /**
         * @param {?} date
         * @return {?}
         */
        SystemReferenceDateService.prototype.convertDateToISO = /**
         * @param {?} date
         * @return {?}
         */
            function (date) {
                /** @type {?} */
                var newDate = new Date(date);
                /** @type {?} */
                var year = newDate.getFullYear();
                /** @type {?} */
                var month = '' + (newDate.getMonth() + 1);
                /** @type {?} */
                var day = '' + newDate.getDate();
                if (month.length < 2) {
                    month = '0' + month;
                }
                if (day.length < 2) {
                    day = '0' + day;
                }
                return [year, month, day].join('-') + 'T00:00:00.000Z';
            };
        /**
         * @param {?} dateAsString
         * @return {?}
         */
        SystemReferenceDateService.prototype.convertDateToDateUTC = /**
         * @param {?} dateAsString
         * @return {?}
         */
            function (dateAsString) {
                /** @type {?} */
                var newDate = new Date(dateAsString);
                return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
            };
        SystemReferenceDateService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        SystemReferenceDateService.ctorParameters = function () {
            return [
                { type: StorageService }
            ];
        };
        return SystemReferenceDateService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // declare let toastr: any;
    var ToastrUtilsService = /** @class */ (function () {
        function ToastrUtilsService(toastr, translate) {
            this.toastr = toastr;
            this.translate = translate;
        }
        /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
        ToastrUtilsService.prototype.success = /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
            function (message, title) {
                this.translateValue(message, 'success', title);
            };
        /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
        ToastrUtilsService.prototype.info = /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
            function (message, title) {
                this.translateValue(message, 'info', title);
            };
        /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
        ToastrUtilsService.prototype.warning = /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
            function (message, title) {
                this.translateValue(message, 'warning', title);
            };
        /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
        ToastrUtilsService.prototype.error = /**
         * @param {?} message
         * @param {?=} title
         * @return {?}
         */
            function (message, title) {
                this.translateValue(message, 'error', title);
            };
        /**
         * @param {?} message
         * @param {?} messageType
         * @param {?=} title
         * @return {?}
         */
        ToastrUtilsService.prototype.translateValue = /**
         * @param {?} message
         * @param {?} messageType
         * @param {?=} title
         * @return {?}
         */
            function (message, messageType, title) {
                var _this = this;
                title = title || '';
                message = message || '';
                this.translate.get([message, title]).subscribe(function (translatedResult) {
                    if (messageType === 'success') {
                        _this.toastr.success(translatedResult[message] || message, translatedResult[title] || title);
                    }
                    else if (messageType === 'info') {
                        _this.toastr.info(translatedResult[message] || message, translatedResult[title] || title);
                    }
                    else if (messageType === 'warning') {
                        _this.toastr.warning(translatedResult[message] || message, translatedResult[title] || title);
                    }
                    else if (messageType === 'error') {
                        _this.toastr.error(translatedResult[message] || message, translatedResult[title] || title);
                    }
                }, function (error) {
                });
            };
        ToastrUtilsService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        ToastrUtilsService.ctorParameters = function () {
            return [
                { type: ngxToastr.ToastrService },
                { type: TranslateHelperService }
            ];
        };
        return ToastrUtilsService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ValidationService = /** @class */ (function () {
        function ValidationService() {
        }
        /**
         * @param {?} control
         * @return {?}
         */
        ValidationService.tcValidate = /**
         * @param {?} control
         * @return {?}
         */
            function (control) {
                /** @type {?} */
                var value = control.value;
                // event.target.value;
                /** @type {?} */
                var isZero = false;
                value = String(value);
                // Sadece rakamlardan oluşup oluşmadığını ve 11 haneli olup olmadığını kontrol ediyoruz.
                /** @type {?} */
                var isEleven = /^[0-9]{11}$/.test(value);
                // İlk rakamın 0 ile başlama kontrolü
                /** @type {?} */
                var firstValue = value.substr(0, 1);
                if (firstValue === '0') {
                    isZero = true;
                }
                else {
                    isZero = false;
                }
                /** @type {?} */
                var totalX = 0;
                for (var i = 0; i < 10; i++) {
                    totalX += Number(value.substr(i, 1));
                }
                // İlk 10 hanesinin toplamınınn 11. haneyi verme kontrolü
                /** @type {?} */
                var isRuleX = String(totalX % 10) === value.substr(10, 1);
                // tek ve çift hanelerin toplamlarının atanacağı degişkenleri tanımlıyoruz
                /** @type {?} */
                var totalY1 = 0;
                /** @type {?} */
                var totalY2 = 0;
                // tek hanelerdeki sayıları toplar
                for (var j = 0; j < 10; j += 2) {
                    totalY1 += Number(value.substr(j, 1));
                }
                // çift hanelerdeki sayıları toplar
                for (var k = 1; k < 9; k += 2) {
                    totalY2 += Number(value.substr(k, 1));
                }
                /** @type {?} */
                var isRuleY = String(((totalY1 * 7) - totalY2) % 10) === value.substr(9, 1);
                /** @type {?} */
                var deger = isEleven && !isZero && isRuleX && isRuleY;
                // this.isTrue = deger === true ? true : false;
                if (!(deger === true ? true : false)) {
                    return { 'identificationNumberIsNotValid': true };
                }
                else {
                    return null;
                }
            };
        /**
         * @param {?} control
         * @return {?}
         */
        ValidationService.spaceControl = /**
         * @param {?} control
         * @return {?}
         */
            function (control) {
                /** @type {?} */
                var value = control.value;
                value = String(value);
                /** @type {?} */
                var usingSpace = false;
                if (value === '' || value.indexOf(' ') > -1) {
                    usingSpace = true;
                }
                if (usingSpace) {
                    return { 'usingSpaceValid': true };
                }
                else {
                    return null;
                }
            };
        return ValidationService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PreventUnsavedChangesGuard = /** @class */ (function () {
        function PreventUnsavedChangesGuard(sweetAlert, preventUnsavedChangesService) {
            this.sweetAlert = sweetAlert;
            this.preventUnsavedChangesService = preventUnsavedChangesService;
        }
        /**
         * @return {?}
         */
        PreventUnsavedChangesGuard.prototype.canDeactivate = /**
         * @return {?}
         */
            function () {
                var _this = this;
                try {
                    setTimeout(function () {
                        _this.preventUnsavedChangesService.setStatus('waiting');
                    }, 100);
                    return this.preventUnsavedChangesService.getStatus()
                        .pipe(operators.concatMap(function (result) {
                        if (result === 'finished') {
                            /** @type {?} */
                            var isChanged = _this.preventUnsavedChangesService.getIsChanged();
                            if (isChanged) {
                                return _this.sweetAlert.preventUnsavedChangedPopup()
                                    .pipe(operators.map(function (alertResult) {
                                    // console.log('canDeactivate in pipe', alertResult);
                                    if (alertResult.value) {
                                        _this.preventUnsavedChangesService.setIsChanged(false);
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }));
                            }
                            else {
                                _this.preventUnsavedChangesService.setIsChanged(false);
                                return rxjs.of(true);
                            }
                        }
                        // return result;
                    }), operators.catchError(function (error) {
                        console.error(error);
                        return rxjs.of(true);
                    }));
                }
                catch (err) {
                    console.error(err);
                    return true;
                }
            };
        PreventUnsavedChangesGuard.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        PreventUnsavedChangesGuard.ctorParameters = function () {
            return [
                { type: SweetAlertService },
                { type: PreventUnsavedChangesService }
            ];
        };
        /** @nocollapse */ PreventUnsavedChangesGuard.ngInjectableDef = i0.defineInjectable({ factory: function PreventUnsavedChangesGuard_Factory() { return new PreventUnsavedChangesGuard(i0.inject(SweetAlertService), i0.inject(PreventUnsavedChangesService)); }, token: PreventUnsavedChangesGuard, providedIn: "root" });
        return PreventUnsavedChangesGuard;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BaseHttpService = /** @class */ (function () {
        function BaseHttpService(httpClient) {
            this.httpClient = httpClient;
        }
        /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
        BaseHttpService.prototype.httpGet = /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
            function (requestUrl, headers) {
                if (requestUrl === void 0) {
                    requestUrl = '';
                }
                return this.httpClient.get(requestUrl, {
                    headers: headers,
                    observe: 'response'
                });
            };
        /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
        BaseHttpService.prototype.httpGetFile = /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
            function (requestUrl, headers) {
                if (requestUrl === void 0) {
                    requestUrl = '';
                }
                return this.httpClient.get(requestUrl, {
                    headers: headers,
                    observe: 'response',
                    responseType: 'blob'
                });
            };
        /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} data
         * @param {?=} headers
         * @return {?}
         */
        BaseHttpService.prototype.httpPost = /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} data
         * @param {?=} headers
         * @return {?}
         */
            function (requestUrl, data, headers) {
                if (requestUrl === void 0) {
                    requestUrl = '';
                }
                /** @type {?} */
                var fileOperation;
                if (headers) {
                    fileOperation = headers.get(Headers.FileOperation);
                }
                return this.httpClient.post(requestUrl, fileOperation ? data : JSON.stringify(data), {
                    headers: headers
                });
            };
        /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} data
         * @param {?=} headers
         * @return {?}
         */
        BaseHttpService.prototype.httpPatch = /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} data
         * @param {?=} headers
         * @return {?}
         */
            function (requestUrl, data, headers) {
                if (requestUrl === void 0) {
                    requestUrl = '';
                }
                /** @type {?} */
                var fileOperation;
                if (headers) {
                    fileOperation = headers.get(Headers.FileOperation);
                }
                return this.httpClient.patch(requestUrl, fileOperation ? data : JSON.stringify(data), {
                    headers: headers
                });
            };
        /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
        BaseHttpService.prototype.httpDelete = /**
         * @template T
         * @param {?=} requestUrl
         * @param {?=} headers
         * @return {?}
         */
            function (requestUrl, headers) {
                if (requestUrl === void 0) {
                    requestUrl = '';
                }
                return this.httpClient.delete(requestUrl, {
                    headers: headers
                });
            };
        /**
         * @protected
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseHttpService.prototype.getHttpHeader = /**
         * @protected
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
            function (filterQuery, headerParameters) {
                /** @type {?} */
                var httpHeaders = new http.HttpHeaders();
                if (filterQuery) {
                    httpHeaders = httpHeaders.append('Charset', 'UTF-8');
                    /** @type {?} */
                    var newFilterQuery = JSON.parse(JSON.stringify(filterQuery));
                    if (newFilterQuery && newFilterQuery.FilterGroups && newFilterQuery.FilterGroups.length > 0) {
                        newFilterQuery.FilterGroups.forEach(function (filterGroup) {
                            if (filterGroup.Filters && filterGroup.Filters.length > 0) {
                                filterGroup.Filters.forEach(function (filter$$1) {
                                    if (filter$$1.Value) {
                                        filter$$1.Value = encodeURIComponent(filter$$1.Value);
                                    }
                                    if (filter$$1.OtherValue) {
                                        filter$$1.OtherValue = encodeURIComponent(filter$$1.OtherValue);
                                    }
                                });
                            }
                        });
                    }
                    httpHeaders = httpHeaders.append(Headers.PagingHeader, JSON.stringify(newFilterQuery));
                }
                if (headerParameters) {
                    headerParameters.forEach(function (headerParameter) {
                        if (headerParameter) {
                            httpHeaders = httpHeaders.append(headerParameter.header, headerParameter.value);
                        }
                    });
                }
                return httpHeaders;
            };
        return BaseHttpService;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m)
            return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length)
                    o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BaseService = /** @class */ (function (_super) {
        __extends(BaseService, _super);
        function BaseService(httpClient) {
            var _this = _super.call(this, httpClient) || this;
            _this.httpClient = httpClient;
            _this.endpoint = '';
            _this.moduleUrl = '';
            return _this;
        }
        /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.create = /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (resource, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint;
                return this.httpPost(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.update = /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (resource, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint;
                return this.httpPatch(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.delete = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint;
                return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.deleteById = /**
         * @template T
         * @param {?} id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (id, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint;
                return this.httpDelete(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.getList = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint;
                return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} filterQuery
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.getListBy = /**
         * @template T
         * @param {?} filterQuery
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (filterQuery, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint + '/GetListBy';
                return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.get = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, headerParameters) {
                endPointUrl = endPointUrl || this.endpoint;
                return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, null, headerParameters);
            };
        /**
         * @template T
         * @param {?} id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.getById = /**
         * @template T
         * @param {?} id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (id, endPointUrl, headerParameters) {
                endPointUrl = endPointUrl || this.endpoint;
                return this.callHttpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl + '/' + id, null, headerParameters);
            };
        /**
         * @template T
         * @param {?} filterQuery
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.getBy = /**
         * @template T
         * @param {?} filterQuery
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (filterQuery, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint + '/GetBy';
                return this.httpGet(AppConfig.settings.env.apiUrl + this.moduleUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.bulkOperation = /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (resource, endPointUrl, headerParameters) {
                var _this = this;
                endPointUrl = endPointUrl || this.endpoint + '/BulkOperation';
                return this.update(resource, endPointUrl, headerParameters)
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @private
         * @template T
         * @param {?} url
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.callHttpGet = /**
         * @private
         * @template T
         * @param {?} url
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
            function (url, filterQuery, headerParameters) {
                var _this = this;
                /** @type {?} */
                var isFileOPeration = this.isFileOPeration(headerParameters);
                if (isFileOPeration) {
                    return this.httpGetFile(url, this.getHttpHeader(filterQuery, headerParameters))
                        .pipe(operators.map(function (response) {
                        return _this.wrapHttpResponse(response);
                    }));
                }
                else {
                    return this.httpGet(url, this.getHttpHeader(filterQuery, headerParameters))
                        .pipe(operators.map(function (response) {
                        return _this.wrapHttpResponse(response);
                    }));
                }
            };
        /**
         * @private
         * @param {?=} headerParameters
         * @return {?}
         */
        BaseService.prototype.isFileOPeration = /**
         * @private
         * @param {?=} headerParameters
         * @return {?}
         */
            function (headerParameters) {
                if (headerParameters) {
                    /** @type {?} */
                    var fileOPeration = headerParameters.find(function (x) { return x && x.header === Headers.FileOperation; });
                    return fileOPeration ? true : false;
                }
                else {
                    return false;
                }
            };
        /**
         * @private
         * @template T
         * @param {?} response
         * @return {?}
         */
        BaseService.prototype.wrapHttpResponse = /**
         * @private
         * @template T
         * @param {?} response
         * @return {?}
         */
            function (response) {
                /** @type {?} */
                var pagingResult;
                if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
                    pagingResult = ( /** @type {?} */(JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
                }
                /** @type {?} */
                var responseBody = ( /** @type {?} */((response.body || response)));
                /** @type {?} */
                var coreHttpResponse = {
                    serviceResult: responseBody
                };
                if (pagingResult) {
                    coreHttpResponse.pagingResult = pagingResult;
                }
                return coreHttpResponse;
            };
        BaseService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        BaseService.ctorParameters = function () {
            return [
                { type: http.HttpClient }
            ];
        };
        return BaseService;
    }(BaseHttpService));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DataService = /** @class */ (function (_super) {
        __extends(DataService, _super);
        function DataService(httpClient) {
            var _this = _super.call(this, httpClient) || this;
            _this.httpClient = httpClient;
            return _this;
        }
        /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.create = /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (resource, endPointUrl, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpPost(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.update = /**
         * @template T
         * @param {?} resource
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (resource, endPointUrl, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpPatch(AppConfig.settings.env.apiUrl + endPointUrl, resource, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} Id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.delete = /**
         * @template T
         * @param {?} Id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (Id, endPointUrl, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpDelete(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.getList = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.getListBy = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, filterQuery, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?} Id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.get = /**
         * @template T
         * @param {?} Id
         * @param {?=} endPointUrl
         * @param {?=} headerParameters
         * @return {?}
         */
            function (Id, endPointUrl, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl + '/' + Id, this.getHttpHeader(null, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
        DataService.prototype.getBy = /**
         * @template T
         * @param {?=} endPointUrl
         * @param {?=} filterQuery
         * @param {?=} headerParameters
         * @return {?}
         */
            function (endPointUrl, filterQuery, headerParameters) {
                var _this = this;
                if (endPointUrl === void 0) {
                    endPointUrl = '';
                }
                return this.httpGet(AppConfig.settings.env.apiUrl + endPointUrl, this.getHttpHeader(filterQuery, headerParameters))
                    .pipe(operators.map(function (response) {
                    return _this.wrapHttpResponse(response);
                }));
            };
        /**
         * @private
         * @template T
         * @param {?} response
         * @return {?}
         */
        DataService.prototype.wrapHttpResponse = /**
         * @private
         * @template T
         * @param {?} response
         * @return {?}
         */
            function (response) {
                /** @type {?} */
                var pagingResult;
                if (response.headers && response.headers.get(Headers.PagingResponseHeader)) {
                    pagingResult = ( /** @type {?} */(JSON.parse(response.headers.get(Headers.PagingResponseHeader))));
                }
                /** @type {?} */
                var responseBody = ( /** @type {?} */((response.body || response)));
                /** @type {?} */
                var coreHttpResponse = {
                    serviceResult: responseBody
                };
                if (pagingResult) {
                    coreHttpResponse.pagingResult = pagingResult;
                }
                return coreHttpResponse;
            };
        DataService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        DataService.ctorParameters = function () {
            return [
                { type: http.HttpClient }
            ];
        };
        return DataService;
    }(BaseHttpService));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AuthenticationService = /** @class */ (function (_super) {
        __extends(AuthenticationService, _super);
        // @Inject(StorageService) private storageService: StorageService
        function AuthenticationService(httpClient, storageService, router) {
            var _this = _super.call(this, httpClient) || this;
            _this.httpClient = httpClient;
            _this.storageService = storageService;
            _this.router = router;
            _this.chagePasswordEndPoint = 'User/ChangePassword';
            _this.changePasswordWithTokenEndPoint = 'User/ChangePasswordWithToken';
            _this.isSuccess = new rxjs.Subject();
            _this.afterLoginUrl = '/dashboard';
            _this.beforeLoginUrl = '/login';
            _this.jwtHelperService = new angularJwt.JwtHelperService();
            return _this;
        }
        /**
         * @param {?} credentials
         * @param {?=} disableNavigation
         * @return {?}
         */
        AuthenticationService.prototype.login = /**
         * @param {?} credentials
         * @param {?=} disableNavigation
         * @return {?}
         */
            function (credentials, disableNavigation) {
                var _this = this;
                /** @type {?} */
                var headerParameters = [];
                if (disableNavigation) {
                    headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
                }
                /** @type {?} */
                var data = {
                    grant_type: 'password',
                    username: credentials.username,
                    password: credentials.password,
                    client_id: AppConfig.settings.aad.clientId
                };
                return this.create(data, Module.SignIn, headerParameters)
                    .pipe(operators.map(function (response) {
                    _this.handdleTokenResponse(response);
                }));
            };
        /**
         * @param {?} response
         * @return {?}
         */
        AuthenticationService.prototype.handdleTokenResponse = /**
         * @param {?} response
         * @return {?}
         */
            function (response) {
                if (response && response.serviceResult.IsSuccess) {
                    this.storageService.store(Storage.AccessToken, response.serviceResult.Result.access_token.token);
                    this.storageService.store(Storage.RefreshToken, response.serviceResult.Result.refresh_token);
                    /** @type {?} */
                    var decodedToken = this.jwtHelperService.decodeToken(response.serviceResult.Result.access_token.token);
                    /** @type {?} */
                    var expirationDate = this.jwtHelperService.getTokenExpirationDate(response.serviceResult.Result.access_token.token);
                    this.storageService.store(Storage.Expires, expirationDate);
                    /** @type {?} */
                    var userData = {
                        username: decodedToken.sub,
                        employeeId: decodedToken.employee_id,
                        id: decodedToken.id
                    };
                    this.setUserAfterToken(userData);
                }
            };
        /**
         * @param {?=} disableNavigation
         * @param {?=} disableLoading
         * @return {?}
         */
        AuthenticationService.prototype.refreshToken = /**
         * @param {?=} disableNavigation
         * @param {?=} disableLoading
         * @return {?}
         */
            function (disableNavigation, disableLoading) {
                var _this = this;
                /** @type {?} */
                var headerParameters = [];
                /** @type {?} */
                var token = this.storageService.getStored(Storage.RefreshToken);
                if (disableNavigation) {
                    headerParameters.push({ header: Headers.DisableNavigation, value: Headers.DisableNavigation });
                }
                if (disableLoading) {
                    headerParameters.push({ header: Headers.DisableLoading, value: Headers.DisableLoading });
                }
                /** @type {?} */
                var data = {
                    grant_type: 'refresh_token',
                    client_id: AppConfig.settings.aad.clientId,
                    refresh_token: token
                };
                return this.create(data, Module.SignIn, headerParameters)
                    .pipe(operators.tap(function (response) {
                    _this.handdleTokenResponse(response);
                }));
            };
        /**
         * @return {?}
         */
        AuthenticationService.prototype.loggedIn = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var token = this.storageService.getStored(Storage.AccessToken);
                if (token) {
                    /** @type {?} */
                    var isExpired = this.jwtHelperService.isTokenExpired(token);
                    return !isExpired;
                }
                else {
                    this.cleanLocalStorage();
                    return false;
                }
            };
        /**
         * @return {?}
         */
        AuthenticationService.prototype.getTimeoutMiliseconds = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var expires = this.storageService.getStored(Storage.Expires);
                return moment(expires).valueOf() - moment().valueOf() - 120000;
            };
        /**
         * @return {?}
         */
        AuthenticationService.prototype.getAuthToken = /**
         * @return {?}
         */
            function () {
                return this.storageService.getStored(Storage.AccessToken);
            };
        /**
         * @return {?}
         */
        AuthenticationService.prototype.cleanLocalStorage = /**
         * @return {?}
         */
            function () {
                this.storageService.clear(Storage.AccessToken);
                this.storageService.clear(Storage.RefreshToken);
                this.storageService.clear(Storage.Expires);
                this.storageService.clear(Storage.User);
                this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
            };
        /**
         * @param {?} userData
         * @return {?}
         */
        AuthenticationService.prototype.setUserAfterToken = /**
         * @param {?} userData
         * @return {?}
         */
            function (userData) {
                this.storageService.store(Storage.User, userData);
            };
        /**
         * @return {?}
         */
        AuthenticationService.prototype.getUserDataLoggedIn = /**
         * @return {?}
         */
            function () {
                return this.storageService.getStored(Storage.User);
            };
        /**
         * @param {?} formValue
         * @return {?}
         */
        AuthenticationService.prototype.forgotPassword = /**
         * @param {?} formValue
         * @return {?}
         */
            function (formValue) {
                var _this = this;
                /** @type {?} */
                var email = formValue.email;
                /** @type {?} */
                var isLocked = formValue.isLocked;
                this.httpClient.get(AppConfig.settings.env.apiUrl + Module.Auth + 'User/ForgotPassword?email=' + email + '&isLocked=' + isLocked)
                    .subscribe(function (res) {
                    if (res['IsSuccess']) {
                        _this.isSuccess.next(res['IsSuccess']);
                        // token alınacak
                    }
                });
            };
        /**
         * @param {?} formValue
         * @return {?}
         */
        AuthenticationService.prototype.changePassword = /**
         * @param {?} formValue
         * @return {?}
         */
            function (formValue) {
                var _this = this;
                this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.chagePasswordEndPoint, formValue)
                    .subscribe(function (res) {
                    if (res['IsSuccess']) {
                        _this.isSuccess.next(res['IsSuccess']);
                    }
                });
            };
        /**
         * @param {?} formValue
         * @return {?}
         */
        AuthenticationService.prototype.changePasswordWithToken = /**
         * @param {?} formValue
         * @return {?}
         */
            function (formValue) {
                var _this = this;
                this.httpClient.patch(AppConfig.settings.env.apiUrl + Module.Auth + this.changePasswordWithTokenEndPoint, formValue)
                    .subscribe(function (res) {
                    if (res['IsSuccess']) {
                        _this.isSuccess.next(res['IsSuccess']);
                    }
                });
            };
        /**
         * @param {?=} disableNavigation
         * @return {?}
         */
        AuthenticationService.prototype.logout = /**
         * @param {?=} disableNavigation
         * @return {?}
         */
            function (disableNavigation) {
                // disableNavigation it is for session-expired
                this.storageService.clear(Storage.AccessToken);
                this.storageService.clear(Storage.RefreshToken);
                this.storageService.clear(Storage.Expires);
                this.storageService.clear(Storage.User);
                if (!disableNavigation) {
                    // it makes to nagivate signin when cleared in master.app.ts
                    this.storageService.clear(Storage.SystemRefDate, StorageType.SessionStorage);
                    this.router.navigate([this.beforeLoginUrl]);
                }
            };
        AuthenticationService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        AuthenticationService.ctorParameters = function () {
            return [
                { type: http.HttpClient },
                { type: StorageService },
                { type: i5.Router }
            ];
        };
        return AuthenticationService;
    }(BaseService));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var RequestInterceptor = /** @class */ (function () {
        function RequestInterceptor(injector, loadingService, storageService, router, toastr) {
            this.injector = injector;
            this.loadingService = loadingService;
            this.storageService = storageService;
            this.router = router;
            this.toastr = toastr;
            this.isRefreshingToken = false;
            this.tokenSubject = new rxjs.BehaviorSubject(null);
        }
        /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        RequestInterceptor.prototype.intercept = /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                var _this = this;
                this.authenticationService = this.injector.get(AuthenticationService);
                this.loadingService = this.injector.get(LoadingService);
                // it is for disabling general loading
                /** @type {?} */
                var disableGeneralLoading;
                // it is for session-expired to avoid navitaion
                /** @type {?} */
                var disableNavigation;
                // resim gönderme de content type eklemin diye
                /** @type {?} */
                var fileOperation;
                // breadcrumba custom label gönderme
                /** @type {?} */
                var isBreadcrumbCustomLabel;
                /** @type {?} */
                var breadcrumbCustomLabel;
                // controls the header if it exist
                if (request.headers.has(Headers.DisableLoading)) {
                    disableGeneralLoading = true;
                    request = request.clone({ headers: request.headers.delete(Headers.DisableLoading) });
                }
                // controls the header if it exist
                if (request.headers.has(Headers.DisableNavigation)) {
                    disableNavigation = true;
                    request = request.clone({ headers: request.headers.delete(Headers.DisableNavigation) });
                }
                // controls the header if it exist
                if (request.headers.has(Headers.FileOperation)) {
                    fileOperation = true;
                    request = request.clone({ headers: request.headers.delete(Headers.FileOperation) });
                }
                // disabling loading when getting en tr
                if (request.url.indexOf('assets/i18n') !== -1) {
                    disableGeneralLoading = true;
                }
                // controls the header if it exist
                if (request.headers.has(Headers.BreadcrumbCustomLabel)) {
                    this.breadcrumbService = this.injector.get(BreadcrumbService);
                    isBreadcrumbCustomLabel = true;
                    breadcrumbCustomLabel = request.headers.getAll(Headers.BreadcrumbCustomLabel);
                    request = request.clone({ headers: request.headers.delete(Headers.BreadcrumbCustomLabel) });
                }
                // if it is not set true then send request url to loading service
                if (!disableGeneralLoading) {
                    this.loadingService.insertLoadingRequest(request.url);
                }
                if (!request.headers.has(Headers.ContentType) && !fileOperation) {
                    request = request.clone({ headers: request.headers.set(Headers.ContentType, 'application/json') });
                }
                request = request.clone({
                    headers: request.headers.set('Accept', 'application/json')
                });
                /** @type {?} */
                var refDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
                request = request.clone({
                    headers: request.headers.set(Headers.ReferenceDateHeader, refDate || new Date().toISOString())
                });
                return next.handle(this.addTokenToRequest(request, this.authenticationService.getAuthToken()))
                    .pipe(operators.map(function (event) {
                    if (event instanceof http.HttpResponse) {
                        /** @type {?} */
                        var response = ( /** @type {?} */(event));
                        if (response && response.body) {
                            if (response.body.hasOwnProperty('IsSuccess') && !response.body.IsSuccess) {
                                /** @type {?} */
                                var errorMessage = '0001: An Error Occurred';
                                if (response.body.ErrorMessage instanceof Array) {
                                    errorMessage = ( /** @type {?} */(response.body.ErrorMessage));
                                    errorMessage.forEach(function (message) {
                                        _this.toastr.error(message);
                                    });
                                }
                                else {
                                    errorMessage = ( /** @type {?} */(response.body.ErrorMessage));
                                    _this.toastr.error(response.body.ErrorMessage);
                                }
                                return rxjs.throwError(errorMessage);
                            }
                            else if (isBreadcrumbCustomLabel) {
                                _this.handdleResponseForBreadcrumb(response.body, breadcrumbCustomLabel);
                            }
                        }
                    }
                    return event;
                }), operators.catchError(function (err) {
                    if (!disableGeneralLoading) {
                        _this.loadingService.removeLoadingRequest(request.url);
                    }
                    if (err instanceof http.HttpErrorResponse) {
                        switch ((( /** @type {?} */(err))).status) {
                            case 0:
                                _this.showError(err);
                                break;
                            case 401:
                                return _this.handle401Error(request, next, err);
                            case 400:
                                return _this.handle400Error(err, disableNavigation);
                            case 500:
                                _this.showError(err);
                                break;
                        }
                    }
                    return rxjs.throwError(err);
                }), operators.finalize(function () {
                    if (!disableGeneralLoading) {
                        _this.loadingService.removeLoadingRequest(request.url);
                    }
                }));
            };
        /**
         * @private
         * @param {?} response
         * @param {?} breadcrumbCustomLabel
         * @return {?}
         */
        RequestInterceptor.prototype.handdleResponseForBreadcrumb = /**
         * @private
         * @param {?} response
         * @param {?} breadcrumbCustomLabel
         * @return {?}
         */
            function (response, breadcrumbCustomLabel) {
                if (response.IsSuccess && response.Result && this.breadcrumbService) {
                    // came from headers
                    /** @type {?} */
                    var breadcrumbLabels = [];
                    // resposne data
                    /** @type {?} */
                    var data_1;
                    /** @type {?} */
                    var generatedCustomLabel_1 = '';
                    // checkh array or string and convert into array
                    if (breadcrumbCustomLabel) {
                        if (breadcrumbCustomLabel instanceof Array) {
                            if (breadcrumbCustomLabel.indexOf(Headers.BreadcrumbCustomLabel) === -1) {
                                breadcrumbLabels = breadcrumbCustomLabel;
                            }
                        }
                        else if (breadcrumbCustomLabel !== Headers.BreadcrumbCustomLabel) {
                            breadcrumbLabels.push(breadcrumbCustomLabel);
                        }
                    }
                    // check result is array, and convert into single data object
                    if (response.Result instanceof Array) {
                        if (response.Result.length > 0) {
                            data_1 = response.Result[0];
                        }
                    }
                    else {
                        data_1 = response.Result;
                    }
                    if (data_1) {
                        if (breadcrumbLabels && breadcrumbLabels.length > 0) {
                            // use header value
                            breadcrumbLabels.forEach(function (element) {
                                if (data_1[element]) {
                                    generatedCustomLabel_1 += data_1[element] + ' ';
                                }
                            });
                        }
                        else if (data_1.hasOwnProperty('Definition') && data_1.Definition) {
                            // use default label
                            generatedCustomLabel_1 = data_1.Definition;
                        }
                        else if (data_1.hasOwnProperty('Description') && data_1.Description) {
                            // use default label
                            generatedCustomLabel_1 = data_1.Description;
                        }
                    }
                    /** @type {?} */
                    var splitedUrl = this.router.url.split('/');
                    if (splitedUrl && generatedCustomLabel_1 !== '') {
                        this.breadcrumbService.addCustomLabel(splitedUrl[splitedUrl.length - 1], this.router.url, generatedCustomLabel_1);
                    }
                }
            };
        /**
         * @private
         * @param {?} request
         * @param {?} token
         * @return {?}
         */
        RequestInterceptor.prototype.addTokenToRequest = /**
         * @private
         * @param {?} request
         * @param {?} token
         * @return {?}
         */
            function (request, token) {
                // it is for not adding token to header when login or refresh token
                if (token && request.url.lastIndexOf('Token') === -1) {
                    return request.clone({ setHeaders: { Authorization: "Bearer " + token } });
                }
                else {
                    return request;
                }
            };
        /**
         * @param {?} error
         * @param {?=} disableNavigation
         * @return {?}
         */
        RequestInterceptor.prototype.handle400Error = /**
         * @param {?} error
         * @param {?=} disableNavigation
         * @return {?}
         */
            function (error, disableNavigation) {
                this.showError(error);
                this.authenticationService.logout(disableNavigation);
                return rxjs.throwError(error);
            };
        /**
         * @private
         * @param {?} request
         * @param {?} next
         * @param {?} error
         * @return {?}
         */
        RequestInterceptor.prototype.handle401Error = /**
         * @private
         * @param {?} request
         * @param {?} next
         * @param {?} error
         * @return {?}
         */
            function (request, next, error) {
                var _this = this;
                /** @type {?} */
                var authToken = this.authenticationService.getAuthToken();
                // TOken var mı kontrolü yoksa login yapıyordur
                if (authToken) {
                    if (!this.isRefreshingToken) {
                        this.isRefreshingToken = true;
                        // Reset here so that the following requests wait until the token
                        // comes back from the refreshToken call.
                        this.tokenSubject.next(null);
                        return this.authenticationService.refreshToken()
                            .pipe(operators.switchMap(function (response) {
                            if (response) {
                                _this.authenticationService.handdleTokenResponse(response);
                                /** @type {?} */
                                var token = _this.authenticationService.getAuthToken();
                                _this.tokenSubject.next(token);
                                return next.handle(_this.addTokenToRequest(request, token));
                            }
                            else {
                                return rxjs.throwError(error);
                            }
                        }), operators.catchError(function (err) {
                            _this.showError(err);
                            _this.authenticationService.logout();
                            return rxjs.throwError(err);
                        }), operators.finalize(function () {
                            _this.isRefreshingToken = false;
                        }));
                    }
                    else {
                        this.isRefreshingToken = false;
                        return this.tokenSubject
                            .pipe(operators.filter(function (token) { return token != null; }), operators.take(1), operators.switchMap(function (token) {
                            if (token) {
                                return next.handle(_this.addTokenToRequest(request, token));
                            }
                            else {
                                _this.authenticationService.logout();
                                return rxjs.throwError(error);
                            }
                        }));
                    }
                }
                else {
                    this.showError(error);
                    this.authenticationService.logout();
                    return rxjs.throwError(error);
                }
            };
        /**
         * @private
         * @param {?} error
         * @return {?}
         */
        RequestInterceptor.prototype.showError = /**
         * @private
         * @param {?} error
         * @return {?}
         */
            function (error) {
                if (error && error.error && error.error.ErrorMessage) {
                    this.toastr.error(error.error.ErrorMessage);
                }
                else if (error && error.message) {
                    this.toastr.error(error.message);
                }
            };
        RequestInterceptor.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        RequestInterceptor.ctorParameters = function () {
            return [
                { type: i0.Injector },
                { type: LoadingService },
                { type: StorageService },
                { type: i5.Router },
                { type: ToastrUtilsService }
            ];
        };
        return RequestInterceptor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CommandCellComponent = /** @class */ (function () {
        function CommandCellComponent() {
            this.showRowDeleteButton = true;
            this.showRowEditButton = true;
        }
        /**
         * @param {?} params
         * @return {?}
         */
        CommandCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this.rowId = this.params.node.id;
                this.editable = this.params.context.componentParent.gridOptions.editable;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                /** @type {?} */
                var buttonSettings = this.params.context.componentParent.gridOptions.buttonSettings;
                if (buttonSettings) {
                    if (buttonSettings.showRowDeleteButton !== undefined) {
                        this.showRowDeleteButton = buttonSettings.showRowDeleteButton;
                    }
                    if (buttonSettings.showEditButton !== undefined) {
                        this.showRowEditButton = buttonSettings.showEditButton;
                    }
                }
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.refresh = /**
         * @return {?}
         */
            function () {
                return false;
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.showUndoButton = /**
         * @return {?}
         */
            function () {
                return this.params &&
                    this.params.data &&
                    this.params.data.OperationType !== OperationType.Created &&
                    this.editable &&
                    (!this.formGroup.pristine || this.params.data.OperationType === OperationType.Updated);
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.removeRow = /**
         * @return {?}
         */
            function () {
                this.params.context.componentParent.removeRow(this.params, this.rowId, this.key);
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.deleteRecord = /**
         * @return {?}
         */
            function () {
                this.params.context.componentParent.deleteRecord(this.params, this.rowId, this.key);
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.editRecord = /**
         * @return {?}
         */
            function () {
                this.params.context.componentParent.editRecord(this.params, this.rowId, this.key);
            };
        /**
         * @return {?}
         */
        CommandCellComponent.prototype.undoRecord = /**
         * @return {?}
         */
            function () {
                this.params.context.componentParent.undoRecord(this.params, this.rowId, this.key);
            };
        CommandCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-command-cell',
                        template: "\n    <div *ngIf=\"formGroup\" style=\"margin-top: 4px;display: flex;\" >\n      <span  class=\"command-cell-item\">\n      <button *ngIf=\"showUndoButton()\"  type=\"button\" style=\"height: 20px\" (click)=\"undoRecord()\" class=\"pull-right btn-grid-inline btn-grid-edit\">\n      <core-icon icon=\"undo\"></core-icon>\n      </button>\n\n\t\t\t\t<button *ngIf=\"!editable && showRowDeleteButton\"  type=\"button\" style=\"height: 20px\" (click)=\"deleteRecord()\" class=\"pull-right btn-grid-inline btn-grid-delete\">\n          <core-icon icon=\"trash-alt\"></core-icon>\n\t\t\t\t</button>\n\n\t\t\t\t<button *ngIf=\"editable && showRowDeleteButton\"  type=\"button\" style=\"height: 20px\" (click)=\"removeRow()\" class=\"pull-right btn-grid-inline btn-grid-delete\">\n          <core-icon icon=\"trash-alt\"></core-icon>\n\t\t\t\t</button>\n\n\t\t\t\t<button *ngIf=\"!editable && showRowEditButton\"  type=\"button\" style=\"height: 20px\" (click)=\"editRecord()\" class=\"pull-right btn-grid-inline btn-grid-edit\">\n        <core-icon icon=\"edit\"></core-icon>\n        </button>\n      </span>\n    </div>\n  ",
                        styles: [".command-cell-item{margin-left:auto;margin-right:auto}.btn-grid-inline{color:#166dba!important;background:0 0!important;border:none!important;cursor:pointer;font-size:16px}.btn-grid-inline:focus{outline:0}.btn-grid-delete:hover{color:#e30a3a!important}.btn-grid-edit:hover{color:#17a2b8!important}"]
                    }] }
        ];
        return CommandCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TextFilterCellComponent = /** @class */ (function () {
        function TextFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.modelChanged = new rxjs.Subject();
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                textFilterValue: TextComparison.Contains
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        TextFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                // this.dataSource = ComparisonList.createKeyLabelArray(ComparisonType.Text);
                this.modelChangedSubscription = this.modelChanged.pipe(operators.debounceTime(1000)).subscribe(function (model) {
                    _this.currentValue = model;
                    if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                        params.serviceAccess.callUndoConfirmAlert()
                            .subscribe(function (result) {
                            if (result.value) {
                                if (params.column.colDef.columnType === CoreGridColumnType.TextSelect) {
                                    if (params.serviceAccess) {
                                        params.serviceAccess.textSelectFindValuesOnFilter(params.field, params.column.colDef.cellEditorParams.settings, _this.buildModel());
                                    }
                                }
                                else {
                                    if (params.serviceAccess && params.serviceAccess.filter) {
                                        params.serviceAccess.applyChangesToFilter(params.field, _this.buildModel());
                                    }
                                    _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                                    _this.prevValue = _this.currentValue;
                                }
                            }
                            else {
                                _this.currentValue = _this.prevValue || null;
                            }
                        });
                    }
                });
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        TextFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                this.modelChanged.next(newValue);
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        TextFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // *****will be done when needed*****
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        TextFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id) {
                    this.valueChanged(this.currentValue);
                }
            };
        /**
         * @return {?}
         */
        TextFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var value;
                /** @type {?} */
                var type = this.formGroup.get('textFilterValue').value;
                if (this.currentValue) {
                    value = this.currentValue;
                }
                else if (type === TextComparison.IsNullOrWhiteSpace ||
                    type === TextComparison.IsNotNullNorWhiteSpace ||
                    type === TextComparison.IsNull ||
                    type === TextComparison.IsNotNull ||
                    type === TextComparison.IsNotEmpty ||
                    type === TextComparison.IsEmpty) {
                    value = ' ';
                }
                return {
                    filterType: 'string',
                    type: type.toString(),
                    filter: value
                };
            };
        /**
         * @return {?}
         */
        TextFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Text).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Text);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        TextFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.modelChangedSubscription) {
                    this.modelChangedSubscription.unsubscribe();
                }
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        TextFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-text-filter-cell',
                        template: "\n    <div class=\"input-group\">\n      <layout-form-text-input style=\"width: 100%;\"\n          [(ngModel)]=\"currentValue\"\n          [clearable] = \"true\"\n          placeholder=\"{{filterKey|translate}}\"\n          (ngModelChange)=\"valueChanged($event)\">\n      </layout-form-text-input>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"textFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        TextFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return TextFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SelectFilterCellComponent = /** @class */ (function () {
        function SelectFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                filterValue: null
            });
            this.selectorFormGroup = this.fb.group({
                selectFilterValue: DefaultComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        SelectFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                if (this.params.column.getColDef().cellEditorParams) {
                    this.settings = this.params.column.getColDef().cellEditorParams.settings;
                    this.getListData();
                }
                this.dataSubscription = this.params.column.getColDef().cellEditorParams.data.subscribe(function (x) {
                    _this.getListData();
                });
            };
        /**
         * @return {?}
         */
        SelectFilterCellComponent.prototype.getListData = /**
         * @return {?}
         */
            function () {
                if (this.params.column.getColDef().cellEditorParams.dataSource) {
                    this.listData = this.params.column['colDef'].cellEditorParams.dataSource;
                    // this.clearSubscription();
                }
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        SelectFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                var _this = this;
                this.currentValue = newValue && newValue[this.settings.valueField] ? newValue[this.settings.valueField] : null;
                if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
                    this.params.serviceAccess.callUndoConfirmAlert()
                        .subscribe(function (result) {
                        if (result.value) {
                            if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                                _this.params.serviceAccess.applyChangesToFilter(_this.settings && (( /** @type {?} */(_this.settings))).sameFieldWith ?
                                    (( /** @type {?} */(_this.settings))).sameFieldWith : _this.params.field, _this.buildModel());
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            _this.prevValue = _this.currentValue;
                        }
                        else {
                            _this.currentValue = null;
                            _this.formGroup.patchValue({
                                filterValue: _this.prevValue || (_this.prevValue && parseInt(_this.prevValue, 10) === 0) ? _this.prevValue : null
                            });
                        }
                    });
                }
                else {
                    this.prevValue = this.currentValue;
                }
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        SelectFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        SelectFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && (data.Id || data.Id === 0) && this.currentValue) {
                    /** @type {?} */
                    var newValue = {};
                    newValue[this.settings.valueField] = this.currentValue;
                    this.valueChanged(newValue);
                }
            };
        /**
         * @return {?}
         */
        SelectFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'text',
                    type: this.selectorFormGroup.get('selectFilterValue').value,
                    // can be send in here
                    filter: this.currentValue
                };
            };
        /**
         * @return {?}
         */
        SelectFilterCellComponent.prototype.clearSubscription = /**
         * @return {?}
         */
            function () {
                if (this.dataSubscription) {
                    this.dataSubscription.unsubscribe();
                }
            };
        /**
         * @return {?}
         */
        SelectFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Default).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Default);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        SelectFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.clearSubscription();
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        SelectFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-select-filter-cell',
                        template: "\n    <div class=\"input-group\">\n      <div *ngIf=\"settings\" [formGroup]=\"formGroup\" style=\"width:100%;    padding-top: 4px;\">\n        <layout-static-selector\n          #selector\n          formControlName=\"filterValue\"\n          (changed)=\"valueChanged($event)\"\n          [dataSource]=\"listData\"\n          placeholder=\"{{filterKey|translate}}\"\n          [valueField]=\"this.settings.valueField\"\n          [labelField]=\"this.settings.labelField\"\n        >\n        </layout-static-selector>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"selectFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n\n  ",
                        styles: [":host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container .ng-placeholder{font-weight:400;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:#6c7591}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                    }] }
        ];
        /** @nocollapse */
        SelectFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return SelectFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DateFilterCellComponent = /** @class */ (function () {
        function DateFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.selectionMode = DateSelectionMode.Single;
            this.settings = new DateSettings();
            this.formGroup = this.fb.group({
                dateFilterValue: null
            });
            this.selectorFormGroup = this.fb.group({
                selectorDateFilterValue: DateComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        DateFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                if (this.params.column) {
                    /** @type {?} */
                    var column = this.params.column;
                    if (column.colDef && column.colDef.cellEditorParams && column.colDef.cellEditorParams.settings) {
                        this.settings = Object.assign(( /** @type {?} */({})), column.colDef.cellEditorParams.settings);
                    }
                }
                this.settings.hideCalendarButton = true;
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        DateFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                var _this = this;
                if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
                    this.params.serviceAccess.callUndoConfirmAlert()
                        .subscribe(function (result) {
                        if (result.value) {
                            /** @type {?} */
                            var selectedDateComparison = _this.selectorFormGroup.get('selectorDateFilterValue').value;
                            if (selectedDateComparison === DateComparison.Between) {
                                _this.selectionMode = DateSelectionMode.Range;
                            }
                            else {
                                _this.selectionMode = DateSelectionMode.Single;
                            }
                            if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                                _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel(newValue));
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel(newValue) });
                        }
                        else {
                            _this.formGroup.patchValue({
                                dateFilterValue: null
                            });
                        }
                    });
                }
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        DateFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // *****will be done when needed*****
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        DateFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id) {
                    this.valueChanged(this.formGroup.get('dateFilterValue').value);
                }
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        DateFilterCellComponent.prototype.buildModel = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                /** @type {?} */
                var model = {
                    filterType: 'string',
                    type: this.selectorFormGroup.get('selectorDateFilterValue').value,
                    filter: null,
                    other: null
                };
                if (newValue instanceof Array) {
                    model.filter = newValue[0];
                    model.other = newValue[1];
                }
                else {
                    model.filter = newValue;
                }
                return model;
            };
        /**
         * @return {?}
         */
        DateFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Date).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Date);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        DateFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        DateFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-date-filter-cell',
                        template: "\n\n  <div class=\"input-group\">\n    <div *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\"\n      class=\"input-group\" style=\"width:100%;\">\n      <layout-form-datepicker-input style=\"width:100%;\"\n      formControlName=\"dateFilterValue\"\n      [settings]= \"settings\"\n      [selectionMode]=\"selectionMode\"\n      placeholder=\"{{filterKey|translate}}\"\n      (dateChanges)= \"valueChanged($event)\"></layout-form-datepicker-input>\n    </div>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"selectorDateFilterValue\"\n        (changed)=\"changed($event)\"\n        [dataSource]=\"dataSource\"\n        placeholder=\"{{placeholder |translate}} {{filterKey|translate}}\"\n        [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n\n  ",
                        styles: [":host /deep/ .custom-ng-select{min-width:auto!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ui-calendar .ui-inputtext{margin-top:6px}"]
                    }] }
        ];
        /** @nocollapse */
        DateFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return DateFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NumericFilterCellComponent = /** @class */ (function () {
        function NumericFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.modelChanged = new rxjs.Subject();
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                numberFilterValue: NumberComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        NumericFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.field = params.field;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                if (params.column
                    && params.column.colDef
                    && params.column.colDef.cellEditorParams
                    && params.column.colDef.cellEditorParams.settings
                    && params.column.colDef.cellEditorParams.settings.fields
                    && params.column.colDef.cellEditorParams.settings.fields.length > 0) {
                    this.field = _.head(params.column.colDef.cellEditorParams.settings.fields);
                }
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                if (this.modelChangedSubscription) {
                    this.modelChangedSubscription.unsubscribe();
                }
                this.modelChangedSubscription = this.modelChanged.pipe(operators.debounceTime(1000), operators.distinctUntilChanged(), // https://rxjs-dev.firebaseapp.com/api/operators/distinctUntilChanged
                operators.flatMap(function (filterValueResult) { return rxjs.from(params.serviceAccess.callUndoConfirmAlert()); }, function (filterValueResult, confirmrResult) {
                    return { filterValueResult: filterValueResult, confirmrResult: confirmrResult };
                }))
                    .subscribe(function (result) {
                    if (result.confirmrResult.value) {
                        _this.currentValue = result.filterValueResult;
                        if (params.serviceAccess && params.serviceAccess.filter) {
                            params.serviceAccess.applyChangesToFilter(_this.field, _this.buildModel());
                        }
                        _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                        _this.prevValue = _this.currentValue;
                    }
                    else {
                        _this.canceledValue = result.filterValueResult;
                        _this.currentValue = _this.prevValue || _this.prevValue === 0 ? _this.prevValue : null;
                    }
                });
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        NumericFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                this.modelChanged.next(newValue);
                if (this.canceledValue === newValue) {
                    this.onFilter();
                }
            };
        // fastest solution copy paste from top
        // fastest solution copy paste from top
        /**
         * @return {?}
         */
        NumericFilterCellComponent.prototype.onFilter =
            // fastest solution copy paste from top
            /**
             * @return {?}
             */
            function () {
                var _this = this;
                if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
                    this.params.serviceAccess.callUndoConfirmAlert()
                        .subscribe(function (result) {
                        if (result.value) {
                            if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                                _this.params.serviceAccess.applyChangesToFilter(_this.field, _this.buildModel());
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            _this.prevValue = _this.currentValue;
                        }
                        else {
                            _this.canceledValue = _this.currentValue;
                            _this.currentValue = _this.prevValue || _this.prevValue === 0 ? _this.prevValue : null;
                        }
                    });
                }
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        NumericFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // *****will be done when needed*****
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        NumericFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id && this.currentValue) {
                    this.valueChanged(this.currentValue);
                }
            };
        /**
         * @return {?}
         */
        NumericFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'number',
                    type: this.formGroup.get('numberFilterValue').value,
                    // can be send in here
                    filter: this.currentValue || (this.currentValue === 0) ?
                        (this.params.customFilterFunction ? this.params.customFilterFunction(this.currentValue.toString()) : this.currentValue.toString()) : null
                };
            };
        /**
         * @return {?}
         */
        NumericFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        NumericFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.modelChangedSubscription) {
                    this.modelChangedSubscription.unsubscribe();
                }
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        NumericFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-numeric-filter-cell',
                        template: "\n  <div class=\"input-group\">\n    <layout-form-number-input style=\"width: 100%;\"\n        [(ngModel)]=\"currentValue\"\n        [clearable] = \"true\"\n        placeholder=\"{{filterKey|translate}}\"\n        (ngModelChange)=\"valueChanged($event)\">\n    </layout-form-number-input>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"numberFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        NumericFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return NumericFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BooleanFilterCellComponent = /** @class */ (function () {
        function BooleanFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.listData = [];
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                booleanFilterValue: DefaultComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.listData.push({ Id: 'true', Definition: 'True' });
                this.listData.push({ Id: 'false', Definition: 'False' });
            };
        Object.defineProperty(BooleanFilterCellComponent.prototype, "model", {
            get: /**
             * @return {?}
             */ function () {
                return this._model;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._model = val;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} newValue
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                var _this = this;
                if (newValue) {
                    this.currentValue = newValue.Id;
                }
                else {
                    this.currentValue = null;
                }
                if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
                    this.params.serviceAccess.callUndoConfirmAlert()
                        .subscribe(function (result) {
                        if (result.value) {
                            if (_this.params.serviceAccess.filter) {
                                _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel());
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            _this.prevValue = _this.model;
                        }
                        else {
                            _this.model = _this.prevValue || null;
                            _this.currentValue = _this.model;
                        }
                    });
                }
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                if (!parentModel) {
                    this.currentValue = null;
                }
                else {
                    this.currentValue = parentModel.filter;
                }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id) {
                    this.valueChanged({ Id: this.currentValue });
                }
            };
        /**
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'text',
                    type: this.formGroup.get('booleanFilterValue').value,
                    filter: this.currentValue === null || this.currentValue === undefined ? null : this.currentValue.toString()
                };
            };
        /**
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        BooleanFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        BooleanFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-boolean-filter-cell',
                        template: "\n    <div class=\"input-group\">\n      <div style=\"width:100%;\">\n      <ng-select class=\"custom-ng-select\"\n      [items]=\"listData\"\n      [id]=\"key\"\n      bindLabel=\"Definition\"\n      [(ngModel)]=\"model\"\n      placeholder=\"{{filterKey|translate}}\"\n      (change)=\"valueChanged($event)\"\n      bindValue=\"Id\" appendTo=\"body\">\n      </ng-select>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n       <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n          <layout-static-selector formControlName=\"booleanFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n  ",
                        styles: [".custom-ng-select{height:30px!important;margin-top:4px}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important;padding-top:2px}:host /deep/ .ng-select-container .ng-has-value{padding-top:8px}:host /deep/ .custom-ng-select .ng-select-container.ng-has-value{padding-top:0}:host /deep/ .custom-ng-select.ng-touched .ng-select-container{padding-top:0}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:-1px}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                    }] }
        ];
        /** @nocollapse */
        BooleanFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return BooleanFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CurrencyFilterCellComponent = /** @class */ (function () {
        function CurrencyFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.modelChanged = new rxjs.Subject();
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                numberFilterValue: NumberComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                this.modelChangedSubscription = this.modelChanged.pipe(operators.debounceTime(1000)).subscribe(function (model) {
                    _this.currentValue = model;
                    if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                        params.serviceAccess.callUndoConfirmAlert()
                            .subscribe(function (result) {
                            if (result.value) {
                                if (params.serviceAccess.filter) {
                                    params.serviceAccess.applyChangesToFilter(params.field, _this.buildModel());
                                }
                                _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            }
                            else {
                                _this.currentValue = null;
                            }
                        });
                    }
                });
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                this.modelChanged.next(newValue);
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // *****will be done when needed*****
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id && this.currentValue) {
                    this.valueChanged(this.currentValue);
                }
            };
        /**
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'text',
                    type: this.formGroup.get('numberFilterValue').value,
                    // can be send in here
                    filter: this.currentValue
                };
            };
        /**
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        CurrencyFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.modelChangedSubscription) {
                    this.modelChangedSubscription.unsubscribe();
                }
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        CurrencyFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-currency-filter-cell',
                        template: "\n  <div class=\"input-group\">\n    <layout-form-currency-input style=\"width: 100%;\"\n        [(ngModel)]=\"currentValue\"\n        clearable = \"true\"\n        placeholder=\"{{filterKey|translate}}\"\n        (ngModelChange)=\"valueChanged($event)\">\n    </layout-form-currency-input>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"numberFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        CurrencyFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return CurrencyFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LovFilterCellComponent = /** @class */ (function () {
        function LovFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                filterValue: null
            });
            this.selectorFormGroup = this.fb.group({
                selectFilterValue: DefaultComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        LovFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.hideFilter = this.params.hideFilter;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                if (this.params.column.getColDef().cellEditorParams) {
                    if (this.params.column.getColDef().cellEditorParams.settings) {
                        this.settings = this.params.column.getColDef().cellEditorParams.settings;
                    }
                    else {
                        console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
                    }
                }
                else {
                    console.error(this.params.column.getColDef().field + 'lov Setting is not defined');
                }
                if (this.params.column.getColDef().cellEditorParams) {
                    if (this.settings) {
                        this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField);
                    }
                }
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        LovFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                var _this = this;
                this.currentValue = newValue && newValue.id ? newValue.id : null;
                if (this.params.serviceAccess && this.params.serviceAccess.callUndoConfirmAlert) {
                    this.params.serviceAccess.callUndoConfirmAlert()
                        .subscribe(function (result) {
                        if (result.value) {
                            if (_this.params.serviceAccess && _this.params.serviceAccess.filter) {
                                _this.params.serviceAccess.applyChangesToFilter(_this.params.field, _this.buildModel());
                            }
                            _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                            _this.prevValue = _this.currentValue;
                        }
                        else {
                            _this.currentValue = null;
                            _this.formGroup.patchValue({
                                filterValue: _this.prevValue || (_this.prevValue && parseInt(_this.prevValue, 10) === 0) ? _this.prevValue : null
                            });
                        }
                    });
                }
                else {
                    this.prevValue = this.currentValue;
                }
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        LovFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        LovFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && (data.Id || data.Id === 0) && this.currentValue) {
                    this.valueChanged(this.currentValue);
                }
            };
        /**
         * @return {?}
         */
        LovFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'text',
                    type: this.selectorFormGroup.get('selectFilterValue').value,
                    // can be send in here
                    filter: this.currentValue
                };
            };
        /**
         * @return {?}
         */
        LovFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Default).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Default);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        LovFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        LovFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-lov-filter-cell',
                        template: "\n    <div [hidden]=\"hideFilter\" class=\"input-group\">\n      <div [formGroup]=\"formGroup\" style=\"width: calc(100%);\">\n        <layout-list-of-values #listOfValue\n        [id]=\"key\"\n        formControlName=\"filterValue\"\n        placeholder=\"{{filterKey|translate}}\"\n        (lovValueChanged)=\"valueChanged($event)\"></layout-list-of-values>\n      </div>\n\n      <div class=\"grid-filter-icon\">\n        <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n      </div>\n\n      <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n        <div *ngIf=\"selectorFormGroup\" [formGroup]=\"selectorFormGroup\" style=\"width:100%;\">\n          <layout-static-selector\n          formControlName=\"selectFilterValue\"\n          (changed)=\"changed($event)\"\n          [dataSource]=\"dataSource\"\n          placeholder=\"{{filterKey|translate}}\"\n          [clearable]=\"false\"\n          [searchable]=\"false\">\n          </layout-static-selector>\n        </div>\n      </p-overlayPanel>\n    </div>\n\n  ",
                        styles: [":host /deep/ .form-lov .btn-info{background-color:#175169!important;border-color:#175169!important}:host /deep/ .ng-select .ng-select-container .ng-value-container{font-weight:400}"]
                    }] }
        ];
        /** @nocollapse */
        LovFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        LovFilterCellComponent.propDecorators = {
            listOfValue: [{ type: i0.ViewChild, args: ['listOfValue',] }]
        };
        return LovFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskFilterCellComponent = /** @class */ (function () {
        function MaskFilterCellComponent(fb, translate) {
            this.fb = fb;
            this.translate = translate;
            this.modelChanged = new rxjs.Subject();
            this.dataSource = [];
            this.filterKey = 'GridFilter';
            this.formGroup = this.fb.group({
                numberFilterValue: NumberComparison.EqualTo
            });
        }
        /**
         * @param {?} params
         * @return {?}
         */
        MaskFilterCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.placeholder = params.column.colDef.headerName || params.column.colDef.field;
                this.initiateComparisonList();
                this.translateSubscription = this.translate.onLangChange
                    .subscribe(function (event) {
                    if (event) {
                        _this.initiateComparisonList();
                    }
                });
                if (this.params.column.getColDef().cellEditorParams) {
                    if (this.params.column.getColDef().cellEditorParams.settings) {
                        this.settings = this.params.column.getColDef().cellEditorParams.settings;
                        this.maskType = this.settings.maskType;
                        this.maxLength = this.settings.maxLength;
                    }
                }
                this.modelChangedSubscription = this.modelChanged.pipe(operators.debounceTime(1000)).subscribe(function (model) {
                    _this.currentValue = model;
                    if (params.serviceAccess && params.serviceAccess.callUndoConfirmAlert) {
                        params.serviceAccess.callUndoConfirmAlert()
                            .subscribe(function (result) {
                            if (result.value) {
                                if (params.serviceAccess && params.serviceAccess.filter) {
                                    params.serviceAccess.applyChangesToFilter(params.field, _this.buildModel());
                                }
                                _this.params.onFloatingFilterChanged({ model: _this.buildModel() });
                                _this.prevValue = _this.currentValue;
                            }
                            else {
                                _this.currentValue = _this.prevValue || '';
                            }
                        });
                    }
                });
            };
        /**
         * @param {?} newValue
         * @return {?}
         */
        MaskFilterCellComponent.prototype.valueChanged = /**
         * @param {?} newValue
         * @return {?}
         */
            function (newValue) {
                this.modelChanged.next(newValue);
            };
        /**
         * @param {?} parentModel
         * @return {?}
         */
        MaskFilterCellComponent.prototype.onParentModelChanged = /**
         * @param {?} parentModel
         * @return {?}
         */
            function (parentModel) {
                // *****will be done when needed*****
                // if (!parentModel) {
                //   this.currentValue = null;
                // } else {
                //   this.currentValue = parentModel.filter;
                // }
            };
        /**
         * @param {?} data
         * @return {?}
         */
        MaskFilterCellComponent.prototype.changed = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data && data.Id && this.currentValue) {
                    this.valueChanged(this.currentValue);
                }
            };
        /**
         * @return {?}
         */
        MaskFilterCellComponent.prototype.buildModel = /**
         * @return {?}
         */
            function () {
                return {
                    filterType: 'text',
                    type: this.formGroup.get('numberFilterValue').value,
                    // can be send in here
                    filter: this.currentValue
                };
            };
        /**
         * @return {?}
         */
        MaskFilterCellComponent.prototype.initiateComparisonList = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var keyList = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number).map(function (x) { return x.Definition; });
                /** @type {?} */
                var dataSource = exports.ComparisonList.createKeyLabelArray(ComparisonType.Number);
                this.translate.get(keyList)
                    .subscribe(function (result) {
                    /** @type {?} */
                    var counter = 0;
                    keyList.forEach(function (key) {
                        /** @type {?} */
                        var translatedValue = result[key];
                        dataSource[counter].Definition = translatedValue;
                        counter++;
                    });
                    _this.dataSource = dataSource;
                });
            };
        /**
         * @return {?}
         */
        MaskFilterCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.modelChangedSubscription) {
                    this.modelChangedSubscription.unsubscribe();
                }
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
            };
        MaskFilterCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-mask-filter-cell',
                        template: "\n  <div class=\"input-group\">\n  <layout-form-mask-input style=\"width: 100%;\"\n    [maskType]=\"maskType\"\n    [(ngModel)]=\"currentValue\"\n    [clearable] = \"true\"\n    placeholder=\"{{filterKey|translate}}\"\n    (ngModelChange)=\"valueChanged($event)\">\n  </layout-form-mask-input>\n\n    <div class=\"grid-filter-icon\">\n      <core-icon #actualTarget icon=\"filter\" (click)=\"op.toggle($event)\"></core-icon>\n    </div>\n\n    <p-overlayPanel #op appendTo=\"body\" dismissable=\"false\">\n      <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" style=\"width:100%;\">\n        <layout-static-selector formControlName=\"numberFilterValue\" (changed)=\"changed($event)\" [dataSource]=\"dataSource\" [clearable]=\"false\" [searchable]=\"false\">\n        </layout-static-selector>\n      </div>\n    </p-overlayPanel>\n  </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        MaskFilterCellComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        return MaskFilterCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SelectCellComponent = /** @class */ (function () {
        function SelectCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        SelectCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value ? this.params.value : null;
                this.rowId = this.params.node.id;
                this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(function (x) {
                    _this.getListData();
                });
            };
        /**
         * @param {?} params
         * @return {?}
         */
        SelectCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.params
                    && this.params.colDef
                    && this.params.colDef.cellEditorParams
                    && this.params.colDef.cellEditorParams.settings
                    && this.params.colDef.cellEditorParams.settings.sameFieldWith) {
                    // this.value = this.params.node.data[this.params.colDef.cellEditorParams.settings.sameFieldWith];
                    /** @type {?} */
                    var fields = this.params.colDef.cellEditorParams.settings.sameFieldWith.split('.');
                    /** @type {?} */
                    var data_1 = Object.assign({}, this.params.node.data);
                    fields.forEach(function (field) {
                        data_1 = data_1[field] || null;
                    });
                    this.value = data_1;
                }
                // this.params.node.data["PagePanel"]["PageId"]
                // 58
                // this.params.node.data["PagePanel.PageId"]
                if (this.params.colDef.cellEditorParams) {
                    this.getListData();
                }
                return true;
            };
        /**
         * @return {?}
         */
        SelectCellComponent.prototype.getListData = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.params.colDef.cellEditorParams.dataSource) {
                    this.listData = this.params.colDef.cellEditorParams.dataSource;
                    // this.clearSubscription();
                    /** @type {?} */
                    var data_2 = this.listData.find(function (x) { return x[_this.params.colDef.cellEditorParams.settings.valueField] === _this.value; });
                    if (data_2) {
                        this.definition = '';
                        if (this.params.colDef.cellEditorParams.settings.labelField instanceof Array) {
                            /** @type {?} */
                            var labels = ( /** @type {?} */(this.params.colDef.cellEditorParams.settings.labelField));
                            labels.forEach(function (columnName) {
                                _this.definition = _this.definition + ' ' + data_2[columnName];
                            });
                        }
                        else if (this.params.colDef.cellEditorParams.settings.labelField) {
                            this.definition = data_2[this.params.colDef.cellEditorParams.settings.labelField];
                        }
                    }
                    if (this.definition) {
                        if (this.formGroup) {
                            this.formGroup.at(this.key).patchValue(this.definition);
                        }
                    }
                }
            };
        /**
         * @return {?}
         */
        SelectCellComponent.prototype.clearSubscription = /**
         * @return {?}
         */
            function () {
                if (this.dataSubscription) {
                    this.dataSubscription.unsubscribe();
                }
            };
        /**
         * @return {?}
         */
        SelectCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.clearSubscription();
            };
        SelectCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-select-cell',
                        template: "\n  <div *ngIf=\"definition\" class=\"grid-none-editable-select-cell user-select-text text-ellipsis\"  title=\"{{definition}}\">\n    {{ definition }}\n  </div>\n  ",
                        styles: ["\n      .grid-none-editable-select-cell {\n        padding-top: 5px;\n        white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n      }\n    "]
                    }] }
        ];
        return SelectCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LabelCellComponent = /** @class */ (function () {
        function LabelCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        LabelCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this._value = this.value;
                this.rowId = this.params.node.id;
            };
        /**
         * @param {?} params
         * @return {?}
         */
        LabelCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.value = '';
                        if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                            /** @type {?} */
                            var labels = ( /** @type {?} */(this.params.colDef.cellEditorParams.settings.fields));
                            labels.forEach(function (columnName) {
                                _this.value = _this.value + ' ' + _this.params.node.data[columnName];
                            });
                        }
                    }
                }
                this._value = this.value;
                if (this.params.colDef.cellEditorParams.customDisplayFunction) {
                    this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
                }
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        LabelCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-label-cell',
                        template: "\n    <div *ngIf=\"value\" class=\"grid-none-editable-label-cell user-select-text text-ellipsis\" title=\"{{_value}}\">\n      {{ _value }}\n    </div>\n  ",
                        styles: ["\n    .grid-none-editable-label-cell {\n      padding-top: 5px;\n      white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    }\n    "]
                    }] }
        ];
        return LabelCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DateCellComponent = /** @class */ (function () {
        function DateCellComponent() {
            this.defaultDateFormat = 'DD.MM.YYYY';
        }
        /**
         * @param {?} params
         * @return {?}
         */
        DateCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                if (this.params && this.params.colDef && this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.settings = this.params.colDef.cellEditorParams.settings;
                    }
                }
                if (this.settings) {
                    if (this.settings.showYear) {
                        this.defaultDateFormat = 'YYYY';
                    }
                    if (this.settings.showTimeOnly) {
                        this.defaultDateFormat = 'HH:mm';
                    }
                    if (this.settings.showTime) {
                        this.defaultDateFormat = 'DD.MM.YYYY HH:mm';
                    }
                    if (this.settings.dateFormat) {
                        this.defaultDateFormat = this.settings.dateFormat;
                    }
                }
                if (this.params.value) {
                    this.value = moment(this.params.value).format(this.defaultDateFormat);
                }
                else {
                    this.value = null;
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        DateCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.value = this.params.value;
                if (this.defaultDateFormat && this.value) {
                    this.value = moment(this.value).format(this.defaultDateFormat);
                }
                return true;
            };
        DateCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-date-cell',
                        template: "\n      <div *ngIf=\"value\" class=\"grid-none-editable-date-cell user-select-text text-ellipsis\">\n        {{ value }}\n      </div>\n  ",
                        styles: ["\n      .grid-none-editable-date-cell {\n        padding-top: 5px;\n      }\n    "]
                    }] }
        ];
        return DateCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BooleanCellComponent = /** @class */ (function () {
        function BooleanCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        BooleanCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this.rowId = this.params.node.id;
            };
        /**
         * @param {?} params
         * @return {?}
         */
        BooleanCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                this.formGroup.at(this.key).disable({ onlySelf: true });
                this.formGroup.at(this.key).patchValue(this.value);
                return true;
            };
        /**
         * @return {?}
         */
        BooleanCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        BooleanCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-boolean-cell',
                        template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\" class=\"grid-boolean-container\">\n\t\t<layout-form-checkbox [formControl]=\"formGroup.controls[key]\" isReadOnly=\"true\"></layout-form-checkbox>\n    </div>\n  ",
                        styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
                    }] }
        ];
        return BooleanCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CurrencyCellComponent = /** @class */ (function () {
        function CurrencyCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        CurrencyCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this.rowId = this.params.node.id;
            };
        /**
         * @param {?} params
         * @return {?}
         */
        CurrencyCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        CurrencyCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-currency-cell',
                        template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\">\n      <layout-form-currency-input\n        [formControlName]=\"key\"\n        class=\"coreGridLabelCellDisable align-middle\"\n        [isReadOnly]=\"true\">\n      </layout-form-currency-input>\n    </div>\n  ",
                        styles: ["\n      .coreGridLabelCellDisable /deep/ input {\n        background: rgba(0, 0, 0, 0);\n        border: none;\n        width: 100%;\n        white-space: nowrap;\n        overflow: hidden;\n        text-overflow: ellipsis;\n      }\n    "]
                    }] }
        ];
        return CurrencyCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskCellComponent = /** @class */ (function () {
        function MaskCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        MaskCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this.rowId = this.params.node.id;
            };
        /**
         * @param {?} params
         * @return {?}
         */
        MaskCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.settings = this.params.colDef.cellEditorParams.settings;
                        this.maskType = this.settings.maskType;
                    }
                    else {
                        console.error(this.params.colDef.field + ' settings is not defined');
                    }
                }
                else {
                    console.error(this.params.colDef.field + ' settings is not defined');
                }
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        MaskCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-mask-cell',
                        template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\">\n      <layout-form-mask-input\n        class=\"coreGridLabelCellDisable align-middle\"\n        [formControlName]=\"key\"\n        [maskType]=\"maskType\"\n        [isReadOnly]=\"true\">\n      </layout-form-mask-input>\n    </div>\n  ",
                        styles: ["\n      .coreGridLabelCellDisable /deep/ input {\n        background: rgba(0, 0, 0, 0);\n        border: none;\n        width: 100%;\n        white-space: nowrap;\n        overflow: hidden;\n        text-overflow: ellipsis;\n      }\n    "]
                    }] }
        ];
        return MaskCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NumericCellComponent = /** @class */ (function () {
        function NumericCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        NumericCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this._value = this.value;
                this.rowId = this.params.node.id;
            };
        /**
         * @param {?} params
         * @return {?}
         */
        NumericCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                // done for experimental,
                // use case is below
                // {
                // field: 'Department',
                // columnType: CoreGridColumnType.Numeric,
                // settings: {
                //   fields: ['DepartmentId'] //for this reason
                // },
                // headerName: 'DepartmentHistoryDepartmentId'
                // },
                // {
                //   field: 'DepartmentId',
                //   columnType: CoreGridColumnType.Lov,
                //   visible: true,
                //   settings: {
                //     options: this.departmentHistoryLovGridOptions,
                //     valueField: 'DepartmentId',
                //     labelField: ['Definition']
                //   }
                // }
                if (this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        if (this.params.colDef.cellEditorParams.settings.fields instanceof Array) {
                            /** @type {?} */
                            var labels = ( /** @type {?} */(this.params.colDef.cellEditorParams.settings.fields));
                            /** @type {?} */
                            var value_1 = '';
                            labels.forEach(function (columnName) {
                                if (_this.params.node.data[columnName]) {
                                    value_1 += _this.params.node.data[columnName] + ' ';
                                }
                            });
                            if (value_1 !== '') {
                                this.value = value_1;
                            }
                        }
                    }
                }
                this._value = this.value;
                if (this.params.colDef.cellEditorParams.customDisplayFunction) {
                    this._value = this.params.colDef.cellEditorParams.customDisplayFunction(this.value);
                }
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        NumericCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-numeric-cell',
                        template: "\n  <div *ngIf=\"value || value === 0\" class=\"grid-none-editable-numeric-cell user-select-text text-ellipsis\" title=\"{{_value}}\">\n    {{ _value }}\n  </div>\n  ",
                        styles: ["\n    .grid-none-editable-numeric-cell {\n      padding-top: 5px;\n      white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    }\n    "]
                    }] }
        ];
        return NumericCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TextEditCellComponent = /** @class */ (function () {
        function TextEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        TextEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value ? this.params.value.toString() : '';
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        TextEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @return {?}
         */
        TextEditCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        TextEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        TextEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-text-edit-cell',
                        template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-form-text-input\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (changed)=\"onValueChanged()\">\n      </layout-form-text-input>\n    </div>\n  ",
                        styles: [""]
                    }] }
        ];
        return TextEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SelectEditCellComponent = /** @class */ (function () {
        function SelectEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        SelectEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value ? +this.params.value : null;
                this.rowId = this.params.node.id;
                this.gridService = this.params.context.componentParent;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
                if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
                    this.settings = this.params.colDef.cellEditorParams.settings;
                    this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
                }
                this.dataSubscription = this.params.colDef.cellEditorParams.data.subscribe(function (x) {
                    _this.getListData();
                });
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.createListeningSubscription = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.filterByField) {
                    // clear if suscription has
                    this.clearfilterByFieldSubscription();
                    // if formgroup not exist, get formgroup
                    if (!this.formGroup) {
                        this.formGroup = this.params.context.formGroup.controls[this.rowId];
                    }
                    if (this.formGroup) {
                        // get key in order to track changes
                        /** @type {?} */
                        var columnKeyToListen_1 = this.getFilterByFieldKey();
                        // for initial value fill according to that
                        /** @type {?} */
                        var formgroupForKeyToListen = this.formGroup.at(columnKeyToListen_1);
                        if (formgroupForKeyToListen && (formgroupForKeyToListen.value || formgroupForKeyToListen.value === 0)) {
                            this.listData = (( /** @type {?} */(this.params.colDef.cellEditorParams.dataSource)))
                                .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                        }
                        // listen that cloumn changes at that field
                        this.filterByFieldSubscription = this.formGroup.at(columnKeyToListen_1).valueChanges
                            .subscribe(function (data) {
                            if (!data) {
                                // if the data set to null in that column, then set selft to null
                                _this.setValue(null);
                                // clear list because data comes according to this data value
                                _this.listData = [];
                            }
                            else {
                                // clear list in order to assign new ones
                                _this.listData = [];
                                // fill the list according to value at that column
                                _this.listData = (( /** @type {?} */(_this.params.colDef.cellEditorParams.dataSource)))
                                    .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                                // look the current selected id, if it is exist in new listData,
                                /** @type {?} */
                                var foundData = _this.listData.find(function (x) { return x[_this.settings.valueField] === _this.formGroup.at(_this.key).value; });
                                if (!foundData) {
                                    // if it is not clear data of select
                                    _this.setValue(null);
                                    // somehow lisData broken, in order to avoid this, I re assigned that
                                    _this.listData = (( /** @type {?} */(_this.params.colDef.cellEditorParams.dataSource)))
                                        .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_1).value; });
                                }
                            }
                        });
                    }
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        SelectEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                if (this.params.colDef.cellEditorParams) {
                    this.getListData();
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.settings = this.params.colDef.cellEditorParams.settings;
                        this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
                    }
                    else {
                        console.error(this.params.colDef.field + ' lov settings is not defined');
                    }
                }
                else {
                    console.error(this.params.colDef.field + ' data source or lov settings is not defined');
                }
                return true;
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.getListData = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.params.colDef.cellEditorParams.dataSource) {
                    // get filterByField
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.filterByField = this.params.colDef.cellEditorParams.settings.filterByField;
                    }
                    if (!this.filterByField) {
                        // if filterByField not exist, then assign orginal data source
                        this.listData = this.params.colDef.cellEditorParams.dataSource;
                    }
                    else {
                        // if exist, get the key, then filter according to that
                        /** @type {?} */
                        var columnKeyToListen_2 = this.getFilterByFieldKey();
                        this.listData = (( /** @type {?} */(this.params.colDef.cellEditorParams.dataSource)))
                            .filter(function (x) { return x[_this.filterByField] === _this.formGroup.at(columnKeyToListen_2).value; });
                    }
                    this.clearDataSubscription();
                    this.createListeningSubscription();
                }
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.getFilterByFieldKey = /**
         * @return {?}
         */
            function () {
                var _this = this;
                // get find Column according to  filterByField
                /** @type {?} */
                var column = this.gridService.allGridColumns.find(function (x) { return x.colDef.field === _this.filterByField; });
                // createKey with that column in order to use for tracking
                /** @type {?} */
                var columnKeyToListen = this.params.context.createKey(this.params.columnApi, column);
                return columnKeyToListen;
            };
        /**
         * @param {?} data
         * @return {?}
         */
        SelectEditCellComponent.prototype.onValueChanged = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
                this.params.setValue(this.value);
            };
        /**
         * @param {?} value
         * @return {?}
         */
        SelectEditCellComponent.prototype.setValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.value = value;
                this.formGroup.at(this.key).patchValue(value);
                this.params.setValue(value);
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.clearDataSubscription = /**
         * @return {?}
         */
            function () {
                if (this.dataSubscription) {
                    this.dataSubscription.unsubscribe();
                }
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.clearfilterByFieldSubscription = /**
         * @return {?}
         */
            function () {
                if (this.filterByFieldSubscription) {
                    this.filterByFieldSubscription.unsubscribe();
                }
            };
        /**
         * @return {?}
         */
        SelectEditCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.clearDataSubscription();
                this.clearfilterByFieldSubscription();
            };
        SelectEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-select-edit-cell',
                        template: "\n    <div\n      *ngIf=\"formGroup && this.settings && this.settings.valueField\"\n      [formGroup]=\"formGroup\">\n      <layout-static-selector\n        #selector\n\t\t\t\t[class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        (changed)=\"onValueChanged($event)\"\n        [dataSource]=\"listData\"\n        [isReadOnly]=\"isReadOnly\"\n        [valueField]=\"this.settings.valueField\"\n        [labelField]=\"this.settings.labelField\"\n      >\n      </layout-static-selector>\n    </div>\n  ",
                        styles: [":host /deep/ .custom-ng-select{margin-top:2px}:host /deep/ .not-valid .custom-ng-select .ng-select-container{border:1px solid red}:host /deep/ .ng-select.ng-select-single .ng-select-container .ng-value-container .ng-input{top:1px}"]
                    }] }
        ];
        return SelectEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NumericEditCellComponent = /** @class */ (function () {
        function NumericEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        NumericEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value;
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        NumericEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @return {?}
         */
        NumericEditCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        NumericEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        NumericEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-numeric-edit-cell',
                        template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-form-number-input\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (changed)=\"onValueChanged()\">\n      </layout-form-number-input>\n    </div>\n  ",
                        styles: [""]
                    }] }
        ];
        return NumericEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LovEditCellComponent = /** @class */ (function () {
        function LovEditCellComponent() {
        }
        Object.defineProperty(LovEditCellComponent.prototype, "content", {
            set: /**
             * @param {?} component
             * @return {?}
             */ function (component) {
                if (component) {
                    this.setLovComponent(component);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} params
         * @return {?}
         */
        LovEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                // console.log(params);
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value ? +this.params.value : null;
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
                this.dataSource = (( /** @type {?} */(this.params.colDef.cellEditorParams.dataSource)));
            };
        /**
         * @param {?} params
         * @return {?}
         */
        LovEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                var _this = this;
                this.params = params;
                // console.log(params);
                if (this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.settings = this.params.colDef.cellEditorParams.settings;
                    }
                    else {
                        console.error(this.params.colDef.field + 'lov Setting is not defined');
                    }
                }
                else {
                    console.error(this.params.colDef.field + 'lov Setting is not defined');
                }
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                // console.log('patch-value', this.value);
                if (this.formGroup && this.params.colDef.cellEditorParams.customFunctionForRowValueChanges) {
                    this.clearRowChangesSubscription();
                    this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(this);
                    setTimeout(function () {
                    });
                    this.rowChangesSubscription = this.formGroup.valueChanges
                        .subscribe(function (rowData) {
                        _this.params.colDef.cellEditorParams.customFunctionForRowValueChanges(_this);
                    });
                }
                return true;
            };
        /**
         * @param {?} component
         * @return {?}
         */
        LovEditCellComponent.prototype.setLovComponent = /**
         * @param {?} component
         * @return {?}
         */
            function (component) {
                this.listOfValue = component;
                if (this.settings) {
                    this.listOfValue.setOptions(this.settings.options, this.settings.valueField, this.settings.labelField, this.settings.dontInitData);
                }
            };
        /**
         * @return {?}
         */
        LovEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        /**
         * @param {?} event
         * @return {?}
         */
        LovEditCellComponent.prototype.lovValueChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.value = this.formGroup.at(this.key).value ? +this.formGroup.at(this.key).value : null;
                if (!this.params.colDef.cellEditorParams.dataSource) {
                    this.params.colDef.cellEditorParams.dataSource = [];
                }
                // console.log('lovValueChanged', this.params);
                if (event && this.listOfValue) {
                    (( /** @type {?} */(this.params.colDef.cellEditorParams.dataSource))).push({
                        Id: this.listOfValue.listOfValuesService.endPointUrl,
                        Value: event
                    });
                    this.dataSource = this.params.colDef.cellEditorParams.dataSource;
                    // console.log('dataSource', this.dataSource);
                }
                this.params.setValue(this.value);
            };
        /**
         * @param {?} data
         * @return {?}
         */
        LovEditCellComponent.prototype.onAddToDataSource = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (!this.params.colDef.cellEditorParams.dataSource) {
                    this.params.colDef.cellEditorParams.dataSource = [];
                }
                (( /** @type {?} */(this.params.colDef.cellEditorParams.dataSource))).push(data);
                this.dataSource = this.params.colDef.cellEditorParams.dataSource;
            };
        /**
         * @return {?}
         */
        LovEditCellComponent.prototype.clearRowChangesSubscription = /**
         * @return {?}
         */
            function () {
                if (this.rowChangesSubscription) {
                    this.rowChangesSubscription.unsubscribe();
                }
            };
        /**
         * @return {?}
         */
        LovEditCellComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.clearRowChangesSubscription();
            };
        LovEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-lov-edit-cell',
                        template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-list-of-values\n      [class.not-valid]=\"!isValid()\"\n      #listOfValue\n      [dataSource]=\"dataSource\"\n      [isReadOnly]=\"isReadOnly\"\n      [formControlName]=\"key\"\n      (lovValueChanged)=\"lovValueChanged($event)\"\n      (addToDataSource)=\"onAddToDataSource($event)\"\n      ></layout-list-of-values>\n    </div>\n  ",
                        styles: [".not-valid input{border:1px solid red}:host /deep/ .lov-btn-container{margin-top:3px}"]
                    }] }
        ];
        LovEditCellComponent.propDecorators = {
            content: [{ type: i0.ViewChild, args: ['listOfValue',] }]
        };
        return LovEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DateEditCellComponent = /** @class */ (function () {
        function DateEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        DateEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = params.context.createKey(this.params.columnApi, this.params.column);
                this.value = this.params.value ? this.params.value : null;
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
                if (this.params.colDef && this.params.colDef.cellEditorParams && this.params.colDef.cellEditorParams.settings) {
                    this.settings = this.params.colDef.cellEditorParams.settings;
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        DateEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @param {?} date
         * @return {?}
         */
        DateEditCellComponent.prototype.onDateChanges = /**
         * @param {?} date
         * @return {?}
         */
            function (date) {
                this.value = this.formGroup.at(this.key).value ? this.formGroup.at(this.key).value : null;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        DateEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        DateEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-date-edit-cell',
                        template: "\n    <div *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\"\n      class=\"input-group\" style=\"width:100%; height:30px;margin-top:2px;\">\n      <layout-form-datepicker-input style=\"width:100%;\"\n\t\t\t[class.not-valid]=\"!isValid()\"\n      [formControlName]=\"key\"\n      [isReadOnly]=\"isReadOnly\"\n\t\t  [settings]= \"settings\"\n      (dateChanges)= \"onDateChanges($event)\"></layout-form-datepicker-input>\n    </div>\n  ",
                        styles: [":host /deep/ .not-valid .ui-calendar input{border:1px solid red}"]
                    }] }
        ];
        return DateEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BooleanEditCellComponent = /** @class */ (function () {
        function BooleanEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        BooleanEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                if (this.params.value === undefined) {
                    this.value = false;
                    this.params.setValue(this.value);
                }
                else {
                    this.value = this.params.value;
                }
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        BooleanEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @return {?}
         */
        BooleanEditCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        BooleanEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        BooleanEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-boolean-edit-cell',
                        template: "\n    <div  *ngIf=\"formGroup\"  class=\"grid-boolean-container\" [formGroup]=\"formGroup\">\n      <layout-form-checkbox\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (checkChanged)=\"onValueChanged()\">\n      </layout-form-checkbox>\n    </div>\n  ",
                        styles: [".grid-boolean-container{text-align:center;padding-top:3px}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}"]
                    }] }
        ];
        return BooleanEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskEditCellComponent = /** @class */ (function () {
        function MaskEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        MaskEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = this.params.value ? this.params.value.toString() : '';
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        MaskEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                if (this.params.colDef.cellEditorParams) {
                    if (this.params.colDef.cellEditorParams.settings) {
                        this.settings = this.params.colDef.cellEditorParams.settings;
                        this.maskType = this.settings.maskType;
                    }
                    else {
                        console.error(this.params.colDef.field + ' settings is not defined');
                    }
                }
                else {
                    console.error(this.params.colDef.field + ' settings is not defined');
                }
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @return {?}
         */
        MaskEditCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        MaskEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        MaskEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-mask-edit-cell',
                        template: "\n    <div *ngIf=\"formGroup\" [formGroup]=\"formGroup\">\n    <layout-form-mask-input\n      [class.not-valid]=\"!isValid()\"\n      [formControlName]=\"key\"\n      [maskType]=\"maskType\"\n      [isReadOnly]=\"isReadOnly\"\n      (changed)=\"onValueChanged()\"\n      [isReadOnly]=\"true\">\n    </layout-form-mask-input>\n    </div>\n  ",
                        styles: [""]
                    }] }
        ];
        return MaskEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CurrencyEditCellComponent = /** @class */ (function () {
        function CurrencyEditCellComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        CurrencyEditCellComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.key = this.params.context.createKey(this.params.columnApi, params.column);
                this.value = (this.params.value || this.params.value === 0) ? this.params.value.toString() : null;
                this.rowId = this.params.node.id;
                if (this.params.colDef.cellEditorParams.customEditableFunction) {
                    this.isReadOnly = !this.params.colDef.cellEditorParams.customEditableFunction(this.params.data);
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        CurrencyEditCellComponent.prototype.refresh = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.formGroup = this.params.context.formGroup.controls[this.rowId];
                if (this.formGroup) {
                    this.formGroup.at(this.key).patchValue(this.value);
                }
                return true;
            };
        /**
         * @return {?}
         */
        CurrencyEditCellComponent.prototype.onValueChanged = /**
         * @return {?}
         */
            function () {
                this.value = this.formGroup.at(this.key).value;
                this.params.setValue(this.value);
            };
        /**
         * @return {?}
         */
        CurrencyEditCellComponent.prototype.isValid = /**
         * @return {?}
         */
            function () {
                return !((this.formGroup.at(this.key).errors && (this.formGroup.at(this.key).dirty || this.formGroup.at(this.key).touched)));
            };
        CurrencyEditCellComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid-currency-edit-cell',
                        template: "\n    <div\n      *ngIf=\"formGroup\"\n      [formGroup]=\"formGroup\">\n      <layout-form-currency-input\n        [class.not-valid]=\"!isValid()\"\n        [formControlName]=\"key\"\n        [isReadOnly]=\"isReadOnly\"\n        (changed)=\"onValueChanged()\">\n      </layout-form-currency-input>\n    </div>\n  ",
                        styles: [""]
                    }] }
        ];
        return CurrencyEditCellComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ColumnHeaderComponent = /** @class */ (function () {
        function ColumnHeaderComponent(elementRef, sweetAlertService) {
            this.sweetAlertService = sweetAlertService;
            this.elementRef = elementRef;
        }
        /**
         * @param {?} params
         * @return {?}
         */
        ColumnHeaderComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
                this.coreGridService = this.params.context.componentParent;
                this.coldId = this.params.column.getColId();
                this.params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
                this.onSortChanged();
            };
        /**
         * @return {?}
         */
        ColumnHeaderComponent.prototype.onMenuClick = /**
         * @return {?}
         */
            function () {
                this.params.showColumnMenu(this.querySelector('.customHeaderMenuButton'));
            };
        /**
         * @param {?} order
         * @param {?} event
         * @return {?}
         */
        ColumnHeaderComponent.prototype.onSortRequested = /**
         * @param {?} order
         * @param {?} event
         * @return {?}
         */
            function (order, event) {
                this.params.setSort(order, event.shiftKey);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        ColumnHeaderComponent.prototype.onClickHeader = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                var _this = this;
                if (this.params.enableSorting) {
                    /** @type {?} */
                    var coreGridService = this.params.context.componentParent;
                    /** @type {?} */
                    var bulkData = coreGridService.getBulkOperationData();
                    if (bulkData && bulkData.length > 0) {
                        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Cancel)
                            .subscribe(function (res) {
                            if (res && res.value) {
                                _this.setSort(event);
                            }
                        });
                    }
                    else {
                        this.setSort(event);
                    }
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        ColumnHeaderComponent.prototype.setSort = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.sorted === '') {
                    this.sorted = 'desc';
                }
                else if (this.sorted === 'desc') {
                    this.sorted = 'asc';
                }
                else if (this.sorted === 'asc') {
                    this.sorted = 'desc';
                }
                /** @type {?} */
                var sortModel = {
                    colId: this.coldId,
                    sort: this.sorted
                };
                this.coreGridService.onSortChanged(sortModel);
                // this.params.setSort(this.sorted, event.shiftKey);
            };
        /**
         * @return {?}
         */
        ColumnHeaderComponent.prototype.onSortChanged = /**
         * @return {?}
         */
            function () {
                if (this.params.column.isSortAscending()) {
                    this.sorted = 'asc';
                }
                else if (this.params.column.isSortDescending()) {
                    this.sorted = 'desc';
                }
                else {
                    this.sorted = '';
                }
            };
        /**
         * @return {?}
         */
        ColumnHeaderComponent.prototype.isColumnSorted = /**
         * @return {?}
         */
            function () {
                return this.coreGridService.currentSortedColId === this.coldId;
            };
        /**
         * @private
         * @param {?} selector
         * @return {?}
         */
        ColumnHeaderComponent.prototype.querySelector = /**
         * @private
         * @param {?} selector
         * @return {?}
         */
            function (selector) {
                return ( /** @type {?} */(this.elementRef.nativeElement.querySelector('.customHeaderMenuButton', selector)));
            };
        ColumnHeaderComponent.decorators = [
            { type: i0.Component, args: [{
                        template: "<!-- <div  (click) = \"onClickHeader($event)\" style= \"height: 100%; width: 100%;\">\r\n    <div class=\"cutomLabelContainer\">\r\n        <div class=\"customHeaderLabel\">\r\n            {{params.displayName | translate}}\r\n        </div>\r\n\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'desc'\" class=\"{{'customSortDownLabel'+ (this.sorted === 'desc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('desc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-down\"></i>\r\n        </div>\r\n        <div *ngIf=\"params.enableSorting && this.sorted === 'asc'\"  class=\"{{'customSortUpLabel'+ (this.sorted === 'asc' ? ' active' : '') }}\"\r\n        (click)=\"onSortRequested('asc', $event); $event.stopPropagation();\">\r\n            <i class=\"fa fa-long-arrow-up\"></i>\r\n        </div>\r\n    </div>\r\n\r\n    <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->\r\n\r\n<div class=\"ag-cell-label-container\" role=\"presentation\">\r\n    <div class=\"ag-header-cell-label\" role=\"presentation\" (click)=\"onClickHeader($event); $event.stopPropagation();\">\r\n        <span class=\"ag-header-cell-text\" role=\"columnheader\">{{params.displayName | translate}}</span>\r\n        <!-- <span ref=\"eFilter\" class=\"ag-header-icon ag-filter-icon\"></span> -->\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'asc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-ascending-icon sort-icons\"></span>\r\n        <span *ngIf=\"params.enableSorting && this.sorted === 'desc' && isColumnSorted()\"\r\n            class=\"ag-header-icon ag-sort-descending-icon sort-icons\"></span>\r\n        <!-- <span ref=\"eSortNone\" class=\"ag-header-icon ag-sort-none-icon\" ></span> -->\r\n    </div>\r\n    <!-- <span [hidden]=\"!params.enableMenu\"  ref=\"eMenu\" class=\"ag-header-icon ag-header-cell-menu-button\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <span class=\"ag-icon ag-icon-menu\"></span>\r\n    </span> -->\r\n    <!-- <div [hidden]=\"!params.enableMenu\" class=\"customHeaderMenuButton\" (click)=\"onMenuClick(); $event.stopPropagation();\">\r\n        <i class=\"fa fa-bars\"></i>\r\n    </div>\r\n</div> -->",
                        styles: [".customHeaderMenuButton{position:absolute;right:15px;top:0}.customSortDownLabel{float:left;margin-left:7px;margin-top:1px}.customSortUpLabel{float:left;margin-left:7px}.customSortRemoveLabel{float:left;font-size:11px;margin-left:3px}.cutomLabelContainer{width:calc(100% - 30px);height:100%;overflow:hidden}.customHeaderLabel{margin-left:5px;float:left}.active{color:#6495ed}.sort-icons{margin-top:8px!important}"]
                    }] }
        ];
        /** @nocollapse */
        ColumnHeaderComponent.ctorParameters = function () {
            return [
                { type: i0.ElementRef },
                { type: SweetAlertService }
            ];
        };
        return ColumnHeaderComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NoRowsOverlayComponent = /** @class */ (function () {
        function NoRowsOverlayComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        NoRowsOverlayComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
            };
        NoRowsOverlayComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'grid-no-rows-overlay',
                        template: "<div>" +
                            "   {{ 'NoRows' | translate }}" +
                            "</div>"
                    }] }
        ];
        return NoRowsOverlayComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingOverlayComponent = /** @class */ (function () {
        function LoadingOverlayComponent() {
        }
        /**
         * @param {?} params
         * @return {?}
         */
        LoadingOverlayComponent.prototype.agInit = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.params = params;
            };
        LoadingOverlayComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'grid-loading-overlay',
                        template: "\n  <div class=\"loader\">\n  <div class=\"inner one\"></div>\n  <div class=\"inner two\"></div>\n  <div class=\"inner three\"></div> \n</div>\n  ",
                        styles: [".loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
                    }] }
        ];
        return LoadingOverlayComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var GridLoadingService = /** @class */ (function (_super) {
        __extends(GridLoadingService, _super);
        function GridLoadingService() {
            return _super.call(this) || this;
        }
        GridLoadingService.decorators = [
            { type: i0.Injectable }
        ];
        /** @nocollapse */
        GridLoadingService.ctorParameters = function () { return []; };
        return GridLoadingService;
    }(LoadingService));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreGridService = /** @class */ (function () {
        function CoreGridService(dataService, toastr, loadingService, sweetAlertService, ngZone, router, route) {
            var _this = this;
            this.dataService = dataService;
            this.toastr = toastr;
            this.loadingService = loadingService;
            this.sweetAlertService = sweetAlertService;
            this.ngZone = ngZone;
            this.router = router;
            this.route = route;
            this.pagingResult = {};
            this.data = [];
            this.originalRowData = [];
            this.deletedData = [];
            this.filter = [];
            this.filterGroupList = [];
            this.gridForm = new forms.FormGroup({});
            this.loadOnInit = true;
            this.isDataChanged = false;
            this.isLoad = false;
            this.columnDefs = [];
            this.allGridColumns = [];
            this.recordEventsSubject = new rxjs.Subject();
            this.retrievedDataSourceSubject = new rxjs.Subject();
            this.loadingDisplayStatusSubscription = this.loadingService.getLoadingBarDisplayStatus().subscribe(function (data) {
                _this.loadingDisplayStatus = data;
                _this.loadingOverlay();
            });
        }
        /**
         * @param {?} options
         * @param {?} loadOnInit
         * @param {?=} loadColumnDataSourceOnInit
         * @return {?}
         */
        CoreGridService.prototype.init = /**
         * @param {?} options
         * @param {?} loadOnInit
         * @param {?=} loadColumnDataSourceOnInit
         * @return {?}
         */
            function (options, loadOnInit, loadColumnDataSourceOnInit) {
                this.loadOnInit = loadOnInit;
                this.loadColumnDataSourceOnInit = loadColumnDataSourceOnInit;
                this.gridOptions = options;
                if (this.gridApi) {
                    this.pagingResult = {
                        CurrentPage: 1,
                        PageSize: options.requestOptions.CustomFilter.PageSize
                    };
                    this.reloadColumnDefs();
                    this.setLoadUrlSetting();
                    if (this.loadOnInit === true) {
                        this.isLoad = true;
                        this.refreshGridData();
                    }
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.setLoadUrlSetting = /**
         * @return {?}
         */
            function () {
                this.urlOptions = {
                    moduleUrl: this.gridOptions.requestOptions.UrlOptions.moduleUrl,
                    endPointUrl: this.gridOptions.requestOptions.UrlOptions.endPointUrl
                };
            };
        /**
         * @private
         * @return {?}
         */
        CoreGridService.prototype.createColumns = /**
         * @private
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.gridOptions && this.gridOptions.enableCheckboxSelection) {
                    /** @type {?} */
                    var selectColumn = {
                        headerName: 'Select',
                        headerCheckboxSelection: true,
                        headerCheckboxSelectionFilteredOnly: true,
                        checkboxSelection: true,
                        minWidth: 40,
                        maxWidth: 40,
                        cellStyle: { 'padding-top': '5px' },
                        showRowGroup: false,
                        enableRowGroup: false,
                        enablePivot: false,
                        suppressFilter: true,
                        menuTabs: []
                    };
                    this.columnDefs.push(selectColumn);
                }
                this.gridOptions.columns.forEach(function (item) {
                    /** @type {?} */
                    var cellRenderer;
                    /** @type {?} */
                    var columEditable = _this.gridOptions.editable;
                    if (item.editable !== undefined) {
                        columEditable = item.editable;
                    }
                    if (columEditable) {
                        if (item.columnType === CoreGridColumnType.Label) {
                            cellRenderer = CoreGridCellType.CoreLabelCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Text) {
                            cellRenderer = CoreGridCellType.CoreTextEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Date) {
                            cellRenderer = CoreGridCellType.CoreDateEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Select) {
                            cellRenderer = CoreGridCellType.CoreSelectEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Numeric) {
                            cellRenderer = CoreGridCellType.CoreNumericEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Boolean) {
                            cellRenderer = CoreGridCellType.CoreBooleanEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Lov) {
                            cellRenderer = CoreGridCellType.CoreLOVEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Mask) {
                            cellRenderer = CoreGridCellType.CoreMaskEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Currency) {
                            cellRenderer = CoreGridCellType.CoreCurrencyEditCell;
                        }
                        else if (item.columnType === CoreGridColumnType.NumericLabel) {
                            cellRenderer = CoreGridCellType.CoreNumericCell;
                        }
                    }
                    else {
                        if (item.columnType === CoreGridColumnType.Label) {
                            cellRenderer = CoreGridCellType.CoreLabelCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Text) {
                            cellRenderer = CoreGridCellType.CoreLabelCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Date) {
                            cellRenderer = CoreGridCellType.CoreDateCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Select) {
                            cellRenderer = CoreGridCellType.CoreSelectCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Numeric) {
                            cellRenderer = CoreGridCellType.CoreNumericCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Boolean) {
                            cellRenderer = CoreGridCellType.CoreBooleanCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Lov) {
                            cellRenderer = CoreGridCellType.CoreSelectCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Mask) {
                            cellRenderer = CoreGridCellType.CoreMaskCell;
                        }
                        else if (item.columnType === CoreGridColumnType.Currency) {
                            cellRenderer = CoreGridCellType.CoreCurrencyCell;
                        }
                        else if (item.columnType === CoreGridColumnType.NumericLabel) {
                            cellRenderer = CoreGridCellType.CoreNumericCell;
                        }
                        else if (item.columnType === CoreGridColumnType.TextSelect) {
                            cellRenderer = CoreGridCellType.CoreSelectCell;
                        }
                    }
                    if (item.visible !== false) {
                        item.visible = true;
                    }
                    else {
                        item.visible = false;
                    }
                    /** @type {?} */
                    var headerName = item.field;
                    if (_this.gridOptions
                        && _this.gridOptions.requestOptions
                        && _this.gridOptions.requestOptions.UrlOptions
                        && _this.gridOptions.requestOptions.UrlOptions.endPointUrl) {
                        /** @type {?} */
                        var endPointUrl = _this.gridOptions.requestOptions.UrlOptions.endPointUrl;
                        if (endPointUrl.indexOf('/') !== -1) {
                            headerName = endPointUrl.split('/')[0] + item.field;
                        }
                        else {
                            headerName = endPointUrl + item.field;
                        }
                    }
                    /** @type {?} */
                    var column = {
                        field: item.field,
                        headerName: item.headerName || headerName,
                        cellRenderer: cellRenderer,
                        hide: !item.visible,
                        suppressSizeToFit: null,
                        filter: _this.gridOptions.enableFilter || false,
                        sortable: _this.gridOptions.enableSorting || false,
                        resizable: true,
                        cellEditorParams: {
                            dataSource: null,
                            settings: null,
                            data: null,
                            customDisplayFunction: item.customDisplayFunction || null,
                            customEditableFunction: item.customEditableFunction || null,
                            customFunctionForRowValueChanges: item.customFunctionForRowValueChanges || null
                        },
                        floatingFilterComponentParams: {
                            hideFilter: item.hideFilter || false,
                            serviceAccess: _this,
                            field: item.field,
                            customFilterFunction: item.customFilterFunction || null
                        },
                        customFieldForFilter: item.customFieldForFilter,
                        customFieldForSort: item.customFieldForSort,
                        getDataSourceAfterDataLoaded: item.getDataSourceAfterDataLoaded,
                        columnType: item.columnType
                    };
                    if (item.field === _this.gridOptions.keyField) {
                        column.suppressSizeToFit = true;
                    }
                    if (item.columnType === CoreGridColumnType.Date && (_this.gridOptions.editable || column['editable'])) {
                        column['minWidth'] = 135;
                    }
                    if (item.minWidth) {
                        column['minWidth'] = item.minWidth;
                    }
                    if (item.maxWidth) {
                        column['maxWidth'] = item.maxWidth;
                    }
                    if (item.dataSource) {
                        column.cellEditorParams.dataSource = item.dataSource;
                    }
                    if (item.settings) {
                        column.cellEditorParams.settings = item.settings;
                    }
                    if ((item.columnType === CoreGridColumnType.Select || cellRenderer === CoreGridCellType.CoreSelectCell)
                        && item.settings) {
                        column.cellEditorParams.data = new rxjs.Subject();
                        if (!item.getDataSourceAfterDataLoaded) {
                            _this.getColumnDataSource(column.cellEditorParams.data, column);
                        }
                    }
                    /** @type {?} */
                    var index = _.findIndex(_this.columnDefs, ['field', item.field]);
                    if (index < 0) {
                        _this.columnDefs.push(column);
                    }
                    else {
                        _this.columnDefs[index] = column;
                    }
                });
                this.setFilterComponents();
                this.addCommandColumn();
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.reloadColumnDefs = /**
         * @return {?}
         */
            function () {
                this.createColumns();
                this.reloadGridColumnDefs();
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getColumnState = /**
         * @return {?}
         */
            function () {
                console.log(this.columnApi.getColumnState());
            };
        /**
         * @param {?} data
         * @param {?} column
         * @param {?=} extraFilters
         * @return {?}
         */
        CoreGridService.prototype.getColumnDataSource = /**
         * @param {?} data
         * @param {?} column
         * @param {?=} extraFilters
         * @return {?}
         */
            function (data, column, extraFilters) {
                var _this = this;
                if (this.loadOnInit || this.loadColumnDataSourceOnInit) {
                    /** @type {?} */
                    var settings = ( /** @type {?} */(column.cellEditorParams.settings));
                    /** @type {?} */
                    var requestOptions = void 0;
                    if (settings) {
                        if (column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.TextSelect) {
                            requestOptions = JSON.parse(JSON.stringify((( /** @type {?} */(settings))).requestOptions));
                        }
                        else {
                            requestOptions = JSON.parse(JSON.stringify((( /** @type {?} */(settings))).options.requestOptions));
                        }
                    }
                    if (requestOptions) {
                        /** @type {?} */
                        var reqUrl_1 = requestOptions.UrlOptions.moduleUrl + requestOptions.UrlOptions.endPointUrl;
                        /** @type {?} */
                        var disableGeneralLoading = false;
                        if (!this.gridOptions.disableSelfLoading) {
                            disableGeneralLoading = true;
                            this.loadingService.insertLoadingRequest(reqUrl_1);
                        }
                        /** @type {?} */
                        var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                        if (extraFilters && extraFilters.length > 0) {
                            if (requestOptions.CustomFilter && requestOptions.CustomFilter.FilterGroups) {
                                if (requestOptions.CustomFilter.FilterGroups.length === 0) {
                                    requestOptions.CustomFilter.FilterGroups = [{
                                            Filters: []
                                        }];
                                }
                                else if (requestOptions.CustomFilter.FilterGroups.length > 0) {
                                    requestOptions.CustomFilter.FilterGroups.unshift({
                                        Filters: []
                                    });
                                }
                            }
                            else if (!requestOptions.CustomFilter) {
                                requestOptions.CustomFilter = {
                                    FilterGroups: [{
                                            Filters: []
                                        }]
                                };
                            }
                            else if (requestOptions.CustomFilter && !requestOptions.CustomFilter.FilterGroups) {
                                requestOptions.CustomFilter.FilterGroups = [{
                                        Filters: []
                                    }];
                            }
                            if (requestOptions.CustomFilter &&
                                requestOptions.CustomFilter.FilterGroups &&
                                requestOptions.CustomFilter.FilterGroups.length > 0) {
                                requestOptions.CustomFilter.FilterGroups[0].Filters = extraFilters;
                            }
                        }
                        this.dataService.getListBy(reqUrl_1, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                            .pipe(operators.finalize(function () {
                            if (!_this.gridOptions.disableSelfLoading) {
                                _this.loadingService.removeLoadingRequest(reqUrl_1);
                            }
                        }))
                            .subscribe(function (httpResponse) {
                            /** @type {?} */
                            var lovData = httpResponse.serviceResult.Result;
                            /** @type {?} */
                            var dataSource = lovData.map(function (x) { return Object.assign({}, x); });
                            /** @type {?} */
                            var colIndex = _.findIndex(_this.columnDefs, ['field', column.field]);
                            column.cellEditorParams.dataSource = dataSource;
                            _this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                            data.next({ data: true });
                        });
                    }
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getComponents = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var components = {};
                // Edit Cell
                components[CoreGridCellType.CoreBooleanEditCell] = BooleanEditCellComponent;
                components[CoreGridCellType.CoreDateEditCell] = DateEditCellComponent;
                components[CoreGridCellType.CoreLOVEditCell] = LovEditCellComponent;
                components[CoreGridCellType.CoreNumericEditCell] = NumericEditCellComponent;
                components[CoreGridCellType.CoreSelectEditCell] = SelectEditCellComponent;
                components[CoreGridCellType.CoreTextEditCell] = TextEditCellComponent;
                components[CoreGridCellType.CoreMaskEditCell] = MaskEditCellComponent;
                components[CoreGridCellType.CoreCurrencyEditCell] = CurrencyEditCellComponent;
                // Read-Only Cell
                components[CoreGridCellType.CoreBooleanCell] = BooleanCellComponent;
                components[CoreGridCellType.CoreLabelCell] = LabelCellComponent;
                components[CoreGridCellType.CoreDateCell] = DateCellComponent;
                components[CoreGridCellType.CoreSelectCell] = SelectCellComponent;
                components[CoreGridCellType.CoreCurrencyCell] = CurrencyCellComponent;
                components[CoreGridCellType.CoreMaskCell] = MaskCellComponent;
                components[CoreGridCellType.CoreNumericCell] = NumericCellComponent;
                // Filter Cell
                components[CoreGridCellType.CoreDateFilterCell] = DateFilterCellComponent;
                components[CoreGridCellType.CoreSelectFilterCell] = SelectFilterCellComponent;
                components[CoreGridCellType.CoreTextFilterCell] = TextFilterCellComponent;
                components[CoreGridCellType.CoreNumericFilterCell] = NumericFilterCellComponent;
                components[CoreGridCellType.CoreBooleanFilterCell] = BooleanFilterCellComponent;
                components[CoreGridCellType.CoreCurrencyFilterCell] = CurrencyFilterCellComponent;
                components[CoreGridCellType.CoreLOVFilterCell] = LovFilterCellComponent;
                components[CoreGridCellType.CoreMaskFilterCell] = MaskFilterCellComponent;
                // Command Cell
                components[CoreGridCellType.CoreCommandCell] = CommandCellComponent;
                components['agColumnHeader'] = ColumnHeaderComponent;
                components['customNoRowsOverlay'] = NoRowsOverlayComponent;
                components['customLoadingOverlay'] = LoadingOverlayComponent;
                return components;
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.setFilterComponents = /**
         * @return {?}
         */
            function () {
                this.columnDefs.forEach(function (item) {
                    if (item.cellRenderer === CoreGridCellType.CoreBooleanEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreDateEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreDateCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreDateFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreLOVEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreLOVFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreNumericEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreSelectEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreTextEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreBooleanCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreBooleanFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreLabelCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreMaskEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreMaskCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreMaskFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreCurrencyEditCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreCurrencyCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreCurrencyFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreNumericCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreNumericFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreSelectCell
                        && item.columnType === CoreGridColumnType.TextSelect) {
                        item.floatingFilterComponent = CoreGridCellType.CoreTextFilterCell;
                    }
                    else if (item.cellRenderer === CoreGridCellType.CoreSelectCell) {
                        item.floatingFilterComponent = CoreGridCellType.CoreSelectFilterCell;
                    }
                });
            };
        /**
         * @private
         * @param {?} genericExpression
         * @return {?}
         */
        CoreGridService.prototype.loadDataByFilterAndPageSetting = /**
         * @private
         * @param {?} genericExpression
         * @return {?}
         */
            function (genericExpression) {
                var _this = this;
                /** @type {?} */
                var reqUrl = "" + this.urlOptions.moduleUrl + this.urlOptions.endPointUrl;
                /** @type {?} */
                var headerParams = [];
                if (this.gridOptions && this.gridOptions.requestOptions && this.gridOptions.requestOptions.HeaderParameters) {
                    headerParams = this.gridOptions.requestOptions.HeaderParameters;
                }
                if (!this.gridOptions.disableSelfLoading) {
                    this.loadingService.insertLoadingRequest(reqUrl);
                    /** @type {?} */
                    var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                    headerParams.push(disableLoadingHeader);
                }
                return this.dataService.getListBy(reqUrl, genericExpression, headerParams)
                    .pipe(operators.finalize(function () {
                    if (!_this.gridOptions.disableSelfLoading) {
                        _this.loadingService.removeLoadingRequest(reqUrl);
                    }
                }));
            };
        /**
         * @private
         * @return {?}
         */
        CoreGridService.prototype.loadGridViewDataByOptionsUrlAndFilters = /**
         * @private
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.gridOptions) {
                    /** @type {?} */
                    var searchFilter_1 = _.cloneDeep(this.gridOptions.requestOptions.CustomFilter);
                    if (this.pagingResult) {
                        searchFilter_1.PageNumber = this.pagingResult.CurrentPage;
                        searchFilter_1.PageSize = this.pagingResult.PageSize;
                    }
                    searchFilter_1.FilterGroups = searchFilter_1.FilterGroups || [];
                    this.filter.forEach(function (element) {
                        /** @type {?} */
                        var name;
                        /** @type {?} */
                        var foundColumn = _this.gridOptions.columns.find(function (x) { return x.field === element.key; });
                        if (foundColumn) {
                            name = foundColumn.customFieldForFilter;
                        }
                        if (!name) {
                            if (element.key.indexOf('Id') > 0 &&
                                (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                                name = element.key.substr(0, element.key.indexOf('Id')) +
                                    '.' + element.key.substr(element.key.indexOf('Id'), element.key.length);
                            }
                            else {
                                name = element.key;
                            }
                        }
                        /** @type {?} */
                        var group = {
                            Filters: [{
                                    PropertyName: name,
                                    Value: element.value['filter'],
                                    Comparison: element.value['type'],
                                    OtherValue: element.value['other'] || null
                                }]
                        };
                        searchFilter_1.FilterGroups.push(group);
                    });
                    // it is for CoreGridColumnType.TextSelect
                    this.filterGroupList.forEach(function (filterGroupItem) {
                        searchFilter_1.FilterGroups.unshift(filterGroupItem.value);
                    });
                    // if (searchFilter.FilterGroups.length > 0) {
                    //   searchFilter.FilterGroups[searchFilter.FilterGroups.length - 1]['Connector'] = Connector.Or;
                    // }
                    this.loadDataByFilterAndPageSetting(searchFilter_1)
                        .subscribe(function (httpResponse) {
                        _this.pagingResult = httpResponse.pagingResult;
                        _this.data = httpResponse.serviceResult.Result;
                        if (_this.data) {
                            _this.originalRowData = _this.data.map(function (item) { return Object.assign({}, item); });
                        }
                        _this.retrievedDataSourceSubject.next(_this.data);
                        if (!_this.data || (_this.data && _this.data.length === 0)) {
                            _this.loadingOverlay();
                        }
                        _this.loadDataSourceAfterGetGridData();
                    });
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.loadDataSourceAfterGetGridData = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var needToLoadDataSourceColumns = this.columnDefs.filter(function (x) { return x.getDataSourceAfterDataLoaded === true; });
                needToLoadDataSourceColumns.forEach(function (column) {
                    /** @type {?} */
                    var columnValues = _this.data.map(function (x) { return x[column.field]; });
                    /** @type {?} */
                    var newFilters = [];
                    columnValues.forEach(function (columnValue) {
                        /** @type {?} */
                        var newFilter = {
                            PropertyName: column.cellEditorParams.settings.valueField,
                            Value: columnValue,
                            Comparison: Comparison.EqualTo,
                            Connector: Connector.Or
                        };
                        /** @type {?} */
                        var name = column.cellEditorParams.settings.valueField;
                        // can be changable
                        if (name) {
                            if (column.cellEditorParams.settings.valueField.indexOf('Id') !== -1 &&
                                (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                                name = name.substr(0, name.indexOf('Id')) +
                                    '.' + name.substr(name.indexOf('Id'), name.length);
                            }
                        }
                        newFilter.PropertyName = name;
                        newFilters.push(newFilter);
                    });
                    if (newFilters.length > 0) {
                        newFilters[newFilters.length - 1].Connector = Connector.And;
                    }
                    if (newFilters.length > 0) {
                        _this.getColumnDataSource(column.cellEditorParams.data, column, newFilters);
                    }
                });
                // getColumnDataSource 
            };
        /**
         * @private
         * @return {?}
         */
        CoreGridService.prototype.addCommandColumn = /**
         * @private
         * @return {?}
         */
            function () {
                /** @type {?} */
                var showRowDeleteButton = true;
                /** @type {?} */
                var showRowEditButton = true;
                /** @type {?} */
                var buttonCounts = 1;
                if (this.gridOptions && this.gridOptions.buttonSettings) {
                    if (this.gridOptions.buttonSettings.showRowDeleteButton !== undefined) {
                        showRowDeleteButton = this.gridOptions.buttonSettings.showRowDeleteButton;
                        if (showRowDeleteButton) {
                            buttonCounts++;
                        }
                    }
                    if (this.gridOptions.buttonSettings.showEditButton !== undefined) {
                        showRowEditButton = this.gridOptions.buttonSettings.showEditButton;
                        if (showRowEditButton) {
                            buttonCounts++;
                        }
                    }
                }
                // it is for delete and revert for inline grid
                // if (this.gridOptions.editable && buttonCounts === 1) {
                //   buttonCounts = 2;
                // }
                if (showRowDeleteButton || showRowEditButton) {
                    /** @type {?} */
                    var index = _.findIndex(this.columnDefs, ['field', 'CommandColumn']);
                    if (index < 0) {
                        /** @type {?} */
                        var column = {
                            headerName: '',
                            field: 'CommandColumn',
                            editable: false,
                            cellRenderer: CoreGridCellType.CoreCommandCell,
                            filter: false,
                            sortable: false,
                            minWidth: buttonCounts * 50,
                            maxWidth: buttonCounts * 50
                        };
                        this.columnDefs.push(column);
                    }
                    // this.columnDefs.push();
                }
            };
        /**
         * @private
         * @return {?}
         */
        CoreGridService.prototype.createFormControls = /**
         * @private
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var columns = this.columnApi.getAllColumns();
                /** @type {?} */
                var gridFormGroup = (( /** @type {?} */(this.gridForm)));
                // clear out old form group controls if switching between data sources
                /** @type {?} */
                var controlNames = Object.keys(gridFormGroup.controls);
                controlNames.forEach(function (controlName) {
                    gridFormGroup.removeControl(controlName);
                });
                this.gridApi.forEachNode(function (rowNode) {
                    /** @type {?} */
                    var formArray = new forms.FormArray([]);
                    columns.filter(function (column) { return column.getColDef().field !== 'CommandColumn'; })
                        .forEach(function (column) {
                        /** @type {?} */
                        var key = _this.createKey(_this.columnApi, column);
                        // the cells will use this same createKey method
                        /** @type {?} */
                        var col = _.find(_this.gridOptions.columns, ['field', column.getColDef().field]);
                        /** @type {?} */
                        var validation = [];
                        if (col) {
                            if (col.validation) {
                                if (col.validation.required) {
                                    validation.push(forms.Validators.required);
                                }
                                if (col.validation.email) {
                                    validation.push(forms.Validators.email);
                                }
                                if (col.validation.requiredTrue) {
                                    validation.push(forms.Validators.requiredTrue);
                                }
                                if (col.validation.minValue) {
                                    validation.push(forms.Validators.min(col.validation.minValue));
                                }
                                if (col.validation.maxValue) {
                                    validation.push(forms.Validators.max(col.validation.maxValue));
                                }
                                if (col.validation.minLength) {
                                    validation.push(forms.Validators.minLength(col.validation.minLength));
                                }
                                if (col.validation.maxLength) {
                                    validation.push(forms.Validators.maxLength(col.validation.maxLength));
                                }
                                if (col.validation.regex) {
                                    validation.push(forms.Validators.pattern(col.validation.regex));
                                }
                            }
                        }
                        formArray.setControl(( /** @type {?} */(key)), new forms.FormControl('', { validators: validation }));
                    });
                    gridFormGroup.addControl(( /** @type {?} */(rowNode.id)), formArray);
                });
            };
        /**
         * @private
         * @return {?}
         */
        CoreGridService.prototype.reloadGridColumnDefs = /**
         * @private
         * @return {?}
         */
            function () {
                // this.columnDefs = columnDefs;
                if (this.gridApi) {
                    this.gridApi.setColumnDefs(this.columnDefs);
                    this.columnApi.resetColumnState();
                }
                this.refreshFormControls();
                if (this.gridApi) {
                    this.gridApi.sizeColumnsToFit();
                }
            };
        /**
         * @param {?} columnName
         * @param {?} dataSource
         * @return {?}
         */
        CoreGridService.prototype.setColumnDataSource = /**
         * @param {?} columnName
         * @param {?} dataSource
         * @return {?}
         */
            function (columnName, dataSource) {
                /** @type {?} */
                var index = _.findIndex(this.gridOptions.columns, ['field', columnName]);
                this.gridOptions.columns[index].dataSource = dataSource;
                if (this.gridApi) {
                    this.gridApi.refreshCells({ columns: [columnName], force: true });
                }
            };
        /**
         * @param {?} column
         * @return {?}
         */
        CoreGridService.prototype.setColumnLovOptions = /**
         * @param {?} column
         * @return {?}
         */
            function (column) {
                var _this = this;
                if ((column.columnType === CoreGridColumnType.Select || column.columnType === CoreGridColumnType.Lov) && column.settings) {
                    /** @type {?} */
                    var settings_1 = ( /** @type {?} */(column.settings));
                    /** @type {?} */
                    var requestOptions = void 0;
                    if (column.columnType === CoreGridColumnType.Select) {
                        requestOptions = (( /** @type {?} */(settings_1))).requestOptions;
                    }
                    else {
                        requestOptions = (( /** @type {?} */(settings_1))).options.requestOptions;
                    }
                    /** @type {?} */
                    var reqUrl_2 = requestOptions.UrlOptions.moduleUrl + '/'
                        + requestOptions.UrlOptions.endPointUrl;
                    /** @type {?} */
                    var disableGeneralLoading = false;
                    if (!this.gridOptions.disableSelfLoading) {
                        disableGeneralLoading = true;
                        this.loadingService.insertLoadingRequest(reqUrl_2);
                    }
                    /** @type {?} */
                    var disableLoadingHeader = { header: Headers.DisableLoading, value: Headers.DisableLoading };
                    this.dataService.getListBy(reqUrl_2, requestOptions.CustomFilter, disableGeneralLoading ? [disableLoadingHeader] : null)
                        .pipe(operators.finalize(function () {
                        if (!_this.gridOptions.disableSelfLoading) {
                            _this.loadingService.removeLoadingRequest(reqUrl_2);
                        }
                    }))
                        .subscribe(function (httpResponse) {
                        /** @type {?} */
                        var lovData = httpResponse.serviceResult.Result;
                        /** @type {?} */
                        var dataSource = lovData.map(function (x) { return Object.assign({}, x); });
                        /** @type {?} */
                        var colIndex = _.findIndex(_this.columnDefs, ['field', column.field]);
                        column.dataSource = dataSource;
                        _this.columnDefs[colIndex].cellEditorParams.dataSource = dataSource;
                        _this.columnDefs[colIndex].cellEditorParams.settings = settings_1;
                        if (_this.gridApi) {
                            _this.gridApi.refreshCells({ columns: [column.field], force: true });
                        }
                    });
                }
            };
        /**
         * @param {?} params
         * @return {?}
         */
        CoreGridService.prototype.onGridReady = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.gridApi = params.api;
                this.columnApi = params.columnApi;
                this.refreshFormControls();
                if (this.loadingDisplayStatus) {
                    this.gridApi.showLoadingOverlay();
                }
                if (this.isLoad === false) {
                    this.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
                }
                this.gridApi.sizeColumnsToFit();
            };
        /**
         * @param {?} params
         * @return {?}
         */
        CoreGridService.prototype.onColumnEverythingChanged = /**
         * @param {?} params
         * @return {?}
         */
            function (params) {
                this.allGridColumns = params.columnApi.getAllColumns();
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.onGridSizeChanged = /**
         * @return {?}
         */
            function () {
                if (this.gridApi) {
                    if (this.gridOptions && !this.gridOptions.disableSizeToFit && this.gridApi) {
                        this.gridApi.sizeColumnsToFit();
                    }
                    this.gridApi.refreshCells({ force: true });
                }
            };
        /**
         * @private
         * @param {?} columnApi
         * @param {?} column
         * @return {?}
         */
        CoreGridService.prototype.createKey = /**
         * @private
         * @param {?} columnApi
         * @param {?} column
         * @return {?}
         */
            function (columnApi, column) {
                return columnApi.getAllColumns().indexOf(column);
            };
        /**
         * @param {?} self
         * @return {?}
         */
        CoreGridService.prototype.getContext = /**
         * @param {?} self
         * @return {?}
         */
            function (self) {
                return {
                    componentParent: self,
                    formGroup: this.gridForm,
                    createKey: this.createKey
                };
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.refreshFormControls = /**
         * @return {?}
         */
            function () {
                if (this.gridApi) {
                    this.createFormControls();
                    this.gridApi.refreshCells({ force: true });
                }
            };
        /**
         * @param {?=} isCheckBulkData
         * @return {?}
         */
        CoreGridService.prototype.refreshGridData = /**
         * @param {?=} isCheckBulkData
         * @return {?}
         */
            function (isCheckBulkData) {
                var _this = this;
                /** @type {?} */
                var bulkData = this.getBulkOperationData();
                if (bulkData && bulkData.length > 0 && isCheckBulkData) {
                    this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Refresh)
                        .subscribe(function (res) {
                        if (res && res.value) {
                            _this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
                            _this.refreshGrid();
                            _this.toastr.success(ToastrMessages.GridRefreshed);
                        }
                    });
                }
                else {
                    this.recordEventsSubject.next({ type: 'RefreshedGrid', record: null });
                    this.refreshGrid();
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.refreshGrid = /**
         * @return {?}
         */
            function () {
                this.deletedData = [];
                this.isDataChanged = false;
                this.loadGridViewDataByOptionsUrlAndFilters();
                if (this.isLoad === false) {
                    this.init(this.gridOptions, true);
                }
            };
        /**
         * @param {?} item
         * @return {?}
         */
        CoreGridService.prototype.onCellValueChanged = /**
         * @param {?} item
         * @return {?}
         */
            function (item) {
                this.isDataChanged = true;
                if (item.data.OperationType !== OperationType.Created) {
                    item.data.OperationType = OperationType.Updated;
                }
            };
        /**
         * @param {?} sortModel
         * @return {?}
         */
        CoreGridService.prototype.onSortChanged = /**
         * @param {?} sortModel
         * @return {?}
         */
            function (sortModel) {
                if (sortModel) {
                    // const sortModel = _.head(this.gridApi.getSortModel());
                    this.currentSortedColId = sortModel.colId;
                    /** @type {?} */
                    var columnDef = ( /** @type {?} */(this.gridApi.getColumnDef(sortModel.colId)));
                    /** @type {?} */
                    var name_1 = columnDef.customFieldForSort;
                    if (!name_1) {
                        if (columnDef.field.indexOf('Id') > 0 &&
                            (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                            name_1 = columnDef.field.substr(0, columnDef.field.indexOf('Id'));
                        }
                        else {
                            name_1 = columnDef.field;
                        }
                    }
                    /** @type {?} */
                    var sort = [{
                            PropertyName: name_1,
                            Type: sortModel.sort === 'asc' ? SortType.Asc : SortType.Desc
                        }];
                    this.gridOptions.requestOptions.CustomFilter.Sort = sort;
                    this.loadGridViewDataByOptionsUrlAndFilters();
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridService.prototype.onFilterChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                // this.filter = Object.entries(this.gridApi.getFilterModel()).map(([key, value]) => ({ key, value }));
                this.loadGridViewDataByOptionsUrlAndFilters();
            };
        /**
         * @param {?} field
         * @param {?} model
         * @return {?}
         */
        CoreGridService.prototype.applyChangesToFilter = /**
         * @param {?} field
         * @param {?} model
         * @return {?}
         */
            function (field, model) {
                this.addToListForFilter(this.filter, model.filter || model.filter === 0 ? model : null, field);
            };
        /**
         * @param {?} listArray
         * @param {?} value
         * @param {?} key
         * @return {?}
         */
        CoreGridService.prototype.addToListForFilter = /**
         * @param {?} listArray
         * @param {?} value
         * @param {?} key
         * @return {?}
         */
            function (listArray, value, key) {
                /** @type {?} */
                var foundDataIndex = listArray.findIndex(function (x) { return x.key === key; });
                if (value) {
                    /** @type {?} */
                    var dataToSend = {
                        key: key,
                        value: value
                    };
                    if (foundDataIndex !== -1) {
                        listArray[foundDataIndex] = dataToSend;
                    }
                    else {
                        listArray.push(dataToSend);
                    }
                }
                else {
                    if (foundDataIndex !== -1) {
                        listArray.splice(foundDataIndex, 1);
                    }
                }
            };
        // called from text filter for CoreGridColumnType.TextSelect
        // called from text filter for CoreGridColumnType.TextSelect
        /**
         * @param {?} field
         * @param {?} settings
         * @param {?} model
         * @return {?}
         */
        CoreGridService.prototype.textSelectFindValuesOnFilter =
            // called from text filter for CoreGridColumnType.TextSelect
            /**
             * @param {?} field
             * @param {?} settings
             * @param {?} model
             * @return {?}
             */
            function (field, settings, model) {
                var _this = this;
                /** @type {?} */
                var foundColumn = this.gridOptions.columns.find(function (x) { return x.field === field; });
                if (model.filter) {
                    /** @type {?} */
                    var filter$$1 = {
                        PageSize: 100,
                        FilterGroups: [
                            {
                                Filters: [
                                    {
                                        PropertyName: ( /** @type {?} */(settings.labelField)),
                                        Value: model.filter,
                                        Comparison: model.type,
                                        Connector: Connector.And
                                    },
                                ]
                            }
                        ]
                    };
                    this.dataService.getListBy(settings.requestOptions.UrlOptions.moduleUrl + settings.requestOptions.UrlOptions.endPointUrl, filter$$1)
                        .subscribe(function (httpResponse) {
                        /** @type {?} */
                        var result = httpResponse.serviceResult.Result || [];
                        /** @type {?} */
                        var idList = result.map(function (x) { return x[settings.valueField]; }).filter(_this.onlyUnique);
                        /** @type {?} */
                        var newFilters = [];
                        /** @type {?} */
                        var name = '';
                        if (foundColumn) {
                            name = foundColumn.customFieldForFilter;
                        }
                        if (!name) {
                            if (field.indexOf('Id') > 0 &&
                                (_this.gridOptions.requestOptions.CustomEndPoint === false || _this.gridOptions.requestOptions.CustomEndPoint === undefined)) {
                                name = field.substr(0, field.indexOf('Id')) +
                                    '.' + field.substr(field.indexOf('Id'), field.length);
                            }
                            else {
                                name = field;
                            }
                        }
                        idList.forEach(function (id) {
                            newFilters.push({
                                PropertyName: name,
                                Value: id,
                                Comparison: Comparison.EqualTo,
                                Connector: Connector.Or
                            });
                        });
                        if (newFilters.length > 0) {
                            newFilters[newFilters.length - 1].Connector = Connector.And;
                        }
                        /** @type {?} */
                        var filterGroup = {
                            Connector: Connector.And,
                            Filters: newFilters
                        };
                        if (newFilters.length > 0) {
                            _this.addToListForFilter(_this.filterGroupList, filterGroup, field);
                            _this.loadGridViewDataByOptionsUrlAndFilters();
                        }
                        else {
                            // gridi boşalmak lazım
                            _this.data = [];
                            _this.pagingResult.TotalCount = 0;
                        }
                    });
                }
                else {
                    this.addToListForFilter(this.filterGroupList, null, field);
                    this.loadGridViewDataByOptionsUrlAndFilters();
                }
            };
        /**
         * @param {?} value
         * @param {?} index
         * @param {?} self
         * @return {?}
         */
        CoreGridService.prototype.onlyUnique = /**
         * @param {?} value
         * @param {?} index
         * @param {?} self
         * @return {?}
         */
            function (value, index, self) {
                return self.indexOf(value) === index;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridService.prototype.onPageChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.pagingResult.CurrentPage = event.page;
                this.pagingResult.PageSize = event.itemsPerPage;
                this.loadGridViewDataByOptionsUrlAndFilters();
            };
        /**
         * @param {?} pageSizeEvent
         * @return {?}
         */
        CoreGridService.prototype.onChangeItemsPerPage = /**
         * @param {?} pageSizeEvent
         * @return {?}
         */
            function (pageSizeEvent) {
                if (pageSizeEvent.Id) {
                    this.pagingResult.PageSize = pageSizeEvent.Id;
                }
                if (this.pagingResult.TotalCount > 0) {
                    this.loadGridViewDataByOptionsUrlAndFilters();
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getBulkOperationData = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var resultData = [];
                if (this.gridOptions && this.gridOptions.editable) {
                    this.validateForm();
                    this.deletedData
                        .forEach(function (obj) {
                        resultData.push(obj);
                    });
                    this.data.forEach(function (obj) {
                        if (obj.OperationType !== OperationType.None) {
                            resultData.push(obj);
                        }
                    });
                }
                return resultData;
            };
        /**
         * @param {?=} formGroup
         * @return {?}
         */
        CoreGridService.prototype.validateForm = /**
         * @param {?=} formGroup
         * @return {?}
         */
            function (formGroup) {
                var _this = this;
                if (formGroup === void 0) {
                    formGroup = this.gridForm;
                }
                Object.keys(formGroup.controls).forEach(function (field) {
                    /** @type {?} */
                    var control = formGroup.get(field);
                    if (control instanceof forms.FormControl) {
                        control.markAsTouched({ onlySelf: true });
                    }
                    else if (control instanceof forms.FormGroup) {
                        _this.validateForm(control);
                    }
                    else if (control instanceof forms.FormArray) {
                        Object.keys(control.controls).forEach(function (c) {
                            /** @type {?} */
                            var ctrl = control.get(c);
                            ctrl.markAsTouched({ onlySelf: true });
                        });
                    }
                });
                return formGroup.valid;
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.revertData = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                    .subscribe(function (res) {
                    if (res && res.value) {
                        _this.isDataChanged = false;
                        _this.deletedData = [];
                        _this.data = _this.originalRowData.map(function (item) { return Object.assign({}, item); });
                        _this.gridApi.setRowData(_this.data);
                        _this.toastr.success(ToastrMessages.RecordReverted);
                        _this.recordEventsSubject.next({ type: 'RevertedGrid', record: _this.data });
                    }
                });
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.addRow = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.gridOptions.editable) {
                    /** @type {?} */
                    var addedObject_1 = { Id: -1, OperationType: OperationType.Created };
                    this.gridOptions.columns.filter(function (x) { return x.field !== 'Id'; }).forEach(function (element) {
                        addedObject_1[element.field] = element.defaultValue;
                    });
                    this.data.unshift(addedObject_1);
                    this.isDataChanged = true;
                    // this.gridApi.updateRowData({
                    //   add: [addedObject],
                    //   addIndex: 0
                    // });
                    this.gridApi.setRowData(this.data);
                    this.refreshFormControls();
                    this.recordEventsSubject.next({ type: 'Add', record: addedObject_1 });
                }
                else {
                    this.ngZone.run(function () {
                        if (_this.gridOptions.addUrl) {
                            _this.router.navigate([_this.gridOptions.addUrl]);
                        }
                        else {
                            _this.router.navigate(['new'], { relativeTo: _this.route });
                        }
                    });
                }
            };
        /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
        CoreGridService.prototype.editRecord = /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
            function (params, rowId, key) {
                var _this = this;
                if (this.gridOptions.editUrlBlank) {
                    if (this.gridOptions.editUrl) {
                        window.open(window.location.origin + '/' + this.gridOptions.editUrl + '/' + params.data[this.gridOptions.keyField]);
                    }
                    else {
                        window.open(this.router.url + '/' + 'edit/' + params.data[this.gridOptions.keyField]);
                    }
                }
                else {
                    this.ngZone.run(function () {
                        if (_this.gridOptions.editUrl) {
                            _this.router.navigate([_this.gridOptions.editUrl, params.data[_this.gridOptions.keyField]]);
                        }
                        else {
                            _this.router.navigate(['edit', params.data[_this.gridOptions.keyField]], { relativeTo: _this.route });
                        }
                        // this.handleBreadcrumbLabel(params.data);
                    });
                }
            };
        // handleBreadcrumbLabel(rowData: any): void {
        //   if (this.gridOptions &&
        //     this.gridOptions.breadcrumbLabel &&
        //     rowData &&
        //     this.gridOptions.breadcrumbLabel &&
        //     this.gridOptions.keyField &&
        //     rowData[this.gridOptions.keyField]) {
        //     let editUrl: string;
        //     const keyFieldValue = rowData[this.gridOptions.keyField];
        //     if (this.gridOptions.editUrl) {
        //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
        //     } else {
        //       editUrl = this.router.url + '/edit/' + keyFieldValue;
        //     }
        //     let customLabel = '';
        //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
        //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
        //       if (foundColumn && foundColumn.colDef) {
        //         if (foundColumn.colDef.cellEditorParams &&
        //           foundColumn.colDef.cellEditorParams.dataSource &&
        //           foundColumn.colDef.cellEditorParams.settings) {
        //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
        //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
        //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
        //           customLabel += foundData[labelField] + ' ';
        //         } else {
        //           customLabel += rowData[columnField] + ' ';
        //         }
        //       }
        //     });
        //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
        //   }
        // }
        // handleBreadcrumbLabel(rowData: any): void {
        //   if (this.gridOptions &&
        //     this.gridOptions.breadcrumbLabel &&
        //     rowData &&
        //     this.gridOptions.breadcrumbLabel &&
        //     this.gridOptions.keyField &&
        //     rowData[this.gridOptions.keyField]) {
        //     let editUrl: string;
        //     const keyFieldValue = rowData[this.gridOptions.keyField];
        //     if (this.gridOptions.editUrl) {
        //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
        //     } else {
        //       editUrl = this.router.url + '/edit/' + keyFieldValue;
        //     }
        //     let customLabel = '';
        //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
        //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
        //       if (foundColumn && foundColumn.colDef) {
        //         if (foundColumn.colDef.cellEditorParams &&
        //           foundColumn.colDef.cellEditorParams.dataSource &&
        //           foundColumn.colDef.cellEditorParams.settings) {
        //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
        //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
        //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
        //           customLabel += foundData[labelField] + ' ';
        //         } else {
        //           customLabel += rowData[columnField] + ' ';
        //         }
        //       }
        //     });
        //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
        //   }
        // }
        /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
        CoreGridService.prototype.undoRecord =
            // handleBreadcrumbLabel(rowData: any): void {
            //   if (this.gridOptions &&
            //     this.gridOptions.breadcrumbLabel &&
            //     rowData &&
            //     this.gridOptions.breadcrumbLabel &&
            //     this.gridOptions.keyField &&
            //     rowData[this.gridOptions.keyField]) {
            //     let editUrl: string;
            //     const keyFieldValue = rowData[this.gridOptions.keyField];
            //     if (this.gridOptions.editUrl) {
            //       editUrl = this.gridOptions.editUrl + '/' + keyFieldValue;
            //     } else {
            //       editUrl = this.router.url + '/edit/' + keyFieldValue;
            //     }
            //     let customLabel = '';
            //     this.gridOptions.breadcrumbLabel.forEach((columnField: string) => {
            //       const foundColumn = this.allGridColumns.find(x => x.colId === columnField);
            //       if (foundColumn && foundColumn.colDef) {
            //         if (foundColumn.colDef.cellEditorParams &&
            //           foundColumn.colDef.cellEditorParams.dataSource &&
            //           foundColumn.colDef.cellEditorParams.settings) {
            //           const labelField = foundColumn.colDef.cellEditorParams.settings.labelField;
            //           const valueFiled = foundColumn.colDef.cellEditorParams.settings.valueField;
            //           const foundData = foundColumn.colDef.cellEditorParams.dataSource.find(y => y[valueFiled] === rowData[columnField]);
            //           customLabel += foundData[labelField] + ' ';
            //         } else {
            //           customLabel += rowData[columnField] + ' ';
            //         }
            //       }
            //     });
            //     this.breadcrumbService.addCustomLabel(keyFieldValue.toString(), editUrl, customLabel);
            //   }
            // }
            /**
             * @param {?} params
             * @param {?} rowId
             * @param {?} key
             * @return {?}
             */
            function (params, rowId, key) {
                var _this = this;
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                    .subscribe(function (result) {
                    if (result.value) {
                        /** @type {?} */
                        var revertedObject_1 = _this.data[rowId];
                        /** @type {?} */
                        var orginalObject = _this.originalRowData
                            .find(function (x) { return revertedObject_1 && x[_this.gridOptions.keyField] === revertedObject_1[_this.gridOptions.keyField]; });
                        _this.data[rowId] = orginalObject ? JSON.parse(JSON.stringify(orginalObject)) : null;
                        params.node.data = _this.data[rowId];
                        _this.gridApi.redrawRows({ rowNodes: [params.node] });
                        _this.gridApi.refreshCells({ rowNodes: [params.node], force: true });
                        params.context.formGroup.controls[rowId].markAsPristine();
                    }
                });
            };
        /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
        CoreGridService.prototype.deleteRecord = /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
            function (params, rowId, key) {
                var _this = this;
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
                    .subscribe(function (result) {
                    if (result.value) {
                        /** @type {?} */
                        var deleteUrl = _this.gridOptions.deleteEndPointUrl ? _this.gridOptions.deleteEndPointUrl : _this.urlOptions.endPointUrl;
                        /** @type {?} */
                        var reqUrl = "" + _this.urlOptions.moduleUrl + deleteUrl;
                        _this.dataService.delete(_this.data[rowId][_this.gridOptions.keyField], reqUrl)
                            .subscribe(function (res) {
                            _this.recordEventsSubject.next({ type: 'Delete', record: _this.data[rowId] });
                            // const indexOfData = this.data.indexOf(params.data);
                            if (_this.pagingResult.TotalCount > 10) {
                                _this.refreshAndRevertChanges();
                            }
                            else {
                                _this.data.splice(rowId, 1);
                                if (_this.pagingResult.TotalCount && _this.pagingResult.TotalCount > 0) {
                                    _this.pagingResult.TotalCount--;
                                }
                                _this.gridApi.setRowData(_this.data);
                            }
                            _this.toastr.success(ToastrMessages.RecordDeleted);
                        });
                    }
                    else {
                        _this.sweetAlertService.confirmOperationPopup(result);
                    }
                });
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.callUndoConfirmAlert = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var bulkData = this.getBulkOperationData();
                if (bulkData && bulkData.length > 0) {
                    return this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo);
                }
                else {
                    return rxjs.of({ value: true });
                }
            };
        /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
        CoreGridService.prototype.removeRow = /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
            function (params, rowId, key) {
                var _this = this;
                this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Delete)
                    .subscribe(function (res) {
                    if (res && res.value) {
                        // this.data[rowId] = Object.assign({}, this.originalRowData[rowId]);
                        /** @type {?} */
                        var deletedObject_1 = _this.data[rowId];
                        _this.data.splice(rowId, 1);
                        _this.recordEventsSubject.next({ type: 'Delete', record: deletedObject_1 });
                        if (params.data.OperationType !== OperationType.Created) {
                            /** @type {?} */
                            var orginalDeletedObject = _this.originalRowData
                                .find(function (x) { return x[_this.gridOptions.keyField] === deletedObject_1[_this.gridOptions.keyField]; });
                            if (orginalDeletedObject) {
                                orginalDeletedObject['OperationType'] = OperationType.Deleted;
                                _this.deletedData.push(orginalDeletedObject);
                            }
                            else {
                                deletedObject_1['OperationType'] = OperationType.Deleted;
                                _this.deletedData.push(deletedObject_1);
                            }
                        }
                        _this.isDataChanged = true;
                        _this.gridApi.setRowData(_this.data);
                        // if (!this.gridOptions.editable) {
                        //   this.toastr.success(ToastrMessages.RecordDeleted);
                        // }
                    }
                });
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.refreshAndRevertChanges = /**
         * @return {?}
         */
            function () {
                this.deletedData = [];
                this.isDataChanged = false;
                this.refreshGridData();
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.isChanged = /**
         * @return {?}
         */
            function () {
                return this.isDataChanged;
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getTotalCount = /**
         * @return {?}
         */
            function () {
                return this.pagingResult.TotalCount - this.deletedData.length;
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getNoRowsOverlayComponent = /**
         * @return {?}
         */
            function () {
                return 'customNoRowsOverlay';
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getLoadingOverlayComponent = /**
         * @return {?}
         */
            function () {
                return 'customLoadingOverlay';
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.getSelectedRows = /**
         * @return {?}
         */
            function () {
                return this.gridApi.getSelectedRows();
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.loadingOverlay = /**
         * @return {?}
         */
            function () {
                if (this.gridApi) {
                    if (this.loadingDisplayStatus) {
                        this.gridApi.showLoadingOverlay();
                    }
                    else {
                        this.gridApi.hideOverlay();
                        if (!this.data || (this.data && this.data.length === 0)) {
                            this.gridApi.showNoRowsOverlay();
                        }
                    }
                }
            };
        /**
         * @return {?}
         */
        CoreGridService.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.loadingDisplayStatusSubscription) {
                    this.loadingDisplayStatusSubscription.unsubscribe();
                }
            };
        CoreGridService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        CoreGridService.ctorParameters = function () {
            return [
                { type: DataService },
                { type: ToastrUtilsService },
                { type: GridLoadingService },
                { type: SweetAlertService },
                { type: i0.NgZone },
                { type: i5.Router },
                { type: i5.ActivatedRoute }
            ];
        };
        /** @nocollapse */ CoreGridService.ngInjectableDef = i0.defineInjectable({ factory: function CoreGridService_Factory() { return new CoreGridService(i0.inject(DataService), i0.inject(ToastrUtilsService), i0.inject(GridLoadingService), i0.inject(SweetAlertService), i0.inject(i0.NgZone), i0.inject(i5.Router), i0.inject(i5.ActivatedRoute)); }, token: CoreGridService, providedIn: "root" });
        return CoreGridService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreGridComponent = /** @class */ (function () {
        function CoreGridComponent(gridService, sweetAlertService) {
            var _this = this;
            this.gridService = gridService;
            this.sweetAlertService = sweetAlertService;
            this.loadOnInit = true;
            this.selectedChanged = new i0.EventEmitter();
            this.doubleClickSelectedChanged = new i0.EventEmitter();
            this.recordEvents = new i0.EventEmitter();
            this.retrievedDataSourceEvent = new i0.EventEmitter();
            this.paginationSelectorSource = [];
            this.paginationSelectorSource = [
                { Id: 1, Definition: 1 },
                { Id: 5, Definition: 5 },
                { Id: 10, Definition: 10 },
                { Id: 25, Definition: 25 },
                { Id: 50, Definition: 50 }
            ];
            this.gridStyle = {
                'height': '500px',
                'margin-top': '-1px'
            };
            this.recordEventsSubscription = this.gridService.recordEventsSubject
                .subscribe(function (data) {
                _this.recordEvents.emit(data);
            });
            this.retrievedDataSourceEventSubscription = this.gridService.retrievedDataSourceSubject
                .subscribe(function (data) {
                _this.retrievedDataSourceEvent.emit(data);
            });
        }
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.gridService.gridOptions = this.gridOptions;
                this.gridService.init(this.gridOptions, this.loadOnInit, this.loadColumnDataSourceOnInit);
            };
        /**
         * @param {?} changes
         * @return {?}
         */
        CoreGridComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.isInsideModal && this.isInsideModal) {
                    /** @type {?} */
                    var height = window.innerHeight * 60 / 100;
                    this.gridStyle = {
                        'height': height > 500 ? '500px' : height + 'px',
                        'margin-top': '-1px'
                    };
                }
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.load = /**
         * @return {?}
         */
            function () {
                this.gridService.init(this.gridOptions, true);
            };
        /**
         * @param {?} columnName
         * @param {?} dataSource
         * @return {?}
         */
        CoreGridComponent.prototype.setColumnDataSource = /**
         * @param {?} columnName
         * @param {?} dataSource
         * @return {?}
         */
            function (columnName, dataSource) {
                this.gridService.setColumnDataSource(columnName, dataSource);
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.refreshGridDataSource = /**
         * @return {?}
         */
            function () {
                this.gridService.refreshGridData(true);
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.reloadColumnDefs = /**
         * @return {?}
         */
            function () {
                this.gridService.reloadColumnDefs();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.isDataUpdated = /**
         * @return {?}
         */
            function () {
                return true;
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.addRow = /**
         * @return {?}
         */
            function () {
                this.gridService.addRow();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.revertDataSource = /**
         * @return {?}
         */
            function () {
                this.gridService.revertData();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.getBulkOperationData = /**
         * @return {?}
         */
            function () {
                return this.gridService.getBulkOperationData();
            };
        /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
        CoreGridComponent.prototype.removeRow = /**
         * @param {?} params
         * @param {?} rowId
         * @param {?} key
         * @return {?}
         */
            function (params, rowId, key) {
                this.gridService.removeRow(params, rowId, key);
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.refreshAndRevertChanges = /**
         * @return {?}
         */
            function () {
                this.gridService.refreshAndRevertChanges();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.validateForm = /**
         * @return {?}
         */
            function () {
                return this.gridService.validateForm();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.showAddButton = /**
         * @return {?}
         */
            function () {
                if (this.gridOptions && this.gridOptions.buttonSettings) {
                    if (this.gridOptions.buttonSettings.showAddButton !== undefined) {
                        return this.gridOptions.buttonSettings.showAddButton;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.showReloadButton = /**
         * @return {?}
         */
            function () {
                if (this.gridOptions && this.gridOptions.buttonSettings) {
                    if (this.gridOptions.buttonSettings.showReloadButton !== undefined) {
                        return this.gridOptions.buttonSettings.showReloadButton;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.showRevertButton = /**
         * @return {?}
         */
            function () {
                return this.isChanged() && this.isEditable();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.isEditable = /**
         * @return {?}
         */
            function () {
                return this.gridOptions && this.gridOptions.editable;
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.isChanged = /**
         * @return {?}
         */
            function () {
                return this.gridService.isChanged();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.getTotalCount = /**
         * @return {?}
         */
            function () {
                return this.gridService.getTotalCount();
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridComponent.prototype.onSelectionChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                /** @type {?} */
                var selectedRows = this.gridService.getSelectedRows();
                this.selectedChanged.emit(selectedRows);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridComponent.prototype.onRowDoubleClick = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                /** @type {?} */
                var selectedRows = this.gridService.getSelectedRows();
                this.doubleClickSelectedChanged.emit(selectedRows);
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.getSelectedRows = /**
         * @return {?}
         */
            function () {
                return this.gridService.getSelectedRows();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.getSelectedRow = /**
         * @return {?}
         */
            function () {
                return _.head(this.gridService.getSelectedRows());
            };
        /**
         * @param {?} data
         * @return {?}
         */
        CoreGridComponent.prototype.onFirstValue = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                if (data.Id) {
                    this.prevItemsPerPage = data.Id;
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridComponent.prototype.onChangeItemsPerPage = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                var _this = this;
                if (this.gridOptions && this.gridOptions.editable) {
                    /** @type {?} */
                    var bulkData = this.getBulkOperationData();
                    if (bulkData && bulkData.length > 0) {
                        this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                            .subscribe(function (res) {
                            if (res && res.value) {
                                _this.gridService.onChangeItemsPerPage(event);
                                _this.prevItemsPerPage = event.Id;
                            }
                            else {
                                _this.gridService.pagingResult.PageSize = _this.prevItemsPerPage;
                            }
                        });
                    }
                    else {
                        this.gridService.onChangeItemsPerPage(event);
                        this.prevItemsPerPage = event.Id;
                    }
                }
                else {
                    this.gridService.onChangeItemsPerPage(event);
                    this.prevItemsPerPage = event.Id;
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreGridComponent.prototype.onPageChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                var _this = this;
                if (event.page !== this.currentPage) {
                    if (this.gridOptions && this.gridOptions.editable) {
                        /** @type {?} */
                        var bulkData = this.getBulkOperationData();
                        if (bulkData && bulkData.length > 0) {
                            this.sweetAlertService.manageRequestOperationWithConfirm(ConfirmDialogOperationType.Undo)
                                .subscribe(function (res) {
                                if (res && res.value) {
                                    _this.gridService.onPageChanged(event);
                                }
                                else {
                                    _this.currentPage = _this.gridService.pagingResult.CurrentPage;
                                }
                            });
                        }
                        else {
                            this.gridService.onPageChanged(event);
                        }
                    }
                    else {
                        this.gridService.onPageChanged(event);
                    }
                }
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.getColumnState = /**
         * @return {?}
         */
            function () {
                this.gridService.getColumnState();
            };
        /**
         * @return {?}
         */
        CoreGridComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.recordEventsSubscription) {
                    this.recordEventsSubscription.unsubscribe();
                }
                if (this.retrievedDataSourceEventSubscription) {
                    this.retrievedDataSourceEventSubscription.unsubscribe();
                }
            };
        CoreGridComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-core-grid',
                        template: "<div class=\"core-grid\">\r\n  <div class=\"core-grid-header\">\r\n    <h5>{{gridService.gridOptions.title | translate}}</h5>\r\n\r\n    <button *ngIf=\"showReloadButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\"{{'Refresh' | translate}}\"\r\n            (click)=\"refreshGridDataSource()\">\r\n      <core-icon icon=\"sync\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showAddButton()\" type=\"button\" class=\"btn btn-sm btn-info\" title=\"{{'Add' | translate}}\"\r\n            (click)=\"addRow()\">\r\n      <core-icon icon=\"plus\"></core-icon>\r\n    </button>\r\n\r\n    <button *ngIf=\"showRevertButton()\" type=\"button\" class=\"btn btn-sm btn-default\" title=\" {{'Revert' | translate}}\"\r\n            (click)=\"revertDataSource()\">\r\n      <core-icon icon=\"undo\"></core-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n  <!-- suppressColumnVirtualisation = Set to true so that the grid doesn't virtualise the columns.\r\n  So if you have 100 columns, but only 10 visible due to scrolling, all 100 will always be rendered. -->\r\n\r\n  <ag-grid-angular #coreGrid [ngStyle]=\"gridStyle\" class=\"ag-theme-balham\" [rowHeight]=\"40\"\r\n                   [floatingFiltersHeight]=\"40\" [suppressRowTransform]=\"true\" [sortingOrder]=\"['asc', 'desc']\"\r\n                   [sortable]=\"gridService.gridOptions.enableSorting\" [filter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowBuffer]=\"50\" [suppressColumnVirtualisation]=\"true\" [suppressDragLeaveHidesColumns]=\"true\"\r\n                   [ensureDomOrder]=\"true\" [rowData]=\"gridService.data\" [columnDefs]=\"gridService.columnDefs\"\r\n                   [accentedSort]=\"true\" [floatingFilter]=\"gridService.gridOptions.enableFilter\"\r\n                   [rowSelection]=\"gridService.gridOptions.rowSelection\" [context]=\"gridService.getContext(gridService)\"\r\n                   [frameworkComponents]=\"gridService.getComponents()\"\r\n                   [noRowsOverlayComponent]=\"gridService.getNoRowsOverlayComponent()\"\r\n                   [loadingOverlayComponent]=\"gridService.getLoadingOverlayComponent()\"\r\n                   (rowDataChanged)=\"gridService.refreshFormControls()\" (gridReady)=\"gridService.onGridReady($event)\"\r\n                   (columnEverythingChanged)=\"gridService.onColumnEverythingChanged($event)\"\r\n                   (gridSizeChanged)=\"gridService.onGridSizeChanged()\"\r\n                   (cellValueChanged)=\"gridService.onCellValueChanged($event)\"\r\n                   (filterChanged)=\"gridService.onFilterChanged($event)\" (selectionChanged)=\"onSelectionChanged($event)\"\r\n                   (rowDoubleClicked)=\"onRowDoubleClick($event)\">\r\n  </ag-grid-angular>\r\n\r\n  <!-- (sortChanged)=\"gridService.onSortChanged($event)\"  -->\r\n\r\n  <div #inputRef class=\"under-grid-footer\">\r\n    <ng-content select=\"[footer]\"></ng-content>\r\n  </div>\r\n\r\n  <div *ngIf=\"gridService && gridService.pagingResult && (gridService.pagingResult.PageSize || gridService.pagingResult.PageSize === 0)\"\r\n       class=\"core-grid-pager\">\r\n    <div>\r\n      <layout-static-selector class=\"pull-left grid-static-layout-selector\" [dataSource]=\"paginationSelectorSource\"\r\n                              [searchable]=\"false\" [clearable]=\"false\" [(ngModel)]=\"gridService.pagingResult.PageSize\"\r\n                              (changed)=\"onChangeItemsPerPage($event)\" (firstValue)=\"onFirstValue($event)\"\r\n                              ngDefaultControl>\r\n      </layout-static-selector>\r\n\r\n      <pagination class=\"grid-pagination\" [totalItems]=\"gridService.pagingResult.TotalCount\"\r\n                  [itemsPerPage]=\"gridService.pagingResult.PageSize\" [(ngModel)]=\"currentPage\" [maxSize]=\"5\"\r\n                  [rotate]=\"true\" [boundaryLinks]=\"true\" previousText=\"{{'PreviousePage'|translate}}\"\r\n                  nextText=\"{{'NextPage'|translate}}\" firstText=\"{{'FirstPage'|translate}}\"\r\n                  lastText=\"{{'LastPage'|translate}}\" (pageChanged)=\"onPageChanged($event)\">\r\n      </pagination>\r\n\r\n    </div>\r\n\r\n    <div>\r\n      <span> <b>{{'TotalRowCount'|translate}}: </b>{{gridService.pagingResult.TotalCount}}</span>\r\n    </div>\r\n  </div>\r\n</div>",
                        encapsulation: i0.ViewEncapsulation.None,
                        providers: [GridLoadingService, CoreGridService],
                        styles: [".ag-cell{overflow:visible!important}.core-grid-header{font-size:12px!important;background-color:#166dba!important;border:none!important;padding:5px;height:37px;border-radius:2px 2px 0 0}.core-grid-header h5{float:left;margin-top:4px;color:#fff;font-size:14px;padding-left:5px}.btn-sm{float:right;margin-left:8px}.ag-header-cell{background-color:#fff!important;color:#175169!important;font-weight:700!important}.ag-cell,.ag-floating-filter-body{padding-top:1px}.ag-cell input,.ag-floating-filter-body input{height:30px!important;margin-top:3px}.ag-floating-filter-body input{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;height:30px!important;margin-top:4px}.ag-cell ng-select input,.ag-floating-filter-body ng-select input{height:19px!important}.ag-floating-filter-button{padding-top:3px}.core-grid-pager{background:#fff;display:flex;align-items:center;overflow:hidden;font-size:14px;margin-top:-1px;border:1px solid #bdc3c7;justify-content:space-between;padding:5px 12px}.grid-pagination{padding:0;float:left;margin-bottom:0}.grid-static-layout-selector{margin-right:12px;margin-bottom:2px}.ag-floating-filter-button{display:none}.grid-filter-icon{position:absolute;right:-22px;top:2px}.under-grid-footer{background-color:#fff;border-right:1px solid #d7dfe3;border-left:1px solid #d7dfe3;border-top:0}.grid-pagination .page-item.active .page-link{background-color:#166dba!important;border-color:#166dba!important}"]
                    }] }
        ];
        /** @nocollapse */
        CoreGridComponent.ctorParameters = function () {
            return [
                { type: CoreGridService },
                { type: SweetAlertService }
            ];
        };
        CoreGridComponent.propDecorators = {
            gridOptions: [{ type: i0.Input }],
            loadOnInit: [{ type: i0.Input }],
            selectedChanged: [{ type: i0.Output }],
            doubleClickSelectedChanged: [{ type: i0.Output }],
            recordEvents: [{ type: i0.Output }],
            retrievedDataSourceEvent: [{ type: i0.Output }],
            loadColumnDataSourceOnInit: [{ type: i0.Input }],
            isInsideModal: [{ type: i0.Input }]
        };
        return CoreGridComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreModalComponent = /** @class */ (function () {
        function CoreModalComponent(coreModalService) {
            this.coreModalService = coreModalService;
            this.lockBackground = true;
            this.closeOnEscape = true;
            this.maximizable = false;
            this.draggable = true;
            this.closable = true;
            this.showHeader = true;
            this.width = 50;
            this.modalWidth = (window.screen.width) * this.width / 100;
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        CoreModalComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.displayData) {
                    if (this.displayData && this.displayData.display) {
                        this._display = true;
                    }
                    else {
                        this._display = false;
                    }
                }
                if (changes.width) {
                    if (this.width) {
                        this.modalWidth = (window.screen.width) * this.width / 100;
                    }
                    else {
                        this.modalWidth = null;
                    }
                }
            };
        /**
         * @return {?}
         */
        CoreModalComponent.prototype.openModal = /**
         * @return {?}
         */
            function () {
                this.displayData = {
                    display: true
                };
                this._display = true;
            };
        /**
         * @return {?}
         */
        CoreModalComponent.prototype.closeModal = /**
         * @return {?}
         */
            function () {
                this.displayData = {
                    display: false
                };
                this._display = false;
            };
        /**
         * @return {?}
         */
        CoreModalComponent.prototype.onDialogShow = /**
         * @return {?}
         */
            function () {
                this.coreModalService.setModalDisplayStatus(true);
            };
        /**
         * @return {?}
         */
        CoreModalComponent.prototype.onDialogHide = /**
         * @return {?}
         */
            function () {
                this.coreModalService.setModalDisplayStatus(false);
            };
        CoreModalComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'core-modal',
                        template: "<p-dialog appendTo=\"body\" focusOnShow=\"false\" [(visible)]=\"_display\" [(modal)]=\"lockBackground\"\r\n  [(maximizable)]=\"maximizable\" [(draggable)]=\"draggable\" [(closable)]=\"closable\" [(showHeader)]=\"showHeader\"\r\n  [(closeOnEscape)]=\"closeOnEscape\" [autoZIndex]=\"false\" [blockScroll]=\"true\" [baseZIndex]=\"1001\" [width]=\"modalWidth\"\r\n  (onShow)=\"onDialogShow()\" (onHide)=\"onDialogHide()\">\r\n  <p-header *ngIf=\"modalTitle\">\r\n    {{modalTitle | translate}}\r\n  </p-header>\r\n  <div>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</p-dialog>",
                        styles: [".active-item:hover{z-index:2;color:#fff;background-color:#007bff;border-color:#007bff;cursor:pointer}::ng-deep .ui-dialog .ui-dialog-titlebar{background:#2d5f8b!important;color:#fff!important;font-weight:700!important;font-size:18px!important;padding:10px 15px!important;cursor:pointer!important;border:none!important}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:not(:hover){color:#fff!important;margin-top:2px}::ng-deep .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-icon:hover{margin-top:2px}::ng-deep .ui-dialog .ui-dialog-content{padding:30px!important}::ng-deep .ui-dialog{z-index:1001}::ng-deep .ui-widget-overlay{z-index:1000}:host /deep/ .ng-dropdown-panel.ng-select-bottom{position:fixed!important}"]
                    }] }
        ];
        /** @nocollapse */
        CoreModalComponent.ctorParameters = function () {
            return [
                { type: CoreModalService }
            ];
        };
        CoreModalComponent.propDecorators = {
            modalTitle: [{ type: i0.Input }],
            lockBackground: [{ type: i0.Input }],
            closeOnEscape: [{ type: i0.Input }],
            maximizable: [{ type: i0.Input }],
            draggable: [{ type: i0.Input }],
            closable: [{ type: i0.Input }],
            showHeader: [{ type: i0.Input }],
            displayData: [{ type: i0.Input }],
            width: [{ type: i0.Input }]
        };
        return CoreModalComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // @Inject(DOCUMENT) private document: Document
    var CoreOrgChartComponent = /** @class */ (function () {
        function CoreOrgChartComponent(ngZone) {
            this.ngZone = ngZone;
            this.clickedExport = new i0.EventEmitter();
        }
        /**
         * @param {?} simpleChanges
         * @return {?}
         */
        CoreOrgChartComponent.prototype.ngOnChanges = /**
         * @param {?} simpleChanges
         * @return {?}
         */
            function (simpleChanges) {
                if (simpleChanges.export && this.export) {
                    this.onExport();
                }
            };
        /**
         * @return {?}
         */
        CoreOrgChartComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.datasource = {
                    name: 'A',
                    title: 'Yönetim Kurulu Başkanı',
                    className: 'org-node-blue',
                    gender: 'M',
                    office: 'Ankara',
                    imgUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAvCAYAAABOmTCPAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAEmJJREFUeJztnQtUU1e6gK3WKgVR3oS8ICQQEgLhaZAY5SUvBQGFKmqtiIooUh+oKFqtU2d0zbTT1lerrk7vmtVqOzNt1207Tqe3qzO3M7Vzrd65szq9vY7kvPMgBAStWvXcvU8IQnJyEsAAsfxr7RUXyb/3f/79nX+//nOcNGlCJmRCHgipLTxEyNSfEgnpv2cruET1KZmqvdC5tknnbVuun3rd31hU/htkRoQZi074FyaWsxe+jKCy87c56mMiVQIWLf8O/Kbdpa5AhhLSpHYiRZvlqG9pPRCPy1P+jvGlqCt9NCoWxWVJ7abyJWXe9gdVvDQfF8gITBjn8npwsRzDoxVfYGJFlLft8SkhYhP/jIYIaSw8mrWggZE0Lk6gO5atXu5tW3qOnQg05pVcQZ4IorEoKY3xYtlLmJgmM3RHHPWR8Og0lBfjWg8WcE24UE4TiowCR31ry/5kXKK8C+t3pY+GiGgAFG0sKVvlbX8YcxZVMf0QwX1NaKSkE42IEXvbHp8S4JQLIALaQGIpaIiAxmNVdEdtXYW3bek5fjLAmF/6V2RaMM1lE+xsMnPe8476SGSsGkTUOy71YAGQ4KIEmlBm5jjqW3fuV+GxiT0MTK78EQqgj06gjaXly7ztD0NeWRkDdaTE9fVAm3ixGChCb9vjU4LwJJ9wOi24H+xKb9vykMD+YQLsCZlEZuo+w4KFLkGaAHsCbJ8Uo27Bn9BZggmwJ02A/UgJlamzLR4nwPZlsNEJsB2ESM76AoW7AOMA7N4TJwMB2N95BHaG7iVH/ZGC3dmyLxWATeMegG0qLVvjbX9QmvwaBmoOsHEb2LeRSEmMt+3xKSHU2QBs0bgAuxtEbEN+yZv6aUH/jfKllwCk7CVcfIWcnbvJUX+kYHft3h8HwP5PJFx82VXbSJjoMhot/5oqqSz1tj+My1bngfauoBHRX7u0JyoW2Cr9BOXLeN62x6cEl6d+yTn0jiLYUIj8Ur9r04Nn6gVSfz0/lr1EiGYYsoumOuqOFOwbew9OxqQq/2vhogCXbYcLA5DohACsqGaKt31hffkXk/XBUYH6cLFLe65FSWaggjg/MlrxmLft8SnBpaqvmHkcF9jSJLqjspYVbGJuwePEvAV+hDbXj8wueHy07R8oIwV7Qh4IpcmdbK7bwDM1bk0xrducbV6/Kd3U0urxqGAsr5lmfbY1xlD9dKa5EeivbUw1Vq8K96bNgwSXJbkHO0ZJW5av6QfbUFIZb6zbsI7KLngVT9Z8hKdoLuJJmReJJM2HBl3hcUNpdZOxqlZjrm+YNhRbbr569gmwYFrS7jdrCyqUNqKCWPYSKd6szyrSOOqPFGxz66EQTJZUj0aINrlqG0TsTWiMvNFYUhXnyTUZ122aQWbqVIZFNSuMRVVtVMa8X1KFC08bK6rP4uqs49T8wkNUbvEqQ/lSpzmyde/hGCRUsBmJELv2BV+yGUTs1YhIMcNRv+vgIb5l+y5tx47dGtby7HadZc++tO4LFwL67a1aLuh4um4dmZL9OzBSk5gw/g4mkN0Dnz9gsYkkqc1701zX4HRq26+/ZEUaVVy5n0jK/DNYi1gxYdxdTAj0BXF3QIBEyPmFZ81bd+R64rsRCSqQ/RfG41hxz+LTwKgfeo68OK/zhZ8qiaycX+PihOvozEgaDQLfhYL5eYjQdtQMFqHwb8zfeRKazM77jMwpWmNpevYJT2zpOX4CLh6/9WxXRPeio/7Id0X2pcDFo2e7ImV1XNdCli0XE3Ny28is3C/x6IS7cOcJDYpi/IXO5NFoQAT4N/BXsJBGZvFoImXONx0NzaGD6phTUOPhrsgtlGXxSGpy2tBw0B4vhrUgAWGg3SxL1yuvhnbUN042Ln6qDZMmIkhgJBPQ+o/zYfsRElv/QtuDBLShuOLgwLaMzS0CQ27pafD7u4x+qBDoRD/QhZ99KRoIqJssXnzGsn1PgKPND03QyJhLnCDA3AqR/IaxsOxjXKKkEL9QALLI9h2Ej61EgRIOHOcfwegTaXO+ImtWuR36H5XtPqK4fCMmS9ZjoAMZgOGuE4e/UNgeX9Zh1BUP2rIb6T62IX1eG7xxsLBo1oL4h9OkOktv3b6rFE/M+JABclbUA1vZ2oP2ArCRJ8Npy/rG3bCd7qNHs1FxwjUU1IeGRQ/ggE0/juFHP3UmvDl+8/35cx4FvSELypNwg81nPu8j8E6F0ZvLaBYnQEigA5FZgu+tO1pXc9nyKIBtqd/4cyQwgolsGExS8sBfEGww7GPmhdWDMvRGDLYmtxXlmGYyRSTvxWJV3f2JcJ70rTCeRgLB9cUoblp3tu4kU7Pa0SfDuG+IgUUQB/wootunBNLWHS173EI6HAFz7Ctcc+z+AjvJE5hZC7jLgSOQgMj7vadPVruyxdfBtjbvaNBPC2Eimsc3vx1sSSJmLl06umDDoAXqhpD134Se9mlf0IJTUGZU4A+RCQg3GB1QUXzX9b37ZJ4T66EQiRn/QD29U0dS7Hd5eEx3z7HjKjZbfBnsnpdf5ZGJ6RQyPYTptKH4ZszAfhhlJAEP+AmO5pY161qGTq4bIRRp/zMqYPfBrQfQGhcs/OyHP/7eaR/Yl8E2121cq/cLGVpHM/nUA6Yivgj2SAozXxfSZNb8PwyPXg4ZVbAZKMGiEixSrAcOVTna4stgG6trTyDTPIjWsDPhzsgMMA+HOyVgOEb8w+AC3QzAHrRP/MiDDQu4Nlyi/HZ49HIIEaf+BsI2ZLCddkI814XDD5Vb+ImjLb4KNpGR/TguV3+AhbkJEHxb+2DBdoeau+B9SpPzAjU37wyePPsyIVZgFm3hoC07nwPbfu1DmW+DUQvc1O3DJ9iF4NKkfzL7jJ4aDp0MFxtw4QAXSXDhACNQqNjji4G7K3hcUk/nrlb1QFt8FWxMkTIVi1X+O7NvG+VmBwREafPKZ3YMbNeyfsss85aWLP1zh4MG/n1UwYaPmMFtSeZMQkh7zITdBnhTQz24mIR1QEag3e6Cng3sa8Nhl1NwmYdgwyG0b4Mej0tuJ9LnHrfUb9xqqlm5G1dnvYtLVT0IBD3CffRn6gE3RMe6DYMSmXwVbAaigoVn4JSC2afluvZgAW1tafEogWpUwIZ+Br+BfQLm+RYiWfNXIjH9G2gnVzrzIH3oEzilyNS9ZVq/eaX5mfV1aKzyU2b/3h3cYwp2X6SGHQfmkr80b93plDNg2bRV0x4i+htzYsVxkmm/GLjBbyyrOjuwDl8Gu2P9pia3i0cYHMBoReUV/an35DG3SVSjAjbc7gM2k6rM8x2NzQnMtezZP920uq4VE8qYE1N3QQqMVHese/YPemuAZc8+f+PCqj8g00PHKdh98yUIG4jQr3DVdf3s6TAqO+8qnENzOhM6ZAaPptLnft65paW/g3tOnPJZsHt/e04OAOlmrp1rAcmT2La4Vq05465vvL+PLaMRsIglNPP/YWna7nQCaF5e+y7iF8Z9s4KRF6wv2i07WwOd/Hn4aBU82GOmruMPbHDx4OJwVcbVO19+EeSuPutPD9cgwYK7zIKUq87ASJpM116xNG/vr9OXwYbS2dj0QvtjMx4cK7uK2iAKwlM3y8bmw1y+HI2IjQbzaTJF+z5b+6blzzQxbHAlycF+lqmuGgoKZzrqm5c/rcOi5beYvJFRBztG+Z07x+mnh9CmiprXPKmv98yZIEKRhrsbwrAQMKeLT/6OKCgS9Oueet2nwb715edPUEVlH+unzOQ+noYHE2BKon98Jt21/9B2V74cFbBBNCWUsy+wtY/Hp61nroEjYjM3sUx1zVReEeLUH0Hi+UD/BucuidfAFidc5TIcfgc7k9TltXpSX2fbwamkOusSyjX8wKgVEE4Tadnt5m27+re4ek++5tNgQ+l6+XgokTr3PPJkKM1A5WpaAo+UYTJRYOT9ntPHWBeTowI2CECESsMKNljgb+T0pRuw0fDYHLDeuslZh9fAFiX8ixNsUJCACLjQc0oTZZOuX70RQKg1V5ltQC6wYWZZWrbeumOXxK7b8wiAbRfjqrW7QVS+z2yJcsEdGEXjArmx9+Rpp7TTsQYbkygaUOZ3w4vYaKRsDMGWKP+P03FwPgxAIzQ5l25dueg2xfD7U6fyQH230TDufW2UWXSk/K+ppJxv132UwIZiKCpbhYvj76Eh3KMXPLE0Lq0976Q/xmBTGu065neRvgh2gvrvnNl9cH4Ej8Gnh9Ldx17a7K6+juLy/9DDLR43215wNU6mzf26s3mXv13X1xePrP6oW/dvcI3CBSeM6mDqdrv76ItzBuqONdiGovJ622kph+3jFWwyc+6HTEThPAq25TeAC7zZuXtfPVs9XceOhxpKK34NgXWbBgv3sacH06bSqncH1tFz7MQjB7b5mboyNFzMvUsEzwjANZuqV54cqDvWYHc0PbvGdjDH4Y/xCra5uPIIkxTvLnnHvtgBhUjN/oTUFjR0bN6abVpVX0DOyTtAJGX+sz8yCdwn1iMBkbS1oal5oC2+DLb54pXJbP4FgaMB+O6eu0MwJn8mc97l3heO9D8uNdZgU4uXrkT50vuc233jFezOnburmTxprsbtxZ6ZNosHFjzgRohW3MCjFbdtz7LBOjxLVoe/xaWq611HX1QMtMWXTx57P74wuWPDljYiXfscVV5d2HnwULZhyYq1WIyCsNXHvUDvexuA2bywMtFe51iDjYjkNeA3dzn1xyvYN869G4GJlRSEdUgJ8jC/AHayPR/AUz0YncAc3FhWdc7RFl8GG4q58qkL7ZMDmCNq5rf294x7EjDASIjHJd+gKqv7X0g/1mCTReWV8EFuzvXBeAUbimVD40/0kwOHBOewSl8HgqlKb+9rp5Ic7fB5sJfWvqeHL62HYA8lOw76hYnYqm5qUWW6vb7ROaARujygAaPOIkyccJvrTGJcg3378wsRWFzyNf2UWUN+rGkoncc82Dsjku7e27aFzY5HAuypQcPyDXz6n8zQXun6+Ut+9vpGA2xkZhRNZeV8zNYfZMnifEwYd9Mnd0XsAiJoFXxsCx7GuFv8DbkwTyWLmSmIoaLmlCsbfrRgAzjgMXzXrtZBp7ujAjYINIa84o/Y+gNXpukACzc43zsz3sGG0rW3dTvchmNSTx9W5O6bfsAXpVDzik93NG1z+Rq0HyXYIIjA6Quhyrh069z5QYlEowU2Nb+QFezOfc9p8FhFD8Zx2OYTYEOxbNm6AQkT9zJvcxriopANIuYRe77sprF29T53bf+owO5LLkJgzow689tbH33g9AqCUQEbjNBULnvEtu7ck4LFKLvR8EcAbCjGhi1ZRELqV3CIRMEcbNB7JzjSMZlPmLgOn76wv+YsXfvHzpa9c9y3+jDAlowp2KbF1Rc8WoQz26Zi5i1RhCLt4u0P3pOy+cPrYIM+Zd4aUPkUa9oqKs9QgpHbig0zuw+NkBZ48tAJLpSbXDHx0KVjxVp/Y1HlZjxp9mVMGH8X9Q+zPVkdLLQ9NcE8ORHT97os2zv74LDGPDomSbQSyZrPDEtqa4bSZs8rxwIN8wra9ZOetO2X9z1/51RAG4Q6y+mhB/3MqEwkhO9ar++pcLgYwqXJJY761m17UsFiyfZ+Ohf6zDVGSWjDgmKnE9iOlWve0T8WwCR4wYDg8uaEIxlP0mMsWnzMWLbUZY67QVu4jBnxgrivCb4LDwniS5z0U7U/Q6C+K12gp5/kR5sqqv/C1r6pfqMCF8Uz00iXbQcCX4nivjeWLAxz1Ed48flolLQXBJt74PMOa+FJ7oGIjbjygdfEsnn7FNPSlYVUec1REqyecblaj0kSb+NiOQM1/D8gAcjXcdXsK+TcgreoqmXbTPWb0obTVvcbb/oZl654HhPL3yFS5rwN4GUviRnnDMWVTjcNkqqLBt+/5VIPFlXmOTJd9zaVU5roqG9+/oiYnJ3zBqzflT6uTD9HZuje7qhdo3XU72o7oCSyC/aQugXHSbXmsqtjdObxsNwi1ig5UIzL1mZAe4mk2a6vR605h6vnnMBTtKGO+tSi6goc6rvSTda8DcB9x7x2I+sLa3rfez+cml/0Oi5Pce2PxMzzVFbOy50rnnZ626telBiE8KW5CF9SjPBj81kLL6YQi1V4NKJ7VQz5pSJCmaGm8hdqjA3NWtCJmcDxSkNFbfBY2zaexHLwJ/XMCzwdh3GY324D2ymbb0ImZNyL5fCRjcxawRXYOYW/G2sbJ2RChiyWfQebEban1h+A/duxtnFCJmTIYmk70ML8n/D2XQ17gQtHsOCi5i1gPcaekMHy/wuA7v+MZ1yPAAAAAElFTkSuQmCC',
                    children: []
                };
                /** @type {?} */
                var nodeTemplate = function (data) {
                    /** @type {?} */
                    var office = "";
                    /** @type {?} */
                    var title = "<div class=\"title\">" + data.name + "</div>";
                    /** @type {?} */
                    var content = "<div class=\"content\" title=\"" + data.title + "\"> " + data.title + " </div>";
                    if (data.gender) {
                        if (data.gender === 'F') {
                            content = "\n          <div class=\"content\">\n          <img src=\"./assets/images/user-female.png\" class=\"org-node-pic\"> " + data.title + "\n          </div>";
                        }
                        else {
                            content = "\n          <div class=\"content\">\n          <img src=\"./assets/images/user.png\" class=\"org-node-pic\"> " + data.title + "\n          </div>";
                        }
                    }
                    if (data.imgUrl) {
                        content = "\n        <div class=\"content\">\n        <img src=\"" + data.imgUrl + "\" class=\"org-node-pic\"> " + data.title + "\n        </div>";
                    }
                    if (data.office) {
                        office = "<span class=\"office\">" + data.office + "</span>";
                    }
                    /** @type {?} */
                    var template = '';
                    template += office;
                    template += title;
                    template += content;
                    return template;
                };
                // dragable false çünkü ekrandaki order bozuluyor sapıtıyor
                this.orgChart = $('.chart-container').orgchart({
                    data: this.datasource,
                    nodeTemplate: nodeTemplate,
                    verticalLevel: 5,
                    visibleLevel: 4,
                    nodeContent: 'title',
                    exportButton: false,
                    // exportFilename: 'MyOrgChart',
                    // exportFileextension: 'png',
                    draggable: false,
                    pan: false,
                    zoom: false,
                });
            };
        /**
         * @param {?} orgChart
         * @param {?} datasource
         * @return {?}
         */
        CoreOrgChartComponent.prototype.refreshOrgChart = /**
         * @param {?} orgChart
         * @param {?} datasource
         * @return {?}
         */
            function (orgChart, datasource) {
                // $('.chart-container').empty();
                orgChart.init({ data: JSON.parse(datasource) });
            };
        /**
         * @param {?} event
         * @return {?}
         */
        CoreOrgChartComponent.prototype.fileChange = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                /** @type {?} */
                var fileList = event.target.files;
                if (fileList.length > 0) {
                    /** @type {?} */
                    var file = fileList[0];
                    /** @type {?} */
                    var formData = new FormData();
                    formData.append('uploadFile', file, file.name);
                    if (file) {
                        this.readFile(file, this.refreshOrgChart, this.orgChart);
                    }
                }
            };
        /**
         * @param {?} file
         * @param {?} refreshOrgChart
         * @param {?} orgChart
         * @return {?}
         */
        CoreOrgChartComponent.prototype.readFile = /**
         * @param {?} file
         * @param {?} refreshOrgChart
         * @param {?} orgChart
         * @return {?}
         */
            function (file, refreshOrgChart, orgChart) {
                var _this = this;
                /** @type {?} */
                var reader = new FileReader();
                reader.readAsText(file, 'UTF-8');
                reader.onload = function (evt) {
                    /** @type {?} */
                    var datasource = (( /** @type {?} */((evt.target)))).result;
                    _this.datasource = datasource;
                    refreshOrgChart(orgChart, datasource);
                };
                reader.onerror = function (evt) {
                    console.log('error reading file');
                };
            };
        /**
         * @param {?} zoomType
         * @return {?}
         */
        CoreOrgChartComponent.prototype.onClickZoom = /**
         * @param {?} zoomType
         * @return {?}
         */
            function (zoomType) {
                console.log(zoomType);
                console.log(this.orgChart);
                /** @type {?} */
                var currentZoom = parseFloat($('.orgchart').css('zoom'));
                this.orgChart.setChartScale(this.orgChart.$chart, zoomType === '+' ? currentZoom += 0.2 : currentZoom -= 0.2);
            };
        /**
         * @return {?}
         */
        CoreOrgChartComponent.prototype.onClickExport = /**
         * @return {?}
         */
            function () {
                this.clickedExport.emit({ data: true });
            };
        /**
         * @return {?}
         */
        CoreOrgChartComponent.prototype.onExport = /**
         * @return {?}
         */
            function () {
                this.orgChart.export('organization-schema', 'png');
            };
        /**
         * @return {?}
         */
        CoreOrgChartComponent.prototype.onClickResetAll = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.ngZone.run(function () {
                    $('.orgchart').css('transform', ''); // remove the tansform settings
                    _this.orgChart.init({ data: _this.datasource });
                });
            };
        CoreOrgChartComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'core-org-chart',
                        template: "<input id=\"file-upload\" type=\"file\" (change)=\"fileChange($event)\" placeholder=\"Upload file\" accept=\".json\">\r\n\r\n\r\n\r\n<div class='chart-container'>\r\n</div>\r\n\r\n<div class=\"fixed-operation-bar\">\r\n\r\n  <!-- <label for=\"file-upload\" class=\"btn btn-primary   fl-l\">\r\n    <core-icon class=\"main-icon\" icon=\"upload\"></core-icon>\r\n    {{ 'FileUpload' | translate}}\r\n  </label> -->\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickResetAll()\" class=\"btn btn-danger ml-10 fl-r\" title=\"{{'Reset'| translate}}\">\r\n    <core-icon icon=\"times\"></core-icon> {{'ResetAll' | translate}}\r\n  </button> -->\r\n\r\n  <button type=\"button\" (click)=\"onClickExport()\" class=\"btn btn-primary ml-10 fl-r\" title=\"{{'Export'| translate}}\">\r\n    <core-icon icon=\"download\"></core-icon> {{'Export' | translate}}\r\n  </button>\r\n\r\n  <!-- <button type=\"button\" (click)=\"onClickZoom('-')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomOut'| translate}}\">\r\n    <core-icon icon=\"search-minus\"></core-icon> {{'ZoomOut' | translate}}\r\n  </button>\r\n\r\n  <button type=\"button\" (click)=\"onClickZoom('+')\" class=\"btn btn-success ml-10 fl-r\" title=\"{{'ZoomIn'| translate}}\">\r\n    <core-icon icon=\"search-plus\"></core-icon> {{'ZoomIn' | translate}}\r\n  </button> -->\r\n</div>",
                        styles: [".chart-container.canvasContainer{position:initial}:host /deep/ .orgchart{background:#fff}input[type=file]{display:none}.btn{cursor:pointer}:host /deep/ .chart-container.canvasContainer{position:initial}:host /deep/ .orgchart .node .content{height:auto;padding:5px}:host /deep/ .orgchart .node .content .org-node-pic{width:40px}:host /deep/ .orgchart .node{width:auto}:host /deep/ .orgchart .node .office{font-weight:600}:host /deep/ .orgchart .node .title{padding:1px 5px}:host /deep/ .orgchart .node .title .symbol{margin-top:2px;margin-left:0}:host /deep/ .orgchart .node .title .fa-group:before,:host /deep/ .orgchart .node .title .fa-users:before{padding-right:4px}.chart-container{zoom:1}:host /deep/ .org-node-blue .title{background-color:#007bff}:host /deep/ .org-node-blue .content{border-color:#007bff}:host /deep/ .org-node-blue-light .title{background-color:#e9f5ff}:host /deep/ .org-node-blue-light .content{border-color:#e9f5ff}:host /deep/ .org-node-blue-dark .title{background-color:#166dba}:host /deep/ .org-node-blue-dark .content{border-color:#166dba}:host /deep/ .org-node-blue-extra-dark .title{background-color:#15233d}:host /deep/ .org-node-blue-extra-dark .content{border-color:#15233d}:host /deep/ .org-node-purple .title{background-color:#8622cd}:host /deep/ .org-node-purple .content{border-color:#8622cd}:host /deep/ .org-node-pink .title{background-color:#e83e8c}:host /deep/ .org-node-pink .content{border-color:#e83e8c}:host /deep/ .org-node-red .title{background-color:#e30a3a}:host /deep/ .org-node-red .content{border-color:#e30a3a}:host /deep/ .org-node-orange .title{background-color:#fd7e14}:host /deep/ .org-node-orange .content{border-color:#fd7e14}:host /deep/ .org-node-orange-light .title{background-color:#ffb822}:host /deep/ .org-node-orange-light .content{border-color:#ffb822}:host /deep/ .org-node-yellow .title{background-color:#ffc107}:host /deep/ .org-node-yellow .content{border-color:#ffc107}:host /deep/ .org-node-yellow-light .title{background-color:#fff589}:host /deep/ .org-node-yellow-light .content{border-color:#fff589}:host /deep/ .org-node-green .title{background-color:#28a745}:host /deep/ .org-node-green .content{border-color:#28a745}:host /deep/ .org-node-indigo .title{background-color:#6610f2}:host /deep/ .org-node-indigo .content{border-color:#6610f2}:host /deep/ .org-node-teal .title{background-color:#20c997}:host /deep/ .org-node-teal .content{border-color:#20c997}:host /deep/ .org-node-cyan .title{background-color:#17a2b8}:host /deep/ .org-node-cyan .content{border-color:#17a2b8}:host /deep/ .org-node-gray-dark .title{background-color:#343a40}:host /deep/ .org-node-gray-dark .content{border-color:#343a40}"]
                    }] }
        ];
        /** @nocollapse */
        CoreOrgChartComponent.ctorParameters = function () {
            return [
                { type: i0.NgZone }
            ];
        };
        CoreOrgChartComponent.propDecorators = {
            export: [{ type: i0.Input }],
            clickedExport: [{ type: i0.Output }]
        };
        return CoreOrgChartComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BreadcrumbComponent = /** @class */ (function () {
        function BreadcrumbComponent(breadcrumbService, router, activatedRoute, ngZone) {
            var _this = this;
            this.breadcrumbService = breadcrumbService;
            this.router = router;
            this.activatedRoute = activatedRoute;
            this.ngZone = ngZone;
            // subscribe to the NavigationEnd event 
            this.routingSubscription = this.router.events.filter(function (event) { return event instanceof i5.NavigationEnd; })
                .subscribe(function (event) {
                // set breadcrumbs
                /** @type {?} */
                var root = _this.activatedRoute.root;
                _this.breadcrumbService.breadcrumbs = _this.getBreadcrumbs(root);
            });
        }
        /**
         * @private
         * @param {?} route
         * @param {?=} url
         * @param {?=} breadcrumbs
         * @return {?}
         */
        BreadcrumbComponent.prototype.getBreadcrumbs = /**
         * @private
         * @param {?} route
         * @param {?=} url
         * @param {?=} breadcrumbs
         * @return {?}
         */
            function (route, url, breadcrumbs) {
                if (url === void 0) {
                    url = '';
                }
                if (breadcrumbs === void 0) {
                    breadcrumbs = [];
                }
                var e_1, _a;
                /** @type {?} */
                var ROUTE_DATA_BREADCRUMB = 'breadcrumb';
                // get the child routes
                /** @type {?} */
                var children = route.children;
                // return if there are no more children
                if (children.length === 0) {
                    return breadcrumbs;
                }
                var _loop_1 = function (child) {
                    // verify primary route
                    if (child.outlet !== i5.PRIMARY_OUTLET) {
                        return "continue";
                    }
                    // verify the custom data property 'breadcrumb' is specified on the route
                    if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
                        return { value: this_1.getBreadcrumbs(child, url, breadcrumbs) };
                    }
                    // get the route's URL segment
                    /** @type {?} */
                    var routeURL = child.snapshot.url.map(function (segment) { return segment.path; }).join('/');
                    // append route URL to URL
                    url += "/" + routeURL;
                    /** @type {?} */
                    var currentUrl = this_1.router.url;
                    // /organization/operations/company
                    /** @type {?} */
                    var indexUrl = currentUrl.indexOf(url);
                    /** @type {?} */
                    var parentUrl = null;
                    if (indexUrl !== -1) {
                        if (indexUrl === 0 && url === '/') {
                            currentUrl = currentUrl.substring(1);
                            /** @type {?} */
                            var secondIndex = currentUrl.indexOf('/');
                            if (secondIndex !== -1) {
                                parentUrl = currentUrl.substring(0, secondIndex);
                            }
                        }
                        else {
                            parentUrl = currentUrl.substring(0, indexUrl);
                        }
                    }
                    url = url.replace('//', parentUrl ? parentUrl + '/' : '/');
                    if (url === '/' && parentUrl) {
                        url = '/' + parentUrl;
                    }
                    // url = url.replace('//', this.mainRout ? '/' + this.mainRout + '/' : '/');
                    // if (url === '/' && this.mainRout) {
                    //   url = '/' + this.mainRout;
                    // }
                    // add breadcrumb
                    /** @type {?} */
                    var breadcrumb = {
                        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
                        url: this_1.getFullPath(child.snapshot),
                        customLabel: null
                    };
                    if (breadcrumbs.findIndex(function (x) { return x.label === breadcrumb.label && x.url === breadcrumb.url; }) === -1) {
                        breadcrumbs.push(breadcrumb);
                    }
                    if (child.snapshot.params && child.snapshot.params.id) {
                        /** @type {?} */
                        var moreBreadcrumb_1 = {
                            label: child.snapshot.params.id,
                            url: this_1.getFullPath(child.snapshot),
                            customLabel: null
                        };
                        // find from list and update moreBreadcrumb according to that
                        /** @type {?} */
                        var breadcrumbsWithCustomLabel = this_1.breadcrumbService.breadcrumbsWithCustomLabels.find(function (x) { return x.label === moreBreadcrumb_1.label && x.url === moreBreadcrumb_1.url; });
                        if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                            moreBreadcrumb_1['customLabel'] = breadcrumbsWithCustomLabel.customLabel;
                        }
                        /** @type {?} */
                        var foundBreadcrumb = breadcrumbs.find(function (x) { return x.label === moreBreadcrumb_1.label && x.url === moreBreadcrumb_1.url; });
                        // check exist if not push, if exist then maybe customLabel was changed and update it 
                        if (!foundBreadcrumb) {
                            breadcrumbs.push(moreBreadcrumb_1);
                        }
                        else if (breadcrumbsWithCustomLabel && breadcrumbsWithCustomLabel.customLabel) {
                            foundBreadcrumb.customLabel = breadcrumbsWithCustomLabel.customLabel;
                        }
                    }
                    return { value: this_1.getBreadcrumbs(child, url, breadcrumbs) };
                };
                var this_1 = this;
                try {
                    // iterate over each children
                    for (var children_1 = __values(children), children_1_1 = children_1.next(); !children_1_1.done; children_1_1 = children_1.next()) {
                        var child = children_1_1.value;
                        var state_1 = _loop_1(child);
                        if (typeof state_1 === "object")
                            return state_1.value;
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (children_1_1 && !children_1_1.done && (_a = children_1.return))
                            _a.call(children_1);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                // we should never get here, but just in case
                return breadcrumbs;
            };
        /**
         * @param {?} route
         * @return {?}
         */
        BreadcrumbComponent.prototype.getFullPath = /**
         * @param {?} route
         * @return {?}
         */
            function (route) {
                /** @type {?} */
                var relativePath = function (segments) { return segments.reduce(function (a, v) { return a += '/' + v.path; }, ''); };
                /** @type {?} */
                var fullPath = function (routes) { return routes.reduce(function (a, v) { return a += relativePath(v.url); }, ''); };
                return fullPath(route.pathFromRoot);
            };
        /**
         * @param {?} url
         * @return {?}
         */
        BreadcrumbComponent.prototype.onClick = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                var _this = this;
                this.ngZone.run(function () {
                    _this.router.navigate([url]);
                });
            };
        /**
         * @return {?}
         */
        BreadcrumbComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.routingSubscription) {
                    this.routingSubscription.unsubscribe();
                }
            };
        BreadcrumbComponent.decorators = [
            { type: i0.Component, args: [{
                        template: "\n  <ol *ngIf=\"breadcrumbService.breadcrumbs && breadcrumbService.breadcrumbs.length > 0\" class=\"breadcrumb\" [class.fixed-breadcrumb]=\"isFixed\">\n    <li *ngFor=\"let breadcrumb of breadcrumbService.breadcrumbs\" (click) =\"onClick(breadcrumb.url)\"  title=\"{{breadcrumb.label | translate}}\">\n      <a><span *ngIf=\"!breadcrumb.customLabel\">{{breadcrumb.label | translate}}</span><span *ngIf=\"breadcrumb.customLabel\">{{breadcrumb.customLabel | translate}}</span></a>\n    </li>\n  </ol>\n  ",
                        selector: 'breadcrumb',
                        styles: [".breadcrumb{margin-bottom:0;background-color:transparent;padding:0 0 0 2px;font-size:10px;font-weight:600}.breadcrumb li{cursor:pointer;color:#333!important}.breadcrumb li a{color:#333!important}.breadcrumb li:not(:last-child):after{content:'\\f054';font-family:FontAwesome;font-style:normal;font-weight:400;text-decoration:inherit;margin-left:10px;margin-right:10px;color:#455a64!important;position:relative;top:1px}.breadcrumb li:last-child{font-weight:600}.fixed-breadcrumb{height:35px;width:calc(100%);background-color:#fff;position:fixed;margin-left:-22px;z-index:49;top:45px;box-shadow:0 5px 5px -2px rgba(0,0,0,.2);padding-left:30px;padding-top:10px}"]
                    }] }
        ];
        /** @nocollapse */
        BreadcrumbComponent.ctorParameters = function () {
            return [
                { type: BreadcrumbService },
                { type: i5.Router },
                { type: i5.ActivatedRoute },
                { type: i0.NgZone }
            ];
        };
        BreadcrumbComponent.propDecorators = {
            isFixed: [{ type: i0.Input }]
        };
        return BreadcrumbComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormCheckboxComponent = /** @class */ (function () {
        function FormCheckboxComponent() {
            this.isDisabled = false;
            this.isReadOnly = false;
            this.hidden = false;
            this.checkChanged = new i0.EventEmitter();
        }
        /**
         * @param {?} val
         * @return {?}
         */
        FormCheckboxComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) { };
        /**
         * @return {?}
         */
        FormCheckboxComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () { };
        Object.defineProperty(FormCheckboxComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this.value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this.value = val;
                this.onChange(val);
                this.onTouched();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormCheckboxComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormCheckboxComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormCheckboxComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormCheckboxComponent.prototype.onCheckChanged = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.checkChanged.emit(event);
            };
        FormCheckboxComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-checkbox',
                        template: "\n\t<div class=\"form-checkbox\" [class.disable-selection]=\"isReadOnly\">\n    <p-checkbox\n    [hidden]=\"hidden\"\n    [(ngModel)]=\"inputValue\"\n    [disabled]=\"isDisabled\"\n    binary=\"true\"\n    (onChange)=\"onCheckChanged($event)\"\n    >\n    </p-checkbox>\n\t</div>\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormCheckboxComponent; }),
                                multi: true
                            }
                        ],
                        styles: [":host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-active{border-color:#2d5f8b;background-color:#2d5f8b}:host /deep/ .ui-chkbox .ui-chkbox-box.ui-state-focus{border-color:#2d5f8b;color:#2d5f8b}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-chkbox .ui-chkbox-box{opacity:.6;border:1px solid #a9a9a9!important;background-color:#ced4da!important;color:#000}"]
                    }] }
        ];
        FormCheckboxComponent.propDecorators = {
            value: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            hidden: [{ type: i0.Input }],
            checkChanged: [{ type: i0.Output }]
        };
        return FormCheckboxComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormTextInputComponent = /** @class */ (function () {
        function FormTextInputComponent() {
            this.isDisabled = false;
            this.isReadOnly = false;
            this.hidden = false;
            this.clearable = false;
            this.changed = new i0.EventEmitter();
            this.numberOnly = false;
        }
        /**
         * @param {?} val
         * @return {?}
         */
        FormTextInputComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormTextInputComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormTextInputComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._value = val;
                this.onChange(val);
                this.onTouched();
                this.changed.emit(this._value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormTextInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormTextInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormTextInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormTextInputComponent.prototype.onKeypress = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.disableSpace) {
                    if (event && event.code === 'Space') {
                        return false;
                    }
                }
                if (this.numberOnly) {
                    /** @type {?} */
                    var charCode = (event.which) ? event.which : event.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                }
                return true;
            };
        FormTextInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-text-input',
                        template: "\n  <div style=\"width:100%;\">\n    <input type=\"text\"\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           name=\"inputValue\"\n           autocomplete=\"off\"\n           [(ngModel)]=\"inputValue\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [attr.maxlength]=\"maxLength\"\n           placeholder=\"{{placeholder|translate}}\"\n           [hidden]=\"hidden\"\n           (keypress)=\"onKeypress($event)\"\n           >\n    <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormTextInputComponent; }),
                                multi: true
                            }
                        ],
                        styles: [""]
                    }] }
        ];
        FormTextInputComponent.propDecorators = {
            placeholder: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            hidden: [{ type: i0.Input }],
            clearable: [{ type: i0.Input }],
            disableSpace: [{ type: i0.Input }],
            maxLength: [{ type: i0.Input }],
            changed: [{ type: i0.Output }],
            numberOnly: [{ type: i0.Input }]
        };
        return FormTextInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormNumberInputComponent = /** @class */ (function () {
        function FormNumberInputComponent() {
            this.isDisabled = false;
            this.isReadOnly = false;
            this.hidden = false;
            this.clearable = false;
            this.changed = new i0.EventEmitter();
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        FormNumberInputComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.maxLength && this.maxLength) {
                    /** @type {?} */
                    var value = '';
                    for (var i = 0; i < this.maxLength; i++) {
                        value += '1';
                    }
                    this.maxValue = parseInt(value, 10) * 9;
                }
            };
        /**
         * @param {?} val
         * @return {?}
         */
        FormNumberInputComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormNumberInputComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormNumberInputComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._value = val;
                this.onChange(val);
                this.onTouched();
                this.changed.emit(this._value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormNumberInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormNumberInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormNumberInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormNumberInputComponent.prototype.onKeyUp = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.maxValue && this.maxLength && this.inputValue && this.inputValue > this.maxValue) {
                    this.inputValue = parseInt(this.inputValue.toString().slice(0, this.maxLength), 10);
                }
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormNumberInputComponent.prototype.numberOnly = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (event && event.code === 'KeyE') {
                    return false;
                }
                return true;
            };
        FormNumberInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-number-input',
                        template: "\n    <div style=\"width:100%;\">\n      <input type=\"number\"\n             class=\"form-control form-control-sm fixed-height-input form-input\"\n             name=\"inputValue\"\n             autocomplete=\"off\"\n             [(ngModel)]=\"inputValue\"\n             [disabled]=\"isDisabled\"\n             [readonly]=\"isReadOnly\"\n             [class.pr-25]=\"clearable && _value\"\n             (keyup)=\"onKeyUp($event)\"\n             (keypress)=\"numberOnly($event)\"\n              placeholder=\"{{placeholder|translate}}\"\n             [hidden]=\"hidden\">\n      <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n    </div>\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormNumberInputComponent; }),
                                multi: true
                            }
                        ],
                        styles: ["input::-webkit-inner-spin-button,input::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}"]
                    }] }
        ];
        FormNumberInputComponent.propDecorators = {
            placeholder: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            hidden: [{ type: i0.Input }],
            clearable: [{ type: i0.Input }],
            changed: [{ type: i0.Output }],
            maxLength: [{ type: i0.Input }]
        };
        return FormNumberInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormPasswordInputComponent = /** @class */ (function () {
        function FormPasswordInputComponent() {
        }
        /**
         * @param {?} val
         * @return {?}
         */
        FormPasswordInputComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormPasswordInputComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormPasswordInputComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._value = val;
                this.onChange(val);
                this.onTouched();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormPasswordInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormPasswordInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormPasswordInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        FormPasswordInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-password-input',
                        template: "\n    <input type=\"password\"\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           name=\"inputValue\"\n           placeholder=\"{{placeholder|translate}}\"\n           [(ngModel)]=\"inputValue\">\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormPasswordInputComponent; }),
                                multi: true
                            }
                        ]
                    }] }
        ];
        FormPasswordInputComponent.propDecorators = {
            placeholder: [{ type: i0.Input }]
        };
        return FormPasswordInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormCurrencyInputComponent = /** @class */ (function () {
        function FormCurrencyInputComponent() {
            this.isDisabled = false;
            this.isReadOnly = false;
            this.hidden = false;
            this.changed = new i0.EventEmitter();
            this.clearable = false;
        }
        /**
         * @param {?} val
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormCurrencyInputComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._value = val;
                this.onChange(val);
                this.onTouched();
                this.changed.emit(this._value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormCurrencyInputComponent.prototype.onKeyDown = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.isReadOnly) {
                    event.preventDefault();
                }
            };
        FormCurrencyInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-currency-input',
                        template: "\n  <div style=\"width:100%;\">\n    <input currencyMask\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           autocomplete=\"off\"\n           name=\"inputValue\"\n           [(ngModel)]=\"inputValue\"\n           (keydown)=\"onKeyDown($event)\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           placeholder=\"{{placeholder|translate}}\"\n           [class.readonly-input] = \"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [hidden]=\"hidden\">\n     <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormCurrencyInputComponent; }),
                                multi: true
                            }
                        ],
                        styles: [".readonly-input:focus{outline:0;box-shadow:none}"]
                    }] }
        ];
        FormCurrencyInputComponent.propDecorators = {
            placeholder: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            hidden: [{ type: i0.Input }],
            changed: [{ type: i0.Output }],
            clearable: [{ type: i0.Input }]
        };
        return FormCurrencyInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormTextAreaComponent = /** @class */ (function () {
        function FormTextAreaComponent() {
            this.isReadOnly = false;
        }
        /**
         * @param {?} val
         * @return {?}
         */
        FormTextAreaComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormTextAreaComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormTextAreaComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this._value = val;
                this.onChange(val);
                this.onTouched();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormTextAreaComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormTextAreaComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormTextAreaComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} event
         * @return {?}
         */
        FormTextAreaComponent.prototype.onKeyDown = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.isReadOnly) {
                    event.preventDefault();
                }
            };
        FormTextAreaComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-textarea',
                        template: "\n    <textarea name=\"inputValue\" class=\"form-textarea\"\n              id=\"inputValue\" style=\"min-width: 100%;\"\n              rows=\"5\"\n              [attr.maxlength]=\"maxLength\"\n              [class.readonly-input] = \"isReadOnly\"\n              [readonly]=\"isReadOnly\"\n              [(ngModel)]=\"inputValue\"\n              (keydown)=\"onKeyDown($event)\"\n              placeholder=\"{{placeholder|translate}}\"\n              ></textarea>\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormTextAreaComponent; }),
                                multi: true
                            }
                        ],
                        styles: [".readonly-input{background-color:#e9ecef;cursor:default}"]
                    }] }
        ];
        FormTextAreaComponent.propDecorators = {
            placeholder: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            maxLength: [{ type: i0.Input }]
        };
        return FormTextAreaComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormDatepickerInputComponent = /** @class */ (function () {
        function FormDatepickerInputComponent(fb, translate) {
            var _this = this;
            this.fb = fb;
            this.translate = translate;
            this.isReadOnly = false;
            this.dateChanges = new i0.EventEmitter();
            this._selectionMode = DateSelectionMode.Single;
            this.onChange = function () { };
            this.onTouched = function () { };
            this.view = 'date';
            this.yearDataSource = [];
            this.defaultSettings = new DateSettings();
            this.formGroup = this.fb.group({
                Year: null
            });
            // until the better solution for translate.
            this.localEn = {
                firstDayOfWeek: 1,
                dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear'
            };
            // until the better solution for translate.
            this.localeTr = {
                firstDayOfWeek: 1,
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
                dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                today: 'Bugün',
                clear: 'Temizle'
            };
            // until the better solution for translate.
            this.locale = this.localeTr;
            this.translateSubscription = this.translate.onLangChange.subscribe(function (event) {
                if (event) {
                    _this.locale = event.lang === 'tr' ? _this.localeTr : _this.localEn;
                }
            });
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.selectionMode && this.selectionMode) {
                    this._selectionMode = this.selectionMode;
                }
                if (changes.settings && this.settings) {
                    this.defaultSettings = _.assign({}, this.defaultSettings, this.settings);
                    if (this.settings.selectionMode && !this.selectionMode) {
                        this._selectionMode = this.settings.selectionMode;
                    }
                    if (this.settings.showYear) {
                        this.defaultSettings.dateFormat = 'yy';
                        this.initiateYearDataSource();
                    }
                    if (this.settings.showTimeOnly) {
                        this.defaultSettings.disableUTC = true;
                        this.defaultSettings.hideCalendarButton = true;
                        this.defaultSettings.showButtonBar = false;
                    }
                    if (this.settings.showTime) {
                        this.defaultSettings.disableUTC = true;
                    }
                    if (this.settings.showMonthPicker) {
                        this.view = 'month';
                    }
                    if (this.settings.minDate) {
                        this.minDate = this.convertDateToDateUTC(this.settings.minDate);
                    }
                    if (this.settings.maxDate) {
                        this.maxDate = this.convertDateToDateUTC(this.settings.maxDate);
                    }
                }
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (value) {
                    if (value.indexOf('T') !== -1 && !this.defaultSettings.disableUTC && !this.defaultSettings.showTimeOnly && !this.defaultSettings.showTime) {
                        // can be done via moment
                        /** @type {?} */
                        var splittedValue = value.split('T');
                        splittedValue[1] = '00:00:00.000Z';
                        value = splittedValue.join('T');
                    }
                    if (this.selectedDateString !== value) {
                        // due to UTC, without being influenced timezone changes, it creates date according to value
                        // in order to avoid reflecting timezone changes to UI
                        // console.log('value ', value);
                        // console.log('moment(value).toDate() ', moment(value).toDate());
                        // console.log('this.convertDateToDateUTC(value) ', this.convertDateToDateUTC(value));
                        if (!this.usedMometToDate && (this.defaultSettings.showTimeOnly || this.defaultSettings.showTime)) {
                            // first initiation needs to be done this way
                            this._calendarDateValue = moment(value).toDate();
                            this.usedMometToDate = true;
                        }
                        else {
                            // after the initiation needs to be used this way
                            this._calendarDateValue = this.convertDateToDateUTC(value);
                        }
                        this.formGroup.setValue({
                            Year: moment(value).format('YYYY')
                        });
                        this.selectedDateString = value;
                    }
                }
                else {
                    this._calendarDateValue = null;
                    this.selectedDateString = null;
                }
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        Object.defineProperty(FormDatepickerInputComponent.prototype, "selectedDate", {
            get: /**
             * @return {?}
             */ function () {
                return this.selectedDateString;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this.selectedDateString = value;
                // console.log('selectedDate', this.selectedDateString);
                this.onChange(this.selectedDateString);
                this.dateChanges.emit(this.selectedDateString);
                this.onTouched();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FormDatepickerInputComponent.prototype, "calendarDateValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._calendarDateValue;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._calendarDateValue = value;
                this.onChangeDate();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.onChangeDate = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this._selectionMode === DateSelectionMode.Range && this._calendarDateValue) {
                    /** @type {?} */
                    var dateRangeList = ( /** @type {?} */(( /** @type {?} */(this._calendarDateValue))));
                    /** @type {?} */
                    var newSelectedDateList_1 = [];
                    dateRangeList.forEach(function (date) {
                        if (date) {
                            newSelectedDateList_1.push(_this.convertDateToISO(date));
                        }
                    });
                    if (newSelectedDateList_1.length === 2) {
                        this.selectedDate = newSelectedDateList_1;
                    }
                }
                else {
                    if (this.selectedDateString && !this._calendarDateValue) {
                        this.selectedDate = null;
                    }
                    else if (this._calendarDateValue) {
                        if (this.defaultSettings.showYear) {
                            this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                            this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                        }
                        if (this.defaultSettings.showTimeOnly) {
                            this._calendarDateValue.setMonth(this.defaultSettings.defaultMonth);
                            this._calendarDateValue.setDate(this.defaultSettings.defaultDay);
                            this._calendarDateValue.setFullYear(this.defaultSettings.defaultYear);
                        }
                        if (!this.defaultSettings.enableSeconds && this._calendarDateValue.setSeconds) {
                            this._calendarDateValue.setSeconds(0);
                        }
                        /** @type {?} */
                        var calendarDateValueISO = this.convertDateToISO(this._calendarDateValue);
                        if (this.defaultSettings.disableUTC) {
                            calendarDateValueISO = moment(this._calendarDateValue).format('YYYY-MM-DDTHH:mm:ss');
                        }
                        if (calendarDateValueISO !== this.selectedDateString) {
                            this.selectedDate = calendarDateValueISO;
                        }
                    }
                }
            };
        /**
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.initiateYearDataSource = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var dataSource = [];
                for (var i = ((new Date()).getFullYear() + 30); i > 1922; i--) {
                    dataSource.push({
                        Value: i.toString(),
                        Label: i.toString()
                    });
                }
                dataSource.push({ Value: '2900', Label: '2900' });
                this.yearDataSource = dataSource;
            };
        /**
         * @param {?} selectedData
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.onYearValueChanged = /**
         * @param {?} selectedData
         * @return {?}
         */
            function (selectedData) {
                if (selectedData) {
                    /** @type {?} */
                    var month = (this.defaultSettings.defaultMonth + 1).toString();
                    if (month.length < 2) {
                        month = '0' + month;
                    }
                    /** @type {?} */
                    var day = (this.defaultSettings.defaultDay).toString();
                    if (day.length < 2) {
                        day = '0' + day;
                    }
                    this.selectedDate = selectedData['Value'] + '-' + month + '-' + day + 'T00:00:00.000Z';
                }
                else {
                    this.selectedDate = null;
                }
            };
        /**
         * @param {?} date
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.convertDateToISO = /**
         * @param {?} date
         * @return {?}
         */
            function (date) {
                /** @type {?} */
                var newDate = new Date(date);
                /** @type {?} */
                var year = newDate.getFullYear();
                /** @type {?} */
                var month = '' + (newDate.getMonth() + 1);
                /** @type {?} */
                var day = '' + newDate.getDate();
                if (month.length < 2) {
                    month = '0' + month;
                }
                if (day.length < 2) {
                    day = '0' + day;
                }
                return [year, month, day].join('-') + 'T00:00:00.000Z';
            };
        /**
         * @param {?} dateAsString
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.convertDateToDateUTC = /**
         * @param {?} dateAsString
         * @return {?}
         */
            function (dateAsString) {
                /** @type {?} */
                var newDate = new Date(dateAsString);
                return new Date(newDate.getUTCFullYear(), newDate.getUTCMonth(), newDate.getUTCDate(), newDate.getUTCHours(), newDate.getUTCMinutes(), newDate.getUTCSeconds());
            };
        /**
         * @return {?}
         */
        FormDatepickerInputComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.translateSubscription.unsubscribe();
            };
        FormDatepickerInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-datepicker-input',
                        template: "<div class=\"dp-container form-datepicker-input\" [class.disable-selection]=\"isReadOnly\"\r\n     [class.hidden-btn]=\"defaultSettings.hideCalendarButton\">\r\n  <p-calendar class=\"calendar-fit\" *ngIf=\"!defaultSettings.showYear\" [(ngModel)]=\"calendarDateValue\"\r\n              [dateFormat]=\"defaultSettings.dateFormat\" [timeOnly]=\"defaultSettings.showTimeOnly\"\r\n              [disabled]=\"isDisabled\" [showTime]=\"defaultSettings.showTime\"\r\n              [showIcon]=\"!defaultSettings.hideCalendarButton\" [showButtonBar]=\"defaultSettings.showButtonBar\"\r\n              [minDate]=\"minDate\" [maxDate]=\"maxDate\" [locale]=\"locale\" [view]=\"view\" [selectionMode]=\"_selectionMode\"\r\n              [style]=\"{'width':'100%', 'height': '30px', 'top': '-2px'}\" monthNavigator=\"true\" yearNavigator=\"true\"\r\n              yearRange=\"1930:2030\" placeholder=\"{{placeholder|translate}}\" appendTo=\"body\"></p-calendar>\r\n  <!--\r\n  <div [hidden]=\"hideCalendarButton\" class=\"dp-btn-container\">\r\n    <button class=\"btn btn-success dp-btn\" type=\"button\" (click)=\"dpStart.show(); $event.stopPropagation();\">\r\n      <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n    </button>\r\n  </div> -->\r\n</div>\r\n\r\n<div *ngIf=\"formGroup && defaultSettings.showYear\" [formGroup]=\"formGroup\" class=\"form-datepicker-input\"\r\n     [class.disable-selection]=\"isReadOnly\">\r\n  <layout-static-selector formControlName=\"Year\" [dataSource]=\"yearDataSource\" [valueField]=\"'Value'\"\r\n                          [labelField]=\"'Label'\" [isDisabled]=\"isDisabled\" [placeholder]=\"placeholder\"\r\n                          (changed)=\"onYearValueChanged($event)\"></layout-static-selector>\r\n</div>",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormDatepickerInputComponent; }),
                                multi: true
                            }
                        ],
                        styles: [".dp-btn-container,.dp-container{display:flex}.dp-input{height:26px!important;border-radius:3px 0 0 3px}.dp-btn{border-radius:0 3px 3px 0;border:none;height:26px}.bs-timepicker-field{height:30px!important;width:50px!important}:host /deep/ .ui-calendar .ui-inputtext{border:1px solid #ced4da;height:30px!important;font-size:inherit;width:calc(100% - 33px);border-right:1px solid #ced4da;padding-left:10px}:host /deep/ .ui-calendar .ui-calendar-button{height:30px;top:2px;background-color:#3594e8;border-color:#3594e8}:host /deep/ .ui-calendar .ui-inputtext:disabled{background-color:#ebebe4}:host /deep/ .ui-calendar .ui-state-disabled,:host /deep/ .ui-calendar .ui-widget:disabled{opacity:1}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext{width:calc(100%)}:host /deep/ .hidden-btn .ui-calendar .ui-inputtext:hover{border-right:1px solid #000!important}::ng-deep .ui-datepicker table td{padding:.2em!important;font-size:12px}::ng-deep .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}::ng-deep .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}.calendar-fit{width:calc(100%)}.disable-selection{outline:0;pointer-events:none}:host /deep/ .disable-selection .ui-calendar .ui-inputtext{background-color:#e9ecef}"]
                    }] }
        ];
        /** @nocollapse */
        FormDatepickerInputComponent.ctorParameters = function () {
            return [
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        FormDatepickerInputComponent.propDecorators = {
            settings: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            dateChanges: [{ type: i0.Output }],
            placeholder: [{ type: i0.Input }],
            selectionMode: [{ type: i0.Input }]
        };
        return FormDatepickerInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormMaskInputComponent = /** @class */ (function () {
        function FormMaskInputComponent() {
            this.isDisabled = false;
            this.isReadOnly = false;
            this.hidden = false;
            this.clearable = false;
            this.changed = new i0.EventEmitter();
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        FormMaskInputComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.maskType) {
                    if (this.maskType === MaskType.Phone) {
                        this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
                        this.placeholder = '(__) ___-____';
                    }
                    else if (this.maskType === MaskType.Mail) {
                        this.mask = emailMask;
                        // this.placeholder = 'john@smith.com';
                        this.placeholder = '';
                    }
                    else if (this.maskType === MaskType.PostCode) {
                        this.mask = [/\d/, /\d/, /\d/, /\d/, /\d/];
                        // this.placeholder = '38000';
                        this.placeholder = '';
                    }
                }
                if (changes.placeholder && changes.placeholder.currentValue) {
                    this.placeholder = changes.placeholder.currentValue;
                }
            };
        /**
         * @param {?} val
         * @return {?}
         */
        FormMaskInputComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        FormMaskInputComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormMaskInputComponent.prototype, "inputValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._value;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                if (val) {
                    if (this.maskType === MaskType.Phone) {
                        val = val.replace(/\D+/g, '');
                    }
                }
                this._value = val;
                this.onChange(val);
                this.onTouched();
                this.changed.emit(this._value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        FormMaskInputComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormMaskInputComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this._value = value;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        FormMaskInputComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        FormMaskInputComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-form-mask-input',
                        template: "\n\n  <div style=\"width:100%;\">\n    <input type=\"text\"\n           class=\"form-control form-control-sm fixed-height-input form-input\"\n           name=\"inputValue\"\n           autocomplete=\"off\"\n           type=\"text\"\n           [textMask]=\"{mask: mask}\"\n           [(ngModel)]=\"inputValue\"\n           [disabled]=\"isDisabled\"\n           [readonly]=\"isReadOnly\"\n           [class.pr-25]=\"clearable && _value\"\n           [hidden]=\"hidden\"\n           [placeholder]=\"placeholder\"\n           >\n    <core-icon icon=\"times\" *ngIf=\"clearable && _value\" class=\"searchclear\" (click)=\"inputValue = null\"></core-icon>\n  </div>\n",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return FormMaskInputComponent; }),
                                multi: true
                            }
                        ]
                    }] }
        ];
        /** @nocollapse */
        FormMaskInputComponent.ctorParameters = function () { return []; };
        FormMaskInputComponent.propDecorators = {
            maskType: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            hidden: [{ type: i0.Input }],
            placeholder: [{ type: i0.Input }],
            clearable: [{ type: i0.Input }],
            changed: [{ type: i0.Output }]
        };
        return FormMaskInputComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var components = [
        FormTextInputComponent,
        FormNumberInputComponent,
        FormPasswordInputComponent,
        FormCurrencyInputComponent,
        FormTextAreaComponent,
        FormCheckboxComponent,
        FormDatepickerInputComponent,
        FormMaskInputComponent
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ListOfValuesModalComponent = /** @class */ (function () {
        function ListOfValuesModalComponent() {
            this.getSelectedRow = new i0.EventEmitter();
        }
        Object.defineProperty(ListOfValuesModalComponent.prototype, "content", {
            set: /**
             * @param {?} component
             * @return {?}
             */ function (component) {
                // console.error('setContent', component);
                if (component) {
                    this.coreGrid = component;
                    this.gridOptions = this.displayData.gridOptions;
                    component.load();
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} changes
         * @return {?}
         */
        ListOfValuesModalComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.displayData && this.displayData && this.displayData.gridOptions) {
                    this.gridOptions = this.displayData.gridOptions;
                }
            };
        /**
         * @return {?}
         */
        ListOfValuesModalComponent.prototype.save = /**
         * @return {?}
         */
            function () {
                this.getSelectedRow.emit(this.coreGrid.getSelectedRow());
                this.displayData = { display: false };
            };
        /**
         * @return {?}
         */
        ListOfValuesModalComponent.prototype.close = /**
         * @return {?}
         */
            function () {
                this.displayData = { display: false };
            };
        /**
         * @param {?} selectedRows
         * @return {?}
         */
        ListOfValuesModalComponent.prototype.selectedChanged = /**
         * @param {?} selectedRows
         * @return {?}
         */
            function (selectedRows) {
                this.getSelectedRow.emit(_.head(selectedRows));
                this.displayData = { display: false };
            };
        /**
         * @return {?}
         */
        ListOfValuesModalComponent.prototype.gridReload = /**
         * @return {?}
         */
            function () {
                this.coreGrid.refreshGridDataSource();
            };
        ListOfValuesModalComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-list-of-values-modal',
                        template: "<core-modal [displayData]=\"displayData\" [draggable]=\"false\" [showHeader]=\"false\" [closeOnEscape]=\"false\"\r\n  [closable]=\"false\" [width]=\"65\">\r\n  <layout-core-grid *ngIf=\"displayData && displayData.display\" #coreGrid [gridOptions]=\"gridOptions\"\r\n    [isInsideModal]=\"true\" (doubleClickSelectedChanged)=\"selectedChanged($event)\">\r\n  </layout-core-grid>\r\n  <div class=\"col-sm-12 prl-0 mt-10\">\r\n    <button class=\"btn btn-danger ml-10 fl-r\" type=\"button\" (click)=\"close()\">\r\n      <core-icon icon=\"times\"></core-icon> {{'CancelButton'|translate}}\r\n    </button>\r\n\r\n    <button type=\"button\" class=\"btn btn-success ml-10 fl-r\" (click)=\"save()\">\r\n      <core-icon icon=\"check\"></core-icon> {{'ChooseButton' | translate}}\r\n    </button>\r\n  </div>\r\n</core-modal>",
                        styles: [".table-row-selection{cursor:pointer}.modal-pager{padding:20px 0!important;height:16px!important;display:flex;align-items:center;overflow:hidden;font-size:14px;justify-content:space-between;margin-bottom:10px}.modal-pagination{padding:0;margin-top:6px;margin-right:10px;float:left}.modal-static-layout-selector{margin-top:7px;float:left;margin-right:20px}"]
                    }] }
        ];
        /** @nocollapse */
        ListOfValuesModalComponent.ctorParameters = function () { return []; };
        ListOfValuesModalComponent.propDecorators = {
            displayData: [{ type: i0.Input }],
            getSelectedRow: [{ type: i0.Output }],
            content: [{ type: i0.ViewChild, args: ['coreGrid',] }]
        };
        return ListOfValuesModalComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ListOfValuesService = /** @class */ (function () {
        function ListOfValuesService(dataService) {
            this.dataService = dataService;
            this.lovServiceResultStateChanged = new rxjs.Subject();
        }
        /**
         * @param {?} urlOptions
         * @return {?}
         */
        ListOfValuesService.prototype.setLoadUrlSetting = /**
         * @param {?} urlOptions
         * @return {?}
         */
            function (urlOptions) {
                this.moduleUrl = urlOptions.moduleUrl;
                this.endPointUrl = urlOptions.endPointUrl;
            };
        /**
         * @param {?} expression
         * @param {?=} headerParameters
         * @return {?}
         */
        ListOfValuesService.prototype.loadDataByFilterAndPageSetting = /**
         * @param {?} expression
         * @param {?=} headerParameters
         * @return {?}
         */
            function (expression, headerParameters) {
                var _this = this;
                /** @type {?} */
                var reqUrl = "" + this.moduleUrl + this.endPointUrl;
                this.dataService.getListBy(reqUrl, expression, headerParameters)
                    .subscribe(function (httpResponse) {
                    _this.pagingResult = httpResponse.pagingResult;
                    _this.lovData = httpResponse.serviceResult.Result;
                    _this.lovServiceResultStateChanged.next(_this.lovData);
                });
            };
        ListOfValuesService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        ListOfValuesService.ctorParameters = function () {
            return [
                { type: DataService }
            ];
        };
        /** @nocollapse */ ListOfValuesService.ngInjectableDef = i0.defineInjectable({ factory: function ListOfValuesService_Factory() { return new ListOfValuesService(i0.inject(DataService)); }, token: ListOfValuesService, providedIn: "root" });
        return ListOfValuesService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ListOfValuesComponent = /** @class */ (function () {
        function ListOfValuesComponent(listOfValuesService) {
            this.listOfValuesService = listOfValuesService;
            this.isReadOnly = false;
            // @ContentChild(FormTextInputComponent) formTextInputComponent;
            this.lovValueChanged = new i0.EventEmitter();
            this.addToDataSource = new i0.EventEmitter();
        }
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.listOfValuesService.lovServiceResultStateChanged
                    .subscribe(function (result) {
                    _this.initialData = result;
                    _this.setLovFields();
                });
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.openModal = /**
         * @return {?}
         */
            function () {
                this.displayData = {
                    display: true,
                    gridOptions: this.gridOptions
                };
            };
        /**
         * @param {?} selectedModel
         * @return {?}
         */
        ListOfValuesComponent.prototype.getSelectedRow = /**
         * @param {?} selectedModel
         * @return {?}
         */
            function (selectedModel) {
                var _this = this;
                if (selectedModel) {
                    this.KeyColumnValue = selectedModel[this.gridOptions.keyField];
                    this.value = selectedModel[this.valueColumnName];
                    this.label = '';
                    if (this.labelColumnNames instanceof Array) {
                        /** @type {?} */
                        var labels = ( /** @type {?} */(this.labelColumnNames));
                        labels.forEach(function (columnName) {
                            _this.label = _this.label + ' ' + selectedModel[columnName];
                        });
                    }
                    else if (this.labelColumnNames) {
                        this.label = selectedModel[this.labelColumnNames.toString()];
                    }
                    // it will send the grid edit cell
                    this.addToDataSource.emit({
                        Id: this.listOfValuesService.endPointUrl,
                        Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
                    });
                    this.hasSelectedItem = true;
                    this.lovValueChanged.emit({ id: this.KeyColumnValue, value: this.value, label: this.label });
                }
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.gridReload = /**
         * @return {?}
         */
            function () {
                this.lovModal.gridReload();
            };
        /**
         * @param {?} gridOptions
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @param {?=} dontInitData
         * @return {?}
         */
        ListOfValuesComponent.prototype.setOptions = /**
         * @param {?} gridOptions
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @param {?=} dontInitData
         * @return {?}
         */
            function (gridOptions, valueColumnName, labelColumnNames, dontInitData) {
                if (valueColumnName === void 0) {
                    valueColumnName = 'Id';
                }
                if (labelColumnNames === void 0) {
                    labelColumnNames = 'Definition';
                }
                this.gridOptions = gridOptions;
                this.valueColumnName = valueColumnName;
                this.labelColumnNames = labelColumnNames;
                if (!dontInitData && this.keyColumnValue) {
                    this.initData();
                }
                else if (gridOptions) {
                    this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                }
            };
        /**
         * @param {?} gridOptions
         * @param {?=} initData
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @return {?}
         */
        ListOfValuesComponent.prototype.setGridOptions = /**
         * @param {?} gridOptions
         * @param {?=} initData
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @return {?}
         */
            function (gridOptions, initData, valueColumnName, labelColumnNames) {
                this.gridOptions = gridOptions;
                if (gridOptions) {
                    this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                    if (initData) {
                        this.initData();
                    }
                }
                if (valueColumnName) {
                    this.valueColumnName = valueColumnName;
                }
                if (labelColumnNames) {
                    this.labelColumnNames = labelColumnNames;
                }
            };
        /**
         * @param {?} gridOptions
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @return {?}
         */
        ListOfValuesComponent.prototype.setOptionsWithNoRequest = /**
         * @param {?} gridOptions
         * @param {?=} valueColumnName
         * @param {?=} labelColumnNames
         * @return {?}
         */
            function (gridOptions, valueColumnName, labelColumnNames) {
                if (valueColumnName === void 0) {
                    valueColumnName = 'Id';
                }
                if (labelColumnNames === void 0) {
                    labelColumnNames = 'Definition';
                }
                this.gridOptions = gridOptions;
                this.valueColumnName = valueColumnName;
                this.labelColumnNames = labelColumnNames;
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.initData = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.gridOptions && this.keyColumnValue && this.keyColumnValue !== -1) {
                    // console.log('initData - gridOptions', this.gridOptions);
                    // console.log('initData - dataSource', this.dataSource);
                    // console.log('initData - keyColumnValue', this.keyColumnValue);
                    if (this.dataSource) {
                        /** @type {?} */
                        var selectedRowData_1 = this.dataSource.find(function (x) {
                            return x.Id === _this.gridOptions.requestOptions.UrlOptions.endPointUrl
                                && x.Value.id === _this.keyColumnValue;
                        });
                        // console.log('initData - valueColumnName', this.valueColumnName);
                        // console.log('initData - keyColumnValue', this.keyColumnValue);
                        // console.log('initData - selectedRowData', selectedRowData);
                        if (selectedRowData_1) {
                            setTimeout(function () {
                                _this.label = selectedRowData_1.Value.label;
                                _this.value = selectedRowData_1.Value.value;
                            }, 10);
                            return;
                        }
                    }
                    if (this.gridOptions.requestOptions && this.gridOptions.requestOptions.UrlOptions) {
                        this.listOfValuesService.setLoadUrlSetting(this.gridOptions.requestOptions.UrlOptions);
                        this.fieldIniData = this.gridOptions.keyField;
                        if (this.gridOptions.requestOptions.CustomEndPoint === false || this.gridOptions.requestOptions.CustomEndPoint === undefined) {
                            if (this.fieldIniData.indexOf('Id') > 0) {
                                this.fieldIniData = this.fieldIniData.substr(0, this.fieldIniData.indexOf('Id')) + '.' + this.fieldIniData.substr(this.fieldIniData.indexOf('Id'), this.fieldIniData.length);
                            }
                        }
                        this.genericExpression = {
                            FilterGroups: [
                                {
                                    Filters: [
                                        { PropertyName: this.fieldIniData, Value: this.keyColumnValue, Comparison: TextComparison.EqualTo }
                                    ]
                                }
                            ],
                            PageSize: 1,
                            PageNumber: 1
                        };
                        if (this.gridOptions.requestOptions.CustomFilter) {
                            if (this.gridOptions.requestOptions.CustomFilter.FilterGroups) {
                                this.gridOptions.requestOptions.CustomFilter.FilterGroups.forEach(function (item) {
                                    _this.genericExpression.FilterGroups.push(item);
                                });
                            }
                        }
                        this.listOfValuesService.loadDataByFilterAndPageSetting(this.genericExpression, this.gridOptions.requestOptions.HeaderParameters);
                    }
                }
            };
        /**
         * @param {?=} disableSetToKeyColumnValue
         * @return {?}
         */
        ListOfValuesComponent.prototype.clearLov = /**
         * @param {?=} disableSetToKeyColumnValue
         * @return {?}
         */
            function (disableSetToKeyColumnValue) {
                if (!disableSetToKeyColumnValue) {
                    this.KeyColumnValue = null;
                }
                else {
                    this.keyColumnValue = null;
                }
                this.value = null;
                this.label = null;
                this.hasSelectedItem = false;
                // if (this.formTextInputComponent) {
                //   this.formTextInputComponent.inputValue = null;
                // }
                if (!disableSetToKeyColumnValue) {
                    this.lovValueChanged.emit(null);
                }
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.showClearButton = /**
         * @return {?}
         */
            function () {
                return !_.isNil(this.KeyColumnValue);
            };
        /**
         * @param {?} val
         * @return {?}
         */
        ListOfValuesComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(ListOfValuesComponent.prototype, "KeyColumnValue", {
            get: /**
             * @return {?}
             */ function () {
                return this.keyColumnValue;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this.keyColumnValue = val;
                this.setLovFields();
                this.onChange(val);
                this.onTouched();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        ListOfValuesComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        ListOfValuesComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                // console.log('writeValue1', value);
                // console.log('writeValue2', this.keyColumnValue);
                if (value) {
                    if (value !== this.keyColumnValue) {
                        this.keyColumnValue = value;
                        this.initData();
                    }
                }
                else {
                    this.keyColumnValue = value;
                    this.clearLov(true);
                }
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        ListOfValuesComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.setLovFields = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (!_.isNil(this.keyColumnValue) && !_.isNil(this.gridOptions)) {
                    /** @type {?} */
                    var keyColumn_1 = this.gridOptions.columns.find(function (item) { return item.field === _this.gridOptions.keyField; });
                    if (!_.isNil(this.initialData) && keyColumn_1 && keyColumn_1.field) {
                        /** @type {?} */
                        var filteredObject_1 = this.initialData.find(function (item) { return item[keyColumn_1.field] === _this.keyColumnValue; });
                        if (!_.isNil(filteredObject_1)) {
                            this.value = filteredObject_1[this.valueColumnName];
                            this.label = '';
                            this.freeText = '';
                            if (this.labelColumnNames instanceof Array) {
                                /** @type {?} */
                                var labels = ( /** @type {?} */(this.labelColumnNames));
                                labels.forEach(function (columnName) {
                                    _this.label = _this.label + ' ' + filteredObject_1[columnName];
                                    _this.freeText = _this.freeText + ' ' + filteredObject_1[columnName];
                                });
                            }
                            else if (this.labelColumnNames) {
                                this.label = filteredObject_1[this.labelColumnNames.toString()];
                                this.freeText = filteredObject_1[this.labelColumnNames.toString()];
                            }
                            // it will send the grid edit cell
                            this.addToDataSource.emit({
                                Id: this.listOfValuesService.endPointUrl,
                                Value: { id: this.KeyColumnValue, value: this.value, label: this.label }
                            });
                            this.hasSelectedItem = true;
                        }
                    }
                }
            };
        /**
         * @return {?}
         */
        ListOfValuesComponent.prototype.getInputFiledWidth = /**
         * @return {?}
         */
            function () {
                if (!this.isReadOnly) {
                    if (this.showClearButton()) {
                        return 'calc(60% - 76px)';
                    }
                    else {
                        return 'calc(60% - 36px)';
                    }
                }
                else {
                    return 'calc(60% )';
                }
            };
        ListOfValuesComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-list-of-values',
                        template: "<div class=\"form-lov\">\r\n  <layout-form-text-input [isReadOnly]=\"true\" [(ngModel)]=\"KeyColumnValue\" [hidden]=\"true\"></layout-form-text-input>\r\n  <div class=\"row no-gutters\">\r\n    <div class=\"pr-2\" style=\"width: 40%;\">\r\n      <layout-form-text-input [class]=\"isReadOnly ? '' : 'showLikeEditable'\" [isReadOnly]=\"true\" [(ngModel)]=\"value\">\r\n      </layout-form-text-input>\r\n    </div>\r\n    <div [class.pr-2]=\"!isReadOnly\" [ngStyle]=\"{'width': getInputFiledWidth()}\">\r\n      <layout-form-text-input *ngIf=\"!inputRef.innerHTML.trim() || (inputRef.innerHTML.trim() && hasSelectedItem)\"\r\n        [isReadOnly]=\"true\" [placeholder]=\"placeholder\" [class]=\"isReadOnly ? '' : 'showLikeEditable'\"\r\n        [(ngModel)]=\"label\"></layout-form-text-input>\r\n      <div #inputRef [hidden]=\"hasSelectedItem\">\r\n        <ng-content select=\"layout-form-text-input\"></ng-content>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"!isReadOnly\" class=\"lov-btn-container\" [ngStyle]=\"{'width': showClearButton() ? '76px' : '36px'}\">\r\n      <button type=\"button\" class=\"btn btn-info\" title=\" {{'Search' | translate}}\" (click)=\"openModal()\">\r\n        <core-icon icon=\"search\"></core-icon>\r\n      </button>\r\n      <button type=\"button\" [hidden]=\"!showClearButton()\" class=\"btn btn-danger ml-1\" title=\" {{'Clear' | translate}}\"\r\n        (click)=\"clearLov()\" style=\"float:right;\">\r\n        <core-icon icon=\"times\"></core-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n<layout-list-of-values-modal #lovModal [displayData]=\"displayData\" (getSelectedRow)=\"getSelectedRow($event)\">\r\n</layout-list-of-values-modal>",
                        encapsulation: i0.ViewEncapsulation.None,
                        providers: [
                            ListOfValuesService,
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return ListOfValuesComponent; }),
                                multi: true
                            }
                        ],
                        styles: [".showLikeEditable>input{background-color:#fff!important}"]
                    }] }
        ];
        /** @nocollapse */
        ListOfValuesComponent.ctorParameters = function () {
            return [
                { type: ListOfValuesService }
            ];
        };
        ListOfValuesComponent.propDecorators = {
            placeholder: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            dataSource: [{ type: i0.Input }],
            lovValueChanged: [{ type: i0.Output }],
            addToDataSource: [{ type: i0.Output }],
            lovModal: [{ type: i0.ViewChild, args: ['lovModal',] }]
        };
        return ListOfValuesComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SelectorService = /** @class */ (function () {
        function SelectorService(dataService) {
            this.dataService = dataService;
            this.listStateChanged = new rxjs.Subject();
            this.counter = 0;
        }
        /**
         * @param {?} urlOptions
         * @return {?}
         */
        SelectorService.prototype.setLoadUrlSetting = /**
         * @param {?} urlOptions
         * @return {?}
         */
            function (urlOptions) {
                this.moduleUrl = urlOptions.moduleUrl;
                this.endPointUrl = urlOptions.endPointUrl;
            };
        /**
         * @return {?}
         */
        SelectorService.prototype.getLoadUrlSetting = /**
         * @return {?}
         */
            function () {
                return ( /** @type {?} */({
                    moduleUrl: this.moduleUrl,
                    endPointUrl: this.endPointUrl
                }));
            };
        /**
         * @param {?} headerParameters
         * @return {?}
         */
        SelectorService.prototype.setHeaders = /**
         * @param {?} headerParameters
         * @return {?}
         */
            function (headerParameters) {
                this.headerParameters = headerParameters;
            };
        /**
         * @return {?}
         */
        SelectorService.prototype.getHeaders = /**
         * @return {?}
         */
            function () {
                return this.headerParameters;
            };
        /**
         * @param {?} expression
         * @return {?}
         */
        SelectorService.prototype.loadDataByFilterAndPageSetting = /**
         * @param {?} expression
         * @return {?}
         */
            function (expression) {
                var _this = this;
                /** @type {?} */
                var reqUrl = "" + this.moduleUrl + this.endPointUrl;
                this.dataService.getListBy(reqUrl, expression, this.headerParameters)
                    .subscribe(function (httpResponse) {
                    _this.listStateChanged.next(httpResponse.serviceResult.Result.slice());
                });
            };
        SelectorService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        SelectorService.ctorParameters = function () {
            return [
                { type: DataService }
            ];
        };
        /** @nocollapse */ SelectorService.ngInjectableDef = i0.defineInjectable({ factory: function SelectorService_Factory() { return new SelectorService(i0.inject(DataService)); }, token: SelectorService, providedIn: "root" });
        return SelectorService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SelectorComponent = /** @class */ (function () {
        function SelectorComponent(selectorService) {
            var _this = this;
            this.selectorService = selectorService;
            this.isDisabled = false;
            this.isReadOnly = false;
            this.searchable = true;
            this.clearable = true;
            this.changed = new i0.EventEmitter();
            // it is for getting the first selected value
            this.firstValue = new i0.EventEmitter();
            this.value = 'Id';
            this._labelField = 'Definition';
            this.seperator = ' ';
            this.selectorService.listStateChanged
                .subscribe(function (result) {
                if (result && result.length > 0 && _this._labelField === 'CombinedFields') {
                    result.map(function (item) {
                        /** @type {?} */
                        var combinedValue = '';
                        (( /** @type {?} */(_this.labelField))).forEach(function (field) {
                            if (item[field]) {
                                combinedValue = combinedValue + item[field] + _this.seperator;
                            }
                        });
                        item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -_this.seperator.length) : null;
                        return item;
                    });
                }
                _this.listData = _.orderBy(result, [
                    'OrderNo',
                    function (z) { return z[_this._labelField] && (typeof z[_this._labelField] === 'string') ? z[_this._labelField].toLowerCase() : z[_this._labelField]; },
                    // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                    _this._labelField
                ], ['asc']);
                _this.findReadonlyValue();
            });
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        SelectorComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.requestOptions && this.requestOptions) {
                    this.urlOptions = this.requestOptions.UrlOptions;
                    this.customFilter = this.requestOptions.CustomFilter;
                    this.selectorService.setLoadUrlSetting(this.urlOptions);
                    this.selectorService.setHeaders(this.requestOptions.HeaderParameters || []);
                    this.load();
                }
            };
        /**
         * @param {?} requestOptions
         * @param {?=} value
         * @param {?=} labelField
         * @param {?=} seperator
         * @return {?}
         */
        SelectorComponent.prototype.setRequestOptions = /**
         * @param {?} requestOptions
         * @param {?=} value
         * @param {?=} labelField
         * @param {?=} seperator
         * @return {?}
         */
            function (requestOptions, value, labelField, seperator) {
                if (value === void 0) {
                    value = null;
                }
                if (labelField === void 0) {
                    labelField = null;
                }
                if (seperator === void 0) {
                    seperator = ' ';
                }
                if (value) {
                    this.value = value;
                    this.model = value;
                }
                if (labelField) {
                    this.labelField = labelField;
                }
                if (seperator) {
                    this.seperator = seperator;
                }
                if (this.labelField instanceof Array) {
                    this._labelField = 'CombinedFields';
                }
                else if (this.labelField) {
                    this._labelField = ( /** @type {?} */(this.labelField));
                }
                if (requestOptions) {
                    this.urlOptions = requestOptions.UrlOptions;
                    this.customFilter = requestOptions.CustomFilter;
                    this.selectorService.setLoadUrlSetting(this.urlOptions);
                    this.selectorService.setHeaders(requestOptions.HeaderParameters || []);
                }
            };
        /**
         * @return {?}
         */
        SelectorComponent.prototype.load = /**
         * @return {?}
         */
            function () {
                this.getDataSource();
            };
        /**
         * @return {?}
         */
        SelectorComponent.prototype.reload = /**
         * @return {?}
         */
            function () {
                this.getDataSource();
            };
        /**
         * @return {?}
         */
        SelectorComponent.prototype.reset = /**
         * @return {?}
         */
            function () {
                this.listData = [];
            };
        /**
         * @param {?} val
         * @return {?}
         */
        SelectorComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        SelectorComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(SelectorComponent.prototype, "Model", {
            get: /**
             * @return {?}
             */ function () {
                return this.model;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                var _this = this;
                this.model = val;
                this.onChange(val);
                this.onTouched();
                if (val) {
                    if (this.listData) {
                        /** @type {?} */
                        var data = this.listData.find(function (x) { return x[_this.value] === _this.model; });
                        if (data) {
                            this.changed.emit(data);
                            this.readOnlyValue = data[this._labelField];
                        }
                    }
                    else {
                        /** @type {?} */
                        var emitData = {};
                        emitData[this.value] = this.model;
                        this.readOnlyValue = this.model;
                        this.changed.emit(emitData);
                    }
                }
                else {
                    this.changed.emit(null);
                    this.readOnlyValue = null;
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        SelectorComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        SelectorComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        SelectorComponent.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
            function (isDisabled) {
            };
        /**
         * @param {?} value
         * @return {?}
         */
        SelectorComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.model = value;
                this.findReadonlyValue();
            };
        /**
         * @return {?}
         */
        SelectorComponent.prototype.findReadonlyValue = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.readOnlyValue = null;
                if (this.listData) {
                    if (this.model || this.model === 0) {
                        /** @type {?} */
                        var data = this.listData.find(function (x) { return x[_this.value] === _this.model; });
                        if (data) {
                            this.readOnlyValue = data[this._labelField];
                            if (!this.isFirstValueSend) {
                                this.firstValue.emit(data);
                                this.isFirstValueSend = true;
                            }
                        }
                    }
                }
            };
        /**
         * @private
         * @return {?}
         */
        SelectorComponent.prototype.getDataSource = /**
         * @private
         * @return {?}
         */
            function () {
                this.selectorService.loadDataByFilterAndPageSetting(this.customFilter);
            };
        SelectorComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-selector',
                        template: "\n    <ng-select *ngIf=\"!isReadOnly\"\n      class=\"custom-ng-select form-selector\"\n      [items]=\"listData\"\n      [searchable]=\"searchable\"\n\t\t\t[clearable]=\"clearable\"\n      [bindValue]=\"value\"\n      [bindLabel]=\"_labelField\"\n      [(ngModel)]=\"Model\"\n      [disabled]=\"isDisabled\"\n    >\n    </ng-select>\n    <layout-form-text-input class=\"readonly-text-input\" *ngIf=\"isReadOnly\"\n      [isReadOnly]=\"isReadOnly\"\n      [isDisabled]=\"isDisabled\"\n      [(ngModel)]=\"readOnlyValue\"></layout-form-text-input>\n  ",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return SelectorComponent; }),
                                multi: true
                            },
                            SelectorService
                        ],
                        styles: [".custom-ng-select{height:30px!important;min-width:100px;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
                    }] }
        ];
        /** @nocollapse */
        SelectorComponent.ctorParameters = function () {
            return [
                { type: SelectorService }
            ];
        };
        SelectorComponent.propDecorators = {
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            searchable: [{ type: i0.Input }],
            clearable: [{ type: i0.Input }],
            requestOptions: [{ type: i0.Input }],
            changed: [{ type: i0.Output }],
            firstValue: [{ type: i0.Output }]
        };
        return SelectorComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var StaticSelectorComponent = /** @class */ (function () {
        function StaticSelectorComponent() {
            this.valueField = 'Id';
            this.labelField = 'Definition';
            this._labelField = 'Definition';
            this.seperator = ' ';
            this.isDisabled = false;
            this.isReadOnly = false;
            this.searchable = true;
            this.clearable = true;
            this.changed = new i0.EventEmitter();
            // it is for getting the first selected value
            this.firstValue = new i0.EventEmitter();
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        StaticSelectorComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                var _this = this;
                if (changes.dataSource && this.selector && this.model) {
                    if (this.dataSource && this.dataSource.length > 500) {
                        this.virtualScroll = true;
                    }
                    // this.dataSource = [...this.dataSource, changes.dataSource];
                    /** @type {?} */
                    var index = _.findIndex(this.dataSource, ['Id', this.Model]);
                    if (index === -1) {
                        this.model = null;
                    }
                }
                if (changes.dataSource && this.dataSource) {
                    this.dataSource = _.orderBy(this.dataSource, [
                        'OrderNo',
                        function (z) { return z[_this._labelField] && (typeof z[_this._labelField] === 'string') ? z[_this._labelField].toLowerCase() : z[_this._labelField]; },
                        // y => y['Definition'] && (typeof y['Definition'] === 'string') ? y['Definition'].toLowerCase() : y['Definition'],
                        this._labelField
                    ], ['asc']);
                    this.findDisplayValue();
                }
                if (changes.labelField && this.labelField) {
                    if (changes.labelField.currentValue instanceof Array) {
                        this._labelField = 'CombinedFields';
                    }
                    else {
                        this._labelField = ( /** @type {?} */(this.labelField));
                    }
                }
                if (changes.dataSource || changes.labelField || changes.seperator) {
                    if (this.dataSource && this.dataSource.length > 0 && this._labelField === 'CombinedFields') {
                        this.dataSource.map(function (item) {
                            /** @type {?} */
                            var combinedValue = '';
                            (( /** @type {?} */(_this.labelField))).forEach(function (field) {
                                if (item[field]) {
                                    combinedValue = combinedValue + item[field] + _this.seperator;
                                }
                            });
                            item['CombinedFields'] = combinedValue ? combinedValue.slice(0, -_this.seperator.length) : null;
                            return item;
                        });
                        this.findDisplayValue();
                    }
                }
            };
        /**
         * @param {?} val
         * @return {?}
         */
        StaticSelectorComponent.prototype.onChange = /**
         * @param {?} val
         * @return {?}
         */
            function (val) {
            };
        /**
         * @return {?}
         */
        StaticSelectorComponent.prototype.onTouched = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(StaticSelectorComponent.prototype, "Model", {
            get: /**
             * @return {?}
             */ function () {
                return this.model;
            },
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                var _this = this;
                this.model = val;
                this.onChange(val);
                this.onTouched();
                if (val) {
                    if (this.dataSource) {
                        /** @type {?} */
                        var data = this.dataSource.find(function (x) { return x[_this.valueField] === _this.model; });
                        if (data) {
                            this.value = data[this._labelField];
                            this.changed.emit(data);
                        }
                    }
                    else {
                        /** @type {?} */
                        var emitData = {};
                        emitData[this.valueField] = this.model;
                        this.changed.emit(emitData);
                    }
                }
                else {
                    this.changed.emit(null);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        StaticSelectorComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onChange = fn;
            };
        /**
         * @param {?} fn
         * @return {?}
         */
        StaticSelectorComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
            function (fn) {
                this.onTouched = fn;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        StaticSelectorComponent.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.model = value;
                this.findDisplayValue();
            };
        /**
         * @return {?}
         */
        StaticSelectorComponent.prototype.findDisplayValue = /**
         * @return {?}
         */
            function () {
                var _this = this;
                // if (this.isReadOnly)
                if (this.dataSource && this.dataSource.length > 0 && (this.model || this.model === 0)) {
                    /** @type {?} */
                    var data = this.dataSource.find(function (x) { return x[_this.valueField] === _this.model; });
                    if (data) {
                        this.value = data[this._labelField];
                        if (!this.isFirstValueSend) {
                            this.firstValue.emit(data);
                        }
                    }
                }
            };
        StaticSelectorComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-static-selector',
                        template: "\n\t<ng-select *ngIf=\"!isReadOnly\"\n\t\t#selector\n      \tclass=\"custom-ng-select form-selector\"\n\t\t[items]=\"dataSource\"\n\t\t[searchable]=\"searchable\"\n\t\t[clearable]=\"clearable\"\n      \t[(ngModel)]=\"Model\"\n\t\t[bindValue]=\"valueField\"\n\t\t[bindLabel]=\"_labelField\"\n\t\t[disabled]=\"isDisabled\"\n    appendTo= \"body\"\n    [virtualScroll]=\"virtualScroll\"\n    placeholder=\"{{placeholder|translate}}\"\n\t\tngDefaultControl\n\t>\n\t</ng-select>\n\t<layout-form-text-input class=\"readonly-text-input\" *ngIf=\"isReadOnly\" [isReadOnly]=\"isReadOnly\" [isDisabled]=\"isDisabled\" [(ngModel)]=\"value\"></layout-form-text-input>\n  \t",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: i0.forwardRef(function () { return StaticSelectorComponent; }),
                                multi: true
                            }
                        ],
                        styles: [".custom-ng-select{height:30px!important;width:auto!important}:host /deep/ .custom-ng-select .ng-select-container{min-height:30px!important;height:30px!important;border-radius:.2rem!important}::ng-deep .ng-dropdown-panel.ng-select-bottom{width:auto!important;min-width:100%;max-width:200px}::ng-deep .custom-ng-select .ng-option{white-space:normal!important}::ng-deep .custom-ng-select .ng-option-marked{background-color:#e9f5ff!important}"]
                    }] }
        ];
        StaticSelectorComponent.propDecorators = {
            dataSource: [{ type: i0.Input }],
            valueField: [{ type: i0.Input }],
            labelField: [{ type: i0.Input }],
            seperator: [{ type: i0.Input }],
            isDisabled: [{ type: i0.Input }],
            isReadOnly: [{ type: i0.Input }],
            searchable: [{ type: i0.Input }],
            clearable: [{ type: i0.Input }],
            changed: [{ type: i0.Output }],
            placeholder: [{ type: i0.Input }],
            selector: [{ type: i0.ViewChild, args: ['selector',] }],
            firstValue: [{ type: i0.Output }]
        };
        return StaticSelectorComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TabsComponent = /** @class */ (function () {
        function TabsComponent(router, activatedRoute) {
            var _this = this;
            this.router = router;
            this.activatedRoute = activatedRoute;
            this.tabChange = new i0.EventEmitter();
            this.tabs = [];
            // this.routingSubscription = this.router.events.filter(event => event instanceof NavigationEnd)
            //   .subscribe(event => {
            //     // set breadcrumbs
            //     const foundData = this.tabs.find(x => x.link === this.router.url);
            //     if (foundData) {
            //       this.onTabClick(foundData);
            //     }
            //   });
            this.routingSubscription = this.router.events.pipe(operators.filter(function (event) { return event instanceof i5.NavigationEnd; })).subscribe(function () {
                /** @type {?} */
                var foundData = _this.tabs.find(function (x) { return x.link === _this.router.url; });
                if (foundData) {
                    _this.onTabClick(foundData);
                }
            });
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        TabsComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                var _this = this;
                if (changes.activeTab && !changes.activeTab.isFirstChange()) {
                    this.foundData = this.tabs.find(function (x) { return x.title === _this.activeTab; });
                    this.tabChange.emit(this.foundData);
                }
            };
        /**
         * @param {?} tab
         * @return {?}
         */
        TabsComponent.prototype.getClassActive = /**
         * @param {?} tab
         * @return {?}
         */
            function (tab) {
                // return this.activeTab === tab.title && !tab.link;
                // (bu alan hep false dönüyo)
                return this.activeTab === tab.title && ((tab.link !== undefined && tab.link !== null) || !tab.link);
            };
        /**
         * @param {?} tab
         * @return {?}
         */
        TabsComponent.prototype.onTabClick = /**
         * @param {?} tab
         * @return {?}
         */
            function (tab) {
                this.activeTab = tab.title;
                this.tabChange.emit(tab);
            };
        /**
         * @param {?} title
         * @param {?} link
         * @return {?}
         */
        TabsComponent.prototype.addTabs = /**
         * @param {?} title
         * @param {?} link
         * @return {?}
         */
            function (title, link) {
                this.tabs.push({ title: title, link: link });
                if (this.router && this.router.url && link === this.router.url) {
                    this.activeTab = title;
                    // this.tabChange.emit({ title, link });
                }
                // if (title === this.activeTab) {
                //   this.tabChange.emit(this.activeTab);
                //   if (link) {
                //     this.router.navigate([link]);
                //   }
                // }
            };
        /**
         * @return {?}
         */
        TabsComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.routingSubscription) {
                    this.routingSubscription.unsubscribe();
                }
            };
        TabsComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'core-tabs',
                        template: "<nav class=\"navbar navbar-expand-sm navbar-dark justify-content-md-center mb-5\">\r\n  <ul class=\"navbar-nav\">\r\n    <li class=\"nav-item\" *ngFor=\"let tab of tabs\" (click)=\"onTabClick(tab)\" [class.active]=\"getClassActive(tab)\">\r\n      <a *ngIf=\"tab.link\" class=\"nav-link\" [routerLinkActive]=\"['router-link-active']\"\r\n        [routerLink]=\"[tab.link]\">{{tab.title|translate}}</a>\r\n      <a *ngIf=\"!tab.link\" class=\"nav-link\">{{tab.title|translate}}</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n\r\n<ng-content></ng-content>",
                        styles: [".navbar{margin:15px 0!important;padding:0}.navbar-nav{display:flex;width:100%;margin-bottom:0;padding-left:0;padding-right:0;margin-left:0;margin-right:0;box-shadow:0 5px 20px rgba(0,0,0,.05)}.nav-item{font-weight:600;flex:1;text-align:center;padding:0;border:1px solid #e3f1f8;border-left-width:0}.nav-item:first-child{border-left-width:1px}.nav-item a.nav-link{background:#fff;color:#788d96;line-height:24px;border-radius:0;transition:.2s ease-in-out;cursor:pointer;height:100%;display:flex;align-items:center;justify-content:center}.nav-item a.nav-link.router-link-active,.nav-item.active a{color:#175169;border-bottom:2px solid #409fdd}.nav-item a.nav-link:hover{background-color:#e3f1f8;color:#1a2b49}"]
                    }] }
        ];
        /** @nocollapse */
        TabsComponent.ctorParameters = function () {
            return [
                { type: i5.Router },
                { type: i5.ActivatedRoute }
            ];
        };
        TabsComponent.propDecorators = {
            activeTab: [{ type: i0.Input }],
            tabChange: [{ type: i0.Output }]
        };
        return TabsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TabComponent = /** @class */ (function () {
        function TabComponent(parent, router, _ngZone) {
            this.parent = parent;
            this.router = router;
            this._ngZone = _ngZone;
            this.title = '';
            // this.step = 0;
        }
        /**
         * @return {?}
         */
        TabComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.parent.addTabs(this.title, this.link);
                this.isCurrent = this.title === this.parent.activeTab;
                if (this.isCurrent && this.link) {
                    this.router.navigate([this.link]);
                }
                this.tabChangeSubcriotion = this.parent.tabChange
                    .subscribe(function (tab) {
                    _this.isCurrent = _this.title === tab.title;
                    if (_this.isCurrent && _this.link) {
                        _this._ngZone.run(function () { return _this.router.navigate([_this.link]); });
                    }
                });
            };
        /**
         * @return {?}
         */
        TabComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.tabChangeSubcriotion) {
                    this.tabChangeSubcriotion.unsubscribe();
                }
            };
        TabComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'core-tab',
                        host: {
                            '[style.display]': 'isCurrent ? "flex" : "none"',
                        },
                        template: "\n    <ng-content></ng-content>\n  "
                    }] }
        ];
        /** @nocollapse */
        TabComponent.ctorParameters = function () {
            return [
                { type: TabsComponent },
                { type: i5.Router },
                { type: i0.NgZone }
            ];
        };
        TabComponent.propDecorators = {
            title: [{ type: i0.Input }],
            link: [{ type: i0.Input }]
        };
        return TabComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormWizardComponent = /** @class */ (function () {
        function FormWizardComponent(toastrUtilsService) {
            this.toastrUtilsService = toastrUtilsService;
            this.finishText = 'Finish';
            this.step = 1;
            this.finish = new i0.EventEmitter();
            this.stepChange = new i0.EventEmitter();
            this.steps = [];
            this.isValid = true;
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        FormWizardComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.step) {
                    this.stepChange.emit(this.step);
                }
            };
        /**
         * @param {?} index
         * @return {?}
         */
        FormWizardComponent.prototype.getClassActive = /**
         * @param {?} index
         * @return {?}
         */
            function (index) {
                return this.step === index + 1;
            };
        /**
         * @param {?} isNext
         * @return {?}
         */
        FormWizardComponent.prototype.onStepChanged = /**
         * @param {?} isNext
         * @return {?}
         */
            function (isNext) {
                if (isNext) {
                    if (this.isValid == true) {
                        this.step = this.step + 1;
                        this.stepChange.emit(this.step);
                    }
                    else {
                        this.toastrUtilsService.error('FormIsNotValid', 'NotValid');
                    }
                }
                else {
                    this.step = this.step - 1;
                    this.stepChange.emit(this.step);
                }
            };
        /**
         * @return {?}
         */
        FormWizardComponent.prototype.isOnFirstStep = /**
         * @return {?}
         */
            function () {
                return this.step === 1;
            };
        /**
         * @return {?}
         */
        FormWizardComponent.prototype.isOnFinalStep = /**
         * @return {?}
         */
            function () {
                return this.step === (this.steps ? this.steps.length : 0);
            };
        /**
         * @return {?}
         */
        FormWizardComponent.prototype.getClassNameAToSteps = /**
         * @return {?}
         */
            function () {
                if (this.steps && this.steps.length > 0) {
                    return 'wizard wizard-steps-' + this.steps.length;
                }
                else {
                    return 'wizard wizard-steps-1';
                }
            };
        /**
         * @param {?} title
         * @return {?}
         */
        FormWizardComponent.prototype.addStep = /**
         * @param {?} title
         * @return {?}
         */
            function (title) {
                /** @type {?} */
                var newStep = {
                    Header: title
                };
                this.steps.push(newStep);
                return this.steps.length;
            };
        FormWizardComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'form-wizard',
                        template: "<ul *ngIf=\"steps\" [className]=\"getClassNameAToSteps()\">\r\n  <li *ngFor=\"let step of steps; let i=index;\">\r\n    <div *ngIf=\"step.Header\" class=\"step-header\">\r\n      {{step.Header}}\r\n    </div>\r\n    <div class=\"step\" [class.complete]=\"step.Completed\" [class.active]=\"getClassActive(i)\">\r\n      <span>{{i + 1}}</span>\r\n    </div>\r\n  </li>\r\n</ul>\r\n<div class=\" col-sm-12 prl-0 ptb-10\">\r\n  <ng-content></ng-content>\r\n</div>\r\n<div class=\"my-wizard__footer\">\r\n  <button class=\"btn btn-warning\" [style.visibility]=\"isOnFirstStep() ? 'hidden' : 'visible'\"\r\n    (click)=\"onStepChanged(false)\">\r\n    <core-icon icon=\"arrow-alt-circle-left\"></core-icon> Previous\r\n  </button>\r\n  <!-- {{step}} / {{steps.length}} -->\r\n  <button class=\"btn btn-info\" *ngIf=\"!isOnFinalStep()\" (click)=\"onStepChanged(true)\">\r\n    Next <core-icon icon=\"arrow-alt-circle-right\"></core-icon>\r\n  </button>\r\n  <!-- <button\r\n    *ngIf=\"isOnFinalStep()\"\r\n    (click)=\"finish.emit(step + 1)\">\r\n    {{finishText}}\r\n  </button> -->\r\n</div>",
                        styles: [":host{display:flex;flex-direction:column}.my-wizard__footer{display:flex;align-items:center;justify-content:space-between;flex-shrink:0}.wizard{padding:0;margin:0;list-style:none;width:100%;display:table;position:relative}.wizard .step-header{color:#000;position:relative;width:100%;text-align:center;margin-top:-15px;top:-10px}.wizard>*{display:table-cell;text-align:center}.wizard>* .step{width:30px;height:30px;border-radius:30px;border:1px solid #bdc3c7;color:#bdc3c7;font-weight:300;background:#fff;text-align:center;display:inline-block;position:relative;box-sizing:content-box;cursor:default}.wizard>* .step.complete{border-color:#1abc9c;color:#1abc9c}.wizard>* .step.active{border-color:#3498db;color:#3498db;font-weight:500;border-width:2px;margin-top:-1px}.wizard>* .step:after{content:\" \";display:block;width:30px;height:30px;background-color:#fff;position:absolute;z-index:-1;border:10px solid #fff;top:-10px;left:-10px;box-sizing:content-box}.wizard>* .step>*{line-height:30px}.wizard:after{content:\" \";border-bottom:1px dotted rgba(0,0,0,.2);position:absolute;z-index:-2;top:50%}.wizard-steps-1>*{width:100%}.wizard-steps-1:after{left:50%;right:50%}.wizard-steps-2>*{width:50%}.wizard-steps-2:after{left:25%;right:25%}.wizard-steps-3>*{width:33.33333%}.wizard-steps-3:after{left:16.66667%;right:16.66667%}.wizard-steps-4>*{width:25%}.wizard-steps-4:after{left:12.5%;right:12.5%}.wizard-steps-5>*{width:20%}.wizard-steps-5:after{left:10%;right:10%}.wizard-steps-6>*{width:16.66667%}.wizard-steps-6:after{left:8.33333%;right:8.33333%}.wizard-steps-7>*{width:14.28571%}.wizard-steps-7:after{left:7.14286%;right:7.14286%}.wizard-steps-8>*{width:12.5%}.wizard-steps-8:after{left:6.25%;right:6.25%}.wizard-steps-9>*{width:11.11111%}.wizard-steps-9:after{left:5.55556%;right:5.55556%}.wizard-steps-10>*{width:10%}.wizard-steps-10:after{left:5%;right:5%}.wizard-steps-11>*{width:9.09091%}.wizard-steps-11:after{left:4.54545%;right:4.54545%}.wizard-steps-12>*{width:8.33333%}.wizard-steps-12:after{left:4.16667%;right:4.16667%}.wizard-steps-13>*{width:7.69231%}.wizard-steps-13:after{left:3.84615%;right:3.84615%}.wizard-steps-14>*{width:7.14286%}.wizard-steps-14:after{left:3.57143%;right:3.57143%}.wizard-steps-15>*{width:6.66667%}.wizard-steps-15:after{left:3.33333%;right:3.33333%}.wizard-steps-16>*{width:6.25%}.wizard-steps-16:after{left:3.125%;right:3.125%}.wizard-steps-17>*{width:5.88235%}.wizard-steps-17:after{left:2.94118%;right:2.94118%}.wizard-steps-18>*{width:5.55556%}.wizard-steps-18:after{left:2.77778%;right:2.77778%}.wizard-steps-19>*{width:5.26316%}.wizard-steps-19:after{left:2.63158%;right:2.63158%}.wizard-steps-20>*{width:5%}.wizard-steps-20:after{left:2.5%;right:2.5%}"]
                    }] }
        ];
        /** @nocollapse */
        FormWizardComponent.ctorParameters = function () {
            return [
                { type: ToastrUtilsService }
            ];
        };
        FormWizardComponent.propDecorators = {
            finishText: [{ type: i0.Input }],
            step: [{ type: i0.Input }],
            finish: [{ type: i0.Output }],
            stepChange: [{ type: i0.Output }]
        };
        return FormWizardComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FormWizardStepComponent = /** @class */ (function () {
        function FormWizardStepComponent(parent) {
            this.parent = parent;
            this.title = '';
            this.isValid = true;
            // this.step = 0;
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        FormWizardStepComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.isValid) {
                    console.log(this.isValid);
                    this.parent.isValid = this.isValid;
                }
            };
        /**
         * @return {?}
         */
        FormWizardStepComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.step = this.parent.addStep(this.title);
                this.isCurrent = this.step === this.parent.step;
                this.stepChangeSubcriotion = this.parent.stepChange.subscribe(function (step) {
                    _this.isCurrent = _this.step === step;
                });
            };
        /**
         * @return {?}
         */
        FormWizardStepComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.stepChangeSubcriotion) {
                    this.stepChangeSubcriotion.unsubscribe();
                }
            };
        FormWizardStepComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'wizard-step',
                        host: {
                            '[style.display]': 'isCurrent ? "flex" : "none"',
                        },
                        template: "\n    <ng-content></ng-content>\n  "
                    }] }
        ];
        /** @nocollapse */
        FormWizardStepComponent.ctorParameters = function () {
            return [
                { type: FormWizardComponent }
            ];
        };
        FormWizardStepComponent.propDecorators = {
            title: [{ type: i0.Input }],
            isValid: [{ type: i0.Input }]
        };
        return FormWizardStepComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CoreIconComponent = /** @class */ (function () {
        function CoreIconComponent() {
        }
        CoreIconComponent.decorators = [
            { type: i0.Component, args: [{
                        template: "\n        <fa-icon *ngIf=\"icon\" [icon]=\"icon\"></fa-icon>\n    ",
                        selector: 'core-icon'
                    }] }
        ];
        CoreIconComponent.propDecorators = {
            icon: [{ type: i0.Input }]
        };
        return CoreIconComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ BaseComponent = /** @class */ (function () {
        function BaseComponent(preventUnsavedChangesService) {
            var _this = this;
            this.preventUnsavedChangesService = preventUnsavedChangesService;
            this.subscriptions = [];
            this.isSaveOperationEnabled = true;
            this.safeSubscription(this.preventUnsavedChangesService.getStatus()
                .subscribe(function (status) {
                if (status === 'waiting') {
                    _this.preventUnsavedChangesService.setIsChanged(_this.skipUnsavedChanges ? false : _this.hasUnsavedChanges());
                    _this.preventUnsavedChangesService.setStatus('finished');
                }
            }));
        }
        /**
         * @protected
         * @param {?} sub
         * @return {?}
         */
        BaseComponent.prototype.safeSubscription = /**
         * @protected
         * @param {?} sub
         * @return {?}
         */
            function (sub) {
                this.subscriptions.push(sub);
                return sub;
            };
        /**
         * @private
         * @return {?}
         */
        BaseComponent.prototype.unsubscribeAll = /**
         * @private
         * @return {?}
         */
            function () {
                this.subscriptions.forEach(function (subs) {
                    if (subs) {
                        subs.unsubscribe();
                    }
                });
            };
        /**
         * @protected
         * @return {?}
         */
        BaseComponent.prototype.baseNgOnDestroy = /**
         * @protected
         * @return {?}
         */
            function () {
                this.unsubscribeAll();
            };
        return BaseComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SystemReferenceDateComponent = /** @class */ (function () {
        function SystemReferenceDateComponent(systemReferenceDateService, storageService, fb, translate) {
            var _this = this;
            this.systemReferenceDateService = systemReferenceDateService;
            this.storageService = storageService;
            this.fb = fb;
            this.translate = translate;
            /** @type {?} */
            var currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
            this._calendarDateValue = currentsystemRefDate ?
                this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
                this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
            this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
            // until the better solution for translate.
            this.localEn = {
                firstDayOfWeek: 1,
                dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear'
            };
            // until the better solution for translate.
            this.localeTr = {
                firstDayOfWeek: 1,
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Paz', 'Pts', 'Sal', 'Çar', 'Per', 'Cum', 'Cts'],
                dayNamesMin: ['Pz', 'Pt', 'Sl', 'Çr', 'Pr', 'Cm', 'Ct'],
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                today: 'Bugün',
                clear: 'Temizle'
            };
            // until the better solution for translate.
            this.locale = this.localeTr;
            this.translateSubscription = this.translate.onLangChange
                .subscribe(function (event) {
                if (event) {
                    _this.locale = event.lang === 'tr' ? _this.localeTr : _this.localEn;
                }
            });
            this.systemReferenceDateService.getSelectedReferenceDate()
                .subscribe(function (selectedRefDate) {
                if (selectedRefDate !== '') {
                    _this._calendarDateValue = _this.systemReferenceDateService.convertDateToDateUTC(selectedRefDate);
                }
            });
        }
        Object.defineProperty(SystemReferenceDateComponent.prototype, "calendarDateValue", {
            get: /**
             * @return {?}
             */ function () {
                return this._calendarDateValue;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._calendarDateValue = value;
                // this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} selectedDateISO
         * @return {?}
         */
        SystemReferenceDateComponent.prototype.setSelectedDate = /**
         * @param {?} selectedDateISO
         * @return {?}
         */
            function (selectedDateISO) {
                this.systemReferenceDateService.setSelectedReferenceDate(selectedDateISO);
            };
        /**
         * @return {?}
         */
        SystemReferenceDateComponent.prototype.onClose = /**
         * @return {?}
         */
            function () {
                this.checkAndApplyDate();
            };
        /**
         * @param {?} event
         * @return {?}
         */
        SystemReferenceDateComponent.prototype.keyDownFunction = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (event.keyCode === 13) {
                    this.checkAndApplyDate();
                    if (this._calendarDateValue && this.systemRefDatePicker && this.systemRefDatePicker.overlayVisible) {
                        this.systemRefDatePicker.overlayVisible = false;
                    }
                }
            };
        /**
         * @return {?}
         */
        SystemReferenceDateComponent.prototype.checkAndApplyDate = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var currentsystemRefDate = this.storageService.getStored(Storage.SystemRefDate, StorageType.SessionStorage);
                if (this._calendarDateValue) {
                    /** @type {?} */
                    var selectedDate = this.systemReferenceDateService.convertDateToISO(this._calendarDateValue);
                    if (currentsystemRefDate !== selectedDate) {
                        this.setSelectedDate(this.systemReferenceDateService.convertDateToISO(this._calendarDateValue));
                    }
                }
                else {
                    this._calendarDateValue = currentsystemRefDate ?
                        this.systemReferenceDateService.convertDateToDateUTC(currentsystemRefDate) :
                        this.systemReferenceDateService.convertDateToDateUTC(this.systemReferenceDateService.convertDateToISO(new Date));
                }
            };
        /**
         * @return {?}
         */
        SystemReferenceDateComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.translateSubscription) {
                    this.translateSubscription.unsubscribe();
                }
                if (this.sysRefDateSubscription) {
                    this.sysRefDateSubscription.unsubscribe();
                }
            };
        SystemReferenceDateComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-system-reference-date',
                        template: " <div class=\"system-reference-container\">\r\n   <p-calendar #systemRefDatePicker class=\"system-reference-datepicker\" [(ngModel)]=\"calendarDateValue\"\r\n     [dateFormat]=\"'dd.mm.yy'\" [showIcon]=\"false\" [showButtonBar]=\"true\" [style]=\"{'width':'100%', 'height': '30px'}\"\r\n     monthNavigator=\"true\" yearNavigator=\"true\" yearRange=\"1930:2030\" [locale]=\"locale\" (onClose)=\"onClose()\"\r\n     (keydown)=\"keyDownFunction($event)\" appendTo=\"body\">\r\n   </p-calendar>\r\n </div>\r\n",
                        styles: [":host /deep/ .ui-calendar .ui-inputtext{height:30px!important;font-size:inherit;width:calc(100%);color:#fff;border:none;cursor:pointer;background:#e30a3a!important;border-color:rgba(255,255,255,.2)!important}:host /deep/ .ui-datepicker table td{padding:.2em!important;font-size:12px}:host /deep/ .ui-datepicker table td>a.ui-state-active{background-color:#3594e8!important}:host /deep/ .ui-datepicker.ui-datepicker-timeonly{min-width:147px!important}"]
                    }] }
        ];
        /** @nocollapse */
        SystemReferenceDateComponent.ctorParameters = function () {
            return [
                { type: SystemReferenceDateService },
                { type: StorageService },
                { type: forms.FormBuilder },
                { type: core.TranslateService }
            ];
        };
        SystemReferenceDateComponent.propDecorators = {
            systemRefDatePicker: [{ type: i0.ViewChild, args: ['systemRefDatePicker',] }]
        };
        return SystemReferenceDateComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var LayoutComponents = [
        CoreIconComponent,
        CoreGridComponent,
        CommandCellComponent,
        ListOfValuesModalComponent,
        ListOfValuesComponent,
        SelectorComponent,
        components,
        StaticSelectorComponent,
        CoreModalComponent,
        ColumnHeaderComponent,
        NoRowsOverlayComponent,
        LoadingOverlayComponent,
        FormWizardComponent,
        FormWizardStepComponent,
        TabsComponent,
        TabComponent,
        CoreOrgChartComponent,
        BreadcrumbComponent
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var coreGridEditElements = [
        BooleanEditCellComponent,
        DateEditCellComponent,
        LovEditCellComponent,
        NumericEditCellComponent,
        SelectEditCellComponent,
        TextEditCellComponent,
        MaskEditCellComponent,
        CurrencyEditCellComponent
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var coreGridReadOnlyElements = [
        BooleanCellComponent,
        LabelCellComponent,
        SelectCellComponent,
        DateCellComponent,
        CurrencyCellComponent,
        MaskCellComponent,
        NumericCellComponent
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var coreGridFilterElements = [
        DateFilterCellComponent,
        TextFilterCellComponent,
        SelectFilterCellComponent,
        NumericFilterCellComponent,
        BooleanFilterCellComponent,
        CurrencyFilterCellComponent,
        LovFilterCellComponent,
        MaskFilterCellComponent
    ];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var customCurrencyMaskConfig = {
        align: 'left',
        allowNegative: true,
        allowZero: true,
        decimal: ',',
        precision: 2,
        prefix: '',
        suffix: '',
        thousands: '.',
        nullable: true
    };
    /**
     * @param {?} http
     * @return {?}
     */
    function HttpLoaderFactory(http$$1) {
        return new httpLoader.TranslateHttpLoader(http$$1);
    }
    var CoreCommonModule = /** @class */ (function () {
        function CoreCommonModule() {
        }
        CoreCommonModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [
                            coreGridEditElements,
                            coreGridReadOnlyElements,
                            coreGridFilterElements,
                            LayoutComponents,
                            SafePipe
                        ],
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            http.HttpClientModule,
                            forms.ReactiveFormsModule,
                            i5.RouterModule,
                            angular2TextMask.TextMaskModule,
                            ngSelect.NgSelectModule,
                            fileupload.FileUploadModule,
                            ngxBootstrap.BsDropdownModule.forRoot(),
                            ngxBootstrap.BsDatepickerModule.forRoot(),
                            ngxBootstrap.TabsModule.forRoot(),
                            ngxBootstrap.ModalModule.forRoot(),
                            ngxBootstrap.CollapseModule.forRoot(),
                            ngxBootstrap.SortableModule.forRoot(),
                            ngxBootstrap.PaginationModule.forRoot(),
                            core.TranslateModule.forRoot({
                                loader: [
                                    { provide: core.TranslateLoader, useFactory: HttpLoaderFactory, deps: [http.HttpClient] }
                                ]
                            }),
                            agGridAngular.AgGridModule.withComponents([
                                coreGridEditElements,
                                coreGridReadOnlyElements,
                                coreGridFilterElements,
                                CommandCellComponent,
                                ColumnHeaderComponent,
                                NoRowsOverlayComponent,
                                LoadingOverlayComponent
                            ]),
                            ngxToastr.ToastContainerModule,
                            ngxToastr.ToastrModule.forRoot({
                                closeButton: true,
                                progressBar: true
                            }),
                            ngxCurrency.NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
                            calendar.CalendarModule,
                            checkbox.CheckboxModule,
                            dialog.DialogModule,
                            overlaypanel.OverlayPanelModule,
                            angularFontawesome.FontAwesomeModule
                        ],
                        entryComponents: [
                            ListOfValuesModalComponent
                        ],
                        providers: [
                            DataService,
                            SweetAlertService,
                            ToastrUtilsService,
                            AuthenticationService,
                            TranslateHelperService,
                            ValidationService
                        ],
                        exports: [
                            LayoutComponents,
                            core.TranslateModule,
                            SafePipe
                        ],
                        schemas: [
                            i0.CUSTOM_ELEMENTS_SCHEMA,
                            i0.NO_ERRORS_SCHEMA
                        ]
                    },] }
        ];
        return CoreCommonModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingComponent = /** @class */ (function () {
        function LoadingComponent(loadingService, changeDetector) {
            this.loadingService = loadingService;
            this.changeDetector = changeDetector;
        }
        /**
         * @return {?}
         */
        LoadingComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.loadingService.getLoadingBarDisplayStatus()
                    .subscribe(function (status) {
                    if (_this.showLoading !== status) {
                        _this.showLoading = status;
                        _this.detectChanges();
                    }
                });
            };
        /**
         * @return {?}
         */
        LoadingComponent.prototype.detectChanges = /**
         * @return {?}
         */
            function () {
                var _this = this;
                setTimeout(function () {
                    if (!_this.changeDetector['destroyed']) {
                        _this.changeDetector.detectChanges();
                    }
                }, 251);
            };
        /**
         * @return {?}
         */
        LoadingComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.loadingService.loadingBarDisplayStatus.unsubscribe();
            };
        LoadingComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'layout-loading',
                        template: "<div *ngIf=\"showLoading\" class=\"core-loader\">\r\n  <div id=\"loading-wrapper\">\r\n    <!-- <div id=\"loading-text\">Y\u00FCkleniyor</div> -->\r\n    <div class=\"loader\">\r\n      <div class=\"inner one\"></div>\r\n      <div class=\"inner two\"></div>\r\n      <div class=\"inner three\"></div>\r\n    </div>\r\n  </div>\r\n</div>",
                        styles: ["#loading-wrapper{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;background-color:rgba(0,0,0,.05)}.core-loader{position:fixed;width:100%;height:100%;left:0;top:0;z-index:48;display:block;background-position:center center;background-repeat:no-repeat;background-color:transparent!important}.loader{position:absolute;top:calc(50% - 32px);left:calc(50% - 32px);width:64px;height:64px;border-radius:50%;perspective:800px;z-index:999}.loader-text{color:#000;position:relative;top:25px;left:75px;font-weight:700}.inner{position:absolute;box-sizing:border-box;width:100%;height:100%;border-radius:50%}.inner.one{left:0;top:0;-webkit-animation:1s linear infinite rotate-one;animation:1s linear infinite rotate-one;border-bottom:3px solid #2d5f8b}.inner.two{right:0;top:0;-webkit-animation:1s linear infinite rotate-two;animation:1s linear infinite rotate-two;border-right:3px solid #2d5f8b}.inner.three{right:0;bottom:0;-webkit-animation:1s linear infinite rotate-three;animation:1s linear infinite rotate-three;border-top:3px solid #2d5f8b}@-webkit-keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@keyframes rotate-one{0%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(-45deg) rotateZ(360deg)}}@-webkit-keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@keyframes rotate-two{0%{transform:rotateX(50deg) rotateY(10deg) rotateZ(0)}100%{transform:rotateX(50deg) rotateY(10deg) rotateZ(360deg)}}@-webkit-keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}@keyframes rotate-three{0%{transform:rotateX(35deg) rotateY(55deg) rotateZ(0)}100%{transform:rotateX(35deg) rotateY(55deg) rotateZ(360deg)}}"]
                    }] }
        ];
        /** @nocollapse */
        LoadingComponent.ctorParameters = function () {
            return [
                { type: LoadingService },
                { type: i0.ChangeDetectorRef }
            ];
        };
        return LoadingComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingLibModule = /** @class */ (function () {
        function LoadingLibModule() {
        }
        LoadingLibModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [
                            LoadingComponent
                        ],
                        imports: [
                            common.CommonModule
                        ],
                        providers: [
                        // LoadingService
                        ],
                        exports: [
                            LoadingComponent
                        ]
                    },] }
        ];
        return LoadingLibModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // import { SystemReferenceDateService } from './shared/services/system-reference-date.service';
    var SystemReferanceDateLibModule = /** @class */ (function () {
        function SystemReferanceDateLibModule() {
        }
        SystemReferanceDateLibModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [
                            SystemReferenceDateComponent
                        ],
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            calendar.CalendarModule,
                            ngxBootstrap.BsDatepickerModule.forRoot()
                        ],
                        exports: [
                            SystemReferenceDateComponent
                        ]
                    },] }
        ];
        return SystemReferanceDateLibModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.CoreGridCellType = CoreGridCellType;
    exports.CoreGridColumnType = CoreGridColumnType;
    exports.CoreGridSelectionType = CoreGridSelectionType;
    exports.Comparison = Comparison;
    exports.DefaultComparison = DefaultComparison;
    exports.TextComparison = TextComparison;
    exports.NumberComparison = NumberComparison;
    exports.BooleanComparison = BooleanComparison;
    exports.DateComparison = DateComparison;
    exports.ComparisonType = ComparisonType;
    exports.Conditions = Conditions;
    exports.FormButtonType = FormButtonType;
    exports.LovColumnType = LovColumnType;
    exports.Module = Module;
    exports.OperationType = OperationType;
    exports.SortType = SortType;
    exports.FormType = FormType;
    exports.MaskType = MaskType;
    exports.AlertDismissReason = AlertDismissReason;
    exports.AlertType = AlertType;
    exports.Connector = Connector;
    exports.ConfirmDialogOperationType = ConfirmDialogOperationType;
    exports.ToastrMessages = ToastrMessages;
    exports.Storage = Storage;
    exports.Headers = Headers;
    exports.StorageType = StorageType;
    exports.DateSelectionMode = DateSelectionMode;
    exports.BaseModuleUrl = BaseModuleUrl;
    exports.DateSettings = DateSettings;
    exports.Filter = Filter;
    exports.FilterGroup = FilterGroup;
    exports.LovColumn = LovColumn;
    exports.ConfirmDialogSettings = ConfirmDialogSettings;
    exports.GetListByResult = GetListByResult;
    exports.ServiceResult = ServiceResult;
    exports.BaseModel = BaseModel;
    exports.SafePipe = SafePipe;
    exports.AppConfig = AppConfig;
    exports.CoreModalService = CoreModalService;
    exports.BreadcrumbService = BreadcrumbService;
    exports.LoadingService = LoadingService;
    exports.PreventUnsavedChangesService = PreventUnsavedChangesService;
    exports.StorageService = StorageService;
    exports.SweetAlertService = SweetAlertService;
    exports.SystemReferenceDateService = SystemReferenceDateService;
    exports.ToastrUtilsService = ToastrUtilsService;
    exports.TranslateHelperService = TranslateHelperService;
    exports.ValidationService = ValidationService;
    exports.PreventUnsavedChangesGuard = PreventUnsavedChangesGuard;
    exports.BaseHttpService = BaseHttpService;
    exports.BaseService = BaseService;
    exports.DataService = DataService;
    exports.RequestInterceptor = RequestInterceptor;
    exports.AuthenticationService = AuthenticationService;
    exports.LayoutComponents = LayoutComponents;
    exports.BaseComponent = BaseComponent;
    exports.CoreGridComponent = CoreGridComponent;
    exports.ColumnHeaderComponent = ColumnHeaderComponent;
    exports.NoRowsOverlayComponent = NoRowsOverlayComponent;
    exports.LoadingOverlayComponent = LoadingOverlayComponent;
    exports.CommandCellComponent = CommandCellComponent;
    exports.CoreModalComponent = CoreModalComponent;
    exports.CoreOrgChartComponent = CoreOrgChartComponent;
    exports.BreadcrumbComponent = BreadcrumbComponent;
    exports.ListOfValuesModalComponent = ListOfValuesModalComponent;
    exports.ListOfValuesComponent = ListOfValuesComponent;
    exports.SelectorComponent = SelectorComponent;
    exports.StaticSelectorComponent = StaticSelectorComponent;
    exports.TabsComponent = TabsComponent;
    exports.TabComponent = TabComponent;
    exports.FormWizardComponent = FormWizardComponent;
    exports.FormWizardStepComponent = FormWizardStepComponent;
    exports.SystemReferenceDateComponent = SystemReferenceDateComponent;
    exports.CoreIconComponent = CoreIconComponent;
    exports.components = components;
    exports.FormCheckboxComponent = FormCheckboxComponent;
    exports.FormTextInputComponent = FormTextInputComponent;
    exports.FormNumberInputComponent = FormNumberInputComponent;
    exports.FormPasswordInputComponent = FormPasswordInputComponent;
    exports.FormCurrencyInputComponent = FormCurrencyInputComponent;
    exports.FormTextAreaComponent = FormTextAreaComponent;
    exports.FormDatepickerInputComponent = FormDatepickerInputComponent;
    exports.FormMaskInputComponent = FormMaskInputComponent;
    exports.HttpLoaderFactory = HttpLoaderFactory;
    exports.customCurrencyMaskConfig = customCurrencyMaskConfig;
    exports.CoreCommonModule = CoreCommonModule;
    exports.LoadingLibModule = LoadingLibModule;
    exports.SystemReferanceDateLibModule = SystemReferanceDateLibModule;
    exports.ɵf = BooleanEditCellComponent;
    exports.ɵe = coreGridEditElements;
    exports.ɵm = CurrencyEditCellComponent;
    exports.ɵg = DateEditCellComponent;
    exports.ɵh = LovEditCellComponent;
    exports.ɵl = MaskEditCellComponent;
    exports.ɵi = NumericEditCellComponent;
    exports.ɵj = SelectEditCellComponent;
    exports.ɵk = TextEditCellComponent;
    exports.ɵba = BooleanFilterCellComponent;
    exports.ɵv = coreGridFilterElements;
    exports.ɵbb = CurrencyFilterCellComponent;
    exports.ɵw = DateFilterCellComponent;
    exports.ɵbc = LovFilterCellComponent;
    exports.ɵbd = MaskFilterCellComponent;
    exports.ɵz = NumericFilterCellComponent;
    exports.ɵy = SelectFilterCellComponent;
    exports.ɵx = TextFilterCellComponent;
    exports.ɵo = BooleanCellComponent;
    exports.ɵn = coreGridReadOnlyElements;
    exports.ɵs = CurrencyCellComponent;
    exports.ɵr = DateCellComponent;
    exports.ɵp = LabelCellComponent;
    exports.ɵt = MaskCellComponent;
    exports.ɵu = NumericCellComponent;
    exports.ɵq = SelectCellComponent;
    exports.ɵb = CoreGridService;
    exports.ɵa = GridLoadingService;
    exports.ɵc = ListOfValuesService;
    exports.ɵbe = LoadingComponent;
    exports.ɵd = SelectorService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=core-common.umd.js.map